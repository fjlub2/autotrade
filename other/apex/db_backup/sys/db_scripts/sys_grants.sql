GRANT CREATE SEQUENCE          TO DAT;
GRANT CREATE SESSION           TO DAT;
GRANT CREATE TABLE             TO DAT;
GRANT CREATE TRIGGER           TO DAT;
GRANT CREATE VIEW              TO DAT;
GRANT CREATE TYPE              TO DAT;
GRANT CREATE MATERIALIZED VIEW TO DAT;
GRANT UNLIMITED TABLESPACE     TO DAT;


GRANT CREATE PROCEDURE TO API;
GRANT CREATE SESSION   TO API;
GRANT CREATE SYNONYM   TO API;
GRANT CREATE VIEW      TO API;
GRANT CREATE JOB       TO API;

GRANT EXECUTE ON DBMS_CRYPTO TO API;
GRANT EXECUTE ON UTL_SMTP TO API;
GRANT EXECUTE ON UTL_TCP TO API;


/* If the next grant fails, run the following scripts as SYS

   @$ORACLE_HOME/rdbms/admin/utlmail.sql
   @$ORACLE_HOME/rdbms/admin/prvtmail.plb
   
*/
--GRANT EXECUTE ON UTL_MAIL TO API;



GRANT CREATE SYNONYM TO APP;
GRANT CREATE SESSION TO APP;
-- other grants are assigned when the apex workspace is created


-- set the email server
--ALTER SYSTEM SET SMTP_OUT_SERVER = 'localhost:25';
/*
-- need this to be able to use the UTL_MAIL pkg
BEGIN
  -- Create ACL
  DBMS_NETWORK_ACL_ADMIN.CREATE_ACL (acl         => 'utl_mail.xml',
                                     description => 'Allow mail to be sent',
                                     principal   => 'API',
                                     is_grant    => TRUE,
                                     privilege   => 'connect');
  
  -- Add Privilege

  DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE (acl       => 'utl_mail.xml',
                                        principal => 'API',
                                        is_grant  => TRUE,
                                        privilege => 'resolve');

  -- Assign ACL
  DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL(acl  => 'utl_mail.xml',
                                    host => 'localhost',
                                    lower_port => 25,
                                    upper_port => 25);

  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    RAISE;
END;
/
*/

/*
BEGIN
  BEGIN
    DBMS_NETWORK_ACL_ADMIN.DROP_ACL('email_acl.xml');
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;
  
  -- Create ACL
  DBMS_NETWORK_ACL_ADMIN.CREATE_ACL (acl         => 'email_acl.xml',
                                     description => 'Allow mail to be sent',
                                     principal   => 'API',
                                     is_grant    => TRUE,
                                     privilege   => 'connect');
  
  -- Add Privilege

  DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE (acl       => 'email_acl.xml',
                                        principal => 'API',
                                        is_grant  => TRUE,
                                        privilege => 'resolve');
                                    
  -- Assign ACL
  DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL(acl  => 'email_acl.xml',
                                    host => 'email-smtp.us-east-1.amazonaws.com',
                                    lower_port => 25,
                                    upper_port => 25);

  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    RAISE;
END;
/
*/

BEGIN
  BEGIN
    DBMS_NETWORK_ACL_ADMIN.DROP_ACL('email_acl.xml');
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;
  
  -- Create ACL
  DBMS_NETWORK_ACL_ADMIN.CREATE_ACL (acl         => 'email_acl.xml',
                                     description => 'Allow mail to be sent',
                                     principal   => 'API',
                                     is_grant    => TRUE,
                                     privilege   => 'connect');
  
  -- Add Privilege

  DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE (acl       => 'email_acl.xml',
                                        principal => 'API',
                                        is_grant  => TRUE,
                                        privilege => 'resolve');
                                    
  -- Assign ACL
  DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL(acl  => 'email_acl.xml',
                                    host => 'smtp.gmail.com',
                                    lower_port => 587,
                                    upper_port => 587);

  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    RAISE;
END;
/

GRANT CREATE ANY CONTEXT TO API;

CREATE OR REPLACE CONTEXT SES_VPD USING API.C_SESSION;

CREATE OR REPLACE CONTEXT SEC_VPD USING API.C_SECURITY;



--Permissions required in dev, to be revoked from live system
GRANT EXECUTE ON UTL_FILE TO DAT;

GRANT CREATE PROCEDURE    TO DAT;
--Also, DdlUtils package should be dropped from live system
