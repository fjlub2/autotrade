CREATE OR REPLACE PACKAGE t_UserAddresses IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for user_addresses table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF user_addresses%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              uad_usr_id   user_addresses.uad_usr_id%TYPE,
                              uad_line_1   user_addresses.uad_line_1%TYPE,
                              uad_line_2   user_addresses.uad_line_2%TYPE,
                              uad_line_3   user_addresses.uad_line_3%TYPE,
                              uad_line_4   user_addresses.uad_line_4%TYPE,
                              uad_line_5   user_addresses.uad_line_5%TYPE,
                              uad_post_code   user_addresses.uad_post_code%TYPE,
                              uad_cty_id   user_addresses.uad_cty_id%TYPE,
                              uad_rgn_id   user_addresses.uad_rgn_id%TYPE,
                              uad_twn_id   user_addresses.uad_twn_id%TYPE,
                              uad_adf_id   user_addresses.uad_adf_id%TYPE,
                              uad_address_type   user_addresses.uad_address_type%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              uad_usr_id   VARCHAR2(1),
                              uad_line_1   VARCHAR2(1),
                              uad_line_2   VARCHAR2(1),
                              uad_line_3   VARCHAR2(1),
                              uad_line_4   VARCHAR2(1),
                              uad_line_5   VARCHAR2(1),
                              uad_post_code   VARCHAR2(1),
                              uad_cty_id   VARCHAR2(1),
                              uad_rgn_id   VARCHAR2(1),
                              uad_twn_id   VARCHAR2(1),
                              uad_adf_id   VARCHAR2(1),
                              uad_address_type   VARCHAR2(1));

 /* Returns a row from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses%ROWTYPE;

 /* Pipes a row from the USER_ADDRESSES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_ADDRESSES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses%ROWTYPE;

 /* Pipes a row from the USER_ADDRESSES table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_ADDRESSES table for the supplied UK */
  FUNCTION exists_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN VARCHAR2;

 /* Returns UAD_ID from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_id%TYPE;

 /* Returns UAD_ID from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_id%TYPE;

 /* Returns UAD_USR_ID from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getUsrId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_usr_id%TYPE;

 /* Returns UAD_USR_ID from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getUsrId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_usr_id%TYPE;

 /* Returns UAD_LINE_1 from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getLine1_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_line_1%TYPE;

 /* Returns UAD_LINE_1 from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getLine1_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_line_1%TYPE;

 /* Returns UAD_LINE_2 from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getLine2_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_line_2%TYPE;

 /* Returns UAD_LINE_2 from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getLine2_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_line_2%TYPE;

 /* Returns UAD_LINE_3 from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getLine3_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_line_3%TYPE;

 /* Returns UAD_LINE_3 from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getLine3_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_line_3%TYPE;

 /* Returns UAD_LINE_4 from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getLine4_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_line_4%TYPE;

 /* Returns UAD_LINE_4 from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getLine4_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_line_4%TYPE;

 /* Returns UAD_LINE_5 from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getLine5_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_line_5%TYPE;

 /* Returns UAD_LINE_5 from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getLine5_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_line_5%TYPE;

 /* Returns UAD_POST_CODE from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getPostCode_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_post_code%TYPE;

 /* Returns UAD_POST_CODE from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getPostCode_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_post_code%TYPE;

 /* Returns UAD_CTY_ID from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getCtyId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_cty_id%TYPE;

 /* Returns UAD_CTY_ID from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getCtyId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_cty_id%TYPE;

 /* Returns UAD_RGN_ID from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getRgnId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_rgn_id%TYPE;

 /* Returns UAD_RGN_ID from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getRgnId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_rgn_id%TYPE;

 /* Returns UAD_TWN_ID from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getTwnId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_twn_id%TYPE;

 /* Returns UAD_TWN_ID from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getTwnId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_twn_id%TYPE;

 /* Returns UAD_ADF_ID from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getAdfId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_adf_id%TYPE;

 /* Returns UAD_ADF_ID from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getAdfId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_adf_id%TYPE;

 /* Returns UAD_ADDRESS_TYPE from the USER_ADDRESSES table for the supplied PK */
  FUNCTION getAddressType_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_address_type%TYPE;

 /* Returns UAD_ADDRESS_TYPE from the USER_ADDRESSES table for the supplied UK */
  FUNCTION getAddressType_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_address_type%TYPE;

 /* Updates USER_ADDRESSES.UAD_USR_ID to the supplied value for the supplied PK */
  PROCEDURE setUsrId (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_usr_id%TYPE);

 /* Updates USER_ADDRESSES.UAD_LINE_1 to the supplied value for the supplied PK */
  PROCEDURE setLine1 (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_line_1%TYPE);

 /* Updates USER_ADDRESSES.UAD_LINE_2 to the supplied value for the supplied PK */
  PROCEDURE setLine2 (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_line_2%TYPE);

 /* Updates USER_ADDRESSES.UAD_LINE_3 to the supplied value for the supplied PK */
  PROCEDURE setLine3 (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_line_3%TYPE);

 /* Updates USER_ADDRESSES.UAD_LINE_4 to the supplied value for the supplied PK */
  PROCEDURE setLine4 (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_line_4%TYPE);

 /* Updates USER_ADDRESSES.UAD_LINE_5 to the supplied value for the supplied PK */
  PROCEDURE setLine5 (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_line_5%TYPE);

 /* Updates USER_ADDRESSES.UAD_POST_CODE to the supplied value for the supplied PK */
  PROCEDURE setPostCode (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_post_code%TYPE);

 /* Updates USER_ADDRESSES.UAD_CTY_ID to the supplied value for the supplied PK */
  PROCEDURE setCtyId (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_cty_id%TYPE);

 /* Updates USER_ADDRESSES.UAD_RGN_ID to the supplied value for the supplied PK */
  PROCEDURE setRgnId (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_rgn_id%TYPE);

 /* Updates USER_ADDRESSES.UAD_TWN_ID to the supplied value for the supplied PK */
  PROCEDURE setTwnId (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_twn_id%TYPE);

 /* Updates USER_ADDRESSES.UAD_ADF_ID to the supplied value for the supplied PK */
  PROCEDURE setAdfId (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_adf_id%TYPE);

 /* Updates USER_ADDRESSES.UAD_ADDRESS_TYPE to the supplied value for the supplied PK */
  PROCEDURE setAddressType (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_address_type%TYPE);

 /* Updates a row on the USER_ADDRESSES table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN user_addresses.uad_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the USER_ADDRESSES table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN user_addresses.uad_id%TYPE, 
                      p_usr_id IN user_addresses.uad_usr_id%TYPE,
                      p_line_1 IN user_addresses.uad_line_1%TYPE,
                      p_line_2 IN user_addresses.uad_line_2%TYPE,
                      p_line_3 IN user_addresses.uad_line_3%TYPE,
                      p_line_4 IN user_addresses.uad_line_4%TYPE,
                      p_line_5 IN user_addresses.uad_line_5%TYPE,
                      p_post_code IN user_addresses.uad_post_code%TYPE,
                      p_cty_id IN user_addresses.uad_cty_id%TYPE,
                      p_rgn_id IN user_addresses.uad_rgn_id%TYPE,
                      p_twn_id IN user_addresses.uad_twn_id%TYPE,
                      p_adf_id IN user_addresses.uad_adf_id%TYPE,
                      p_address_type IN user_addresses.uad_address_type%TYPE );

 /* Inserts a row into the USER_ADDRESSES table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN user_addresses%ROWTYPE, p_newPk OUT user_addresses.uad_id%TYPE);

 /* Inserts a row into the USER_ADDRESSES table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN user_addresses.uad_id%TYPE,
                      p_usr_id IN user_addresses.uad_usr_id%TYPE,
                      p_line_1 IN user_addresses.uad_line_1%TYPE,
                      p_line_2 IN user_addresses.uad_line_2%TYPE,
                      p_line_3 IN user_addresses.uad_line_3%TYPE,
                      p_line_4 IN user_addresses.uad_line_4%TYPE,
                      p_line_5 IN user_addresses.uad_line_5%TYPE,
                      p_post_code IN user_addresses.uad_post_code%TYPE,
                      p_cty_id IN user_addresses.uad_cty_id%TYPE,
                      p_rgn_id IN user_addresses.uad_rgn_id%TYPE,
                      p_twn_id IN user_addresses.uad_twn_id%TYPE,
                      p_adf_id IN user_addresses.uad_adf_id%TYPE,
                      p_address_type IN user_addresses.uad_address_type%TYPE, p_newPk OUT user_addresses.uad_id%TYPE);

 /* Deletes a row from the USER_ADDRESSES table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN user_addresses.uad_id%TYPE);

END t_UserAddresses;
/
