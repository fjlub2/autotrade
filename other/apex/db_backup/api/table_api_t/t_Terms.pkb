CREATE OR REPLACE PACKAGE BODY t_Terms IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN terms.ter_id%TYPE) RETURN terms%ROWTYPE;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN terms.ter_id%TYPE) RETURN terms%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN terms.ter_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN terms.ter_id%TYPE) RETURN VARCHAR2 IS
    l_row    terms%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.ter_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN terms.ter_id%TYPE) RETURN terms.ter_id%TYPE IS
    l_row    terms%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ter_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getText_PK (p_pk IN terms.ter_id%TYPE) RETURN terms.ter_text%TYPE IS
    l_row    terms%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ter_text;
  END getText_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDate_PK (p_pk IN terms.ter_id%TYPE) RETURN terms.ter_date%TYPE IS
    l_row    terms%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ter_date;
  END getDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCurrentInd_PK (p_pk IN terms.ter_id%TYPE) RETURN terms.ter_current_ind%TYPE IS
    l_row    terms%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ter_current_ind;
  END getCurrentInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getType_PK (p_pk IN terms.ter_id%TYPE) RETURN terms.ter_type%TYPE IS
    l_row    terms%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ter_type;
  END getType_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN terms.ter_id%TYPE) RETURN terms%ROWTYPE IS
    l_row    terms%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM terms
       WHERE ter_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

END t_Terms;
/
