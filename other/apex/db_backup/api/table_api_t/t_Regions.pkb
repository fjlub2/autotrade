CREATE OR REPLACE PACKAGE BODY t_Regions IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN regions.rgn_id%TYPE) RETURN regions%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN regions.rgn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN regions.rgn_id%TYPE);

  PROCEDURE doInsert (p_row IN regions%ROWTYPE, p_newPk OUT regions.rgn_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN regions.rgn_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN regions.rgn_id%TYPE) RETURN VARCHAR2 IS
    l_row    regions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.rgn_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_id%TYPE IS
    l_row    regions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.rgn_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCtyId_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_cty_id%TYPE IS
    l_row    regions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.rgn_cty_id;
  END getCtyId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_code%TYPE IS
    l_row    regions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.rgn_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIsoCode_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_iso_code%TYPE IS
    l_row    regions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.rgn_iso_code;
  END getIsoCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_name%TYPE IS
    l_row    regions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.rgn_name;
  END getName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_enabled_ind%TYPE IS
    l_row    regions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.rgn_enabled_ind;
  END getEnabledInd_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setCtyId (p_pk IN regions.rgn_id%TYPE, p_val IN regions.rgn_cty_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.rgn_cty_id := a_data.gFalse;
    l_ignoreRow.rgn_code := a_data.gTrue;
    l_ignoreRow.rgn_iso_code := a_data.gTrue;
    l_ignoreRow.rgn_name := a_data.gTrue;
    l_ignoreRow.rgn_enabled_ind := a_data.gTrue;

    l_updateRow.rgn_cty_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCtyId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCode (p_pk IN regions.rgn_id%TYPE, p_val IN regions.rgn_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.rgn_cty_id := a_data.gTrue;
    l_ignoreRow.rgn_code := a_data.gFalse;
    l_ignoreRow.rgn_iso_code := a_data.gTrue;
    l_ignoreRow.rgn_name := a_data.gTrue;
    l_ignoreRow.rgn_enabled_ind := a_data.gTrue;

    l_updateRow.rgn_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setIsoCode (p_pk IN regions.rgn_id%TYPE, p_val IN regions.rgn_iso_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.rgn_cty_id := a_data.gTrue;
    l_ignoreRow.rgn_code := a_data.gTrue;
    l_ignoreRow.rgn_iso_code := a_data.gFalse;
    l_ignoreRow.rgn_name := a_data.gTrue;
    l_ignoreRow.rgn_enabled_ind := a_data.gTrue;

    l_updateRow.rgn_iso_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setIsoCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setName (p_pk IN regions.rgn_id%TYPE, p_val IN regions.rgn_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.rgn_cty_id := a_data.gTrue;
    l_ignoreRow.rgn_code := a_data.gTrue;
    l_ignoreRow.rgn_iso_code := a_data.gTrue;
    l_ignoreRow.rgn_name := a_data.gFalse;
    l_ignoreRow.rgn_enabled_ind := a_data.gTrue;

    l_updateRow.rgn_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEnabledInd (p_pk IN regions.rgn_id%TYPE, p_val IN regions.rgn_enabled_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.rgn_cty_id := a_data.gTrue;
    l_ignoreRow.rgn_code := a_data.gTrue;
    l_ignoreRow.rgn_iso_code := a_data.gTrue;
    l_ignoreRow.rgn_name := a_data.gTrue;
    l_ignoreRow.rgn_enabled_ind := a_data.gFalse;

    l_updateRow.rgn_enabled_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEnabledInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN regions.rgn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN regions.rgn_id%TYPE, 
                      p_cty_id IN regions.rgn_cty_id%TYPE,
                      p_code IN regions.rgn_code%TYPE,
                      p_iso_code IN regions.rgn_iso_code%TYPE,
                      p_name IN regions.rgn_name%TYPE,
                      p_enabled_ind IN regions.rgn_enabled_ind%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.rgn_cty_id := p_cty_id;
    l_row.rgn_code := p_code;
    l_row.rgn_iso_code := p_iso_code;
    l_row.rgn_name := p_name;
    l_row.rgn_enabled_ind := p_enabled_ind;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN regions%ROWTYPE, p_newPK OUT regions.rgn_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN regions.rgn_id%TYPE,
                      p_cty_id IN regions.rgn_cty_id%TYPE,
                      p_code IN regions.rgn_code%TYPE,
                      p_iso_code IN regions.rgn_iso_code%TYPE,
                      p_name IN regions.rgn_name%TYPE,
                      p_enabled_ind IN regions.rgn_enabled_ind%TYPE, p_newPK OUT regions.rgn_id%TYPE) IS
    l_row   regions%ROWTYPE;
  BEGIN
    l_row.rgn_id := p_id;
    l_row.rgn_cty_id := p_cty_id;
    l_row.rgn_code := p_code;
    l_row.rgn_iso_code := p_iso_code;
    l_row.rgn_name := p_name;
    l_row.rgn_enabled_ind := p_enabled_ind;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN regions.rgn_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN regions.rgn_id%TYPE) RETURN regions%ROWTYPE IS
    l_row    regions%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM regions
       WHERE rgn_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN regions.rgn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE regions
         SET 
             rgn_cty_id = CASE NVL(p_ignore.rgn_cty_id, a_data.gFalse) WHEN a_data.gTrue THEN rgn_cty_id ELSE p_row.rgn_cty_id END,
             rgn_code = CASE NVL(p_ignore.rgn_code, a_data.gFalse) WHEN a_data.gTrue THEN rgn_code ELSE p_row.rgn_code END,
             rgn_iso_code = CASE NVL(p_ignore.rgn_iso_code, a_data.gFalse) WHEN a_data.gTrue THEN rgn_iso_code ELSE p_row.rgn_iso_code END,
             rgn_name = CASE NVL(p_ignore.rgn_name, a_data.gFalse) WHEN a_data.gTrue THEN rgn_name ELSE p_row.rgn_name END,
             rgn_enabled_ind = CASE NVL(p_ignore.rgn_enabled_ind, a_data.gFalse) WHEN a_data.gTrue THEN rgn_enabled_ind ELSE p_row.rgn_enabled_ind END
         WHERE rgn_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN regions.rgn_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE regions
       WHERE rgn_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN regions%ROWTYPE, p_newPk OUT regions.rgn_id%TYPE) IS
    l_pk    regions.rgn_id%TYPE;
  BEGIN 
    INSERT
      INTO regions
    VALUES p_row
    RETURNING rgn_id
         INTO p_newPk;
  END doInsert;

END t_Regions;
/
