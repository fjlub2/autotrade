CREATE OR REPLACE PACKAGE BODY t_SignupData IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN signup_data.sud_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN signup_data.sud_id%TYPE);

  PROCEDURE doInsert (p_row IN signup_data%ROWTYPE, p_newPk OUT signup_data.sud_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN signup_data.sud_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN signup_data.sud_id%TYPE) RETURN VARCHAR2 IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.sud_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_id%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getStoredDate_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_stored_date%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_stored_date;
  END getStoredDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSessionId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_session_id%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_session_id;
  END getSessionId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUserIp_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_user_ip%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_user_ip;
  END getUserIp_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsername_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_username%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_username;
  END getUsername_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getFirstName_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_first_name%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_first_name;
  END getFirstName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLastName_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_last_name%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_last_name;
  END getLastName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDob_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_dob%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_dob;
  END getDob_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getHomePhoneNo_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_home_phone_no%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_home_phone_no;
  END getHomePhoneNo_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMobilePhoneNo_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_mobile_phone_no%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_mobile_phone_no;
  END getMobilePhoneNo_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getBusinessPhoneNo_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_business_phone_no%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_business_phone_no;
  END getBusinessPhoneNo_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEmail_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_email%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_email;
  END getEmail_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLngId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_lng_id%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_lng_id;
  END getLngId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getResAddLine1_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_res_add_line_1%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_res_add_line_1;
  END getResAddLine1_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getResAddLine2_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_res_add_line_2%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_res_add_line_2;
  END getResAddLine2_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getResAddCity_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_res_add_city%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_res_add_city;
  END getResAddCity_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getResAddPostCode_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_res_add_post_code%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_res_add_post_code;
  END getResAddPostCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getResAddCtyId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_res_add_cty_id%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_res_add_cty_id;
  END getResAddCtyId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTermsAcceptInd_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_terms_accept_ind%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_terms_accept_ind;
  END getTermsAcceptInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getBilAddSameAsResInd_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_same_as_res_ind%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_bil_add_same_as_res_ind;
  END getBilAddSameAsResInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getBilAddLine1_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_line_1%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_bil_add_line_1;
  END getBilAddLine1_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getBilAddLine2_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_line_2%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_bil_add_line_2;
  END getBilAddLine2_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getBilAddCity_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_city%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_bil_add_city;
  END getBilAddCity_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getBilAddPostCode_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_post_code%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_bil_add_post_code;
  END getBilAddPostCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getBilAddCtyId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_cty_id%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_bil_add_cty_id;
  END getBilAddCtyId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPplId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_ppl_id%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_ppl_id;
  END getPplId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPymId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_pym_id%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_pym_id;
  END getPymId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCcNo_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_cc_no%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_cc_no;
  END getCcNo_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCcExpiryMonth_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_cc_expiry_month%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_cc_expiry_month;
  END getCcExpiryMonth_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCcExpiryYear_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_cc_expiry_year%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_cc_expiry_year;
  END getCcExpiryYear_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCcCvc_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_cc_cvc%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_cc_cvc;
  END getCcCvc_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAcceptedInd_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_accepted_ind%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_accepted_ind;
  END getAcceptedInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAcceptedDate_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_accepted_date%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_accepted_date;
  END getAcceptedDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_usr_id%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_usr_id;
  END getUsrId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEmailValidatedInd_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_email_validated_ind%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_email_validated_ind;
  END getEmailValidatedInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSessionCode_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_session_code%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_session_code;
  END getSessionCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getExpiryDate_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_expiry_date%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_expiry_date;
  END getExpiryDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getReplacedDate_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_replaced_date%TYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sud_replaced_date;
  END getReplacedDate_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setStoredDate (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_stored_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gFalse;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_stored_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setStoredDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setSessionId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_session_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gFalse;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_session_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setSessionId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUserIp (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_user_ip%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gFalse;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_user_ip := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUserIp;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUsername (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_username%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gFalse;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_username := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsername;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setFirstName (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_first_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gFalse;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_first_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setFirstName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLastName (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_last_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gFalse;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_last_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLastName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDob (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_dob%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gFalse;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_dob := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDob;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setHomePhoneNo (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_home_phone_no%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gFalse;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_home_phone_no := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setHomePhoneNo;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setMobilePhoneNo (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_mobile_phone_no%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gFalse;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_mobile_phone_no := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setMobilePhoneNo;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setBusinessPhoneNo (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_business_phone_no%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gFalse;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_business_phone_no := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setBusinessPhoneNo;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEmail (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_email%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gFalse;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_email := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEmail;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLngId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_lng_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gFalse;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_lng_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLngId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setResAddLine1 (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_res_add_line_1%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gFalse;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_res_add_line_1 := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setResAddLine1;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setResAddLine2 (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_res_add_line_2%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gFalse;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_res_add_line_2 := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setResAddLine2;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setResAddCity (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_res_add_city%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gFalse;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_res_add_city := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setResAddCity;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setResAddPostCode (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_res_add_post_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gFalse;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_res_add_post_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setResAddPostCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setResAddCtyId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_res_add_cty_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gFalse;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_res_add_cty_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setResAddCtyId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTermsAcceptInd (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_terms_accept_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gFalse;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_terms_accept_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTermsAcceptInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setBilAddSameAsResInd (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_same_as_res_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gFalse;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_bil_add_same_as_res_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setBilAddSameAsResInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setBilAddLine1 (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_line_1%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gFalse;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_bil_add_line_1 := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setBilAddLine1;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setBilAddLine2 (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_line_2%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gFalse;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_bil_add_line_2 := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setBilAddLine2;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setBilAddCity (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_city%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gFalse;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_bil_add_city := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setBilAddCity;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setBilAddPostCode (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_post_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gFalse;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_bil_add_post_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setBilAddPostCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setBilAddCtyId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_cty_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gFalse;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_bil_add_cty_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setBilAddCtyId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setPplId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_ppl_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gFalse;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_ppl_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setPplId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setPymId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_pym_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gFalse;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_pym_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setPymId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCcNo (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_cc_no%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gFalse;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_cc_no := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCcNo;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCcExpiryMonth (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_cc_expiry_month%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gFalse;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_cc_expiry_month := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCcExpiryMonth;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCcExpiryYear (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_cc_expiry_year%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gFalse;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_cc_expiry_year := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCcExpiryYear;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCcCvc (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_cc_cvc%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gFalse;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_cc_cvc := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCcCvc;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setAcceptedInd (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_accepted_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gFalse;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_accepted_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setAcceptedInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setAcceptedDate (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_accepted_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gFalse;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_accepted_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setAcceptedDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUsrId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_usr_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gFalse;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_usr_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsrId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEmailValidatedInd (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_email_validated_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gFalse;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_email_validated_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEmailValidatedInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setSessionCode (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_session_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gFalse;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_session_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setSessionCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setExpiryDate (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_expiry_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gFalse;
    l_ignoreRow.sud_replaced_date := a_data.gTrue;

    l_updateRow.sud_expiry_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setExpiryDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setReplacedDate (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_replaced_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sud_stored_date := a_data.gTrue;
    l_ignoreRow.sud_session_id := a_data.gTrue;
    l_ignoreRow.sud_user_ip := a_data.gTrue;
    l_ignoreRow.sud_username := a_data.gTrue;
    l_ignoreRow.sud_first_name := a_data.gTrue;
    l_ignoreRow.sud_last_name := a_data.gTrue;
    l_ignoreRow.sud_dob := a_data.gTrue;
    l_ignoreRow.sud_home_phone_no := a_data.gTrue;
    l_ignoreRow.sud_mobile_phone_no := a_data.gTrue;
    l_ignoreRow.sud_business_phone_no := a_data.gTrue;
    l_ignoreRow.sud_email := a_data.gTrue;
    l_ignoreRow.sud_lng_id := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_res_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_res_add_city := a_data.gTrue;
    l_ignoreRow.sud_res_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_res_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_1 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_line_2 := a_data.gTrue;
    l_ignoreRow.sud_bil_add_city := a_data.gTrue;
    l_ignoreRow.sud_bil_add_post_code := a_data.gTrue;
    l_ignoreRow.sud_bil_add_cty_id := a_data.gTrue;
    l_ignoreRow.sud_ppl_id := a_data.gTrue;
    l_ignoreRow.sud_pym_id := a_data.gTrue;
    l_ignoreRow.sud_cc_no := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_month := a_data.gTrue;
    l_ignoreRow.sud_cc_expiry_year := a_data.gTrue;
    l_ignoreRow.sud_cc_cvc := a_data.gTrue;
    l_ignoreRow.sud_accepted_ind := a_data.gTrue;
    l_ignoreRow.sud_accepted_date := a_data.gTrue;
    l_ignoreRow.sud_usr_id := a_data.gTrue;
    l_ignoreRow.sud_email_validated_ind := a_data.gTrue;
    l_ignoreRow.sud_session_code := a_data.gTrue;
    l_ignoreRow.sud_expiry_date := a_data.gTrue;
    l_ignoreRow.sud_replaced_date := a_data.gFalse;

    l_updateRow.sud_replaced_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setReplacedDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN signup_data.sud_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN signup_data.sud_id%TYPE, 
                      p_stored_date IN signup_data.sud_stored_date%TYPE,
                      p_session_id IN signup_data.sud_session_id%TYPE,
                      p_user_ip IN signup_data.sud_user_ip%TYPE,
                      p_username IN signup_data.sud_username%TYPE,
                      p_first_name IN signup_data.sud_first_name%TYPE,
                      p_last_name IN signup_data.sud_last_name%TYPE,
                      p_dob IN signup_data.sud_dob%TYPE,
                      p_home_phone_no IN signup_data.sud_home_phone_no%TYPE,
                      p_mobile_phone_no IN signup_data.sud_mobile_phone_no%TYPE,
                      p_business_phone_no IN signup_data.sud_business_phone_no%TYPE,
                      p_email IN signup_data.sud_email%TYPE,
                      p_lng_id IN signup_data.sud_lng_id%TYPE,
                      p_res_add_line_1 IN signup_data.sud_res_add_line_1%TYPE,
                      p_res_add_line_2 IN signup_data.sud_res_add_line_2%TYPE,
                      p_res_add_city IN signup_data.sud_res_add_city%TYPE,
                      p_res_add_post_code IN signup_data.sud_res_add_post_code%TYPE,
                      p_res_add_cty_id IN signup_data.sud_res_add_cty_id%TYPE,
                      p_terms_accept_ind IN signup_data.sud_terms_accept_ind%TYPE,
                      p_bil_add_same_as_res_ind IN signup_data.sud_bil_add_same_as_res_ind%TYPE,
                      p_bil_add_line_1 IN signup_data.sud_bil_add_line_1%TYPE,
                      p_bil_add_line_2 IN signup_data.sud_bil_add_line_2%TYPE,
                      p_bil_add_city IN signup_data.sud_bil_add_city%TYPE,
                      p_bil_add_post_code IN signup_data.sud_bil_add_post_code%TYPE,
                      p_bil_add_cty_id IN signup_data.sud_bil_add_cty_id%TYPE,
                      p_ppl_id IN signup_data.sud_ppl_id%TYPE,
                      p_pym_id IN signup_data.sud_pym_id%TYPE,
                      p_cc_no IN signup_data.sud_cc_no%TYPE,
                      p_cc_expiry_month IN signup_data.sud_cc_expiry_month%TYPE,
                      p_cc_expiry_year IN signup_data.sud_cc_expiry_year%TYPE,
                      p_cc_cvc IN signup_data.sud_cc_cvc%TYPE,
                      p_accepted_ind IN signup_data.sud_accepted_ind%TYPE,
                      p_accepted_date IN signup_data.sud_accepted_date%TYPE,
                      p_usr_id IN signup_data.sud_usr_id%TYPE,
                      p_email_validated_ind IN signup_data.sud_email_validated_ind%TYPE,
                      p_session_code IN signup_data.sud_session_code%TYPE,
                      p_expiry_date IN signup_data.sud_expiry_date%TYPE,
                      p_replaced_date IN signup_data.sud_replaced_date%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.sud_stored_date := p_stored_date;
    l_row.sud_session_id := p_session_id;
    l_row.sud_user_ip := p_user_ip;
    l_row.sud_username := p_username;
    l_row.sud_first_name := p_first_name;
    l_row.sud_last_name := p_last_name;
    l_row.sud_dob := p_dob;
    l_row.sud_home_phone_no := p_home_phone_no;
    l_row.sud_mobile_phone_no := p_mobile_phone_no;
    l_row.sud_business_phone_no := p_business_phone_no;
    l_row.sud_email := p_email;
    l_row.sud_lng_id := p_lng_id;
    l_row.sud_res_add_line_1 := p_res_add_line_1;
    l_row.sud_res_add_line_2 := p_res_add_line_2;
    l_row.sud_res_add_city := p_res_add_city;
    l_row.sud_res_add_post_code := p_res_add_post_code;
    l_row.sud_res_add_cty_id := p_res_add_cty_id;
    l_row.sud_terms_accept_ind := p_terms_accept_ind;
    l_row.sud_bil_add_same_as_res_ind := p_bil_add_same_as_res_ind;
    l_row.sud_bil_add_line_1 := p_bil_add_line_1;
    l_row.sud_bil_add_line_2 := p_bil_add_line_2;
    l_row.sud_bil_add_city := p_bil_add_city;
    l_row.sud_bil_add_post_code := p_bil_add_post_code;
    l_row.sud_bil_add_cty_id := p_bil_add_cty_id;
    l_row.sud_ppl_id := p_ppl_id;
    l_row.sud_pym_id := p_pym_id;
    l_row.sud_cc_no := p_cc_no;
    l_row.sud_cc_expiry_month := p_cc_expiry_month;
    l_row.sud_cc_expiry_year := p_cc_expiry_year;
    l_row.sud_cc_cvc := p_cc_cvc;
    l_row.sud_accepted_ind := p_accepted_ind;
    l_row.sud_accepted_date := p_accepted_date;
    l_row.sud_usr_id := p_usr_id;
    l_row.sud_email_validated_ind := p_email_validated_ind;
    l_row.sud_session_code := p_session_code;
    l_row.sud_expiry_date := p_expiry_date;
    l_row.sud_replaced_date := p_replaced_date;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN signup_data%ROWTYPE, p_newPK OUT signup_data.sud_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN signup_data.sud_id%TYPE,
                      p_stored_date IN signup_data.sud_stored_date%TYPE,
                      p_session_id IN signup_data.sud_session_id%TYPE,
                      p_user_ip IN signup_data.sud_user_ip%TYPE,
                      p_username IN signup_data.sud_username%TYPE,
                      p_first_name IN signup_data.sud_first_name%TYPE,
                      p_last_name IN signup_data.sud_last_name%TYPE,
                      p_dob IN signup_data.sud_dob%TYPE,
                      p_home_phone_no IN signup_data.sud_home_phone_no%TYPE,
                      p_mobile_phone_no IN signup_data.sud_mobile_phone_no%TYPE,
                      p_business_phone_no IN signup_data.sud_business_phone_no%TYPE,
                      p_email IN signup_data.sud_email%TYPE,
                      p_lng_id IN signup_data.sud_lng_id%TYPE,
                      p_res_add_line_1 IN signup_data.sud_res_add_line_1%TYPE,
                      p_res_add_line_2 IN signup_data.sud_res_add_line_2%TYPE,
                      p_res_add_city IN signup_data.sud_res_add_city%TYPE,
                      p_res_add_post_code IN signup_data.sud_res_add_post_code%TYPE,
                      p_res_add_cty_id IN signup_data.sud_res_add_cty_id%TYPE,
                      p_terms_accept_ind IN signup_data.sud_terms_accept_ind%TYPE,
                      p_bil_add_same_as_res_ind IN signup_data.sud_bil_add_same_as_res_ind%TYPE,
                      p_bil_add_line_1 IN signup_data.sud_bil_add_line_1%TYPE,
                      p_bil_add_line_2 IN signup_data.sud_bil_add_line_2%TYPE,
                      p_bil_add_city IN signup_data.sud_bil_add_city%TYPE,
                      p_bil_add_post_code IN signup_data.sud_bil_add_post_code%TYPE,
                      p_bil_add_cty_id IN signup_data.sud_bil_add_cty_id%TYPE,
                      p_ppl_id IN signup_data.sud_ppl_id%TYPE,
                      p_pym_id IN signup_data.sud_pym_id%TYPE,
                      p_cc_no IN signup_data.sud_cc_no%TYPE,
                      p_cc_expiry_month IN signup_data.sud_cc_expiry_month%TYPE,
                      p_cc_expiry_year IN signup_data.sud_cc_expiry_year%TYPE,
                      p_cc_cvc IN signup_data.sud_cc_cvc%TYPE,
                      p_accepted_ind IN signup_data.sud_accepted_ind%TYPE,
                      p_accepted_date IN signup_data.sud_accepted_date%TYPE,
                      p_usr_id IN signup_data.sud_usr_id%TYPE,
                      p_email_validated_ind IN signup_data.sud_email_validated_ind%TYPE,
                      p_session_code IN signup_data.sud_session_code%TYPE,
                      p_expiry_date IN signup_data.sud_expiry_date%TYPE,
                      p_replaced_date IN signup_data.sud_replaced_date%TYPE, p_newPK OUT signup_data.sud_id%TYPE) IS
    l_row   signup_data%ROWTYPE;
  BEGIN
    l_row.sud_id := p_id;
    l_row.sud_stored_date := p_stored_date;
    l_row.sud_session_id := p_session_id;
    l_row.sud_user_ip := p_user_ip;
    l_row.sud_username := p_username;
    l_row.sud_first_name := p_first_name;
    l_row.sud_last_name := p_last_name;
    l_row.sud_dob := p_dob;
    l_row.sud_home_phone_no := p_home_phone_no;
    l_row.sud_mobile_phone_no := p_mobile_phone_no;
    l_row.sud_business_phone_no := p_business_phone_no;
    l_row.sud_email := p_email;
    l_row.sud_lng_id := p_lng_id;
    l_row.sud_res_add_line_1 := p_res_add_line_1;
    l_row.sud_res_add_line_2 := p_res_add_line_2;
    l_row.sud_res_add_city := p_res_add_city;
    l_row.sud_res_add_post_code := p_res_add_post_code;
    l_row.sud_res_add_cty_id := p_res_add_cty_id;
    l_row.sud_terms_accept_ind := p_terms_accept_ind;
    l_row.sud_bil_add_same_as_res_ind := p_bil_add_same_as_res_ind;
    l_row.sud_bil_add_line_1 := p_bil_add_line_1;
    l_row.sud_bil_add_line_2 := p_bil_add_line_2;
    l_row.sud_bil_add_city := p_bil_add_city;
    l_row.sud_bil_add_post_code := p_bil_add_post_code;
    l_row.sud_bil_add_cty_id := p_bil_add_cty_id;
    l_row.sud_ppl_id := p_ppl_id;
    l_row.sud_pym_id := p_pym_id;
    l_row.sud_cc_no := p_cc_no;
    l_row.sud_cc_expiry_month := p_cc_expiry_month;
    l_row.sud_cc_expiry_year := p_cc_expiry_year;
    l_row.sud_cc_cvc := p_cc_cvc;
    l_row.sud_accepted_ind := p_accepted_ind;
    l_row.sud_accepted_date := p_accepted_date;
    l_row.sud_usr_id := p_usr_id;
    l_row.sud_email_validated_ind := p_email_validated_ind;
    l_row.sud_session_code := p_session_code;
    l_row.sud_expiry_date := p_expiry_date;
    l_row.sud_replaced_date := p_replaced_date;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN signup_data.sud_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data%ROWTYPE IS
    l_row    signup_data%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM signup_data
       WHERE sud_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN signup_data.sud_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE signup_data
         SET 
             sud_stored_date = CASE NVL(p_ignore.sud_stored_date, a_data.gFalse) WHEN a_data.gTrue THEN sud_stored_date ELSE p_row.sud_stored_date END,
             sud_session_id = CASE NVL(p_ignore.sud_session_id, a_data.gFalse) WHEN a_data.gTrue THEN sud_session_id ELSE p_row.sud_session_id END,
             sud_user_ip = CASE NVL(p_ignore.sud_user_ip, a_data.gFalse) WHEN a_data.gTrue THEN sud_user_ip ELSE p_row.sud_user_ip END,
             sud_username = CASE NVL(p_ignore.sud_username, a_data.gFalse) WHEN a_data.gTrue THEN sud_username ELSE p_row.sud_username END,
             sud_first_name = CASE NVL(p_ignore.sud_first_name, a_data.gFalse) WHEN a_data.gTrue THEN sud_first_name ELSE p_row.sud_first_name END,
             sud_last_name = CASE NVL(p_ignore.sud_last_name, a_data.gFalse) WHEN a_data.gTrue THEN sud_last_name ELSE p_row.sud_last_name END,
             sud_dob = CASE NVL(p_ignore.sud_dob, a_data.gFalse) WHEN a_data.gTrue THEN sud_dob ELSE p_row.sud_dob END,
             sud_home_phone_no = CASE NVL(p_ignore.sud_home_phone_no, a_data.gFalse) WHEN a_data.gTrue THEN sud_home_phone_no ELSE p_row.sud_home_phone_no END,
             sud_mobile_phone_no = CASE NVL(p_ignore.sud_mobile_phone_no, a_data.gFalse) WHEN a_data.gTrue THEN sud_mobile_phone_no ELSE p_row.sud_mobile_phone_no END,
             sud_business_phone_no = CASE NVL(p_ignore.sud_business_phone_no, a_data.gFalse) WHEN a_data.gTrue THEN sud_business_phone_no ELSE p_row.sud_business_phone_no END,
             sud_email = CASE NVL(p_ignore.sud_email, a_data.gFalse) WHEN a_data.gTrue THEN sud_email ELSE p_row.sud_email END,
             sud_lng_id = CASE NVL(p_ignore.sud_lng_id, a_data.gFalse) WHEN a_data.gTrue THEN sud_lng_id ELSE p_row.sud_lng_id END,
             sud_res_add_line_1 = CASE NVL(p_ignore.sud_res_add_line_1, a_data.gFalse) WHEN a_data.gTrue THEN sud_res_add_line_1 ELSE p_row.sud_res_add_line_1 END,
             sud_res_add_line_2 = CASE NVL(p_ignore.sud_res_add_line_2, a_data.gFalse) WHEN a_data.gTrue THEN sud_res_add_line_2 ELSE p_row.sud_res_add_line_2 END,
             sud_res_add_city = CASE NVL(p_ignore.sud_res_add_city, a_data.gFalse) WHEN a_data.gTrue THEN sud_res_add_city ELSE p_row.sud_res_add_city END,
             sud_res_add_post_code = CASE NVL(p_ignore.sud_res_add_post_code, a_data.gFalse) WHEN a_data.gTrue THEN sud_res_add_post_code ELSE p_row.sud_res_add_post_code END,
             sud_res_add_cty_id = CASE NVL(p_ignore.sud_res_add_cty_id, a_data.gFalse) WHEN a_data.gTrue THEN sud_res_add_cty_id ELSE p_row.sud_res_add_cty_id END,
             sud_terms_accept_ind = CASE NVL(p_ignore.sud_terms_accept_ind, a_data.gFalse) WHEN a_data.gTrue THEN sud_terms_accept_ind ELSE p_row.sud_terms_accept_ind END,
             sud_bil_add_same_as_res_ind = CASE NVL(p_ignore.sud_bil_add_same_as_res_ind, a_data.gFalse) WHEN a_data.gTrue THEN sud_bil_add_same_as_res_ind ELSE p_row.sud_bil_add_same_as_res_ind END,
             sud_bil_add_line_1 = CASE NVL(p_ignore.sud_bil_add_line_1, a_data.gFalse) WHEN a_data.gTrue THEN sud_bil_add_line_1 ELSE p_row.sud_bil_add_line_1 END,
             sud_bil_add_line_2 = CASE NVL(p_ignore.sud_bil_add_line_2, a_data.gFalse) WHEN a_data.gTrue THEN sud_bil_add_line_2 ELSE p_row.sud_bil_add_line_2 END,
             sud_bil_add_city = CASE NVL(p_ignore.sud_bil_add_city, a_data.gFalse) WHEN a_data.gTrue THEN sud_bil_add_city ELSE p_row.sud_bil_add_city END,
             sud_bil_add_post_code = CASE NVL(p_ignore.sud_bil_add_post_code, a_data.gFalse) WHEN a_data.gTrue THEN sud_bil_add_post_code ELSE p_row.sud_bil_add_post_code END,
             sud_bil_add_cty_id = CASE NVL(p_ignore.sud_bil_add_cty_id, a_data.gFalse) WHEN a_data.gTrue THEN sud_bil_add_cty_id ELSE p_row.sud_bil_add_cty_id END,
             sud_ppl_id = CASE NVL(p_ignore.sud_ppl_id, a_data.gFalse) WHEN a_data.gTrue THEN sud_ppl_id ELSE p_row.sud_ppl_id END,
             sud_pym_id = CASE NVL(p_ignore.sud_pym_id, a_data.gFalse) WHEN a_data.gTrue THEN sud_pym_id ELSE p_row.sud_pym_id END,
             sud_cc_no = CASE NVL(p_ignore.sud_cc_no, a_data.gFalse) WHEN a_data.gTrue THEN sud_cc_no ELSE p_row.sud_cc_no END,
             sud_cc_expiry_month = CASE NVL(p_ignore.sud_cc_expiry_month, a_data.gFalse) WHEN a_data.gTrue THEN sud_cc_expiry_month ELSE p_row.sud_cc_expiry_month END,
             sud_cc_expiry_year = CASE NVL(p_ignore.sud_cc_expiry_year, a_data.gFalse) WHEN a_data.gTrue THEN sud_cc_expiry_year ELSE p_row.sud_cc_expiry_year END,
             sud_cc_cvc = CASE NVL(p_ignore.sud_cc_cvc, a_data.gFalse) WHEN a_data.gTrue THEN sud_cc_cvc ELSE p_row.sud_cc_cvc END,
             sud_accepted_ind = CASE NVL(p_ignore.sud_accepted_ind, a_data.gFalse) WHEN a_data.gTrue THEN sud_accepted_ind ELSE p_row.sud_accepted_ind END,
             sud_accepted_date = CASE NVL(p_ignore.sud_accepted_date, a_data.gFalse) WHEN a_data.gTrue THEN sud_accepted_date ELSE p_row.sud_accepted_date END,
             sud_usr_id = CASE NVL(p_ignore.sud_usr_id, a_data.gFalse) WHEN a_data.gTrue THEN sud_usr_id ELSE p_row.sud_usr_id END,
             sud_email_validated_ind = CASE NVL(p_ignore.sud_email_validated_ind, a_data.gFalse) WHEN a_data.gTrue THEN sud_email_validated_ind ELSE p_row.sud_email_validated_ind END,
             sud_session_code = CASE NVL(p_ignore.sud_session_code, a_data.gFalse) WHEN a_data.gTrue THEN sud_session_code ELSE p_row.sud_session_code END,
             sud_expiry_date = CASE NVL(p_ignore.sud_expiry_date, a_data.gFalse) WHEN a_data.gTrue THEN sud_expiry_date ELSE p_row.sud_expiry_date END,
             sud_replaced_date = CASE NVL(p_ignore.sud_replaced_date, a_data.gFalse) WHEN a_data.gTrue THEN sud_replaced_date ELSE p_row.sud_replaced_date END
         WHERE sud_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN signup_data.sud_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE signup_data
       WHERE sud_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN signup_data%ROWTYPE, p_newPk OUT signup_data.sud_id%TYPE) IS
    l_pk    signup_data.sud_id%TYPE;
  BEGIN 
    INSERT
      INTO signup_data
    VALUES p_row
    RETURNING sud_id
         INTO p_newPk;
  END doInsert;

END t_SignupData;
/
