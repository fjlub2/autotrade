CREATE OR REPLACE PACKAGE t_SignupData IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for signup_data table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF signup_data%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              sud_stored_date   signup_data.sud_stored_date%TYPE,
                              sud_session_id   signup_data.sud_session_id%TYPE,
                              sud_user_ip   signup_data.sud_user_ip%TYPE,
                              sud_username   signup_data.sud_username%TYPE,
                              sud_first_name   signup_data.sud_first_name%TYPE,
                              sud_last_name   signup_data.sud_last_name%TYPE,
                              sud_dob   signup_data.sud_dob%TYPE,
                              sud_home_phone_no   signup_data.sud_home_phone_no%TYPE,
                              sud_mobile_phone_no   signup_data.sud_mobile_phone_no%TYPE,
                              sud_business_phone_no   signup_data.sud_business_phone_no%TYPE,
                              sud_email   signup_data.sud_email%TYPE,
                              sud_lng_id   signup_data.sud_lng_id%TYPE,
                              sud_res_add_line_1   signup_data.sud_res_add_line_1%TYPE,
                              sud_res_add_line_2   signup_data.sud_res_add_line_2%TYPE,
                              sud_res_add_city   signup_data.sud_res_add_city%TYPE,
                              sud_res_add_post_code   signup_data.sud_res_add_post_code%TYPE,
                              sud_res_add_cty_id   signup_data.sud_res_add_cty_id%TYPE,
                              sud_terms_accept_ind   signup_data.sud_terms_accept_ind%TYPE,
                              sud_bil_add_same_as_res_ind   signup_data.sud_bil_add_same_as_res_ind%TYPE,
                              sud_bil_add_line_1   signup_data.sud_bil_add_line_1%TYPE,
                              sud_bil_add_line_2   signup_data.sud_bil_add_line_2%TYPE,
                              sud_bil_add_city   signup_data.sud_bil_add_city%TYPE,
                              sud_bil_add_post_code   signup_data.sud_bil_add_post_code%TYPE,
                              sud_bil_add_cty_id   signup_data.sud_bil_add_cty_id%TYPE,
                              sud_ppl_id   signup_data.sud_ppl_id%TYPE,
                              sud_pym_id   signup_data.sud_pym_id%TYPE,
                              sud_cc_no   signup_data.sud_cc_no%TYPE,
                              sud_cc_expiry_month   signup_data.sud_cc_expiry_month%TYPE,
                              sud_cc_expiry_year   signup_data.sud_cc_expiry_year%TYPE,
                              sud_cc_cvc   signup_data.sud_cc_cvc%TYPE,
                              sud_accepted_ind   signup_data.sud_accepted_ind%TYPE,
                              sud_accepted_date   signup_data.sud_accepted_date%TYPE,
                              sud_usr_id   signup_data.sud_usr_id%TYPE,
                              sud_email_validated_ind   signup_data.sud_email_validated_ind%TYPE,
                              sud_session_code   signup_data.sud_session_code%TYPE,
                              sud_expiry_date   signup_data.sud_expiry_date%TYPE,
                              sud_replaced_date   signup_data.sud_replaced_date%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              sud_stored_date   VARCHAR2(1),
                              sud_session_id   VARCHAR2(1),
                              sud_user_ip   VARCHAR2(1),
                              sud_username   VARCHAR2(1),
                              sud_first_name   VARCHAR2(1),
                              sud_last_name   VARCHAR2(1),
                              sud_dob   VARCHAR2(1),
                              sud_home_phone_no   VARCHAR2(1),
                              sud_mobile_phone_no   VARCHAR2(1),
                              sud_business_phone_no   VARCHAR2(1),
                              sud_email   VARCHAR2(1),
                              sud_lng_id   VARCHAR2(1),
                              sud_res_add_line_1   VARCHAR2(1),
                              sud_res_add_line_2   VARCHAR2(1),
                              sud_res_add_city   VARCHAR2(1),
                              sud_res_add_post_code   VARCHAR2(1),
                              sud_res_add_cty_id   VARCHAR2(1),
                              sud_terms_accept_ind   VARCHAR2(1),
                              sud_bil_add_same_as_res_ind   VARCHAR2(1),
                              sud_bil_add_line_1   VARCHAR2(1),
                              sud_bil_add_line_2   VARCHAR2(1),
                              sud_bil_add_city   VARCHAR2(1),
                              sud_bil_add_post_code   VARCHAR2(1),
                              sud_bil_add_cty_id   VARCHAR2(1),
                              sud_ppl_id   VARCHAR2(1),
                              sud_pym_id   VARCHAR2(1),
                              sud_cc_no   VARCHAR2(1),
                              sud_cc_expiry_month   VARCHAR2(1),
                              sud_cc_expiry_year   VARCHAR2(1),
                              sud_cc_cvc   VARCHAR2(1),
                              sud_accepted_ind   VARCHAR2(1),
                              sud_accepted_date   VARCHAR2(1),
                              sud_usr_id   VARCHAR2(1),
                              sud_email_validated_ind   VARCHAR2(1),
                              sud_session_code   VARCHAR2(1),
                              sud_expiry_date   VARCHAR2(1),
                              sud_replaced_date   VARCHAR2(1));

 /* Returns a row from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data%ROWTYPE;

 /* Pipes a row from the SIGNUP_DATA table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN signup_data.sud_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the SIGNUP_DATA table for the supplied PK */
  FUNCTION exists_PK (p_pk IN signup_data.sud_id%TYPE) RETURN VARCHAR2;

 /* Returns SUD_ID from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_id%TYPE;

 /* Returns SUD_STORED_DATE from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getStoredDate_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_stored_date%TYPE;

 /* Returns SUD_SESSION_ID from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getSessionId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_session_id%TYPE;

 /* Returns SUD_USER_IP from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getUserIp_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_user_ip%TYPE;

 /* Returns SUD_USERNAME from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getUsername_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_username%TYPE;

 /* Returns SUD_FIRST_NAME from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getFirstName_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_first_name%TYPE;

 /* Returns SUD_LAST_NAME from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getLastName_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_last_name%TYPE;

 /* Returns SUD_DOB from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getDob_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_dob%TYPE;

 /* Returns SUD_HOME_PHONE_NO from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getHomePhoneNo_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_home_phone_no%TYPE;

 /* Returns SUD_MOBILE_PHONE_NO from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getMobilePhoneNo_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_mobile_phone_no%TYPE;

 /* Returns SUD_BUSINESS_PHONE_NO from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getBusinessPhoneNo_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_business_phone_no%TYPE;

 /* Returns SUD_EMAIL from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getEmail_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_email%TYPE;

 /* Returns SUD_LNG_ID from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getLngId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_lng_id%TYPE;

 /* Returns SUD_RES_ADD_LINE_1 from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getResAddLine1_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_res_add_line_1%TYPE;

 /* Returns SUD_RES_ADD_LINE_2 from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getResAddLine2_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_res_add_line_2%TYPE;

 /* Returns SUD_RES_ADD_CITY from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getResAddCity_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_res_add_city%TYPE;

 /* Returns SUD_RES_ADD_POST_CODE from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getResAddPostCode_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_res_add_post_code%TYPE;

 /* Returns SUD_RES_ADD_CTY_ID from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getResAddCtyId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_res_add_cty_id%TYPE;

 /* Returns SUD_TERMS_ACCEPT_IND from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getTermsAcceptInd_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_terms_accept_ind%TYPE;

 /* Returns SUD_BIL_ADD_SAME_AS_RES_IND from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getBilAddSameAsResInd_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_same_as_res_ind%TYPE;

 /* Returns SUD_BIL_ADD_LINE_1 from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getBilAddLine1_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_line_1%TYPE;

 /* Returns SUD_BIL_ADD_LINE_2 from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getBilAddLine2_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_line_2%TYPE;

 /* Returns SUD_BIL_ADD_CITY from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getBilAddCity_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_city%TYPE;

 /* Returns SUD_BIL_ADD_POST_CODE from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getBilAddPostCode_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_post_code%TYPE;

 /* Returns SUD_BIL_ADD_CTY_ID from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getBilAddCtyId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_bil_add_cty_id%TYPE;

 /* Returns SUD_PPL_ID from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getPplId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_ppl_id%TYPE;

 /* Returns SUD_PYM_ID from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getPymId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_pym_id%TYPE;

 /* Returns SUD_CC_NO from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getCcNo_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_cc_no%TYPE;

 /* Returns SUD_CC_EXPIRY_MONTH from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getCcExpiryMonth_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_cc_expiry_month%TYPE;

 /* Returns SUD_CC_EXPIRY_YEAR from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getCcExpiryYear_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_cc_expiry_year%TYPE;

 /* Returns SUD_CC_CVC from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getCcCvc_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_cc_cvc%TYPE;

 /* Returns SUD_ACCEPTED_IND from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getAcceptedInd_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_accepted_ind%TYPE;

 /* Returns SUD_ACCEPTED_DATE from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getAcceptedDate_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_accepted_date%TYPE;

 /* Returns SUD_USR_ID from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getUsrId_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_usr_id%TYPE;

 /* Returns SUD_EMAIL_VALIDATED_IND from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getEmailValidatedInd_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_email_validated_ind%TYPE;

 /* Returns SUD_SESSION_CODE from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getSessionCode_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_session_code%TYPE;

 /* Returns SUD_EXPIRY_DATE from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getExpiryDate_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_expiry_date%TYPE;

 /* Returns SUD_REPLACED_DATE from the SIGNUP_DATA table for the supplied PK */
  FUNCTION getReplacedDate_PK (p_pk IN signup_data.sud_id%TYPE) RETURN signup_data.sud_replaced_date%TYPE;

 /* Updates SIGNUP_DATA.SUD_STORED_DATE to the supplied value for the supplied PK */
  PROCEDURE setStoredDate (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_stored_date%TYPE);

 /* Updates SIGNUP_DATA.SUD_SESSION_ID to the supplied value for the supplied PK */
  PROCEDURE setSessionId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_session_id%TYPE);

 /* Updates SIGNUP_DATA.SUD_USER_IP to the supplied value for the supplied PK */
  PROCEDURE setUserIp (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_user_ip%TYPE);

 /* Updates SIGNUP_DATA.SUD_USERNAME to the supplied value for the supplied PK */
  PROCEDURE setUsername (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_username%TYPE);

 /* Updates SIGNUP_DATA.SUD_FIRST_NAME to the supplied value for the supplied PK */
  PROCEDURE setFirstName (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_first_name%TYPE);

 /* Updates SIGNUP_DATA.SUD_LAST_NAME to the supplied value for the supplied PK */
  PROCEDURE setLastName (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_last_name%TYPE);

 /* Updates SIGNUP_DATA.SUD_DOB to the supplied value for the supplied PK */
  PROCEDURE setDob (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_dob%TYPE);

 /* Updates SIGNUP_DATA.SUD_HOME_PHONE_NO to the supplied value for the supplied PK */
  PROCEDURE setHomePhoneNo (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_home_phone_no%TYPE);

 /* Updates SIGNUP_DATA.SUD_MOBILE_PHONE_NO to the supplied value for the supplied PK */
  PROCEDURE setMobilePhoneNo (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_mobile_phone_no%TYPE);

 /* Updates SIGNUP_DATA.SUD_BUSINESS_PHONE_NO to the supplied value for the supplied PK */
  PROCEDURE setBusinessPhoneNo (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_business_phone_no%TYPE);

 /* Updates SIGNUP_DATA.SUD_EMAIL to the supplied value for the supplied PK */
  PROCEDURE setEmail (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_email%TYPE);

 /* Updates SIGNUP_DATA.SUD_LNG_ID to the supplied value for the supplied PK */
  PROCEDURE setLngId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_lng_id%TYPE);

 /* Updates SIGNUP_DATA.SUD_RES_ADD_LINE_1 to the supplied value for the supplied PK */
  PROCEDURE setResAddLine1 (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_res_add_line_1%TYPE);

 /* Updates SIGNUP_DATA.SUD_RES_ADD_LINE_2 to the supplied value for the supplied PK */
  PROCEDURE setResAddLine2 (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_res_add_line_2%TYPE);

 /* Updates SIGNUP_DATA.SUD_RES_ADD_CITY to the supplied value for the supplied PK */
  PROCEDURE setResAddCity (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_res_add_city%TYPE);

 /* Updates SIGNUP_DATA.SUD_RES_ADD_POST_CODE to the supplied value for the supplied PK */
  PROCEDURE setResAddPostCode (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_res_add_post_code%TYPE);

 /* Updates SIGNUP_DATA.SUD_RES_ADD_CTY_ID to the supplied value for the supplied PK */
  PROCEDURE setResAddCtyId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_res_add_cty_id%TYPE);

 /* Updates SIGNUP_DATA.SUD_TERMS_ACCEPT_IND to the supplied value for the supplied PK */
  PROCEDURE setTermsAcceptInd (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_terms_accept_ind%TYPE);

 /* Updates SIGNUP_DATA.SUD_BIL_ADD_SAME_AS_RES_IND to the supplied value for the supplied PK */
  PROCEDURE setBilAddSameAsResInd (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_same_as_res_ind%TYPE);

 /* Updates SIGNUP_DATA.SUD_BIL_ADD_LINE_1 to the supplied value for the supplied PK */
  PROCEDURE setBilAddLine1 (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_line_1%TYPE);

 /* Updates SIGNUP_DATA.SUD_BIL_ADD_LINE_2 to the supplied value for the supplied PK */
  PROCEDURE setBilAddLine2 (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_line_2%TYPE);

 /* Updates SIGNUP_DATA.SUD_BIL_ADD_CITY to the supplied value for the supplied PK */
  PROCEDURE setBilAddCity (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_city%TYPE);

 /* Updates SIGNUP_DATA.SUD_BIL_ADD_POST_CODE to the supplied value for the supplied PK */
  PROCEDURE setBilAddPostCode (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_post_code%TYPE);

 /* Updates SIGNUP_DATA.SUD_BIL_ADD_CTY_ID to the supplied value for the supplied PK */
  PROCEDURE setBilAddCtyId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_bil_add_cty_id%TYPE);

 /* Updates SIGNUP_DATA.SUD_PPL_ID to the supplied value for the supplied PK */
  PROCEDURE setPplId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_ppl_id%TYPE);

 /* Updates SIGNUP_DATA.SUD_PYM_ID to the supplied value for the supplied PK */
  PROCEDURE setPymId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_pym_id%TYPE);

 /* Updates SIGNUP_DATA.SUD_CC_NO to the supplied value for the supplied PK */
  PROCEDURE setCcNo (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_cc_no%TYPE);

 /* Updates SIGNUP_DATA.SUD_CC_EXPIRY_MONTH to the supplied value for the supplied PK */
  PROCEDURE setCcExpiryMonth (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_cc_expiry_month%TYPE);

 /* Updates SIGNUP_DATA.SUD_CC_EXPIRY_YEAR to the supplied value for the supplied PK */
  PROCEDURE setCcExpiryYear (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_cc_expiry_year%TYPE);

 /* Updates SIGNUP_DATA.SUD_CC_CVC to the supplied value for the supplied PK */
  PROCEDURE setCcCvc (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_cc_cvc%TYPE);

 /* Updates SIGNUP_DATA.SUD_ACCEPTED_IND to the supplied value for the supplied PK */
  PROCEDURE setAcceptedInd (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_accepted_ind%TYPE);

 /* Updates SIGNUP_DATA.SUD_ACCEPTED_DATE to the supplied value for the supplied PK */
  PROCEDURE setAcceptedDate (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_accepted_date%TYPE);

 /* Updates SIGNUP_DATA.SUD_USR_ID to the supplied value for the supplied PK */
  PROCEDURE setUsrId (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_usr_id%TYPE);

 /* Updates SIGNUP_DATA.SUD_EMAIL_VALIDATED_IND to the supplied value for the supplied PK */
  PROCEDURE setEmailValidatedInd (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_email_validated_ind%TYPE);

 /* Updates SIGNUP_DATA.SUD_SESSION_CODE to the supplied value for the supplied PK */
  PROCEDURE setSessionCode (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_session_code%TYPE);

 /* Updates SIGNUP_DATA.SUD_EXPIRY_DATE to the supplied value for the supplied PK */
  PROCEDURE setExpiryDate (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_expiry_date%TYPE);

 /* Updates SIGNUP_DATA.SUD_REPLACED_DATE to the supplied value for the supplied PK */
  PROCEDURE setReplacedDate (p_pk IN signup_data.sud_id%TYPE, p_val IN signup_data.sud_replaced_date%TYPE);

 /* Updates a row on the SIGNUP_DATA table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN signup_data.sud_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the SIGNUP_DATA table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN signup_data.sud_id%TYPE, 
                      p_stored_date IN signup_data.sud_stored_date%TYPE,
                      p_session_id IN signup_data.sud_session_id%TYPE,
                      p_user_ip IN signup_data.sud_user_ip%TYPE,
                      p_username IN signup_data.sud_username%TYPE,
                      p_first_name IN signup_data.sud_first_name%TYPE,
                      p_last_name IN signup_data.sud_last_name%TYPE,
                      p_dob IN signup_data.sud_dob%TYPE,
                      p_home_phone_no IN signup_data.sud_home_phone_no%TYPE,
                      p_mobile_phone_no IN signup_data.sud_mobile_phone_no%TYPE,
                      p_business_phone_no IN signup_data.sud_business_phone_no%TYPE,
                      p_email IN signup_data.sud_email%TYPE,
                      p_lng_id IN signup_data.sud_lng_id%TYPE,
                      p_res_add_line_1 IN signup_data.sud_res_add_line_1%TYPE,
                      p_res_add_line_2 IN signup_data.sud_res_add_line_2%TYPE,
                      p_res_add_city IN signup_data.sud_res_add_city%TYPE,
                      p_res_add_post_code IN signup_data.sud_res_add_post_code%TYPE,
                      p_res_add_cty_id IN signup_data.sud_res_add_cty_id%TYPE,
                      p_terms_accept_ind IN signup_data.sud_terms_accept_ind%TYPE,
                      p_bil_add_same_as_res_ind IN signup_data.sud_bil_add_same_as_res_ind%TYPE,
                      p_bil_add_line_1 IN signup_data.sud_bil_add_line_1%TYPE,
                      p_bil_add_line_2 IN signup_data.sud_bil_add_line_2%TYPE,
                      p_bil_add_city IN signup_data.sud_bil_add_city%TYPE,
                      p_bil_add_post_code IN signup_data.sud_bil_add_post_code%TYPE,
                      p_bil_add_cty_id IN signup_data.sud_bil_add_cty_id%TYPE,
                      p_ppl_id IN signup_data.sud_ppl_id%TYPE,
                      p_pym_id IN signup_data.sud_pym_id%TYPE,
                      p_cc_no IN signup_data.sud_cc_no%TYPE,
                      p_cc_expiry_month IN signup_data.sud_cc_expiry_month%TYPE,
                      p_cc_expiry_year IN signup_data.sud_cc_expiry_year%TYPE,
                      p_cc_cvc IN signup_data.sud_cc_cvc%TYPE,
                      p_accepted_ind IN signup_data.sud_accepted_ind%TYPE,
                      p_accepted_date IN signup_data.sud_accepted_date%TYPE,
                      p_usr_id IN signup_data.sud_usr_id%TYPE,
                      p_email_validated_ind IN signup_data.sud_email_validated_ind%TYPE,
                      p_session_code IN signup_data.sud_session_code%TYPE,
                      p_expiry_date IN signup_data.sud_expiry_date%TYPE,
                      p_replaced_date IN signup_data.sud_replaced_date%TYPE );

 /* Inserts a row into the SIGNUP_DATA table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN signup_data%ROWTYPE, p_newPk OUT signup_data.sud_id%TYPE);

 /* Inserts a row into the SIGNUP_DATA table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN signup_data.sud_id%TYPE,
                      p_stored_date IN signup_data.sud_stored_date%TYPE,
                      p_session_id IN signup_data.sud_session_id%TYPE,
                      p_user_ip IN signup_data.sud_user_ip%TYPE,
                      p_username IN signup_data.sud_username%TYPE,
                      p_first_name IN signup_data.sud_first_name%TYPE,
                      p_last_name IN signup_data.sud_last_name%TYPE,
                      p_dob IN signup_data.sud_dob%TYPE,
                      p_home_phone_no IN signup_data.sud_home_phone_no%TYPE,
                      p_mobile_phone_no IN signup_data.sud_mobile_phone_no%TYPE,
                      p_business_phone_no IN signup_data.sud_business_phone_no%TYPE,
                      p_email IN signup_data.sud_email%TYPE,
                      p_lng_id IN signup_data.sud_lng_id%TYPE,
                      p_res_add_line_1 IN signup_data.sud_res_add_line_1%TYPE,
                      p_res_add_line_2 IN signup_data.sud_res_add_line_2%TYPE,
                      p_res_add_city IN signup_data.sud_res_add_city%TYPE,
                      p_res_add_post_code IN signup_data.sud_res_add_post_code%TYPE,
                      p_res_add_cty_id IN signup_data.sud_res_add_cty_id%TYPE,
                      p_terms_accept_ind IN signup_data.sud_terms_accept_ind%TYPE,
                      p_bil_add_same_as_res_ind IN signup_data.sud_bil_add_same_as_res_ind%TYPE,
                      p_bil_add_line_1 IN signup_data.sud_bil_add_line_1%TYPE,
                      p_bil_add_line_2 IN signup_data.sud_bil_add_line_2%TYPE,
                      p_bil_add_city IN signup_data.sud_bil_add_city%TYPE,
                      p_bil_add_post_code IN signup_data.sud_bil_add_post_code%TYPE,
                      p_bil_add_cty_id IN signup_data.sud_bil_add_cty_id%TYPE,
                      p_ppl_id IN signup_data.sud_ppl_id%TYPE,
                      p_pym_id IN signup_data.sud_pym_id%TYPE,
                      p_cc_no IN signup_data.sud_cc_no%TYPE,
                      p_cc_expiry_month IN signup_data.sud_cc_expiry_month%TYPE,
                      p_cc_expiry_year IN signup_data.sud_cc_expiry_year%TYPE,
                      p_cc_cvc IN signup_data.sud_cc_cvc%TYPE,
                      p_accepted_ind IN signup_data.sud_accepted_ind%TYPE,
                      p_accepted_date IN signup_data.sud_accepted_date%TYPE,
                      p_usr_id IN signup_data.sud_usr_id%TYPE,
                      p_email_validated_ind IN signup_data.sud_email_validated_ind%TYPE,
                      p_session_code IN signup_data.sud_session_code%TYPE,
                      p_expiry_date IN signup_data.sud_expiry_date%TYPE,
                      p_replaced_date IN signup_data.sud_replaced_date%TYPE, p_newPk OUT signup_data.sud_id%TYPE);

 /* Deletes a row from the SIGNUP_DATA table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN signup_data.sud_id%TYPE);

END t_SignupData;
/
