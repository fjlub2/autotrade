CREATE OR REPLACE PACKAGE BODY t_CardTypes IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN card_types.cct_id%TYPE) RETURN card_types%ROWTYPE;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN card_types.cct_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN card_types.cct_id%TYPE) RETURN VARCHAR2 IS
    l_row    card_types%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.cct_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_id%TYPE IS
    l_row    card_types%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cct_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getNetworkName_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_network_name%TYPE IS
    l_row    card_types%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cct_network_name;
  END getNetworkName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getActiveInd_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_active_ind%TYPE IS
    l_row    card_types%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cct_active_ind;
  END getActiveInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLhunValidation_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_lhun_validation%TYPE IS
    l_row    card_types%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cct_lhun_validation;
  END getLhunValidation_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAllowedInd_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_allowed_ind%TYPE IS
    l_row    card_types%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cct_allowed_ind;
  END getAllowedInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIcon_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_icon%TYPE IS
    l_row    card_types%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cct_icon;
  END getIcon_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN card_types.cct_id%TYPE) RETURN card_types%ROWTYPE IS
    l_row    card_types%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM card_types
       WHERE cct_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

END t_CardTypes;
/
