CREATE OR REPLACE PACKAGE t_Currencies IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for currencies table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF currencies%ROWTYPE;

 /* Returns a row from the CURRENCIES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies%ROWTYPE;

 /* Pipes a row from the CURRENCIES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN currencies.ccy_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the CURRENCIES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN currencies.ccy_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the CURRENCIES table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies%ROWTYPE;

 /* Pipes a row from the CURRENCIES table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN currencies.ccy_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the CURRENCIES table for the supplied UK */
  FUNCTION exists_UK (p_uk IN currencies.ccy_code%TYPE) RETURN VARCHAR2;

 /* Returns CCY_ID from the CURRENCIES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_id%TYPE;

 /* Returns CCY_ID from the CURRENCIES table for the supplied UK */
  FUNCTION getId_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_id%TYPE;

 /* Returns CCY_CODE from the CURRENCIES table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_code%TYPE;

 /* Returns CCY_CODE from the CURRENCIES table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_code%TYPE;

 /* Returns CCY_SYMBOL from the CURRENCIES table for the supplied PK */
  FUNCTION getSymbol_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_symbol%TYPE;

 /* Returns CCY_SYMBOL from the CURRENCIES table for the supplied UK */
  FUNCTION getSymbol_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_symbol%TYPE;

 /* Returns CCY_DECIMALS from the CURRENCIES table for the supplied PK */
  FUNCTION getDecimals_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_decimals%TYPE;

 /* Returns CCY_DECIMALS from the CURRENCIES table for the supplied UK */
  FUNCTION getDecimals_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_decimals%TYPE;

 /* Returns CCY_NAME from the CURRENCIES table for the supplied PK */
  FUNCTION getName_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_name%TYPE;

 /* Returns CCY_NAME from the CURRENCIES table for the supplied UK */
  FUNCTION getName_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_name%TYPE;

 /* Returns CCY_FORMAT from the CURRENCIES table for the supplied PK */
  FUNCTION getFormat_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_format%TYPE;

 /* Returns CCY_FORMAT from the CURRENCIES table for the supplied UK */
  FUNCTION getFormat_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_format%TYPE;

END t_Currencies;
/
