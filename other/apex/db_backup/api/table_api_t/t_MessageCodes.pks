CREATE OR REPLACE PACKAGE t_MessageCodes IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for message_codes table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF message_codes%ROWTYPE;

 /* Returns a row from the MESSAGE_CODES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN message_codes.msc_id%TYPE) RETURN message_codes%ROWTYPE;

 /* Pipes a row from the MESSAGE_CODES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN message_codes.msc_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the MESSAGE_CODES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN message_codes.msc_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the MESSAGE_CODES table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN message_codes%ROWTYPE;

 /* Pipes a row from the MESSAGE_CODES table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the MESSAGE_CODES table for the supplied UK */
  FUNCTION exists_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN VARCHAR2;

 /* Returns MSC_ID from the MESSAGE_CODES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN message_codes.msc_id%TYPE) RETURN message_codes.msc_id%TYPE;

 /* Returns MSC_ID from the MESSAGE_CODES table for the supplied UK */
  FUNCTION getId_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN message_codes.msc_id%TYPE;

 /* Returns MSC_CODE from the MESSAGE_CODES table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN message_codes.msc_id%TYPE) RETURN message_codes.msc_code%TYPE;

 /* Returns MSC_CODE from the MESSAGE_CODES table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN message_codes.msc_code%TYPE;

 /* Returns MSC_DEFAULT_TEXT from the MESSAGE_CODES table for the supplied PK */
  FUNCTION getDefaultText_PK (p_pk IN message_codes.msc_id%TYPE) RETURN message_codes.msc_default_text%TYPE;

 /* Returns MSC_DEFAULT_TEXT from the MESSAGE_CODES table for the supplied UK */
  FUNCTION getDefaultText_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN message_codes.msc_default_text%TYPE;

 /* Returns MSC_CONSTRAINT_NAME from the MESSAGE_CODES table for the supplied PK */
  FUNCTION getConstraintName_PK (p_pk IN message_codes.msc_id%TYPE) RETURN message_codes.msc_constraint_name%TYPE;

 /* Returns MSC_CONSTRAINT_NAME from the MESSAGE_CODES table for the supplied UK */
  FUNCTION getConstraintName_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN message_codes.msc_constraint_name%TYPE;

END t_MessageCodes;
/
