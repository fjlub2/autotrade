CREATE OR REPLACE PACKAGE BODY t_PaymentMethods IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN payment_methods.pym_id%TYPE, p_uk IN payment_methods.pym_code%TYPE DEFAULT NULL) RETURN payment_methods%ROWTYPE;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN VARCHAR2 IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.pym_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN VARCHAR2 IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.pym_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_id%TYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.pym_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_id%TYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.pym_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_code%TYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.pym_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_code%TYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.pym_code;
  END getCode_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_name%TYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.pym_name;
  END getName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_name%TYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.pym_name;
  END getName_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_enabled_ind%TYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.pym_enabled_ind;
  END getEnabledInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_enabled_ind%TYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.pym_enabled_ind;
  END getEnabledInd_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIcon_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_icon%TYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.pym_icon;
  END getIcon_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIcon_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_icon%TYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.pym_icon;
  END getIcon_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN payment_methods.pym_id%TYPE, p_uk IN payment_methods.pym_code%TYPE DEFAULT NULL) RETURN payment_methods%ROWTYPE IS
    l_row    payment_methods%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM payment_methods
       WHERE pym_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM payment_methods
       WHERE pym_code = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

END t_PaymentMethods;
/
