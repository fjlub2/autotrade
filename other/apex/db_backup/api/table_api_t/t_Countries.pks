CREATE OR REPLACE PACKAGE t_Countries IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for countries table

   Should be applied on the CORE schema. No grants or synonyms should be applied to this package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF countries%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              cty_iso_2_code   countries.cty_iso_2_code%TYPE,
                              cty_iso_3_code   countries.cty_iso_3_code%TYPE,
                              cty_name   countries.cty_name%TYPE,
                              cty_enabled_ind   countries.cty_enabled_ind%TYPE,
                              cty_adf_id   countries.cty_adf_id%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              cty_iso_2_code   VARCHAR2(1),
                              cty_iso_3_code   VARCHAR2(1),
                              cty_name   VARCHAR2(1),
                              cty_enabled_ind   VARCHAR2(1),
                              cty_adf_id   VARCHAR2(1));

 /* Returns a row from the COUNTRIES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN countries.cty_id%TYPE) RETURN countries%ROWTYPE;

 /* Pipes a row from the COUNTRIES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN countries.cty_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the COUNTRIES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN countries.cty_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the COUNTRIES table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries%ROWTYPE;

 /* Pipes a row from the COUNTRIES table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the COUNTRIES table for the supplied UK */
  FUNCTION exists_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN VARCHAR2;

 /* Returns CTY_ID from the COUNTRIES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_id%TYPE;

 /* Returns CTY_ID from the COUNTRIES table for the supplied UK */
  FUNCTION getId_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_id%TYPE;

 /* Returns CTY_ISO_2_CODE from the COUNTRIES table for the supplied PK */
  FUNCTION getIso2Code_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_iso_2_code%TYPE;

 /* Returns CTY_ISO_2_CODE from the COUNTRIES table for the supplied UK */
  FUNCTION getIso2Code_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_iso_2_code%TYPE;

 /* Returns CTY_ISO_3_CODE from the COUNTRIES table for the supplied PK */
  FUNCTION getIso3Code_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_iso_3_code%TYPE;

 /* Returns CTY_ISO_3_CODE from the COUNTRIES table for the supplied UK */
  FUNCTION getIso3Code_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_iso_3_code%TYPE;

 /* Returns CTY_NAME from the COUNTRIES table for the supplied PK */
  FUNCTION getName_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_name%TYPE;

 /* Returns CTY_NAME from the COUNTRIES table for the supplied UK */
  FUNCTION getName_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_name%TYPE;

 /* Returns CTY_ENABLED_IND from the COUNTRIES table for the supplied PK */
  FUNCTION getEnabledInd_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_enabled_ind%TYPE;

 /* Returns CTY_ENABLED_IND from the COUNTRIES table for the supplied UK */
  FUNCTION getEnabledInd_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_enabled_ind%TYPE;

 /* Returns CTY_ADF_ID from the COUNTRIES table for the supplied PK */
  FUNCTION getAdfId_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_adf_id%TYPE;

 /* Returns CTY_ADF_ID from the COUNTRIES table for the supplied UK */
  FUNCTION getAdfId_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_adf_id%TYPE;

 /* Updates COUNTRIES.CTY_ISO_2_CODE to the supplied value for the supplied PK */
  PROCEDURE setIso2Code (p_pk IN countries.cty_id%TYPE, p_val IN countries.cty_iso_2_code%TYPE);

 /* Updates COUNTRIES.CTY_ISO_3_CODE to the supplied value for the supplied PK */
  PROCEDURE setIso3Code (p_pk IN countries.cty_id%TYPE, p_val IN countries.cty_iso_3_code%TYPE);

 /* Updates COUNTRIES.CTY_NAME to the supplied value for the supplied PK */
  PROCEDURE setName (p_pk IN countries.cty_id%TYPE, p_val IN countries.cty_name%TYPE);

 /* Updates COUNTRIES.CTY_ENABLED_IND to the supplied value for the supplied PK */
  PROCEDURE setEnabledInd (p_pk IN countries.cty_id%TYPE, p_val IN countries.cty_enabled_ind%TYPE);

 /* Updates COUNTRIES.CTY_ADF_ID to the supplied value for the supplied PK */
  PROCEDURE setAdfId (p_pk IN countries.cty_id%TYPE, p_val IN countries.cty_adf_id%TYPE);

 /* Updates a row on the COUNTRIES table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN countries.cty_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the COUNTRIES table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN countries.cty_id%TYPE, 
                      p_iso_2_code IN countries.cty_iso_2_code%TYPE,
                      p_iso_3_code IN countries.cty_iso_3_code%TYPE,
                      p_name IN countries.cty_name%TYPE,
                      p_enabled_ind IN countries.cty_enabled_ind%TYPE,
                      p_adf_id IN countries.cty_adf_id%TYPE );

 /* Inserts a row into the COUNTRIES table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN countries%ROWTYPE, p_newPk OUT countries.cty_id%TYPE);

 /* Inserts a row into the COUNTRIES table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN countries.cty_id%TYPE,
                      p_iso_2_code IN countries.cty_iso_2_code%TYPE,
                      p_iso_3_code IN countries.cty_iso_3_code%TYPE,
                      p_name IN countries.cty_name%TYPE,
                      p_enabled_ind IN countries.cty_enabled_ind%TYPE,
                      p_adf_id IN countries.cty_adf_id%TYPE, p_newPk OUT countries.cty_id%TYPE);

 /* Deletes a row from the COUNTRIES table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN countries.cty_id%TYPE);

END t_Countries;
/
