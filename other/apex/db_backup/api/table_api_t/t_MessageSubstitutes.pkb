CREATE OR REPLACE PACKAGE BODY t_MessageSubstitutes IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN message_substitutes.msu_id%TYPE, p_uk IN message_substitutes.msu_msc_id%TYPE DEFAULT NULL, p_uk2 IN message_substitutes.msu_code%TYPE DEFAULT NULL) RETURN message_substitutes%ROWTYPE;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN message_substitutes%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN VARCHAR2 IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.msu_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN message_substitutes%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk, p_uk2 => p_uk2);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk, p_uk2 => p_uk2));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN VARCHAR2 IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    IF l_row.msu_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN message_substitutes.msu_id%TYPE IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.msu_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN message_substitutes.msu_id%TYPE IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.msu_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMscId_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN message_substitutes.msu_msc_id%TYPE IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.msu_msc_id;
  END getMscId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMscId_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN message_substitutes.msu_msc_id%TYPE IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.msu_msc_id;
  END getMscId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN message_substitutes.msu_code%TYPE IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.msu_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN message_substitutes.msu_code%TYPE IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.msu_code;
  END getCode_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN message_substitutes.msu_name%TYPE IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.msu_name;
  END getName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN message_substitutes.msu_name%TYPE IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.msu_name;
  END getName_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN message_substitutes.msu_id%TYPE, p_uk IN message_substitutes.msu_msc_id%TYPE DEFAULT NULL, p_uk2 IN message_substitutes.msu_code%TYPE DEFAULT NULL) RETURN message_substitutes%ROWTYPE IS
    l_row    message_substitutes%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM message_substitutes
       WHERE msu_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM message_substitutes
       WHERE msu_msc_id = p_uk
         AND msu_code = p_uk2;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

END t_MessageSubstitutes;
/
