CREATE OR REPLACE PACKAGE BODY t_MessageLog2 IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN message_log_2.mss_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN message_log_2.mss_id%TYPE);

  PROCEDURE doInsert (p_row IN message_log_2%ROWTYPE, p_newPk OUT message_log_2.mss_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN VARCHAR2 IS
    l_row    message_log_2%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.mss_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_id%TYPE IS
    l_row    message_log_2%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mss_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUssId_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_uss_id%TYPE IS
    l_row    message_log_2%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mss_uss_id;
  END getUssId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDate_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_date%TYPE IS
    l_row    message_log_2%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mss_date;
  END getDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getType_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_type%TYPE IS
    l_row    message_log_2%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mss_type;
  END getType_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMessage_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_message%TYPE IS
    l_row    message_log_2%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mss_message;
  END getMessage_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getStack_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_stack%TYPE IS
    l_row    message_log_2%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mss_stack;
  END getStack_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTrace_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_trace%TYPE IS
    l_row    message_log_2%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mss_trace;
  END getTrace_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTaskId_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_task_id%TYPE IS
    l_row    message_log_2%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mss_task_id;
  END getTaskId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getData_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_data%TYPE IS
    l_row    message_log_2%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mss_data;
  END getData_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setUssId (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_uss_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mss_uss_id := a_data.gFalse;
    l_ignoreRow.mss_date := a_data.gTrue;
    l_ignoreRow.mss_type := a_data.gTrue;
    l_ignoreRow.mss_message := a_data.gTrue;
    l_ignoreRow.mss_stack := a_data.gTrue;
    l_ignoreRow.mss_trace := a_data.gTrue;
    l_ignoreRow.mss_task_id := a_data.gTrue;
    l_ignoreRow.mss_data := a_data.gTrue;

    l_updateRow.mss_uss_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUssId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDate (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mss_uss_id := a_data.gTrue;
    l_ignoreRow.mss_date := a_data.gFalse;
    l_ignoreRow.mss_type := a_data.gTrue;
    l_ignoreRow.mss_message := a_data.gTrue;
    l_ignoreRow.mss_stack := a_data.gTrue;
    l_ignoreRow.mss_trace := a_data.gTrue;
    l_ignoreRow.mss_task_id := a_data.gTrue;
    l_ignoreRow.mss_data := a_data.gTrue;

    l_updateRow.mss_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setType (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_type%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mss_uss_id := a_data.gTrue;
    l_ignoreRow.mss_date := a_data.gTrue;
    l_ignoreRow.mss_type := a_data.gFalse;
    l_ignoreRow.mss_message := a_data.gTrue;
    l_ignoreRow.mss_stack := a_data.gTrue;
    l_ignoreRow.mss_trace := a_data.gTrue;
    l_ignoreRow.mss_task_id := a_data.gTrue;
    l_ignoreRow.mss_data := a_data.gTrue;

    l_updateRow.mss_type := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setType;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setMessage (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_message%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mss_uss_id := a_data.gTrue;
    l_ignoreRow.mss_date := a_data.gTrue;
    l_ignoreRow.mss_type := a_data.gTrue;
    l_ignoreRow.mss_message := a_data.gFalse;
    l_ignoreRow.mss_stack := a_data.gTrue;
    l_ignoreRow.mss_trace := a_data.gTrue;
    l_ignoreRow.mss_task_id := a_data.gTrue;
    l_ignoreRow.mss_data := a_data.gTrue;

    l_updateRow.mss_message := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setMessage;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setStack (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_stack%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mss_uss_id := a_data.gTrue;
    l_ignoreRow.mss_date := a_data.gTrue;
    l_ignoreRow.mss_type := a_data.gTrue;
    l_ignoreRow.mss_message := a_data.gTrue;
    l_ignoreRow.mss_stack := a_data.gFalse;
    l_ignoreRow.mss_trace := a_data.gTrue;
    l_ignoreRow.mss_task_id := a_data.gTrue;
    l_ignoreRow.mss_data := a_data.gTrue;

    l_updateRow.mss_stack := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setStack;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTrace (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_trace%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mss_uss_id := a_data.gTrue;
    l_ignoreRow.mss_date := a_data.gTrue;
    l_ignoreRow.mss_type := a_data.gTrue;
    l_ignoreRow.mss_message := a_data.gTrue;
    l_ignoreRow.mss_stack := a_data.gTrue;
    l_ignoreRow.mss_trace := a_data.gFalse;
    l_ignoreRow.mss_task_id := a_data.gTrue;
    l_ignoreRow.mss_data := a_data.gTrue;

    l_updateRow.mss_trace := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTrace;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTaskId (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_task_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mss_uss_id := a_data.gTrue;
    l_ignoreRow.mss_date := a_data.gTrue;
    l_ignoreRow.mss_type := a_data.gTrue;
    l_ignoreRow.mss_message := a_data.gTrue;
    l_ignoreRow.mss_stack := a_data.gTrue;
    l_ignoreRow.mss_trace := a_data.gTrue;
    l_ignoreRow.mss_task_id := a_data.gFalse;
    l_ignoreRow.mss_data := a_data.gTrue;

    l_updateRow.mss_task_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTaskId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setData (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_data%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mss_uss_id := a_data.gTrue;
    l_ignoreRow.mss_date := a_data.gTrue;
    l_ignoreRow.mss_type := a_data.gTrue;
    l_ignoreRow.mss_message := a_data.gTrue;
    l_ignoreRow.mss_stack := a_data.gTrue;
    l_ignoreRow.mss_trace := a_data.gTrue;
    l_ignoreRow.mss_task_id := a_data.gTrue;
    l_ignoreRow.mss_data := a_data.gFalse;

    l_updateRow.mss_data := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setData;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN message_log_2.mss_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN message_log_2.mss_id%TYPE, 
                      p_uss_id IN message_log_2.mss_uss_id%TYPE,
                      p_date IN message_log_2.mss_date%TYPE,
                      p_type IN message_log_2.mss_type%TYPE,
                      p_message IN message_log_2.mss_message%TYPE,
                      p_stack IN message_log_2.mss_stack%TYPE,
                      p_trace IN message_log_2.mss_trace%TYPE,
                      p_task_id IN message_log_2.mss_task_id%TYPE,
                      p_data IN message_log_2.mss_data%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.mss_uss_id := p_uss_id;
    l_row.mss_date := p_date;
    l_row.mss_type := p_type;
    l_row.mss_message := p_message;
    l_row.mss_stack := p_stack;
    l_row.mss_trace := p_trace;
    l_row.mss_task_id := p_task_id;
    l_row.mss_data := p_data;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN message_log_2%ROWTYPE, p_newPK OUT message_log_2.mss_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN message_log_2.mss_id%TYPE,
                      p_uss_id IN message_log_2.mss_uss_id%TYPE,
                      p_date IN message_log_2.mss_date%TYPE,
                      p_type IN message_log_2.mss_type%TYPE,
                      p_message IN message_log_2.mss_message%TYPE,
                      p_stack IN message_log_2.mss_stack%TYPE,
                      p_trace IN message_log_2.mss_trace%TYPE,
                      p_task_id IN message_log_2.mss_task_id%TYPE,
                      p_data IN message_log_2.mss_data%TYPE, p_newPK OUT message_log_2.mss_id%TYPE) IS
    l_row   message_log_2%ROWTYPE;
  BEGIN
    l_row.mss_id := p_id;
    l_row.mss_uss_id := p_uss_id;
    l_row.mss_date := p_date;
    l_row.mss_type := p_type;
    l_row.mss_message := p_message;
    l_row.mss_stack := p_stack;
    l_row.mss_trace := p_trace;
    l_row.mss_task_id := p_task_id;
    l_row.mss_data := p_data;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN message_log_2.mss_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2%ROWTYPE IS
    l_row    message_log_2%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM message_log_2
       WHERE mss_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN message_log_2.mss_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE message_log_2
         SET 
             mss_uss_id = CASE NVL(p_ignore.mss_uss_id, a_data.gFalse) WHEN a_data.gTrue THEN mss_uss_id ELSE p_row.mss_uss_id END,
             mss_date = CASE NVL(p_ignore.mss_date, a_data.gFalse) WHEN a_data.gTrue THEN mss_date ELSE p_row.mss_date END,
             mss_type = CASE NVL(p_ignore.mss_type, a_data.gFalse) WHEN a_data.gTrue THEN mss_type ELSE p_row.mss_type END,
             mss_message = CASE NVL(p_ignore.mss_message, a_data.gFalse) WHEN a_data.gTrue THEN mss_message ELSE p_row.mss_message END,
             mss_stack = CASE NVL(p_ignore.mss_stack, a_data.gFalse) WHEN a_data.gTrue THEN mss_stack ELSE p_row.mss_stack END,
             mss_trace = CASE NVL(p_ignore.mss_trace, a_data.gFalse) WHEN a_data.gTrue THEN mss_trace ELSE p_row.mss_trace END,
             mss_task_id = CASE NVL(p_ignore.mss_task_id, a_data.gFalse) WHEN a_data.gTrue THEN mss_task_id ELSE p_row.mss_task_id END,
             mss_data = CASE NVL(p_ignore.mss_data, a_data.gFalse) WHEN a_data.gTrue THEN mss_data ELSE p_row.mss_data END
         WHERE mss_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN message_log_2.mss_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE message_log_2
       WHERE mss_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN message_log_2%ROWTYPE, p_newPk OUT message_log_2.mss_id%TYPE) IS
    l_pk    message_log_2.mss_id%TYPE;
  BEGIN 
    INSERT
      INTO message_log_2
    VALUES p_row
    RETURNING mss_id
         INTO p_newPk;
  END doInsert;

END t_MessageLog2;
/
