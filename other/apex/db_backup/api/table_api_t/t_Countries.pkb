CREATE OR REPLACE PACKAGE BODY t_Countries IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN countries.cty_id%TYPE, p_uk IN countries.cty_iso_2_code%TYPE DEFAULT NULL) RETURN countries%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN countries.cty_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN countries.cty_id%TYPE);

  PROCEDURE doInsert (p_row IN countries%ROWTYPE, p_newPk OUT countries.cty_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN countries.cty_id%TYPE) RETURN countries%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN countries.cty_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN countries.cty_id%TYPE) RETURN VARCHAR2 IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.cty_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN VARCHAR2 IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.cty_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_id%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cty_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_id%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.cty_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIso2Code_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_iso_2_code%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cty_iso_2_code;
  END getIso2Code_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIso2Code_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_iso_2_code%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.cty_iso_2_code;
  END getIso2Code_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIso3Code_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_iso_3_code%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cty_iso_3_code;
  END getIso3Code_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIso3Code_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_iso_3_code%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.cty_iso_3_code;
  END getIso3Code_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_name%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cty_name;
  END getName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_name%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.cty_name;
  END getName_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_enabled_ind%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cty_enabled_ind;
  END getEnabledInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_enabled_ind%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.cty_enabled_ind;
  END getEnabledInd_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAdfId_PK (p_pk IN countries.cty_id%TYPE) RETURN countries.cty_adf_id%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.cty_adf_id;
  END getAdfId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAdfId_UK (p_uk IN countries.cty_iso_2_code%TYPE) RETURN countries.cty_adf_id%TYPE IS
    l_row    countries%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.cty_adf_id;
  END getAdfId_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setIso2Code (p_pk IN countries.cty_id%TYPE, p_val IN countries.cty_iso_2_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.cty_iso_2_code := a_data.gFalse;
    l_ignoreRow.cty_iso_3_code := a_data.gTrue;
    l_ignoreRow.cty_name := a_data.gTrue;
    l_ignoreRow.cty_enabled_ind := a_data.gTrue;
    l_ignoreRow.cty_adf_id := a_data.gTrue;

    l_updateRow.cty_iso_2_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setIso2Code;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setIso3Code (p_pk IN countries.cty_id%TYPE, p_val IN countries.cty_iso_3_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.cty_iso_2_code := a_data.gTrue;
    l_ignoreRow.cty_iso_3_code := a_data.gFalse;
    l_ignoreRow.cty_name := a_data.gTrue;
    l_ignoreRow.cty_enabled_ind := a_data.gTrue;
    l_ignoreRow.cty_adf_id := a_data.gTrue;

    l_updateRow.cty_iso_3_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setIso3Code;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setName (p_pk IN countries.cty_id%TYPE, p_val IN countries.cty_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.cty_iso_2_code := a_data.gTrue;
    l_ignoreRow.cty_iso_3_code := a_data.gTrue;
    l_ignoreRow.cty_name := a_data.gFalse;
    l_ignoreRow.cty_enabled_ind := a_data.gTrue;
    l_ignoreRow.cty_adf_id := a_data.gTrue;

    l_updateRow.cty_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEnabledInd (p_pk IN countries.cty_id%TYPE, p_val IN countries.cty_enabled_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.cty_iso_2_code := a_data.gTrue;
    l_ignoreRow.cty_iso_3_code := a_data.gTrue;
    l_ignoreRow.cty_name := a_data.gTrue;
    l_ignoreRow.cty_enabled_ind := a_data.gFalse;
    l_ignoreRow.cty_adf_id := a_data.gTrue;

    l_updateRow.cty_enabled_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEnabledInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setAdfId (p_pk IN countries.cty_id%TYPE, p_val IN countries.cty_adf_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.cty_iso_2_code := a_data.gTrue;
    l_ignoreRow.cty_iso_3_code := a_data.gTrue;
    l_ignoreRow.cty_name := a_data.gTrue;
    l_ignoreRow.cty_enabled_ind := a_data.gTrue;
    l_ignoreRow.cty_adf_id := a_data.gFalse;

    l_updateRow.cty_adf_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setAdfId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN countries.cty_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN countries.cty_id%TYPE, 
                      p_iso_2_code IN countries.cty_iso_2_code%TYPE,
                      p_iso_3_code IN countries.cty_iso_3_code%TYPE,
                      p_name IN countries.cty_name%TYPE,
                      p_enabled_ind IN countries.cty_enabled_ind%TYPE,
                      p_adf_id IN countries.cty_adf_id%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.cty_iso_2_code := p_iso_2_code;
    l_row.cty_iso_3_code := p_iso_3_code;
    l_row.cty_name := p_name;
    l_row.cty_enabled_ind := p_enabled_ind;
    l_row.cty_adf_id := p_adf_id;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN countries%ROWTYPE, p_newPK OUT countries.cty_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN countries.cty_id%TYPE,
                      p_iso_2_code IN countries.cty_iso_2_code%TYPE,
                      p_iso_3_code IN countries.cty_iso_3_code%TYPE,
                      p_name IN countries.cty_name%TYPE,
                      p_enabled_ind IN countries.cty_enabled_ind%TYPE,
                      p_adf_id IN countries.cty_adf_id%TYPE, p_newPK OUT countries.cty_id%TYPE) IS
    l_row   countries%ROWTYPE;
  BEGIN
    l_row.cty_id := p_id;
    l_row.cty_iso_2_code := p_iso_2_code;
    l_row.cty_iso_3_code := p_iso_3_code;
    l_row.cty_name := p_name;
    l_row.cty_enabled_ind := p_enabled_ind;
    l_row.cty_adf_id := p_adf_id;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN countries.cty_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN countries.cty_id%TYPE, p_uk IN countries.cty_iso_2_code%TYPE DEFAULT NULL) RETURN countries%ROWTYPE IS
    l_row    countries%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM countries
       WHERE cty_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM countries
       WHERE cty_iso_2_code = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN countries.cty_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE countries
         SET 
             cty_iso_2_code = CASE NVL(p_ignore.cty_iso_2_code, a_data.gFalse) WHEN a_data.gTrue THEN cty_iso_2_code ELSE p_row.cty_iso_2_code END,
             cty_iso_3_code = CASE NVL(p_ignore.cty_iso_3_code, a_data.gFalse) WHEN a_data.gTrue THEN cty_iso_3_code ELSE p_row.cty_iso_3_code END,
             cty_name = CASE NVL(p_ignore.cty_name, a_data.gFalse) WHEN a_data.gTrue THEN cty_name ELSE p_row.cty_name END,
             cty_enabled_ind = CASE NVL(p_ignore.cty_enabled_ind, a_data.gFalse) WHEN a_data.gTrue THEN cty_enabled_ind ELSE p_row.cty_enabled_ind END,
             cty_adf_id = CASE NVL(p_ignore.cty_adf_id, a_data.gFalse) WHEN a_data.gTrue THEN cty_adf_id ELSE p_row.cty_adf_id END
         WHERE cty_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN countries.cty_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE countries
       WHERE cty_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN countries%ROWTYPE, p_newPk OUT countries.cty_id%TYPE) IS
    l_pk    countries.cty_id%TYPE;
  BEGIN 
    INSERT
      INTO countries
    VALUES p_row
    RETURNING cty_id
         INTO p_newPk;
  END doInsert;

END t_Countries;
/
