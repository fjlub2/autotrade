CREATE OR REPLACE PACKAGE t_MessageSubstitutes IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for message_substitutes table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF message_substitutes%ROWTYPE;

 /* Returns a row from the MESSAGE_SUBSTITUTES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN message_substitutes%ROWTYPE;

 /* Pipes a row from the MESSAGE_SUBSTITUTES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the MESSAGE_SUBSTITUTES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the MESSAGE_SUBSTITUTES table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN message_substitutes%ROWTYPE;

 /* Pipes a row from the MESSAGE_SUBSTITUTES table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the MESSAGE_SUBSTITUTES table for the supplied UK */
  FUNCTION exists_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN VARCHAR2;

 /* Returns MSU_ID from the MESSAGE_SUBSTITUTES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN message_substitutes.msu_id%TYPE;

 /* Returns MSU_ID from the MESSAGE_SUBSTITUTES table for the supplied UK */
  FUNCTION getId_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN message_substitutes.msu_id%TYPE;

 /* Returns MSU_MSC_ID from the MESSAGE_SUBSTITUTES table for the supplied PK */
  FUNCTION getMscId_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN message_substitutes.msu_msc_id%TYPE;

 /* Returns MSU_MSC_ID from the MESSAGE_SUBSTITUTES table for the supplied UK */
  FUNCTION getMscId_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN message_substitutes.msu_msc_id%TYPE;

 /* Returns MSU_CODE from the MESSAGE_SUBSTITUTES table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN message_substitutes.msu_code%TYPE;

 /* Returns MSU_CODE from the MESSAGE_SUBSTITUTES table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN message_substitutes.msu_code%TYPE;

 /* Returns MSU_NAME from the MESSAGE_SUBSTITUTES table for the supplied PK */
  FUNCTION getName_PK (p_pk IN message_substitutes.msu_id%TYPE) RETURN message_substitutes.msu_name%TYPE;

 /* Returns MSU_NAME from the MESSAGE_SUBSTITUTES table for the supplied UK */
  FUNCTION getName_UK (p_uk IN message_substitutes.msu_msc_id%TYPE, p_uk2 IN message_substitutes.msu_code%TYPE) RETURN message_substitutes.msu_name%TYPE;

END t_MessageSubstitutes;
/
