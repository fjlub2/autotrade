CREATE OR REPLACE PACKAGE t_Languages IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for languages table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF languages%ROWTYPE;

 /* Returns a row from the LANGUAGES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN languages.lng_id%TYPE) RETURN languages%ROWTYPE;

 /* Pipes a row from the LANGUAGES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN languages.lng_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the LANGUAGES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN languages.lng_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the LANGUAGES table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN languages.lng_code%TYPE) RETURN languages%ROWTYPE;

 /* Pipes a row from the LANGUAGES table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN languages.lng_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the LANGUAGES table for the supplied UK */
  FUNCTION exists_UK (p_uk IN languages.lng_code%TYPE) RETURN VARCHAR2;

 /* Returns LNG_ID from the LANGUAGES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN languages.lng_id%TYPE) RETURN languages.lng_id%TYPE;

 /* Returns LNG_ID from the LANGUAGES table for the supplied UK */
  FUNCTION getId_UK (p_uk IN languages.lng_code%TYPE) RETURN languages.lng_id%TYPE;

 /* Returns LNG_CODE from the LANGUAGES table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN languages.lng_id%TYPE) RETURN languages.lng_code%TYPE;

 /* Returns LNG_CODE from the LANGUAGES table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN languages.lng_code%TYPE) RETURN languages.lng_code%TYPE;

 /* Returns LNG_NAME from the LANGUAGES table for the supplied PK */
  FUNCTION getName_PK (p_pk IN languages.lng_id%TYPE) RETURN languages.lng_name%TYPE;

 /* Returns LNG_NAME from the LANGUAGES table for the supplied UK */
  FUNCTION getName_UK (p_uk IN languages.lng_code%TYPE) RETURN languages.lng_name%TYPE;

 /* Returns LNG_NATIVE_NAME from the LANGUAGES table for the supplied PK */
  FUNCTION getNativeName_PK (p_pk IN languages.lng_id%TYPE) RETURN languages.lng_native_name%TYPE;

 /* Returns LNG_NATIVE_NAME from the LANGUAGES table for the supplied UK */
  FUNCTION getNativeName_UK (p_uk IN languages.lng_code%TYPE) RETURN languages.lng_native_name%TYPE;

 /* Returns LNG_ENABLED_IND from the LANGUAGES table for the supplied PK */
  FUNCTION getEnabledInd_PK (p_pk IN languages.lng_id%TYPE) RETURN languages.lng_enabled_ind%TYPE;

 /* Returns LNG_ENABLED_IND from the LANGUAGES table for the supplied UK */
  FUNCTION getEnabledInd_UK (p_uk IN languages.lng_code%TYPE) RETURN languages.lng_enabled_ind%TYPE;

END t_Languages;
/
