CREATE OR REPLACE PACKAGE BODY t_UserAddresses IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_addresses.uad_id%TYPE, p_uk IN user_addresses.uad_usr_id%TYPE DEFAULT NULL, p_uk2 IN user_addresses.uad_address_type%TYPE DEFAULT NULL) RETURN user_addresses%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN user_addresses.uad_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN user_addresses.uad_id%TYPE);

  PROCEDURE doInsert (p_row IN user_addresses%ROWTYPE, p_newPk OUT user_addresses.uad_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN VARCHAR2 IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.uad_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk, p_uk2 => p_uk2);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk, p_uk2 => p_uk2));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN VARCHAR2 IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    IF l_row.uad_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_usr_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_usr_id;
  END getUsrId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_usr_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_usr_id;
  END getUsrId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine1_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_line_1%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_line_1;
  END getLine1_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine1_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_line_1%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_line_1;
  END getLine1_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine2_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_line_2%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_line_2;
  END getLine2_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine2_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_line_2%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_line_2;
  END getLine2_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine3_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_line_3%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_line_3;
  END getLine3_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine3_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_line_3%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_line_3;
  END getLine3_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine4_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_line_4%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_line_4;
  END getLine4_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine4_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_line_4%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_line_4;
  END getLine4_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine5_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_line_5%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_line_5;
  END getLine5_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine5_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_line_5%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_line_5;
  END getLine5_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPostCode_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_post_code%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_post_code;
  END getPostCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPostCode_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_post_code%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_post_code;
  END getPostCode_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCtyId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_cty_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_cty_id;
  END getCtyId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCtyId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_cty_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_cty_id;
  END getCtyId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRgnId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_rgn_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_rgn_id;
  END getRgnId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRgnId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_rgn_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_rgn_id;
  END getRgnId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTwnId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_twn_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_twn_id;
  END getTwnId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTwnId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_twn_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_twn_id;
  END getTwnId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAdfId_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_adf_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_adf_id;
  END getAdfId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAdfId_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_adf_id%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_adf_id;
  END getAdfId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAddressType_PK (p_pk IN user_addresses.uad_id%TYPE) RETURN user_addresses.uad_address_type%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uad_address_type;
  END getAddressType_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAddressType_UK (p_uk IN user_addresses.uad_usr_id%TYPE, p_uk2 IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_address_type%TYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uad_address_type;
  END getAddressType_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setUsrId (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_usr_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gFalse;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_usr_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsrId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine1 (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_line_1%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gFalse;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_line_1 := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine1;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine2 (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_line_2%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gFalse;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_line_2 := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine2;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine3 (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_line_3%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gFalse;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_line_3 := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine3;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine4 (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_line_4%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gFalse;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_line_4 := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine4;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine5 (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_line_5%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gFalse;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_line_5 := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine5;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setPostCode (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_post_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gFalse;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_post_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setPostCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCtyId (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_cty_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gFalse;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_cty_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCtyId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setRgnId (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_rgn_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gFalse;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_rgn_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setRgnId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTwnId (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_twn_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gFalse;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_twn_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTwnId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setAdfId (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_adf_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gFalse;
    l_ignoreRow.uad_address_type := a_data.gTrue;

    l_updateRow.uad_adf_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setAdfId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setAddressType (p_pk IN user_addresses.uad_id%TYPE, p_val IN user_addresses.uad_address_type%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uad_usr_id := a_data.gTrue;
    l_ignoreRow.uad_line_1 := a_data.gTrue;
    l_ignoreRow.uad_line_2 := a_data.gTrue;
    l_ignoreRow.uad_line_3 := a_data.gTrue;
    l_ignoreRow.uad_line_4 := a_data.gTrue;
    l_ignoreRow.uad_line_5 := a_data.gTrue;
    l_ignoreRow.uad_post_code := a_data.gTrue;
    l_ignoreRow.uad_cty_id := a_data.gTrue;
    l_ignoreRow.uad_rgn_id := a_data.gTrue;
    l_ignoreRow.uad_twn_id := a_data.gTrue;
    l_ignoreRow.uad_adf_id := a_data.gTrue;
    l_ignoreRow.uad_address_type := a_data.gFalse;

    l_updateRow.uad_address_type := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setAddressType;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_addresses.uad_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_addresses.uad_id%TYPE, 
                      p_usr_id IN user_addresses.uad_usr_id%TYPE,
                      p_line_1 IN user_addresses.uad_line_1%TYPE,
                      p_line_2 IN user_addresses.uad_line_2%TYPE,
                      p_line_3 IN user_addresses.uad_line_3%TYPE,
                      p_line_4 IN user_addresses.uad_line_4%TYPE,
                      p_line_5 IN user_addresses.uad_line_5%TYPE,
                      p_post_code IN user_addresses.uad_post_code%TYPE,
                      p_cty_id IN user_addresses.uad_cty_id%TYPE,
                      p_rgn_id IN user_addresses.uad_rgn_id%TYPE,
                      p_twn_id IN user_addresses.uad_twn_id%TYPE,
                      p_adf_id IN user_addresses.uad_adf_id%TYPE,
                      p_address_type IN user_addresses.uad_address_type%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.uad_usr_id := p_usr_id;
    l_row.uad_line_1 := p_line_1;
    l_row.uad_line_2 := p_line_2;
    l_row.uad_line_3 := p_line_3;
    l_row.uad_line_4 := p_line_4;
    l_row.uad_line_5 := p_line_5;
    l_row.uad_post_code := p_post_code;
    l_row.uad_cty_id := p_cty_id;
    l_row.uad_rgn_id := p_rgn_id;
    l_row.uad_twn_id := p_twn_id;
    l_row.uad_adf_id := p_adf_id;
    l_row.uad_address_type := p_address_type;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN user_addresses%ROWTYPE, p_newPK OUT user_addresses.uad_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN user_addresses.uad_id%TYPE,
                      p_usr_id IN user_addresses.uad_usr_id%TYPE,
                      p_line_1 IN user_addresses.uad_line_1%TYPE,
                      p_line_2 IN user_addresses.uad_line_2%TYPE,
                      p_line_3 IN user_addresses.uad_line_3%TYPE,
                      p_line_4 IN user_addresses.uad_line_4%TYPE,
                      p_line_5 IN user_addresses.uad_line_5%TYPE,
                      p_post_code IN user_addresses.uad_post_code%TYPE,
                      p_cty_id IN user_addresses.uad_cty_id%TYPE,
                      p_rgn_id IN user_addresses.uad_rgn_id%TYPE,
                      p_twn_id IN user_addresses.uad_twn_id%TYPE,
                      p_adf_id IN user_addresses.uad_adf_id%TYPE,
                      p_address_type IN user_addresses.uad_address_type%TYPE, p_newPK OUT user_addresses.uad_id%TYPE) IS
    l_row   user_addresses%ROWTYPE;
  BEGIN
    l_row.uad_id := p_id;
    l_row.uad_usr_id := p_usr_id;
    l_row.uad_line_1 := p_line_1;
    l_row.uad_line_2 := p_line_2;
    l_row.uad_line_3 := p_line_3;
    l_row.uad_line_4 := p_line_4;
    l_row.uad_line_5 := p_line_5;
    l_row.uad_post_code := p_post_code;
    l_row.uad_cty_id := p_cty_id;
    l_row.uad_rgn_id := p_rgn_id;
    l_row.uad_twn_id := p_twn_id;
    l_row.uad_adf_id := p_adf_id;
    l_row.uad_address_type := p_address_type;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN user_addresses.uad_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_addresses.uad_id%TYPE, p_uk IN user_addresses.uad_usr_id%TYPE DEFAULT NULL, p_uk2 IN user_addresses.uad_address_type%TYPE DEFAULT NULL) RETURN user_addresses%ROWTYPE IS
    l_row    user_addresses%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_addresses
       WHERE uad_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_addresses
       WHERE uad_usr_id = p_uk
         AND ((p_uk2 IS NOT NULL AND
               uad_address_type = p_uk2) OR
              (p_uk2 IS NULL AND
               uad_address_type IS NULL));
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN user_addresses.uad_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE user_addresses
         SET 
             uad_usr_id = CASE NVL(p_ignore.uad_usr_id, a_data.gFalse) WHEN a_data.gTrue THEN uad_usr_id ELSE p_row.uad_usr_id END,
             uad_line_1 = CASE NVL(p_ignore.uad_line_1, a_data.gFalse) WHEN a_data.gTrue THEN uad_line_1 ELSE p_row.uad_line_1 END,
             uad_line_2 = CASE NVL(p_ignore.uad_line_2, a_data.gFalse) WHEN a_data.gTrue THEN uad_line_2 ELSE p_row.uad_line_2 END,
             uad_line_3 = CASE NVL(p_ignore.uad_line_3, a_data.gFalse) WHEN a_data.gTrue THEN uad_line_3 ELSE p_row.uad_line_3 END,
             uad_line_4 = CASE NVL(p_ignore.uad_line_4, a_data.gFalse) WHEN a_data.gTrue THEN uad_line_4 ELSE p_row.uad_line_4 END,
             uad_line_5 = CASE NVL(p_ignore.uad_line_5, a_data.gFalse) WHEN a_data.gTrue THEN uad_line_5 ELSE p_row.uad_line_5 END,
             uad_post_code = CASE NVL(p_ignore.uad_post_code, a_data.gFalse) WHEN a_data.gTrue THEN uad_post_code ELSE p_row.uad_post_code END,
             uad_cty_id = CASE NVL(p_ignore.uad_cty_id, a_data.gFalse) WHEN a_data.gTrue THEN uad_cty_id ELSE p_row.uad_cty_id END,
             uad_rgn_id = CASE NVL(p_ignore.uad_rgn_id, a_data.gFalse) WHEN a_data.gTrue THEN uad_rgn_id ELSE p_row.uad_rgn_id END,
             uad_twn_id = CASE NVL(p_ignore.uad_twn_id, a_data.gFalse) WHEN a_data.gTrue THEN uad_twn_id ELSE p_row.uad_twn_id END,
             uad_adf_id = CASE NVL(p_ignore.uad_adf_id, a_data.gFalse) WHEN a_data.gTrue THEN uad_adf_id ELSE p_row.uad_adf_id END,
             uad_address_type = CASE NVL(p_ignore.uad_address_type, a_data.gFalse) WHEN a_data.gTrue THEN uad_address_type ELSE p_row.uad_address_type END
         WHERE uad_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN user_addresses.uad_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE user_addresses
       WHERE uad_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN user_addresses%ROWTYPE, p_newPk OUT user_addresses.uad_id%TYPE) IS
    l_pk    user_addresses.uad_id%TYPE;
  BEGIN 
    INSERT
      INTO user_addresses
    VALUES p_row
    RETURNING uad_id
         INTO p_newPk;
  END doInsert;

END t_UserAddresses;
/
