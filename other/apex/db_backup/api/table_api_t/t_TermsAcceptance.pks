CREATE OR REPLACE PACKAGE t_TermsAcceptance IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for terms_acceptance table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF terms_acceptance%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              tac_ter_id   terms_acceptance.tac_ter_id%TYPE,
                              tac_usr_id   terms_acceptance.tac_usr_id%TYPE,
                              tac_accepted_ind   terms_acceptance.tac_accepted_ind%TYPE,
                              tac_date   terms_acceptance.tac_date%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              tac_ter_id   VARCHAR2(1),
                              tac_usr_id   VARCHAR2(1),
                              tac_accepted_ind   VARCHAR2(1),
                              tac_date   VARCHAR2(1));

 /* Returns a row from the TERMS_ACCEPTANCE table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance%ROWTYPE;

 /* Pipes a row from the TERMS_ACCEPTANCE table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the TERMS_ACCEPTANCE table for the supplied PK */
  FUNCTION exists_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the TERMS_ACCEPTANCE table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance%ROWTYPE;

 /* Pipes a row from the TERMS_ACCEPTANCE table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the TERMS_ACCEPTANCE table for the supplied UK */
  FUNCTION exists_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN VARCHAR2;

 /* Returns TAC_ID from the TERMS_ACCEPTANCE table for the supplied PK */
  FUNCTION getId_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance.tac_id%TYPE;

 /* Returns TAC_ID from the TERMS_ACCEPTANCE table for the supplied UK */
  FUNCTION getId_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance.tac_id%TYPE;

 /* Returns TAC_TER_ID from the TERMS_ACCEPTANCE table for the supplied PK */
  FUNCTION getTerId_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance.tac_ter_id%TYPE;

 /* Returns TAC_TER_ID from the TERMS_ACCEPTANCE table for the supplied UK */
  FUNCTION getTerId_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance.tac_ter_id%TYPE;

 /* Returns TAC_USR_ID from the TERMS_ACCEPTANCE table for the supplied PK */
  FUNCTION getUsrId_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance.tac_usr_id%TYPE;

 /* Returns TAC_USR_ID from the TERMS_ACCEPTANCE table for the supplied UK */
  FUNCTION getUsrId_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance.tac_usr_id%TYPE;

 /* Returns TAC_ACCEPTED_IND from the TERMS_ACCEPTANCE table for the supplied PK */
  FUNCTION getAcceptedInd_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance.tac_accepted_ind%TYPE;

 /* Returns TAC_ACCEPTED_IND from the TERMS_ACCEPTANCE table for the supplied UK */
  FUNCTION getAcceptedInd_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance.tac_accepted_ind%TYPE;

 /* Returns TAC_DATE from the TERMS_ACCEPTANCE table for the supplied PK */
  FUNCTION getDate_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance.tac_date%TYPE;

 /* Returns TAC_DATE from the TERMS_ACCEPTANCE table for the supplied UK */
  FUNCTION getDate_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance.tac_date%TYPE;

 /* Updates TERMS_ACCEPTANCE.TAC_TER_ID to the supplied value for the supplied PK */
  PROCEDURE setTerId (p_pk IN terms_acceptance.tac_id%TYPE, p_val IN terms_acceptance.tac_ter_id%TYPE);

 /* Updates TERMS_ACCEPTANCE.TAC_USR_ID to the supplied value for the supplied PK */
  PROCEDURE setUsrId (p_pk IN terms_acceptance.tac_id%TYPE, p_val IN terms_acceptance.tac_usr_id%TYPE);

 /* Updates TERMS_ACCEPTANCE.TAC_ACCEPTED_IND to the supplied value for the supplied PK */
  PROCEDURE setAcceptedInd (p_pk IN terms_acceptance.tac_id%TYPE, p_val IN terms_acceptance.tac_accepted_ind%TYPE);

 /* Updates TERMS_ACCEPTANCE.TAC_DATE to the supplied value for the supplied PK */
  PROCEDURE setDate (p_pk IN terms_acceptance.tac_id%TYPE, p_val IN terms_acceptance.tac_date%TYPE);

 /* Updates a row on the TERMS_ACCEPTANCE table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN terms_acceptance.tac_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the TERMS_ACCEPTANCE table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN terms_acceptance.tac_id%TYPE, 
                      p_ter_id IN terms_acceptance.tac_ter_id%TYPE,
                      p_usr_id IN terms_acceptance.tac_usr_id%TYPE,
                      p_accepted_ind IN terms_acceptance.tac_accepted_ind%TYPE,
                      p_date IN terms_acceptance.tac_date%TYPE );

 /* Inserts a row into the TERMS_ACCEPTANCE table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN terms_acceptance%ROWTYPE, p_newPk OUT terms_acceptance.tac_id%TYPE);

 /* Inserts a row into the TERMS_ACCEPTANCE table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN terms_acceptance.tac_id%TYPE,
                      p_ter_id IN terms_acceptance.tac_ter_id%TYPE,
                      p_usr_id IN terms_acceptance.tac_usr_id%TYPE,
                      p_accepted_ind IN terms_acceptance.tac_accepted_ind%TYPE,
                      p_date IN terms_acceptance.tac_date%TYPE, p_newPk OUT terms_acceptance.tac_id%TYPE);

 /* Deletes a row from the TERMS_ACCEPTANCE table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN terms_acceptance.tac_id%TYPE);

END t_TermsAcceptance;
/
