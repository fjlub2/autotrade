CREATE OR REPLACE PACKAGE BODY t_Currencies IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN currencies.ccy_id%TYPE, p_uk IN currencies.ccy_code%TYPE DEFAULT NULL) RETURN currencies%ROWTYPE;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN currencies.ccy_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN currencies.ccy_id%TYPE) RETURN VARCHAR2 IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.ccy_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN currencies.ccy_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN currencies.ccy_code%TYPE) RETURN VARCHAR2 IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.ccy_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_id%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ccy_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_id%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ccy_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_code%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ccy_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_code%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ccy_code;
  END getCode_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSymbol_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_symbol%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ccy_symbol;
  END getSymbol_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSymbol_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_symbol%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ccy_symbol;
  END getSymbol_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDecimals_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_decimals%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ccy_decimals;
  END getDecimals_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDecimals_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_decimals%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ccy_decimals;
  END getDecimals_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_name%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ccy_name;
  END getName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_name%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ccy_name;
  END getName_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getFormat_PK (p_pk IN currencies.ccy_id%TYPE) RETURN currencies.ccy_format%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ccy_format;
  END getFormat_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getFormat_UK (p_uk IN currencies.ccy_code%TYPE) RETURN currencies.ccy_format%TYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ccy_format;
  END getFormat_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN currencies.ccy_id%TYPE, p_uk IN currencies.ccy_code%TYPE DEFAULT NULL) RETURN currencies%ROWTYPE IS
    l_row    currencies%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM currencies
       WHERE ccy_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM currencies
       WHERE ccy_code = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

END t_Currencies;
/
