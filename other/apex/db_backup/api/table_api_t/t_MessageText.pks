CREATE OR REPLACE PACKAGE t_MessageText IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for message_text table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF message_text%ROWTYPE;

 /* Returns a row from the MESSAGE_TEXT table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN message_text.mst_id%TYPE) RETURN message_text%ROWTYPE;

 /* Pipes a row from the MESSAGE_TEXT table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN message_text.mst_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the MESSAGE_TEXT table for the supplied PK */
  FUNCTION exists_PK (p_pk IN message_text.mst_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the MESSAGE_TEXT table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN message_text%ROWTYPE;

 /* Pipes a row from the MESSAGE_TEXT table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the MESSAGE_TEXT table for the supplied UK */
  FUNCTION exists_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN VARCHAR2;

 /* Returns MST_ID from the MESSAGE_TEXT table for the supplied PK */
  FUNCTION getId_PK (p_pk IN message_text.mst_id%TYPE) RETURN message_text.mst_id%TYPE;

 /* Returns MST_ID from the MESSAGE_TEXT table for the supplied UK */
  FUNCTION getId_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN message_text.mst_id%TYPE;

 /* Returns MST_MSC_ID from the MESSAGE_TEXT table for the supplied PK */
  FUNCTION getMscId_PK (p_pk IN message_text.mst_id%TYPE) RETURN message_text.mst_msc_id%TYPE;

 /* Returns MST_MSC_ID from the MESSAGE_TEXT table for the supplied UK */
  FUNCTION getMscId_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN message_text.mst_msc_id%TYPE;

 /* Returns MST_LNG_ID from the MESSAGE_TEXT table for the supplied PK */
  FUNCTION getLngId_PK (p_pk IN message_text.mst_id%TYPE) RETURN message_text.mst_lng_id%TYPE;

 /* Returns MST_LNG_ID from the MESSAGE_TEXT table for the supplied UK */
  FUNCTION getLngId_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN message_text.mst_lng_id%TYPE;

 /* Returns MST_TEXT from the MESSAGE_TEXT table for the supplied PK */
  FUNCTION getText_PK (p_pk IN message_text.mst_id%TYPE) RETURN message_text.mst_text%TYPE;

 /* Returns MST_TEXT from the MESSAGE_TEXT table for the supplied UK */
  FUNCTION getText_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN message_text.mst_text%TYPE;

END t_MessageText;
/
