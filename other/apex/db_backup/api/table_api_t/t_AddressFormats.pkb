CREATE OR REPLACE PACKAGE BODY t_AddressFormats IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN address_formats.adf_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN address_formats.adf_id%TYPE);

  PROCEDURE doInsert (p_row IN address_formats%ROWTYPE, p_newPk OUT address_formats.adf_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN address_formats.adf_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN address_formats.adf_id%TYPE) RETURN VARCHAR2 IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.adf_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_id%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCtyId_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_cty_id%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_cty_id;
  END getCtyId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDefaultInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_default_ind%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_default_ind;
  END getDefaultInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine1Name_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_1_name%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_line_1_name;
  END getLine1Name_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine2Name_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_2_name%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_line_2_name;
  END getLine2Name_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine3Name_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_3_name%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_line_3_name;
  END getLine3Name_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine4Name_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_4_name%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_line_4_name;
  END getLine4Name_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine5Name_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_5_name%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_line_5_name;
  END getLine5Name_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPostCodeName_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_post_code_name%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_post_code_name;
  END getPostCodeName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRegionName_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_region_name%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_region_name;
  END getRegionName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTownName_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_town_name%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_town_name;
  END getTownName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getFormat_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_format%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_format;
  END getFormat_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_name%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_name;
  END getName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_enabled_ind%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_enabled_ind;
  END getEnabledInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine1MandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_1_mand_ind%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_line_1_mand_ind;
  END getLine1MandInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine2MandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_2_mand_ind%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_line_2_mand_ind;
  END getLine2MandInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine3MandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_3_mand_ind%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_line_3_mand_ind;
  END getLine3MandInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine4MandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_4_mand_ind%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_line_4_mand_ind;
  END getLine4MandInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLine5MandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_5_mand_ind%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_line_5_mand_ind;
  END getLine5MandInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPostCodeMandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_post_code_mand_ind%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_post_code_mand_ind;
  END getPostCodeMandInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRegionMandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_region_mand_ind%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_region_mand_ind;
  END getRegionMandInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTownMandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_town_mand_ind%TYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.adf_town_mand_ind;
  END getTownMandInd_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setCtyId (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_cty_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gFalse;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_cty_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCtyId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDefaultInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_default_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gFalse;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_default_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDefaultInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine1Name (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_1_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gFalse;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_line_1_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine1Name;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine2Name (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_2_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gFalse;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_line_2_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine2Name;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine3Name (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_3_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gFalse;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_line_3_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine3Name;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine4Name (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_4_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gFalse;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_line_4_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine4Name;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine5Name (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_5_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gFalse;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_line_5_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine5Name;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setPostCodeName (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_post_code_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gFalse;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_post_code_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setPostCodeName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setRegionName (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_region_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gFalse;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_region_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setRegionName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTownName (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_town_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gFalse;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_town_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTownName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setFormat (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_format%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gFalse;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_format := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setFormat;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setName (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gFalse;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEnabledInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_enabled_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gFalse;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_enabled_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEnabledInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine1MandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_1_mand_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gFalse;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_line_1_mand_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine1MandInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine2MandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_2_mand_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gFalse;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_line_2_mand_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine2MandInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine3MandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_3_mand_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gFalse;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_line_3_mand_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine3MandInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine4MandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_4_mand_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gFalse;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_line_4_mand_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine4MandInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLine5MandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_5_mand_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gFalse;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_line_5_mand_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLine5MandInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setPostCodeMandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_post_code_mand_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gFalse;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_post_code_mand_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setPostCodeMandInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setRegionMandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_region_mand_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gFalse;
    l_ignoreRow.adf_town_mand_ind := a_data.gTrue;

    l_updateRow.adf_region_mand_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setRegionMandInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTownMandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_town_mand_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.adf_cty_id := a_data.gTrue;
    l_ignoreRow.adf_default_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_name := a_data.gTrue;
    l_ignoreRow.adf_line_2_name := a_data.gTrue;
    l_ignoreRow.adf_line_3_name := a_data.gTrue;
    l_ignoreRow.adf_line_4_name := a_data.gTrue;
    l_ignoreRow.adf_line_5_name := a_data.gTrue;
    l_ignoreRow.adf_post_code_name := a_data.gTrue;
    l_ignoreRow.adf_region_name := a_data.gTrue;
    l_ignoreRow.adf_town_name := a_data.gTrue;
    l_ignoreRow.adf_format := a_data.gTrue;
    l_ignoreRow.adf_name := a_data.gTrue;
    l_ignoreRow.adf_enabled_ind := a_data.gTrue;
    l_ignoreRow.adf_line_1_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_2_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_3_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_4_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_line_5_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_post_code_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_region_mand_ind := a_data.gTrue;
    l_ignoreRow.adf_town_mand_ind := a_data.gFalse;

    l_updateRow.adf_town_mand_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTownMandInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN address_formats.adf_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN address_formats.adf_id%TYPE, 
                      p_cty_id IN address_formats.adf_cty_id%TYPE,
                      p_default_ind IN address_formats.adf_default_ind%TYPE,
                      p_line_1_name IN address_formats.adf_line_1_name%TYPE,
                      p_line_2_name IN address_formats.adf_line_2_name%TYPE,
                      p_line_3_name IN address_formats.adf_line_3_name%TYPE,
                      p_line_4_name IN address_formats.adf_line_4_name%TYPE,
                      p_line_5_name IN address_formats.adf_line_5_name%TYPE,
                      p_post_code_name IN address_formats.adf_post_code_name%TYPE,
                      p_region_name IN address_formats.adf_region_name%TYPE,
                      p_town_name IN address_formats.adf_town_name%TYPE,
                      p_format IN address_formats.adf_format%TYPE,
                      p_name IN address_formats.adf_name%TYPE,
                      p_enabled_ind IN address_formats.adf_enabled_ind%TYPE,
                      p_line_1_mand_ind IN address_formats.adf_line_1_mand_ind%TYPE,
                      p_line_2_mand_ind IN address_formats.adf_line_2_mand_ind%TYPE,
                      p_line_3_mand_ind IN address_formats.adf_line_3_mand_ind%TYPE,
                      p_line_4_mand_ind IN address_formats.adf_line_4_mand_ind%TYPE,
                      p_line_5_mand_ind IN address_formats.adf_line_5_mand_ind%TYPE,
                      p_post_code_mand_ind IN address_formats.adf_post_code_mand_ind%TYPE,
                      p_region_mand_ind IN address_formats.adf_region_mand_ind%TYPE,
                      p_town_mand_ind IN address_formats.adf_town_mand_ind%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.adf_cty_id := p_cty_id;
    l_row.adf_default_ind := p_default_ind;
    l_row.adf_line_1_name := p_line_1_name;
    l_row.adf_line_2_name := p_line_2_name;
    l_row.adf_line_3_name := p_line_3_name;
    l_row.adf_line_4_name := p_line_4_name;
    l_row.adf_line_5_name := p_line_5_name;
    l_row.adf_post_code_name := p_post_code_name;
    l_row.adf_region_name := p_region_name;
    l_row.adf_town_name := p_town_name;
    l_row.adf_format := p_format;
    l_row.adf_name := p_name;
    l_row.adf_enabled_ind := p_enabled_ind;
    l_row.adf_line_1_mand_ind := p_line_1_mand_ind;
    l_row.adf_line_2_mand_ind := p_line_2_mand_ind;
    l_row.adf_line_3_mand_ind := p_line_3_mand_ind;
    l_row.adf_line_4_mand_ind := p_line_4_mand_ind;
    l_row.adf_line_5_mand_ind := p_line_5_mand_ind;
    l_row.adf_post_code_mand_ind := p_post_code_mand_ind;
    l_row.adf_region_mand_ind := p_region_mand_ind;
    l_row.adf_town_mand_ind := p_town_mand_ind;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN address_formats%ROWTYPE, p_newPK OUT address_formats.adf_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN address_formats.adf_id%TYPE,
                      p_cty_id IN address_formats.adf_cty_id%TYPE,
                      p_default_ind IN address_formats.adf_default_ind%TYPE,
                      p_line_1_name IN address_formats.adf_line_1_name%TYPE,
                      p_line_2_name IN address_formats.adf_line_2_name%TYPE,
                      p_line_3_name IN address_formats.adf_line_3_name%TYPE,
                      p_line_4_name IN address_formats.adf_line_4_name%TYPE,
                      p_line_5_name IN address_formats.adf_line_5_name%TYPE,
                      p_post_code_name IN address_formats.adf_post_code_name%TYPE,
                      p_region_name IN address_formats.adf_region_name%TYPE,
                      p_town_name IN address_formats.adf_town_name%TYPE,
                      p_format IN address_formats.adf_format%TYPE,
                      p_name IN address_formats.adf_name%TYPE,
                      p_enabled_ind IN address_formats.adf_enabled_ind%TYPE,
                      p_line_1_mand_ind IN address_formats.adf_line_1_mand_ind%TYPE,
                      p_line_2_mand_ind IN address_formats.adf_line_2_mand_ind%TYPE,
                      p_line_3_mand_ind IN address_formats.adf_line_3_mand_ind%TYPE,
                      p_line_4_mand_ind IN address_formats.adf_line_4_mand_ind%TYPE,
                      p_line_5_mand_ind IN address_formats.adf_line_5_mand_ind%TYPE,
                      p_post_code_mand_ind IN address_formats.adf_post_code_mand_ind%TYPE,
                      p_region_mand_ind IN address_formats.adf_region_mand_ind%TYPE,
                      p_town_mand_ind IN address_formats.adf_town_mand_ind%TYPE, p_newPK OUT address_formats.adf_id%TYPE) IS
    l_row   address_formats%ROWTYPE;
  BEGIN
    l_row.adf_id := p_id;
    l_row.adf_cty_id := p_cty_id;
    l_row.adf_default_ind := p_default_ind;
    l_row.adf_line_1_name := p_line_1_name;
    l_row.adf_line_2_name := p_line_2_name;
    l_row.adf_line_3_name := p_line_3_name;
    l_row.adf_line_4_name := p_line_4_name;
    l_row.adf_line_5_name := p_line_5_name;
    l_row.adf_post_code_name := p_post_code_name;
    l_row.adf_region_name := p_region_name;
    l_row.adf_town_name := p_town_name;
    l_row.adf_format := p_format;
    l_row.adf_name := p_name;
    l_row.adf_enabled_ind := p_enabled_ind;
    l_row.adf_line_1_mand_ind := p_line_1_mand_ind;
    l_row.adf_line_2_mand_ind := p_line_2_mand_ind;
    l_row.adf_line_3_mand_ind := p_line_3_mand_ind;
    l_row.adf_line_4_mand_ind := p_line_4_mand_ind;
    l_row.adf_line_5_mand_ind := p_line_5_mand_ind;
    l_row.adf_post_code_mand_ind := p_post_code_mand_ind;
    l_row.adf_region_mand_ind := p_region_mand_ind;
    l_row.adf_town_mand_ind := p_town_mand_ind;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN address_formats.adf_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats%ROWTYPE IS
    l_row    address_formats%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM address_formats
       WHERE adf_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN address_formats.adf_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE address_formats
         SET 
             adf_cty_id = CASE NVL(p_ignore.adf_cty_id, a_data.gFalse) WHEN a_data.gTrue THEN adf_cty_id ELSE p_row.adf_cty_id END,
             adf_default_ind = CASE NVL(p_ignore.adf_default_ind, a_data.gFalse) WHEN a_data.gTrue THEN adf_default_ind ELSE p_row.adf_default_ind END,
             adf_line_1_name = CASE NVL(p_ignore.adf_line_1_name, a_data.gFalse) WHEN a_data.gTrue THEN adf_line_1_name ELSE p_row.adf_line_1_name END,
             adf_line_2_name = CASE NVL(p_ignore.adf_line_2_name, a_data.gFalse) WHEN a_data.gTrue THEN adf_line_2_name ELSE p_row.adf_line_2_name END,
             adf_line_3_name = CASE NVL(p_ignore.adf_line_3_name, a_data.gFalse) WHEN a_data.gTrue THEN adf_line_3_name ELSE p_row.adf_line_3_name END,
             adf_line_4_name = CASE NVL(p_ignore.adf_line_4_name, a_data.gFalse) WHEN a_data.gTrue THEN adf_line_4_name ELSE p_row.adf_line_4_name END,
             adf_line_5_name = CASE NVL(p_ignore.adf_line_5_name, a_data.gFalse) WHEN a_data.gTrue THEN adf_line_5_name ELSE p_row.adf_line_5_name END,
             adf_post_code_name = CASE NVL(p_ignore.adf_post_code_name, a_data.gFalse) WHEN a_data.gTrue THEN adf_post_code_name ELSE p_row.adf_post_code_name END,
             adf_region_name = CASE NVL(p_ignore.adf_region_name, a_data.gFalse) WHEN a_data.gTrue THEN adf_region_name ELSE p_row.adf_region_name END,
             adf_town_name = CASE NVL(p_ignore.adf_town_name, a_data.gFalse) WHEN a_data.gTrue THEN adf_town_name ELSE p_row.adf_town_name END,
             adf_format = CASE NVL(p_ignore.adf_format, a_data.gFalse) WHEN a_data.gTrue THEN adf_format ELSE p_row.adf_format END,
             adf_name = CASE NVL(p_ignore.adf_name, a_data.gFalse) WHEN a_data.gTrue THEN adf_name ELSE p_row.adf_name END,
             adf_enabled_ind = CASE NVL(p_ignore.adf_enabled_ind, a_data.gFalse) WHEN a_data.gTrue THEN adf_enabled_ind ELSE p_row.adf_enabled_ind END,
             adf_line_1_mand_ind = CASE NVL(p_ignore.adf_line_1_mand_ind, a_data.gFalse) WHEN a_data.gTrue THEN adf_line_1_mand_ind ELSE p_row.adf_line_1_mand_ind END,
             adf_line_2_mand_ind = CASE NVL(p_ignore.adf_line_2_mand_ind, a_data.gFalse) WHEN a_data.gTrue THEN adf_line_2_mand_ind ELSE p_row.adf_line_2_mand_ind END,
             adf_line_3_mand_ind = CASE NVL(p_ignore.adf_line_3_mand_ind, a_data.gFalse) WHEN a_data.gTrue THEN adf_line_3_mand_ind ELSE p_row.adf_line_3_mand_ind END,
             adf_line_4_mand_ind = CASE NVL(p_ignore.adf_line_4_mand_ind, a_data.gFalse) WHEN a_data.gTrue THEN adf_line_4_mand_ind ELSE p_row.adf_line_4_mand_ind END,
             adf_line_5_mand_ind = CASE NVL(p_ignore.adf_line_5_mand_ind, a_data.gFalse) WHEN a_data.gTrue THEN adf_line_5_mand_ind ELSE p_row.adf_line_5_mand_ind END,
             adf_post_code_mand_ind = CASE NVL(p_ignore.adf_post_code_mand_ind, a_data.gFalse) WHEN a_data.gTrue THEN adf_post_code_mand_ind ELSE p_row.adf_post_code_mand_ind END,
             adf_region_mand_ind = CASE NVL(p_ignore.adf_region_mand_ind, a_data.gFalse) WHEN a_data.gTrue THEN adf_region_mand_ind ELSE p_row.adf_region_mand_ind END,
             adf_town_mand_ind = CASE NVL(p_ignore.adf_town_mand_ind, a_data.gFalse) WHEN a_data.gTrue THEN adf_town_mand_ind ELSE p_row.adf_town_mand_ind END
         WHERE adf_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN address_formats.adf_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE address_formats
       WHERE adf_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN address_formats%ROWTYPE, p_newPk OUT address_formats.adf_id%TYPE) IS
    l_pk    address_formats.adf_id%TYPE;
  BEGIN 
    INSERT
      INTO address_formats
    VALUES p_row
    RETURNING adf_id
         INTO p_newPk;
  END doInsert;

END t_AddressFormats;
/
