CREATE OR REPLACE PACKAGE t_PaymentPlans IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for payment_plans table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF payment_plans%ROWTYPE;

 /* Returns a row from the PAYMENT_PLANS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans%ROWTYPE;

 /* Pipes a row from the PAYMENT_PLANS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the PAYMENT_PLANS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN VARCHAR2;

 /* Returns PPL_ID from the PAYMENT_PLANS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_id%TYPE;

 /* Returns PPL_DESC from the PAYMENT_PLANS table for the supplied PK */
  FUNCTION getDesc_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_desc%TYPE;

 /* Returns PPL_AMOUNT from the PAYMENT_PLANS table for the supplied PK */
  FUNCTION getAmount_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_amount%TYPE;

 /* Returns PPL_FREQUENCY from the PAYMENT_PLANS table for the supplied PK */
  FUNCTION getFrequency_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_frequency%TYPE;

 /* Returns PPL_ENABLED_IND from the PAYMENT_PLANS table for the supplied PK */
  FUNCTION getEnabledInd_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_enabled_ind%TYPE;

 /* Returns PPL_MIN_TERM from the PAYMENT_PLANS table for the supplied PK */
  FUNCTION getMinTerm_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_min_term%TYPE;

 /* Returns PPL_CCY_ID from the PAYMENT_PLANS table for the supplied PK */
  FUNCTION getCcyId_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_ccy_id%TYPE;

 /* Returns PPL_DISPLAY_ORDER from the PAYMENT_PLANS table for the supplied PK */
  FUNCTION getDisplayOrder_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_display_order%TYPE;

END t_PaymentPlans;
/
