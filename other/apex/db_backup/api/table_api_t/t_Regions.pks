CREATE OR REPLACE PACKAGE t_Regions IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for regions table

   Should be applied on the CORE schema. No grants or synonyms should be applied to this package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF regions%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              rgn_cty_id   regions.rgn_cty_id%TYPE,
                              rgn_code   regions.rgn_code%TYPE,
                              rgn_iso_code   regions.rgn_iso_code%TYPE,
                              rgn_name   regions.rgn_name%TYPE,
                              rgn_enabled_ind   regions.rgn_enabled_ind%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              rgn_cty_id   VARCHAR2(1),
                              rgn_code   VARCHAR2(1),
                              rgn_iso_code   VARCHAR2(1),
                              rgn_name   VARCHAR2(1),
                              rgn_enabled_ind   VARCHAR2(1));

 /* Returns a row from the REGIONS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions%ROWTYPE;

 /* Pipes a row from the REGIONS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN regions.rgn_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the REGIONS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN regions.rgn_id%TYPE) RETURN VARCHAR2;

 /* Returns RGN_ID from the REGIONS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_id%TYPE;

 /* Returns RGN_CTY_ID from the REGIONS table for the supplied PK */
  FUNCTION getCtyId_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_cty_id%TYPE;

 /* Returns RGN_CODE from the REGIONS table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_code%TYPE;

 /* Returns RGN_ISO_CODE from the REGIONS table for the supplied PK */
  FUNCTION getIsoCode_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_iso_code%TYPE;

 /* Returns RGN_NAME from the REGIONS table for the supplied PK */
  FUNCTION getName_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_name%TYPE;

 /* Returns RGN_ENABLED_IND from the REGIONS table for the supplied PK */
  FUNCTION getEnabledInd_PK (p_pk IN regions.rgn_id%TYPE) RETURN regions.rgn_enabled_ind%TYPE;

 /* Updates REGIONS.RGN_CTY_ID to the supplied value for the supplied PK */
  PROCEDURE setCtyId (p_pk IN regions.rgn_id%TYPE, p_val IN regions.rgn_cty_id%TYPE);

 /* Updates REGIONS.RGN_CODE to the supplied value for the supplied PK */
  PROCEDURE setCode (p_pk IN regions.rgn_id%TYPE, p_val IN regions.rgn_code%TYPE);

 /* Updates REGIONS.RGN_ISO_CODE to the supplied value for the supplied PK */
  PROCEDURE setIsoCode (p_pk IN regions.rgn_id%TYPE, p_val IN regions.rgn_iso_code%TYPE);

 /* Updates REGIONS.RGN_NAME to the supplied value for the supplied PK */
  PROCEDURE setName (p_pk IN regions.rgn_id%TYPE, p_val IN regions.rgn_name%TYPE);

 /* Updates REGIONS.RGN_ENABLED_IND to the supplied value for the supplied PK */
  PROCEDURE setEnabledInd (p_pk IN regions.rgn_id%TYPE, p_val IN regions.rgn_enabled_ind%TYPE);

 /* Updates a row on the REGIONS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN regions.rgn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the REGIONS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN regions.rgn_id%TYPE, 
                      p_cty_id IN regions.rgn_cty_id%TYPE,
                      p_code IN regions.rgn_code%TYPE,
                      p_iso_code IN regions.rgn_iso_code%TYPE,
                      p_name IN regions.rgn_name%TYPE,
                      p_enabled_ind IN regions.rgn_enabled_ind%TYPE );

 /* Inserts a row into the REGIONS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN regions%ROWTYPE, p_newPk OUT regions.rgn_id%TYPE);

 /* Inserts a row into the REGIONS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN regions.rgn_id%TYPE,
                      p_cty_id IN regions.rgn_cty_id%TYPE,
                      p_code IN regions.rgn_code%TYPE,
                      p_iso_code IN regions.rgn_iso_code%TYPE,
                      p_name IN regions.rgn_name%TYPE,
                      p_enabled_ind IN regions.rgn_enabled_ind%TYPE, p_newPk OUT regions.rgn_id%TYPE);

 /* Deletes a row from the REGIONS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN regions.rgn_id%TYPE);

END t_Regions;
/
