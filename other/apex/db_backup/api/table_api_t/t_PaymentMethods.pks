CREATE OR REPLACE PACKAGE t_PaymentMethods IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for payment_methods table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF payment_methods%ROWTYPE;

 /* Returns a row from the PAYMENT_METHODS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods%ROWTYPE;

 /* Pipes a row from the PAYMENT_METHODS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the PAYMENT_METHODS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the PAYMENT_METHODS table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods%ROWTYPE;

 /* Pipes a row from the PAYMENT_METHODS table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the PAYMENT_METHODS table for the supplied UK */
  FUNCTION exists_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN VARCHAR2;

 /* Returns PYM_ID from the PAYMENT_METHODS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_id%TYPE;

 /* Returns PYM_ID from the PAYMENT_METHODS table for the supplied UK */
  FUNCTION getId_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_id%TYPE;

 /* Returns PYM_CODE from the PAYMENT_METHODS table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_code%TYPE;

 /* Returns PYM_CODE from the PAYMENT_METHODS table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_code%TYPE;

 /* Returns PYM_NAME from the PAYMENT_METHODS table for the supplied PK */
  FUNCTION getName_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_name%TYPE;

 /* Returns PYM_NAME from the PAYMENT_METHODS table for the supplied UK */
  FUNCTION getName_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_name%TYPE;

 /* Returns PYM_ENABLED_IND from the PAYMENT_METHODS table for the supplied PK */
  FUNCTION getEnabledInd_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_enabled_ind%TYPE;

 /* Returns PYM_ENABLED_IND from the PAYMENT_METHODS table for the supplied UK */
  FUNCTION getEnabledInd_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_enabled_ind%TYPE;

 /* Returns PYM_ICON from the PAYMENT_METHODS table for the supplied PK */
  FUNCTION getIcon_PK (p_pk IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_icon%TYPE;

 /* Returns PYM_ICON from the PAYMENT_METHODS table for the supplied UK */
  FUNCTION getIcon_UK (p_uk IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_icon%TYPE;

END t_PaymentMethods;
/
