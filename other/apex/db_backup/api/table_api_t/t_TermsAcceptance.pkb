CREATE OR REPLACE PACKAGE BODY t_TermsAcceptance IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN terms_acceptance.tac_id%TYPE, p_uk IN terms_acceptance.tac_ter_id%TYPE DEFAULT NULL, p_uk2 IN terms_acceptance.tac_usr_id%TYPE DEFAULT NULL) RETURN terms_acceptance%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN terms_acceptance.tac_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN terms_acceptance.tac_id%TYPE);

  PROCEDURE doInsert (p_row IN terms_acceptance%ROWTYPE, p_newPk OUT terms_acceptance.tac_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN VARCHAR2 IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.tac_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk, p_uk2 => p_uk2);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk, p_uk2 => p_uk2));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN VARCHAR2 IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    IF l_row.tac_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance.tac_id%TYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tac_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance.tac_id%TYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.tac_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTerId_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance.tac_ter_id%TYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tac_ter_id;
  END getTerId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTerId_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance.tac_ter_id%TYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.tac_ter_id;
  END getTerId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance.tac_usr_id%TYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tac_usr_id;
  END getUsrId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance.tac_usr_id%TYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.tac_usr_id;
  END getUsrId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAcceptedInd_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance.tac_accepted_ind%TYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tac_accepted_ind;
  END getAcceptedInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAcceptedInd_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance.tac_accepted_ind%TYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.tac_accepted_ind;
  END getAcceptedInd_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDate_PK (p_pk IN terms_acceptance.tac_id%TYPE) RETURN terms_acceptance.tac_date%TYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tac_date;
  END getDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDate_UK (p_uk IN terms_acceptance.tac_ter_id%TYPE, p_uk2 IN terms_acceptance.tac_usr_id%TYPE) RETURN terms_acceptance.tac_date%TYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.tac_date;
  END getDate_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setTerId (p_pk IN terms_acceptance.tac_id%TYPE, p_val IN terms_acceptance.tac_ter_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tac_ter_id := a_data.gFalse;
    l_ignoreRow.tac_usr_id := a_data.gTrue;
    l_ignoreRow.tac_accepted_ind := a_data.gTrue;
    l_ignoreRow.tac_date := a_data.gTrue;

    l_updateRow.tac_ter_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTerId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUsrId (p_pk IN terms_acceptance.tac_id%TYPE, p_val IN terms_acceptance.tac_usr_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tac_ter_id := a_data.gTrue;
    l_ignoreRow.tac_usr_id := a_data.gFalse;
    l_ignoreRow.tac_accepted_ind := a_data.gTrue;
    l_ignoreRow.tac_date := a_data.gTrue;

    l_updateRow.tac_usr_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsrId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setAcceptedInd (p_pk IN terms_acceptance.tac_id%TYPE, p_val IN terms_acceptance.tac_accepted_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tac_ter_id := a_data.gTrue;
    l_ignoreRow.tac_usr_id := a_data.gTrue;
    l_ignoreRow.tac_accepted_ind := a_data.gFalse;
    l_ignoreRow.tac_date := a_data.gTrue;

    l_updateRow.tac_accepted_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setAcceptedInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDate (p_pk IN terms_acceptance.tac_id%TYPE, p_val IN terms_acceptance.tac_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tac_ter_id := a_data.gTrue;
    l_ignoreRow.tac_usr_id := a_data.gTrue;
    l_ignoreRow.tac_accepted_ind := a_data.gTrue;
    l_ignoreRow.tac_date := a_data.gFalse;

    l_updateRow.tac_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN terms_acceptance.tac_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN terms_acceptance.tac_id%TYPE, 
                      p_ter_id IN terms_acceptance.tac_ter_id%TYPE,
                      p_usr_id IN terms_acceptance.tac_usr_id%TYPE,
                      p_accepted_ind IN terms_acceptance.tac_accepted_ind%TYPE,
                      p_date IN terms_acceptance.tac_date%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.tac_ter_id := p_ter_id;
    l_row.tac_usr_id := p_usr_id;
    l_row.tac_accepted_ind := p_accepted_ind;
    l_row.tac_date := p_date;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN terms_acceptance%ROWTYPE, p_newPK OUT terms_acceptance.tac_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN terms_acceptance.tac_id%TYPE,
                      p_ter_id IN terms_acceptance.tac_ter_id%TYPE,
                      p_usr_id IN terms_acceptance.tac_usr_id%TYPE,
                      p_accepted_ind IN terms_acceptance.tac_accepted_ind%TYPE,
                      p_date IN terms_acceptance.tac_date%TYPE, p_newPK OUT terms_acceptance.tac_id%TYPE) IS
    l_row   terms_acceptance%ROWTYPE;
  BEGIN
    l_row.tac_id := p_id;
    l_row.tac_ter_id := p_ter_id;
    l_row.tac_usr_id := p_usr_id;
    l_row.tac_accepted_ind := p_accepted_ind;
    l_row.tac_date := p_date;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN terms_acceptance.tac_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN terms_acceptance.tac_id%TYPE, p_uk IN terms_acceptance.tac_ter_id%TYPE DEFAULT NULL, p_uk2 IN terms_acceptance.tac_usr_id%TYPE DEFAULT NULL) RETURN terms_acceptance%ROWTYPE IS
    l_row    terms_acceptance%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM terms_acceptance
       WHERE tac_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM terms_acceptance
       WHERE tac_ter_id = p_uk
         AND ((p_uk2 IS NOT NULL AND
               tac_usr_id = p_uk2) OR
              (p_uk2 IS NULL AND
               tac_usr_id IS NULL));
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN terms_acceptance.tac_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE terms_acceptance
         SET 
             tac_ter_id = CASE NVL(p_ignore.tac_ter_id, a_data.gFalse) WHEN a_data.gTrue THEN tac_ter_id ELSE p_row.tac_ter_id END,
             tac_usr_id = CASE NVL(p_ignore.tac_usr_id, a_data.gFalse) WHEN a_data.gTrue THEN tac_usr_id ELSE p_row.tac_usr_id END,
             tac_accepted_ind = CASE NVL(p_ignore.tac_accepted_ind, a_data.gFalse) WHEN a_data.gTrue THEN tac_accepted_ind ELSE p_row.tac_accepted_ind END,
             tac_date = CASE NVL(p_ignore.tac_date, a_data.gFalse) WHEN a_data.gTrue THEN tac_date ELSE p_row.tac_date END
         WHERE tac_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN terms_acceptance.tac_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE terms_acceptance
       WHERE tac_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN terms_acceptance%ROWTYPE, p_newPk OUT terms_acceptance.tac_id%TYPE) IS
    l_pk    terms_acceptance.tac_id%TYPE;
  BEGIN 
    INSERT
      INTO terms_acceptance
    VALUES p_row
    RETURNING tac_id
         INTO p_newPk;
  END doInsert;

END t_TermsAcceptance;
/
