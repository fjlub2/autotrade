CREATE OR REPLACE PACKAGE t_AddressFormats IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for address_formats table

   Should be applied on the CORE schema. No grants or synonyms should be applied to this package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF address_formats%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              adf_cty_id   address_formats.adf_cty_id%TYPE,
                              adf_default_ind   address_formats.adf_default_ind%TYPE,
                              adf_line_1_name   address_formats.adf_line_1_name%TYPE,
                              adf_line_2_name   address_formats.adf_line_2_name%TYPE,
                              adf_line_3_name   address_formats.adf_line_3_name%TYPE,
                              adf_line_4_name   address_formats.adf_line_4_name%TYPE,
                              adf_line_5_name   address_formats.adf_line_5_name%TYPE,
                              adf_post_code_name   address_formats.adf_post_code_name%TYPE,
                              adf_region_name   address_formats.adf_region_name%TYPE,
                              adf_town_name   address_formats.adf_town_name%TYPE,
                              adf_format   address_formats.adf_format%TYPE,
                              adf_name   address_formats.adf_name%TYPE,
                              adf_enabled_ind   address_formats.adf_enabled_ind%TYPE,
                              adf_line_1_mand_ind   address_formats.adf_line_1_mand_ind%TYPE,
                              adf_line_2_mand_ind   address_formats.adf_line_2_mand_ind%TYPE,
                              adf_line_3_mand_ind   address_formats.adf_line_3_mand_ind%TYPE,
                              adf_line_4_mand_ind   address_formats.adf_line_4_mand_ind%TYPE,
                              adf_line_5_mand_ind   address_formats.adf_line_5_mand_ind%TYPE,
                              adf_post_code_mand_ind   address_formats.adf_post_code_mand_ind%TYPE,
                              adf_region_mand_ind   address_formats.adf_region_mand_ind%TYPE,
                              adf_town_mand_ind   address_formats.adf_town_mand_ind%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              adf_cty_id   VARCHAR2(1),
                              adf_default_ind   VARCHAR2(1),
                              adf_line_1_name   VARCHAR2(1),
                              adf_line_2_name   VARCHAR2(1),
                              adf_line_3_name   VARCHAR2(1),
                              adf_line_4_name   VARCHAR2(1),
                              adf_line_5_name   VARCHAR2(1),
                              adf_post_code_name   VARCHAR2(1),
                              adf_region_name   VARCHAR2(1),
                              adf_town_name   VARCHAR2(1),
                              adf_format   VARCHAR2(1),
                              adf_name   VARCHAR2(1),
                              adf_enabled_ind   VARCHAR2(1),
                              adf_line_1_mand_ind   VARCHAR2(1),
                              adf_line_2_mand_ind   VARCHAR2(1),
                              adf_line_3_mand_ind   VARCHAR2(1),
                              adf_line_4_mand_ind   VARCHAR2(1),
                              adf_line_5_mand_ind   VARCHAR2(1),
                              adf_post_code_mand_ind   VARCHAR2(1),
                              adf_region_mand_ind   VARCHAR2(1),
                              adf_town_mand_ind   VARCHAR2(1));

 /* Returns a row from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats%ROWTYPE;

 /* Pipes a row from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN address_formats.adf_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN address_formats.adf_id%TYPE) RETURN VARCHAR2;

 /* Returns ADF_ID from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_id%TYPE;

 /* Returns ADF_CTY_ID from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getCtyId_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_cty_id%TYPE;

 /* Returns ADF_DEFAULT_IND from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getDefaultInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_default_ind%TYPE;

 /* Returns ADF_LINE_1_NAME from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getLine1Name_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_1_name%TYPE;

 /* Returns ADF_LINE_2_NAME from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getLine2Name_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_2_name%TYPE;

 /* Returns ADF_LINE_3_NAME from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getLine3Name_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_3_name%TYPE;

 /* Returns ADF_LINE_4_NAME from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getLine4Name_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_4_name%TYPE;

 /* Returns ADF_LINE_5_NAME from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getLine5Name_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_5_name%TYPE;

 /* Returns ADF_POST_CODE_NAME from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getPostCodeName_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_post_code_name%TYPE;

 /* Returns ADF_REGION_NAME from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getRegionName_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_region_name%TYPE;

 /* Returns ADF_TOWN_NAME from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getTownName_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_town_name%TYPE;

 /* Returns ADF_FORMAT from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getFormat_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_format%TYPE;

 /* Returns ADF_NAME from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getName_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_name%TYPE;

 /* Returns ADF_ENABLED_IND from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getEnabledInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_enabled_ind%TYPE;

 /* Returns ADF_LINE_1_MAND_IND from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getLine1MandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_1_mand_ind%TYPE;

 /* Returns ADF_LINE_2_MAND_IND from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getLine2MandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_2_mand_ind%TYPE;

 /* Returns ADF_LINE_3_MAND_IND from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getLine3MandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_3_mand_ind%TYPE;

 /* Returns ADF_LINE_4_MAND_IND from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getLine4MandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_4_mand_ind%TYPE;

 /* Returns ADF_LINE_5_MAND_IND from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getLine5MandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_line_5_mand_ind%TYPE;

 /* Returns ADF_POST_CODE_MAND_IND from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getPostCodeMandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_post_code_mand_ind%TYPE;

 /* Returns ADF_REGION_MAND_IND from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getRegionMandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_region_mand_ind%TYPE;

 /* Returns ADF_TOWN_MAND_IND from the ADDRESS_FORMATS table for the supplied PK */
  FUNCTION getTownMandInd_PK (p_pk IN address_formats.adf_id%TYPE) RETURN address_formats.adf_town_mand_ind%TYPE;

 /* Updates ADDRESS_FORMATS.ADF_CTY_ID to the supplied value for the supplied PK */
  PROCEDURE setCtyId (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_cty_id%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_DEFAULT_IND to the supplied value for the supplied PK */
  PROCEDURE setDefaultInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_default_ind%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_LINE_1_NAME to the supplied value for the supplied PK */
  PROCEDURE setLine1Name (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_1_name%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_LINE_2_NAME to the supplied value for the supplied PK */
  PROCEDURE setLine2Name (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_2_name%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_LINE_3_NAME to the supplied value for the supplied PK */
  PROCEDURE setLine3Name (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_3_name%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_LINE_4_NAME to the supplied value for the supplied PK */
  PROCEDURE setLine4Name (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_4_name%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_LINE_5_NAME to the supplied value for the supplied PK */
  PROCEDURE setLine5Name (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_5_name%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_POST_CODE_NAME to the supplied value for the supplied PK */
  PROCEDURE setPostCodeName (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_post_code_name%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_REGION_NAME to the supplied value for the supplied PK */
  PROCEDURE setRegionName (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_region_name%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_TOWN_NAME to the supplied value for the supplied PK */
  PROCEDURE setTownName (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_town_name%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_FORMAT to the supplied value for the supplied PK */
  PROCEDURE setFormat (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_format%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_NAME to the supplied value for the supplied PK */
  PROCEDURE setName (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_name%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_ENABLED_IND to the supplied value for the supplied PK */
  PROCEDURE setEnabledInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_enabled_ind%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_LINE_1_MAND_IND to the supplied value for the supplied PK */
  PROCEDURE setLine1MandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_1_mand_ind%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_LINE_2_MAND_IND to the supplied value for the supplied PK */
  PROCEDURE setLine2MandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_2_mand_ind%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_LINE_3_MAND_IND to the supplied value for the supplied PK */
  PROCEDURE setLine3MandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_3_mand_ind%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_LINE_4_MAND_IND to the supplied value for the supplied PK */
  PROCEDURE setLine4MandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_4_mand_ind%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_LINE_5_MAND_IND to the supplied value for the supplied PK */
  PROCEDURE setLine5MandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_line_5_mand_ind%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_POST_CODE_MAND_IND to the supplied value for the supplied PK */
  PROCEDURE setPostCodeMandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_post_code_mand_ind%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_REGION_MAND_IND to the supplied value for the supplied PK */
  PROCEDURE setRegionMandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_region_mand_ind%TYPE);

 /* Updates ADDRESS_FORMATS.ADF_TOWN_MAND_IND to the supplied value for the supplied PK */
  PROCEDURE setTownMandInd (p_pk IN address_formats.adf_id%TYPE, p_val IN address_formats.adf_town_mand_ind%TYPE);

 /* Updates a row on the ADDRESS_FORMATS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN address_formats.adf_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the ADDRESS_FORMATS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN address_formats.adf_id%TYPE, 
                      p_cty_id IN address_formats.adf_cty_id%TYPE,
                      p_default_ind IN address_formats.adf_default_ind%TYPE,
                      p_line_1_name IN address_formats.adf_line_1_name%TYPE,
                      p_line_2_name IN address_formats.adf_line_2_name%TYPE,
                      p_line_3_name IN address_formats.adf_line_3_name%TYPE,
                      p_line_4_name IN address_formats.adf_line_4_name%TYPE,
                      p_line_5_name IN address_formats.adf_line_5_name%TYPE,
                      p_post_code_name IN address_formats.adf_post_code_name%TYPE,
                      p_region_name IN address_formats.adf_region_name%TYPE,
                      p_town_name IN address_formats.adf_town_name%TYPE,
                      p_format IN address_formats.adf_format%TYPE,
                      p_name IN address_formats.adf_name%TYPE,
                      p_enabled_ind IN address_formats.adf_enabled_ind%TYPE,
                      p_line_1_mand_ind IN address_formats.adf_line_1_mand_ind%TYPE,
                      p_line_2_mand_ind IN address_formats.adf_line_2_mand_ind%TYPE,
                      p_line_3_mand_ind IN address_formats.adf_line_3_mand_ind%TYPE,
                      p_line_4_mand_ind IN address_formats.adf_line_4_mand_ind%TYPE,
                      p_line_5_mand_ind IN address_formats.adf_line_5_mand_ind%TYPE,
                      p_post_code_mand_ind IN address_formats.adf_post_code_mand_ind%TYPE,
                      p_region_mand_ind IN address_formats.adf_region_mand_ind%TYPE,
                      p_town_mand_ind IN address_formats.adf_town_mand_ind%TYPE );

 /* Inserts a row into the ADDRESS_FORMATS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN address_formats%ROWTYPE, p_newPk OUT address_formats.adf_id%TYPE);

 /* Inserts a row into the ADDRESS_FORMATS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN address_formats.adf_id%TYPE,
                      p_cty_id IN address_formats.adf_cty_id%TYPE,
                      p_default_ind IN address_formats.adf_default_ind%TYPE,
                      p_line_1_name IN address_formats.adf_line_1_name%TYPE,
                      p_line_2_name IN address_formats.adf_line_2_name%TYPE,
                      p_line_3_name IN address_formats.adf_line_3_name%TYPE,
                      p_line_4_name IN address_formats.adf_line_4_name%TYPE,
                      p_line_5_name IN address_formats.adf_line_5_name%TYPE,
                      p_post_code_name IN address_formats.adf_post_code_name%TYPE,
                      p_region_name IN address_formats.adf_region_name%TYPE,
                      p_town_name IN address_formats.adf_town_name%TYPE,
                      p_format IN address_formats.adf_format%TYPE,
                      p_name IN address_formats.adf_name%TYPE,
                      p_enabled_ind IN address_formats.adf_enabled_ind%TYPE,
                      p_line_1_mand_ind IN address_formats.adf_line_1_mand_ind%TYPE,
                      p_line_2_mand_ind IN address_formats.adf_line_2_mand_ind%TYPE,
                      p_line_3_mand_ind IN address_formats.adf_line_3_mand_ind%TYPE,
                      p_line_4_mand_ind IN address_formats.adf_line_4_mand_ind%TYPE,
                      p_line_5_mand_ind IN address_formats.adf_line_5_mand_ind%TYPE,
                      p_post_code_mand_ind IN address_formats.adf_post_code_mand_ind%TYPE,
                      p_region_mand_ind IN address_formats.adf_region_mand_ind%TYPE,
                      p_town_mand_ind IN address_formats.adf_town_mand_ind%TYPE, p_newPk OUT address_formats.adf_id%TYPE);

 /* Deletes a row from the ADDRESS_FORMATS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN address_formats.adf_id%TYPE);

END t_AddressFormats;
/
