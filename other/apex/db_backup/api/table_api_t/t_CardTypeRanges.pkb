CREATE OR REPLACE PACKAGE BODY t_CardTypeRanges IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges%ROWTYPE;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN VARCHAR2 IS
    l_row    card_type_ranges%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.ctr_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_id%TYPE IS
    l_row    card_type_ranges%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ctr_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCctId_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_cct_id%TYPE IS
    l_row    card_type_ranges%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ctr_cct_id;
  END getCctId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIinFrom_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_iin_from%TYPE IS
    l_row    card_type_ranges%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ctr_iin_from;
  END getIinFrom_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIinTo_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_iin_to%TYPE IS
    l_row    card_type_ranges%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ctr_iin_to;
  END getIinTo_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLengthFrom_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_length_from%TYPE IS
    l_row    card_type_ranges%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ctr_length_from;
  END getLengthFrom_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLengthTo_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_length_to%TYPE IS
    l_row    card_type_ranges%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ctr_length_to;
  END getLengthTo_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCheckOrder_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_check_order%TYPE IS
    l_row    card_type_ranges%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ctr_check_order;
  END getCheckOrder_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIinLength_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_iin_length%TYPE IS
    l_row    card_type_ranges%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ctr_iin_length;
  END getIinLength_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges%ROWTYPE IS
    l_row    card_type_ranges%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM card_type_ranges
       WHERE ctr_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

END t_CardTypeRanges;
/
