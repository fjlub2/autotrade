CREATE OR REPLACE PACKAGE t_CardTypeRanges IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for card_type_ranges table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF card_type_ranges%ROWTYPE;

 /* Returns a row from the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges%ROWTYPE;

 /* Pipes a row from the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN VARCHAR2;

 /* Returns CTR_ID from the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_id%TYPE;

 /* Returns CTR_CCT_ID from the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION getCctId_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_cct_id%TYPE;

 /* Returns CTR_IIN_FROM from the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION getIinFrom_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_iin_from%TYPE;

 /* Returns CTR_IIN_TO from the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION getIinTo_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_iin_to%TYPE;

 /* Returns CTR_LENGTH_FROM from the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION getLengthFrom_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_length_from%TYPE;

 /* Returns CTR_LENGTH_TO from the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION getLengthTo_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_length_to%TYPE;

 /* Returns CTR_CHECK_ORDER from the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION getCheckOrder_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_check_order%TYPE;

 /* Returns CTR_IIN_LENGTH from the CARD_TYPE_RANGES table for the supplied PK */
  FUNCTION getIinLength_PK (p_pk IN card_type_ranges.ctr_id%TYPE) RETURN card_type_ranges.ctr_iin_length%TYPE;

END t_CardTypeRanges;
/
