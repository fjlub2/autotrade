CREATE OR REPLACE PACKAGE t_Terms IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for terms table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF terms%ROWTYPE;

 /* Returns a row from the TERMS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN terms.ter_id%TYPE) RETURN terms%ROWTYPE;

 /* Pipes a row from the TERMS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN terms.ter_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the TERMS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN terms.ter_id%TYPE) RETURN VARCHAR2;

 /* Returns TER_ID from the TERMS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN terms.ter_id%TYPE) RETURN terms.ter_id%TYPE;

 /* Returns TER_TEXT from the TERMS table for the supplied PK */
  FUNCTION getText_PK (p_pk IN terms.ter_id%TYPE) RETURN terms.ter_text%TYPE;

 /* Returns TER_DATE from the TERMS table for the supplied PK */
  FUNCTION getDate_PK (p_pk IN terms.ter_id%TYPE) RETURN terms.ter_date%TYPE;

 /* Returns TER_CURRENT_IND from the TERMS table for the supplied PK */
  FUNCTION getCurrentInd_PK (p_pk IN terms.ter_id%TYPE) RETURN terms.ter_current_ind%TYPE;

 /* Returns TER_TYPE from the TERMS table for the supplied PK */
  FUNCTION getType_PK (p_pk IN terms.ter_id%TYPE) RETURN terms.ter_type%TYPE;

END t_Terms;
/
