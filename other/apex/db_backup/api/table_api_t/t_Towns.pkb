CREATE OR REPLACE PACKAGE BODY t_Towns IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN towns.twn_id%TYPE) RETURN towns%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN towns.twn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN towns.twn_id%TYPE);

  PROCEDURE doInsert (p_row IN towns%ROWTYPE, p_newPk OUT towns.twn_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN towns.twn_id%TYPE) RETURN towns%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN towns.twn_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN towns.twn_id%TYPE) RETURN VARCHAR2 IS
    l_row    towns%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.twn_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_id%TYPE IS
    l_row    towns%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.twn_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRgnId_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_rgn_id%TYPE IS
    l_row    towns%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.twn_rgn_id;
  END getRgnId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_name%TYPE IS
    l_row    towns%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.twn_name;
  END getName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPostCode_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_post_code%TYPE IS
    l_row    towns%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.twn_post_code;
  END getPostCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLat_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_lat%TYPE IS
    l_row    towns%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.twn_lat;
  END getLat_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLon_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_lon%TYPE IS
    l_row    towns%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.twn_lon;
  END getLon_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_enabled_ind%TYPE IS
    l_row    towns%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.twn_enabled_ind;
  END getEnabledInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCtyId_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_cty_id%TYPE IS
    l_row    towns%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.twn_cty_id;
  END getCtyId_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setRgnId (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_rgn_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.twn_rgn_id := a_data.gFalse;
    l_ignoreRow.twn_name := a_data.gTrue;
    l_ignoreRow.twn_post_code := a_data.gTrue;
    l_ignoreRow.twn_lat := a_data.gTrue;
    l_ignoreRow.twn_lon := a_data.gTrue;
    l_ignoreRow.twn_enabled_ind := a_data.gTrue;
    l_ignoreRow.twn_cty_id := a_data.gTrue;

    l_updateRow.twn_rgn_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setRgnId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setName (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.twn_rgn_id := a_data.gTrue;
    l_ignoreRow.twn_name := a_data.gFalse;
    l_ignoreRow.twn_post_code := a_data.gTrue;
    l_ignoreRow.twn_lat := a_data.gTrue;
    l_ignoreRow.twn_lon := a_data.gTrue;
    l_ignoreRow.twn_enabled_ind := a_data.gTrue;
    l_ignoreRow.twn_cty_id := a_data.gTrue;

    l_updateRow.twn_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setPostCode (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_post_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.twn_rgn_id := a_data.gTrue;
    l_ignoreRow.twn_name := a_data.gTrue;
    l_ignoreRow.twn_post_code := a_data.gFalse;
    l_ignoreRow.twn_lat := a_data.gTrue;
    l_ignoreRow.twn_lon := a_data.gTrue;
    l_ignoreRow.twn_enabled_ind := a_data.gTrue;
    l_ignoreRow.twn_cty_id := a_data.gTrue;

    l_updateRow.twn_post_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setPostCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLat (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_lat%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.twn_rgn_id := a_data.gTrue;
    l_ignoreRow.twn_name := a_data.gTrue;
    l_ignoreRow.twn_post_code := a_data.gTrue;
    l_ignoreRow.twn_lat := a_data.gFalse;
    l_ignoreRow.twn_lon := a_data.gTrue;
    l_ignoreRow.twn_enabled_ind := a_data.gTrue;
    l_ignoreRow.twn_cty_id := a_data.gTrue;

    l_updateRow.twn_lat := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLat;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLon (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_lon%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.twn_rgn_id := a_data.gTrue;
    l_ignoreRow.twn_name := a_data.gTrue;
    l_ignoreRow.twn_post_code := a_data.gTrue;
    l_ignoreRow.twn_lat := a_data.gTrue;
    l_ignoreRow.twn_lon := a_data.gFalse;
    l_ignoreRow.twn_enabled_ind := a_data.gTrue;
    l_ignoreRow.twn_cty_id := a_data.gTrue;

    l_updateRow.twn_lon := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLon;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEnabledInd (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_enabled_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.twn_rgn_id := a_data.gTrue;
    l_ignoreRow.twn_name := a_data.gTrue;
    l_ignoreRow.twn_post_code := a_data.gTrue;
    l_ignoreRow.twn_lat := a_data.gTrue;
    l_ignoreRow.twn_lon := a_data.gTrue;
    l_ignoreRow.twn_enabled_ind := a_data.gFalse;
    l_ignoreRow.twn_cty_id := a_data.gTrue;

    l_updateRow.twn_enabled_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEnabledInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCtyId (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_cty_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.twn_rgn_id := a_data.gTrue;
    l_ignoreRow.twn_name := a_data.gTrue;
    l_ignoreRow.twn_post_code := a_data.gTrue;
    l_ignoreRow.twn_lat := a_data.gTrue;
    l_ignoreRow.twn_lon := a_data.gTrue;
    l_ignoreRow.twn_enabled_ind := a_data.gTrue;
    l_ignoreRow.twn_cty_id := a_data.gFalse;

    l_updateRow.twn_cty_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCtyId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN towns.twn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN towns.twn_id%TYPE, 
                      p_rgn_id IN towns.twn_rgn_id%TYPE,
                      p_name IN towns.twn_name%TYPE,
                      p_post_code IN towns.twn_post_code%TYPE,
                      p_lat IN towns.twn_lat%TYPE,
                      p_lon IN towns.twn_lon%TYPE,
                      p_enabled_ind IN towns.twn_enabled_ind%TYPE,
                      p_cty_id IN towns.twn_cty_id%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.twn_rgn_id := p_rgn_id;
    l_row.twn_name := p_name;
    l_row.twn_post_code := p_post_code;
    l_row.twn_lat := p_lat;
    l_row.twn_lon := p_lon;
    l_row.twn_enabled_ind := p_enabled_ind;
    l_row.twn_cty_id := p_cty_id;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN towns%ROWTYPE, p_newPK OUT towns.twn_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN towns.twn_id%TYPE,
                      p_rgn_id IN towns.twn_rgn_id%TYPE,
                      p_name IN towns.twn_name%TYPE,
                      p_post_code IN towns.twn_post_code%TYPE,
                      p_lat IN towns.twn_lat%TYPE,
                      p_lon IN towns.twn_lon%TYPE,
                      p_enabled_ind IN towns.twn_enabled_ind%TYPE,
                      p_cty_id IN towns.twn_cty_id%TYPE, p_newPK OUT towns.twn_id%TYPE) IS
    l_row   towns%ROWTYPE;
  BEGIN
    l_row.twn_id := p_id;
    l_row.twn_rgn_id := p_rgn_id;
    l_row.twn_name := p_name;
    l_row.twn_post_code := p_post_code;
    l_row.twn_lat := p_lat;
    l_row.twn_lon := p_lon;
    l_row.twn_enabled_ind := p_enabled_ind;
    l_row.twn_cty_id := p_cty_id;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN towns.twn_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN towns.twn_id%TYPE) RETURN towns%ROWTYPE IS
    l_row    towns%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM towns
       WHERE twn_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN towns.twn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE towns
         SET 
             twn_rgn_id = CASE NVL(p_ignore.twn_rgn_id, a_data.gFalse) WHEN a_data.gTrue THEN twn_rgn_id ELSE p_row.twn_rgn_id END,
             twn_name = CASE NVL(p_ignore.twn_name, a_data.gFalse) WHEN a_data.gTrue THEN twn_name ELSE p_row.twn_name END,
             twn_post_code = CASE NVL(p_ignore.twn_post_code, a_data.gFalse) WHEN a_data.gTrue THEN twn_post_code ELSE p_row.twn_post_code END,
             twn_lat = CASE NVL(p_ignore.twn_lat, a_data.gFalse) WHEN a_data.gTrue THEN twn_lat ELSE p_row.twn_lat END,
             twn_lon = CASE NVL(p_ignore.twn_lon, a_data.gFalse) WHEN a_data.gTrue THEN twn_lon ELSE p_row.twn_lon END,
             twn_enabled_ind = CASE NVL(p_ignore.twn_enabled_ind, a_data.gFalse) WHEN a_data.gTrue THEN twn_enabled_ind ELSE p_row.twn_enabled_ind END,
             twn_cty_id = CASE NVL(p_ignore.twn_cty_id, a_data.gFalse) WHEN a_data.gTrue THEN twn_cty_id ELSE p_row.twn_cty_id END
         WHERE twn_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN towns.twn_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE towns
       WHERE twn_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN towns%ROWTYPE, p_newPk OUT towns.twn_id%TYPE) IS
    l_pk    towns.twn_id%TYPE;
  BEGIN 
    INSERT
      INTO towns
    VALUES p_row
    RETURNING twn_id
         INTO p_newPk;
  END doInsert;

END t_Towns;
/
