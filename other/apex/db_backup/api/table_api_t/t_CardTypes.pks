CREATE OR REPLACE PACKAGE t_CardTypes IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for card_types table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF card_types%ROWTYPE;

 /* Returns a row from the CARD_TYPES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types%ROWTYPE;

 /* Pipes a row from the CARD_TYPES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN card_types.cct_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the CARD_TYPES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN card_types.cct_id%TYPE) RETURN VARCHAR2;

 /* Returns CCT_ID from the CARD_TYPES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_id%TYPE;

 /* Returns CCT_NETWORK_NAME from the CARD_TYPES table for the supplied PK */
  FUNCTION getNetworkName_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_network_name%TYPE;

 /* Returns CCT_ACTIVE_IND from the CARD_TYPES table for the supplied PK */
  FUNCTION getActiveInd_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_active_ind%TYPE;

 /* Returns CCT_LHUN_VALIDATION from the CARD_TYPES table for the supplied PK */
  FUNCTION getLhunValidation_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_lhun_validation%TYPE;

 /* Returns CCT_ALLOWED_IND from the CARD_TYPES table for the supplied PK */
  FUNCTION getAllowedInd_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_allowed_ind%TYPE;

 /* Returns CCT_ICON from the CARD_TYPES table for the supplied PK */
  FUNCTION getIcon_PK (p_pk IN card_types.cct_id%TYPE) RETURN card_types.cct_icon%TYPE;

END t_CardTypes;
/
