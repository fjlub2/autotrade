CREATE OR REPLACE PACKAGE t_Users IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for users table

   Should be applied on the API schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF users%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              usr_username   users.usr_username%TYPE,
                              usr_first_name   users.usr_first_name%TYPE,
                              usr_last_name   users.usr_last_name%TYPE,
                              usr_company   users.usr_company%TYPE,
                              usr_email   users.usr_email%TYPE,
                              usr_status   users.usr_status%TYPE,
                              usr_password   users.usr_password%TYPE,
                              usr_pwd_tries   users.usr_pwd_tries%TYPE,
                              usr_change_pw_on_logon   users.usr_change_pw_on_logon%TYPE,
                              usr_lng_id   users.usr_lng_id%TYPE,
                              usr_bill_add_same_as_res_ind   users.usr_bill_add_same_as_res_ind%TYPE,
                              usr_dob   users.usr_dob%TYPE,
                              usr_mobile_no   users.usr_mobile_no%TYPE,
                              usr_business_no   users.usr_business_no%TYPE,
                              usr_home_no   users.usr_home_no%TYPE,
                              usr_objective   users.usr_objective%TYPE,
                              usr_vision   users.usr_vision%TYPE,
                              usr_message_level   users.usr_message_level%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              usr_username   VARCHAR2(1),
                              usr_first_name   VARCHAR2(1),
                              usr_last_name   VARCHAR2(1),
                              usr_company   VARCHAR2(1),
                              usr_email   VARCHAR2(1),
                              usr_status   VARCHAR2(1),
                              usr_password   VARCHAR2(1),
                              usr_pwd_tries   VARCHAR2(1),
                              usr_change_pw_on_logon   VARCHAR2(1),
                              usr_lng_id   VARCHAR2(1),
                              usr_bill_add_same_as_res_ind   VARCHAR2(1),
                              usr_dob   VARCHAR2(1),
                              usr_mobile_no   VARCHAR2(1),
                              usr_business_no   VARCHAR2(1),
                              usr_home_no   VARCHAR2(1),
                              usr_objective   VARCHAR2(1),
                              usr_vision   VARCHAR2(1),
                              usr_message_level   VARCHAR2(1));

 /* Returns a row from the USERS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN users.usr_id%TYPE) RETURN users%ROWTYPE;

 /* Pipes a row from the USERS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN users.usr_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USERS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN users.usr_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the USERS table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN users.usr_username%TYPE) RETURN users%ROWTYPE;

 /* Pipes a row from the USERS table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN users.usr_username%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USERS table for the supplied UK */
  FUNCTION exists_UK (p_uk IN users.usr_username%TYPE) RETURN VARCHAR2;

 /* Returns USR_ID from the USERS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_id%TYPE;

 /* Returns USR_ID from the USERS table for the supplied UK */
  FUNCTION getId_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_id%TYPE;

 /* Returns USR_USERNAME from the USERS table for the supplied PK */
  FUNCTION getUsername_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_username%TYPE;

 /* Returns USR_USERNAME from the USERS table for the supplied UK */
  FUNCTION getUsername_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_username%TYPE;

 /* Returns USR_FIRST_NAME from the USERS table for the supplied PK */
  FUNCTION getFirstName_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_first_name%TYPE;

 /* Returns USR_FIRST_NAME from the USERS table for the supplied UK */
  FUNCTION getFirstName_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_first_name%TYPE;

 /* Returns USR_LAST_NAME from the USERS table for the supplied PK */
  FUNCTION getLastName_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_last_name%TYPE;

 /* Returns USR_LAST_NAME from the USERS table for the supplied UK */
  FUNCTION getLastName_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_last_name%TYPE;

 /* Returns USR_COMPANY from the USERS table for the supplied PK */
  FUNCTION getCompany_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_company%TYPE;

 /* Returns USR_COMPANY from the USERS table for the supplied UK */
  FUNCTION getCompany_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_company%TYPE;

 /* Returns USR_EMAIL from the USERS table for the supplied PK */
  FUNCTION getEmail_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_email%TYPE;

 /* Returns USR_EMAIL from the USERS table for the supplied UK */
  FUNCTION getEmail_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_email%TYPE;

 /* Returns USR_STATUS from the USERS table for the supplied PK */
  FUNCTION getStatus_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_status%TYPE;

 /* Returns USR_STATUS from the USERS table for the supplied UK */
  FUNCTION getStatus_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_status%TYPE;

 /* Returns USR_PASSWORD from the USERS table for the supplied PK */
  FUNCTION getPassword_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_password%TYPE;

 /* Returns USR_PASSWORD from the USERS table for the supplied UK */
  FUNCTION getPassword_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_password%TYPE;

 /* Returns USR_PWD_TRIES from the USERS table for the supplied PK */
  FUNCTION getPwdTries_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_pwd_tries%TYPE;

 /* Returns USR_PWD_TRIES from the USERS table for the supplied UK */
  FUNCTION getPwdTries_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_pwd_tries%TYPE;

 /* Returns USR_CHANGE_PW_ON_LOGON from the USERS table for the supplied PK */
  FUNCTION getChangePwOnLogon_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_change_pw_on_logon%TYPE;

 /* Returns USR_CHANGE_PW_ON_LOGON from the USERS table for the supplied UK */
  FUNCTION getChangePwOnLogon_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_change_pw_on_logon%TYPE;

 /* Returns USR_LNG_ID from the USERS table for the supplied PK */
  FUNCTION getLngId_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_lng_id%TYPE;

 /* Returns USR_LNG_ID from the USERS table for the supplied UK */
  FUNCTION getLngId_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_lng_id%TYPE;

 /* Returns USR_BILL_ADD_SAME_AS_RES_IND from the USERS table for the supplied PK */
  FUNCTION getBillAddSameAsResInd_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_bill_add_same_as_res_ind%TYPE;

 /* Returns USR_BILL_ADD_SAME_AS_RES_IND from the USERS table for the supplied UK */
  FUNCTION getBillAddSameAsResInd_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_bill_add_same_as_res_ind%TYPE;

 /* Returns USR_DOB from the USERS table for the supplied PK */
  FUNCTION getDob_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_dob%TYPE;

 /* Returns USR_DOB from the USERS table for the supplied UK */
  FUNCTION getDob_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_dob%TYPE;

 /* Returns USR_MOBILE_NO from the USERS table for the supplied PK */
  FUNCTION getMobileNo_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_mobile_no%TYPE;

 /* Returns USR_MOBILE_NO from the USERS table for the supplied UK */
  FUNCTION getMobileNo_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_mobile_no%TYPE;

 /* Returns USR_BUSINESS_NO from the USERS table for the supplied PK */
  FUNCTION getBusinessNo_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_business_no%TYPE;

 /* Returns USR_BUSINESS_NO from the USERS table for the supplied UK */
  FUNCTION getBusinessNo_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_business_no%TYPE;

 /* Returns USR_HOME_NO from the USERS table for the supplied PK */
  FUNCTION getHomeNo_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_home_no%TYPE;

 /* Returns USR_HOME_NO from the USERS table for the supplied UK */
  FUNCTION getHomeNo_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_home_no%TYPE;

 /* Returns USR_OBJECTIVE from the USERS table for the supplied PK */
  FUNCTION getObjective_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_objective%TYPE;

 /* Returns USR_OBJECTIVE from the USERS table for the supplied UK */
  FUNCTION getObjective_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_objective%TYPE;

 /* Returns USR_VISION from the USERS table for the supplied PK */
  FUNCTION getVision_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_vision%TYPE;

 /* Returns USR_VISION from the USERS table for the supplied UK */
  FUNCTION getVision_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_vision%TYPE;

 /* Returns USR_MESSAGE_LEVEL from the USERS table for the supplied PK */
  FUNCTION getMessageLevel_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_message_level%TYPE;

 /* Returns USR_MESSAGE_LEVEL from the USERS table for the supplied UK */
  FUNCTION getMessageLevel_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_message_level%TYPE;

 /* Updates USERS.USR_USERNAME to the supplied value for the supplied PK */
  PROCEDURE setUsername (p_pk IN users.usr_id%TYPE, p_val IN users.usr_username%TYPE);

 /* Updates USERS.USR_FIRST_NAME to the supplied value for the supplied PK */
  PROCEDURE setFirstName (p_pk IN users.usr_id%TYPE, p_val IN users.usr_first_name%TYPE);

 /* Updates USERS.USR_LAST_NAME to the supplied value for the supplied PK */
  PROCEDURE setLastName (p_pk IN users.usr_id%TYPE, p_val IN users.usr_last_name%TYPE);

 /* Updates USERS.USR_COMPANY to the supplied value for the supplied PK */
  PROCEDURE setCompany (p_pk IN users.usr_id%TYPE, p_val IN users.usr_company%TYPE);

 /* Updates USERS.USR_EMAIL to the supplied value for the supplied PK */
  PROCEDURE setEmail (p_pk IN users.usr_id%TYPE, p_val IN users.usr_email%TYPE);

 /* Updates USERS.USR_STATUS to the supplied value for the supplied PK */
  PROCEDURE setStatus (p_pk IN users.usr_id%TYPE, p_val IN users.usr_status%TYPE);

 /* Updates USERS.USR_PASSWORD to the supplied value for the supplied PK */
  PROCEDURE setPassword (p_pk IN users.usr_id%TYPE, p_val IN users.usr_password%TYPE);

 /* Updates USERS.USR_PWD_TRIES to the supplied value for the supplied PK */
  PROCEDURE setPwdTries (p_pk IN users.usr_id%TYPE, p_val IN users.usr_pwd_tries%TYPE);

 /* Updates USERS.USR_CHANGE_PW_ON_LOGON to the supplied value for the supplied PK */
  PROCEDURE setChangePwOnLogon (p_pk IN users.usr_id%TYPE, p_val IN users.usr_change_pw_on_logon%TYPE);

 /* Updates USERS.USR_LNG_ID to the supplied value for the supplied PK */
  PROCEDURE setLngId (p_pk IN users.usr_id%TYPE, p_val IN users.usr_lng_id%TYPE);

 /* Updates USERS.USR_BILL_ADD_SAME_AS_RES_IND to the supplied value for the supplied PK */
  PROCEDURE setBillAddSameAsResInd (p_pk IN users.usr_id%TYPE, p_val IN users.usr_bill_add_same_as_res_ind%TYPE);

 /* Updates USERS.USR_DOB to the supplied value for the supplied PK */
  PROCEDURE setDob (p_pk IN users.usr_id%TYPE, p_val IN users.usr_dob%TYPE);

 /* Updates USERS.USR_MOBILE_NO to the supplied value for the supplied PK */
  PROCEDURE setMobileNo (p_pk IN users.usr_id%TYPE, p_val IN users.usr_mobile_no%TYPE);

 /* Updates USERS.USR_BUSINESS_NO to the supplied value for the supplied PK */
  PROCEDURE setBusinessNo (p_pk IN users.usr_id%TYPE, p_val IN users.usr_business_no%TYPE);

 /* Updates USERS.USR_HOME_NO to the supplied value for the supplied PK */
  PROCEDURE setHomeNo (p_pk IN users.usr_id%TYPE, p_val IN users.usr_home_no%TYPE);

 /* Updates USERS.USR_OBJECTIVE to the supplied value for the supplied PK */
  PROCEDURE setObjective (p_pk IN users.usr_id%TYPE, p_val IN users.usr_objective%TYPE);

 /* Updates USERS.USR_VISION to the supplied value for the supplied PK */
  PROCEDURE setVision (p_pk IN users.usr_id%TYPE, p_val IN users.usr_vision%TYPE);

 /* Updates USERS.USR_MESSAGE_LEVEL to the supplied value for the supplied PK */
  PROCEDURE setMessageLevel (p_pk IN users.usr_id%TYPE, p_val IN users.usr_message_level%TYPE);

 /* Updates a row on the USERS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN users.usr_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the USERS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN users.usr_id%TYPE, 
                      p_username IN users.usr_username%TYPE,
                      p_first_name IN users.usr_first_name%TYPE,
                      p_last_name IN users.usr_last_name%TYPE,
                      p_company IN users.usr_company%TYPE,
                      p_email IN users.usr_email%TYPE,
                      p_status IN users.usr_status%TYPE,
                      p_password IN users.usr_password%TYPE,
                      p_pwd_tries IN users.usr_pwd_tries%TYPE,
                      p_change_pw_on_logon IN users.usr_change_pw_on_logon%TYPE,
                      p_lng_id IN users.usr_lng_id%TYPE,
                      p_bill_add_same_as_res_ind IN users.usr_bill_add_same_as_res_ind%TYPE,
                      p_dob IN users.usr_dob%TYPE,
                      p_mobile_no IN users.usr_mobile_no%TYPE,
                      p_business_no IN users.usr_business_no%TYPE,
                      p_home_no IN users.usr_home_no%TYPE,
                      p_objective IN users.usr_objective%TYPE,
                      p_vision IN users.usr_vision%TYPE,
                      p_message_level IN users.usr_message_level%TYPE );

 /* Inserts a row into the USERS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN users%ROWTYPE, p_newPk OUT users.usr_id%TYPE);

 /* Inserts a row into the USERS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN users.usr_id%TYPE,
                      p_username IN users.usr_username%TYPE,
                      p_first_name IN users.usr_first_name%TYPE,
                      p_last_name IN users.usr_last_name%TYPE,
                      p_company IN users.usr_company%TYPE,
                      p_email IN users.usr_email%TYPE,
                      p_status IN users.usr_status%TYPE,
                      p_password IN users.usr_password%TYPE,
                      p_pwd_tries IN users.usr_pwd_tries%TYPE,
                      p_change_pw_on_logon IN users.usr_change_pw_on_logon%TYPE,
                      p_lng_id IN users.usr_lng_id%TYPE,
                      p_bill_add_same_as_res_ind IN users.usr_bill_add_same_as_res_ind%TYPE,
                      p_dob IN users.usr_dob%TYPE,
                      p_mobile_no IN users.usr_mobile_no%TYPE,
                      p_business_no IN users.usr_business_no%TYPE,
                      p_home_no IN users.usr_home_no%TYPE,
                      p_objective IN users.usr_objective%TYPE,
                      p_vision IN users.usr_vision%TYPE,
                      p_message_level IN users.usr_message_level%TYPE, p_newPk OUT users.usr_id%TYPE);

 /* Deletes a row from the USERS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN users.usr_id%TYPE);

END t_Users;
/
