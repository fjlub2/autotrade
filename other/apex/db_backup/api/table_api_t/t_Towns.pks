CREATE OR REPLACE PACKAGE t_Towns IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for towns table

   Should be applied on the CORE schema. No grants or synonyms should be applied to this package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF towns%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              twn_rgn_id   towns.twn_rgn_id%TYPE,
                              twn_name   towns.twn_name%TYPE,
                              twn_post_code   towns.twn_post_code%TYPE,
                              twn_lat   towns.twn_lat%TYPE,
                              twn_lon   towns.twn_lon%TYPE,
                              twn_enabled_ind   towns.twn_enabled_ind%TYPE,
                              twn_cty_id   towns.twn_cty_id%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              twn_rgn_id   VARCHAR2(1),
                              twn_name   VARCHAR2(1),
                              twn_post_code   VARCHAR2(1),
                              twn_lat   VARCHAR2(1),
                              twn_lon   VARCHAR2(1),
                              twn_enabled_ind   VARCHAR2(1),
                              twn_cty_id   VARCHAR2(1));

 /* Returns a row from the TOWNS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN towns.twn_id%TYPE) RETURN towns%ROWTYPE;

 /* Pipes a row from the TOWNS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN towns.twn_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the TOWNS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN towns.twn_id%TYPE) RETURN VARCHAR2;

 /* Returns TWN_ID from the TOWNS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_id%TYPE;

 /* Returns TWN_RGN_ID from the TOWNS table for the supplied PK */
  FUNCTION getRgnId_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_rgn_id%TYPE;

 /* Returns TWN_NAME from the TOWNS table for the supplied PK */
  FUNCTION getName_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_name%TYPE;

 /* Returns TWN_POST_CODE from the TOWNS table for the supplied PK */
  FUNCTION getPostCode_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_post_code%TYPE;

 /* Returns TWN_LAT from the TOWNS table for the supplied PK */
  FUNCTION getLat_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_lat%TYPE;

 /* Returns TWN_LON from the TOWNS table for the supplied PK */
  FUNCTION getLon_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_lon%TYPE;

 /* Returns TWN_ENABLED_IND from the TOWNS table for the supplied PK */
  FUNCTION getEnabledInd_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_enabled_ind%TYPE;

 /* Returns TWN_CTY_ID from the TOWNS table for the supplied PK */
  FUNCTION getCtyId_PK (p_pk IN towns.twn_id%TYPE) RETURN towns.twn_cty_id%TYPE;

 /* Updates TOWNS.TWN_RGN_ID to the supplied value for the supplied PK */
  PROCEDURE setRgnId (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_rgn_id%TYPE);

 /* Updates TOWNS.TWN_NAME to the supplied value for the supplied PK */
  PROCEDURE setName (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_name%TYPE);

 /* Updates TOWNS.TWN_POST_CODE to the supplied value for the supplied PK */
  PROCEDURE setPostCode (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_post_code%TYPE);

 /* Updates TOWNS.TWN_LAT to the supplied value for the supplied PK */
  PROCEDURE setLat (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_lat%TYPE);

 /* Updates TOWNS.TWN_LON to the supplied value for the supplied PK */
  PROCEDURE setLon (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_lon%TYPE);

 /* Updates TOWNS.TWN_ENABLED_IND to the supplied value for the supplied PK */
  PROCEDURE setEnabledInd (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_enabled_ind%TYPE);

 /* Updates TOWNS.TWN_CTY_ID to the supplied value for the supplied PK */
  PROCEDURE setCtyId (p_pk IN towns.twn_id%TYPE, p_val IN towns.twn_cty_id%TYPE);

 /* Updates a row on the TOWNS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN towns.twn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the TOWNS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN towns.twn_id%TYPE, 
                      p_rgn_id IN towns.twn_rgn_id%TYPE,
                      p_name IN towns.twn_name%TYPE,
                      p_post_code IN towns.twn_post_code%TYPE,
                      p_lat IN towns.twn_lat%TYPE,
                      p_lon IN towns.twn_lon%TYPE,
                      p_enabled_ind IN towns.twn_enabled_ind%TYPE,
                      p_cty_id IN towns.twn_cty_id%TYPE );

 /* Inserts a row into the TOWNS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN towns%ROWTYPE, p_newPk OUT towns.twn_id%TYPE);

 /* Inserts a row into the TOWNS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN towns.twn_id%TYPE,
                      p_rgn_id IN towns.twn_rgn_id%TYPE,
                      p_name IN towns.twn_name%TYPE,
                      p_post_code IN towns.twn_post_code%TYPE,
                      p_lat IN towns.twn_lat%TYPE,
                      p_lon IN towns.twn_lon%TYPE,
                      p_enabled_ind IN towns.twn_enabled_ind%TYPE,
                      p_cty_id IN towns.twn_cty_id%TYPE, p_newPk OUT towns.twn_id%TYPE);

 /* Deletes a row from the TOWNS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN towns.twn_id%TYPE);

END t_Towns;
/
