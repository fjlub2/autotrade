CREATE OR REPLACE PACKAGE BODY t_TempLinks IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN temp_links.tln_id%TYPE, p_uk IN temp_links.tln_link_code%TYPE DEFAULT NULL) RETURN temp_links%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN temp_links.tln_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN temp_links.tln_id%TYPE);

  PROCEDURE doInsert (p_row IN temp_links%ROWTYPE, p_newPk OUT temp_links.tln_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN temp_links.tln_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN temp_links.tln_id%TYPE) RETURN VARCHAR2 IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.tln_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN VARCHAR2 IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.tln_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_id%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tln_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_id%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.tln_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_usr_id%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tln_usr_id;
  END getUsrId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_usr_id%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.tln_usr_id;
  END getUsrId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLinkCode_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_link_code%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tln_link_code;
  END getLinkCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLinkCode_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_link_code%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.tln_link_code;
  END getLinkCode_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getExpiryDate_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_expiry_date%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tln_expiry_date;
  END getExpiryDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getExpiryDate_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_expiry_date%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.tln_expiry_date;
  END getExpiryDate_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsedInd_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_used_ind%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tln_used_ind;
  END getUsedInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsedInd_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_used_ind%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.tln_used_ind;
  END getUsedInd_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsedDate_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_used_date%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tln_used_date;
  END getUsedDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsedDate_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_used_date%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.tln_used_date;
  END getUsedDate_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getType_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_type%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tln_type;
  END getType_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getType_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_type%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.tln_type;
  END getType_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSudId_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_sud_id%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.tln_sud_id;
  END getSudId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSudId_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_sud_id%TYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.tln_sud_id;
  END getSudId_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setUsrId (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_usr_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tln_usr_id := a_data.gFalse;
    l_ignoreRow.tln_link_code := a_data.gTrue;
    l_ignoreRow.tln_expiry_date := a_data.gTrue;
    l_ignoreRow.tln_used_ind := a_data.gTrue;
    l_ignoreRow.tln_used_date := a_data.gTrue;
    l_ignoreRow.tln_type := a_data.gTrue;
    l_ignoreRow.tln_sud_id := a_data.gTrue;

    l_updateRow.tln_usr_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsrId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLinkCode (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_link_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tln_usr_id := a_data.gTrue;
    l_ignoreRow.tln_link_code := a_data.gFalse;
    l_ignoreRow.tln_expiry_date := a_data.gTrue;
    l_ignoreRow.tln_used_ind := a_data.gTrue;
    l_ignoreRow.tln_used_date := a_data.gTrue;
    l_ignoreRow.tln_type := a_data.gTrue;
    l_ignoreRow.tln_sud_id := a_data.gTrue;

    l_updateRow.tln_link_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLinkCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setExpiryDate (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_expiry_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tln_usr_id := a_data.gTrue;
    l_ignoreRow.tln_link_code := a_data.gTrue;
    l_ignoreRow.tln_expiry_date := a_data.gFalse;
    l_ignoreRow.tln_used_ind := a_data.gTrue;
    l_ignoreRow.tln_used_date := a_data.gTrue;
    l_ignoreRow.tln_type := a_data.gTrue;
    l_ignoreRow.tln_sud_id := a_data.gTrue;

    l_updateRow.tln_expiry_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setExpiryDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUsedInd (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_used_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tln_usr_id := a_data.gTrue;
    l_ignoreRow.tln_link_code := a_data.gTrue;
    l_ignoreRow.tln_expiry_date := a_data.gTrue;
    l_ignoreRow.tln_used_ind := a_data.gFalse;
    l_ignoreRow.tln_used_date := a_data.gTrue;
    l_ignoreRow.tln_type := a_data.gTrue;
    l_ignoreRow.tln_sud_id := a_data.gTrue;

    l_updateRow.tln_used_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsedInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUsedDate (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_used_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tln_usr_id := a_data.gTrue;
    l_ignoreRow.tln_link_code := a_data.gTrue;
    l_ignoreRow.tln_expiry_date := a_data.gTrue;
    l_ignoreRow.tln_used_ind := a_data.gTrue;
    l_ignoreRow.tln_used_date := a_data.gFalse;
    l_ignoreRow.tln_type := a_data.gTrue;
    l_ignoreRow.tln_sud_id := a_data.gTrue;

    l_updateRow.tln_used_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsedDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setType (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_type%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tln_usr_id := a_data.gTrue;
    l_ignoreRow.tln_link_code := a_data.gTrue;
    l_ignoreRow.tln_expiry_date := a_data.gTrue;
    l_ignoreRow.tln_used_ind := a_data.gTrue;
    l_ignoreRow.tln_used_date := a_data.gTrue;
    l_ignoreRow.tln_type := a_data.gFalse;
    l_ignoreRow.tln_sud_id := a_data.gTrue;

    l_updateRow.tln_type := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setType;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setSudId (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_sud_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.tln_usr_id := a_data.gTrue;
    l_ignoreRow.tln_link_code := a_data.gTrue;
    l_ignoreRow.tln_expiry_date := a_data.gTrue;
    l_ignoreRow.tln_used_ind := a_data.gTrue;
    l_ignoreRow.tln_used_date := a_data.gTrue;
    l_ignoreRow.tln_type := a_data.gTrue;
    l_ignoreRow.tln_sud_id := a_data.gFalse;

    l_updateRow.tln_sud_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setSudId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN temp_links.tln_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN temp_links.tln_id%TYPE, 
                      p_usr_id IN temp_links.tln_usr_id%TYPE,
                      p_link_code IN temp_links.tln_link_code%TYPE,
                      p_expiry_date IN temp_links.tln_expiry_date%TYPE,
                      p_used_ind IN temp_links.tln_used_ind%TYPE,
                      p_used_date IN temp_links.tln_used_date%TYPE,
                      p_type IN temp_links.tln_type%TYPE,
                      p_sud_id IN temp_links.tln_sud_id%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.tln_usr_id := p_usr_id;
    l_row.tln_link_code := p_link_code;
    l_row.tln_expiry_date := p_expiry_date;
    l_row.tln_used_ind := p_used_ind;
    l_row.tln_used_date := p_used_date;
    l_row.tln_type := p_type;
    l_row.tln_sud_id := p_sud_id;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN temp_links%ROWTYPE, p_newPK OUT temp_links.tln_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN temp_links.tln_id%TYPE,
                      p_usr_id IN temp_links.tln_usr_id%TYPE,
                      p_link_code IN temp_links.tln_link_code%TYPE,
                      p_expiry_date IN temp_links.tln_expiry_date%TYPE,
                      p_used_ind IN temp_links.tln_used_ind%TYPE,
                      p_used_date IN temp_links.tln_used_date%TYPE,
                      p_type IN temp_links.tln_type%TYPE,
                      p_sud_id IN temp_links.tln_sud_id%TYPE, p_newPK OUT temp_links.tln_id%TYPE) IS
    l_row   temp_links%ROWTYPE;
  BEGIN
    l_row.tln_id := p_id;
    l_row.tln_usr_id := p_usr_id;
    l_row.tln_link_code := p_link_code;
    l_row.tln_expiry_date := p_expiry_date;
    l_row.tln_used_ind := p_used_ind;
    l_row.tln_used_date := p_used_date;
    l_row.tln_type := p_type;
    l_row.tln_sud_id := p_sud_id;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN temp_links.tln_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN temp_links.tln_id%TYPE, p_uk IN temp_links.tln_link_code%TYPE DEFAULT NULL) RETURN temp_links%ROWTYPE IS
    l_row    temp_links%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM temp_links
       WHERE tln_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM temp_links
       WHERE tln_link_code = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN temp_links.tln_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE temp_links
         SET 
             tln_usr_id = CASE NVL(p_ignore.tln_usr_id, a_data.gFalse) WHEN a_data.gTrue THEN tln_usr_id ELSE p_row.tln_usr_id END,
             tln_link_code = CASE NVL(p_ignore.tln_link_code, a_data.gFalse) WHEN a_data.gTrue THEN tln_link_code ELSE p_row.tln_link_code END,
             tln_expiry_date = CASE NVL(p_ignore.tln_expiry_date, a_data.gFalse) WHEN a_data.gTrue THEN tln_expiry_date ELSE p_row.tln_expiry_date END,
             tln_used_ind = CASE NVL(p_ignore.tln_used_ind, a_data.gFalse) WHEN a_data.gTrue THEN tln_used_ind ELSE p_row.tln_used_ind END,
             tln_used_date = CASE NVL(p_ignore.tln_used_date, a_data.gFalse) WHEN a_data.gTrue THEN tln_used_date ELSE p_row.tln_used_date END,
             tln_type = CASE NVL(p_ignore.tln_type, a_data.gFalse) WHEN a_data.gTrue THEN tln_type ELSE p_row.tln_type END,
             tln_sud_id = CASE NVL(p_ignore.tln_sud_id, a_data.gFalse) WHEN a_data.gTrue THEN tln_sud_id ELSE p_row.tln_sud_id END
         WHERE tln_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN temp_links.tln_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE temp_links
       WHERE tln_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN temp_links%ROWTYPE, p_newPk OUT temp_links.tln_id%TYPE) IS
    l_pk    temp_links.tln_id%TYPE;
  BEGIN 
    INSERT
      INTO temp_links
    VALUES p_row
    RETURNING tln_id
         INTO p_newPk;
  END doInsert;

END t_TempLinks;
/
