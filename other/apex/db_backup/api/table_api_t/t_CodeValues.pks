CREATE OR REPLACE PACKAGE t_CodeValues IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for code_values table

   Should be applied on the CORE schema. No grants or synonyms should be applied to this package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF code_values%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              cv_value   code_values.cv_value%TYPE,
                              cv_name   code_values.cv_name%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              cv_value   VARCHAR2(1),
                              cv_name   VARCHAR2(1));

 /* Returns a row from the CODE_VALUES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values%ROWTYPE;

 /* Pipes a row from the CODE_VALUES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the CODE_VALUES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN VARCHAR2;

 /* Returns CV_DOMAIN from the CODE_VALUES table for the supplied PK */
  FUNCTION getDomain_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values.cv_domain%TYPE;

 /* Returns CV_CODE from the CODE_VALUES table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values.cv_code%TYPE;

 /* Returns CV_VALUE from the CODE_VALUES table for the supplied PK */
  FUNCTION getValue_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values.cv_value%TYPE;

 /* Returns CV_NAME from the CODE_VALUES table for the supplied PK */
  FUNCTION getName_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values.cv_name%TYPE;

 /* Updates CODE_VALUES.CV_VALUE to the supplied value for the supplied PK */
  PROCEDURE setValue (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE, p_val IN code_values.cv_value%TYPE);

 /* Updates CODE_VALUES.CV_NAME to the supplied value for the supplied PK */
  PROCEDURE setName (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE, p_val IN code_values.cv_name%TYPE);

 /* Updates a row on the CODE_VALUES table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the CODE_VALUES table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE, 
                      p_value IN code_values.cv_value%TYPE,
                      p_name IN code_values.cv_name%TYPE );

 /* Inserts a row into the CODE_VALUES table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN code_values%ROWTYPE, p_newPk OUT code_values.cv_domain%TYPE, p_newPk2 OUT code_values.cv_code%TYPE);

 /* Inserts a row into the CODE_VALUES table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_domain IN code_values.cv_domain%TYPE,
                      p_code IN code_values.cv_code%TYPE,
                      p_value IN code_values.cv_value%TYPE,
                      p_name IN code_values.cv_name%TYPE, p_newPk OUT code_values.cv_domain%TYPE, p_newPk2 OUT code_values.cv_code%TYPE);

 /* Deletes a row from the CODE_VALUES table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE);

END t_CodeValues;
/
