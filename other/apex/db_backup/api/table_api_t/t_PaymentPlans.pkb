CREATE OR REPLACE PACKAGE BODY t_PaymentPlans IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans%ROWTYPE;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN VARCHAR2 IS
    l_row    payment_plans%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.ppl_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_id%TYPE IS
    l_row    payment_plans%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ppl_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDesc_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_desc%TYPE IS
    l_row    payment_plans%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ppl_desc;
  END getDesc_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAmount_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_amount%TYPE IS
    l_row    payment_plans%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ppl_amount;
  END getAmount_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getFrequency_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_frequency%TYPE IS
    l_row    payment_plans%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ppl_frequency;
  END getFrequency_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_enabled_ind%TYPE IS
    l_row    payment_plans%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ppl_enabled_ind;
  END getEnabledInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMinTerm_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_min_term%TYPE IS
    l_row    payment_plans%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ppl_min_term;
  END getMinTerm_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCcyId_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_ccy_id%TYPE IS
    l_row    payment_plans%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ppl_ccy_id;
  END getCcyId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDisplayOrder_PK (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans.ppl_display_order%TYPE IS
    l_row    payment_plans%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ppl_display_order;
  END getDisplayOrder_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN payment_plans.ppl_id%TYPE) RETURN payment_plans%ROWTYPE IS
    l_row    payment_plans%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM payment_plans
       WHERE ppl_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

END t_PaymentPlans;
/
