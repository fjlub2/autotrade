CREATE OR REPLACE PACKAGE a_Cards IS 
/* ----------------------------------------------------------------------------------------------------------
   Serivce APIs to perform card validations
   
   Always add the schema name on calls to this package to prevent interception.
   e.g. api.c_CardValidations.lhunValidation
   
   ---------------------------------------------------------------------------------------------------------- */
                           
 /* Does card number pass lhun validation a_Data.g_True or a_Data.g_False */
  FUNCTION lhunValidation (p_cardNo IN VARCHAR2) RETURN VARCHAR2;

 /* Identify the card type. Use this to dynamically identify the card type while the user is still entering the card number.
    No errors will be returned. */
  FUNCTION identifyCardType (p_cardNo IN VARCHAR2) RETURN card_types.cct_id%TYPE;

 /* Identify the card type. Use this to identify the card type when the user submits the card number.
    Errors will be returned. */
  PROCEDURE identifyCardType (p_cardNo  IN  VARCHAR2,
                              p_cctId   OUT card_types.cct_id%TYPE,
                              p_errMsg  OUT message_codes.msc_default_text%TYPE);

 /* Returns the card type icon for the supplied cct id */                 
  FUNCTION getCardTypeIcon (p_cctId IN card_types.cct_id%TYPE) RETURN card_types.cct_icon%TYPE;
END a_Cards;
/

CREATE OR REPLACE PACKAGE BODY a_Cards IS 
  ------------------------------------------------------------------------------------------
  -- Forward declarations
  ------------------------------------------------------------------------------------------
  
  PROCEDURE identifyCardType (p_cardNo   IN  VARCHAR2,
                              p_complete IN  VARCHAR2,
                              p_cctId    OUT card_types.cct_id%TYPE,
                              p_errMsg   OUT message_codes.msc_default_text%TYPE);
  
  ------------------------------------------------------------------------------------------
  -- Public procedures / functions
  ------------------------------------------------------------------------------------------
  
  FUNCTION lhunValidation (p_cardNo IN VARCHAR2) RETURN VARCHAR2 IS
    l_digit INTEGER;
    l_total INTEGER := 0;
  BEGIN
    FOR i IN 1..LENGTH(p_cardNo) LOOP
      l_digit := TO_NUMBER(SUBSTR(p_cardNo, -i, 1));
   
      IF MOD(i,2) = 0 THEN
        l_digit := l_digit * 2;
        IF l_digit > 9 THEN
          l_digit := l_digit - 9;
        END IF;
      END IF;
    
      l_total := l_total + l_digit;
    END LOOP;
    
    IF MOD(l_total,10) != 0 THEN
      RETURN a_data.gFalse;
    ELSE
      RETURN a_data.gTrue;
    END IF; 
  END lhunValidation;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION identifyCardType (p_cardNo IN VARCHAR2) RETURN card_types.cct_id%TYPE IS
    l_errMsg      message_codes.msc_default_text%TYPE;
    l_cctId       card_types.cct_id%TYPE;
  BEGIN
    identifyCardType (p_cardNo   => p_cardNo,
                      p_complete => a_Data.gFalse,
                      p_cctId    => l_cctId,
                      p_errMsg   => l_errMsg);
    
    RETURN l_cctId;
  END identifyCardType;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE identifyCardType (p_cardNo  IN  VARCHAR2,
                              p_cctId   OUT card_types.cct_id%TYPE,
                              p_errMsg  OUT message_codes.msc_default_text%TYPE) IS
  BEGIN
    identifyCardType (p_cardNo   => p_cardNo,
                      p_complete => a_Data.gTrue,
                      p_cctId    => p_cctId,
                      p_errMsg   => p_errMsg);
  END identifyCardType;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getCardTypeIcon (p_cctId IN card_types.cct_id%TYPE) RETURN card_types.cct_icon%TYPE IS
    l_errMsg      message_codes.msc_default_text%TYPE;
    l_cctId       card_types.cct_id%TYPE;
  BEGIN
    RETURN t_CardTypes.getIcon_PK(p_pk => p_cctId);
  END getCardTypeIcon;
  
  ------------------------------------------------------------------------------------------
  -- Private procedures / functions
  ------------------------------------------------------------------------------------------
  
  PROCEDURE identifyCardType (p_cardNo   IN  VARCHAR2,
                              p_complete IN  VARCHAR2,
                              p_cctId    OUT card_types.cct_id%TYPE,
                              p_errMsg   OUT message_codes.msc_default_text%TYPE) IS    
    l_cctId         card_types.cct_id%TYPE; 
    l_cctActive     card_types.cct_active_ind%TYPE; 
    l_cctAllowed    card_types.cct_allowed_ind%TYPE; 
    l_cctNetwork    card_types.cct_network_name%TYPE;
  BEGIN
    SELECT cct_id,
           cct_active_ind,
           cct_allowed_ind,
           cct_network_name
      INTO l_cctId,
           l_cctActive,
           l_cctAllowed,
           l_cctNetwork
      FROM (SELECT cct_id,
                   cct_active_ind,
                   cct_allowed_ind,
                   cct_network_name,
                   ROW_NUMBER() OVER (ORDER BY ctr_check_order,
                                               cct_allowed_ind DESC,
                                               cct_active_ind  DESC,
                                               ctr_length_from) keep_one
              FROM card_types cct
                JOIN card_type_ranges ctr ON ctr_cct_id = cct_id
             WHERE SUBSTR(p_cardNo,1,ctr_iin_length) BETWEEN ctr_iin_from AND ctr_iin_to
               AND ((p_complete = a_Data.gTrue AND
                     LENGTH(p_cardNo) BETWEEN ctr_length_from AND ctr_length_to) OR
                    (p_complete = a_Data.gFalse AND
                     LENGTH(p_cardNo) <= ctr_length_to)))
     WHERE keep_one = 1;
              
    IF p_complete = a_Data.gTrue THEN
      IF l_cctActive = a_Data.gFalse THEN
        p_errMsg := a_Message.getMessageText(p_code => 'XAPP-00038',
                                             p_vars => a_Data.addTag(NULL,'CARD_TYPE',l_cctNetwork));
      ELSIF l_cctAllowed = a_Data.gFalse THEN
        p_errMsg := a_Message.getMessageText(p_code => 'XAPP-00037',
                                             p_vars => a_Data.addTag(NULL,'CARD_TYPE',l_cctNetwork));
      END IF;
    END IF;
    
    p_cctId := l_cctId;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_errMsg := NULL;
      p_cctId  := NULL;
      
      IF p_complete = a_Data.gTrue THEN
        p_errMsg := a_Message.getMessageText(p_code => 'XAPP-00039');
      END IF;
  END identifyCardType;
  
END a_Cards;
/
