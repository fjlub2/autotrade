CREATE OR REPLACE PACKAGE a_Addresses IS 
/* ----------------------------------------------------------------------------------------------------------
   Service APIs for related to addresses
   
   ---------------------------------------------------------------------------------------------------------- */

 /* Returns the full address according to the countries address format */
  FUNCTION getAddressText (p_type IN user_addresses.uad_address_type%TYPE) RETURN VARCHAR2;
 
 /* Update a users address address */
  PROCEDURE updateAddress (p_uadId     IN user_addresses.uad_id%TYPE,
                           p_line1     IN user_addresses.uad_line_1%TYPE,
                           p_line2     IN user_addresses.uad_line_2%TYPE,
                           p_line3     IN user_addresses.uad_line_3%TYPE,
                           p_line4     IN user_addresses.uad_line_4%TYPE,
                           p_line5     IN user_addresses.uad_line_5%TYPE,
                           p_postCode  IN user_addresses.uad_post_code%TYPE,
                           p_ctyId     IN user_addresses.uad_cty_id%TYPE,
                           p_rgnId     IN user_addresses.uad_rgn_id%TYPE,
                           p_twnId     IN user_addresses.uad_twn_id%TYPE,
                           p_adfId     IN user_addresses.uad_adf_id%TYPE);
                           
  /* Returns a_Data.gTrue or a_Data.gFalse depending on if supplied value is required 
     Valid values for the p_value parameter is LINE1, LINE2, LINE3, LINE4, LINE5, POST_CODE,
     REGION and TOWN */
  FUNCTION valueRequired (p_adfId IN address_formats.adf_id%TYPE,
                          p_value IN VARCHAR2) RETURN VARCHAR2;
                          
  /* Insert a blank address record and return the uad_id */
  FUNCTION insertStub (p_type IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_id%TYPE;
END a_Addresses;
/

CREATE OR REPLACE PACKAGE BODY a_Addresses IS 

  FUNCTION getAddressText (p_type IN user_addresses.uad_address_type%TYPE) RETURN VARCHAR2 IS
    l_out     VARCHAR2(4000);
    l_uadRow  user_addresses%ROWTYPE;
  BEGIN
    c_Session.validSession;
    
    l_uadRow := t_UserAddresses.getRow_UK(p_uk  => c_Session.getCurrentUsrId,
                                          p_uk2 => p_type);
    l_out    := t_AddressFormats.getFormat_PK(l_uadRow.uad_adf_id);
    
    -- replace all substitution variables 
    -- could be any of the following: LINE1, LINE2, LINE3, LINE4, LINE5, POST_CODE, REGION, REGION_CODE, TOWN, TOWN_POST_CODE, COUNTRY
    
    l_out := REPLACE(l_out,'<LINE1>',    l_uadRow.uad_line_1);
    l_out := REPLACE(l_out,'<LINE2>',    l_uadRow.uad_line_2);
    l_out := REPLACE(l_out,'<LINE3>',    l_uadRow.uad_line_3);
    l_out := REPLACE(l_out,'<LINE4>',    l_uadRow.uad_line_4);
    l_out := REPLACE(l_out,'<LINE5>',    l_uadRow.uad_line_5);
    l_out := REPLACE(l_out,'<POST_CODE>',l_uadRow.uad_post_code);
    
    IF INSTR(l_out,'<REGION>') != 0 AND
       l_uadRow.uad_rgn_id IS NOT NULL THEN
      l_out := REPLACE(l_out,'<REGION>', t_Regions.getName_PK(l_uadRow.uad_rgn_id));
    END IF;
    
    IF INSTR(l_out,'<REGION_CODE>') != 0 AND
       l_uadRow.uad_rgn_id IS NOT NULL THEN
      l_out := REPLACE(l_out,'<REGION_CODE>', t_Regions.getCode_PK(l_uadRow.uad_rgn_id));
    END IF;
    
    IF INSTR(l_out,'<TOWN>') != 0 AND
       l_uadRow.uad_twn_id IS NOT NULL THEN
      l_out := REPLACE(l_out,'<TOWN>', t_Towns.getName_PK(l_uadRow.uad_twn_id));
    END IF;
    
    IF INSTR(l_out,'<TOWN_POST_CODE>') != 0 AND
       l_uadRow.uad_twn_id IS NOT NULL THEN
      l_out := REPLACE(l_out,'<TOWN_POST_CODE>', t_Towns.getPostCode_PK(l_uadRow.uad_twn_id));
    END IF;
    
    IF INSTR(l_out,'<COUNTRY>') != 0 AND
       l_uadRow.uad_cty_id IS NOT NULL THEN
      l_out := REPLACE(l_out,'<COUNTRY>', t_Countries.getName_PK(l_uadRow.uad_cty_id));
    END IF;
     
     RETURN l_out;
  END getAddressText;
  
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateAddress (p_uadId     IN user_addresses.uad_id%TYPE,
                           p_line1     IN user_addresses.uad_line_1%TYPE,
                           p_line2     IN user_addresses.uad_line_2%TYPE,
                           p_line3     IN user_addresses.uad_line_3%TYPE,
                           p_line4     IN user_addresses.uad_line_4%TYPE,
                           p_line5     IN user_addresses.uad_line_5%TYPE,
                           p_postCode  IN user_addresses.uad_post_code%TYPE,
                           p_ctyId     IN user_addresses.uad_cty_id%TYPE,
                           p_rgnId     IN user_addresses.uad_rgn_id%TYPE,
                           p_twnId     IN user_addresses.uad_twn_id%TYPE,
                           p_adfId     IN user_addresses.uad_adf_id%TYPE) IS
    l_data    t_UserAddresses.g_updateRow       := NULL;
    l_ignore  t_UserAddresses.g_updateIgnoreRow := NULL;
    l_adfRow  address_formats%ROWTYPE;
  BEGIN
    c_Session.validSession;
    
    l_adfRow := t_AddressFormats.getRow_PK(p_pk => p_adfId);

    -- say which columns should remain unchanged
    l_ignore.uad_usr_id       := a_data.gTrue;
    l_ignore.uad_address_type := a_data.gTrue;
    
    -- validate data
    IF p_adfId IS NULL THEN
      null;
    END IF;
    
    IF p_ctyId IS NULL THEN
      null;
    END IF;
    
    IF p_line1 IS NULL AND a_Addresses.valueRequired(p_adfId => p_adfId, p_value => 'LINE1') = a_Data.gTrue THEN
      null;
    END IF;
    
    IF p_line2 IS NULL AND a_Addresses.valueRequired(p_adfId => p_adfId, p_value => 'LINE2') = a_Data.gTrue THEN
      null;
    END IF;
    
    IF p_line3 IS NULL AND a_Addresses.valueRequired(p_adfId => p_adfId, p_value => 'LINE3') = a_Data.gTrue THEN
      null;
    END IF;
    
    IF p_line4 IS NULL AND a_Addresses.valueRequired(p_adfId => p_adfId, p_value => 'LINE4') = a_Data.gTrue THEN
      null;
    END IF;
    
    IF p_line5 IS NULL AND a_Addresses.valueRequired(p_adfId => p_adfId, p_value => 'LINE5') = a_Data.gTrue THEN
      null;
    END IF;
    
    IF p_postCode IS NULL AND a_Addresses.valueRequired(p_adfId => p_adfId, p_value => 'POST_CODE') = a_Data.gTrue THEN
      null;
    END IF;
    
    IF p_rgnId IS NULL AND a_Addresses.valueRequired(p_adfId => p_adfId, p_value => 'REGION') = a_Data.gTrue THEN
      null;
    END IF;
    
    IF p_twnId IS NULL AND a_Addresses.valueRequired(p_adfId => p_adfId, p_value => 'TOWN') = a_Data.gTrue THEN
      null;
    END IF;
    
    -- set the values to be updated
    IF l_adfRow.adf_line_1_name IS NULL THEN
      l_data.uad_line_1 := NULL;
    ELSE
      l_data.uad_line_1 := p_line1;
    END IF;
    
    IF l_adfRow.adf_line_2_name IS NULL THEN
      l_data.uad_line_2 := NULL;
    ELSE
      l_data.uad_line_2 := p_line2;
    END IF;
    
    IF l_adfRow.adf_line_3_name IS NULL THEN
      l_data.uad_line_3 := NULL;
    ELSE
      l_data.uad_line_3 := p_line3;
    END IF;
    
    IF l_adfRow.adf_line_4_name IS NULL THEN
      l_data.uad_line_4 := NULL;
    ELSE
      l_data.uad_line_4 := p_line4;
    END IF;
    
    IF l_adfRow.adf_line_5_name IS NULL THEN
      l_data.uad_line_5 := NULL;
    ELSE
      l_data.uad_line_5 := p_line5;
    END IF;
    
    IF l_adfRow.adf_post_code_name IS NULL THEN
      l_data.uad_post_code := NULL;
    ELSE
      l_data.uad_post_code := p_postCode;
    END IF;
    
    IF l_adfRow.adf_region_name IS NULL THEN
      l_data.uad_rgn_id := NULL;
    ELSE
      l_data.uad_rgn_id := p_rgnId;
    END IF;
    
    IF l_adfRow.adf_town_name IS NULL THEN
      l_data.uad_twn_id := NULL;
    ELSE
      l_data.uad_twn_id := p_twnId;
    END IF;
    
    l_data.uad_adf_id     := p_adfId;
    l_data.uad_cty_id     := p_ctyId;
    
    t_UserAddresses.updateRow(p_pk     => p_uadId,
                              p_row    => l_data,
                              p_ignore => l_ignore);
  END updateAddress;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION valueRequired (p_adfId IN address_formats.adf_id%TYPE,
                          p_value IN VARCHAR2) RETURN VARCHAR2 IS
    l_out      VARCHAR2(1);
    l_adfRow  address_formats%ROWTYPE;
  BEGIN
    c_Session.validSession;
    
    l_out := a_Data.gFalse;
    
    l_adfRow := t_AddressFormats.getRow_PK(p_pk => p_adfId);
    
    -- check to see if LINE1, LINE2, LINE3, LINE4, LINE5, POST_CODE, REGION, TOWN are available and mandatory
    IF p_value                         = 'LINE1'     AND
       l_adfRow.adf_line_1_name          IS NOT NULL AND
       l_adfRow.adf_line_1_mand_ind    = 'Y'         THEN
      l_out := a_Data.gTRUE;
    ELSIF p_value                      = 'LINE2'     AND
       l_adfRow.adf_line_2_name          IS NOT NULL AND
       l_adfRow.adf_line_2_mand_ind    = 'Y'         THEN
      l_out := a_Data.gTRUE;
    ELSIF p_value                      = 'LINE3'     AND
       l_adfRow.adf_line_3_name          IS NOT NULL AND
       l_adfRow.adf_line_3_mand_ind    = 'Y'         THEN
      l_out := a_Data.gTRUE;
    ELSIF p_value                      = 'LINE4'     AND
       l_adfRow.adf_line_4_name          IS NOT NULL AND
       l_adfRow.adf_line_4_mand_ind    = 'Y'         THEN
      l_out := a_Data.gTRUE;
    ELSIF p_value                      = 'LINE5'     AND
       l_adfRow.adf_line_5_name          IS NOT NULL AND
       l_adfRow.adf_line_5_mand_ind    = 'Y'         THEN
      l_out := a_Data.gTRUE;
    ELSIF p_value                      = 'POST_CODE' AND
       l_adfRow.adf_post_code_name       IS NOT NULL AND
       l_adfRow.adf_post_code_mand_ind = 'Y'         THEN
      l_out := a_Data.gTRUE;
    ELSIF p_value                      = 'REGION'    AND
       l_adfRow.adf_region_name          IS NOT NULL AND
       l_adfRow.adf_region_mand_ind    = 'Y'         THEN
      l_out := a_Data.gTRUE;
    ELSIF p_value                      = 'TOWN'      AND
       l_adfRow.adf_town_name            IS NOT NULL AND
       l_adfRow.adf_town_mand_ind      = 'Y'         THEN
      l_out := a_Data.gTRUE;
    END IF;
    
    RETURN l_out;
  END valueRequired;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION insertStub (p_type IN user_addresses.uad_address_type%TYPE) RETURN user_addresses.uad_id%TYPE IS
    l_row     user_addresses%ROWTYPE := NULL;
    l_uadId   INTEGER;
  BEGIN
    c_Session.validSession;
    
    l_row.uad_usr_id       := c_Session.getCurrentUsrId;
    l_row.uad_address_type := p_type;
    t_UserAddresses.insertRow (p_row => l_row, p_newPk => l_uadId);
    
    RETURN l_uadId;
  END insertStub;

END a_Addresses;
/
