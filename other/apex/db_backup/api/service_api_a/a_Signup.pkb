CREATE OR REPLACE PACKAGE a_Signup IS 
/* ----------------------------------------------------------------------------------------------------------
   Service APIs for related to the signup wizard
   
   ---------------------------------------------------------------------------------------------------------- */
                           
 /* Get the signup terms */
  FUNCTION getSignupTerms RETURN terms.ter_text%TYPE;

 /* Create a record stub to hold the signup details */
  PROCEDURE createStub (p_sudId    OUT signup_data.sud_id%TYPE,
                        p_sessCode OUT signup_data.sud_session_code%TYPE);

 /* Update page 1 details 
  PROCEDURE updatePage1 (p_sudId IN signup_data.sud_id%TYPE,
                         p_data  IN tr_signup_page1);
                         
 -- Update page 2 details 
  PROCEDURE updatePage2 (p_sudId IN signup_data.sud_id%TYPE,
                         p_data  IN tr_signup_page2);*/

 /* Set context for signup session */
  PROCEDURE setSignupContext;
  
 /* Validate items on page 1 
  PROCEDURE validatePage1 (p_sudId    IN signup_data.sud_id%TYPE,
                           p_data     IN tr_signup_page1,
                           p_errMsgs OUT tt_data,
                           p_valid   OUT VARCHAR2);
                           
 -- Validate items on page 2 
  PROCEDURE validatePage2 (p_sudId    IN signup_data.sud_id%TYPE,
                           p_data     IN tr_signup_page2,
                           p_errMsgs OUT tt_data,
                           p_valid   OUT VARCHAR2);*/
                           
 /* Validate and update Trade Plan Signup details */
  PROCEDURE validateUpdateTPSignup (p_sudId    IN signup_data.sud_id%TYPE,
                                    p_data     IN tr_tp_signup,
                                    p_errMsgs OUT tt_data,
                                    p_valid   OUT VARCHAR2);
                           
 /* Returns a_Data.g_True if password has been saved, a_Data.g_False if not 
  FUNCTION passwordStored (p_sudId IN signup_data.sud_id%TYPE) RETURN VARCHAR2;*/
  
 /* Is signup active a_Data.g_True or a_Data.g_False */
  FUNCTION signupAllowed RETURN VARCHAR2;
  
 /* Returns the code for the supplied payment method id 
  FUNCTION getPayentMethodCode (p_id IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_code%TYPE;
  
  Returns the id for the supplied payment method code 
  FUNCTION getPayentMethodId (p_code IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_id%TYPE;*/

 /* Send registration email */
  PROCEDURE sendRegEmail (p_sudId IN signup_data.sud_id%TYPE);
  
 /* Create user from signup data */
  PROCEDURE createUser (p_code IN temp_links.tln_link_code%TYPE,
                        p_pw   IN VARCHAR2);
END a_Signup;
/

CREATE OR REPLACE PACKAGE BODY a_Signup IS 
  ----------------------------------------------------------------------------------------------------------------
  -- Forward declarations
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE expireOldSignups (p_user IN signup_data.sud_username%TYPE);

  ----------------------------------------------------------------------------------------------------------------
  -- Public declarations
  ----------------------------------------------------------------------------------------------------------------
 
  FUNCTION getSignupTerms RETURN terms.ter_text%TYPE IS
    l_text  terms.ter_text%TYPE;
  BEGIN
    a_Message.logMsgText(p_message => 'Getting signup terms.',
                         p_data    => a_Data.addTag(NULL,'SESSION_CODE',c_Session.getCurrentSessionCode),
                         p_type    => a_Message.gInfo);
                         
    SELECT ter_text
      INTO l_text
      FROM terms
     WHERE ter_current_ind = 'Y'
       AND ter_type        = 'SIGNUP';
    
    RETURN l_text;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getSignupTerms;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE createStub (p_sudId    OUT signup_data.sud_id%TYPE,
                        p_sessCode OUT signup_data.sud_session_code%TYPE) IS
    l_sudId  signup_data.sud_id%TYPE;
    l_row    signup_data%ROWTYPE;
  BEGIN
    IF c_Session.getCurrentSessionId IS NULL OR -- make sure this request is coming from the signup screen
       NVL(c_Session.getCurrentSignup,'N') = 'N' THEN
      a_Message.logMsgCode (p_code => 'XAPP-00019', -- try signing up again
                            p_type => a_Message.gFatal);
    END IF;
    
    l_row.sud_stored_date             := SYSDATE;
    l_row.sud_session_id              := c_Session.getCurrentSessionId;
    l_row.sud_user_ip                 := owa_util.get_cgi_env('REMOTE_ADDR');
    l_row.sud_terms_accept_ind        := 'N';
    l_row.sud_bil_add_same_as_res_ind := 'Y';
    l_row.sud_accepted_ind            := 'N';
    l_row.sud_email_validated_ind     := 'N';
    l_row.sud_session_code            := api.c_Session.getUniqueSignupSessionCode;
    l_row.sud_expiry_date             := SYSDATE + 14;
  
    t_SignupData.insertRow (p_row   => l_row,
                            p_newPk => l_sudId);
                            
    p_sudId    := l_sudId;
    p_sessCode := l_row.sud_session_code;
    
    a_Message.logMsgText(p_message => 'Created signup stub.',
                         p_data    => a_Data.addTag(a_Data.addTag(NULL,'SESSION_CODE',l_row.sud_session_code),'SUD_ID',l_sudId),
                         p_type    => a_Message.gInfo);
  END createStub;
  
  ----------------------------------------------------------------------------------------------------------------
  /*
  PROCEDURE updatePage1 (p_sudId IN signup_data.sud_id%TYPE,
                         p_data  IN tr_signup_page1) IS
    l_data    t_SignupData.g_updateRow       := NULL;
    l_ignore  t_SignupData.g_updateIgnoreRow := NULL;
    l_errMsgs tt_data;
    l_valid   VARCHAR2(1);
  BEGIN
    IF c_Session.getCurrentSessionId IS NULL OR -- make sure this request is coming from the signup screen
       NVL(c_Session.getCurrentSignup,'N') = 'N' THEN
      a_Message.logMsgCode (p_code => 'XAPP-00019', -- try signing up again
                            p_type => a_Message.gFatal);
    END IF;
    
    validatePage1(p_sudId   => p_sudId,
                  p_data    => p_data,
                  p_errMsgs => l_errMsgs,
                  p_valid   => l_valid);
                  
    IF a_Data.isFalse(l_valid) THEN
      a_Message.logMsgCode (p_code => 'XAPP-00029', -- validation errors not addressed
                            p_type => a_Message.gFatal);
    END IF;
    
    -- say which columns should remain unchanged
    l_ignore.sud_session_id              := a_data.gTrue;
    l_ignore.sud_user_ip                 := a_data.gTrue;
    l_ignore.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignore.sud_bil_add_line_1          := a_data.gTrue;
    l_ignore.sud_bil_add_line_2          := a_data.gTrue;
    l_ignore.sud_bil_add_city            := a_data.gTrue;
    l_ignore.sud_bil_add_post_code       := a_data.gTrue;
    l_ignore.sud_bil_add_cty_id          := a_data.gTrue;
    l_ignore.sud_ppl_id                  := a_data.gTrue;
    l_ignore.sud_pym_id                  := a_data.gTrue;
    l_ignore.sud_cc_no                   := a_data.gTrue;
    l_ignore.sud_cc_expiry_month         := a_data.gTrue;
    l_ignore.sud_cc_expiry_year          := a_data.gTrue;
    l_ignore.sud_cc_cvc                  := a_data.gTrue;
    l_ignore.sud_accepted_ind            := a_data.gTrue;
    l_ignore.sud_accepted_date           := a_data.gTrue;
    l_ignore.sud_usr_id                  := a_data.gTrue;
    l_ignore.sud_email_validated_ind     := a_data.gTrue;
    l_ignore.sud_validation_code         := a_data.gTrue;
    l_ignore.sud_session_code            := a_data.gTrue;
    
    -- set the values to be updated
    l_data.sud_stored_date              := SYSDATE;
    l_data.sud_username                 := p_data.username;
    l_data.sud_first_name               := p_data.first_name;
    l_data.sud_last_name                := p_data.last_name;
    l_data.sud_dob                      := p_data.dob;
    l_data.sud_home_phone_no            := p_data.home_phone_no;
    l_data.sud_mobile_phone_no          := p_data.mobile_phone_no;
    l_data.sud_business_phone_no        := p_data.business_phone_no;
    l_data.sud_email                    := p_data.email;
    l_data.sud_lng_id                   := p_data.lng_id;
    l_data.sud_res_add_line_1           := p_data.res_add_line_1;
    l_data.sud_res_add_line_2           := p_data.res_add_line_2;
    l_data.sud_res_add_city             := p_data.res_add_city;
    l_data.sud_res_add_post_code        := p_data.res_add_post_code;
    l_data.sud_res_add_cty_id           := p_data.res_add_cty_id;
    l_data.sud_terms_accept_ind         := NVL(p_data.terms_accept_ind,'N');
    
    IF p_data.password IS NOT NULL THEN
      l_data.sud_password := api.c_Security.encrypt(p_value => p_data.password, p_key => p_sudId);
    ELSE
      l_ignore.sud_password := a_data.gTrue;
    END IF;
    
    t_SignupData.updateRow(p_pk     => p_sudId,
                           p_row    => l_data,
                           p_ignore => l_ignore);
  END updatePage1;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE updatePage2 (p_sudId IN signup_data.sud_id%TYPE,
                         p_data  IN tr_signup_page2) IS
    l_data    t_SignupData.g_updateRow       := NULL;
    l_ignore  t_SignupData.g_updateIgnoreRow := NULL;
    l_errMsgs tt_data;
    l_valid   VARCHAR2(1);
  BEGIN
    IF c_Session.getCurrentSessionId IS NULL OR -- make sure this request is coming from the signup screen
       NVL(c_Session.getCurrentSignup,'N') = 'N' THEN
      a_Message.logMsgCode (p_code => 'XAPP-00019', -- try signing up again
                            p_type => a_Message.gFatal);
    END IF;
    
    validatePage2(p_sudId   => p_sudId,
                  p_data    => p_data,
                  p_errMsgs => l_errMsgs,
                  p_valid   => l_valid);
                  
    IF a_Data.isFalse(l_valid) THEN
      a_Message.logMsgCode (p_code => 'XAPP-00029', -- validation errors not addressed
                            p_type => a_Message.gFatal);
    END IF;
    
    -- say which columns should remain unchanged
    l_ignore.sud_session_id              := a_data.gTrue;
    l_ignore.sud_user_ip                 := a_data.gTrue;

    l_ignore.sud_usr_id                  := a_data.gTrue;
    l_ignore.sud_email_validated_ind     := a_data.gTrue;
    l_ignore.sud_validation_code         := a_data.gTrue;
    l_ignore.sud_session_code            := a_data.gTrue;
    l_ignore.sud_accepted_ind            := a_data.gTrue;
    l_ignore.sud_accepted_date           := a_data.gTrue;
    l_ignore.sud_username                := a_data.gTrue;
    l_ignore.sud_first_name              := a_data.gTrue;
    l_ignore.sud_last_name               := a_data.gTrue;
    l_ignore.sud_dob                     := a_data.gTrue;
    l_ignore.sud_home_phone_no           := a_data.gTrue;
    l_ignore.sud_mobile_phone_no         := a_data.gTrue;
    l_ignore.sud_business_phone_no       := a_data.gTrue;
    l_ignore.sud_email                   := a_data.gTrue;
    l_ignore.sud_lng_id                  := a_data.gTrue;
    l_ignore.sud_res_add_line_1          := a_data.gTrue;
    l_ignore.sud_res_add_line_2          := a_data.gTrue;
    l_ignore.sud_res_add_city            := a_data.gTrue;
    l_ignore.sud_res_add_post_code       := a_data.gTrue;
    l_ignore.sud_res_add_cty_id          := a_data.gTrue;
    l_ignore.sud_terms_accept_ind        := a_data.gTrue;
    l_ignore.sud_password                := a_data.gTrue;
    
    -- set the values to be updated
    l_data.sud_stored_date               := SYSDATE;
    
    IF NVL(p_data.bil_add_same_as_res_ind,'N') = 'Y' THEN
      l_data.sud_bil_add_same_as_res_ind := 'Y';
      
      l_data.sud_bil_add_line_1          := NULL;
      l_data.sud_bil_add_line_2          := NULL;
      l_data.sud_bil_add_city            := NULL;
      l_data.sud_bil_add_post_code       := NULL;
      l_data.sud_bil_add_cty_id          := NULL;
    ELSE
      l_data.sud_bil_add_same_as_res_ind := 'N';
      
      l_data.sud_bil_add_line_1          := p_data.bil_add_line_1;
      l_data.sud_bil_add_line_2          := p_data.bil_add_line_2;
      l_data.sud_bil_add_city            := p_data.bil_add_city;
      l_data.sud_bil_add_post_code       := p_data.bil_add_post_code;
      l_data.sud_bil_add_cty_id          := p_data.bil_add_cty_id;
    END IF;
    
    l_data.sud_ppl_id                    := p_data.ppl_id;
    l_data.sud_pym_id                    := p_data.pym_id;
    
    IF t_PaymentMethods.getCode_PK(p_pk => p_data.pym_id) = 'CC' THEN 
      IF p_data.cc_no IS NOT NULL THEN
        l_data.sud_cc_no                 := api.c_Security.encrypt(p_value => p_data.cc_no, p_key => p_sudId);
      ELSE
        l_data.sud_cc_no                 := NULL;
      END IF;
    
      IF p_data.cc_expiry_month IS NOT NULL THEN
        l_data.sud_cc_expiry_month       := api.c_Security.encrypt(p_value => p_data.cc_expiry_month, p_key => p_sudId);
      ELSE
        l_data.sud_cc_expiry_month       := NULL;
      END IF;
    
      IF p_data.cc_expiry_year IS NOT NULL THEN
        l_data.sud_cc_expiry_year        := api.c_Security.encrypt(p_value => p_data.cc_expiry_year, p_key => p_sudId);
      ELSE
        l_data.sud_cc_expiry_year        := NULL;
      END IF;
    
      IF p_data.cc_cvc IS NOT NULL THEN
        l_data.sud_cc_cvc                := api.c_Security.encrypt(p_value => p_data.cc_cvc, p_key => p_sudId);
      ELSE
        l_data.sud_cc_cvc                := NULL;
      END IF;
    ELSE
      l_data.sud_cc_no                   := NULL;
      l_data.sud_cc_expiry_month         := NULL;
      l_data.sud_cc_expiry_year          := NULL;
      l_data.sud_cc_cvc                  := NULL;
    END IF;
    
    t_SignupData.updateRow(p_pk     => p_sudId,
                           p_row    => l_data,
                           p_ignore => l_ignore);
  END updatePage2;*/
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE setSignupContext IS
  BEGIN
    api.c_Session.setSignupContext;
  END setSignupContext;
 
  ----------------------------------------------------------------------------------------------------------------
 
/*  PROCEDURE validatePage1 (p_sudId    IN signup_data.sud_id%TYPE,
                           p_data     IN tr_signup_page1,
                           p_errMsgs OUT tt_data,
                           p_valid   OUT VARCHAR2) IS
    l_out        tt_data;
    l_pwErr      message_codes.msc_code%TYPE;
  BEGIN
    p_valid := a_data.gTrue;
    
    IF p_data.username IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'USERNAME',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00020')); -- username must be supplied
      
      p_valid := a_data.gFalse;
    ELSIF a_Data.isTrue(c_Session.userExists(p_newUser => p_data.username)) THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'USERNAME',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00021')); -- user name already in use
                              
      --  ************************************************************************* also check signup usernames
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.lng_id IS NOT NULL AND
       a_Data.isFalse(t_Languages.exists_PK(p_pk => p_data.lng_id)) THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'LANGUAGE',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00022')); -- invalid language
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.res_add_cty_id IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'COUNTRY',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00023'));  -- country must be supplied
      
      p_valid := a_data.gFalse;
    ELSIF a_Data.isFalse(t_Countries.exists_PK(p_pk => p_data.res_add_cty_id)) THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'COUNTRY',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00024')); -- invalid country
      
      p_valid := a_data.gFalse;
    END IF;
    
    -- XAPP-00025 res address must be supplied  p_name => ADDRESS *************************************************************************

    IF NOT (a_Data.isTrue(passwordStored(p_sudId => p_sudId)) AND
            p_data.password IS NULL) THEN
      IF p_data.password IS NULL THEN
        l_out := a_Data.addTag (p_data  => l_out,
                                p_name  => 'PASSWORD',
                                p_value => a_Message.getMessageText(p_code => 'XAPP-00026')); -- password must be supplied
       
        p_valid := a_data.gFalse;
      END IF;
    
      IF p_data.password_conf IS NULL THEN
        l_out := a_Data.addTag (p_data  => l_out,
                                p_name  => 'PASSWORD_CONF',
                                p_value => a_Message.getMessageText(p_code => 'XAPP-00027')); -- password confirmation must be supplied
      
        p_valid := a_data.gFalse;
      END IF;
    
      IF p_data.password      IS NOT NULL AND
         p_data.password_conf IS NOT NULL AND 
         p_data.password      != p_data.password_conf THEN
        l_out := a_Data.addTag (p_data  => l_out,
                                p_name  => 'PASSWORD',
                                p_value => a_Message.getMessageText(p_code => 'XAPP-00028')); -- passwords do not match
      
        p_valid := a_data.gFalse;
      ELSE
        l_pwErr := api.c_Security.passwordPolicy(p_pw => p_data.password); -- password policy checks
        
        IF l_pwErr IS NOT NULL THEN
          l_out := a_Data.addTag (p_data  => l_out,
                                  p_name  => 'PASSWORD',
                                  p_value => l_pwErr); -- various password policy messages
      
          p_valid := a_data.gFalse;
        END IF;
      END IF;
    END IF;
    
    IF p_data.home_phone_no     IS NULL AND
       p_data.mobile_phone_no   IS NULL AND
       p_data.business_phone_no IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'PHONE',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00030')); -- at least 1 phone number must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.email IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'EMAIL',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00031')); -- email must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF NVL(p_data.terms_accept_ind,'N') = 'N' THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'TERMS',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00032')); -- terms must be accepted
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.last_name IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'LASTNAME',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00033')); -- last name must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.first_name IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'FIRSTNAME',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00034')); -- first name must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.dob IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'DOB',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00035')); -- dob must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF a_Data.isFalse(p_valid) THEN
      p_errMsgs := l_out;
    END IF;
  END validatePage1;
 
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE validatePage2 (p_sudId    IN signup_data.sud_id%TYPE,
                           p_data     IN tr_signup_page2,
                           p_errMsgs OUT tt_data,
                           p_valid   OUT VARCHAR2) IS
    l_out        tt_data;
    l_digit      INTEGER;
    l_total      INTEGER := 0;
    l_cctId      INTEGER;
    l_errMsg     message_codes.msc_default_text%TYPE;
    l_expDate    DATE;
  BEGIN
    p_valid := a_data.gTrue;
    
    IF t_PaymentMethods.getCode_PK(p_pk => p_data.pym_id) = 'CC' THEN 
      IF p_data.cc_no IS NULL THEN
        l_out := a_Data.addTag (p_data  => l_out,
                                p_name  => 'CC_NO',
                                p_value => a_Message.getMessageText(p_code => 'XAPP-00040')); -- supply cc number
                                
        p_valid := a_data.gFalse;
      ELSE
        api.a_Cards.identifyCardType(p_CardNo => p_data.cc_no,
                                     p_cctId  => l_cctId,
                                     p_errMsg => l_errMsg);                              
        IF l_errMsg IS NOT NULL THEN
          l_out := a_Data.addTag (p_data  => l_out,
                                  p_name  => 'CC_NO',
                                  p_value => l_errMsg); -- invalid cc type
      
          p_valid := a_data.gFalse;
        ELSIF a_Data.isTrue(t_CardTypes.getLhunValidation_PK(p_pk => l_cctId)) THEN
          IF a_Data.isFalse(api.a_Cards.lhunValidation(p_CardNo => p_data.cc_no)) THEN
            l_out := a_Data.addTag (p_data  => l_out,
                                    p_name  => 'CC_NO',
                                    p_value => a_Message.getMessageText(p_code => 'XAPP-00036')); -- invalid cc number
      
            p_valid := a_data.gFalse;
          END IF;
        END IF;
      END IF;
      
      IF p_data.cc_expiry_year  IS NULL OR
         p_data.cc_expiry_month IS NULL THEN
        l_out := a_Data.addTag (p_data  => l_out,
                                p_name  => 'EXPIRY',
                                p_value => a_Message.getMessageText(p_code => 'XAPP-00044'));  -- expiry date must be supplied
      ELSE
        BEGIN
          l_expDate := LAST_DAY(TO_DATE(LPAD(p_data.cc_expiry_month,2,'0')||p_data.cc_expiry_year,'MMYYYY'));
          
          IF TRUNC(SYSDATE) > l_expDate THEN
            l_out := a_Data.addTag (p_data  => l_out,
                                    p_name  => 'EXPIRY',
                                    p_value => a_Message.getMessageText(p_code => 'XAPP-00045'));  -- card expired
          END IF;
        EXCEPTION
          WHEN INVALID_NUMBER OR
               VALUE_ERROR THEN
            l_out := a_Data.addTag (p_data  => l_out,
                                    p_name  => 'EXPIRY',
                                    p_value => a_Message.getMessageText(p_code => 'XAPP-00047'));  -- invalid expiry date
        END;
      END IF;
      
      IF p_data.cc_cvc IS NULL THEN
        l_out := a_Data.addTag (p_data  => l_out,
                                p_name  => 'EXPIRY',
                                p_value => a_Message.getMessageText(p_code => 'XAPP-00046'));  -- card security code must be supplied
      END IF;
    END IF;
    
    IF a_Data.isFalse(p_data.bil_add_same_as_res_ind) THEN -- billing address different from residential, so needs to be validated
      IF p_data.bil_add_cty_id IS NULL THEN
        l_out := a_Data.addTag (p_data  => l_out,
                                p_name  => 'COUNTRY',
                                p_value => a_Message.getMessageText(p_code => 'XAPP-00041'));  -- country must be supplied
      
        p_valid := a_data.gFalse;
      ELSIF a_Data.isFalse(t_Countries.exists_PK(p_pk => p_data.bil_add_cty_id)) THEN
        l_out := a_Data.addTag (p_data  => l_out,
                                p_name  => 'COUNTRY',
                                p_value => a_Message.getMessageText(p_code => 'XAPP-00042')); -- invalid country
      
        p_valid := a_data.gFalse;
      END IF;
      
      -- XAPP-00043 bil address must be supplied  p_name => ADDRESS *************************************************************************
    END IF;
    
    IF a_Data.isFalse(p_valid) THEN
      p_errMsgs := l_out;
    END IF;
  END validatePage2;*/
 
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE validateUpdateTPSignup (p_sudId    IN signup_data.sud_id%TYPE,
                                    p_data     IN tr_tp_signup,
                                    p_errMsgs OUT tt_data,
                          p_valid   OUT VARCHAR2) IS
    l_out        tt_data;
    l_pwErr      message_codes.msc_code%TYPE;
    l_data       t_SignupData.g_updateRow       := NULL;
    l_ignore     t_SignupData.g_updateIgnoreRow := NULL;
  BEGIN
    IF c_Session.getCurrentSessionId IS NULL OR -- make sure this request is coming from the signup screen
       NVL(c_Session.getCurrentSignup,'N') = 'N' THEN
      a_Message.logMsgCode (p_code => 'XAPP-00019', -- try signing up again
                            p_type => a_Message.gFatal);
    END IF;
    
    a_Message.logMsgText(p_message => 'Validating signup values',
                         p_data    => a_Data.addTag(a_Data.addTag(NULL,'SESSION_CODE',c_Session.getCurrentSessionCode),'SUD_ID',p_sudId),
                         p_type    => a_Message.gInfo);
  
    p_valid := a_data.gTrue;
    
    IF p_data.email IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'EMAIL',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00031')); -- email must be supplied

      p_valid := a_data.gFalse;
    ELSIF a_Data.isTrue(c_Session.userExists(p_newUser => UPPER(p_data.email))) THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'EMAIL',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00021')); -- email already registered
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.lng_id IS NOT NULL AND
       a_Data.isFalse(t_Languages.exists_PK(p_pk => p_data.lng_id)) THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'LANGUAGE',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00022')); -- invalid language
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.res_add_cty_id IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'COUNTRY',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00023'));  -- country must be supplied
      
      p_valid := a_data.gFalse;
    ELSIF a_Data.isFalse(t_Countries.exists_PK(p_pk => p_data.res_add_cty_id)) THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'COUNTRY',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00024')); -- invalid country
      
      p_valid := a_data.gFalse;
    END IF;
    
    -- XAPP-00025 res address must be supplied  p_name => ADDRESS *************************************************************************
   
    IF p_data.home_phone_no     IS NULL AND
       p_data.mobile_phone_no   IS NULL AND
       p_data.business_phone_no IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'PHONE',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00030')); -- at least 1 phone number must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF NVL(p_data.terms_accept_ind,'N') = 'N' THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'TERMS',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00032')); -- terms must be accepted
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.last_name IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'LASTNAME',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00033')); -- last name must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.first_name IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'FIRSTNAME',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00034')); -- first name must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_data.dob IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'DOB',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00035')); -- dob must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF a_Data.isFalse(p_valid) THEN
      p_errMsgs := l_out;
    END IF;
    
    -- store data
    
    -- say which columns should remain unchanged
    l_ignore.sud_session_id              := a_data.gTrue;
    l_ignore.sud_user_ip                 := a_data.gTrue;
    l_ignore.sud_bil_add_same_as_res_ind := a_data.gTrue;
    l_ignore.sud_bil_add_line_1          := a_data.gTrue;
    l_ignore.sud_bil_add_line_2          := a_data.gTrue;
    l_ignore.sud_bil_add_city            := a_data.gTrue;
    l_ignore.sud_bil_add_post_code       := a_data.gTrue;
    l_ignore.sud_bil_add_cty_id          := a_data.gTrue;
    l_ignore.sud_ppl_id                  := a_data.gTrue;
    l_ignore.sud_pym_id                  := a_data.gTrue;
    l_ignore.sud_cc_no                   := a_data.gTrue;
    l_ignore.sud_cc_expiry_month         := a_data.gTrue;
    l_ignore.sud_cc_expiry_year          := a_data.gTrue;
    l_ignore.sud_cc_cvc                  := a_data.gTrue;
    l_ignore.sud_accepted_ind            := a_data.gTrue;
    l_ignore.sud_accepted_date           := a_data.gTrue;
    l_ignore.sud_usr_id                  := a_data.gTrue;
    l_ignore.sud_email_validated_ind     := a_data.gTrue;
    l_ignore.sud_session_code            := a_data.gTrue;
    l_ignore.sud_expiry_date             := a_data.gTrue;
    l_ignore.sud_replaced_date           := a_data.gTrue;
    
    -- set the values to be updated
    l_data.sud_stored_date              := SYSDATE;
    l_data.sud_username                 := UPPER(p_data.email);
    l_data.sud_first_name               := p_data.first_name;
    l_data.sud_last_name                := p_data.last_name;
    l_data.sud_dob                      := p_data.dob;
    l_data.sud_home_phone_no            := p_data.home_phone_no;
    l_data.sud_mobile_phone_no          := p_data.mobile_phone_no;
    l_data.sud_business_phone_no        := p_data.business_phone_no;
    l_data.sud_email                    := p_data.email;
    l_data.sud_lng_id                   := p_data.lng_id;
    l_data.sud_res_add_line_1           := p_data.res_add_line_1;
    l_data.sud_res_add_line_2           := p_data.res_add_line_2;
    l_data.sud_res_add_city             := p_data.res_add_city;
    l_data.sud_res_add_post_code        := p_data.res_add_post_code;
    l_data.sud_res_add_cty_id           := p_data.res_add_cty_id;
    l_data.sud_terms_accept_ind         := NVL(p_data.terms_accept_ind,'N');
    
    t_SignupData.updateRow(p_pk     => p_sudId,
                           p_row    => l_data,
                           p_ignore => l_ignore);

    IF UPPER(p_data.email) IS NOT NULL THEN
      expireOldSignups(p_user => UPPER(p_data.email));
    END IF;
    
    IF p_valid = a_data.gFalse THEN
      a_Message.logMsgText(p_message => 'Signup data validated. Errors found',
                           p_data    => a_Data.addTag(a_Data.addTag(l_out,'SESSION_CODE',c_Session.getCurrentSessionCode),'SUD_ID',p_sudId),
                           p_type    => a_Message.gInfo);
    ELSE
      a_Message.logMsgText(p_message => 'Signup data validated',
                           p_data    => a_Data.addTag(a_Data.addTag(NULL,'SESSION_CODE',c_Session.getCurrentSessionCode),'SUD_ID',p_sudId),
                           p_type    => a_Message.gInfo);
    END IF;
  END validateUpdateTPSignup;
 
  /*--------------------------------------------------------------------------------------------------------------
  
  FUNCTION passwordStored (p_sudId IN signup_data.sud_id%TYPE) RETURN VARCHAR2 IS
  BEGIN
    IF t_SignupData.getPassword_PK(p_pk => p_sudId) IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE 
      RETURN a_data.gFalse;
    END IF;
  END passwordStored;*/
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION signupAllowed RETURN VARCHAR2 IS
  BEGIN
    IF t_CodeValues.getValue_PK('config_values', 'signup_allowed') = 'Y' THEN
      RETURN a_Data.gTrue;
    ELSE
      RETURN a_Data.gFalse;
    END IF;
  END signupAllowed;
  
  ----------------------------------------------------------------------------------------------------------------
  /*
  FUNCTION getPayentMethodCode (p_id IN payment_methods.pym_id%TYPE) RETURN payment_methods.pym_code%TYPE IS
  BEGIN
    RETURN t_PaymentMethods.getCode_PK(p_pk => p_id);
  END getPayentMethodCode;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getPayentMethodId (p_code IN payment_methods.pym_code%TYPE) RETURN payment_methods.pym_id%TYPE IS
  BEGIN
    RETURN t_PaymentMethods.getId_UK(p_uk => p_code);
  END getPayentMethodId;*/
  
  PROCEDURE sendRegEmail (p_sudId IN signup_data.sud_id%TYPE) IS
  BEGIN
    api.c_Security.sendRegMail(p_sudId => p_sudId);
  END sendRegEmail;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE createUser (p_code IN temp_links.tln_link_code%TYPE,
                        p_pw   IN VARCHAR2) IS
  BEGIN
    api.c_Security.createUser(p_code => p_code,
                              p_pw   => p_pw);
  END createUser;
  
 -- ----------------------------------------------------------------------------------------------------------
 -- Private declarations
 -- ----------------------------------------------------------------------------------------------------------
 
  PROCEDURE expireOldSignups (p_user IN signup_data.sud_username%TYPE) IS
  BEGIN
    IF a_Data.isTrue(c_Session.signupExists(p_newUser => p_user)) THEN
      c_Session.expireOldSignups(p_user => p_user);
    END IF;
  END expireOldSignups;
END a_Signup;
/
