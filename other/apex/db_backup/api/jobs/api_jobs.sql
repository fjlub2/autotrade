BEGIN
  -- Define Intervals  
  DBMS_SCHEDULER.CREATE_SCHEDULE (schedule_name   => 'INTERVAL_EVERY_MINUTE',  
                                  start_date      => TRUNC(SYSDATE)+18/24,  
                                  repeat_interval => 'freq=MINUTELY;interval=1',  
                                  comments        => 'Runtime: Every day every minute');


  -- Define Procedures
  DBMS_SCHEDULER.CREATE_PROGRAM (program_name   => 'SEND_MAIL',  
                                 program_type   => 'STORED_PROCEDURE',  
                                 program_action => 'c_Mail.sendMessages',  
                                 enabled        => TRUE,  
                                 comments       => 'Procedure to send queued emails');  


  -- Define Jobs
  DBMS_SCHEDULER.CREATE_JOB (job_name      => 'SEND_MAILS',  
                             program_name  => 'SEND_MAIL',
                             schedule_name => 'INTERVAL_EVERY_MINUTE',  
                             enabled       => TRUE,  
                             auto_drop     => FALSE,  
                             comments      => 'Job to send out queued emails');  
                             
  COMMIT;
END;
/
