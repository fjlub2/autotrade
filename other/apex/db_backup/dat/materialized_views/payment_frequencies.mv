DROP MATERIALIZED VIEW payment_frequencies_mv;

CREATE MATERIALIZED VIEW payment_frequencies_mv
  BUILD IMMEDIATE
  REFRESH FORCE
  ON COMMIT
  AS (SELECT cv_code,
             cv_value,
             cv_name,
             cv_domain
        FROM code_values
       WHERE cv_domain = 'payment_frequencies');

COMMENT ON MATERIALIZED VIEW payment_frequencies_MV IS 'MV table for config_values - data sourced from the code_values table. Constrain against the UK if needed';

COMMENT ON COLUMN payment_frequencies_MV.CV_CODE IS 'Code';
COMMENT ON COLUMN payment_frequencies_MV.CV_VALUE IS 'Value';
COMMENT ON COLUMN payment_frequencies_MV.CV_NAME IS 'Name';
COMMENT ON COLUMN payment_frequencies_MV.CV_DOMAIN IS 'Domain';

ALTER TABLE payment_frequencies_MV ADD CONSTRAINT payment_frequencies_UK UNIQUE (CV_CODE);

CREATE OR REPLACE FORCE VIEW payment_frequencies_vw
(
   CV_CODE,
   CV_VALUE,
   CV_NAME
)
AS
   SELECT
          cv_code,
          cv_value,
          cv_name
     FROM payment_frequencies_mv;

COMMENT ON TABLE payment_frequencies_vw IS 'View of config_values - data sourced from the code_values table via a MV';

COMMENT ON COLUMN payment_frequencies_vw.CV_CODE IS 'Primary Key';
COMMENT ON COLUMN payment_frequencies_vw.CV_VALUE IS 'Value';
COMMENT ON COLUMN payment_frequencies_vw.CV_NAME IS 'Name';
