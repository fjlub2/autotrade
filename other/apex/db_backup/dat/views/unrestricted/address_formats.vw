CREATE OR REPLACE FORCE VIEW address_formats_vw
(
   adf_id,
   adf_cty_id,
   adf_default_ind,
   adf_line_1_name,
   adf_line_2_name,
   adf_line_3_name,
   adf_line_4_name,
   adf_line_5_name,
   adf_post_code_name,
   adf_region_name,
   adf_town_name,
   adf_format,
   adf_name,
   adf_enabled_ind,
   adf_line_1_mand_ind,
   adf_line_2_mand_ind,
   adf_line_3_mand_ind,
   adf_line_4_mand_ind,
   adf_line_5_mand_ind,
   adf_post_code_mand_ind,
   adf_region_mand_ind,
   adf_town_mand_ind
)
AS
   SELECT
          adf_id,
          adf_cty_id,
          adf_default_ind,
          adf_line_1_name,
          adf_line_2_name,
          adf_line_3_name,
          adf_line_4_name,
          adf_line_5_name,
          adf_post_code_name,
          adf_region_name,
          adf_town_name,
          adf_format,
          adf_name,
          adf_enabled_ind,
          adf_line_1_mand_ind,
          adf_line_2_mand_ind,
          adf_line_3_mand_ind,
          adf_line_4_mand_ind,
          adf_line_5_mand_ind,
          adf_post_code_mand_ind,
          adf_region_mand_ind,
          adf_town_mand_ind
     FROM address_formats;

COMMENT ON TABLE address_formats_vw IS 'Table to hold the address requirments for each country';

COMMENT ON COLUMN address_formats_vw.adf_id IS 'Primary Key';
COMMENT ON COLUMN address_formats_vw.adf_cty_id IS 'Country Id';
COMMENT ON COLUMN address_formats_vw.adf_default_ind IS 'Default Address Format? Y/N';
COMMENT ON COLUMN address_formats_vw.adf_line_1_name IS 'Address Line 1 name. Null if not used';
COMMENT ON COLUMN address_formats_vw.adf_line_2_name IS 'Address Line 2 name. Null if not used';
COMMENT ON COLUMN address_formats_vw.adf_line_3_name IS 'Address Line 3 name. Null if not used';
COMMENT ON COLUMN address_formats_vw.adf_line_4_name IS 'Address Line 4 name. Null if not used';
COMMENT ON COLUMN address_formats_vw.adf_line_5_name IS 'Address Line 5 name. Null if not used';
COMMENT ON COLUMN address_formats_vw.adf_post_code_name IS 'Post Code name. Null if not used';
COMMENT ON COLUMN address_formats_vw.adf_region_name IS 'Region name. Null if not used';
COMMENT ON COLUMN address_formats_vw.adf_town_name IS 'Town name. Null if not used';
COMMENT ON COLUMN address_formats_vw.adf_format IS 'Format of address. Surround substitution values with <>. Values avaialble are LINE1-LINE5, POST_CODE, REGION, TOWN, TOWN_POST_CODE, COUNTRY';
COMMENT ON COLUMN address_formats_vw.adf_line_1_mand_ind IS 'Value Mandatory? Y/N';
COMMENT ON COLUMN address_formats_vw.adf_line_2_mand_ind IS 'Value Mandatory? Y/N';
COMMENT ON COLUMN address_formats_vw.adf_line_3_mand_ind IS 'Value Mandatory? Y/N';
COMMENT ON COLUMN address_formats_vw.adf_line_4_mand_ind IS 'Value Mandatory? Y/N';
COMMENT ON COLUMN address_formats_vw.adf_line_5_mand_ind IS 'Value Mandatory? Y/N';
COMMENT ON COLUMN address_formats_vw.adf_post_code_mand_ind IS 'Value Mandatory? Y/N';
COMMENT ON COLUMN address_formats_vw.adf_region_mand_ind IS 'Value Mandatory? Y/N';
COMMENT ON COLUMN address_formats_vw.adf_town_mand_ind IS 'Value Mandatory? Y/N';
COMMENT ON COLUMN address_formats_vw.adf_name IS 'Format Name';
COMMENT ON COLUMN address_formats_vw.adf_enabled_ind IS 'Enabled? Y/N';
