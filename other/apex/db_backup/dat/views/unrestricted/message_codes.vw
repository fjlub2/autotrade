CREATE OR REPLACE FORCE VIEW message_codes_vw
(
   msc_id,
   msc_code,
   msc_default_text,
   msc_constraint_name
)
AS
   SELECT
          msc_id,
          msc_code,
          msc_default_text,
          msc_constraint_name
     FROM message_codes;

COMMENT ON TABLE message_codes_vw IS 'Table to hold message codes';

COMMENT ON COLUMN message_codes_vw.msc_constraint_name IS 'Optional constraint names for Oracle constraint errors';
COMMENT ON COLUMN message_codes_vw.msc_id IS 'Primary Key';
COMMENT ON COLUMN message_codes_vw.msc_code IS 'Message Code';
COMMENT ON COLUMN message_codes_vw.msc_default_text IS 'Default Text to be used in lieu of a translation. Substitute values should be surrounded by <>. Remember that the max length of an oracle error is 512 chars';
