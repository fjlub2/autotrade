CREATE OR REPLACE FORCE VIEW countries_vw
(
   cty_id,
   cty_iso_2_code,
   cty_iso_3_code,
   cty_name,
   cty_enabled_ind,
   cty_adf_id
)
AS
   SELECT
          cty_id,
          cty_iso_2_code,
          cty_iso_3_code,
          cty_name,
          cty_enabled_ind,
          cty_adf_id
     FROM countries;

COMMENT ON TABLE countries_vw IS 'Holds all countries';

COMMENT ON COLUMN countries_vw.cty_id IS 'Primary Key';
COMMENT ON COLUMN countries_vw.cty_iso_2_code IS '2 digit ISO code for country';
COMMENT ON COLUMN countries_vw.cty_iso_3_code IS '3 digit ISO code for country';
COMMENT ON COLUMN countries_vw.cty_name IS 'Country short name';
COMMENT ON COLUMN countries_vw.cty_enabled_ind IS 'Show / hide country? Y/N';
COMMENT ON COLUMN countries_vw.cty_adf_id IS 'Address Format Id';
