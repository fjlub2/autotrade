CREATE OR REPLACE FORCE VIEW payment_plans_vw
(
   ppl_id,
   ppl_desc,
   ppl_amount,
   ppl_frequency,
   ppl_enabled_ind,
   ppl_min_term,
   ppl_ccy_id,
   ppl_display_order
)
AS
   SELECT
          ppl_id,
          ppl_desc,
          ppl_amount,
          ppl_frequency,
          ppl_enabled_ind,
          ppl_min_term,
          ppl_ccy_id,
          ppl_display_order
     FROM payment_plans;

COMMENT ON TABLE payment_plans_vw IS 'Payment terms';

COMMENT ON COLUMN payment_plans_vw.ppl_id IS 'Primary Key';
COMMENT ON COLUMN payment_plans_vw.ppl_desc IS 'Description';
COMMENT ON COLUMN payment_plans_vw.ppl_amount IS 'Plan amount';
COMMENT ON COLUMN payment_plans_vw.ppl_frequency IS 'Payment frequency code';
COMMENT ON COLUMN payment_plans_vw.ppl_enabled_ind IS 'Enabled? Y/N';
COMMENT ON COLUMN payment_plans_vw.ppl_min_term IS 'Minimum required number of months';
COMMENT ON COLUMN payment_plans_vw.ppl_ccy_id IS 'Currency Id';
COMMENT ON COLUMN payment_plans_vw.ppl_display_order IS 'Display order';
