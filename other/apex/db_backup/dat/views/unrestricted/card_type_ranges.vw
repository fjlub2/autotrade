CREATE OR REPLACE FORCE VIEW card_type_ranges_vw
(
   ctr_id,
   ctr_cct_id,
   ctr_iin_from,
   ctr_iin_to,
   ctr_length_from,
   ctr_length_to,
   ctr_check_order,
   ctr_iin_length
)
AS
   SELECT
          ctr_id,
          ctr_cct_id,
          ctr_iin_from,
          ctr_iin_to,
          ctr_length_from,
          ctr_length_to,
          ctr_check_order,
          ctr_iin_length
     FROM card_type_ranges;

COMMENT ON TABLE card_type_ranges_vw IS 'Used to identify the card type from the supplied card number.';

COMMENT ON COLUMN card_type_ranges_vw.ctr_id IS 'Primary Key';
COMMENT ON COLUMN card_type_ranges_vw.ctr_cct_id IS 'Card Type FK';
COMMENT ON COLUMN card_type_ranges_vw.ctr_iin_from IS 'IIN From';
COMMENT ON COLUMN card_type_ranges_vw.ctr_iin_to IS 'IIN To';
COMMENT ON COLUMN card_type_ranges_vw.ctr_length_from IS 'Card Number Length From';
COMMENT ON COLUMN card_type_ranges_vw.ctr_length_to IS 'Card Number Length To';
COMMENT ON COLUMN card_type_ranges_vw.ctr_check_order IS 'Use this to check certain card types before others. E.g. Visa Electron should be checked before Visa';
COMMENT ON COLUMN card_type_ranges_vw.ctr_iin_length IS '';
