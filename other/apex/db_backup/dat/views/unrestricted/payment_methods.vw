CREATE OR REPLACE FORCE VIEW payment_methods_vw
(
   pym_id,
   pym_code,
   pym_name,
   pym_enabled_ind,
   pym_icon
)
AS
   SELECT
          pym_id,
          pym_code,
          pym_name,
          pym_enabled_ind,
          pym_icon
     FROM payment_methods;

COMMENT ON TABLE payment_methods_vw IS 'Payment Methods';

COMMENT ON COLUMN payment_methods_vw.pym_id IS 'Primary Key';
COMMENT ON COLUMN payment_methods_vw.pym_code IS 'Unique Code - CC = Credit Card, PP = Pay Pal, BT = Bank Transfer';
COMMENT ON COLUMN payment_methods_vw.pym_name IS 'Method Name';
COMMENT ON COLUMN payment_methods_vw.pym_enabled_ind IS 'Is this method enabled? Y/N';
COMMENT ON COLUMN payment_methods_vw.pym_icon IS 'Icon to be displayed next to payment method';
