CREATE OR REPLACE FORCE VIEW languages_vw
(
   lng_id,
   lng_code,
   lng_name,
   lng_native_name,
   lng_enabled_ind
)
AS
   SELECT
          lng_id,
          lng_code,
          lng_name,
          lng_native_name,
          lng_enabled_ind
     FROM languages
    WHERE lng_enabled_ind = 'Y';

COMMENT ON TABLE languages_vw IS 'Table to hold all language codes';

COMMENT ON COLUMN languages_vw.lng_id IS 'Primary Key';
COMMENT ON COLUMN languages_vw.lng_code IS 'Language Code';
COMMENT ON COLUMN languages_vw.lng_name IS 'English name for language';
COMMENT ON COLUMN languages_vw.lng_native_name IS 'Native name for language';
COMMENT ON COLUMN languages_vw.lng_enabled_ind IS 'Enabled? Y/N. Set to Y only when translations have been created';
