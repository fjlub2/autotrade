CREATE OR REPLACE FORCE VIEW message_log_1_vw
(
   mss_id,
   mss_uss_id,
   mss_date,
   mss_type,
   mss_message,
   mss_stack,
   mss_trace,
   mss_task_id,
   mss_data
)
AS
   SELECT
          mss_id,
          mss_uss_id,
          mss_date,
          mss_type,
          mss_message,
          mss_stack,
          mss_trace,
          mss_task_id,
          mss_data
     FROM message_log_1;

COMMENT ON TABLE message_log_1_vw IS 'Message Log table no 1. No 2 will be used while clearing out number 1.';

COMMENT ON COLUMN message_log_1_vw.mss_id IS 'Primary Key';
COMMENT ON COLUMN message_log_1_vw.mss_uss_id IS 'User Session Id. Always pass unless the supplied session is invalid';
COMMENT ON COLUMN message_log_1_vw.mss_date IS 'Message Timestamp';
COMMENT ON COLUMN message_log_1_vw.mss_type IS 'Message Type - see a_Message for valid types';
COMMENT ON COLUMN message_log_1_vw.mss_message IS 'Message text - restricted to max length of raise application_error';
COMMENT ON COLUMN message_log_1_vw.mss_stack IS 'Error Stack';
COMMENT ON COLUMN message_log_1_vw.mss_data IS '';
COMMENT ON COLUMN message_log_1_vw.mss_trace IS 'Error Backtrace';
COMMENT ON COLUMN message_log_1_vw.mss_task_id IS 'Optional task Id, allows you to tie multiple messages back to a particular task that the user was running';
