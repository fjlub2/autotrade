CREATE OR REPLACE FORCE VIEW card_types_vw
(
   cct_id,
   cct_network_name,
   cct_active_ind,
   cct_lhun_validation,
   cct_allowed_ind,
   cct_icon
)
AS
   SELECT
          cct_id,
          cct_network_name,
          cct_active_ind,
          cct_lhun_validation,
          cct_allowed_ind,
          cct_icon
     FROM card_types;

COMMENT ON TABLE card_types_vw IS 'Credit Card Types';

COMMENT ON COLUMN card_types_vw.cct_id IS 'Primary Key';
COMMENT ON COLUMN card_types_vw.cct_network_name IS 'Bank / Network name';
COMMENT ON COLUMN card_types_vw.cct_active_ind IS 'Is this card type still in use? Y/N';
COMMENT ON COLUMN card_types_vw.cct_allowed_ind IS 'Is this card type accepted by us? Y/N';
COMMENT ON COLUMN card_types_vw.cct_lhun_validation IS 'Should Lhun Validation be applied to the card number? Y/N';
COMMENT ON COLUMN card_types_vw.cct_icon IS 'Icon for this card type. Icons should be loaded into the APEX workspace';
