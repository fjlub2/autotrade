CREATE OR REPLACE FORCE VIEW regions_vw
(
   rgn_id,
   rgn_cty_id,
   rgn_code,
   rgn_iso_code,
   rgn_name,
   rgn_enabled_ind
)
AS
   SELECT
          rgn_id,
          rgn_cty_id,
          rgn_code,
          rgn_iso_code,
          rgn_name,
          rgn_enabled_ind
     FROM regions;

COMMENT ON TABLE regions_vw IS 'Table to hold regions / territories';

COMMENT ON COLUMN regions_vw.rgn_id IS 'Primary Key';
COMMENT ON COLUMN regions_vw.rgn_cty_id IS 'Country ID';
COMMENT ON COLUMN regions_vw.rgn_code IS 'Region Code';
COMMENT ON COLUMN regions_vw.rgn_iso_code IS 'ISO code for region';
COMMENT ON COLUMN regions_vw.rgn_name IS 'Region Name';
COMMENT ON COLUMN regions_vw.rgn_enabled_ind IS 'Show / Hide Region? Y/N';
