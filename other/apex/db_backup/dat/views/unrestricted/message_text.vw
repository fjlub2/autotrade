CREATE OR REPLACE FORCE VIEW message_text_vw
(
   mst_id,
   mst_msc_id,
   mst_lng_id,
   mst_text
)
AS
   SELECT
          mst_id,
          mst_msc_id,
          mst_lng_id,
          mst_text
     FROM message_text;

COMMENT ON TABLE message_text_vw IS 'Table to hold translations for all message codes';

COMMENT ON COLUMN message_text_vw.mst_text IS 'Translated Message. Substitute values should be surrounded by <>';
COMMENT ON COLUMN message_text_vw.mst_id IS 'Primary Key';
COMMENT ON COLUMN message_text_vw.mst_msc_id IS 'Message Id';
COMMENT ON COLUMN message_text_vw.mst_lng_id IS 'Language Id';
