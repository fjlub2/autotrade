CREATE OR REPLACE FORCE VIEW currencies_vw
(
   ccy_id,
   ccy_code,
   ccy_symbol,
   ccy_decimals,
   ccy_name,
   ccy_format
)
AS
   SELECT
          ccy_id,
          ccy_code,
          ccy_symbol,
          ccy_decimals,
          ccy_name,
          ccy_format
     FROM currencies;

COMMENT ON TABLE currencies_vw IS 'Currency details';

COMMENT ON COLUMN currencies_vw.ccy_id IS 'Primary Key';
COMMENT ON COLUMN currencies_vw.ccy_code IS 'Currency Code';
COMMENT ON COLUMN currencies_vw.ccy_symbol IS 'Currency Symbol';
COMMENT ON COLUMN currencies_vw.ccy_decimals IS 'Number of decimals';
COMMENT ON COLUMN currencies_vw.ccy_name IS 'Currency Name';
COMMENT ON COLUMN currencies_vw.ccy_format IS '';
