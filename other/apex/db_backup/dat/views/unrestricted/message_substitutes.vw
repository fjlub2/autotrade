CREATE OR REPLACE FORCE VIEW message_substitutes_vw
(
   msu_id,
   msu_msc_id,
   msu_code,
   msu_name
)
AS
   SELECT
          msu_id,
          msu_msc_id,
          msu_code,
          msu_name
     FROM message_substitutes;

COMMENT ON TABLE message_substitutes_vw IS 'Table that lists all the substitution values that can be used for a message';

COMMENT ON COLUMN message_substitutes_vw.msu_id IS 'Primary Key';
COMMENT ON COLUMN message_substitutes_vw.msu_msc_id IS 'Message ID';
COMMENT ON COLUMN message_substitutes_vw.msu_code IS 'Substitute Code';
COMMENT ON COLUMN message_substitutes_vw.msu_name IS 'Substitute Name';
