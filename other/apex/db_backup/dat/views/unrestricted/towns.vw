CREATE OR REPLACE FORCE VIEW towns_vw
(
   twn_id,
   twn_cty_id,
   twn_rgn_id,
   twn_name,
   twn_post_code,
   twn_lat,
   twn_lon,
   twn_enabled_ind
)
AS
   SELECT
          twn_id,
          twn_cty_id,
          twn_rgn_id,
          twn_name,
          twn_post_code,
          twn_lat,
          twn_lon,
          twn_enabled_ind
     FROM towns;

COMMENT ON TABLE towns_vw IS 'Table to hold town, cities, suburbs, etc...';

COMMENT ON COLUMN towns_vw.twn_id IS 'Primary Key';
COMMENT ON COLUMN towns_vw.twn_cty_id IS 'Country Id';
COMMENT ON COLUMN towns_vw.twn_rgn_id IS 'Optional Region Id';
COMMENT ON COLUMN towns_vw.twn_name IS 'Town Name';
COMMENT ON COLUMN towns_vw.twn_post_code IS 'Post / Zip Code';
COMMENT ON COLUMN towns_vw.twn_lat IS 'Latitude';
COMMENT ON COLUMN towns_vw.twn_lon IS 'Longitude';
COMMENT ON COLUMN towns_vw.twn_enabled_ind IS 'Show town? Y/N';
