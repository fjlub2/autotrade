CREATE OR REPLACE FORCE VIEW terms_vw
(
   ter_id,
   ter_text,
   ter_date,
   ter_current_ind,
   ter_type
)
AS
   SELECT
          ter_id,
          ter_text,
          ter_date,
          ter_current_ind,
          ter_type
     FROM terms;

COMMENT ON TABLE terms_vw IS 'Table to hold all terms that users need to agree to - there should only be one current term per type';

COMMENT ON COLUMN terms_vw.ter_id IS 'Primary Key';
COMMENT ON COLUMN terms_vw.ter_text IS 'Terms';
COMMENT ON COLUMN terms_vw.ter_date IS 'Created Date';
COMMENT ON COLUMN terms_vw.ter_current_ind IS 'Currently Active? Y/N';
COMMENT ON COLUMN terms_vw.ter_type IS 'Term Type';
