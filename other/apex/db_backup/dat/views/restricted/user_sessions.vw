CREATE OR REPLACE FORCE VIEW user_sessions_vw
(
   uss_id,
   uss_usr_id,
   uss_session_start,
   uss_session_id,
   uss_user_ip,
   uss_session_end,
   uss_lng_id
)
AS
   SELECT
          uss_id,
          uss_usr_id,
          uss_session_start,
          uss_session_id,
          uss_user_ip,
          uss_session_end,
          uss_lng_id
     FROM user_sessions
    WHERE api.c_VPD.userVPD_XE(uss_usr_id) = 'Y';

COMMENT ON TABLE user_sessions_vw IS 'Table to hold details of user sessions - this table and child tables should be cleared out reguarly';

COMMENT ON COLUMN user_sessions_vw.uss_lng_id IS 'Browser language. Only set if found and language is enabled.';
COMMENT ON COLUMN user_sessions_vw.uss_id IS 'Primary Key';
COMMENT ON COLUMN user_sessions_vw.uss_usr_id IS 'User Id';
COMMENT ON COLUMN user_sessions_vw.uss_session_start IS 'Login Timestamp';
COMMENT ON COLUMN user_sessions_vw.uss_session_id IS 'Apex Session Id';
COMMENT ON COLUMN user_sessions_vw.uss_user_ip IS 'User IP address';
COMMENT ON COLUMN user_sessions_vw.uss_session_end IS 'Logout timestamp - wont be populated when a session times out';
