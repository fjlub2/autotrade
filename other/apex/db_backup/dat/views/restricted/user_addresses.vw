CREATE OR REPLACE FORCE VIEW user_addresses_vw
(
   uad_id,
   uad_usr_id,
   uad_line_1,
   uad_line_2,
   uad_line_3,
   uad_line_4,
   uad_line_5,
   uad_post_code,
   uad_cty_id,
   uad_rgn_id,
   uad_twn_id,
   uad_adf_id,
   uad_address_type
)
AS
   SELECT
          uad_id,
          uad_usr_id,
          uad_line_1,
          uad_line_2,
          uad_line_3,
          uad_line_4,
          uad_line_5,
          uad_post_code,
          uad_cty_id,
          uad_rgn_id,
          uad_twn_id,
          uad_adf_id,
          uad_address_type
     FROM user_addresses
    WHERE api.c_VPD.userVPD_XE (uad_usr_id) = 'Y';

COMMENT ON TABLE user_addresses_vw IS 'Table to hold user address details. Different columns will need to be populated depending on the country and if the address has been manually entered or not - see the ADDRESS_FORMATS table.';

COMMENT ON COLUMN user_addresses_vw.uad_address_type IS 'Address Type Code';
COMMENT ON COLUMN user_addresses_vw.uad_id IS 'Primary Key';
COMMENT ON COLUMN user_addresses_vw.uad_usr_id IS 'User Id';
COMMENT ON COLUMN user_addresses_vw.uad_line_1 IS 'Address Line 1';
COMMENT ON COLUMN user_addresses_vw.uad_line_2 IS 'Address Line 2';
COMMENT ON COLUMN user_addresses_vw.uad_line_3 IS 'Address Line 3';
COMMENT ON COLUMN user_addresses_vw.uad_line_4 IS 'Address Line 4';
COMMENT ON COLUMN user_addresses_vw.uad_line_5 IS 'Address Line 5';
COMMENT ON COLUMN user_addresses_vw.uad_post_code IS 'Post Code';
COMMENT ON COLUMN user_addresses_vw.uad_cty_id IS 'Country Id';
COMMENT ON COLUMN user_addresses_vw.uad_rgn_id IS 'Region Id';
COMMENT ON COLUMN user_addresses_vw.uad_twn_id IS 'Town Id';
COMMENT ON COLUMN user_addresses_vw.uad_adf_id IS 'Address Format Id';
