CREATE OR REPLACE FORCE VIEW temp_links_vw
(
   tln_id,
   tln_usr_id,
   tln_link_code,
   tln_expiry_date,
   tln_used_ind,
   tln_used_date,
   tln_type,
   tln_sud_id
)
AS
   SELECT
          tln_id,
          tln_usr_id,
          tln_link_code,
          tln_expiry_date,
          tln_used_ind,
          tln_used_date,
          tln_type,
          tln_sud_id
     FROM temp_links
    WHERE api.c_VPD.userVPD_XE (tln_usr_id) = 'Y';

COMMENT ON TABLE temp_links_vw IS 'Table to hold temporary links. E.g. for password resets via email';

COMMENT ON COLUMN temp_links_vw.tln_id IS 'Primary Key';
COMMENT ON COLUMN temp_links_vw.tln_usr_id IS 'User that the link relates to';
COMMENT ON COLUMN temp_links_vw.tln_link_code IS 'Code that is passed in the url of the link. Unique.';
COMMENT ON COLUMN temp_links_vw.tln_expiry_date IS 'When does the link expire';
COMMENT ON COLUMN temp_links_vw.tln_used_ind IS 'Has the link been successfully used. It can only be used once';
COMMENT ON COLUMN temp_links_vw.tln_used_date IS 'When was the link successfully used';
COMMENT ON COLUMN temp_links_vw.tln_type IS 'Type of link, e.g. password reset';
COMMENT ON COLUMN temp_links_vw.tln_usr_id IS 'Signup Request that the link relates to';
