CREATE OR REPLACE FORCE VIEW users_vw
(
   usr_id,
   usr_username,
   usr_first_name,
   usr_last_name,
   usr_company,
   usr_email,
   usr_status,
   usr_password,
   usr_pwd_tries,
   usr_change_pw_on_logon,
   usr_lng_id,
   usr_vision,
   usr_objective,
   usr_bill_add_same_as_res_ind,
   usr_dob,
   usr_mobile_no,
   usr_business_no,
   usr_home_no,
   usr_message_level
)
AS
   SELECT
          usr_id,
          usr_username,
          usr_first_name,
          usr_last_name,
          usr_company,
          usr_email,
          usr_status,
          usr_password,
          usr_pwd_tries,
          usr_change_pw_on_logon,
          usr_lng_id,
          usr_vision,
          usr_objective,
          usr_bill_add_same_as_res_ind,
          usr_dob,
          usr_mobile_no,
          usr_business_no,
          usr_home_no,
          usr_message_level
     FROM users
    WHERE api.c_VPD.userVPD_XE (usr_id) = 'Y';

COMMENT ON TABLE users_vw IS 'Main user table';

COMMENT ON COLUMN users_vw.usr_bill_add_same_as_res_ind IS 'Is the Billing Address the same as the Residential Address';
COMMENT ON COLUMN users_vw.usr_dob IS 'Date of Birth';
COMMENT ON COLUMN users_vw.usr_mobile_no IS 'Mobile Phone Number';
COMMENT ON COLUMN users_vw.usr_business_no IS 'Business Phone Number';
COMMENT ON COLUMN users_vw.usr_home_no IS 'Home Phone Number';
COMMENT ON COLUMN users_vw.usr_id IS 'Primary Key';
COMMENT ON COLUMN users_vw.usr_username IS 'Uppercase username';
COMMENT ON COLUMN users_vw.usr_first_name IS 'First Name';
COMMENT ON COLUMN users_vw.usr_last_name IS 'First Name';
COMMENT ON COLUMN users_vw.usr_company IS 'Company Name';
COMMENT ON COLUMN users_vw.usr_email IS 'Email Address';
COMMENT ON COLUMN users_vw.usr_status IS 'User Status = (A)ctive, (C)losed, (N)ot paid';
COMMENT ON COLUMN users_vw.usr_password IS 'Users password - not decrytable';
COMMENT ON COLUMN users_vw.usr_pwd_tries IS 'Number of password tries login not allowed after 3 tries';
COMMENT ON COLUMN users_vw.usr_change_pw_on_logon IS 'If Y, user will be forced to change their password the next time they login';
COMMENT ON COLUMN users_vw.usr_lng_id IS 'Optional language id. If null, the language from the browser will be used.';
COMMENT ON COLUMN users_vw.usr_vision IS 'Users trading vision and intention';
COMMENT ON COLUMN users_vw.usr_objective IS 'Users trading objective';
COMMENT ON COLUMN users_vw.usr_message_level IS 'Message logging level for this user. Set to null to use level from system config. Changes picked up at login.';


