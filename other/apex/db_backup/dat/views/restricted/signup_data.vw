CREATE OR REPLACE FORCE VIEW signup_data_vw
(
   sud_id,
   sud_stored_date,
   sud_session_id,
   sud_user_ip,
   sud_username,
   sud_first_name,
   sud_last_name,
   sud_dob,
   sud_home_phone_no,
   sud_mobile_phone_no,
   sud_business_phone_no,
   sud_email,
   sud_lng_id,
   sud_res_add_line_1,
   sud_res_add_line_2,
   sud_res_add_city,
   sud_res_add_post_code,
   sud_res_add_cty_id,
   sud_terms_accept_ind,
   sud_bil_add_same_as_res_ind,
   sud_bil_add_line_1,
   sud_bil_add_line_2,
   sud_bil_add_city,
   sud_bil_add_post_code,
   sud_bil_add_cty_id,
   sud_ppl_id,
   sud_pym_id,
   sud_cc_no,
   sud_cc_expiry_month,
   sud_cc_expiry_year,
   sud_cc_cvc,
   sud_accepted_ind,
   sud_accepted_date,
   sud_usr_id,
   sud_email_validated_ind,
   sud_session_code,
   sud_expiry_date,
   sud_replaced_date
)
AS
   SELECT
          sud_id,
          sud_stored_date,
          sud_session_id,
          sud_user_ip,
          sud_username,
          sud_first_name,
          sud_last_name,
          sud_dob,
          sud_home_phone_no,
          sud_mobile_phone_no,
          sud_business_phone_no,
          sud_email,
          sud_lng_id,
          sud_res_add_line_1,
          sud_res_add_line_2,
          sud_res_add_city,
          sud_res_add_post_code,
          sud_res_add_cty_id,
          sud_terms_accept_ind,
          sud_bil_add_same_as_res_ind,
          sud_bil_add_line_1,
          sud_bil_add_line_2,
          sud_bil_add_city,
          sud_bil_add_post_code,
          sud_bil_add_cty_id,
          sud_ppl_id,
          sud_pym_id,
          sud_cc_no,
          sud_cc_expiry_month,
          sud_cc_expiry_year,
          sud_cc_cvc,
          sud_accepted_ind,
          sud_accepted_date,
          sud_usr_id,
          sud_email_validated_ind,
          sud_session_code,
          sud_expiry_date,
          sud_replaced_date
     FROM signup_data
    WHERE (sud_usr_id IS NOT NULL AND
           api.c_VPD.userVPD_XE (sud_usr_id) = 'Y')
       OR (sud_usr_id IS NULL AND
           api.c_VPD.signupVPD_XE (sud_session_id, sud_session_code) = 'Y');

COMMENT ON TABLE signup_data_vw IS 'Table to hold signup data up to the point even if user only progressed partway through signup. Few constraints as we want to store data regardless of validation';

COMMENT ON COLUMN signup_data_vw.sud_expiry_date IS 'Date this signup request expired';
COMMENT ON COLUMN signup_data_vw.sud_replaced_date IS 'Date this signup request was replaced by a new one';
COMMENT ON COLUMN signup_data_vw.sud_id IS 'Primary Key';
COMMENT ON COLUMN signup_data_vw.sud_stored_date IS 'Last Saved Date';
COMMENT ON COLUMN signup_data_vw.sud_session_id IS 'Session Id';
COMMENT ON COLUMN signup_data_vw.sud_user_ip IS 'User IP Address';
COMMENT ON COLUMN signup_data_vw.sud_username IS 'Username';
COMMENT ON COLUMN signup_data_vw.sud_first_name IS 'First Name';
COMMENT ON COLUMN signup_data_vw.sud_last_name IS 'Last Name';
COMMENT ON COLUMN signup_data_vw.sud_dob IS 'Date of Birth';
COMMENT ON COLUMN signup_data_vw.sud_home_phone_no IS 'Home Phone Number';
COMMENT ON COLUMN signup_data_vw.sud_mobile_phone_no IS 'Mobile Phone Number';
COMMENT ON COLUMN signup_data_vw.sud_business_phone_no IS 'Business Phone Number';
COMMENT ON COLUMN signup_data_vw.sud_email IS 'Email Address';
COMMENT ON COLUMN signup_data_vw.sud_lng_id IS 'Language Id';
COMMENT ON COLUMN signup_data_vw.sud_res_add_line_1 IS 'Residential Address Line 1';
COMMENT ON COLUMN signup_data_vw.sud_res_add_line_2 IS 'Residential Address Line 2';
COMMENT ON COLUMN signup_data_vw.sud_res_add_city IS 'Residential Address Town / City';
COMMENT ON COLUMN signup_data_vw.sud_res_add_post_code IS 'Residential Address Post Code';
COMMENT ON COLUMN signup_data_vw.sud_res_add_cty_id IS 'Residential Address Country ID';
COMMENT ON COLUMN signup_data_vw.sud_terms_accept_ind IS 'Has User Accepted the Terms? Y/N';
COMMENT ON COLUMN signup_data_vw.sud_bil_add_same_as_res_ind IS 'Billing Address Same as Residential? Y/N';
COMMENT ON COLUMN signup_data_vw.sud_bil_add_line_1 IS 'Billingl Address Line 1';
COMMENT ON COLUMN signup_data_vw.sud_bil_add_line_2 IS 'Billing Address Line 2';
COMMENT ON COLUMN signup_data_vw.sud_bil_add_city IS 'Billing Address Town / City';
COMMENT ON COLUMN signup_data_vw.sud_bil_add_post_code IS 'Billingl Address Post Code';
COMMENT ON COLUMN signup_data_vw.sud_bil_add_cty_id IS 'Billing Address Country ID';
COMMENT ON COLUMN signup_data_vw.sud_ppl_id IS 'Payment Plan Id';
COMMENT ON COLUMN signup_data_vw.sud_pym_id IS 'Payment Method Id';
COMMENT ON COLUMN signup_data_vw.sud_cc_no IS 'Credit Card Number - encrypted';
COMMENT ON COLUMN signup_data_vw.sud_cc_expiry_month IS 'Credit Card Expiry Month - encrypted';
COMMENT ON COLUMN signup_data_vw.sud_cc_expiry_year IS 'Credit Card Expiry Year - encrypted';
COMMENT ON COLUMN signup_data_vw.sud_cc_cvc IS 'Credit Card Security Code - cleared after first collection.';
COMMENT ON COLUMN signup_data_vw.sud_accepted_ind IS 'User Accepted? Y/N';
COMMENT ON COLUMN signup_data_vw.sud_accepted_date IS 'User Accepted Date';
COMMENT ON COLUMN signup_data_vw.sud_usr_id IS 'Populated with the related user id after the user record has been created';
COMMENT ON COLUMN signup_data_vw.sud_email_validated_ind IS 'Email address verified';
COMMENT ON COLUMN signup_data_vw.sud_session_code IS 'Code used to validate signup session';
