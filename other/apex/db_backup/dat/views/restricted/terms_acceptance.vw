CREATE OR REPLACE FORCE VIEW terms_acceptance_vw
(
   tac_id,
   tac_ter_id,
   tac_usr_id,
   tac_accepted_ind,
   tac_date
)
AS
   SELECT
          tac_id,
          tac_ter_id,
          tac_usr_id,
          tac_accepted_ind,
          tac_date
     FROM terms_acceptance
    WHERE api.c_VPD.userVPD_XE (tac_usr_id) = 'Y';

COMMENT ON TABLE terms_acceptance_vw IS '';

COMMENT ON COLUMN terms_acceptance_vw.tac_id IS 'Primary Key';
COMMENT ON COLUMN terms_acceptance_vw.tac_ter_id IS 'FK to Terms table';
COMMENT ON COLUMN terms_acceptance_vw.tac_usr_id IS 'FK to Users Table';
COMMENT ON COLUMN terms_acceptance_vw.tac_accepted_ind IS 'User Accepted? Y/N';
COMMENT ON COLUMN terms_acceptance_vw.tac_date IS 'User Accepted Date';
