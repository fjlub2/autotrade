CREATE OR REPLACE TRIGGER uad_vpd
    BEFORE INSERT OR UPDATE OR DELETE ON user_addresses
    REFERENCING NEW AS New OLD AS Old
    FOR EACH ROW
DECLARE
  not_allowed EXCEPTION;
  PRAGMA EXCEPTION_INIT(not_allowed, -1031);
BEGIN
  IF UPDATING OR DELETING THEN
    IF api.c_VPD.userVPD_XE (:OLD.uad_usr_id) != 'Y' THEN
      RAISE not_allowed;
    END IF;
  END IF;
  
  IF UPDATING OR INSERTING THEN
    IF api.c_VPD.userVPD_XE (:NEW.uad_usr_id) != 'Y' THEN
      RAISE not_allowed;
    END IF;
  END IF;
END;
/