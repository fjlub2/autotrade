CREATE OR REPLACE TRIGGER usr_vpd
    BEFORE INSERT OR UPDATE OR DELETE ON users
    REFERENCING NEW AS New OLD AS Old
    FOR EACH ROW
DECLARE
  not_allowed EXCEPTION;
  PRAGMA EXCEPTION_INIT(not_allowed, -1031);
BEGIN
  IF UPDATING OR DELETING THEN
    IF api.c_VPD.userVPD_XE (:OLD.usr_id) != 'Y' THEN
      RAISE not_allowed;
    END IF;
  END IF;
  
  IF UPDATING OR INSERTING THEN
    IF api.c_VPD.userVPD_XE (:NEW.usr_id) != 'Y' THEN
      RAISE not_allowed;
    END IF;
  END IF;
END;
/