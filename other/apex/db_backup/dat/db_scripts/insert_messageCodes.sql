DECLARE
  l_mscId   message_codes.msc_id%TYPE;
  
  FUNCTION insert_msc(p_mscCode IN message_codes.msc_code%TYPE,
                      p_mscText IN message_codes.msc_default_text%TYPE,
                      p_mscCon  IN message_codes.msc_constraint_name%TYPE DEFAULT NULL) RETURN message_codes.msc_id%TYPE IS
    l_newMscId   message_codes.msc_id%TYPE;
  BEGIN
    SELECT msc_id
      INTO l_newMscId
      FROM message_codes
     WHERE msc_code = p_mscCode;
     
    UPDATE message_codes
       SET msc_default_text    = p_mscText,
           msc_constraint_name = p_mscCon
     WHERE msc_id = l_newMscId;
     
    DELETE message_substitutes
     WHERE msu_msc_id = l_newMscId;
    
    RETURN l_newMscId;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      INSERT
        INTO message_codes (msc_code,
                            msc_default_text,
                            msc_constraint_name)
                    VALUES (p_mscCode,
                            p_mscText,
                            p_mscCon)
                  RETURNING msc_id
                       INTO l_newMscId;
                       
      DELETE message_substitutes
       WHERE msu_msc_id = l_newMscId;
                       
      RETURN l_newMscId;
  END insert_msc;
  
  PROCEDURE insert_msu(p_mscId   IN message_codes.msc_id%TYPE,
                       p_msuCode IN message_substitutes.msu_code%TYPE,
                       p_msuName IN message_substitutes.msu_name%TYPE) IS
  BEGIN
    INSERT
      INTO message_substitutes (msu_msc_id,
                                msu_code,
                                msu_name)
                        VALUES (p_mscId,
                                p_msuCode,
                                p_msuName);
  END insert_msu;
BEGIN
  /* Message code text can include substitution variables.
     They should be in UPPER case and surrounded by <>.
     The substitution variables must should be inserted into
     the message_substitutes table.
     
     Message code prefixes are as follows:
     XSYS = System error message - generally the user shouldn't see much detail.
            When it is raised, support (config support_email) will be emailed with the details.
     XAPP = General application error that is fine to return to the user.
     */
     
  -- oracle error messages
  l_mscId := insert_msc('-1'    ,'Record already exists.');
--l_mscId := insert_msc('-1'    ,'Goal already exists.', constraint name); allows you to tailor the message to the constraint
  l_mscId := insert_msc('-1031' ,'You are not permitted to perform this action.');
  l_mscId := insert_msc('-2091' ,'Parent record not found.');
  l_mscId := insert_msc('-2291' ,'Child records must be deleted first.');

  -- our application error messages (hiding non user friendly messages)
  l_mscId := insert_msc('XSYS-00001','Oops. Something went wrong. The support team has been informed.');
  l_mscId := insert_msc('XSYS-00002','Oops. Something went wrong. Your email has not been sent. The support team has been informed.');
  l_mscId := insert_msc('XSYS-00003','You may only update data that belongs to you.');
  l_mscId := insert_msc('XSYS-00004','You may only delete data that belongs to you.');
  l_mscId := insert_msc('XSYS-00005','You may only insert data that belongs to you.');
  
  -- our application messages (user friendly)
  l_mscId := insert_msc('XAPP-00001','The supplied email address is assigned to another user.');
  l_mscId := insert_msc('XAPP-00002','The supplied password is not correct');
  l_mscId := insert_msc('XAPP-00003','The supplied username has already been allocated, please choose another.');
  l_mscId := insert_msc('XAPP-00004','Invalid goal end details.');
  l_mscId := insert_msc('XAPP-00005','Too many incorrect password attempts. Please reset your password.');
  l_mscId := insert_msc('XAPP-00006','Your session has expired.');
  l_mscId := insert_msc('XAPP-00007','Invalid login credentials.');
  l_mscId := insert_msc('XAPP-00008','TradeIgnite password reset.');
  l_mscId := insert_msc('XAPP-00009','Please use the following link to reset your password - https://54.241.240.126:8181/apex/f?p=110:104:::NO::P104_LC:<LINKCODE>');
             insert_msu(l_mscId, 'LINKCODE', 'Link Code');
  l_mscId := insert_msc('XAPP-00010','Logged out.');
  l_mscId := insert_msc('XAPP-00011','<CALC> on <EQUITY> of <PCT>%');
             insert_msu(l_mscId, 'PCT',    'Pct risk per trade');
             insert_msu(l_mscId, 'CALC',   'Calc name');
             insert_msu(l_mscId, 'EQUITY', 'Equity Model');
  l_mscId := insert_msc('XAPP-00012','<CALC> on <EQUITY> of <CCY> <AMT>');
             insert_msu(l_mscId, 'AMT',    'Amt risk per trade');
             insert_msu(l_mscId, 'CALC',   'Calc name');
             insert_msu(l_mscId, 'CCY',    'Currency Code');
             insert_msu(l_mscId, 'EQUITY', 'Equity Model');
  l_mscId := insert_msc('XAPP-00013','Value must be numeric and in the format 999.99');
  l_mscId := insert_msc('XAPP-00014','Value must be between 0.01 and 100');
  l_mscId := insert_msc('XAPP-00015','<CALC> on <EQUITY> of <UNITS> units');
             insert_msu(l_mscId, 'UNITS',  'Number of units');
             insert_msu(l_mscId, 'CALC',   'Calc name');
             insert_msu(l_mscId, 'EQUITY', 'Equity Model');
  l_mscId := insert_msc('XAPP-00016','The supplied password is on a public list of the most commonly used password. Please provide another.');
  l_mscId := insert_msc('XAPP-00017','Your session has expired.');
  l_mscId := insert_msc('XAPP-00018','Your session has expired.');
  l_mscId := insert_msc('XAPP-00019','Oops. Something went wrong. Please try signing up again.');
  l_mscId := insert_msc('XAPP-00020','Username must be supplied.');
  l_mscId := insert_msc('XAPP-00021','This email address has already been registered.');
  l_mscId := insert_msc('XAPP-00022','The selected language is not valid, please choose another.');
  l_mscId := insert_msc('XAPP-00023','Country of residence must be supplied.');
  l_mscId := insert_msc('XAPP-00024','The selected country of residence is not valid, please choose another.');
  l_mscId := insert_msc('XAPP-00025','Residential address must be supplied.');
  l_mscId := insert_msc('XAPP-00026','Password must be supplied.');
  l_mscId := insert_msc('XAPP-00027','Password confirmation must be supplied.');
  l_mscId := insert_msc('XAPP-00028','Passwords do not match.');
  l_mscId := insert_msc('XAPP-00029','Please review and action all errors before saving.');
  l_mscId := insert_msc('XAPP-00030','At least one contact number must be supplied.');
  l_mscId := insert_msc('XAPP-00031','Email address must be supplied.');
  l_mscId := insert_msc('XAPP-00032','Terms must be accepted.');
  l_mscId := insert_msc('XAPP-00033','Last Name must be supplied.');
  l_mscId := insert_msc('XAPP-00034','First Name must be supplied.');
  l_mscId := insert_msc('XAPP-00035','Date of Birth must be supplied.');
  l_mscId := insert_msc('XAPP-00036','The supplied credit card number does not appear to be valid. Please check that it has been entered correctly.');
  l_mscId := insert_msc('XAPP-00037','<CARD_TYPE> is not accepted by us at present. Please use a different card or payment method.');
             insert_msu(l_mscId, 'CARD_TYPE',  'Card Type');
  l_mscId := insert_msc('XAPP-00038','<CARD_TYPE> no longer exists as a card network. Please use a different card or payment method.');
             insert_msu(l_mscId, 'CARD_TYPE',  'Card Type');
  l_mscId := insert_msc('XAPP-00039','The supplied credit card number does not appear to be valid or is not associated with a card network that we accept. Please check that it has been entered correctly.');
  l_mscId := insert_msc('XAPP-00040','Credit card number must be supplied.');
  l_mscId := insert_msc('XAPP-00041','Billing country must be supplied.');
  l_mscId := insert_msc('XAPP-00042','The selected billing country is not valid, please choose another.');
  l_mscId := insert_msc('XAPP-00043','Billing address must be supplied.');
  l_mscId := insert_msc('XAPP-00044','Expiration date must be supplied.');
  l_mscId := insert_msc('XAPP-00045','Expiration date must not be in the past.');
  l_mscId := insert_msc('XAPP-00046','Card security code must be supplied.');
  l_mscId := insert_msc('XAPP-00047','Expiration date is not valid.');
  l_mscId := insert_msc('XAPP-00048','A link to reset your password has been emailed to you.');
  l_mscId := insert_msc('XAPP-00049','The selected image was not correct, please try again.');
  l_mscId := insert_msc('XAPP-00050','The email subject must be supplied.');
  l_mscId := insert_msc('XAPP-00051','The email body must be supplied.');
  l_mscId := insert_msc('XAPP-00052','The email recipient must be supplied.');  
  l_mscId := insert_msc('XAPP-00053','Oops. Something went wrong. Please try again.');
  l_mscId := insert_msc('XAPP-00054','TradeIgnite registration confirmation.');
  l_mscId := insert_msc('XAPP-00055','Thanks for registering.'||CHR(10)||CHR(10)||'Your username is this email address.'||CHR(10)||'Please use the following link to complete your registration and set your password - https://54.241.240.126:8181/apex/f?p=110:104:::NO::P104_LC:<LINKCODE>');
             insert_msu(l_mscId, 'LINKCODE', 'Link Code');
  l_mscId := insert_msc('XAPP-00056','The supplied registration code is not valid.'); 
  
  COMMIT;
END;
/
