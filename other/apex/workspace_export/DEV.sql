set define off
set verify off
set feedback off
WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK
begin wwv_flow.g_import_in_progress := true; end;
/
 
--       AAAA       PPPPP   EEEEEE  XX      XX
--      AA  AA      PP  PP  EE       XX    XX
--     AA    AA     PP  PP  EE        XX  XX
--    AAAAAAAAAA    PPPPP   EEEE       XXXX
--   AA        AA   PP      EE        XX  XX
--  AA          AA  PP      EE       XX    XX
--  AA          AA  PP      EEEEEE  XX      XX
begin
select value into wwv_flow_api.g_nls_numeric_chars from nls_session_parameters where parameter='NLS_NUMERIC_CHARACTERS';
execute immediate 'alter session set nls_numeric_characters=''.,''';
end;
/
prompt  WORKSPACE 10558130621669941
--
-- Workspace, User Group, User, and Team Development Export:
--   Date and Time:   08:32 Monday May 13, 2013
--   Exported By:     ADMIN
--   Export Type:     Workspace Export
--   Version:         4.2.2.00.11
--   Instance ID:     61810093136401
--
-- Import:
--   Using Instance Administration / Manage Workspaces
--   or
--   Using SQL*Plus as the Oracle user APEX_040200
 
begin
    wwv_flow_api.set_security_group_id(p_security_group_id=>10558130621669941);
end;
/
----------------
-- W O R K S P A C E
-- Creating a workspace will not create database schemas or objects.
-- This API creates only the meta data for this APEX workspace
prompt  Creating workspace DEV...
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
end;
/
begin
wwv_flow_fnd_user_api.create_company (
  p_id => 10558314205670231
 ,p_provisioning_company_id => 10558130621669941
 ,p_short_name => 'DEV'
 ,p_display_name => 'DEV'
 ,p_first_schema_provisioned => 'APP'
 ,p_company_schemas => 'APP'
 ,p_expire_fnd_user_accounts => 'Y'
 ,p_fnd_user_max_login_failures => 15
 ,p_account_status => 'ASSIGNED'
 ,p_allow_plsql_editing => 'Y'
 ,p_allow_app_building_yn => 'Y'
 ,p_allow_sql_workshop_yn => 'Y'
 ,p_allow_websheet_dev_yn => 'Y'
 ,p_allow_team_development_yn => 'Y'
 ,p_allow_to_be_purged_yn => 'Y'
 ,p_allow_restful_services_yn => 'Y'
 ,p_source_identifier => 'TRADE BO'
 ,p_path_prefix => 'DEV'
 ,p_workspace_image => wwv_flow_api.g_varchar2_table
);
end;
/
----------------
-- G R O U P S
--
prompt  Creating Groups...
----------------
-- U S E R S
-- User repository for use with APEX cookie-based authentication.
--
prompt  Creating Users...
begin
wwv_flow_fnd_user_api.create_fnd_user (
  p_user_id      => '1517621699619968',
  p_user_name    => 'DAN',
  p_first_name   => 'Dan',
  p_last_name    => '',
  p_description  => '',
  p_email_address=> 'daniel.rintoul@au.bynx.com',
  p_web_password => 'F2FD309D3FFBB67A9E092C30CD230F88',
  p_web_password_format => 'HEX_ENCODED_DIGEST_V2',
  p_group_ids                    => '2584706256366646:2584821420366647:3406503668544539:',
  p_developer_privs              => 'CREATE:EDIT:HELP:MONITOR:SQL:MONITOR:DATA_LOADER',
  p_default_schema               => 'APP',
  p_account_locked               => 'N',
  p_account_expiry               => to_date('201305040547','YYYYMMDDHH24MI'),
  p_failed_access_attempts       => 0,
  p_change_password_on_first_use => 'N',
  p_first_password_use_occurred  => 'Y',
  p_allow_app_building_yn        => 'Y',
  p_allow_sql_workshop_yn        => 'Y',
  p_allow_websheet_dev_yn        => 'Y',
  p_allow_team_development_yn    => 'Y',
  p_allow_access_to_schemas      => '');
end;
/
begin
wwv_flow_fnd_user_api.create_fnd_user (
  p_user_id      => '39747522333130001',
  p_user_name    => 'FRANS',
  p_first_name   => 'Frans',
  p_last_name    => '',
  p_description  => '',
  p_email_address=> 'jtrade_admin@dodo.com.au',
  p_web_password => 'BDDE07BDEF9FC36C6F2B348F253B104E',
  p_web_password_format => 'HEX_ENCODED_DIGEST_V2',
  p_group_ids                    => '2584706256366646:2584821420366647:3406503668544539:',
  p_developer_privs              => 'CREATE:EDIT:HELP:MONITOR:SQL:MONITOR:DATA_LOADER',
  p_default_schema               => 'APP',
  p_account_locked               => 'N',
  p_account_expiry               => to_date('201203051434','YYYYMMDDHH24MI'),
  p_failed_access_attempts       => 0,
  p_change_password_on_first_use => 'N',
  p_first_password_use_occurred  => 'N',
  p_allow_app_building_yn        => 'Y',
  p_allow_sql_workshop_yn        => 'Y',
  p_allow_websheet_dev_yn        => 'Y',
  p_allow_team_development_yn    => 'Y',
  p_allow_access_to_schemas      => '');
end;
/
----------------
--Application Builder Preferences
--
----------------
--Click Count Logs
--
----------------
--csv data loading
--
----------------
--mail
--
----------------
--mail log
--
----------------
--app models
--
----------------
--password history
--
begin
  wwv_flow_api.create_password_history (
    p_id => 2859716240476618,
    p_user_id => 1517621699619968,
    p_password => '3FC4D28475DDC055CC3244AAAB7A0D15');
end;
/
----------------
--preferences
--
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2888814057460374,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P405_W3852329031687921',
    p_attribute_value => '3853503855690337____3853503855690337');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2889013410460665,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P4050_W10642116325440827',
    p_attribute_value => '10643624462441172____10643624462441172');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2890304980753310,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P4110_W1548412223182178',
    p_attribute_value => '1550029190194632____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2909810703820463,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P4307_R262121216799808381_SORT',
    p_attribute_value => 'fsp_sort_5_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3753130347133255,
    p_user_id => 'JAKES@TRADEIGNITE.COM',
    p_preference_name => 'FSP_IR_110_P14_W5269105119171666',
    p_attribute_value => '5292908387174110____5292908387174110');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2930510890934011,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P195_R225787614827384691_SORT',
    p_attribute_value => 'fsp_sort_2_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3549611108011522,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_110_P22_W29500803516394745',
    p_attribute_value => '29508930917395077____29508930917395077');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3672815014786857,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P566_R7925320509424205_SORT',
    p_attribute_value => 'fsp_sort_2_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3674110952842459,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P582_R108017116347018440_SORT',
    p_attribute_value => 'fsp_sort_1_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3690714583229325,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P4445_W14886206368621919',
    p_attribute_value => '14887631485621929____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3711107690755869,
    p_user_id => 'JAKES',
    p_preference_name => 'FSP_IR_110_P19_W11857204253553528',
    p_attribute_value => '11891701049563966____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3716003622985645,
    p_user_id => 'JEANNINE',
    p_preference_name => 'FSP_IR_110_P14_W5269105119171666',
    p_attribute_value => '5292908387174110____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3719226763745122,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P102_R170975515323537885_SORT',
    p_attribute_value => 'fsp_sort_1_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3719304298745123,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P102_R170976026405541124_SORT',
    p_attribute_value => 'fsp_sort_1_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3722519181045747,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P194_R115839224419800245_SORT',
    p_attribute_value => 'fsp_sort_1_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3722611383045749,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P194_R224505023496694888_SORT',
    p_attribute_value => 'fsp_sort_1_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3558520423315094,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P707_R468075709995571377_SORT',
    p_attribute_value => 'fsp_sort_2_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3558626310316765,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P707_R90441604076983253_SORT',
    p_attribute_value => 'fsp_sort_2_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3559107067393475,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P688_R205852217711540402_SORT',
    p_attribute_value => 'fsp_sort_4');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3559224151393546,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P688_R207469307328320096_SORT',
    p_attribute_value => 'fsp_sort_2_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3575211103410679,
    p_user_id => 'JAKES',
    p_preference_name => 'FSP_IR_110_P14_W5269105119171666',
    p_attribute_value => '5292908387174110____5292908387174110');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3605331869096352,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4350_P63_W10287608164985354',
    p_attribute_value => '10288308760985571____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3606905792351511,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P4005_W48422219241609962',
    p_attribute_value => '48424005315623443____48424005315623443');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2860323925480279,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P1500_W3519715528105919',
    p_attribute_value => '3521529006112497____3521529006112497');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2878024485586350,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_110_P14_W5269105119171666',
    p_attribute_value => '5292908387174110____5292908387174110');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2878216937589924,
    p_user_id => 'DAN',
    p_preference_name => 'PERSISTENT_ITEM_P1_DISPLAY_MODE',
    p_attribute_value => 'ICONS');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2878311070589992,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P1_W3326806401130228',
    p_attribute_value => '3328003692130542____3328003692130542');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2878401395590053,
    p_user_id => 'DAN',
    p_preference_name => 'FB_FLOW_ID',
    p_attribute_value => '110');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2878611545592421,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P4100_W3727618522871356',
    p_attribute_value => '3728530690872449____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2878809605593320,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P4035_W48073422771824924',
    p_attribute_value => '48075424951829904____48075424951829904');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2884412168622532,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P4400_W27796519609844319',
    p_attribute_value => '27798220762844327____27798220762844327');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2884605698625449,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P260_W40585608941890825',
    p_attribute_value => '40587517034894377____40587517034894377');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2887429739282913,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_110_P19_W11857204253553528',
    p_attribute_value => '11891701049563966____11891701049563966');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 3486601146648516,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P386_R121204124843870496_SORT',
    p_attribute_value => 'fsp_sort_1_desc');
end;
/
----------------
--service mods
--
----------------
--query builder
--
----------------
--sql scripts
--
----------------
--sql commands
--
----------------
--user access log
--
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305040648','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305091026','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305091139','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305091203','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305091322','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305120940','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305050918','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305050937','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'JAKES@TRADEIGNITE.COM',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305122347','YYYYMMDDHH24MI'),
    p_ip_address => '124.176.106.105',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305051330','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305051331','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 4,
    p_custom_status_text => 'Invalid Login Credentials');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305051331','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305051333','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305051412','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305081233','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305081340','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305081353','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305081406','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'JAKES',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305090038','YYYYMMDDHH24MI'),
    p_ip_address => '124.184.1.247',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305090806','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305090807','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305100959','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305101000','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305101021','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305101155','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305102320','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305102324','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110103','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'JAKES',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110217','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'JEANNINE',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110250','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110251','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'JEANNINE',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110257','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'JAKES',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110432','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110539','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110542','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305110806','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110828','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110831','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110948','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305110948','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305110955','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305111212','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305061107','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305061302','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305061305','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'JAKES',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305070008','YYYYMMDDHH24MI'),
    p_ip_address => '138.130.141.72',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305071208','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305071230','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305071240','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305071250','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305071318','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305071356','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305081111','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305081111','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305081422','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305101243','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305101247','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305101415','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305101415','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN@TRADEIGNITE.COM',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305111353','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305040547','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305040550','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305040752','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305040800','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305050918','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305051149','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305051151','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305051238','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305051238','YYYYMMDDHH24MI'),
    p_ip_address => '121.217.197.138',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201305061615','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log2$ (
    p_login_name => 'JAKES',
    p_auth_method => 'custom_auth',
    p_app => 110,
    p_owner => 'APP',
    p_access_date => to_date('201305061617','YYYYMMDDHH24MI'),
    p_ip_address => '121.216.72.232',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
prompt Check Compatibility...
begin
-- This date identifies the minimum version required to import this file.
wwv_flow_team_api.check_version(p_version_yyyy_mm_dd=>'2010.05.13');
end;
/
 
begin wwv_flow.g_import_in_progress := true; wwv_flow.g_user := USER; end; 
/
 
--
prompt ...news
--
begin
null;
end;
/
--
prompt ...links
--
begin
null;
end;
/
--
prompt ...bugs
--
begin
null;
end;
/
--
prompt ...events
--
begin
null;
end;
/
--
prompt ...features
--
begin
null;
end;
/
--
prompt ...tasks
--
begin
null;
end;
/
--
prompt ...feedback
--
begin
null;
end;
/
--
prompt ...task defaults
--
begin
null;
end;
/
 
prompt ...RESTful Services
 
-- SET SCHEMA
 
begin
 
   wwv_flow_api.g_id_offset := 0;
   wwv_flow_hint.g_schema   := 'APP';
   wwv_flow_hint.check_schema_privs;
 
end;
/

 
--------------------------------------------------------------------
prompt  SCHEMA APP - User Interface Defaults, Table Defaults
--
-- Import using sqlplus as the Oracle user: APEX_040200
-- Exported 08:32 Monday May 13, 2013 by: ADMIN
--
 
--------------------------------------------------------------------
prompt User Interface Defaults, Attribute Dictionary
--
-- Exported 08:32 Monday May 13, 2013 by: ADMIN
--
-- SHOW EXPORTING WORKSPACE
 
begin
 
   wwv_flow_api.g_id_offset := 0;
   wwv_flow_hint.g_exp_workspace := 'DEV';
 
end;
/

commit;
begin
execute immediate 'begin sys.dbms_session.set_nls( param => ''NLS_NUMERIC_CHARACTERS'', value => '''''''' || replace(wwv_flow_api.g_nls_numeric_chars,'''''''','''''''''''') || ''''''''); end;';
end;
/
set verify on
set feedback on
set define on
prompt  ...done
