set define off
set verify off
set feedback off
WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK
begin wwv_flow.g_import_in_progress := true; end;
/
 
--       AAAA       PPPPP   EEEEEE  XX      XX
--      AA  AA      PP  PP  EE       XX    XX
--     AA    AA     PP  PP  EE        XX  XX
--    AAAAAAAAAA    PPPPP   EEEE       XXXX
--   AA        AA   PP      EE        XX  XX
--  AA          AA  PP      EE       XX    XX
--  AA          AA  PP      EEEEEE  XX      XX
begin
select value into wwv_flow_api.g_nls_numeric_chars from nls_session_parameters where parameter='NLS_NUMERIC_CHARACTERS';
execute immediate 'alter session set nls_numeric_characters=''.,''';
end;
/
prompt  WORKSPACE 2587102080948363
--
-- Workspace, User Group, User, and Team Development Export:
--   Date and Time:   08:33 Monday May 13, 2013
--   Exported By:     ADMIN
--   Export Type:     Workspace Export
--   Version:         4.2.2.00.11
--   Instance ID:     61810093136401
--
-- Import:
--   Using Instance Administration / Manage Workspaces
--   or
--   Using SQL*Plus as the Oracle user APEX_040200
 
begin
    wwv_flow_api.set_security_group_id(p_security_group_id=>2587102080948363);
end;
/
----------------
-- W O R K S P A C E
-- Creating a workspace will not create database schemas or objects.
-- This API creates only the meta data for this APEX workspace
prompt  Creating workspace GCMDEMO...
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
end;
/
begin
wwv_flow_fnd_user_api.create_company (
  p_id => 2587300377948659
 ,p_provisioning_company_id => 2587102080948363
 ,p_short_name => 'GCMDEMO'
 ,p_display_name => 'GCMDEMO'
 ,p_first_schema_provisioned => 'GCMDEMO'
 ,p_company_schemas => 'GCMDEMO'
 ,p_ws_schema => 'GCMDEMO'
 ,p_account_status => 'ASSIGNED'
 ,p_allow_plsql_editing => 'Y'
 ,p_allow_app_building_yn => 'Y'
 ,p_allow_sql_workshop_yn => 'Y'
 ,p_allow_websheet_dev_yn => 'Y'
 ,p_allow_team_development_yn => 'Y'
 ,p_allow_to_be_purged_yn => 'Y'
 ,p_allow_restful_services_yn => 'Y'
 ,p_source_identifier => 'GCMDEMO'
 ,p_path_prefix => 'GCMDEMO'
 ,p_workspace_image => wwv_flow_api.g_varchar2_table
);
end;
/
----------------
-- G R O U P S
--
prompt  Creating Groups...
----------------
-- U S E R S
-- User repository for use with APEX cookie-based authentication.
--
prompt  Creating Users...
begin
wwv_flow_fnd_user_api.create_fnd_user (
  p_user_id      => '2587015799948363',
  p_user_name    => 'ADMIN',
  p_first_name   => '',
  p_last_name    => '',
  p_description  => '',
  p_email_address=> 'dan@tradeignite.com',
  p_web_password => 'A397A64699B4EF29C79FAC14138B3153',
  p_web_password_format => 'HEX_ENCODED_DIGEST_V2',
  p_group_ids                    => '2584706256366646:2584821420366647:3406503668544539:',
  p_developer_privs              => 'ADMIN:CREATE:DATA_LOADER:EDIT:HELP:MONITOR:SQL',
  p_default_schema               => 'GCMDEMO',
  p_account_locked               => 'N',
  p_account_expiry               => to_date('201303112232','YYYYMMDDHH24MI'),
  p_failed_access_attempts       => 0,
  p_change_password_on_first_use => 'N',
  p_first_password_use_occurred  => 'Y',
  p_allow_app_building_yn        => 'Y',
  p_allow_sql_workshop_yn        => 'Y',
  p_allow_websheet_dev_yn        => 'Y',
  p_allow_team_development_yn    => 'Y',
  p_allow_access_to_schemas      => '');
end;
/
begin
wwv_flow_fnd_user_api.create_fnd_user (
  p_user_id      => '2846627124487509',
  p_user_name    => 'DAN',
  p_first_name   => 'Dan',
  p_last_name    => '',
  p_description  => '',
  p_email_address=> 'dan@tradeignite.com',
  p_web_password => 'DBD3BCAD9A875FA9993E838013C52DFD',
  p_web_password_format => 'HEX_ENCODED_DIGEST_V2',
  p_group_ids                    => '2584706256366646:2584821420366647:3406503668544539:',
  p_developer_privs              => 'ADMIN:CREATE:DATA_LOADER:EDIT:HELP:MONITOR:SQL',
  p_default_schema               => 'GCMDEMO',
  p_account_locked               => 'N',
  p_account_expiry               => to_date('201303112233','YYYYMMDDHH24MI'),
  p_failed_access_attempts       => 0,
  p_change_password_on_first_use => 'N',
  p_first_password_use_occurred  => 'N',
  p_allow_app_building_yn        => 'Y',
  p_allow_sql_workshop_yn        => 'Y',
  p_allow_websheet_dev_yn        => 'Y',
  p_allow_team_development_yn    => 'Y',
  p_allow_access_to_schemas      => '');
end;
/
begin
wwv_flow_fnd_user_api.create_fnd_user (
  p_user_id      => '2847108339501040',
  p_user_name    => 'JAKES',
  p_first_name   => 'Jakes',
  p_last_name    => '',
  p_description  => '',
  p_email_address=> 'jakes@tradeignite.com',
  p_web_password => 'A574A21CA8623632A8F98893AA024A35',
  p_web_password_format => 'HEX_ENCODED_DIGEST_V2',
  p_group_ids                    => '2584706256366646:2584821420366647:3406503668544539:',
  p_developer_privs              => 'CREATE:EDIT:HELP:MONITOR:SQL:MONITOR:DATA_LOADER',
  p_default_schema               => 'GCMDEMO',
  p_account_locked               => 'N',
  p_account_expiry               => to_date('201301230000','YYYYMMDDHH24MI'),
  p_failed_access_attempts       => 0,
  p_change_password_on_first_use => 'Y',
  p_first_password_use_occurred  => 'N',
  p_allow_app_building_yn        => 'Y',
  p_allow_sql_workshop_yn        => 'Y',
  p_allow_websheet_dev_yn        => 'Y',
  p_allow_team_development_yn    => 'Y',
  p_allow_access_to_schemas      => '');
end;
/
----------------
--Application Builder Preferences
--
----------------
--Click Count Logs
--
----------------
--csv data loading
--
----------------
--mail
--
----------------
--mail log
--
----------------
--app models
--
----------------
--password history
--
begin
  wwv_flow_api.create_password_history (
    p_id => 2846705443487518,
    p_user_id => 2846627124487509,
    p_password => 'D3C848D3F9D8F6F18CD320A11DF101B3');
end;
/
begin
  wwv_flow_api.create_password_history (
    p_id => 2847209097501040,
    p_user_id => 2847108339501040,
    p_password => 'D87A9B7F52FC2F4BC2CE40E655C4F5B0');
end;
/
begin
  wwv_flow_api.create_password_history (
    p_id => 2587608922948682,
    p_user_id => 2587015799948363,
    p_password => '726C018AF35D4FD00AEF07DEB3C5D187');
end;
/
begin
  wwv_flow_api.create_password_history (
    p_id => 2707816488970063,
    p_user_id => 2587015799948363,
    p_password => '2C21A8EBE95020BC2ACE6C3BC41666CE');
end;
/
begin
  wwv_flow_api.create_password_history (
    p_id => 2855511519917574,
    p_user_id => 2587015799948363,
    p_password => '8AC7D1B0341CCC6346C8EE2B4FC4C9A5');
end;
/
begin
  wwv_flow_api.create_password_history (
    p_id => 2855906802925705,
    p_user_id => 2846627124487509,
    p_password => '5E988D41C83E1F72BC0C7AEB69DAFA5D');
end;
/
----------------
--preferences
--
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2714513477292179,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P260_W40585608941890825',
    p_attribute_value => '40587517034894377____40587517034894377');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2715008533412397,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP4000_P4307_R262121216799808381_SORT',
    p_attribute_value => 'fsp_sort_5_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2846414716483216,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4350_P55_W10236304983033455',
    p_attribute_value => '10238325656034902____10238325656034902');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2848408963907695,
    p_user_id => 'DAN',
    p_preference_name => 'PERSISTENT_ITEM_P1_DISPLAY_MODE',
    p_attribute_value => 'ICONS');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2848508761907717,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P1_W3326806401130228',
    p_attribute_value => '3328003692130542____3328003692130542');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2848613331907775,
    p_user_id => 'DAN',
    p_preference_name => 'FB_FLOW_ID',
    p_attribute_value => '100');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2848806591908788,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P4035_W48073422771824924',
    p_attribute_value => '48075424951829904____48075424951829904');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2849229140935638,
    p_user_id => 'DAN',
    p_preference_name => 'FSP4000_P4307_R262121216799808381_SORT',
    p_attribute_value => 'fsp_sort_5_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2708114459971654,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P1500_W3519715528105919',
    p_attribute_value => '3521529006112497____3521529006112497');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2710302942083380,
    p_user_id => 'ADMIN',
    p_preference_name => 'PERSISTENT_ITEM_P1_DISPLAY_MODE',
    p_attribute_value => 'ICONS');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2710403217083413,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P1_W3326806401130228',
    p_attribute_value => '3328003692130542____3328003692130542');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2710526035083469,
    p_user_id => 'ADMIN',
    p_preference_name => 'FB_FLOW_ID',
    p_attribute_value => '100');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2710732260085023,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P170_W47844825293922430',
    p_attribute_value => '47846032049926790____47846032049926790');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2716531836578801,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_100_P2_W6810648114254400229',
    p_attribute_value => '6664600782029442692____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2717224694612518,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4900_P2_W3764989200639696882',
    p_attribute_value => '3764990101332697015____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2717313685613118,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4900_P2_W4514269719980283920',
    p_attribute_value => '4514270322823283943____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2717630873614873,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4900_P11_W451311719920376851',
    p_attribute_value => '451312420393376855____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2717908107616148,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4900_P905_W255846907421688350',
    p_attribute_value => '255849600351701615____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2718114558617182,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4900_P1000_W9947931622145849',
    p_attribute_value => '9948707080146124____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2742719375775573,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP4000_P195_R225787614827384691_SORT',
    p_attribute_value => 'fsp_sort_2_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2743323264780242,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P546_W30205316146531602',
    p_attribute_value => '30206031208532453____30206031208532453');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2743519598782000,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P287_W47869516939800889',
    p_attribute_value => '47870618297805517____47870618297805517');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2745404009865146,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P405_W3852329031687921',
    p_attribute_value => '3853503855690337____3853503855690337');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2751318205904200,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P4050_W10642116325440827',
    p_attribute_value => '10643624462441172____10643624462441172');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2814830092977353,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP4000_P386_R121204124843870496_SORT',
    p_attribute_value => 'fsp_sort_1_desc');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2841803454017404,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P4100_W3727618522871356',
    p_attribute_value => '3728530690872449____3728530690872449');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2842032315049567,
    p_user_id => 'ADMIN',
    p_preference_name => 'FSP_IR_4000_P4035_W48073422771824924',
    p_attribute_value => '48075424951829904____48075424951829904');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2848216647736990,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4000_P1500_W3519715528105919',
    p_attribute_value => '3521529006112497____3521529006112497');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2855010378910394,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4350_P7_W9412205627285339',
    p_attribute_value => '9413006316285546____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2855205202912762,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4350_P70_W450591914456785448',
    p_attribute_value => '450593613404789820____');
end;
/
begin
  wwv_flow_api.create_preferences$ (
    p_id => 2855403476913575,
    p_user_id => 'DAN',
    p_preference_name => 'FSP_IR_4350_P55_W10236304983033455',
    p_attribute_value => '10238325656034902____10238325656034902');
end;
/
----------------
--service mods
--
----------------
--query builder
--
----------------
--sql scripts
--
----------------
--sql commands
--
----------------
--user access log
--
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'DAN',
    p_auth_method => 'Application Express Authentication',
    p_app => 100,
    p_owner => 'GCMDEMO',
    p_access_date => to_date('201303112237','YYYYMMDDHH24MI'),
    p_ip_address => '60.228.49.130',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201303112229','YYYYMMDDHH24MI'),
    p_ip_address => '60.228.49.130',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 4,
    p_custom_status_text => 'Invalid Login Credentials');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201303112230','YYYYMMDDHH24MI'),
    p_ip_address => '60.228.49.130',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'DAN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201303112242','YYYYMMDDHH24MI'),
    p_ip_address => '60.228.49.130',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
begin
  wwv_flow_api.create_user_access_log1$ (
    p_login_name => 'ADMIN',
    p_auth_method => 'Internal Authentication',
    p_app => 4500,
    p_owner => 'APEX_040200',
    p_access_date => to_date('201303112255','YYYYMMDDHH24MI'),
    p_ip_address => '60.228.49.130',
    p_remote_user => 'APEX_PUBLIC_USER',
    p_auth_result => 0,
    p_custom_status_text => '');
end;
/
prompt Check Compatibility...
begin
-- This date identifies the minimum version required to import this file.
wwv_flow_team_api.check_version(p_version_yyyy_mm_dd=>'2010.05.13');
end;
/
 
begin wwv_flow.g_import_in_progress := true; wwv_flow.g_user := USER; end; 
/
 
--
prompt ...news
--
begin
null;
end;
/
--
prompt ...links
--
begin
null;
end;
/
--
prompt ...bugs
--
begin
null;
end;
/
--
prompt ...events
--
begin
null;
end;
/
--
prompt ...features
--
begin
null;
end;
/
--
prompt ...tasks
--
begin
null;
end;
/
--
prompt ...feedback
--
begin
null;
end;
/
--
prompt ...task defaults
--
begin
null;
end;
/
 
prompt ...RESTful Services
 
 
begin
 
wwv_flow_api.remove_restful_service (
  p_id => 2587728795948795 + wwv_flow_api.g_id_offset
 ,p_name => 'oracle.example.hr'
  );
 
null;
 
end;
/

prompt  ...restful service
--
--application/restful_services/oracle_example_hr
 
begin
 
wwv_flow_api.create_restful_module (
  p_id => 2587728795948795 + wwv_flow_api.g_id_offset
 ,p_name => 'oracle.example.hr'
 ,p_uri_prefix => 'hr/'
 ,p_parsing_schema => 'GCMDEMO'
 ,p_items_per_page => 10
 ,p_status => 'PUBLISHED'
  );
 
wwv_flow_api.create_restful_template (
  p_id => 2589108174948811 + wwv_flow_api.g_id_offset
 ,p_module_id => 2587728795948795 + wwv_flow_api.g_id_offset
 ,p_uri_template => 'empinfo/'
 ,p_priority => 0
 ,p_etag_type => 'HASH'
  );
 
wwv_flow_api.create_restful_handler (
  p_id => 2589221785948811 + wwv_flow_api.g_id_offset
 ,p_template_id => 2589108174948811 + wwv_flow_api.g_id_offset
 ,p_source_type => 'QUERY'
 ,p_format => 'CSV'
 ,p_method => 'GET'
 ,p_require_https => 'NO'
 ,p_source => 
'select * from emp'
  );
 
wwv_flow_api.create_restful_template (
  p_id => 2587824093948796 + wwv_flow_api.g_id_offset
 ,p_module_id => 2587728795948795 + wwv_flow_api.g_id_offset
 ,p_uri_template => 'employees/'
 ,p_priority => 0
 ,p_etag_type => 'HASH'
  );
 
wwv_flow_api.create_restful_handler (
  p_id => 2587900263948809 + wwv_flow_api.g_id_offset
 ,p_template_id => 2587824093948796 + wwv_flow_api.g_id_offset
 ,p_source_type => 'QUERY'
 ,p_format => 'DEFAULT'
 ,p_method => 'GET'
 ,p_items_per_page => 7
 ,p_require_https => 'NO'
 ,p_source => 
'select empno "$uri", empno, ename'||unistr('\000a')||
'  from ('||unistr('\000a')||
'       select emp.*'||unistr('\000a')||
'            , row_number() over (order by empno) rn'||unistr('\000a')||
'         from emp'||unistr('\000a')||
'       ) tmp'||unistr('\000a')||
' where rn between :row_offset and :row_count '
  );
 
wwv_flow_api.create_restful_template (
  p_id => 2588014843948809 + wwv_flow_api.g_id_offset
 ,p_module_id => 2587728795948795 + wwv_flow_api.g_id_offset
 ,p_uri_template => 'employees/{id}'
 ,p_priority => 0
 ,p_etag_type => 'HASH'
  );
 
wwv_flow_api.create_restful_handler (
  p_id => 2588101975948810 + wwv_flow_api.g_id_offset
 ,p_template_id => 2588014843948809 + wwv_flow_api.g_id_offset
 ,p_source_type => 'QUERY_1_ROW'
 ,p_format => 'DEFAULT'
 ,p_method => 'GET'
 ,p_require_https => 'NO'
 ,p_source => 
'select * from emp'||unistr('\000a')||
' where empno = :id'
  );
 
wwv_flow_api.create_restful_param (
  p_id => 2588219158948810 + wwv_flow_api.g_id_offset
 ,p_handler_id => 2588101975948810 + wwv_flow_api.g_id_offset
 ,p_name => 'ID'
 ,p_bind_variable_name => 'ID'
 ,p_source_type => 'HEADER'
 ,p_access_method => 'IN'
 ,p_param_type => 'STRING'
  );
 
wwv_flow_api.create_restful_template (
  p_id => 2588305941948811 + wwv_flow_api.g_id_offset
 ,p_module_id => 2587728795948795 + wwv_flow_api.g_id_offset
 ,p_uri_template => 'employeesfeed/'
 ,p_priority => 0
 ,p_etag_type => 'HASH'
  );
 
wwv_flow_api.create_restful_handler (
  p_id => 2588413163948811 + wwv_flow_api.g_id_offset
 ,p_template_id => 2588305941948811 + wwv_flow_api.g_id_offset
 ,p_source_type => 'FEED'
 ,p_format => 'DEFAULT'
 ,p_method => 'GET'
 ,p_items_per_page => 25
 ,p_require_https => 'NO'
 ,p_source => 
'select empno, ename from emp order by deptno, ename'
  );
 
wwv_flow_api.create_restful_template (
  p_id => 2588526863948811 + wwv_flow_api.g_id_offset
 ,p_module_id => 2587728795948795 + wwv_flow_api.g_id_offset
 ,p_uri_template => 'employeesfeed/{id}'
 ,p_priority => 0
 ,p_etag_type => 'HASH'
  );
 
wwv_flow_api.create_restful_handler (
  p_id => 2588615840948811 + wwv_flow_api.g_id_offset
 ,p_template_id => 2588526863948811 + wwv_flow_api.g_id_offset
 ,p_source_type => 'QUERY'
 ,p_format => 'CSV'
 ,p_method => 'GET'
 ,p_require_https => 'NO'
 ,p_source => 
'select * from emp where empno = :id'
  );
 
wwv_flow_api.create_restful_template (
  p_id => 2588709114948811 + wwv_flow_api.g_id_offset
 ,p_module_id => 2587728795948795 + wwv_flow_api.g_id_offset
 ,p_uri_template => 'empsec/{empname}'
 ,p_priority => 0
 ,p_etag_type => 'HASH'
  );
 
wwv_flow_api.create_restful_handler (
  p_id => 2588804471948811 + wwv_flow_api.g_id_offset
 ,p_template_id => 2588709114948811 + wwv_flow_api.g_id_offset
 ,p_source_type => 'QUERY'
 ,p_format => 'DEFAULT'
 ,p_method => 'GET'
 ,p_require_https => 'NO'
 ,p_source => 
'select empno, ename, deptno, job from emp '||unistr('\000a')||
'	where ((select job from emp where ename = :empname) IN (''PRESIDENT'', ''MANAGER'')) '||unistr('\000a')||
'    OR  '||unistr('\000a')||
'    (deptno = (select deptno from emp where ename = :empname)) '||unistr('\000a')||
'order by deptno, ename'||unistr('\000a')||
''
  );
 
wwv_flow_api.create_restful_template (
  p_id => 2588907444948811 + wwv_flow_api.g_id_offset
 ,p_module_id => 2587728795948795 + wwv_flow_api.g_id_offset
 ,p_uri_template => 'empsecformat/{empname}'
 ,p_priority => 0
 ,p_etag_type => 'HASH'
  );
 
wwv_flow_api.create_restful_handler (
  p_id => 2589022228948811 + wwv_flow_api.g_id_offset
 ,p_template_id => 2588907444948811 + wwv_flow_api.g_id_offset
 ,p_source_type => 'PLSQL'
 ,p_format => 'DEFAULT'
 ,p_method => 'GET'
 ,p_require_https => 'NO'
 ,p_source => 
'DECLARE'||unistr('\000a')||
'  prevdeptno   number;'||unistr('\000a')||
'  deptloc      varchar2(30);'||unistr('\000a')||
'  deptname     varchar2(30);'||unistr('\000a')||
'  CURSOR getemps IS select * from emp '||unistr('\000a')||
'                     where ((select job from emp where ename = :empname)  IN (''PRESIDENT'', ''MANAGER'')) '||unistr('\000a')||
'                        or deptno = (select deptno from emp where ename = :empname) '||unistr('\000a')||
'                     order by deptno, ename;'||unistr('\000a')||
'BEGIN'||unistr('\000a')||
'  sys.htp.htmlopen;'||unistr('\000a')||
'  sys.htp.he'||
'adopen;'||unistr('\000a')||
'  sys.htp.title(''Departments'');'||unistr('\000a')||
'  sys.htp.headclose;'||unistr('\000a')||
'  sys.htp.bodyopen;'||unistr('\000a')||
' '||unistr('\000a')||
'  for emprecs in getemps'||unistr('\000a')||
'  loop'||unistr('\000a')||
''||unistr('\000a')||
'      if emprecs.deptno != prevdeptno or prevdeptno is null then'||unistr('\000a')||
'          select dname, loc into deptname, deptloc '||unistr('\000a')||
'            from dept where deptno = (select deptno from emp where ename = emprecs.ename);'||unistr('\000a')||
'          if prevdeptno is not null then'||unistr('\000a')||
'              sys.htp.print(''</ul>'''||
');'||unistr('\000a')||
'          end if;'||unistr('\000a')||
'          sys.htp.print(''Department '' || deptname || '' located in '' || deptloc || ''<p/>'');'||unistr('\000a')||
'          sys.htp.print(''<ul>'');'||unistr('\000a')||
'      end if;'||unistr('\000a')||
''||unistr('\000a')||
'      sys.htp.print(''<li>'' || emprecs.ename || '', '' || emprecs.job || '', '' || emprecs.sal || ''</li>'');'||unistr('\000a')||
''||unistr('\000a')||
'      prevdeptno := emprecs.deptno;'||unistr('\000a')||
''||unistr('\000a')||
'  end loop;'||unistr('\000a')||
'  sys.htp.print(''</ul>'');'||unistr('\000a')||
'  sys.htp.bodyclose;'||unistr('\000a')||
'  sys.htp.htmlclose;'||unistr('\000a')||
'END;'||unistr('\000a')||
''
  );
 
null;
 
end;
/

-- SET SCHEMA
 
begin
 
   wwv_flow_api.g_id_offset := 0;
   wwv_flow_hint.g_schema   := 'GCMDEMO';
   wwv_flow_hint.check_schema_privs;
 
end;
/

 
--------------------------------------------------------------------
prompt  SCHEMA GCMDEMO - User Interface Defaults, Table Defaults
--
-- Import using sqlplus as the Oracle user: APEX_040200
-- Exported 08:33 Monday May 13, 2013 by: ADMIN
--
 
--------------------------------------------------------------------
prompt User Interface Defaults, Attribute Dictionary
--
-- Exported 08:33 Monday May 13, 2013 by: ADMIN
--
-- SHOW EXPORTING WORKSPACE
 
begin
 
   wwv_flow_api.g_id_offset := 0;
   wwv_flow_hint.g_exp_workspace := 'GCMDEMO';
 
end;
/

commit;
begin
execute immediate 'begin sys.dbms_session.set_nls( param => ''NLS_NUMERIC_CHARACTERS'', value => '''''''' || replace(wwv_flow_api.g_nls_numeric_chars,'''''''','''''''''''') || ''''''''); end;';
end;
/
set verify on
set feedback on
set define on
prompt  ...done
