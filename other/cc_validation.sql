DECLARE
  p_cc    VARCHAR2(20) := '4111111111111111';
  l_digit INTEGER;
  l_total INTEGER := 0;
BEGIN
  FOR i IN 1..LENGTH(p_cc) LOOP
    l_digit := TO_NUMBER(SUBSTR(p_cc, -i, 1));
    
    IF MOD(i,2) = 0 THEN
      l_digit := l_digit * 2;
      IF l_digit > 9 THEN
        l_digit := l_digit - 9;
      END IF;
    END IF;
    
    l_total := l_total + l_digit;
  END LOOP;
  
  IF MOD(l_total,10) != 0 THEN
    DBMS_OUTPUT.PUT_LINE('Invalid');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Valid');
  END IF;
END;



Issuing network                            Active  Length  Validation                IIN ranges                                           
American Express                           Yes     15[4]   Luhn algorithm            34, 37[3][dead link]                                 
Bankcard[5]                                No      16      Luhn algorithm            5610, 560221-560225                                  
China UnionPay                             Yes     16-19   no validation             62[6]                                                
Diners Club Carte Blanche                  Yes     14      Luhn algorithm            300-305                                              
Diners Club enRoute                        No      15      no validation             2014, 2149                                           
Diners Club International[7]               Yes     14      Luhn algorithm            36                                                   
Diners Club United States & Canada[8]      Yes     16      Luhn algorithm            54, 55                                               
Discover Card[9]                           Yes     16      Luhn algorithm            6011, 622126-622925, 644-649, 65                     
InstaPayment                               Yes     16      Luhn algorithm            637-639[citation needed]                             
JCB                                        Yes     16      Luhn algorithm            3528-3589[10]                                        
Laser                                      Yes     16-19   Luhn algorithm            6304, 6706, 6771, 6709                               
Maestro                                    Yes     12-19   Luhn algorithm            5018, 5020, 5038, 6304, 6759, 6761, 6762, 6763       
MasterCard not real                        Yes     16      Luhn algorithm            51-55                                                
Solo                                       No      16, 18, 19      Luhn algorithm    6334, 6767                                           
Switch                                     No      16, 18, 19      Luhn algorithm    4903, 4905, 4911, 4936, 564182, 633110, 6333, 6759   
Visa                                       Yes     13, 16[11]      Luhn algorithm    4                                                    
Visa Electron                              Yes     16      Luhn algorithm            4026, 417500, 4508, 4844, 4913, 4917                 



create table card_types (ct_id INTEGER, ct_issuer_name VARCHAR2(20), length, validation, allowed_ind

create table card_ranges (cr_id INTEGER, iin from, iin to