ALTER SESSION SET CURRENT_SCHEMA = TI_CORE_SRC;

BEGIN
  -- Define Intervals  
  DBMS_SCHEDULER.CREATE_SCHEDULE (schedule_name   => 'INTERVAL_EVERY_MINUTE',  
                                  start_date      => TRUNC(SYSDATE)+18/24,  
                                  repeat_interval => 'freq=MINUTELY;interval=1',  
                                  comments        => 'Runtime: Every day every minute');
                                  
  DBMS_SCHEDULER.CREATE_SCHEDULE (schedule_name   => 'INTERVAL_EVERY_10MINS',  
                                  start_date      => TRUNC(SYSDATE)+18/24,  
                                  repeat_interval => 'freq=MINUTELY;interval=10',  
                                  comments        => 'Runtime: Every day every 10 mins');


  -- Define Procedures
  DBMS_SCHEDULER.CREATE_PROGRAM (program_name   => 'SEND_MAIL',  
                                 program_type   => 'STORED_PROCEDURE',  
                                 program_action => 'ti_core_src.c_Mail.sendMessages',  
                                 enabled        => TRUE,  
                                 comments       => 'Procedure to send queued emails');  
                                 
  DBMS_SCHEDULER.CREATE_PROGRAM (program_name   => 'SESSION_TIMEOUT',  
                                 program_type   => 'STORED_PROCEDURE',  
                                 program_action => 'ti_core_src.c_Session.timeoutSessions',  
                                 enabled        => TRUE,  
                                 comments       => 'Procedure to timeout inactive sessions'); 


  -- Define Jobs
  DBMS_SCHEDULER.CREATE_JOB (job_name      => 'SEND_MAILS',  
                             program_name  => 'SEND_MAIL',
                             schedule_name => 'INTERVAL_EVERY_MINUTE',  
                             enabled       => TRUE,  
                             auto_drop     => FALSE,  
                             comments      => 'Job to send out queued emails');  
                             
  DBMS_SCHEDULER.CREATE_JOB (job_name      => 'SESSION_CLEANUP',  
                             program_name  => 'SESSION_TIMEOUT',
                             schedule_name => 'INTERVAL_EVERY_10MINS',  
                             enabled       => TRUE,  
                             auto_drop     => FALSE,  
                             comments      => 'Job to timeout inactive sessions');  
                             
  COMMIT;
END;
/
