CREATE OR REPLACE PACKAGE BODY ti_core_src.a_Signup IS 
  ----------------------------------------------------------------------------------------------------------------
  -- Forward declarations
  ----------------------------------------------------------------------------------------------------------------

  /* Send registration email for a signup request - used to validate their email address before creating a user */
  PROCEDURE sendRegEmail (p_sgnId IN signup.sgn_id%TYPE);
  
  /* If a user has tried to signup multiple times, this will just expire the previous attempts */
  PROCEDURE expireOldSignups (p_user IN signup.sgn_username%TYPE);

  /* This indicates is previous signup attempts have been made */  
  FUNCTION signupExists (p_newUser IN signup.sgn_username%TYPE) RETURN VARCHAR2;

  ----------------------------------------------------------------------------------------------------------------
  -- Public declarations
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE createSignup (p_first_name IN  signup.sgn_first_name%TYPE,
                          p_last_name  IN  signup.sgn_last_name%TYPE,
                          p_email      IN  signup.sgn_email%TYPE,
                          p_terms      IN  signup.sgn_terms_accept_ind%TYPE,
                          p_ip         IN  signup.sgn_ip%TYPE,
                          p_session_id IN  signup.sgn_session_id%TYPE,
                          p_errMsgs    OUT tt_data,
                          p_valid      OUT VARCHAR2) IS
    l_sgnId  signup.sgn_id%TYPE;
    l_row    signup%ROWTYPE;
    l_out    tt_data;
  BEGIN
    IF a_data.isFalse(signupAllowed) THEN
      a_Message.logMsgCode (p_code => 'XAPP-00057', -- signup not allowed
                            p_type => a_Message.gFatal);
    END IF;
    
    a_Message.logMsgText(p_message => 'Validating signup values',
                         p_data    => a_Data.addTag(a_Data.addTag(a_Data.addTag(NULL,'SESSION_ID',p_session_id),'IP',p_ip),'EMAIL',p_email),
                         p_type    => a_Message.gInfo);
  
    p_valid := a_data.gTrue;
    
    IF p_email IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'EMAIL',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00031')); -- email must be supplied

      p_valid := a_data.gFalse;
    ELSIF a_Data.isTrue(c_Session.userExists(p_newUser => UPPER(p_email))) THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'EMAIL',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00021')); -- email already registered
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF NVL(p_terms,'N') = 'N' THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'TERMS',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00032')); -- terms must be accepted
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_last_name IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'LASTNAME',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00033')); -- last name must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_first_name IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'FIRSTNAME',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00034')); -- first name must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF p_session_id IS NULL THEN
      l_out := a_Data.addTag (p_data  => l_out,
                              p_name  => 'SESSION_ID',
                              p_value => a_Message.getMessageText(p_code => 'XAPP-00035')); -- session id must be supplied
      
      p_valid := a_data.gFalse;
    END IF;
    
    IF a_Data.isFalse(p_valid) THEN
      p_errMsgs := l_out;
    END IF;
    
    IF p_valid = a_data.gTrue THEN
      a_Message.logMsgText(p_message => 'Signup data validated',
                           p_data    => a_Data.addTag(NULL,'SGN_ID',l_sgnId),
                           p_type    => a_Message.gInfo);
                           
      -- store data
      l_row.sgn_created_date            := SYSDATE;
      l_row.sgn_username                := LOWER(p_email);
      l_row.sgn_first_name              := p_first_name;
      l_row.sgn_last_name               := p_last_name;
      l_row.sgn_email                   := p_email;
      l_row.sgn_terms_accept_ind        := p_terms;
      l_row.sgn_ip                      := p_ip;
      l_row.sgn_session_id              := p_session_id;
      l_row.sgn_status                  := 'C';
    
      t_Signup.insertRow (p_row   => l_row,
                          p_newPk => l_sgnId);
                            
      expireOldSignups(p_user => l_row.sgn_username);
      
      sendRegEmail (p_sgnId => l_sgnId);
    ELSE
      a_Message.logMsgText(p_message => 'Signup data validated. Errors found',
                           p_data    => a_Data.addTag(a_Data.addTag(a_Data.addTag(l_out,'SESSION_ID',p_session_id),'IP',p_ip),'EMAIL',p_email),
                           p_type    => a_Message.gInfo);
    END IF;
  END createSignup;
 
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION signupAllowed RETURN VARCHAR2 IS
  BEGIN
    IF t_CodeValues.getValue_PK('config_values', 'signup_allowed') = 'Y' THEN
      RETURN a_Data.gTrue;
    ELSE
      RETURN a_Data.gFalse;
    END IF;
  END signupAllowed;

  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE createUser (p_code IN temp_links.tln_link_code%TYPE,
                        p_pw   IN VARCHAR2) IS
  BEGIN
    ti_core_src.c_Security.createUser(p_code => p_code,
                                      p_pw   => p_pw);
  END createUser;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getSgnIdFromTlnId (p_tlnId IN temp_links.tln_id%TYPE) RETURN signup.sgn_id%TYPE IS
    l_out signup.sgn_id%TYPE;
  BEGIN
    SELECT sgn_id
      INTO l_out
      FROM signup
     WHERE sgn_tln_id = p_tlnId;
     
    RETURN l_out;
  END getSgnIdFromTlnId;
  
 -- ----------------------------------------------------------------------------------------------------------
 -- Private declarations
 -- ----------------------------------------------------------------------------------------------------------
 
  PROCEDURE sendRegEmail (p_sgnId IN signup.sgn_id%TYPE) IS
  BEGIN
    ti_core_src.c_Security.sendRegMail(p_sgnId => p_sgnId);
  END sendRegEmail;
 
  ----------------------------------------------------------------------------------------------------------
 
  PROCEDURE expireOldSignups (p_user IN signup.sgn_username%TYPE) IS
  BEGIN
    IF a_Data.isTrue(signupExists(p_newUser => p_user)) THEN
      FOR rc1 IN (SELECT sgn_id
                    FROM signup
                   WHERE sgn_username = p_user
                     AND sgn_status   = 'C') LOOP
         
        t_Signup.setStatus(p_pk => rc1.sgn_id, p_val => 'R');
      END LOOP;

      a_Message.logMsgText (p_message => 'Existing signups for user have been expired',
                            p_data    => a_Data.addTag(NULL,'USERNAME',p_user),
                            p_type    => a_Message.gInfo);
    END IF;
  END expireOldSignups;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION signupExists (p_newUser IN signup.sgn_username%TYPE) RETURN VARCHAR2 IS
    l_chk INTEGER;
  BEGIN
    BEGIN
      SELECT 1
        INTO l_chk
        FROM signup
       WHERE sgn_username = p_newUser
         AND sgn_status   = 'C'
         AND ROWNUM       = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_chk := 0;
    END;

    IF l_chk = 1 THEN
      a_Message.logMsgText (p_message => 'Existing signup request found',
                            p_data    => a_Data.addTag(NULL,'USERNAME',p_newUser),
                            p_type    => a_Message.gInfo);
      RETURN a_Data.gTrue;
    ELSE
      a_Message.logMsgText (p_message => 'Existing signup request not found',
                            p_data    => a_Data.addTag(NULL,'USERNAME',p_newUser),
                            p_type    => a_Message.gInfo);
      RETURN a_Data.gFalse;
    END IF; 
  END signupExists;
END a_Signup;
/
