CREATE OR REPLACE PACKAGE BODY ti_core_src.a_message IS
  g_Fatal    CONSTANT VARCHAR2(1) := 'F';
  g_Error    CONSTANT VARCHAR2(1) := 'E';
  g_Warning  CONSTANT VARCHAR2(1) := 'W';
  g_Info     CONSTANT VARCHAR2(1) := 'I';
  g_Debug    CONSTANT VARCHAR2(1) := 'D';
  
  ----------------------------------------------------------------------------------------------------------------
  -- Forward declarations
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE storeMessage (p_message  IN message_log_1.mss_message%TYPE,
                          p_ussId    IN user_sessions.uss_id%TYPE,
                          p_type     IN message_log_1.mss_type%TYPE,
                          p_data     IN message_log_1.mss_data%TYPE    DEFAULT NULL,
                          p_stack    IN message_log_1.mss_stack%TYPE   DEFAULT NULL,
                          p_trace    IN message_log_1.mss_trace%TYPE   DEFAULT NULL,
                          p_taskId   IN message_log_1.mss_task_id%TYPE DEFAULT NULL);
  
  ----------------------------------------------------------------------------------------------------------------
  -- Public procs
  ----------------------------------------------------------------------------------------------------------------
/*FUNCTION errorHandler (p_error IN apex_error.t_error) RETURN apex_error.t_error_result IS
    l_result          apex_error.t_error_result;
    l_reference_id    INTEGER;
    l_constraint_name VARCHAR2(255);
  BEGIN
    l_result := apex_error.init_error_result (p_error => p_error);
    
    IF p_error.ora_sqlcode IN (-20999,-20998) THEN -- should be one of our application errors
      l_result.message := getMessageText (p_code    => apex_error.get_first_ora_error_text (p_error => p_error),
                                          p_vars    => NULL);

      IF l_result.message IS NULL THEN
        l_result.message := apex_error.get_first_ora_error_text (p_error => p_error);
      END IF;
      
      l_result.additional_info := NULL;
        
      IF p_error.ora_sqlcode = -20998 THEN -- force logout
      --apex_custom_auth.logout (p_this_app            => '110',
      --                         p_next_app_page_sess  => '110:101');
        wwv_flow_custom_auth_std.logout(p_this_flow => v('FLOW_ID'),
                                        p_next_flow_page_sess => v('FLOW_ID') || ':' || 101|| ':' || wwv_flow_custom_auth.get_next_session_id);
      END IF;
    ELSE
      -- If it's an internal error raised by APEX, like an invalid statement or
      -- code which can't be executed, the error text might contain security sensitive
      -- information. To avoid this security problem we can rewrite the error to
      -- a generic error message and log the original error message for further
      -- investigation by the help desk.
      IF p_error.is_internal_error THEN
        -- Access Denied errors raised by application or page authorization should
        -- still show up with the original error message
        IF p_error.apex_error_code <> 'APEX.AUTHORIZATION.ACCESS_DENIED' then
          -- log error for example with an autonomous transaction and return
          -- l_reference_id as reference#
          -- l_reference_id := log_error (p_error => p_error );
          --

          -- Change the message to the generic error message which doesn't expose
          -- any sensitive information.
          l_result.message := getMessageText (p_code  => 'XSYS-00001',
                                              p_vars  => NULL);
          l_result.additional_info := NULL;
        END IF;
      ELSE
        -- Always show the error as inline error
        -- Note: If you have created manual tabular forms (using the package
        --       apex_item/htmldb_item in the SQL statement) you should still
        --       use "On error page" on that pages to avoid loosing entered data
        l_result.display_location := CASE
                                       WHEN l_result.display_location = apex_error.c_on_error_page THEN apex_error.c_inline_in_notification
                                       ELSE l_result.display_location
                                     END;

        -- If it's a constraint violation like
        --
        --   -) ORA-00001: unique constraint violated
        --   -) ORA-02091: transaction rolled back (-> can hide a deferred constraint)
        --   -) ORA-02290: check constraint violated
        --   -) ORA-02291: integrity constraint violated - parent key not found
        --   -) ORA-02292: integrity constraint violated - child record found
        --
        -- Try to get a friendly error message from our constraint lookup configuration.
        -- If we don't find the constraint in our lookup table we fallback to
        -- the original ORA error message.
        IF p_error.ora_sqlcode IN (-1, -2091, -2290, -2291, -2292) THEN
          l_constraint_name := apex_error.extract_constraint_name (p_error => p_error);
  
          l_result.message := getMessageText (p_code    => p_error.ora_sqlcode,
                                              p_conName => l_constraint_name,
                                              p_vars    => NULL);
          IF l_result.message IS NULL THEN
            l_result.message := apex_error.get_first_ora_error_text (p_error => p_error);
          END IF;
        END IF;

        -- If an ORA error has been raised, for example a raise_application_error(-20xxx, '...')
        -- in a table trigger or in a PL/SQL package called by a process and we
        -- haven't found the error in our lookup table, then we just want to see
        -- the actual error text and not the full error stack with all the ORA error numbers.
        IF p_error.ora_sqlcode IS NOT NULL AND l_result.message = p_error.message THEN
          l_result.message := apex_error.get_first_ora_error_text (p_error => p_error );
        END IF;

        -- If no associated page item/tabular form column has been set, we can use
        -- apex_error.auto_set_associated_item to automatically guess the affected
        -- error field by examine the ORA error for constraint names or column names.
        IF l_result.page_item_name IS NULL AND l_result.column_alias IS NULL THEN
          apex_error.auto_set_associated_item (p_error        => p_error,
                                               p_error_result => l_result );
        END IF;
      END IF;
    END IF;
    
    storeMessage (p_message  => l_result.message,
                  p_ussId    => c_Session.getCurrentUssId,
                  p_type     => g_Fatal,
                  p_data     => a_Data.addTag(
                                  a_Data.addTag(
                                    a_Data.addTag(NULL,'ORIG_MSG',  TRIM(p_error.message)),
                                                'ORIG_CODE', TRIM(p_error.ora_sqlcode)),
                                              'APEX_CODE', TRIM(p_error.apex_error_code)),
                  p_stack    => 'Caught by a_Message.errorHandler',
                  p_trace    => p_error.error_backtrace);

    RETURN l_result;
  END errorHandler;*/

  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE logMsgCode (p_code     IN message_codes.msc_code%TYPE,
                        p_type     IN message_log_1.mss_type%TYPE,
                        p_data     IN message_log_1.mss_data%TYPE    DEFAULT NULL,
                        p_vars     IN message_log_1.mss_data%TYPE    DEFAULT NULL,
                        p_stack    IN message_log_1.mss_stack%TYPE   DEFAULT NULL,
                        p_trace    IN message_log_1.mss_trace%TYPE   DEFAULT NULL,
                        p_taskId   IN message_log_1.mss_task_id%TYPE DEFAULT NULL,
                        p_logout   IN VARCHAR2                       DEFAULT a_Data.gFalse) IS
    l_message   message_codes.msc_default_text%TYPE;
    l_errorNo   NUMBER;
    l_level     code_values.cv_value%TYPE;
  BEGIN
    l_level := c_Session.getMessageLevel;

    IF l_level = gFatal AND 
       p_type IN (gDebug, gInfo, gWarning, gError) THEN
      RETURN;
    ELSIF l_level = gError AND 
          p_type IN (gDebug, gInfo, gWarning) THEN
      RETURN;
    ELSIF l_level = gWarning AND 
          p_type IN (gDebug, gInfo) THEN
      RETURN;
    ELSIF l_level = gInfo AND 
          p_type = gDebug THEN
      RETURN;
    END IF;
    
    l_message := getMessageText (p_code    => p_code,
                                 p_conName => NULL,
                                 p_vars    => p_vars);

    -- store message and autonomously commit
    storeMessage (p_message  => l_message,
                  p_ussId    => c_Session.getCurrentUssId,
                  p_type     => p_type,
                  p_data     => a_Data.addTag(p_data  => p_data,
                                              p_name  => 'INT_CODE',
                                              p_value => p_code),
                  p_stack    => p_stack,
                  p_trace    => p_trace,
                  p_taskId   => p_taskId);

    IF p_type = gFatal THEN
      IF NVL(p_logout,a_Data.gFalse) = a_Data.gTrue THEN
        c_Session.endSession;
        l_errorNo := -20998;
      ELSE
        l_errorNo := -20999;
      END IF;
      
      IF p_vars IS NOT NULL THEN
        RAISE_APPLICATION_ERROR(l_errorNo, l_message); -- message has substitution variables so we need to raise the text, not the code
      ELSE
        RAISE_APPLICATION_ERROR(l_errorNo, p_code); -- apex will call a_Message.errorHandler to get the error text
      END IF;
    END IF;
  END logMsgCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE logMsgText (p_message  IN message_log_1.mss_message%TYPE,
                        p_type     IN message_log_1.mss_type%TYPE,
                        p_data     IN message_log_1.mss_data%TYPE    DEFAULT NULL,
                        p_stack    IN message_log_1.mss_stack%TYPE   DEFAULT NULL,
                        p_trace    IN message_log_1.mss_trace%TYPE   DEFAULT NULL,
                        p_taskId   IN message_log_1.mss_task_id%TYPE DEFAULT NULL,
                        p_logout   IN VARCHAR2                       DEFAULT a_Data.gFalse) IS
    l_level     code_values.cv_value%TYPE;
  BEGIN
    l_level := c_Session.getMessageLevel;

    IF l_level = gFatal AND 
       p_type IN (gDebug, gInfo, gWarning, gError) THEN
      RETURN;
    ELSIF l_level = gError AND 
          p_type IN (gDebug, gInfo, gWarning) THEN
      RETURN;
    ELSIF l_level = gWarning AND 
          p_type IN (gDebug, gInfo) THEN
      RETURN;
    ELSIF l_level = gInfo AND 
          p_type = gDebug THEN
      RETURN;
    END IF;
    
    -- store message and autonomously commit
    storeMessage (p_message  => p_message,
                  p_ussId    => c_Session.getCurrentUssId,
                  p_type     => p_type,
                  p_data     => p_data,
                  p_stack    => p_stack,
                  p_trace    => p_trace,
                  p_taskId   => p_taskId);

    IF p_type = gFatal THEN
      IF NVL(p_logout,a_Data.gFalse) = a_Data.gTrue THEN
        c_Session.endSession;
      END IF;
      
      RAISE_APPLICATION_ERROR(-20999, SUBSTR(p_message,1,512)); -- max length of Oracle error message text is 512 chars
    END IF;
  END logMsgText;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION taskErrorsWereLogged (p_taskId INTEGER) RETURN VARCHAR2 IS
    l_chk  INTEGER;
  BEGIN
    BEGIN
      SELECT 1
        INTO l_chk
        FROM message_log_1
       WHERE mss_uss_id  = c_Session.getCurrentUssId
         AND mss_task_id = p_taskId
         AND mss_type    = gError
         AND ROWNUM      = 1;
         
      RETURN a_Data.gTrue;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
     
    BEGIN
      SELECT 1
        INTO l_chk
        FROM message_log_2
       WHERE mss_uss_id  = c_Session.getCurrentUssId
         AND mss_task_id = p_taskId
         AND mss_type    = gError
         AND ROWNUM      = 1;
         
      RETURN a_Data.gTrue;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    
    RETURN a_Data.gFalse;
  END taskErrorsWereLogged;

  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getNewTaskId RETURN INTEGER IS
    l_out INTEGER;
  BEGIN
    SELECT mss_task_seq.NEXTVAL
      INTO l_out
      FROM dual;
      
    RETURN l_out;
  END getNewTaskId;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION pipeMessages (p_taskId IN message_log_1.mss_task_id%TYPE,
                         p_type   IN message_log_1.mss_type%TYPE DEFAULT NULL) RETURN t_MessageLog1.g_Rows PIPELINED IS
  BEGIN
    FOR r_out IN (SELECT *
                    FROM TABLE(pipeMessages(p_type => p_type))
                   WHERE mss_task_id = p_taskId) LOOP
      PIPE ROW (r_out);
    END LOOP;
    
    RETURN;
  END pipeMessages;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION pipeMessages (p_type   IN message_log_1.mss_type%TYPE DEFAULT NULL) RETURN t_MessageLog1.g_Rows PIPELINED IS
  BEGIN
    FOR r_out IN (SELECT *
                    FROM message_log_1
                   WHERE mss_uss_id  = c_Session.getCurrentUssId
                     AND (p_type IS NULL OR
                          (p_type IS NOT NULL AND
                           mss_type = p_type))
                  UNION ALL
                  SELECT *
                    FROM message_log_2
                   WHERE mss_uss_id  = c_Session.getCurrentUssId
                     AND (p_type IS NULL OR
                          (p_type IS NOT NULL AND
                           mss_type = p_type))) LOOP
      PIPE ROW (r_out);
    END LOOP;
             
    RETURN;
  END pipeMessages;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE sendMail (p_subject IN VARCHAR2,
                      p_message IN VARCHAR2,
                      p_to      IN VARCHAR2) IS
  BEGIN
    c_Mail.queueMsg (p_subject  => p_subject,
                     p_body     => p_message,
                     p_to       => p_to);
  END sendMail;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION gFatal RETURN VARCHAR2 IS 
  BEGIN
    RETURN g_Fatal;
  END gFatal;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION gError RETURN VARCHAR2 IS
  BEGIN
    RETURN g_Error;
  END gError;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION gWarning RETURN VARCHAR2 IS
  BEGIN
    RETURN g_Warning;
  END gWarning;

  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION gInfo RETURN VARCHAR2 IS
  BEGIN
    RETURN g_Info;
  END gInfo;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION gDebug RETURN VARCHAR2 IS
  BEGIN
    RETURN g_Debug;
  END gDebug;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getMessageText (p_code    IN VARCHAR2,
                           p_conName IN VARCHAR2 DEFAULT NULL,
                           p_vars    IN tt_data  DEFAULT NULL) RETURN message_codes.msc_default_text%TYPE IS
    l_out   message_codes.msc_default_text%TYPE;
    l_mscId message_codes.msc_id%TYPE;
    l_lngId languages.lng_id%TYPE;
  BEGIN
    IF p_code IS NULL THEN
      logMsgCode (p_code   => 'XSYS-00001',
                  p_type   => g_Error,
                  p_stack  => 'p_code supplied as NULL to a_Message.getMessageText',
                  p_data   => p_vars);
    END IF;
    
    l_mscId := t_MessageCodes.getId_UK(p_uk  => p_code,
                                       p_uk2 => p_conName);
    IF l_mscId IS NULL THEN
      l_mscId := t_MessageCodes.getId_UK(p_uk  => p_code,
                                         p_uk2 => NULL);
    END IF;

    l_lngId := c_Session.getCurrentLngID;
  
    IF l_lngId IS NOT NULL THEN
      l_out := t_MessageText.getText_UK(p_uk  => l_mscId,
                                        p_uk2 => l_lngId);
    END IF;
    
    IF l_out IS NULL THEN
      l_out := t_MessageCodes.getDefaultText_PK(p_pk => l_mscId);
    END IF;
    
    FOR r_subs IN (SELECT msu_code, tag_name, tag_value
                     FROM message_substitutes
                       FULL JOIN TABLE(a_Data.getTags(p_vars)) ON msu_code = tag_name
                    WHERE (msu_msc_id = l_mscId OR
                           msu_msc_id IS NULL)) LOOP
      IF r_subs.msu_code IS NULL THEN
        logMsgCode (p_code   => 'XSYS-00001',
                    p_type   => g_Error,
                    p_stack  => 'Substitution variable supplied that is not allowed in the message. '||r_subs.tag_name,
                    p_data   => p_vars);
      ELSIF r_subs.tag_name IS NULL AND
            INSTR(l_out,'<'||r_subs.msu_code||'>') != 0 THEN
        logMsgCode (p_code   => 'XSYS-00001',
                    p_type   => g_Error,
                    p_stack  => 'Substitution variable exists in message text, but no value has been supplied. '||r_subs.msu_code,
                    p_data   => p_vars);
      END IF;
      
      l_out := REPLACE(l_out,'<'||r_subs.msu_code||'>',r_subs.tag_value);
    END LOOP;
    
    IF REGEXP_COUNT(l_out, '([<]{1}[[:upper:][:digit:]]+[>]{1})') != 0 THEN -- looks for <UPPERCASE and/or DIGITS> in the loutput string
      logMsgCode (p_code   => 'XSYS-00001',
                  p_type   => g_Error,
                  p_stack  => 'Some substitution vars were not populated for message code '||p_code,
                  p_data   => p_vars);
    END IF;
    
    RETURN l_out;
  END getMessageText;
  
  ----------------------------------------------------------------------------------------------------------------
  -- Private procs
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE storeMessage (p_message  IN message_log_1.mss_message%TYPE,
                          p_ussId    IN user_sessions.uss_id%TYPE,
                          p_type     IN message_log_1.mss_type%TYPE,
                          p_data     IN message_log_1.mss_data%TYPE    DEFAULT NULL,
                          p_stack    IN message_log_1.mss_stack%TYPE   DEFAULT NULL,
                          p_trace    IN message_log_1.mss_trace%TYPE   DEFAULT NULL,
                          p_taskId   IN message_log_1.mss_task_id%TYPE DEFAULT NULL) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    l_row    message_log_1%ROWTYPE := NULL;
    l_mssId  INTEGER;
  BEGIN    
    l_row.mss_uss_id  := p_ussId;
    l_row.mss_date    := CURRENT_TIMESTAMP;
    l_row.mss_type    := p_type;
    l_row.mss_data    := p_data;
    l_row.mss_message := p_message;
    l_row.mss_stack   := p_stack;
    l_row.mss_trace   := p_trace;
    l_row.mss_task_id := p_taskId;
  
    IF t_CodeValues.getValue_PK('config_values', 'message_table') = '1' THEN
      t_messageLog1.insertRow (p_row => l_row, p_newPk => l_mssId);
    ELSE
      t_messageLog2.insertRow (p_row => l_row, p_newPk => l_mssId);
    END IF;

    COMMIT;
  END storeMessage;
  
  ----------------------------------------------------------------------------------------------------------------
  
END a_message;
/
