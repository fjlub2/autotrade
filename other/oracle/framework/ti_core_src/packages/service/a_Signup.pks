CREATE OR REPLACE PACKAGE ti_core_src.a_Signup IS 
/* ----------------------------------------------------------------------------------------------------------
   Service APIs for related to the signup wizard
   
   ---------------------------------------------------------------------------------------------------------- */
                           
 /* Store the signup details and send the registration email */
  PROCEDURE createSignup (p_first_name IN signup.sgn_first_name%TYPE,
                          p_last_name  IN signup.sgn_last_name%TYPE,
                          p_email      IN signup.sgn_email%TYPE,
                          p_terms      IN signup.sgn_terms_accept_ind%TYPE,
                          p_ip         IN signup.sgn_ip%TYPE,
                          p_session_id IN signup.sgn_session_id%TYPE,
                          p_errMsgs    OUT tt_data,
                          p_valid      OUT VARCHAR2);
  
 /* Is signup active a_Data.g_True or a_Data.g_False */
  FUNCTION signupAllowed RETURN VARCHAR2;
  
 /* Create user from signup data */
  PROCEDURE createUser (p_code IN temp_links.tln_link_code%TYPE,
                        p_pw   IN VARCHAR2);
                        
 /* Function to get the sgn_id for the supplied tln_id */
   FUNCTION getSgnIdFromTlnId (p_tlnId IN temp_links.tln_id%TYPE) RETURN signup.sgn_id%TYPE;
END a_Signup;
/
