CREATE OR REPLACE PACKAGE BODY ti_core_src.a_data IS
  g_true  CONSTANT VARCHAR2(1) := 'Y';
  g_false CONSTANT VARCHAR2(1) := 'N';
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION addTag (p_data  IN tt_data,
                   p_name  IN VARCHAR2,
                   p_value IN VARCHAR2) RETURN tt_data IS
    l_out  tt_data;
    l_data tr_data;
  BEGIN
    IF p_name IS NULL THEN
      RAISE_APPLICATION_ERROR(-20000, 'Tag name must be provided');
    END IF;
    
    l_data := tr_data(p_name,p_value);
    
    IF p_data IS NULL THEN
      l_out := tt_data(l_data);
    ELSE
      l_out := p_data;
      l_out.EXTEND();
      l_out(l_out.LAST) := l_data;
    END IF;
    
    RETURN l_out;
  END addTag;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION addTag_date (p_data  IN tt_data,
                        p_name  IN VARCHAR2,
                        p_value IN DATE) RETURN tt_data IS
    l_data tt_data;
  BEGIN
    l_data := addTag (p_data  => p_data,
                      p_name  => p_name,
                      p_value => TO_CHAR(p_value,'YYYYMMDD'));
    
    RETURN l_data;
  END addTag_date;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION addTag (p_data  IN tt_data,
                   p_value IN VARCHAR2) RETURN tt_data IS
    l_data tt_data;
  BEGIN    
    l_data := a_data.addTag(p_data  => p_data,
                            p_name  => 'VALUE',
                            p_value => p_value);
    
    RETURN l_data;
  END addTag;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION addTag_date (p_data  IN tt_data,
                        p_value IN DATE) RETURN tt_data IS
    l_data tt_data;
  BEGIN
    l_data := a_data.addTag_date(p_data  => p_data,
                                 p_name  => 'VALUE',
                                 p_value => p_value);
    
    RETURN l_data;
  END addTag_date;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getTags (p_data IN tt_data) RETURN tt_data PIPELINED IS
    l_name VARCHAR2(4000);
    l_data VARCHAR2(4000);
    l_out  tr_data;
  BEGIN
    IF p_data IS NULL THEN
      RAISE NO_DATA_FOUND;
    END IF;
    
    FOR i IN 1..p_data.COUNT LOOP
      PIPE ROW (p_data(i));
    END LOOP;
    
    RETURN;
  END getTags;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getTags (p_data IN tt_data,
                    p_name IN VARCHAR2) RETURN tt_data PIPELINED IS
    l_out  tt_data;
  BEGIN
    SELECT tr_data(tag_name, tag_value)
      BULK COLLECT INTO l_out
      FROM TABLE(a_data.getTags(p_data))
     WHERE tag_name = NVL(p_name,'VALUE');
        
    FOR i IN 1..l_out.COUNT LOOP
      PIPE ROW (l_out(i));
    END LOOP;
    
    RETURN;
  END getTags;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getTag (p_data IN tt_data,
                   p_name IN VARCHAR2) RETURN VARCHAR2 IS
    l_out  VARCHAR2(4000);
  BEGIN
    SELECT tag_value
      INTO l_out
      FROM TABLE(a_data.getTags(p_data, p_name));
    
    RETURN l_out;
  EXCEPTION
    WHEN NO_DATA_FOUND OR 
         TOO_MANY_ROWS THEN
      RETURN NULL;
  END getTag;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getTag_num (p_data IN tt_data,
                       p_name IN VARCHAR2) RETURN NUMBER IS
    l_temp VARCHAR2(4000);
    l_out  NUMBER;
  BEGIN
    l_temp := a_data.getTag(p_data => p_data,
                            p_name => p_name);
                            
    l_out := TO_NUMBER(l_temp);
    
    RETURN l_out;
  EXCEPTION
    WHEN VALUE_ERROR OR
         INVALID_NUMBER THEN
      RETURN NULL;
  END getTag_num;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getTag_date (p_data IN tt_data,
                        p_name IN VARCHAR2) RETURN DATE IS
    l_temp VARCHAR2(4000);
    l_out  DATE;
  BEGIN
    l_temp := a_data.getTag(p_data => p_data,
                            p_name => p_name);
                            
    l_out := TO_DATE(l_temp,'YYYYMMDD');
    
    RETURN l_out;
  EXCEPTION
    WHEN VALUE_ERROR OR
         INVALID_NUMBER THEN
      RETURN NULL;
  END getTag_date;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getTag (p_data IN tt_data) RETURN VARCHAR2 IS
    l_out  VARCHAR2(4000);
  BEGIN
    l_out := a_data.getTag(p_data => p_data,
                           p_name => 'VALUE');
    
    RETURN l_out;
  END getTag;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getTag_num (p_data IN tt_data) RETURN NUMBER IS
    l_out  NUMBER;
  BEGIN
    l_out := a_data.getTag_num(p_data => p_data,
                               p_name => 'VALUE');
    
    RETURN l_out;
  END getTag_num;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getTag_date (p_data IN tt_data) RETURN DATE IS
    l_out  DATE;
  BEGIN
    l_out := a_data.getTag_date(p_data => p_data,
                                p_name => 'VALUE');
    
    RETURN l_out;
  END getTag_date;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION valueInData (p_data IN tt_data,
                        p_name IN VARCHAR2,
                        p_val  IN VARCHAR2) RETURN VARCHAR2 IS
    l_chk INTEGER;
  BEGIN
    IF p_name IS NULL THEN
      RAISE_APPLICATION_ERROR(-20000, 'Tag name must be provided');
    END IF;
    
    IF p_val IS NULL THEN
      RAISE_APPLICATION_ERROR(-20000, 'Tag value must be provided');
    END IF;
    
    SELECT 1
      INTO l_chk
      FROM TABLE(a_data.getTags(p_data, p_name))
     WHERE tag_value = p_val
       AND ROWNUM    = 1; 
       
    RETURN a_data.gTrue;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN a_data.gFalse;
  END valueInData;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION valueInData (p_data IN tt_data,
                        p_val  IN VARCHAR2) RETURN VARCHAR2 IS
    l_out VARCHAR2(1);
  BEGIN
    l_out := a_data.valueInData (p_data => p_data,
                                 p_name => 'VALUE',
                                 p_val  => p_val);
       
    RETURN l_out;
  END valueInData;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION isTrue   (p_val  VARCHAR2) RETURN BOOLEAN IS
  BEGIN
    IF NVL(p_val,g_false) = g_true THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  END isTrue;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION isFalse  (p_val  VARCHAR2) RETURN BOOLEAN IS
  BEGIN
    IF NVL(p_val,g_true) = g_false THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  END isFalse;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION boolToChar (p_bool BOOLEAN) RETURN VARCHAR2 IS
  BEGIN
    IF p_bool THEN
      RETURN g_true;
    ELSE
      RETURN g_false;
    END IF;
  END boolToChar;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION gTrue RETURN VARCHAR2 IS
  BEGIN
    RETURN g_true;
  END gTrue;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION gFalse RETURN VARCHAR2 IS
  BEGIN
    RETURN g_false;
  END gFalse;
END a_data;
/
