CREATE OR REPLACE PACKAGE ti_core_src.a_User IS 
/* ----------------------------------------------------------------------------------------------------------
   Service APIs for related to users
   
   ---------------------------------------------------------------------------------------------------------- */

 /* Update user details */
 /* PROCEDURE updateUser (p_firstName    IN users.usr_first_name%TYPE,
                        p_lastName     IN users.usr_last_name%TYPE,
                        p_company      IN users.usr_company%TYPE,
                        p_email        IN users.usr_email%TYPE,
                        p_mobile_no    IN users.usr_mobile_no%TYPE,
                        p_business_no  IN users.usr_business_no%TYPE,
                        p_home_no      IN users.usr_home_no%TYPE,
                        p_lngId        IN users.usr_lng_id%TYPE,
                        p_bill_add_ind IN users.usr_bill_add_same_as_res_ind%TYPE,
                        p_dob          IN users.usr_dob%TYPE);*/
                        
 /* Update username */
  PROCEDURE updateUsername (p_username  IN users.usr_username%TYPE,
                            p_pw        IN VARCHAR2);
                            
 /* Update vision */
  --PROCEDURE updateVision (p_vision  IN users.usr_vision%TYPE);

 /* Update objective */
  --PROCEDURE updateObjective (p_objective  IN users.usr_objective%TYPE);
  
  /* sets context for the current session - allows data to be filtered for that user */
  PROCEDURE setContext (p_sessionCode IN user_sessions.uss_code%TYPE);
  
  /* removes context values from current session */
  PROCEDURE clearContext;
  
  /* stores the session info on the database */
  PROCEDURE createSession (p_username    IN  users.usr_username%TYPE,
                           p_sessionId   IN  user_sessions.uss_session_id%TYPE,
                           p_userIP      IN  user_sessions.uss_user_ip%TYPE,
                           p_pw          IN  VARCHAR2,
                           p_lngCode     IN  VARCHAR2,
                           p_sessionCode OUT user_sessions.uss_code%TYPE);
                           
  /* ends the specified session. Don't rely on session to be ended as the user may not have logged out */
  PROCEDURE endSession;
  
  /* return answers. id = icon id, ic = icon name */
  PROCEDURE getCaptchaAnswer (p_ansId   OUT captcha_icons.cv_code%TYPE,
                              p_ansName OUT captcha_icons.cv_name%TYPE);
                              
 /* Change a users password while logged in */
  PROCEDURE updatePassword (p_oldPw  IN VARCHAR2,
                            p_newPw  IN VARCHAR2);
                            
 /* Change a users password using a temp link to identify the user */
  PROCEDURE updatePasswordExt (p_newPw  IN VARCHAR2,
                               p_code   IN VARCHAR2);

 /* Validates the supplied password against the username - returns a boolean
    Called by the APEX login screen */
  FUNCTION checkPassword (p_username   IN VARCHAR2,
                          p_password   IN VARCHAR2) RETURN BOOLEAN;
                          
 /* Resets the password of a user and emails it to them */
  PROCEDURE resetPassword (p_email   IN users.usr_email%TYPE);
  
 /* Check temp link exists, is current and has not been used */
  FUNCTION checkTempLink (p_type IN temp_links.tln_type%TYPE,
                          p_code IN temp_links.tln_link_code%TYPE) RETURN VARCHAR2;
                          
 /* Checks that the session is valid and active.
    Called by the APEX Authentication Scheme */
  FUNCTION validSession (p_role IN VARCHAR2 DEFAULT NULL) RETURN BOOLEAN;
  
 /* Gets the users current language name */
  FUNCTION getUserLngName RETURN VARCHAR2;
  
 /* Get the user ID for the supplied email address */
  FUNCTION getUsrIdFromEmail (p_email IN users.usr_email%TYPE) RETURN users.usr_id%TYPE;
  
 /* Force the user to change their password */
  FUNCTION changePwOnLogon RETURN VARCHAR2;
  
 /* Get current users info */
  FUNCTION getUserRow RETURN users%ROWTYPE;
END a_User;
/
