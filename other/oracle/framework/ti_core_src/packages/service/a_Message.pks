CREATE OR REPLACE PACKAGE ti_core_src.a_message IS
  /*********************************************************
   Package that can handles various messaging and error
   handling functions.

  **********************************************************/
  
 /* Error handler that APEX calls 
  FUNCTION errorHandler (p_error IN apex_error.t_error) RETURN apex_error.t_error_result; */

 /* Procedure to log a message for the supplied code and raise an error if needed */
  PROCEDURE logMsgCode (p_code     IN message_codes.msc_code%TYPE,
                        p_type     IN message_log_1.mss_type%TYPE,
                        p_data     IN message_log_1.mss_data%TYPE    DEFAULT NULL,
                        p_vars     IN message_log_1.mss_data%TYPE    DEFAULT NULL,
                        p_stack    IN message_log_1.mss_stack%TYPE   DEFAULT NULL,
                        p_trace    IN message_log_1.mss_trace%TYPE   DEFAULT NULL,
                        p_taskId   IN message_log_1.mss_task_id%TYPE DEFAULT NULL,
                        p_logout   IN VARCHAR2                       DEFAULT a_Data.gFalse);

 /* Procedure to log a message with the supplied text and raise an error if needed */
  PROCEDURE logMsgText (p_message  IN message_log_1.mss_message%TYPE,
                        p_type     IN message_log_1.mss_type%TYPE,
                        p_data     IN message_log_1.mss_data%TYPE    DEFAULT NULL,
                        p_stack    IN message_log_1.mss_stack%TYPE   DEFAULT NULL,
                        p_trace    IN message_log_1.mss_trace%TYPE   DEFAULT NULL,
                        p_taskId   IN message_log_1.mss_task_id%TYPE DEFAULT NULL,
                        p_logout   IN VARCHAR2                       DEFAULT a_Data.gFalse);
                        
 /* Returns a_Data.gTrue if errors were logged for the
    task, a_Data.gFalse if none were found */
  FUNCTION taskErrorsWereLogged (p_taskId INTEGER) RETURN VARCHAR2;
                     
 /* get a new task id */
  FUNCTION getNewTaskId RETURN INTEGER;
  
  /* get the messages for a task */
  FUNCTION pipeMessages (p_taskId IN message_log_1.mss_task_id%TYPE,
                         p_type   IN message_log_1.mss_type%TYPE DEFAULT NULL) RETURN t_MessageLog1.g_Rows PIPELINED;
  
  /* get all session messages */
  FUNCTION pipeMessages (p_type   IN message_log_1.mss_type%TYPE DEFAULT NULL) RETURN t_MessageLog1.g_Rows PIPELINED;
       
 /* send an external email */
  PROCEDURE sendMail (p_subject IN VARCHAR2,
                      p_message IN VARCHAR2,
                      p_to      IN VARCHAR2);

  /* Returns the fatal error message type.
     Use to store error and terminate process.
     Messages are visible by the user */
  FUNCTION gFatal RETURN VARCHAR2;
  
  /* Returns the error message type.
     Use to store error. Process will
     continue - to be terminated later after
     additional messages are stored.
     Messages are visible by the user */
  FUNCTION gError RETURN VARCHAR2;
  
  /* Returns the warning message type. 
     Use to store a warning. Process will continue.
     Messages are visible by the user */
  FUNCTION gWarning RETURN VARCHAR2;
  
  /* Returns the info message type.
     Use to store an info message. Process will
     continue. Messages are visible by the user */
  FUNCTION gInfo RETURN VARCHAR2;
  
  /* Returns the info message type.
     Use to store a debug message. Process will
     continue. Messages are not visible by the user */
  FUNCTION gDebug RETURN VARCHAR2;
  
  /* Returns the message text for the supplied code and language */
  FUNCTION getMessageText (p_code    IN VARCHAR2,
                           p_conName IN VARCHAR2 DEFAULT NULL,
                           p_vars    IN tt_data  DEFAULT NULL) RETURN message_codes.msc_default_text%TYPE;
END a_message;
/

