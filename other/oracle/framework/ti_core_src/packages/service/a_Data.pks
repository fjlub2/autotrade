CREATE OR REPLACE PACKAGE ti_core_src.a_data IS
  /*********************************************************
   Package that handles various generic data manipulation
   functions.
   
   Should only be used for small processes.
  **********************************************************/
  
  /* Appends a new value to the supplied data string. Supplied data string can be NULL */
  FUNCTION addTag (p_data  IN tt_data,
                   p_name  IN VARCHAR2,
                   p_value IN VARCHAR2) RETURN tt_data;
                   
  /* Appends a new date to the supplied data string in YYYYMMDD format. Supplied data string can be NULL */
  FUNCTION addTag_date (p_data  IN tt_data,
                        p_name  IN VARCHAR2,
                        p_value IN DATE) RETURN tt_data;
                        
  /* Appends a new value to the supplied data string. Supplied data string can be NULL.
     Tag name will be set to VALUE */
  FUNCTION addTag (p_data  IN tt_data,
                   p_value IN VARCHAR2) RETURN tt_data;
                   
  /* Appends a new date to the supplied data string in YYYYMMDD format. Supplied data string can be NULL.
     Tag name will be set to VALUE */
  FUNCTION addTag_date (p_data  IN tt_data,
                        p_value IN DATE) RETURN tt_data;
                        
  /* Returns all tags as VARCHAR2 that are stored in the supplied data */
  FUNCTION getTags (p_data IN tt_data) RETURN tt_data PIPELINED;
  
  /* Returns all tags as VARCHAR2 with the specified name that are stored in the supplied data */
  FUNCTION getTags (p_data IN tt_data,
                    p_name IN VARCHAR2) RETURN tt_data PIPELINED;
                    
  /* Returns the char value of the tag with the specified name that is stored in the supplied data
     TOO_MANY_ROWS and NO_DATA_FOUND will result in a NULL return value */
  FUNCTION getTag (p_data IN tt_data,
                   p_name IN VARCHAR2) RETURN VARCHAR2;
                   
  /* Returns the numeric value of the tag with the specified name that is stored in the supplied data
     TOO_MANY_ROWS, NO_DATA_FOUND, INVALID_NUMBER and VALUE_ERROR will result in a NULL return value */
  FUNCTION getTag_num (p_data IN tt_data,
                       p_name IN VARCHAR2) RETURN NUMBER;
                       
  /* Returns the date value of the tag with the specified name that is stored in the supplied data
     TOO_MANY_ROWS, NO_DATA_FOUND, INVALID_NUMBER and VALUE_ERROR will result in a NULL return value.
     Date should be stored in the tag as YYYYMMDD. */
  FUNCTION getTag_date (p_data IN tt_data,
                        p_name IN VARCHAR2) RETURN DATE;
                   
  /* Returns the value of the tag with the name VALUE that is stored in the supplied data
     TOO_MANY_ROWS and NO_DATA_FOUND will result in a NULL return value */
  FUNCTION getTag (p_data IN tt_data) RETURN VARCHAR2;
  
  /* Returns the numberic value of the tag with the name VALUE that is stored in the supplied data
     TOO_MANY_ROWS, NO_DATA_FOUND, INVALID_NUMBER and VALUE_ERROR will result in a NULL return value */
  FUNCTION getTag_num (p_data IN tt_data) RETURN NUMBER;
  
  /* Returns the date value of the tag with the name VALUE that is stored in the supplied data
     TOO_MANY_ROWS, NO_DATA_FOUND, INVALID_NUMBER and VALUE_ERROR will result in a NULL return value.
     Date should be stored in the tag as YYYYMMDD. */
  FUNCTION getTag_date (p_data IN tt_data) RETURN DATE;
  
  /* Returns a_bool.gTrue if the supplied value is found within the supplied data with the
     specified name, a_bool.gFalse if it is not. Multiple rows can be evaluated. */
  FUNCTION valueInData (p_data IN tt_data,
                        p_name IN VARCHAR2,
                        p_val  IN VARCHAR2) RETURN VARCHAR2;
                        
  /* Returns a_bool.gTrue if the supplied value is found within the supplied data with the
     name VALUE, a_bool.gFalse if it is not. Multiple rows can be evaluated. */
  FUNCTION valueInData (p_data IN tt_data,
                        p_val  IN VARCHAR2) RETURN VARCHAR2;
                       
  /* Returns a boolean TRUE if the supplied value is the value corresponding to TRUE, FALSE if it is not */
  FUNCTION isTrue  (p_val  VARCHAR2) RETURN BOOLEAN;

  /* Returns a boolean TRUE if the supplied value is the value corresponding to FALSE, FALSE if it is not */
  FUNCTION isFalse (p_val  VARCHAR2) RETURN BOOLEAN;

  /* Returns the value corresponding to the supplied boolean. e.g. Y or N */
  FUNCTION boolToChar  (p_bool BOOLEAN) RETURN VARCHAR2;
  
  /* Returns the value corresponding to TRUE */
  FUNCTION gTrue RETURN VARCHAR2;

  /* Returns the value corresponding to FALSE */
  FUNCTION gFalse RETURN VARCHAR2;
END a_data;
/
