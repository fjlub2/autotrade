CREATE OR REPLACE PACKAGE BODY ti_core_src.a_User IS 
/*  PROCEDURE updateUser (p_firstName    IN users.usr_first_name%TYPE,
                        p_lastName     IN users.usr_last_name%TYPE,
                        p_company      IN users.usr_company%TYPE,
                        p_email        IN users.usr_email%TYPE,
                        p_mobile_no    IN users.usr_mobile_no%TYPE,
                        p_business_no  IN users.usr_business_no%TYPE,
                        p_home_no      IN users.usr_home_no%TYPE,
                        p_lngId        IN users.usr_lng_id%TYPE,
                        p_bill_add_ind IN users.usr_bill_add_same_as_res_ind%TYPE,
                        p_dob          IN users.usr_dob%TYPE) IS
    l_data    t_Users.g_updateRow       := NULL;
    l_ignore  t_Users.g_updateIgnoreRow := NULL;
  BEGIN
    ti_core_src.c_Session.validSession;
    
    -- say which columns should remain unchanged
    l_ignore.usr_status             := a_data.gTrue;
    l_ignore.usr_password           := a_data.gTrue;  -- passwords handled by c_Security package
    l_ignore.usr_pwd_tries          := a_data.gTrue;
    l_ignore.usr_change_pw_on_logon := a_data.gTrue;
    l_ignore.usr_username           := a_data.gTrue;
    l_ignore.usr_vision             := a_data.gTrue;
    l_ignore.usr_objective          := a_data.gTrue;
    l_ignore.usr_message_level      := a_data.gTrue;
    
    -- set the values to be updated
    l_data.usr_first_name               := p_firstName;
    l_data.usr_last_name                := p_lastName;
    l_data.usr_company                  := p_company;
    l_data.usr_email                    := p_email;
    l_data.usr_mobile_no                := p_mobile_no;
    l_data.usr_business_no              := p_business_no;
    l_data.usr_home_no                  := p_home_no;
    l_data.usr_bill_add_same_as_res_ind := p_bill_add_ind;
    l_data.usr_dob                      := p_dob;
    l_data.usr_lng_id                   := p_lngId;
    
    t_Users.updateRow(p_pk     => ti_core_src.c_Session.getCurrentUsrId,
                      p_row    => l_data,
                      p_ignore => l_ignore);
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      a_Message.logMsgCode (p_code => 'XAPP-00001', -- email already in use
                            p_type => a_Message.gFatal);
  END updateUser;*/
  
  ----------------------------------------------------------------------------------------------------------------
 
  PROCEDURE updateUsername (p_username  IN users.usr_username%TYPE,
                            p_pw        IN VARCHAR2) IS
  BEGIN
    ti_core_src.c_Session.validSession;
    
    IF ti_core_src.c_Security.checkPassword(p_usrId => ti_core_src.c_Session.getCurrentUsrId,
                                            p_pw    => p_pw) = a_Data.gTrue THEN                  
      t_Users.setUsername(p_pk  => ti_core_src.c_Session.getCurrentUsrId,
                          p_val => p_username);
                        
      a_Message.sendMail(p_subject => 'New logon details',
                         p_message => 'Your username has been updated to '||p_username||'.',
                         p_to      => t_Users.getEmail_PK (p_pk  => ti_core_src.c_Session.getCurrentUsrId));
    ELSE
      a_Message.logMsgCode (p_code => 'XAPP-00002', -- incorrect password
                            p_type => a_Message.gFatal);
    END IF;
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      a_Message.logMsgCode (p_code => 'XAPP-00003', -- username already in use
                            p_type => a_Message.gFatal);
  END updateUsername;
  
  ----------------------------------------------------------------------------------------------------------------
 /*
  PROCEDURE updateVision (p_vision  IN users.usr_vision%TYPE) IS
  BEGIN
    ti_core_src.c_Session.validSession;
    
    t_Users.setVision(p_pk  => ti_core_src.c_Session.getCurrentUsrId,
                      p_val => p_vision);
  END updateVision;
  
  ----------------------------------------------------------------------------------------------------------------
 
  PROCEDURE updateObjective (p_objective  IN users.usr_objective%TYPE) IS
  BEGIN
    ti_core_src.c_Session.validSession;
    
    t_Users.setObjective(p_pk  => ti_core_src.c_Session.getCurrentUsrId,
                         p_val => p_objective);
  END updateObjective;
  */
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setContext (p_sessionCode IN user_sessions.uss_code%TYPE) IS
  BEGIN
    ti_core_src.c_Session.setContext(p_sessionCode => p_sessionCode);
  END setContext;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE clearContext IS
  BEGIN
    ti_core_src.c_Session.clearContext;
  END clearContext;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE createSession (p_username    IN  users.usr_username%TYPE,
                           p_sessionId   IN  user_sessions.uss_session_id%TYPE,
                           p_userIP      IN  user_sessions.uss_user_ip%TYPE,
                           p_pw          IN  VARCHAR2,
                           p_lngCode     IN  VARCHAR2,
                           p_sessionCode OUT user_sessions.uss_code%TYPE) IS
  BEGIN
    ti_core_src.c_Session.createSession(p_username    => p_username,
                                        p_sessionId   => p_sessionId,
                                        p_userIP      => p_userIP,
                                        p_pw          => p_pw,
                                        p_lngCode     => p_lngCode,
                                        p_sessionCode => p_sessionCode);
  END createSession;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE endSession IS
  BEGIN
    ti_core_src.c_Session.endSession;
  END endSession;
  
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE getCaptchaAnswer (p_ansId   OUT captcha_icons.cv_code%TYPE,
                              p_ansName OUT captcha_icons.cv_name%TYPE) IS
  BEGIN
    ti_core_src.c_Session.getCaptchaAnswer (p_ansId   => p_ansId,
                                            p_ansName => p_ansName);
  END getCaptchaAnswer;
  
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updatePassword (p_oldPw  IN VARCHAR2,
                            p_newPw  IN VARCHAR2) IS
  BEGIN
    ti_core_src.c_Security.updatePassword (p_oldPw => p_oldPw,
                                           p_newPw => p_newPw);
  END updatePassword;
  
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updatePasswordExt (p_newPw  IN VARCHAR2,
                               p_code   IN VARCHAR2) IS
  BEGIN
    ti_core_src.c_Security.updatePasswordExt (p_newPw => p_newPw,
                                              p_code  => p_code);
  END updatePasswordExt;
  
  ----------------------------------------------------------------------------------------------------------------

  FUNCTION checkPassword (p_username   IN VARCHAR2,
                          p_password   IN VARCHAR2) RETURN BOOLEAN IS
  BEGIN                        
    RETURN ti_core_src.c_Security.checkPassword (p_username => p_username,
                                                 p_password => p_password);
  END checkPassword;
  
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE resetPassword (p_email   IN users.usr_email%TYPE) IS
  BEGIN
    ti_core_src.c_Security.resetPassword (p_email => p_email);
  END resetPassword;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION checkTempLink (p_type IN temp_links.tln_type%TYPE,
                          p_code IN temp_links.tln_link_code%TYPE) RETURN VARCHAR2 IS
  BEGIN
    RETURN ti_core_src.c_Security.checkTempLink (p_type => p_type,
                                                 p_code => p_code);
  END checkTempLink;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION validSession (p_role IN VARCHAR2 DEFAULT NULL) RETURN BOOLEAN IS
  BEGIN
    ti_core_src.c_Session.validSession(p_role => p_role);
    
    RETURN TRUE;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN FALSE;
  END validSession;

  ----------------------------------------------------------------------------------------------------------------
  FUNCTION getUserLngName RETURN VARCHAR2 IS
    l_lngName languages.lng_name%TYPE;
  BEGIN
    SELECT lng_name||CASE WHEN lng_name != lng_native_name THEN ' ('||lng_native_name||')' ELSE NULL END
      INTO l_lngName
      FROM languages
     WHERE lng_id = ti_core_src.c_Session.getCurrentLngID;
     
    RETURN l_lngName;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getUserLngName;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getUsrIdFromEmail (p_email IN users.usr_email%TYPE) RETURN users.usr_id%TYPE IS
    l_usrId users.usr_id%TYPE;
  BEGIN
    SELECT usr_id
      INTO l_usrId
      FROM users
     WHERE usr_email = p_email;
     
    RETURN l_usrId;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getUsrIdFromEmail;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION changePwOnLogon RETURN VARCHAR2 IS
  BEGIN                        
    RETURN t_Users.getChangePwOnLogon_PK(p_pk => ti_core_src.c_Session.getCurrentUsrId);
  END changePwOnLogon;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getUserRow RETURN users%ROWTYPE IS
  BEGIN
    RETURN t_Users.getRow_PK (p_pk => ti_core_src.c_Session.getCurrentUsrId);
  END getUserRow;
  
  ----------------------------------------------------------------------------------------------------------------
END a_User;
/
