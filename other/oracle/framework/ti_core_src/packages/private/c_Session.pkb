CREATE OR REPLACE PACKAGE BODY ti_core_src.c_Session IS
  ----------------------------------------------------------------------------------------------------------------
  -- Forward declarations
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE storeSession (p_usrId     IN  users.usr_id%TYPE,
                          p_sessionId IN  user_sessions.uss_session_id%TYPE,
                          p_userIP    IN  user_sessions.uss_user_ip%TYPE,
                          p_lngId     IN  user_sessions.uss_lng_id%TYPE,
                          p_code      IN  user_sessions.uss_code%TYPE,
                          p_newUssId  OUT user_sessions.uss_id%TYPE);
                
  PROCEDURE VpdSwitch (p_alias  IN VARCHAR2, -- Private only. Do not allow this to be called from the pkg spec.
                       p_active IN VARCHAR2);
                       
  ----------------------------------------------------------------------------------------------------------------
  -- Public declarations
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE setContext (p_sessionCode IN user_sessions.uss_code%TYPE) IS
    l_usrId       users.usr_id%TYPE;
    l_ussId       user_sessions.uss_id%TYPE;
    l_lngId       languages.lng_id%TYPE;
    l_level       code_values.cv_value%TYPE;
    l_sessionId   user_sessions.uss_session_id%TYPE;
  BEGIN                
    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gFalse);
               
    SELECT uss_usr_id,
           uss_id,
           NVL(usr_lng_id,uss_lng_id),
           NVL(usr_message_level,t_CodeValues.getValue_PK('config_values', 'message_level')),
           uss_session_id
      INTO l_usrId,
           l_ussId,
           l_lngId,
           l_level,
           l_sessionId
      FROM user_sessions
        JOIN users ON usr_id = uss_usr_id
     WHERE uss_code = p_sessionCode
       AND uss_session_end IS NULL;
       
    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gTrue);
    
    DBMS_SESSION.SET_CONTEXT ('TI_SES_CTX',
                              'USR_ID',
                              l_usrId);
                              
    DBMS_SESSION.SET_CONTEXT ('TI_SES_CTX',
                              'USS_ID',
                              l_ussId);
                              
    DBMS_SESSION.SET_CONTEXT ('TI_SES_CTX',
                              'LNG_ID',
                              l_lngId);
                              
    DBMS_SESSION.SET_CONTEXT ('TI_SES_CTX',
                              'SESSION_ID',
                              l_sessionId);
                              
    DBMS_SESSION.SET_CONTEXT ('TI_SES_CTX',
                              'SIGNUP',
                              'N');

    DBMS_SESSION.SET_CONTEXT ('TI_SES_CTX',
                              'MESSAGE_LEVEL',
                              l_level);
                              
    DBMS_SESSION.SET_CONTEXT ('TI_SES_CTX',
                              'SESSION_CODE',
                              p_sessionCode);
                              
    DBMS_SESSION.SET_IDENTIFIER(p_sessionCode);

    setSessionLastUsed;
    
    a_Message.logMsgText(p_message => 'Session context set.',
                         p_data    => a_Data.addTag(NULL,'USS_ID',l_ussId),
                         p_type    => a_Message.gInfo);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      a_Message.logMsgCode (p_code    => 'XAPP-00018', -- session expired
                            p_data    => a_Data.addTag(NULL,'USS_ID',    l_ussId),
                            p_type    => a_Message.gFatal,
                            p_logout  => a_Data.gTrue);
  END setContext;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE setContext IS
    l_sessionCode   user_sessions.uss_code%TYPE := SYS_CONTEXT('USERENV', 'CLIENT_IDENTIFIER');
  BEGIN
    setContext(p_sessionCode => l_sessionCode);
  END setContext;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE clearContext IS
  BEGIN
    DBMS_SESSION.CLEAR_CONTEXT('TI_SES_CTX');
    DBMS_SESSION.CLEAR_IDENTIFIER;
  END clearContext;
  
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE createSession (p_username    IN  users.usr_username%TYPE,
                           p_sessionId   IN  user_sessions.uss_session_id%TYPE,
                           p_userIP      IN  user_sessions.uss_user_ip%TYPE,
                           p_pw          IN  VARCHAR2,
                           p_lngCode     IN  VARCHAR2,
                           p_sessionCode OUT user_sessions.uss_code%TYPE) IS
    l_usrId    INTEGER;
    l_lngId    INTEGER;
    l_ussId    INTEGER;
    e_invalid  EXCEPTION;
  BEGIN
    clearContext;
    
    IF p_username  IS NULL OR
       p_sessionId IS NULL OR
       p_pw        IS NULL THEN
      RAISE e_invalid;
    END IF;
    
    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gFalse);
               
    l_usrId := t_Users.getId_UK(p_uk => p_username);
    
    IF l_usrId IS NULL THEN
      RAISE e_invalid;
    END IF;
    
    IF ti_core_src.c_Security.checkPassword(p_usrId => l_usrId,
                                            p_pw    => p_pw) = a_Data.gTrue THEN 
      IF t_CodeValues.getValue_PK('config_values', 'multi_session') = 'N' THEN
        --This UPDATE will terminate all other active sessions for the same user.
      
        UPDATE user_sessions
           SET uss_session_end  = CURRENT_TIMESTAMP 
         WHERE uss_usr_id       = l_usrId
           AND uss_session_end IS NULL;
      END IF;

      -- work out users language
      BEGIN
        l_lngId := t_Languages.getId_UK(p_uk => SUBSTR(LOWER(p_lngCode),1,2));
      EXCEPTION
        WHEN NO_DATA_FOUND  OR
             VALUE_ERROR    OR 
             INVALID_NUMBER THEN
          l_lngId := NULL;
      END;
      
      p_sessionCode := TO_CHAR(uss_code_seq.NEXTVAL)||'-'||getRandomString;
      
      storeSession (p_usrId     => l_usrId,
                    p_sessionId => p_sessionId,
                    p_userIP    => p_userIP,
                    p_lngId     => l_lngId,
                    p_code      => p_sessionCode,
                    p_newUssId  => l_ussId);
                    
      setContext(p_sessionCode => p_sessionCode);
      
      a_Message.logMsgText (p_message => 'Session started',
                            p_data    => a_Data.addTag(NULL,'USR_ID',l_usrId),
                            p_type    => a_Message.gInfo);
    ELSE
      a_Message.logMsgText (p_message => 'Session not started',
                            p_data    => a_Data.addTag(NULL,'USR_ID',l_usrId),
                            p_type    => a_Message.gInfo);

      RAISE e_invalid;
    END IF;

    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gTrue);
  EXCEPTION
    WHEN e_invalid THEN
      VpdSwitch (p_alias  => 'USR',
                 p_active => a_Data.gTrue);
                 
      a_Message.logMsgCode (p_code    => 'XAPP-00007', -- invalid login credentials
                            p_data    => a_Data.addTag(
                                           a_Data.addTag(
                                             a_Data.addTag(
                                               a_Data.addTag(NULL,'USER_IP',   p_userIP),
                                                           'SESSION_ID',p_sessionId),
                                                         'USERNAME',  p_username),
                                                       'USR_ID',    l_usrId),
                            p_type    => a_Message.gFatal);
    WHEN OTHERS THEN
      VpdSwitch (p_alias  => 'USR',
                 p_active => a_Data.gTrue);
                 
      RAISE;
  END createSession;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE endSession IS
  BEGIN
    a_Message.logMsgText (p_message => 'Session ended',
                          p_data    => a_Data.addTag(NULL,'USS_ID',getCurrentUssID),
                          p_type    => a_Message.gInfo);
                            
    IF getCurrentUssID IS NOT NULL THEN
      t_userSessions.setSessionEnd(p_pk => getCurrentUssID, p_val => CURRENT_TIMESTAMP);
    END IF;
    
    clearContext;
  END endSession;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getCurrentUsrID RETURN INTEGER IS
  BEGIN                          
    RETURN TO_NUMBER(SYS_CONTEXT ('TI_SES_CTX', 'USR_ID'));
  END getCurrentUsrID;

  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getCurrentUssID RETURN INTEGER IS
  BEGIN
    RETURN TO_NUMBER(SYS_CONTEXT ('TI_SES_CTX', 'USS_ID'));
  END getCurrentUssID;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getCurrentLngID RETURN INTEGER IS
  BEGIN
    RETURN TO_NUMBER(SYS_CONTEXT ('TI_SES_CTX', 'LNG_ID'));
  END getCurrentLngID;

  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getCurrentSessionID RETURN INTEGER IS
  BEGIN
    RETURN TO_NUMBER(SYS_CONTEXT ('TI_SES_CTX', 'SESSION_ID'));
  END getCurrentSessionID;

  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getCurrentSessionCode RETURN VARCHAR2 IS
  BEGIN
    RETURN SYS_CONTEXT ('TI_SES_CTX', 'SESSION_CODE');
  END getCurrentSessionCode;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getCurrentSessionType RETURN VARCHAR2 IS
  BEGIN
    RETURN NVL(SYS_CONTEXT ('TI_SES_CTX', 'SESSION_TYPE'),'N');
  END getCurrentSessionType;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getMessageLevel RETURN VARCHAR2 IS
  BEGIN
    RETURN NVL(SYS_CONTEXT ('TI_SES_CTX', 'MESSAGE_LEVEL'),'I');
  END getMessageLevel;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE validSession (p_role IN VARCHAR2 DEFAULT NULL) IS
    l_chk            INTEGER;
    e_invalidSession EXCEPTION;
  BEGIN
    IF getCurrentUssID IS NULL OR
       getCurrentUsrID IS NULL THEN
      RAISE e_invalidSession;
    END IF;
    
    BEGIN 
      SELECT 1
        INTO l_chk
        FROM user_sessions
       WHERE uss_id           = getCurrentUssID
         AND uss_usr_id       = getCurrentUsrID
         AND uss_session_end IS NULL;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE e_invalidSession;
    END;
    
    IF t_Users.getPwdTries_PK(p_pk => getCurrentUsrID) >= 3 THEN
      a_Message.logMsgCode (p_code    => 'XAPP-00005', -- account locked
                            p_type    => a_Message.gFatal,
                            p_logout  => a_Data.gTrue);
    END IF;
    
    /*
      add code to check role
    */
  EXCEPTION
    WHEN e_invalidSession THEN
      a_Message.logMsgCode (p_code    => 'XAPP-00006', -- context not set
                            p_type    => a_Message.gFatal,
                            p_logout  => a_Data.gTrue);
    WHEN NO_DATA_FOUND THEN
      a_Message.logMsgCode (p_code    => 'XAPP-00017', -- session expired
                            p_type    => a_Message.gFatal,
                            p_logout  => a_Data.gTrue);
  END validSession;

  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE getCaptchaAnswer (p_ansId   OUT captcha_icons.cv_code%TYPE,
                              p_ansName OUT captcha_icons.cv_name%TYPE) IS
  BEGIN
    SELECT cv_code,
           cv_name
      INTO p_ansId,
           p_ansName
      FROM (SELECT cv_code,
                   cv_name
              FROM captcha_icons
             ORDER BY dbms_random.value)
     WHERE ROWNUM = 1;
  END getCaptchaAnswer;
  
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE resetPassword (p_email   IN users.usr_email%TYPE) IS
  BEGIN
    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gFalse);
               
    ti_core_src.c_Security.resetPassword (p_email => p_email);

    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gTrue);
  EXCEPTION
    WHEN OTHERS THEN
      VpdSwitch (p_alias  => 'USR',
                 p_active => a_Data.gTrue);
                 
      RAISE;
  END resetPassword;

  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getRandomString (p_length IN INTEGER DEFAULT 40) RETURN VARCHAR2 IS -- this is not guaranteed to be unique
    l_length INTEGER := p_length;
  BEGIN
    IF l_length BETWEEN 1 AND 100 THEN
      RETURN SUBSTR(LOWER(SYS.DBMS_CRYPTO.RANDOMBYTES(l_length)),1,l_length);
    ELSE
      RETURN NULL;
    END IF;
  END getRandomString;
  

  ----------------------------------------------------------------------------------------------------------------
  /* Functions that require the VPD to be switched off while they run.
  
       It's going to be very rare that you'll need to do this, the only reason I can think of is to check
       uniqueness of an item across multiple users.
       
       Make sure you always turn the VPD back on after running.
  */
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION userExists (p_newUser IN users.usr_username%TYPE) RETURN VARCHAR2 IS
    l_chk INTEGER;
  BEGIN
    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gFalse);
   
    BEGIN
      SELECT 1
        INTO l_chk
        FROM users
       WHERE usr_username = p_newUser
         AND usr_id      != NVL(ti_core_src.c_Session.getCurrentUsrID,-999)
         AND ROWNUM       = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_chk := 0;
    END;
    
    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gTrue);
    
    IF l_chk = 1 THEN
      a_Message.logMsgText (p_message => 'Existing user found',
                            p_data    => a_Data.addTag(NULL,'USERNAME',p_newUser),
                            p_type    => a_Message.gInfo);
      RETURN a_Data.gTrue;
    ELSE
      a_Message.logMsgText (p_message => 'Existing user not found',
                            p_data    => a_Data.addTag(NULL,'USERNAME',p_newUser),
                            p_type    => a_Message.gInfo);
    
      RETURN a_Data.gFalse;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      VpdSwitch (p_alias  => 'USR',
                 p_active => a_Data.gTrue);
                 
      RAISE;   
  END userExists;

  ----------------------------------------------------------------------------------------------------------------  

  FUNCTION getUniqueTempLinkCode RETURN temp_links.tln_link_code%TYPE IS
    l_chk  INTEGER := 1;
    l_code VARCHAR2(40);
  BEGIN
    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gFalse);
   
    WHILE l_chk > 0 LOOP 
      l_code := ti_core_src.c_Session.getRandomString(40);
      
      SELECT COUNT(1)
        INTO l_chk
        FROM temp_links
       WHERE tln_link_code = l_code;
    END LOOP;
    
    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gTrue);
               
    RETURN l_code;
  EXCEPTION
    WHEN OTHERS THEN
      VpdSwitch (p_alias  => 'USR',
                 p_active => a_Data.gTrue);
                 
      RAISE;   
  END getUniqueTempLinkCode;
   
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE setSessionLastUsed IS 
    PRAGMA AUTONOMOUS_TRANSACTION;
    
    l_ussId INTEGER := getCurrentUssID;
  BEGIN
    IF l_ussId IS NOT NULL THEN
      t_userSessions.setLastUsed (p_pk  => l_ussId,
                                  p_val => CURRENT_TIMESTAMP);
    END IF;
    
    COMMIT;
  EXCEPTION WHEN OTHERS THEN
        a_Message.logMsgText (p_message => 'error'||SQLERRM,
                              p_type    => a_Message.gInfo);
  END setSessionLastUsed;
  
  ----------------------------------------------------------------------------------------------------------------
  PROCEDURE timeoutSessions IS
    l_timeOutPeriod NUMBER(10,5);
  BEGIN
    l_timeOutPeriod := TO_NUMBER(t_CodeValues.getValue_PK('config_values', 'max_session_idle'));
  
    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gFalse);
  
    FOR rc1 IN (SELECT uss_id
                  FROM user_sessions
                 WHERE uss_last_used < CURRENT_TIMESTAMP - NUMTODSINTERVAL(l_timeOutPeriod, 'HOUR')
                   AND uss_session_end IS NULL) LOOP      
      t_userSessions.setSessionEnd (p_pk  => rc1.uss_id,
                                    p_val => CURRENT_TIMESTAMP);
       
      a_Message.logMsgText (p_message => 'Session automatically timedout.',
                            p_data    => a_Data.addTag(NULL,'USS_ID',rc1.uss_id),
                            p_type    => a_Message.gInfo);
    END LOOP;
    
    VpdSwitch (p_alias  => 'USR',
               p_active => a_Data.gTrue);
               
    COMMIT;
  EXCEPTION
    WHEN INVALID_NUMBER OR VALUE_ERROR THEN
      a_Message.logMsgText (p_message => 'Config value max_session_idle is set correctly',
                            p_type    => a_Message.gError);
                            
      VpdSwitch (p_alias  => 'USR',
                 p_active => a_Data.gTrue);
    WHEN OTHERS THEN
      VpdSwitch (p_alias  => 'USR',
                 p_active => a_Data.gTrue);
                 
      RAISE;  
  END timeoutSessions;
  
  ----------------------------------------------------------------------------------------------------------------
  -- Private declarations
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE storeSession (p_usrId     IN  users.usr_id%TYPE,
                          p_sessionId IN  user_sessions.uss_session_id%TYPE,
                          p_userIP    IN  user_sessions.uss_user_ip%TYPE,
                          p_lngId     IN  user_sessions.uss_lng_id%TYPE,
                          p_code      IN  user_sessions.uss_code%TYPE,
                          p_newUssId  OUT user_sessions.uss_id%TYPE) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    
    l_row  user_sessions%ROWTYPE;
  BEGIN
    l_row.uss_usr_id        := p_usrId;
    l_row.uss_session_start := CURRENT_TIMESTAMP;
    l_row.uss_session_id    := p_sessionId;
    l_row.uss_user_ip       := p_userIP;
    l_row.uss_lng_id        := p_lngId;
    l_row.uss_code          := p_code;
    l_row.uss_last_used     := CURRENT_TIMESTAMP;
  
    t_userSessions.insertRow (p_row => l_row, p_newPk => p_newUssId);
  
    COMMIT;
  END storeSession;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE VpdSwitch (p_alias  IN VARCHAR2, -- Private only. Do not allow this to be called from the pkg spec.
                       p_active IN VARCHAR2) IS
  
  BEGIN
    DBMS_SESSION.SET_CONTEXT ('TI_SES_CTX',
                              p_alias||'_VPD',
                              p_active);
  END VpdSwitch;
END c_Session;
/
