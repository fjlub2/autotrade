CREATE OR REPLACE PACKAGE ti_core_src.c_Session IS
  /*********************************************************
   Package that can handles various session management
   functions.
   
   All service APIs should use this package to verify
   if the current users permissions before running.
   
   No grants or synonyms to anything
  **********************************************************/
  
  /* sets context for the current session - allows data to be filtered for that user */
  PROCEDURE setContext (p_sessionCode IN user_sessions.uss_code%TYPE);
  
  /* same as the other setContext, but the session code is taken from the client_identifier in the system context */
  PROCEDURE setContext;
  
  /* removes context values from current session */
  PROCEDURE clearContext;
  
  /* stores the session info on the database */
  PROCEDURE createSession (p_username    IN  users.usr_username%TYPE,
                           p_sessionId   IN  user_sessions.uss_session_id%TYPE,
                           p_userIP      IN  user_sessions.uss_user_ip%TYPE,
                           p_pw          IN  VARCHAR2,
                           p_lngCode     IN  VARCHAR2,
                           p_sessionCode OUT user_sessions.uss_code%TYPE);
                           
  /* ends the specified session. Don't rely on session to be ended as the user may not have logged out */
  PROCEDURE endSession;

  /* Returns the USR_ID for the current context */
  FUNCTION getCurrentUsrID RETURN INTEGER;

  /* Returns the USS_ID for the current context */
  FUNCTION getCurrentUssID RETURN INTEGER;

  /* Returns the LNG_ID for the current context */
  FUNCTION getCurrentLngID RETURN INTEGER;
  
  /* Returns the SESSION_ID for the current context */
  FUNCTION getCurrentSessionID RETURN INTEGER;
  
  /* Returns the SESSION_CODE for the current context. */
  FUNCTION getCurrentSessionCode RETURN VARCHAR2;
  
  /* Returns the SESSION_TYPE for the current context.  */
  FUNCTION getCurrentSessionType RETURN VARCHAR2;
  
 /* Returns a the message level for the current session */
  FUNCTION getMessageLevel RETURN VARCHAR2;
  
  /* If the current session is not valid an error will be raised 
     Optional role can be passed in to check if user has permissions to perform the action */
  PROCEDURE validSession (p_role IN VARCHAR2 DEFAULT NULL);
  
  /* return answers. id = icon id, ic = icon name */
  PROCEDURE getCaptchaAnswer (p_ansId   OUT captcha_icons.cv_code%TYPE,
                              p_ansName OUT captcha_icons.cv_name%TYPE);
                              
 /* Resets the password of a user and emails it to them */
  PROCEDURE resetPassword (p_email   IN users.usr_email%TYPE);

 /* Generates a random alpha numeric string of between 1 and 100 characters in length as required */
  FUNCTION getRandomString (p_length IN INTEGER DEFAULT 40) RETURN VARCHAR2;

 /* Returns a_Data.gTrue if it exists and false if not */
  FUNCTION userExists (p_newUser IN users.usr_username%TYPE) RETURN VARCHAR2;

 /* Returns a unique temp link code */
  FUNCTION getUniqueTempLinkCode RETURN temp_links.tln_link_code%TYPE;
  
 /* Update the session last used timestamp on USER_SESSIONS */
  PROCEDURE setSessionLastUsed;
  
 /* Timeout any sessions that have been inactive for a while */
  PROCEDURE timeoutSessions;
END c_Session;
/
