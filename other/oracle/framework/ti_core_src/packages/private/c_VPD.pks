CREATE OR REPLACE PACKAGE ti_core_src.c_VPD IS
  /*********************************************************
   Package that handles various trading plan functions.

   No grants or synonyms to anything
  **********************************************************/

  /* returns condition to append to WHERE clause */
  FUNCTION userVPD (schema_var IN VARCHAR2,
                    table_var  IN VARCHAR2) RETURN VARCHAR2;
                    
 /* Use this on a non Enterprise Edition of the Oracle database as proper VPD wont be available
   FUNCTION userVPD_nonEE(p_usrId IN INTEGER) RETURN VARCHAR2;
  
    Use this on a non Enterprise Edition of the Oracle database as proper VPD wont be available
    FUNCTION signupVPD_nonEE(p_sessionId   IN INTEGER,
                           p_sessionCode IN VARCHAR2) RETURN VARCHAR2;*/
  
END c_VPD;
/
