DECLARE
  /* This will wrap and compile the package body.
  
     Dont put any blank lines or comments in the code below as it will stop it from compiling once wrapped */
     
  l_source  DBMS_SQL.VARCHAR2A;
  l_wrap    DBMS_SQL.VARCHAR2A;
  l_id      INTEGER := 0;
  
  PROCEDURE addLine(p_source IN VARCHAR2) IS
  BEGIN
    l_id := l_id + 1;
    l_source(l_id) := p_source;
  END addLine;
BEGIN
  addLine('CREATE OR REPLACE PACKAGE BODY ti_core_src.c_Security IS');
  
  ------------------------------------------------------------------------------------------
  -- Forward declarations
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION getPassword (p_usrId IN users.usr_id%TYPE,');
  addLine('                        p_pw    IN VARCHAR2) RETURN RAW;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION bannedPassword (p_pw IN VARCHAR2) RETURN VARCHAR2;');

  ------------------------------------------------------------------------------------------
  
  addLine('  PROCEDURE setPasswordTries (p_usrId IN users.usr_id%TYPE,');
  addLine('                              p_tries IN INTEGER);');

  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION getKey (p_seed IN VARCHAR2) RETURN RAW;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION doDecrypt (p_raw IN RAW, p_key IN INTEGER) RETURN VARCHAR2;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION doEncrypt (p_value IN VARCHAR2, p_key IN VARCHAR2) RETURN RAW;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  PROCEDURE sendTempLink (p_usrId   IN users.usr_id%TYPE,');
  addLine('                          p_sgnId   IN signup.sgn_id%TYPE,');
  addLine('                          p_type    IN temp_links.tln_type%TYPE,');
  addLine('                          p_subCode IN VARCHAR2,');
  addLine('                          p_msgCode IN VARCHAR2);');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  PROCEDURE VpdSwitch (p_alias  IN VARCHAR2,'); -- Private only. Do not allow this to be called from the pkg spec.
  addLine('                       p_active IN VARCHAR2);');
  
  ------------------------------------------------------------------------------------------
  -- Public procedures / functions
  ------------------------------------------------------------------------------------------
  
  addLine('  PROCEDURE updatePassword (p_oldPw  IN VARCHAR2,');
  addLine('                            p_newPw  IN VARCHAR2) IS');
  addLine('    l_status    VARCHAR2(1);');
  addLine('    l_newPw     users.usr_password%TYPE;');
  addLine('    l_pwdTries  users.usr_pwd_tries%TYPE;');
  addLine('  BEGIN');
  addLine('    c_Session.validSession;');
    
  addLine('    l_status := checkPassword(p_usrId => c_Session.getCurrentUsrId,');
  addLine('                              p_pw    => p_oldPw);');
  
  addLine('    IF l_status = a_Data.gTrue THEN');
  addLine('      l_newPw := getPassword (p_usrId => c_Session.getCurrentUsrId,');
  addLine('                              p_pw    => p_newPw);');
  
  addLine('      t_Users.setPassword (p_pk  => c_Session.getCurrentUsrId,');
  addLine('                           p_val => l_newPw);');
  
  addLine('      t_Users.setChangePwOnLogon (p_pk  => c_Session.getCurrentUsrId,');
  addLine('                                  p_val => ''N'');');
  
  addLine('        a_Message.logMsgText(p_message => ''Password Updated using internal page.'',');
  addLine('                             p_data    => a_Data.addTag(NULL,''USR_ID'',c_Session.getCurrentUsrId),');
  addLine('                             p_type    => a_Message.gInfo);');
  addLine('    ELSE');
  addLine('      a_Message.logMsgCode (p_code => ''XAPP-00002'','); -- password incorrect
  addLine('                            p_type => a_Message.gFatal);');
  addLine('    END IF;');
  addLine('  END updatePassword;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  PROCEDURE updatePasswordExt (p_newPw  IN VARCHAR2,');
  addLine('                               p_code   IN VARCHAR2) IS');
  addLine('    l_status    VARCHAR2(1);');
  addLine('    l_newPw     users.usr_password%TYPE;');
  addLine('    l_usrId     users.usr_id%TYPE;');
  addLine('    l_tlnId     temp_links.tln_id%TYPE;');
  addLine('  BEGIN');
  addLine('    l_status := checkTempLink (p_type => ''RESETPW'',');
  addLine('                               p_code => p_code);');
  
  addLine('    VpdSwitch (p_alias  => ''USR'',');
  addLine('               p_active => a_Data.gFalse);');

  addLine('    l_tlnId := t_TempLinks.getId_UK   (p_uk => p_code);');
  addLine('    l_usrId := t_TempLinks.getUsrId_PK(p_pk => l_tlnId);');
  
  addLine('    IF l_status = a_Data.gTrue THEN');
  addLine('      l_newPw := getPassword (p_usrId => l_usrId,');
  addLine('                              p_pw    => p_newPw);');
  
  addLine('      t_Users.setPassword (p_pk  => l_usrId,');
  addLine('                           p_val => l_newPw);');
  
  addLine('      t_Users.setChangePwOnLogon (p_pk  => l_usrId,');
  addLine('                                  p_val => ''N'');');
  
  addLine('      t_Users.setPwdTries (p_pk  => l_usrId,');
  addLine('                           p_val => 0);');
  
  addLine('      t_TempLinks.setUsedInd (p_pk  => l_tlnId,');
  addLine('                              p_val => ''Y'');');
  
  addLine('      t_TempLinks.setUsedDate (p_pk  => l_tlnId,');
  addLine('                               p_val => SYSDATE);');
  addLine('    ELSE');
  addLine('      a_Message.logMsgCode (p_code => ''XAPP-00053'','); -- invalid code
  addLine('                            p_type => a_Message.gFatal);');
  addLine('    END IF;');
  
  addLine('    VpdSwitch (p_alias  => ''USR'',');
  addLine('               p_active => a_Data.gTrue);');
  
  addLine('    a_Message.logMsgText(p_message => ''Password updated using temp link.'',');
  addLine('                         p_data    => a_Data.addTag(NULL,''USR_ID'',l_usrId),');
  addLine('                         p_type    => a_Message.gInfo);');
  addLine('  EXCEPTION');
  addLine('    WHEN OTHERS THEN');
  addLine('      VpdSwitch (p_alias  => ''USR'',');
  addLine('                 p_active => a_Data.gTrue);');
                 
  addLine('      RAISE;');
  addLine('  END updatePasswordExt;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION checkPassword (p_usrId   IN users.usr_id%TYPE,');
  addLine('                          p_pw      IN VARCHAR2) RETURN VARCHAR2 IS');
  addLine('    l_supplied  users.usr_password%TYPE;');
  addLine('    l_current   users.usr_password%TYPE;');
  addLine('    l_pwdTries  users.usr_pwd_tries%TYPE;');
  addLine('    l_usrId     users.usr_id%TYPE;');
  addLine('  BEGIN');
  addLine('    l_usrId := NVL(c_Session.getCurrentUsrId,p_usrId);');
  addLine('    l_pwdTries := t_Users.getPwdTries_PK (p_pk  => l_usrId);');
  
  addLine('    IF l_pwdTries >= 3 THEN');
  addLine('      a_Message.logMsgCode (p_code    => ''XAPP-00005'','); -- account locked
  addLine('                            p_type    => a_Message.gFatal,');
  addLine('                            p_logout  => a_Data.gFalse);');
  addLine('    END IF;');
  
  addLine('    l_supplied := getPassword(p_usrId => l_usrId,');
  addLine('                              p_pw    => p_pw);');
  
  addLine('    l_current := t_Users.getPassword_PK(p_pk => l_usrId);');
  
  addLine('    IF l_current = l_supplied THEN');
  addLine('      setPasswordTries (p_usrId => l_usrId,');
  addLine('                        p_tries => 0);');  
  
  addLine('        a_Message.logMsgText(p_message => ''Supplied password is valid.'',');
  addLine('                             p_data    => a_Data.addTag(NULL,''USR_ID'',p_usrId),');
  addLine('                             p_type    => a_Message.gInfo);');
  addLine('      RETURN a_Data.gTrue;');
  addLine('    ELSE');
  addLine('      setPasswordTries (p_usrId => l_usrId,');
  addLine('                        p_tries => l_pwdTries + 1);');
  
  addLine('      IF l_pwdTries >= 3 THEN');
  addLine('        a_Message.logMsgCode (p_code    => ''XAPP-00005'',');  -- account locked
  addLine('                              p_type    => a_Message.gFatal,');
  addLine('                              p_logout  => a_Data.gFalse);');
  addLine('      ELSE');
  addLine('        a_Message.logMsgText(p_message => ''Supplied password is invalid.'',');
  addLine('                             p_data    => a_Data.addTag(NULL,''USR_ID'',p_usrId),');
  addLine('                             p_type    => a_Message.gInfo);');
  addLine('        RETURN a_Data.gFalse;');
  addLine('      END IF;');
  addLine('    END IF;');
  addLine('  END checkPassword;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION checkPassword (p_username   IN VARCHAR2,');
  addLine('                          p_password   IN VARCHAR2) RETURN BOOLEAN IS');
  addLine('  BEGIN');
  addLine('    RETURN a_Data.isTrue(checkPassword(p_usrId => t_Users.getId_UK(p_uk => p_username),');
  addLine('                                       p_pw    => p_password));');
  addLine('  EXCEPTION');
  addLine('    WHEN OTHERS THEN');
  addLine('      RETURN FALSE;');
  addLine('  END checkPassword;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  PROCEDURE resetPassword (p_email   IN users.usr_email%TYPE) IS');
  addLine('    l_usrId     users.usr_id%TYPE;');
  addLine('  BEGIN');
  addLine('    VpdSwitch (p_alias  => ''USR'',');
  addLine('               p_active => a_Data.gFalse);');
  
  addLine('    l_usrId := a_User.getUsrIdFromEmail(p_email => p_email);');
  
  addLine('    VpdSwitch (p_alias  => ''USR'',');
  addLine('               p_active => a_Data.gTrue);');
  /*
  addLine('    sendTempLink (p_usrId   => l_usrId,');
  addLine('                  p_sudId   => NULL,');
  addLine('                  p_type    => ''RESETPW'',');
  addLine('                  p_subCode => ''XAPP-00008'',');
  addLine('                  p_msgCode => ''XAPP-00009'');');*/
  
  addLine('    a_Message.logMsgText(p_message => ''Password Reset.'',');
  addLine('                         p_data    => a_Data.addTag(NULL,''EMAIL'',p_email),');
  addLine('                         p_type    => a_Message.gInfo);');
  addLine('  EXCEPTION');
  addLine('    WHEN OTHERS THEN');
  addLine('      VpdSwitch (p_alias  => ''USR'',');
  addLine('                 p_active => a_Data.gTrue);');
                 
  addLine('      RAISE;');
  addLine('  END resetPassword;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  PROCEDURE sendRegMail (p_sgnId IN signup.sgn_id%TYPE) IS');
  addLine('  BEGIN');  
    
  addLine('    sendTempLink (p_usrId   => NULL,');
  addLine('                  p_sgnId   => p_sgnId,');
  addLine('                  p_type    => ''SIGNUP'',');
  addLine('                  p_subCode => ''XAPP-00054'',');
  addLine('                  p_msgCode => ''XAPP-00055'');');
  
  addLine('    a_Message.logMsgText(p_message => ''Signup email sent.'',');
  addLine('                         p_data    => a_Data.addTag(NULL,''SGN_ID'',p_sgnId),');
  addLine('                         p_type    => a_Message.gInfo);');
  addLine('  END sendRegMail;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION checkTempLink (p_type IN temp_links.tln_type%TYPE,');
  addLine('                          p_code IN temp_links.tln_link_code%TYPE) RETURN VARCHAR2 IS');
  addLine('    l_tlnRow   temp_links%ROWTYPE;');
  addLine('  BEGIN');
  addLine('    VpdSwitch (p_alias  => ''USR'',');
  addLine('               p_active => a_Data.gFalse);');
  
  addLine('    l_tlnRow := t_TempLinks.getRow_UK(p_uk => p_code);');

  addLine('    VpdSwitch (p_alias  => ''USR'',');
  addLine('               p_active => a_Data.gTrue);');
    
  addLine('    IF l_tlnRow.tln_id          IS NOT NULL AND');
  addLine('       l_tlnRow.tln_expiry_date >= SYSDATE  AND');
  addLine('       l_tlnRow.tln_used_ind     = ''N''    AND');
  addLine('       l_tlnRow.tln_type         = p_type   THEN');
  addLine('      a_Message.logMsgText(p_message => ''Temp link valid.'',');
  addLine('                           p_type    => a_Message.gInfo);');
  addLine('      RETURN a_Data.gTrue;');
  addLine('    ELSE ');
  addLine('      a_Message.logMsgText(p_message => ''Temp link invalid.'',');
  addLine('                           p_type    => a_Message.gInfo);');
  addLine('      RETURN a_Data.gFalse;');
  addLine('    END IF;');
  addLine('  EXCEPTION');
  addLine('    WHEN OTHERS THEN');
  addLine('      VpdSwitch (p_alias  => ''USR'',');
  addLine('                 p_active => a_Data.gTrue);');
                 
  addLine('      RAISE;');
  addLine('  END checkTempLink;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION encrypt (p_value IN VARCHAR2, p_key IN INTEGER) RETURN RAW IS');
  addLine('  BEGIN');
  addLine('    RETURN doEncrypt (p_value => p_value, p_key => TO_CHAR(p_key));');
  addLine('  END encrypt;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION decrypt (p_raw IN RAW, p_key IN INTEGER) RETURN VARCHAR2 IS');
  addLine('    l_value VARCHAR2(200);');
  addLine('  BEGIN');
  addLine('    c_Session.validSession;');
  
  addLine('    l_value := doDecrypt(p_raw => p_raw, p_key => TO_CHAR(p_key));');

  addLine('    RETURN l_value;');
  addLine('  END decrypt;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION decrypt (p_raw IN RAW, p_key IN INTEGER, p_digitsToShow IN INTEGER) RETURN VARCHAR2 IS');
  addLine('    l_value VARCHAR2(200);');
  addLine('  BEGIN');
--  addLine('    IF NVL(c_Session.getCurrentSignup,''N'') = ''N'' THEN');
  addLine('      c_Session.validSession;');
--  addLine('    END IF;');
  
  addLine('    l_value := doDecrypt(p_raw => p_raw, p_key => TO_CHAR(p_key));');

  addLine('    IF p_digitsToShow > FLOOR(LENGTH(l_value) / 2) THEN');
  addLine('      RETURN '':p'';');
  addLine('    END IF;');

  addLine('    l_value := LPAD(''*'',GREATEST(LEAST(LENGTH(l_value)-p_digitsToShow,LENGTH(l_value)),0),''*'')||SUBSTR(l_value,-LEAST(LENGTH(l_value),p_digitsToShow),p_digitsToShow);');

  addLine('    RETURN l_value;');
  addLine('  END decrypt;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION passwordPolicy (p_pw IN VARCHAR2) RETURN VARCHAR2 IS');
  addLine('  BEGIN');
  addLine('    RETURN NULL;'); -- ********************** add policy here *************************
  addLine('  END passwordPolicy;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  PROCEDURE createUser (p_code IN temp_links.tln_link_code%TYPE,');
  addLine('                        p_pw   IN VARCHAR2) IS');
  addLine('    l_usrRow    users%ROWTYPE;');
  addLine('    l_sgnRow    signup%ROWTYPE;');
  addLine('    l_usrId     users.usr_id%TYPE;');
  addLine('    l_newPw     users.usr_password%TYPE;');
  addLine('    l_tlnRow    temp_links%ROWTYPE;');
  addLine('  BEGIN');
  addLine('    IF checkTempLink (p_type => ''SIGNUP'',');
  addLine('                      p_code => p_code) = a_Data.gFalse THEN');
  addLine('      a_Message.logMsgCode (p_code => ''XAPP-00056'',');
  addLine('                            p_type => a_Message.gFatal);');
  addLine('    ELSIF p_pw IS NULL THEN');
  addLine('      a_Message.logMsgCode (p_code => ''XAPP-00026'',');
  addLine('                            p_type => a_Message.gFatal);');
  addLine('    END IF;');

  addLine('    VpdSwitch (p_alias  => ''USR'',');
  addLine('               p_active => a_Data.gFalse);');
    
  addLine('    l_tlnRow := t_TempLinks.getRow_UK (p_uk => p_code);');
                        
  addLine('    l_sgnRow := t_Signup.getRow_PK (p_pk => a_Signup.getSgnIdFromTlnId(p_tlnId => l_tlnRow.tln_id));');

  addLine('    l_usrRow := NULL;');
             
  addLine('    l_usrRow.usr_username                  := l_sgnRow.sgn_username;');
  addLine('    l_usrRow.usr_first_name                := l_sgnRow.sgn_first_name;');
  addLine('    l_usrRow.usr_last_name                 := l_sgnRow.sgn_last_name;');
  addLine('    l_usrRow.usr_email                     := l_sgnRow.sgn_email;');
  addLine('    l_usrRow.usr_status                    := ''A'';');
  addLine('    l_usrRow.usr_password                  := ''8F9380079FFA7EEBE105570216FFEEF57F500C'';'); -- temp value gets updated straight after the insert below
  addLine('    l_usrRow.usr_pwd_tries                 := 0;');
  addLine('    l_usrRow.usr_change_pw_on_logon        := ''N'';');
                                     
  addLine('    t_Users.insertRow (p_row   => l_usrRow,');
  addLine('                       p_newPk => l_usrId);');
                                
  addLine('    l_newPw := getPassword (p_usrId => l_usrId,');
  addLine('                            p_pw    => p_pw);');
                                     
  addLine('    t_Users.setPassword (p_pk  => l_usrId,');
  addLine('                         p_val => l_newPw);');
                        
  addLine('    t_TempLinks.setUsedInd (p_pk  => l_tlnRow.tln_id,');
  addLine('                            p_val => ''Y'');');
  
  addLine('    t_TempLinks.setUsedDate (p_pk  => l_tlnRow.tln_id,');
  addLine('                             p_val => SYSDATE);');
                        
  addLine('    VpdSwitch (p_alias  => ''USR'',');
  addLine('               p_active => a_Data.gTrue);');
                        
  addLine('    t_Signup.setStatus(p_pk  => l_sgnRow.sgn_id,');
  addLine('                       p_val => ''V'');');

  addLine('    a_Message.logMsgText (p_message => ''Created user'',');
  addLine('                          p_data    => a_Data.addTag(NULL,''USR_ID'',l_usrId),');
  addLine('                          p_type    => a_Message.gInfo);');
  addLine('  EXCEPTION');
  addLine('    WHEN OTHERS THEN');
  addLine('      VpdSwitch (p_alias  => ''USR'',');
  addLine('                 p_active => a_Data.gTrue);');
                 
  addLine('      RAISE;');
  addLine('  END createUser;');
  
  ------------------------------------------------------------------------------------------
  -- Private procedures / functions
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION getPassword (p_usrId IN users.usr_id%TYPE,');
  addLine('                        p_pw    IN VARCHAR2) RETURN RAW IS');
  addLine('    l_out     users.usr_password%TYPE;');
  addLine('  BEGIN');
  addLine('    l_out := SYS.UTL_RAW.CAST_TO_RAW(p_usrId||p_pw);');
  
  addLine('    FOR i IN REVERSE 1..RPAD(SUBSTR(p_usrId, -2)||SUBSTR(p_usrId, 1, 2), 4, 0) LOOP');
  addLine('      l_out := SYS.DBMS_CRYPTO.MAC(l_out, SYS.DBMS_CRYPTO.HMAC_SH1, SYS.UTL_RAW.CAST_TO_RAW(i));');
  addLine('    END LOOP;');
  
  addLine('    RETURN l_out;');
  addLine('  END getPassword;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION bannedPassword (p_pw IN VARCHAR2) RETURN VARCHAR2 IS'); -- raise XAPP-00016
  addLine('    l_chk     INTEGER;');
  addLine('  BEGIN');
  addLine('    IF t_CodeValues.exists_PK(''banned_passwords'', LOWER(p_pw)) = a_Data.gTrue THEN');
  addLine('      RETURN a_Data.gFalse;');
  addLine('    ELSE');
  addLine('      RETURN a_Data.gTrue;');
  addLine('    END IF;');
  addLine('  END bannedPassword;');

  ------------------------------------------------------------------------------------------

  addLine('  PROCEDURE setPasswordTries (p_usrId IN users.usr_id%TYPE,');
  addLine('                              p_tries IN INTEGER) IS');
  addLine('    PRAGMA AUTONOMOUS_TRANSACTION;');
  
  addLine('    l_chk     INTEGER;');
  addLine('  BEGIN');
  addLine('    t_Users.setPwdTries (p_pk  => p_usrId,');
  addLine('                         p_val => p_tries);');
  
  addLine('    COMMIT;');
  addLine('  END setPasswordTries;');

  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION getKey (p_seed IN VARCHAR2) RETURN RAW IS');
  addLine('    l_key RAW(256);');
  addLine('  BEGIN');
  addLine('    l_key := SYS.UTL_RAW.BIT_XOR(SYS.UTL_RAW.CAST_TO_RAW(p_seed||CHR(65+TO_NUMBER(SUBSTR(p_seed,-1,1)))),SYS.UTL_RAW.REVERSE(SYS.UTL_RAW.CAST_TO_RAW(LPAD(p_seed,32,p_seed))));');

  addLine('    RETURN l_key;');
  addLine('  END getKey;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION doDecrypt (p_raw IN RAW, p_key IN INTEGER) RETURN VARCHAR2 IS');
  addLine('    l_value VARCHAR2(200);');
  addLine('    l_key   RAW(256) := getKey(p_key);');
  
  addLine('    e_wrongKey  EXCEPTION;');
  addLine('    PRAGMA EXCEPTION_INIT(e_wrongKey, -28817);');
  addLine('  BEGIN');
  addLine('    l_value := SYS.UTL_RAW.CAST_TO_VARCHAR2(SYS.DBMS_CRYPTO.DECRYPT(SYS.UTL_RAW.REVERSE(p_raw),');
  addLine('                                                                    SYS.DBMS_CRYPTO.ENCRYPT_AES256 + SYS.DBMS_CRYPTO.CHAIN_CBC + SYS.DBMS_CRYPTO.PAD_PKCS5,');
  addLine('                                                                    l_key));');

  addLine('    RETURN l_value;');
  addLine('  EXCEPTION');
  addLine('    WHEN e_wrongKey THEN');
  addLine('      RETURN '':p'';');
  addLine('  END doDecrypt;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  FUNCTION doEncrypt (p_value IN VARCHAR2, p_key IN VARCHAR2) RETURN RAW IS');
  addLine('    l_raw RAW(2048);');
  addLine('    l_key RAW(256) := getKey(p_key);');
  addLine('  BEGIN');
  addLine('    IF c_Session.getCurrentSessionID IS NULL THEN');
  addLine('      l_raw := NULL;');  
  addLine('    ELSE');
  addLine('      l_raw := SYS.UTL_RAW.REVERSE(SYS.DBMS_CRYPTO.ENCRYPT(SYS.UTL_RAW.CAST_TO_RAW(p_value),');
  addLine('                                                           SYS.DBMS_CRYPTO.ENCRYPT_AES256 + SYS.DBMS_CRYPTO.CHAIN_CBC + SYS.DBMS_CRYPTO.PAD_PKCS5,');
  addLine('                                                           l_key));');
  addLine('    END IF;');

  addLine('    RETURN l_raw;');
  addLine('  END doEncrypt;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('  PROCEDURE sendTempLink (p_usrId   IN users.usr_id%TYPE,');
  addLine('                          p_sgnId   IN signup.sgn_id%TYPE,');
  addLine('                          p_type    IN temp_links.tln_type%TYPE,');
  addLine('                          p_subCode IN VARCHAR2,');
  addLine('                          p_msgCode IN VARCHAR2) IS');
  addLine('    l_row       temp_links%ROWTYPE := NULL;');
  addLine('    l_tlnId     temp_links.tln_id%TYPE;');
  addLine('    l_email     users.usr_email%TYPE;');
  addLine('  BEGIN');
  addLine('    VpdSwitch (p_alias  => ''USR'',');
  addLine('               p_active => a_Data.gFalse);');

  addLine('    IF p_usrId IS NOT NULL OR p_sgnId IS NOT NULL THEN');
  addLine('      l_row.tln_usr_id      := p_usrId;');
  addLine('      l_row.tln_link_code   := c_Session.getUniqueTempLinkCode;');
  addLine('      l_row.tln_expiry_date := SYSDATE + 1;');
  addLine('      l_row.tln_used_ind    := ''N'';');
  addLine('      l_row.tln_type        := p_type;');
  
  addLine('      t_TempLinks.insertRow (p_row   => l_row,');
  addLine('                             p_newPk => l_tlnId);');
  
  addLine('      IF p_usrId IS NOT NULL THEN');
  addLine('        l_email := t_Users.getEmail_PK (p_pk => p_usrId);');
  addLine('      ELSE');
  addLine('        l_email := t_Signup.getEmail_PK (p_pk => p_sgnId);');
  addLine('      END IF;');

  addLine('      a_Message.sendMail(p_subject => a_Message.getMessageText(p_code => p_subCode),');
  addLine('                         p_message => a_Message.getMessageText(p_code => p_msgCode,');
  addLine('                                                               p_vars => a_Data.addTag(NULL,''LINKCODE'', l_row.tln_link_code)),');
  addLine('                         p_to      => l_email);');
  
  addLine('      IF p_sgnId IS NOT NULL THEN');
  addLine('        t_Signup.setTlnId (p_pk => p_sgnId, p_val => l_tlnId);');
  addLine('      END IF;');
  addLine('    END IF;');
  
  addLine('    VpdSwitch (p_alias  => ''USR'',');
  addLine('               p_active => a_Data.gTrue);');
  
  addLine('    a_Message.logMsgText(p_message => ''Created Temp Link'',');
  addLine('                         p_data    => a_Data.addTag(a_Data.addTag(NULL,''SGN_ID'',p_sgnId),''USR_ID'',p_usrId),');
  addLine('                         p_type    => a_Message.gInfo);');
  addLine('  EXCEPTION');
  addLine('    WHEN OTHERS THEN');
  addLine('      VpdSwitch (p_alias  => ''USR'',');
  addLine('                 p_active => a_Data.gTrue);');
  
  addLine('      RAISE;');
  addLine('  END sendTempLink;');

  ------------------------------------------------------------------------------------------
    
  addLine('  PROCEDURE VpdSwitch (p_alias  IN VARCHAR2,'); -- Private only. Do not allow this to be called from the pkg spec.
  addLine('                       p_active IN VARCHAR2) IS');
  
  addLine('  BEGIN');
  addLine('    DBMS_SESSION.SET_CONTEXT (''TI_SEC_CTX'',');
  addLine('                              p_alias||''_VPD'',');
  addLine('                              p_active);');
  addLine('  END VpdSwitch;');
  
  ------------------------------------------------------------------------------------------
  
  addLine('END c_Security;');
  
  SYS.DBMS_DDL.CREATE_WRAPPED(ddl => l_source,
                              lb  => 1,
                              ub  => l_source.COUNT);
END;
/
