CREATE OR REPLACE PACKAGE BODY ti_core_src.c_VPD IS
  ----------------------------------------------------------------------------------------------------------------
  -- Forward declarations
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION VpdIsOff (p_alias IN VARCHAR2) RETURN BOOLEAN;
  
  ----------------------------------------------------------------------------------------------------------------
  -- Public declarations
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION userVPD (schema_var IN VARCHAR2,
                    table_var  IN VARCHAR2) RETURN VARCHAR2 IS
    l_out     VARCHAR2 (400);
    l_usrId   INTEGER        := ti_core_src.c_Session.getCurrentUsrId;
    l_ussId   INTEGER        := ti_core_src.c_Session.getCurrentUssId;
    
    e_noRows  EXCEPTION;
  BEGIN
    IF VpdIsOff(p_alias => 'USR') AND
       table_var  IN ('USERS','USER_SESSIONS','TEMP_LINKS') AND
       schema_var = 'TI_CORE_DAT' THEN
      RETURN NULL;
    END IF;
    
    IF l_usrId IS NULL OR
       l_ussId IS NULL THEN
      RAISE e_noRows;
    END IF;
    
    IF schema_var = 'TI_CORE_DAT' THEN
      IF table_var = 'USERS' THEN
        l_out := 'usr_id = '||l_usrId;/*
      ELSIF table_var = 'MAIL_QUEUE' THEN
        l_out := 'mlq_usr_id = '||l_usrId;*/
      ELSIF table_var = 'TEMP_LINKS' THEN
        l_out := 'tln_usr_id = '||l_usrId;
      ELSIF table_var = 'USER_APP_MODULES' THEN
        l_out := 'uam_usa_id IN (SELECT usa_id FROM user_apps)';
      ELSIF table_var = 'USER_APPS' THEN
        l_out := 'usa_usr_id = '||l_usrId;
      ELSIF table_var = 'USER_INFO' THEN
        l_out := 'ui_usr_id = '||l_usrId;
      ELSIF table_var = 'USER_SESSIONS' THEN
        l_out := 'uss_usr_id = '||l_usrId;
      ELSE
        RAISE e_noRows;
      END IF;
    ELSE
      RAISE e_noRows;
    END IF; 
    
    IF table_var != 'USER_SESSIONS' THEN
      ti_core_src.c_Session.setSessionLastUsed;
    END IF;
    
    RETURN l_out;
  EXCEPTION
    WHEN e_noRows THEN
      RETURN '1 = 2';
  END userVPD;
  
  ----------------------------------------------------------------------------------------------------------------
  /*  
  FUNCTION userVPD_nonEE(p_usrId IN INTEGER) RETURN VARCHAR2 IS
    l_out   VARCHAR2 (400);
    l_usrId INTEGER        := ti_core_src.c_Session.getCurrentUsrId;
    l_ussId INTEGER        := ti_core_src.c_Session.getCurrentUssId;
  BEGIN
    IF VpdIsOff(p_alias => 'USR') THEN
      RETURN 'Y';
    END IF;
    
    IF l_usrId IS NULL OR
       l_ussId IS NULL THEN
      RETURN 'N';
    END IF;
    
    IF p_usrId = l_usrId THEN
      RETURN 'Y';
    ELSE
      RETURN 'N';
    END IF;
  END userVPD_nonEE;
 
  ----------------------------------------------------------------------------------------------------------------

  FUNCTION signupVPD_nonEE(p_sessionId   IN INTEGER,
                           p_sessionCode IN VARCHAR2) RETURN VARCHAR2 IS
    l_out         VARCHAR2 (400);
    l_sessionId   INTEGER      := ti_core_src.c_Session.getCurrentSessionId;
    l_sessionCode VARCHAR2(50) := ti_core_src.c_Session.getCurrentSessionCode;
  BEGIN                            
    IF VpdIsOff(p_alias => 'SESSION') THEN
      RETURN 'Y';
    END IF;
    
    IF ti_core_src.c_Session.getCurrentSignup = 'N'  OR
       l_sessionId                   IS NULL THEN
      RETURN 'N';
    END IF;
    
    IF p_sessionId   = l_sessionId   AND
       p_sessionCode = l_sessionCode THEN
      RETURN 'Y';
    ELSE
      RETURN 'N';
    END IF;
  END signupVPD_nonEE;*/
  
  ----------------------------------------------------------------------------------------------------------------
  -- Private declarations
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION VpdIsOff (p_alias IN VARCHAR2) RETURN BOOLEAN IS
    l_out BOOLEAN;
  BEGIN
    l_out := ti_core_src.a_Data.isFalse(NVL(SYS_CONTEXT ('TI_SES_CTX', p_alias||'_VPD'), ti_core_src.a_Data.gTrue));
    
    IF NOT l_out THEN
      l_out := ti_core_src.a_Data.isFalse(NVL(SYS_CONTEXT ('TI_SEC_CTX', p_alias||'_VPD'), ti_core_src.a_Data.gTrue));
    END IF;
    
    RETURN l_out;
  END VpdIsOff;
END c_VPD;
/
