CREATE OR REPLACE PACKAGE ti_core_src.c_Mail IS
  /*********************************************************
   Package that handles various email functions.

   No grants or synonyms to anything
  **********************************************************/

  /* store a message on the mail queue */
  PROCEDURE queueMsg (p_subject  IN VARCHAR2,
                      p_body     IN CLOB,
                      p_to       IN VARCHAR2,
                      p_from     IN VARCHAR2 DEFAULT NULL);
  
  /* store a message on the mail queue */
  PROCEDURE queueMsg (p_subject  IN VARCHAR2,
                      p_body     IN VARCHAR2,
                      p_to       IN VARCHAR2,
                      p_from     IN VARCHAR2 DEFAULT NULL);

  /* Attempts to output unsent messages. */
  PROCEDURE sendMessages;
  
  /* returns a_Data.gTrue is available  otherwise a_Data.gFalse */
  FUNCTION smtpAvailable RETURN VARCHAR2;
    
END c_Mail;
/
