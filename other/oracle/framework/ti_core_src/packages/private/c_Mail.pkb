CREATE OR REPLACE PACKAGE BODY ti_core_src.c_Mail IS
  ----------------------------------------------------------------------------------------------------------------
  -- Forward declarations
  ----------------------------------------------------------------------------------------------------------------

  g_smtpHost  CONSTANT VARCHAR2(100) := 'smtp.gmail.com';
  g_smtpPort  CONSTANT INTEGER       := 587;
  g_smtpUser  CONSTANT VARCHAR2(100) := 'info@tradeignite.com';
  g_smtpPW    CONSTANT VARCHAR2(100) := 'uKnewJ26'; -- needs to be hidden at some point before go live

  PROCEDURE encryptLob (p_in IN CLOB, p_key IN VARCHAR2, p_out IN OUT NOCOPY BLOB);
  
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE decryptLob (p_in IN BLOB, p_key IN VARCHAR2, p_out IN OUT NOCOPY CLOB);
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getKey (p_seed IN VARCHAR2) RETURN RAW;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE sendMail (p_subject IN VARCHAR2,
                      p_message IN BLOB,
                      p_to      IN VARCHAR2,
                      p_from    IN VARCHAR2);
  
  ----------------------------------------------------------------------------------------------------------------
  -- Public declarations
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE queueMsg (p_subject  IN VARCHAR2,
                      p_body     IN CLOB,
                      p_to       IN VARCHAR2,
                      p_from     IN VARCHAR2 DEFAULT NULL) IS
    l_body   BLOB;
    l_to     VARCHAR2(200);
    l_from   VARCHAR2(200);
    l_live   VARCHAR2(1);
  BEGIN
    a_Message.logMsgText(p_message => 'Attempting to queue message.',
                         p_type    => a_Message.gInfo);
  
    IF p_subject IS NULL THEN
      a_Message.logMsgCode (p_code    => 'XAPP-00050', -- missing subject
                            p_data    => a_Data.addTag(NULL,'EMAIL_TO',  l_to),
                            p_type    => a_Message.gFatal,
                            p_logout  => a_Data.gFalse);
    END IF;
    
    IF SYS.DBMS_LOB.GETLENGTH(p_body) = 0 THEN
      a_Message.logMsgCode (p_code    => 'XAPP-00051', -- missing body
                            p_data    => a_Data.addTag(
                                           a_Data.addTag(NULL,'EMAIL_TO',  l_to),
                                                       'SUBJECT',   p_subject),
                            p_type    => a_Message.gFatal,
                            p_logout  => a_Data.gFalse);
    END IF;
        
    l_live := t_CodeValues.getValue_PK('config_values', 'live_system');
    
    IF l_live = 'Y' THEN
      l_to := p_to;
    ELSE
      IF p_to NOT LIKE '%@tradeignite.com' THEN
        l_to := t_CodeValues.getValue_PK('config_values', 'support_email');
      ELSE
        l_to := p_to;
      END IF;
    END IF;
    
    l_from := NVL(p_from,t_CodeValues.getValue_PK('config_values', 'support_email'));
       
    IF l_to IS NULL THEN
      a_Message.logMsgCode (p_code    => 'XAPP-00052', -- missing recipient
                            p_data    => a_Data.addTag(NULL,'SUBJECT',   p_to),
                            p_type    => a_Message.gFatal,
                            p_logout  => a_Data.gFalse);
    END IF;
    
    DBMS_LOB.CREATETEMPORARY(l_body, TRUE);
    
    encryptLob(p_in  => p_body,
               p_key => l_to,
               p_out => l_body);
    
    INSERT INTO mail_queue (mlq_subject,
                            mlq_body,
                            mlq_to,
                            mlq_from,
                            mlq_tries,
                            mlq_usr_id,
                            mlq_queued)
                    VALUES (p_subject,
                            l_body,
                            l_to,
                            l_from,
                            0,
                            ti_core_src.c_Session.getCurrentUsrID,
                            SYSDATE);
                            
    a_Message.logMsgText(p_message => 'Message queued.',
                         p_data    => a_Data.addTag(
                                         a_Data.addTag(NULL,'EMAIL_TO',  p_to),
                                                    'SUBJECT',   p_body),
                         p_type    => a_Message.gInfo);
  EXCEPTION
    WHEN OTHERS THEN
      a_Message.logMsgCode(p_code    => 'XSYS-00002',
                           p_data    => a_Data.addTag(
                                           a_Data.addTag(NULL,'EMAIL_TO',  p_to),
                                                      'SUBJECT',   p_body),
                           p_type    => a_Message.gFatal,
                           p_stack   => DBMS_UTILITY.FORMAT_ERROR_STACK,
                           p_trace   => DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
  END queueMsg;
  
  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE queueMsg (p_subject  IN VARCHAR2,
                      p_body     IN VARCHAR2,
                      p_to       IN VARCHAR2,
                      p_from     IN VARCHAR2 DEFAULT NULL) IS
  BEGIN
    queueMsg(p_subject => p_subject,
             p_body    => TO_CLOB(p_body),
             p_to      => p_to,
             p_from    => p_from);
  END queueMsg;
  
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE sendMessages IS
    l_error    VARCHAR2(2000);
    l_maxMails INTEGER := TO_NUMBER(t_CodeValues.getValue_PK('config_values', 'max_emails_to_send'));
    l_sent     INTEGER := 0;
  BEGIN
    a_Message.logMsgText(p_message => 'Starting email service',
                         p_type    => a_Message.gDebug);
                         
    --IF a_Data.isFalse(smtpAvailable) THEN      remove on local pc
    --  RETURN;
    --END IF;
      
    FOR mailRec IN (SELECT mlq_id,
                           mlq_subject,
                           mlq_body,
                           mlq_from,
                           mlq_to,
                           ROWNUM row_num
                      FROM mail_queue
                     WHERE mlq_tries < TO_NUMBER(t_CodeValues.getValue_PK('config_values', 'max_email_retries'))
                       AND mlq_sent IS NULL
                       AND (mlq_last_error_date IS NULL OR
                            (mlq_last_error_date IS NOT NULL AND
                             mlq_last_error_date < SYSDATE + TO_NUMBER(t_CodeValues.getValue_PK('config_values', 'email_retry_delay')) / 1440))) LOOP
      BEGIN
        sendMail (p_subject => mailRec.mlq_subject,
                  p_message => mailRec.mlq_body,
                  p_to      => mailRec.mlq_to,
                  p_from    => mailRec.mlq_from);
                  
        UPDATE mail_queue
           SET mlq_sent = SYSDATE
         WHERE mlq_id = mailRec.mlq_id;
      EXCEPTION
        WHEN OTHERS THEN
          l_error := SQLERRM;
          
          UPDATE mail_queue
             SET mlq_tries           = mlq_tries + 1,
                 mlq_last_error      = l_error,
                 mlq_last_error_date = SYSDATE
           WHERE mlq_id = mailRec.mlq_id;
      END;
      
      l_sent := mailRec.row_num;
      
      EXIT WHEN l_sent > l_maxMails;
    END LOOP;
    
    IF l_sent > 0 THEN
      a_Message.logMsgText(p_message => 'Sent '||l_sent||' email'||CASE l_sent WHEN 1 THEN '.' ELSE 's.' END,
                           p_type    => a_Message.gInfo);
    END IF;
    
    COMMIT;
  END sendMessages;
  
  ----------------------------------------------------------------------------------------------------------------
  -- Private declarations
  ----------------------------------------------------------------------------------------------------------------
  
  PROCEDURE encryptLob (p_in IN CLOB, p_key IN VARCHAR2, p_out IN OUT NOCOPY BLOB) IS
    l_key    RAW(256) := getKey(p_key);
  BEGIN
    DBMS_LOB.CREATETEMPORARY(p_out, TRUE);
  
    SYS.DBMS_CRYPTO.ENCRYPT(dst => p_out,
                            src => p_in,
                            typ => SYS.DBMS_CRYPTO.ENCRYPT_AES256 + SYS.DBMS_CRYPTO.CHAIN_CBC + SYS.DBMS_CRYPTO.PAD_PKCS5,
                            key => l_key);
  END encryptLob;
  
  ------------------------------------------------------------------------------------------
  
  PROCEDURE decryptLob (p_in IN BLOB, p_key IN VARCHAR2, p_out IN OUT NOCOPY CLOB) IS
    l_key       RAW(256) := getKey(p_key);
  
    e_wrongKey  EXCEPTION;
    PRAGMA      EXCEPTION_INIT(e_wrongKey, -28817);
  BEGIN
    DBMS_LOB.CREATETEMPORARY(p_out, TRUE);

    SYS.DBMS_CRYPTO.DECRYPT(dst => p_out,
                            src => p_in,
                            typ => SYS.DBMS_CRYPTO.ENCRYPT_AES256 + SYS.DBMS_CRYPTO.CHAIN_CBC + SYS.DBMS_CRYPTO.PAD_PKCS5,
                            key => l_key);
  EXCEPTION
    WHEN e_wrongKey THEN
      p_out := ':p';
  END decryptLob;
  
  ------------------------------------------------------------------------------------------
  
  FUNCTION getKey (p_seed IN VARCHAR2) RETURN RAW IS
    l_key RAW(256);
  BEGIN
    l_key := SYS.UTL_RAW.BIT_XOR(SYS.UTL_RAW.CAST_TO_RAW(p_seed||CHR(ASCII(SUBSTR(p_seed,1,1))+SUBSTR(LENGTH(p_seed),-1,1))),SYS.UTL_RAW.REVERSE(SYS.UTL_RAW.CAST_TO_RAW(LPAD(p_seed,32,p_seed))));
  
    RETURN l_key;
  END getKey;
  
  ------------------------------------------------------------------------------------------
  
  FUNCTION smtpAvailable RETURN VARCHAR2 IS
    l_conn SYS.UTL_SMTP.CONNECTION;
  BEGIN   
    l_conn := SYS.UTL_SMTP.OPEN_CONNECTION(host                          => g_smtpHost,
                                           port                          => g_smtpPort,
                                           wallet_path                   => 'file:D:\Oracle\wallets',--'file:/etc/ORACLE/WALLETS',--file:/usr/local/wallet',
                                   --      wallet_password               => 'tradeIgnitePW!',
                                           secure_connection_before_smtp => FALSE);
                                           
    SYS.UTL_SMTP.HELO(l_conn, 'tradeignite.com');
    
    SYS.UTL_SMTP.STARTTLS(l_conn);
    
    SYS.UTL_SMTP.AUTH(c        => l_conn,
                      username => g_smtpUser,
                      password => g_smtpPW,
                      schemes  => 'LOGIN');
                      
    SYS.UTL_SMTP.QUIT(l_conn);
                      

    a_Message.logMsgText(p_message => 'SMTP Server available.',
                         p_type    => a_Message.gInfo);

    RETURN a_Data.gTrue;
  EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        SYS.UTL_SMTP.QUIT(l_conn);
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
     
      a_Message.logMsgCode(p_code    => 'XSYS-00002',
                           p_type    => a_Message.gInfo,
                           p_stack   => DBMS_UTILITY.FORMAT_ERROR_STACK,
                           p_trace   => DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
                           
    RETURN a_Data.gFalse;
  END smtpAvailable;
  
  ------------------------------------------------------------------------------------------
  
  PROCEDURE sendMail (p_subject IN VARCHAR2,
                      p_message IN BLOB,
                      p_to      IN VARCHAR2,
                      p_from    IN VARCHAR2) IS
    l_conn SYS.UTL_SMTP.CONNECTION;
    l_body CLOB;
    
    PROCEDURE send_header(p_name   IN VARCHAR2,
                          p_header IN VARCHAR2) AS
    BEGIN
      SYS.UTL_SMTP.WRITE_DATA(l_conn, p_name || ': ' || p_header || SYS.UTL_TCP.CRLF);
    END send_header;
  BEGIN
  /*
    l_conn := SYS.UTL_SMTP.OPEN_CONNECTION(host                          => g_smtpHost,
                                           port                          => g_smtpPort,
                                           wallet_path                   => 'file:/etc/ORACLE/WALLETS',--file:/usr/local/wallet',
                                   --      wallet_password               => 'tradeIgnitePW!',
                                           secure_connection_before_smtp => FALSE);
                                           
    SYS.UTL_SMTP.HELO(l_conn, 'tradeignite.com');
    
    SYS.UTL_SMTP.STARTTLS(l_conn);
    
    SYS.UTL_SMTP.AUTH(c        => l_conn,
                      username => g_smtpUser,
                      password => g_smtpPW,
                      schemes  => 'LOGIN');
                      
    SYS.UTL_SMTP.MAIL(l_conn, p_from);
    SYS.UTL_SMTP.RCPT(l_conn, p_to);
    
    SYS.UTL_SMTP.OPEN_DATA(l_conn);

    send_header('Subject', p_subject);
*/
    
    decryptLob(p_in  => p_message,
               p_key => p_to,
               p_out => l_body);
               
    SYS.UTL_MAIL.SEND(SENDER => p_from, RECIPIENTS => p_to, SUBJECT => p_subject, MESSAGE => l_body);
/*    
    SYS.UTL_SMTP.WRITE_DATA(l_conn, SYS.UTL_TCP.CRLF || l_body);
    
    SYS.UTL_SMTP.CLOSE_DATA(l_conn);
    SYS.UTL_SMTP.QUIT(l_conn);
*/
                  
    a_Message.logMsgText(p_message => 'Email sent to '||p_to||'. Subject is '||p_subject,
                         p_type    => a_Message.gInfo);
  EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        SYS.UTL_SMTP.QUIT(l_conn);
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
      
      RAISE;
      
      a_Message.logMsgCode(p_code    => 'XSYS-00002',
                           p_data    => a_Data.addTag(
                                           a_Data.addTag(NULL,'EMAIL_TO',  p_to),
                                                     'SUBJECT',   p_subject),
                           p_type    => a_Message.gFatal,
                           p_stack   => DBMS_UTILITY.FORMAT_ERROR_STACK,
                           p_trace   => DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
  END sendMail;

END c_Mail;
/
