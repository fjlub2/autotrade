CREATE OR REPLACE PACKAGE ti_core_src.c_Security IS 
/* ----------------------------------------------------------------------------------------------------------
   Service APIs for related to user passwords and security stuff
   
   No grants or synonyms to anything
   
   Any references to SYS packages must have SYS. on the front to prevent package interception/spoofing
   ---------------------------------------------------------------------------------------------------------- */

 /* Change a users password while logged in */
  PROCEDURE updatePassword (p_oldPw  IN VARCHAR2,
                            p_newPw  IN VARCHAR2);
                            
 /* Change a users password using a temp link to identify the user */
  PROCEDURE updatePasswordExt (p_newPw  IN VARCHAR2,
                               p_code   IN VARCHAR2);
                           
 /* Checks the supplied password - if correct a_Data.gTrue is returned, otherwise a_Data.gFalse is returned */
  FUNCTION checkPassword (p_usrId   IN users.usr_id%TYPE,
                          p_pw      IN VARCHAR2) RETURN VARCHAR2;
                          
 /* Validates the supplied password against the username - returns a boolean
    Called by the APEX login screen */
  FUNCTION checkPassword (p_username   IN VARCHAR2,
                          p_password   IN VARCHAR2) RETURN BOOLEAN;
                          
 /* Resets the password of a user and emails it to them */
  PROCEDURE resetPassword (p_email   IN users.usr_email%TYPE);
  
 /* Sends registration email */
  PROCEDURE sendRegMail (p_sgnId IN signup.sgn_id%TYPE);
  
 /* Check temp link exists, is current and has not bee used */
  FUNCTION checkTempLink (p_type IN temp_links.tln_type%TYPE,
                          p_code IN temp_links.tln_link_code%TYPE) RETURN VARCHAR2;

 /* Encrypts a value */
  FUNCTION encrypt (p_value IN VARCHAR2, p_key IN INTEGER) RETURN RAW;
  
 /* Decrypts a value */
  FUNCTION decrypt (p_raw IN RAW, p_key IN INTEGER) RETURN VARCHAR2;
  
 /* Decrypts and returns the last n characters of a value. n Must be less than 50% of the characters in the value */
  FUNCTION decrypt (p_raw IN RAW, p_key IN INTEGER, p_digitsToShow IN INTEGER) RETURN VARCHAR2;
  
 /* Checks that the supplied password conforms to the password policy. Returns a message code if there was an error */
  FUNCTION passwordPolicy (p_pw IN VARCHAR2) RETURN VARCHAR2;
  
 /* Create user from signup data */
  PROCEDURE createUser (p_code IN temp_links.tln_link_code%TYPE,
                        p_pw   IN VARCHAR2);
END c_Security;
/
