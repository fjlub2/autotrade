CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_UserSessions IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for user_sessions table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF user_sessions%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              uss_usr_id   user_sessions.uss_usr_id%TYPE,
                              uss_session_start   user_sessions.uss_session_start%TYPE,
                              uss_session_id   user_sessions.uss_session_id%TYPE,
                              uss_user_ip   user_sessions.uss_user_ip%TYPE,
                              uss_session_end   user_sessions.uss_session_end%TYPE,
                              uss_lng_id   user_sessions.uss_lng_id%TYPE,
                              uss_code   user_sessions.uss_code%TYPE,
                              uss_last_used   user_sessions.uss_last_used%TYPE,
                              uss_timezone_offset   user_sessions.uss_timezone_offset%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              uss_usr_id   VARCHAR2(1),
                              uss_session_start   VARCHAR2(1),
                              uss_session_id   VARCHAR2(1),
                              uss_user_ip   VARCHAR2(1),
                              uss_session_end   VARCHAR2(1),
                              uss_lng_id   VARCHAR2(1),
                              uss_code   VARCHAR2(1),
                              uss_last_used   VARCHAR2(1),
                              uss_timezone_offset   VARCHAR2(1));

 /* Returns a row from the USER_SESSIONS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions%ROWTYPE;

 /* Pipes a row from the USER_SESSIONS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_SESSIONS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the USER_SESSIONS table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions%ROWTYPE;

 /* Pipes a row from the USER_SESSIONS table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_SESSIONS table for the supplied UK */
  FUNCTION exists_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN VARCHAR2;

 /* Returns USS_ID from the USER_SESSIONS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_id%TYPE;

 /* Returns USS_ID from the USER_SESSIONS table for the supplied UK */
  FUNCTION getId_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_id%TYPE;

 /* Returns USS_USR_ID from the USER_SESSIONS table for the supplied PK */
  FUNCTION getUsrId_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_usr_id%TYPE;

 /* Returns USS_USR_ID from the USER_SESSIONS table for the supplied UK */
  FUNCTION getUsrId_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_usr_id%TYPE;

 /* Returns USS_SESSION_START from the USER_SESSIONS table for the supplied PK */
  FUNCTION getSessionStart_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_session_start%TYPE;

 /* Returns USS_SESSION_START from the USER_SESSIONS table for the supplied UK */
  FUNCTION getSessionStart_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_session_start%TYPE;

 /* Returns USS_SESSION_ID from the USER_SESSIONS table for the supplied PK */
  FUNCTION getSessionId_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_session_id%TYPE;

 /* Returns USS_SESSION_ID from the USER_SESSIONS table for the supplied UK */
  FUNCTION getSessionId_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_session_id%TYPE;

 /* Returns USS_USER_IP from the USER_SESSIONS table for the supplied PK */
  FUNCTION getUserIp_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_user_ip%TYPE;

 /* Returns USS_USER_IP from the USER_SESSIONS table for the supplied UK */
  FUNCTION getUserIp_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_user_ip%TYPE;

 /* Returns USS_SESSION_END from the USER_SESSIONS table for the supplied PK */
  FUNCTION getSessionEnd_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_session_end%TYPE;

 /* Returns USS_SESSION_END from the USER_SESSIONS table for the supplied UK */
  FUNCTION getSessionEnd_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_session_end%TYPE;

 /* Returns USS_LNG_ID from the USER_SESSIONS table for the supplied PK */
  FUNCTION getLngId_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_lng_id%TYPE;

 /* Returns USS_LNG_ID from the USER_SESSIONS table for the supplied UK */
  FUNCTION getLngId_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_lng_id%TYPE;

 /* Returns USS_CODE from the USER_SESSIONS table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_code%TYPE;

 /* Returns USS_CODE from the USER_SESSIONS table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_code%TYPE;

 /* Returns USS_LAST_USED from the USER_SESSIONS table for the supplied PK */
  FUNCTION getLastUsed_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_last_used%TYPE;

 /* Returns USS_LAST_USED from the USER_SESSIONS table for the supplied UK */
  FUNCTION getLastUsed_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_last_used%TYPE;

 /* Returns USS_TIMEZONE_OFFSET from the USER_SESSIONS table for the supplied PK */
  FUNCTION getTimezoneOffset_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_timezone_offset%TYPE;

 /* Returns USS_TIMEZONE_OFFSET from the USER_SESSIONS table for the supplied UK */
  FUNCTION getTimezoneOffset_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_timezone_offset%TYPE;

 /* Updates USER_SESSIONS.USS_USR_ID to the supplied value for the supplied PK */
  PROCEDURE setUsrId (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_usr_id%TYPE);

 /* Updates USER_SESSIONS.USS_SESSION_START to the supplied value for the supplied PK */
  PROCEDURE setSessionStart (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_session_start%TYPE);

 /* Updates USER_SESSIONS.USS_SESSION_ID to the supplied value for the supplied PK */
  PROCEDURE setSessionId (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_session_id%TYPE);

 /* Updates USER_SESSIONS.USS_USER_IP to the supplied value for the supplied PK */
  PROCEDURE setUserIp (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_user_ip%TYPE);

 /* Updates USER_SESSIONS.USS_SESSION_END to the supplied value for the supplied PK */
  PROCEDURE setSessionEnd (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_session_end%TYPE);

 /* Updates USER_SESSIONS.USS_LNG_ID to the supplied value for the supplied PK */
  PROCEDURE setLngId (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_lng_id%TYPE);

 /* Updates USER_SESSIONS.USS_CODE to the supplied value for the supplied PK */
  PROCEDURE setCode (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_code%TYPE);

 /* Updates USER_SESSIONS.USS_LAST_USED to the supplied value for the supplied PK */
  PROCEDURE setLastUsed (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_last_used%TYPE);

 /* Updates USER_SESSIONS.USS_TIMEZONE_OFFSET to the supplied value for the supplied PK */
  PROCEDURE setTimezoneOffset (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_timezone_offset%TYPE);

 /* Updates a row on the USER_SESSIONS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN user_sessions.uss_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the USER_SESSIONS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN user_sessions.uss_id%TYPE, 
                      p_usr_id IN user_sessions.uss_usr_id%TYPE,
                      p_session_start IN user_sessions.uss_session_start%TYPE,
                      p_session_id IN user_sessions.uss_session_id%TYPE,
                      p_user_ip IN user_sessions.uss_user_ip%TYPE,
                      p_session_end IN user_sessions.uss_session_end%TYPE,
                      p_lng_id IN user_sessions.uss_lng_id%TYPE,
                      p_code IN user_sessions.uss_code%TYPE,
                      p_last_used IN user_sessions.uss_last_used%TYPE,
                      p_timezone_offset IN user_sessions.uss_timezone_offset%TYPE );

 /* Inserts a row into the USER_SESSIONS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN user_sessions%ROWTYPE, p_newPk OUT user_sessions.uss_id%TYPE);

 /* Inserts a row into the USER_SESSIONS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN user_sessions.uss_id%TYPE,
                      p_usr_id IN user_sessions.uss_usr_id%TYPE,
                      p_session_start IN user_sessions.uss_session_start%TYPE,
                      p_session_id IN user_sessions.uss_session_id%TYPE,
                      p_user_ip IN user_sessions.uss_user_ip%TYPE,
                      p_session_end IN user_sessions.uss_session_end%TYPE,
                      p_lng_id IN user_sessions.uss_lng_id%TYPE,
                      p_code IN user_sessions.uss_code%TYPE,
                      p_last_used IN user_sessions.uss_last_used%TYPE,
                      p_timezone_offset IN user_sessions.uss_timezone_offset%TYPE, p_newPk OUT user_sessions.uss_id%TYPE);

 /* Deletes a row from the USER_SESSIONS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN user_sessions.uss_id%TYPE);

END t_UserSessions;
/
