CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_Modules IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for modules table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF modules%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              mod_app_id   modules.mod_app_id%TYPE,
                              mod_name   modules.mod_name%TYPE,
                              mod_enabled_ind   modules.mod_enabled_ind%TYPE,
                              mod_code   modules.mod_code%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              mod_app_id   VARCHAR2(1),
                              mod_name   VARCHAR2(1),
                              mod_enabled_ind   VARCHAR2(1),
                              mod_code   VARCHAR2(1));

 /* Returns a row from the MODULES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN modules.mod_id%TYPE) RETURN modules%ROWTYPE;

 /* Pipes a row from the MODULES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN modules.mod_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the MODULES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN modules.mod_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the MODULES table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN modules.mod_code%TYPE) RETURN modules%ROWTYPE;

 /* Pipes a row from the MODULES table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN modules.mod_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the MODULES table for the supplied UK */
  FUNCTION exists_UK (p_uk IN modules.mod_code%TYPE) RETURN VARCHAR2;

 /* Returns MOD_ID from the MODULES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN modules.mod_id%TYPE) RETURN modules.mod_id%TYPE;

 /* Returns MOD_ID from the MODULES table for the supplied UK */
  FUNCTION getId_UK (p_uk IN modules.mod_code%TYPE) RETURN modules.mod_id%TYPE;

 /* Returns MOD_APP_ID from the MODULES table for the supplied PK */
  FUNCTION getAppId_PK (p_pk IN modules.mod_id%TYPE) RETURN modules.mod_app_id%TYPE;

 /* Returns MOD_APP_ID from the MODULES table for the supplied UK */
  FUNCTION getAppId_UK (p_uk IN modules.mod_code%TYPE) RETURN modules.mod_app_id%TYPE;

 /* Returns MOD_NAME from the MODULES table for the supplied PK */
  FUNCTION getName_PK (p_pk IN modules.mod_id%TYPE) RETURN modules.mod_name%TYPE;

 /* Returns MOD_NAME from the MODULES table for the supplied UK */
  FUNCTION getName_UK (p_uk IN modules.mod_code%TYPE) RETURN modules.mod_name%TYPE;

 /* Returns MOD_ENABLED_IND from the MODULES table for the supplied PK */
  FUNCTION getEnabledInd_PK (p_pk IN modules.mod_id%TYPE) RETURN modules.mod_enabled_ind%TYPE;

 /* Returns MOD_ENABLED_IND from the MODULES table for the supplied UK */
  FUNCTION getEnabledInd_UK (p_uk IN modules.mod_code%TYPE) RETURN modules.mod_enabled_ind%TYPE;

 /* Returns MOD_CODE from the MODULES table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN modules.mod_id%TYPE) RETURN modules.mod_code%TYPE;

 /* Returns MOD_CODE from the MODULES table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN modules.mod_code%TYPE) RETURN modules.mod_code%TYPE;

 /* Updates MODULES.MOD_APP_ID to the supplied value for the supplied PK */
  PROCEDURE setAppId (p_pk IN modules.mod_id%TYPE, p_val IN modules.mod_app_id%TYPE);

 /* Updates MODULES.MOD_NAME to the supplied value for the supplied PK */
  PROCEDURE setName (p_pk IN modules.mod_id%TYPE, p_val IN modules.mod_name%TYPE);

 /* Updates MODULES.MOD_ENABLED_IND to the supplied value for the supplied PK */
  PROCEDURE setEnabledInd (p_pk IN modules.mod_id%TYPE, p_val IN modules.mod_enabled_ind%TYPE);

 /* Updates MODULES.MOD_CODE to the supplied value for the supplied PK */
  PROCEDURE setCode (p_pk IN modules.mod_id%TYPE, p_val IN modules.mod_code%TYPE);

 /* Updates a row on the MODULES table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN modules.mod_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the MODULES table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN modules.mod_id%TYPE, 
                      p_app_id IN modules.mod_app_id%TYPE,
                      p_name IN modules.mod_name%TYPE,
                      p_enabled_ind IN modules.mod_enabled_ind%TYPE,
                      p_code IN modules.mod_code%TYPE );

 /* Inserts a row into the MODULES table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN modules%ROWTYPE, p_newPk OUT modules.mod_id%TYPE);

 /* Inserts a row into the MODULES table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN modules.mod_id%TYPE,
                      p_app_id IN modules.mod_app_id%TYPE,
                      p_name IN modules.mod_name%TYPE,
                      p_enabled_ind IN modules.mod_enabled_ind%TYPE,
                      p_code IN modules.mod_code%TYPE, p_newPk OUT modules.mod_id%TYPE);

 /* Deletes a row from the MODULES table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN modules.mod_id%TYPE);

END t_Modules;
/
