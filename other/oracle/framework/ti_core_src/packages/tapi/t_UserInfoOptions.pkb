CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_UserInfoOptions IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_info_options.uio_id%TYPE, p_uk IN user_info_options.uio_uif_id%TYPE DEFAULT NULL, p_uk2 IN user_info_options.uio_return%TYPE DEFAULT NULL) RETURN user_info_options%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN user_info_options.uio_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN user_info_options.uio_id%TYPE);

  PROCEDURE doInsert (p_row IN user_info_options%ROWTYPE, p_newPk OUT user_info_options.uio_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN user_info_options%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN VARCHAR2 IS
    l_row    user_info_options%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.uio_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN user_info_options%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk, p_uk2 => p_uk2);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk, p_uk2 => p_uk2));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN VARCHAR2 IS
    l_row    user_info_options%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    IF l_row.uio_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN user_info_options.uio_id%TYPE IS
    l_row    user_info_options%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uio_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN user_info_options.uio_id%TYPE IS
    l_row    user_info_options%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uio_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUifId_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN user_info_options.uio_uif_id%TYPE IS
    l_row    user_info_options%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uio_uif_id;
  END getUifId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUifId_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN user_info_options.uio_uif_id%TYPE IS
    l_row    user_info_options%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uio_uif_id;
  END getUifId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getReturn_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN user_info_options.uio_return%TYPE IS
    l_row    user_info_options%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uio_return;
  END getReturn_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getReturn_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN user_info_options.uio_return%TYPE IS
    l_row    user_info_options%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uio_return;
  END getReturn_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDisplay_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN user_info_options.uio_display%TYPE IS
    l_row    user_info_options%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uio_display;
  END getDisplay_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDisplay_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN user_info_options.uio_display%TYPE IS
    l_row    user_info_options%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uio_display;
  END getDisplay_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setUifId (p_pk IN user_info_options.uio_id%TYPE, p_val IN user_info_options.uio_uif_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uio_uif_id := a_data.gFalse;
    l_ignoreRow.uio_return := a_data.gTrue;
    l_ignoreRow.uio_display := a_data.gTrue;

    l_updateRow.uio_uif_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUifId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setReturn (p_pk IN user_info_options.uio_id%TYPE, p_val IN user_info_options.uio_return%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uio_uif_id := a_data.gTrue;
    l_ignoreRow.uio_return := a_data.gFalse;
    l_ignoreRow.uio_display := a_data.gTrue;

    l_updateRow.uio_return := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setReturn;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDisplay (p_pk IN user_info_options.uio_id%TYPE, p_val IN user_info_options.uio_display%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uio_uif_id := a_data.gTrue;
    l_ignoreRow.uio_return := a_data.gTrue;
    l_ignoreRow.uio_display := a_data.gFalse;

    l_updateRow.uio_display := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDisplay;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_info_options.uio_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_info_options.uio_id%TYPE, 
                      p_uif_id IN user_info_options.uio_uif_id%TYPE,
                      p_return IN user_info_options.uio_return%TYPE,
                      p_display IN user_info_options.uio_display%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.uio_uif_id := p_uif_id;
    l_row.uio_return := p_return;
    l_row.uio_display := p_display;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN user_info_options%ROWTYPE, p_newPK OUT user_info_options.uio_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN user_info_options.uio_id%TYPE,
                      p_uif_id IN user_info_options.uio_uif_id%TYPE,
                      p_return IN user_info_options.uio_return%TYPE,
                      p_display IN user_info_options.uio_display%TYPE, p_newPK OUT user_info_options.uio_id%TYPE) IS
    l_row   user_info_options%ROWTYPE;
  BEGIN
    l_row.uio_id := p_id;
    l_row.uio_uif_id := p_uif_id;
    l_row.uio_return := p_return;
    l_row.uio_display := p_display;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN user_info_options.uio_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_info_options.uio_id%TYPE, p_uk IN user_info_options.uio_uif_id%TYPE DEFAULT NULL, p_uk2 IN user_info_options.uio_return%TYPE DEFAULT NULL) RETURN user_info_options%ROWTYPE IS
    l_row    user_info_options%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_info_options
       WHERE uio_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_info_options
       WHERE uio_uif_id = p_uk
         AND ((p_uk2 IS NOT NULL AND
               uio_return = p_uk2) OR
              (p_uk2 IS NULL AND
               uio_return IS NULL));
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN user_info_options.uio_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE user_info_options
         SET 
             uio_uif_id = CASE NVL(p_ignore.uio_uif_id, a_data.gFalse) WHEN a_data.gTrue THEN uio_uif_id ELSE p_row.uio_uif_id END,
             uio_return = CASE NVL(p_ignore.uio_return, a_data.gFalse) WHEN a_data.gTrue THEN uio_return ELSE p_row.uio_return END,
             uio_display = CASE NVL(p_ignore.uio_display, a_data.gFalse) WHEN a_data.gTrue THEN uio_display ELSE p_row.uio_display END
         WHERE uio_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN user_info_options.uio_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE user_info_options
       WHERE uio_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN user_info_options%ROWTYPE, p_newPk OUT user_info_options.uio_id%TYPE) IS
    l_pk    user_info_options.uio_id%TYPE;
  BEGIN 
    INSERT
      INTO user_info_options
    VALUES p_row
    RETURNING uio_id
         INTO p_newPk;
  END doInsert;

END t_UserInfoOptions;
/
