CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_Users IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN users.usr_id%TYPE, p_uk IN users.usr_username%TYPE DEFAULT NULL) RETURN users%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN users.usr_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN users.usr_id%TYPE);

  PROCEDURE doInsert (p_row IN users%ROWTYPE, p_newPk OUT users.usr_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN users.usr_id%TYPE) RETURN users%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN users.usr_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN users.usr_id%TYPE) RETURN VARCHAR2 IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.usr_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN users.usr_username%TYPE) RETURN users%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN users.usr_username%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN users.usr_username%TYPE) RETURN VARCHAR2 IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.usr_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_id%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_id%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsername_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_username%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_username;
  END getUsername_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsername_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_username%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_username;
  END getUsername_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getFirstName_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_first_name%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_first_name;
  END getFirstName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getFirstName_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_first_name%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_first_name;
  END getFirstName_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLastName_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_last_name%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_last_name;
  END getLastName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLastName_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_last_name%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_last_name;
  END getLastName_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEmail_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_email%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_email;
  END getEmail_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEmail_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_email%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_email;
  END getEmail_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getStatus_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_status%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_status;
  END getStatus_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getStatus_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_status%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_status;
  END getStatus_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPassword_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_password%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_password;
  END getPassword_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPassword_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_password%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_password;
  END getPassword_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPwdTries_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_pwd_tries%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_pwd_tries;
  END getPwdTries_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getPwdTries_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_pwd_tries%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_pwd_tries;
  END getPwdTries_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getChangePwOnLogon_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_change_pw_on_logon%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_change_pw_on_logon;
  END getChangePwOnLogon_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getChangePwOnLogon_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_change_pw_on_logon%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_change_pw_on_logon;
  END getChangePwOnLogon_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLngId_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_lng_id%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_lng_id;
  END getLngId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLngId_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_lng_id%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_lng_id;
  END getLngId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMessageLevel_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_message_level%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_message_level;
  END getMessageLevel_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMessageLevel_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_message_level%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_message_level;
  END getMessageLevel_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTimezoneOffset_PK (p_pk IN users.usr_id%TYPE) RETURN users.usr_timezone_offset%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usr_timezone_offset;
  END getTimezoneOffset_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTimezoneOffset_UK (p_uk IN users.usr_username%TYPE) RETURN users.usr_timezone_offset%TYPE IS
    l_row    users%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.usr_timezone_offset;
  END getTimezoneOffset_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setUsername (p_pk IN users.usr_id%TYPE, p_val IN users.usr_username%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gFalse;
    l_ignoreRow.usr_first_name := a_data.gTrue;
    l_ignoreRow.usr_last_name := a_data.gTrue;
    l_ignoreRow.usr_email := a_data.gTrue;
    l_ignoreRow.usr_status := a_data.gTrue;
    l_ignoreRow.usr_password := a_data.gTrue;
    l_ignoreRow.usr_pwd_tries := a_data.gTrue;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gTrue;
    l_ignoreRow.usr_lng_id := a_data.gTrue;
    l_ignoreRow.usr_message_level := a_data.gTrue;
    l_ignoreRow.usr_timezone_offset := a_data.gTrue;

    l_updateRow.usr_username := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsername;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setFirstName (p_pk IN users.usr_id%TYPE, p_val IN users.usr_first_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gTrue;
    l_ignoreRow.usr_first_name := a_data.gFalse;
    l_ignoreRow.usr_last_name := a_data.gTrue;
    l_ignoreRow.usr_email := a_data.gTrue;
    l_ignoreRow.usr_status := a_data.gTrue;
    l_ignoreRow.usr_password := a_data.gTrue;
    l_ignoreRow.usr_pwd_tries := a_data.gTrue;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gTrue;
    l_ignoreRow.usr_lng_id := a_data.gTrue;
    l_ignoreRow.usr_message_level := a_data.gTrue;
    l_ignoreRow.usr_timezone_offset := a_data.gTrue;

    l_updateRow.usr_first_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setFirstName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLastName (p_pk IN users.usr_id%TYPE, p_val IN users.usr_last_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gTrue;
    l_ignoreRow.usr_first_name := a_data.gTrue;
    l_ignoreRow.usr_last_name := a_data.gFalse;
    l_ignoreRow.usr_email := a_data.gTrue;
    l_ignoreRow.usr_status := a_data.gTrue;
    l_ignoreRow.usr_password := a_data.gTrue;
    l_ignoreRow.usr_pwd_tries := a_data.gTrue;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gTrue;
    l_ignoreRow.usr_lng_id := a_data.gTrue;
    l_ignoreRow.usr_message_level := a_data.gTrue;
    l_ignoreRow.usr_timezone_offset := a_data.gTrue;

    l_updateRow.usr_last_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLastName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEmail (p_pk IN users.usr_id%TYPE, p_val IN users.usr_email%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gTrue;
    l_ignoreRow.usr_first_name := a_data.gTrue;
    l_ignoreRow.usr_last_name := a_data.gTrue;
    l_ignoreRow.usr_email := a_data.gFalse;
    l_ignoreRow.usr_status := a_data.gTrue;
    l_ignoreRow.usr_password := a_data.gTrue;
    l_ignoreRow.usr_pwd_tries := a_data.gTrue;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gTrue;
    l_ignoreRow.usr_lng_id := a_data.gTrue;
    l_ignoreRow.usr_message_level := a_data.gTrue;
    l_ignoreRow.usr_timezone_offset := a_data.gTrue;

    l_updateRow.usr_email := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEmail;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setStatus (p_pk IN users.usr_id%TYPE, p_val IN users.usr_status%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gTrue;
    l_ignoreRow.usr_first_name := a_data.gTrue;
    l_ignoreRow.usr_last_name := a_data.gTrue;
    l_ignoreRow.usr_email := a_data.gTrue;
    l_ignoreRow.usr_status := a_data.gFalse;
    l_ignoreRow.usr_password := a_data.gTrue;
    l_ignoreRow.usr_pwd_tries := a_data.gTrue;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gTrue;
    l_ignoreRow.usr_lng_id := a_data.gTrue;
    l_ignoreRow.usr_message_level := a_data.gTrue;
    l_ignoreRow.usr_timezone_offset := a_data.gTrue;

    l_updateRow.usr_status := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setStatus;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setPassword (p_pk IN users.usr_id%TYPE, p_val IN users.usr_password%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gTrue;
    l_ignoreRow.usr_first_name := a_data.gTrue;
    l_ignoreRow.usr_last_name := a_data.gTrue;
    l_ignoreRow.usr_email := a_data.gTrue;
    l_ignoreRow.usr_status := a_data.gTrue;
    l_ignoreRow.usr_password := a_data.gFalse;
    l_ignoreRow.usr_pwd_tries := a_data.gTrue;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gTrue;
    l_ignoreRow.usr_lng_id := a_data.gTrue;
    l_ignoreRow.usr_message_level := a_data.gTrue;
    l_ignoreRow.usr_timezone_offset := a_data.gTrue;

    l_updateRow.usr_password := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setPassword;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setPwdTries (p_pk IN users.usr_id%TYPE, p_val IN users.usr_pwd_tries%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gTrue;
    l_ignoreRow.usr_first_name := a_data.gTrue;
    l_ignoreRow.usr_last_name := a_data.gTrue;
    l_ignoreRow.usr_email := a_data.gTrue;
    l_ignoreRow.usr_status := a_data.gTrue;
    l_ignoreRow.usr_password := a_data.gTrue;
    l_ignoreRow.usr_pwd_tries := a_data.gFalse;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gTrue;
    l_ignoreRow.usr_lng_id := a_data.gTrue;
    l_ignoreRow.usr_message_level := a_data.gTrue;
    l_ignoreRow.usr_timezone_offset := a_data.gTrue;

    l_updateRow.usr_pwd_tries := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setPwdTries;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setChangePwOnLogon (p_pk IN users.usr_id%TYPE, p_val IN users.usr_change_pw_on_logon%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gTrue;
    l_ignoreRow.usr_first_name := a_data.gTrue;
    l_ignoreRow.usr_last_name := a_data.gTrue;
    l_ignoreRow.usr_email := a_data.gTrue;
    l_ignoreRow.usr_status := a_data.gTrue;
    l_ignoreRow.usr_password := a_data.gTrue;
    l_ignoreRow.usr_pwd_tries := a_data.gTrue;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gFalse;
    l_ignoreRow.usr_lng_id := a_data.gTrue;
    l_ignoreRow.usr_message_level := a_data.gTrue;
    l_ignoreRow.usr_timezone_offset := a_data.gTrue;

    l_updateRow.usr_change_pw_on_logon := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setChangePwOnLogon;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLngId (p_pk IN users.usr_id%TYPE, p_val IN users.usr_lng_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gTrue;
    l_ignoreRow.usr_first_name := a_data.gTrue;
    l_ignoreRow.usr_last_name := a_data.gTrue;
    l_ignoreRow.usr_email := a_data.gTrue;
    l_ignoreRow.usr_status := a_data.gTrue;
    l_ignoreRow.usr_password := a_data.gTrue;
    l_ignoreRow.usr_pwd_tries := a_data.gTrue;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gTrue;
    l_ignoreRow.usr_lng_id := a_data.gFalse;
    l_ignoreRow.usr_message_level := a_data.gTrue;
    l_ignoreRow.usr_timezone_offset := a_data.gTrue;

    l_updateRow.usr_lng_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLngId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setMessageLevel (p_pk IN users.usr_id%TYPE, p_val IN users.usr_message_level%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gTrue;
    l_ignoreRow.usr_first_name := a_data.gTrue;
    l_ignoreRow.usr_last_name := a_data.gTrue;
    l_ignoreRow.usr_email := a_data.gTrue;
    l_ignoreRow.usr_status := a_data.gTrue;
    l_ignoreRow.usr_password := a_data.gTrue;
    l_ignoreRow.usr_pwd_tries := a_data.gTrue;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gTrue;
    l_ignoreRow.usr_lng_id := a_data.gTrue;
    l_ignoreRow.usr_message_level := a_data.gFalse;
    l_ignoreRow.usr_timezone_offset := a_data.gTrue;

    l_updateRow.usr_message_level := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setMessageLevel;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTimezoneOffset (p_pk IN users.usr_id%TYPE, p_val IN users.usr_timezone_offset%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usr_username := a_data.gTrue;
    l_ignoreRow.usr_first_name := a_data.gTrue;
    l_ignoreRow.usr_last_name := a_data.gTrue;
    l_ignoreRow.usr_email := a_data.gTrue;
    l_ignoreRow.usr_status := a_data.gTrue;
    l_ignoreRow.usr_password := a_data.gTrue;
    l_ignoreRow.usr_pwd_tries := a_data.gTrue;
    l_ignoreRow.usr_change_pw_on_logon := a_data.gTrue;
    l_ignoreRow.usr_lng_id := a_data.gTrue;
    l_ignoreRow.usr_message_level := a_data.gTrue;
    l_ignoreRow.usr_timezone_offset := a_data.gFalse;

    l_updateRow.usr_timezone_offset := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTimezoneOffset;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN users.usr_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN users.usr_id%TYPE, 
                      p_username IN users.usr_username%TYPE,
                      p_first_name IN users.usr_first_name%TYPE,
                      p_last_name IN users.usr_last_name%TYPE,
                      p_email IN users.usr_email%TYPE,
                      p_status IN users.usr_status%TYPE,
                      p_password IN users.usr_password%TYPE,
                      p_pwd_tries IN users.usr_pwd_tries%TYPE,
                      p_change_pw_on_logon IN users.usr_change_pw_on_logon%TYPE,
                      p_lng_id IN users.usr_lng_id%TYPE,
                      p_message_level IN users.usr_message_level%TYPE,
                      p_timezone_offset IN users.usr_timezone_offset%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.usr_username := p_username;
    l_row.usr_first_name := p_first_name;
    l_row.usr_last_name := p_last_name;
    l_row.usr_email := p_email;
    l_row.usr_status := p_status;
    l_row.usr_password := p_password;
    l_row.usr_pwd_tries := p_pwd_tries;
    l_row.usr_change_pw_on_logon := p_change_pw_on_logon;
    l_row.usr_lng_id := p_lng_id;
    l_row.usr_message_level := p_message_level;
    l_row.usr_timezone_offset := p_timezone_offset;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN users%ROWTYPE, p_newPK OUT users.usr_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN users.usr_id%TYPE,
                      p_username IN users.usr_username%TYPE,
                      p_first_name IN users.usr_first_name%TYPE,
                      p_last_name IN users.usr_last_name%TYPE,
                      p_email IN users.usr_email%TYPE,
                      p_status IN users.usr_status%TYPE,
                      p_password IN users.usr_password%TYPE,
                      p_pwd_tries IN users.usr_pwd_tries%TYPE,
                      p_change_pw_on_logon IN users.usr_change_pw_on_logon%TYPE,
                      p_lng_id IN users.usr_lng_id%TYPE,
                      p_message_level IN users.usr_message_level%TYPE,
                      p_timezone_offset IN users.usr_timezone_offset%TYPE, p_newPK OUT users.usr_id%TYPE) IS
    l_row   users%ROWTYPE;
  BEGIN
    l_row.usr_id := p_id;
    l_row.usr_username := p_username;
    l_row.usr_first_name := p_first_name;
    l_row.usr_last_name := p_last_name;
    l_row.usr_email := p_email;
    l_row.usr_status := p_status;
    l_row.usr_password := p_password;
    l_row.usr_pwd_tries := p_pwd_tries;
    l_row.usr_change_pw_on_logon := p_change_pw_on_logon;
    l_row.usr_lng_id := p_lng_id;
    l_row.usr_message_level := p_message_level;
    l_row.usr_timezone_offset := p_timezone_offset;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN users.usr_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN users.usr_id%TYPE, p_uk IN users.usr_username%TYPE DEFAULT NULL) RETURN users%ROWTYPE IS
    l_row    users%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM users
       WHERE usr_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM users
       WHERE usr_username = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN users.usr_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE users
         SET 
             usr_username = CASE NVL(p_ignore.usr_username, a_data.gFalse) WHEN a_data.gTrue THEN usr_username ELSE p_row.usr_username END,
             usr_first_name = CASE NVL(p_ignore.usr_first_name, a_data.gFalse) WHEN a_data.gTrue THEN usr_first_name ELSE p_row.usr_first_name END,
             usr_last_name = CASE NVL(p_ignore.usr_last_name, a_data.gFalse) WHEN a_data.gTrue THEN usr_last_name ELSE p_row.usr_last_name END,
             usr_email = CASE NVL(p_ignore.usr_email, a_data.gFalse) WHEN a_data.gTrue THEN usr_email ELSE p_row.usr_email END,
             usr_status = CASE NVL(p_ignore.usr_status, a_data.gFalse) WHEN a_data.gTrue THEN usr_status ELSE p_row.usr_status END,
             usr_password = CASE NVL(p_ignore.usr_password, a_data.gFalse) WHEN a_data.gTrue THEN usr_password ELSE p_row.usr_password END,
             usr_pwd_tries = CASE NVL(p_ignore.usr_pwd_tries, a_data.gFalse) WHEN a_data.gTrue THEN usr_pwd_tries ELSE p_row.usr_pwd_tries END,
             usr_change_pw_on_logon = CASE NVL(p_ignore.usr_change_pw_on_logon, a_data.gFalse) WHEN a_data.gTrue THEN usr_change_pw_on_logon ELSE p_row.usr_change_pw_on_logon END,
             usr_lng_id = CASE NVL(p_ignore.usr_lng_id, a_data.gFalse) WHEN a_data.gTrue THEN usr_lng_id ELSE p_row.usr_lng_id END,
             usr_message_level = CASE NVL(p_ignore.usr_message_level, a_data.gFalse) WHEN a_data.gTrue THEN usr_message_level ELSE p_row.usr_message_level END,
             usr_timezone_offset = CASE NVL(p_ignore.usr_timezone_offset, a_data.gFalse) WHEN a_data.gTrue THEN usr_timezone_offset ELSE p_row.usr_timezone_offset END
         WHERE usr_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN users.usr_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE users
       WHERE usr_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN users%ROWTYPE, p_newPk OUT users.usr_id%TYPE) IS
    l_pk    users.usr_id%TYPE;
  BEGIN 
    INSERT
      INTO users
    VALUES p_row
    RETURNING usr_id
         INTO p_newPk;
  END doInsert;

END t_Users;
/
