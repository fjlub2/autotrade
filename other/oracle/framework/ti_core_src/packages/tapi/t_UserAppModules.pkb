CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_UserAppModules IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_app_modules.uam_id%TYPE, p_uk IN user_app_modules.uam_usa_id%TYPE DEFAULT NULL, p_uk2 IN user_app_modules.uam_mod_id%TYPE DEFAULT NULL) RETURN user_app_modules%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN user_app_modules.uam_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN user_app_modules.uam_id%TYPE);

  PROCEDURE doInsert (p_row IN user_app_modules%ROWTYPE, p_newPk OUT user_app_modules.uam_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN user_app_modules%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN VARCHAR2 IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.uam_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN user_app_modules%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk, p_uk2 => p_uk2);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk, p_uk2 => p_uk2));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN VARCHAR2 IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    IF l_row.uam_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN user_app_modules.uam_id%TYPE IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uam_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN user_app_modules.uam_id%TYPE IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uam_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsaId_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN user_app_modules.uam_usa_id%TYPE IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uam_usa_id;
  END getUsaId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsaId_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN user_app_modules.uam_usa_id%TYPE IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uam_usa_id;
  END getUsaId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getModId_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN user_app_modules.uam_mod_id%TYPE IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uam_mod_id;
  END getModId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getModId_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN user_app_modules.uam_mod_id%TYPE IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uam_mod_id;
  END getModId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN user_app_modules.uam_enabled_ind%TYPE IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uam_enabled_ind;
  END getEnabledInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN user_app_modules.uam_enabled_ind%TYPE IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.uam_enabled_ind;
  END getEnabledInd_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setUsaId (p_pk IN user_app_modules.uam_id%TYPE, p_val IN user_app_modules.uam_usa_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uam_usa_id := a_data.gFalse;
    l_ignoreRow.uam_mod_id := a_data.gTrue;
    l_ignoreRow.uam_enabled_ind := a_data.gTrue;

    l_updateRow.uam_usa_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsaId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setModId (p_pk IN user_app_modules.uam_id%TYPE, p_val IN user_app_modules.uam_mod_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uam_usa_id := a_data.gTrue;
    l_ignoreRow.uam_mod_id := a_data.gFalse;
    l_ignoreRow.uam_enabled_ind := a_data.gTrue;

    l_updateRow.uam_mod_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setModId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEnabledInd (p_pk IN user_app_modules.uam_id%TYPE, p_val IN user_app_modules.uam_enabled_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uam_usa_id := a_data.gTrue;
    l_ignoreRow.uam_mod_id := a_data.gTrue;
    l_ignoreRow.uam_enabled_ind := a_data.gFalse;

    l_updateRow.uam_enabled_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEnabledInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_app_modules.uam_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_app_modules.uam_id%TYPE, 
                      p_usa_id IN user_app_modules.uam_usa_id%TYPE,
                      p_mod_id IN user_app_modules.uam_mod_id%TYPE,
                      p_enabled_ind IN user_app_modules.uam_enabled_ind%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.uam_usa_id := p_usa_id;
    l_row.uam_mod_id := p_mod_id;
    l_row.uam_enabled_ind := p_enabled_ind;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN user_app_modules%ROWTYPE, p_newPK OUT user_app_modules.uam_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN user_app_modules.uam_id%TYPE,
                      p_usa_id IN user_app_modules.uam_usa_id%TYPE,
                      p_mod_id IN user_app_modules.uam_mod_id%TYPE,
                      p_enabled_ind IN user_app_modules.uam_enabled_ind%TYPE, p_newPK OUT user_app_modules.uam_id%TYPE) IS
    l_row   user_app_modules%ROWTYPE;
  BEGIN
    l_row.uam_id := p_id;
    l_row.uam_usa_id := p_usa_id;
    l_row.uam_mod_id := p_mod_id;
    l_row.uam_enabled_ind := p_enabled_ind;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN user_app_modules.uam_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_app_modules.uam_id%TYPE, p_uk IN user_app_modules.uam_usa_id%TYPE DEFAULT NULL, p_uk2 IN user_app_modules.uam_mod_id%TYPE DEFAULT NULL) RETURN user_app_modules%ROWTYPE IS
    l_row    user_app_modules%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_app_modules
       WHERE uam_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_app_modules
       WHERE uam_usa_id = p_uk
         AND ((p_uk2 IS NOT NULL AND
               uam_mod_id = p_uk2) OR
              (p_uk2 IS NULL AND
               uam_mod_id IS NULL));
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN user_app_modules.uam_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE user_app_modules
         SET 
             uam_usa_id = CASE NVL(p_ignore.uam_usa_id, a_data.gFalse) WHEN a_data.gTrue THEN uam_usa_id ELSE p_row.uam_usa_id END,
             uam_mod_id = CASE NVL(p_ignore.uam_mod_id, a_data.gFalse) WHEN a_data.gTrue THEN uam_mod_id ELSE p_row.uam_mod_id END,
             uam_enabled_ind = CASE NVL(p_ignore.uam_enabled_ind, a_data.gFalse) WHEN a_data.gTrue THEN uam_enabled_ind ELSE p_row.uam_enabled_ind END
         WHERE uam_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN user_app_modules.uam_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE user_app_modules
       WHERE uam_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN user_app_modules%ROWTYPE, p_newPk OUT user_app_modules.uam_id%TYPE) IS
    l_pk    user_app_modules.uam_id%TYPE;
  BEGIN 
    INSERT
      INTO user_app_modules
    VALUES p_row
    RETURNING uam_id
         INTO p_newPk;
  END doInsert;

END t_UserAppModules;
/
