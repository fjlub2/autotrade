CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_UserApps IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_apps.usa_id%TYPE, p_uk IN user_apps.usa_usr_id%TYPE DEFAULT NULL, p_uk2 IN user_apps.usa_app_id%TYPE DEFAULT NULL) RETURN user_apps%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN user_apps.usa_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN user_apps.usa_id%TYPE);

  PROCEDURE doInsert (p_row IN user_apps%ROWTYPE, p_newPk OUT user_apps.usa_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN user_apps.usa_id%TYPE) RETURN user_apps%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN user_apps.usa_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN user_apps.usa_id%TYPE) RETURN VARCHAR2 IS
    l_row    user_apps%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.usa_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN user_apps%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk, p_uk2 => p_uk2);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk, p_uk2 => p_uk2));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN VARCHAR2 IS
    l_row    user_apps%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    IF l_row.usa_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN user_apps.usa_id%TYPE) RETURN user_apps.usa_id%TYPE IS
    l_row    user_apps%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usa_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN user_apps.usa_id%TYPE IS
    l_row    user_apps%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.usa_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_PK (p_pk IN user_apps.usa_id%TYPE) RETURN user_apps.usa_usr_id%TYPE IS
    l_row    user_apps%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usa_usr_id;
  END getUsrId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN user_apps.usa_usr_id%TYPE IS
    l_row    user_apps%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.usa_usr_id;
  END getUsrId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAppId_PK (p_pk IN user_apps.usa_id%TYPE) RETURN user_apps.usa_app_id%TYPE IS
    l_row    user_apps%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usa_app_id;
  END getAppId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAppId_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN user_apps.usa_app_id%TYPE IS
    l_row    user_apps%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.usa_app_id;
  END getAppId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_PK (p_pk IN user_apps.usa_id%TYPE) RETURN user_apps.usa_enabled_ind%TYPE IS
    l_row    user_apps%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.usa_enabled_ind;
  END getEnabledInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN user_apps.usa_enabled_ind%TYPE IS
    l_row    user_apps%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.usa_enabled_ind;
  END getEnabledInd_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setUsrId (p_pk IN user_apps.usa_id%TYPE, p_val IN user_apps.usa_usr_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usa_usr_id := a_data.gFalse;
    l_ignoreRow.usa_app_id := a_data.gTrue;
    l_ignoreRow.usa_enabled_ind := a_data.gTrue;

    l_updateRow.usa_usr_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsrId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setAppId (p_pk IN user_apps.usa_id%TYPE, p_val IN user_apps.usa_app_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usa_usr_id := a_data.gTrue;
    l_ignoreRow.usa_app_id := a_data.gFalse;
    l_ignoreRow.usa_enabled_ind := a_data.gTrue;

    l_updateRow.usa_app_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setAppId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEnabledInd (p_pk IN user_apps.usa_id%TYPE, p_val IN user_apps.usa_enabled_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.usa_usr_id := a_data.gTrue;
    l_ignoreRow.usa_app_id := a_data.gTrue;
    l_ignoreRow.usa_enabled_ind := a_data.gFalse;

    l_updateRow.usa_enabled_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEnabledInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_apps.usa_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_apps.usa_id%TYPE, 
                      p_usr_id IN user_apps.usa_usr_id%TYPE,
                      p_app_id IN user_apps.usa_app_id%TYPE,
                      p_enabled_ind IN user_apps.usa_enabled_ind%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.usa_usr_id := p_usr_id;
    l_row.usa_app_id := p_app_id;
    l_row.usa_enabled_ind := p_enabled_ind;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN user_apps%ROWTYPE, p_newPK OUT user_apps.usa_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN user_apps.usa_id%TYPE,
                      p_usr_id IN user_apps.usa_usr_id%TYPE,
                      p_app_id IN user_apps.usa_app_id%TYPE,
                      p_enabled_ind IN user_apps.usa_enabled_ind%TYPE, p_newPK OUT user_apps.usa_id%TYPE) IS
    l_row   user_apps%ROWTYPE;
  BEGIN
    l_row.usa_id := p_id;
    l_row.usa_usr_id := p_usr_id;
    l_row.usa_app_id := p_app_id;
    l_row.usa_enabled_ind := p_enabled_ind;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN user_apps.usa_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_apps.usa_id%TYPE, p_uk IN user_apps.usa_usr_id%TYPE DEFAULT NULL, p_uk2 IN user_apps.usa_app_id%TYPE DEFAULT NULL) RETURN user_apps%ROWTYPE IS
    l_row    user_apps%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_apps
       WHERE usa_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_apps
       WHERE usa_usr_id = p_uk
         AND ((p_uk2 IS NOT NULL AND
               usa_app_id = p_uk2) OR
              (p_uk2 IS NULL AND
               usa_app_id IS NULL));
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN user_apps.usa_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE user_apps
         SET 
             usa_usr_id = CASE NVL(p_ignore.usa_usr_id, a_data.gFalse) WHEN a_data.gTrue THEN usa_usr_id ELSE p_row.usa_usr_id END,
             usa_app_id = CASE NVL(p_ignore.usa_app_id, a_data.gFalse) WHEN a_data.gTrue THEN usa_app_id ELSE p_row.usa_app_id END,
             usa_enabled_ind = CASE NVL(p_ignore.usa_enabled_ind, a_data.gFalse) WHEN a_data.gTrue THEN usa_enabled_ind ELSE p_row.usa_enabled_ind END
         WHERE usa_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN user_apps.usa_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE user_apps
       WHERE usa_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN user_apps%ROWTYPE, p_newPk OUT user_apps.usa_id%TYPE) IS
    l_pk    user_apps.usa_id%TYPE;
  BEGIN 
    INSERT
      INTO user_apps
    VALUES p_row
    RETURNING usa_id
         INTO p_newPk;
  END doInsert;

END t_UserApps;
/
