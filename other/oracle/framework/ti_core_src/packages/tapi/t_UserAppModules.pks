CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_UserAppModules IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for user_app_modules table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF user_app_modules%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              uam_usa_id   user_app_modules.uam_usa_id%TYPE,
                              uam_mod_id   user_app_modules.uam_mod_id%TYPE,
                              uam_enabled_ind   user_app_modules.uam_enabled_ind%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              uam_usa_id   VARCHAR2(1),
                              uam_mod_id   VARCHAR2(1),
                              uam_enabled_ind   VARCHAR2(1));

 /* Returns a row from the USER_APP_MODULES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN user_app_modules%ROWTYPE;

 /* Pipes a row from the USER_APP_MODULES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_APP_MODULES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the USER_APP_MODULES table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN user_app_modules%ROWTYPE;

 /* Pipes a row from the USER_APP_MODULES table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_APP_MODULES table for the supplied UK */
  FUNCTION exists_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN VARCHAR2;

 /* Returns UAM_ID from the USER_APP_MODULES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN user_app_modules.uam_id%TYPE;

 /* Returns UAM_ID from the USER_APP_MODULES table for the supplied UK */
  FUNCTION getId_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN user_app_modules.uam_id%TYPE;

 /* Returns UAM_USA_ID from the USER_APP_MODULES table for the supplied PK */
  FUNCTION getUsaId_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN user_app_modules.uam_usa_id%TYPE;

 /* Returns UAM_USA_ID from the USER_APP_MODULES table for the supplied UK */
  FUNCTION getUsaId_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN user_app_modules.uam_usa_id%TYPE;

 /* Returns UAM_MOD_ID from the USER_APP_MODULES table for the supplied PK */
  FUNCTION getModId_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN user_app_modules.uam_mod_id%TYPE;

 /* Returns UAM_MOD_ID from the USER_APP_MODULES table for the supplied UK */
  FUNCTION getModId_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN user_app_modules.uam_mod_id%TYPE;

 /* Returns UAM_ENABLED_IND from the USER_APP_MODULES table for the supplied PK */
  FUNCTION getEnabledInd_PK (p_pk IN user_app_modules.uam_id%TYPE) RETURN user_app_modules.uam_enabled_ind%TYPE;

 /* Returns UAM_ENABLED_IND from the USER_APP_MODULES table for the supplied UK */
  FUNCTION getEnabledInd_UK (p_uk IN user_app_modules.uam_usa_id%TYPE, p_uk2 IN user_app_modules.uam_mod_id%TYPE) RETURN user_app_modules.uam_enabled_ind%TYPE;

 /* Updates USER_APP_MODULES.UAM_USA_ID to the supplied value for the supplied PK */
  PROCEDURE setUsaId (p_pk IN user_app_modules.uam_id%TYPE, p_val IN user_app_modules.uam_usa_id%TYPE);

 /* Updates USER_APP_MODULES.UAM_MOD_ID to the supplied value for the supplied PK */
  PROCEDURE setModId (p_pk IN user_app_modules.uam_id%TYPE, p_val IN user_app_modules.uam_mod_id%TYPE);

 /* Updates USER_APP_MODULES.UAM_ENABLED_IND to the supplied value for the supplied PK */
  PROCEDURE setEnabledInd (p_pk IN user_app_modules.uam_id%TYPE, p_val IN user_app_modules.uam_enabled_ind%TYPE);

 /* Updates a row on the USER_APP_MODULES table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN user_app_modules.uam_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the USER_APP_MODULES table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN user_app_modules.uam_id%TYPE, 
                      p_usa_id IN user_app_modules.uam_usa_id%TYPE,
                      p_mod_id IN user_app_modules.uam_mod_id%TYPE,
                      p_enabled_ind IN user_app_modules.uam_enabled_ind%TYPE );

 /* Inserts a row into the USER_APP_MODULES table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN user_app_modules%ROWTYPE, p_newPk OUT user_app_modules.uam_id%TYPE);

 /* Inserts a row into the USER_APP_MODULES table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN user_app_modules.uam_id%TYPE,
                      p_usa_id IN user_app_modules.uam_usa_id%TYPE,
                      p_mod_id IN user_app_modules.uam_mod_id%TYPE,
                      p_enabled_ind IN user_app_modules.uam_enabled_ind%TYPE, p_newPk OUT user_app_modules.uam_id%TYPE);

 /* Deletes a row from the USER_APP_MODULES table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN user_app_modules.uam_id%TYPE);

END t_UserAppModules;
/
