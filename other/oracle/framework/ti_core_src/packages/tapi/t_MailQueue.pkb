CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_MailQueue IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN mail_queue.mlq_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN mail_queue.mlq_id%TYPE);

  PROCEDURE doInsert (p_row IN mail_queue%ROWTYPE, p_newPk OUT mail_queue.mlq_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN VARCHAR2 IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.mlq_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_id%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSubject_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_subject%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_subject;
  END getSubject_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getBody_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_body%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_body;
  END getBody_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTo_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_to%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_to;
  END getTo_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getFrom_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_from%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_from;
  END getFrom_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSent_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_sent%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_sent;
  END getSent_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTries_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_tries%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_tries;
  END getTries_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLastError_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_last_error%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_last_error;
  END getLastError_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_usr_id%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_usr_id;
  END getUsrId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getQueued_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_queued%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_queued;
  END getQueued_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLastErrorDate_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_last_error_date%TYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mlq_last_error_date;
  END getLastErrorDate_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setSubject (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_subject%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mlq_subject := a_data.gFalse;
    l_ignoreRow.mlq_body := a_data.gTrue;
    l_ignoreRow.mlq_to := a_data.gTrue;
    l_ignoreRow.mlq_from := a_data.gTrue;
    l_ignoreRow.mlq_sent := a_data.gTrue;
    l_ignoreRow.mlq_tries := a_data.gTrue;
    l_ignoreRow.mlq_last_error := a_data.gTrue;
    l_ignoreRow.mlq_usr_id := a_data.gTrue;
    l_ignoreRow.mlq_queued := a_data.gTrue;
    l_ignoreRow.mlq_last_error_date := a_data.gTrue;

    l_updateRow.mlq_subject := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setSubject;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setBody (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_body%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mlq_subject := a_data.gTrue;
    l_ignoreRow.mlq_body := a_data.gFalse;
    l_ignoreRow.mlq_to := a_data.gTrue;
    l_ignoreRow.mlq_from := a_data.gTrue;
    l_ignoreRow.mlq_sent := a_data.gTrue;
    l_ignoreRow.mlq_tries := a_data.gTrue;
    l_ignoreRow.mlq_last_error := a_data.gTrue;
    l_ignoreRow.mlq_usr_id := a_data.gTrue;
    l_ignoreRow.mlq_queued := a_data.gTrue;
    l_ignoreRow.mlq_last_error_date := a_data.gTrue;

    l_updateRow.mlq_body := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setBody;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTo (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_to%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mlq_subject := a_data.gTrue;
    l_ignoreRow.mlq_body := a_data.gTrue;
    l_ignoreRow.mlq_to := a_data.gFalse;
    l_ignoreRow.mlq_from := a_data.gTrue;
    l_ignoreRow.mlq_sent := a_data.gTrue;
    l_ignoreRow.mlq_tries := a_data.gTrue;
    l_ignoreRow.mlq_last_error := a_data.gTrue;
    l_ignoreRow.mlq_usr_id := a_data.gTrue;
    l_ignoreRow.mlq_queued := a_data.gTrue;
    l_ignoreRow.mlq_last_error_date := a_data.gTrue;

    l_updateRow.mlq_to := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTo;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setFrom (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_from%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mlq_subject := a_data.gTrue;
    l_ignoreRow.mlq_body := a_data.gTrue;
    l_ignoreRow.mlq_to := a_data.gTrue;
    l_ignoreRow.mlq_from := a_data.gFalse;
    l_ignoreRow.mlq_sent := a_data.gTrue;
    l_ignoreRow.mlq_tries := a_data.gTrue;
    l_ignoreRow.mlq_last_error := a_data.gTrue;
    l_ignoreRow.mlq_usr_id := a_data.gTrue;
    l_ignoreRow.mlq_queued := a_data.gTrue;
    l_ignoreRow.mlq_last_error_date := a_data.gTrue;

    l_updateRow.mlq_from := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setFrom;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setSent (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_sent%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mlq_subject := a_data.gTrue;
    l_ignoreRow.mlq_body := a_data.gTrue;
    l_ignoreRow.mlq_to := a_data.gTrue;
    l_ignoreRow.mlq_from := a_data.gTrue;
    l_ignoreRow.mlq_sent := a_data.gFalse;
    l_ignoreRow.mlq_tries := a_data.gTrue;
    l_ignoreRow.mlq_last_error := a_data.gTrue;
    l_ignoreRow.mlq_usr_id := a_data.gTrue;
    l_ignoreRow.mlq_queued := a_data.gTrue;
    l_ignoreRow.mlq_last_error_date := a_data.gTrue;

    l_updateRow.mlq_sent := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setSent;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTries (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_tries%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mlq_subject := a_data.gTrue;
    l_ignoreRow.mlq_body := a_data.gTrue;
    l_ignoreRow.mlq_to := a_data.gTrue;
    l_ignoreRow.mlq_from := a_data.gTrue;
    l_ignoreRow.mlq_sent := a_data.gTrue;
    l_ignoreRow.mlq_tries := a_data.gFalse;
    l_ignoreRow.mlq_last_error := a_data.gTrue;
    l_ignoreRow.mlq_usr_id := a_data.gTrue;
    l_ignoreRow.mlq_queued := a_data.gTrue;
    l_ignoreRow.mlq_last_error_date := a_data.gTrue;

    l_updateRow.mlq_tries := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTries;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLastError (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_last_error%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mlq_subject := a_data.gTrue;
    l_ignoreRow.mlq_body := a_data.gTrue;
    l_ignoreRow.mlq_to := a_data.gTrue;
    l_ignoreRow.mlq_from := a_data.gTrue;
    l_ignoreRow.mlq_sent := a_data.gTrue;
    l_ignoreRow.mlq_tries := a_data.gTrue;
    l_ignoreRow.mlq_last_error := a_data.gFalse;
    l_ignoreRow.mlq_usr_id := a_data.gTrue;
    l_ignoreRow.mlq_queued := a_data.gTrue;
    l_ignoreRow.mlq_last_error_date := a_data.gTrue;

    l_updateRow.mlq_last_error := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLastError;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUsrId (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_usr_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mlq_subject := a_data.gTrue;
    l_ignoreRow.mlq_body := a_data.gTrue;
    l_ignoreRow.mlq_to := a_data.gTrue;
    l_ignoreRow.mlq_from := a_data.gTrue;
    l_ignoreRow.mlq_sent := a_data.gTrue;
    l_ignoreRow.mlq_tries := a_data.gTrue;
    l_ignoreRow.mlq_last_error := a_data.gTrue;
    l_ignoreRow.mlq_usr_id := a_data.gFalse;
    l_ignoreRow.mlq_queued := a_data.gTrue;
    l_ignoreRow.mlq_last_error_date := a_data.gTrue;

    l_updateRow.mlq_usr_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsrId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setQueued (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_queued%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mlq_subject := a_data.gTrue;
    l_ignoreRow.mlq_body := a_data.gTrue;
    l_ignoreRow.mlq_to := a_data.gTrue;
    l_ignoreRow.mlq_from := a_data.gTrue;
    l_ignoreRow.mlq_sent := a_data.gTrue;
    l_ignoreRow.mlq_tries := a_data.gTrue;
    l_ignoreRow.mlq_last_error := a_data.gTrue;
    l_ignoreRow.mlq_usr_id := a_data.gTrue;
    l_ignoreRow.mlq_queued := a_data.gFalse;
    l_ignoreRow.mlq_last_error_date := a_data.gTrue;

    l_updateRow.mlq_queued := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setQueued;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLastErrorDate (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_last_error_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mlq_subject := a_data.gTrue;
    l_ignoreRow.mlq_body := a_data.gTrue;
    l_ignoreRow.mlq_to := a_data.gTrue;
    l_ignoreRow.mlq_from := a_data.gTrue;
    l_ignoreRow.mlq_sent := a_data.gTrue;
    l_ignoreRow.mlq_tries := a_data.gTrue;
    l_ignoreRow.mlq_last_error := a_data.gTrue;
    l_ignoreRow.mlq_usr_id := a_data.gTrue;
    l_ignoreRow.mlq_queued := a_data.gTrue;
    l_ignoreRow.mlq_last_error_date := a_data.gFalse;

    l_updateRow.mlq_last_error_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLastErrorDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN mail_queue.mlq_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN mail_queue.mlq_id%TYPE, 
                      p_subject IN mail_queue.mlq_subject%TYPE,
                      p_body IN mail_queue.mlq_body%TYPE,
                      p_to IN mail_queue.mlq_to%TYPE,
                      p_from IN mail_queue.mlq_from%TYPE,
                      p_sent IN mail_queue.mlq_sent%TYPE,
                      p_tries IN mail_queue.mlq_tries%TYPE,
                      p_last_error IN mail_queue.mlq_last_error%TYPE,
                      p_usr_id IN mail_queue.mlq_usr_id%TYPE,
                      p_queued IN mail_queue.mlq_queued%TYPE,
                      p_last_error_date IN mail_queue.mlq_last_error_date%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.mlq_subject := p_subject;
    l_row.mlq_body := p_body;
    l_row.mlq_to := p_to;
    l_row.mlq_from := p_from;
    l_row.mlq_sent := p_sent;
    l_row.mlq_tries := p_tries;
    l_row.mlq_last_error := p_last_error;
    l_row.mlq_usr_id := p_usr_id;
    l_row.mlq_queued := p_queued;
    l_row.mlq_last_error_date := p_last_error_date;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN mail_queue%ROWTYPE, p_newPK OUT mail_queue.mlq_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN mail_queue.mlq_id%TYPE,
                      p_subject IN mail_queue.mlq_subject%TYPE,
                      p_body IN mail_queue.mlq_body%TYPE,
                      p_to IN mail_queue.mlq_to%TYPE,
                      p_from IN mail_queue.mlq_from%TYPE,
                      p_sent IN mail_queue.mlq_sent%TYPE,
                      p_tries IN mail_queue.mlq_tries%TYPE,
                      p_last_error IN mail_queue.mlq_last_error%TYPE,
                      p_usr_id IN mail_queue.mlq_usr_id%TYPE,
                      p_queued IN mail_queue.mlq_queued%TYPE,
                      p_last_error_date IN mail_queue.mlq_last_error_date%TYPE, p_newPK OUT mail_queue.mlq_id%TYPE) IS
    l_row   mail_queue%ROWTYPE;
  BEGIN
    l_row.mlq_id := p_id;
    l_row.mlq_subject := p_subject;
    l_row.mlq_body := p_body;
    l_row.mlq_to := p_to;
    l_row.mlq_from := p_from;
    l_row.mlq_sent := p_sent;
    l_row.mlq_tries := p_tries;
    l_row.mlq_last_error := p_last_error;
    l_row.mlq_usr_id := p_usr_id;
    l_row.mlq_queued := p_queued;
    l_row.mlq_last_error_date := p_last_error_date;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN mail_queue.mlq_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue%ROWTYPE IS
    l_row    mail_queue%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM mail_queue
       WHERE mlq_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN mail_queue.mlq_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE mail_queue
         SET 
             mlq_subject = CASE NVL(p_ignore.mlq_subject, a_data.gFalse) WHEN a_data.gTrue THEN mlq_subject ELSE p_row.mlq_subject END,
             mlq_body = CASE NVL(p_ignore.mlq_body, a_data.gFalse) WHEN a_data.gTrue THEN mlq_body ELSE p_row.mlq_body END,
             mlq_to = CASE NVL(p_ignore.mlq_to, a_data.gFalse) WHEN a_data.gTrue THEN mlq_to ELSE p_row.mlq_to END,
             mlq_from = CASE NVL(p_ignore.mlq_from, a_data.gFalse) WHEN a_data.gTrue THEN mlq_from ELSE p_row.mlq_from END,
             mlq_sent = CASE NVL(p_ignore.mlq_sent, a_data.gFalse) WHEN a_data.gTrue THEN mlq_sent ELSE p_row.mlq_sent END,
             mlq_tries = CASE NVL(p_ignore.mlq_tries, a_data.gFalse) WHEN a_data.gTrue THEN mlq_tries ELSE p_row.mlq_tries END,
             mlq_last_error = CASE NVL(p_ignore.mlq_last_error, a_data.gFalse) WHEN a_data.gTrue THEN mlq_last_error ELSE p_row.mlq_last_error END,
             mlq_usr_id = CASE NVL(p_ignore.mlq_usr_id, a_data.gFalse) WHEN a_data.gTrue THEN mlq_usr_id ELSE p_row.mlq_usr_id END,
             mlq_queued = CASE NVL(p_ignore.mlq_queued, a_data.gFalse) WHEN a_data.gTrue THEN mlq_queued ELSE p_row.mlq_queued END,
             mlq_last_error_date = CASE NVL(p_ignore.mlq_last_error_date, a_data.gFalse) WHEN a_data.gTrue THEN mlq_last_error_date ELSE p_row.mlq_last_error_date END
         WHERE mlq_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN mail_queue.mlq_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE mail_queue
       WHERE mlq_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN mail_queue%ROWTYPE, p_newPk OUT mail_queue.mlq_id%TYPE) IS
    l_pk    mail_queue.mlq_id%TYPE;
  BEGIN 
    INSERT
      INTO mail_queue
    VALUES p_row
    RETURNING mlq_id
         INTO p_newPk;
  END doInsert;

END t_MailQueue;
/
