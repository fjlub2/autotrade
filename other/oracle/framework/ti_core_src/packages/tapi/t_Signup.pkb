CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_Signup IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN signup.sgn_id%TYPE) RETURN signup%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN signup.sgn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN signup.sgn_id%TYPE);

  PROCEDURE doInsert (p_row IN signup%ROWTYPE, p_newPk OUT signup.sgn_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN signup.sgn_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN signup.sgn_id%TYPE) RETURN VARCHAR2 IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.sgn_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_id%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCreatedDate_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_created_date%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_created_date;
  END getCreatedDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsername_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_username%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_username;
  END getUsername_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getFirstName_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_first_name%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_first_name;
  END getFirstName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLastName_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_last_name%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_last_name;
  END getLastName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEmail_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_email%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_email;
  END getEmail_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTermsAcceptInd_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_terms_accept_ind%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_terms_accept_ind;
  END getTermsAcceptInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTlnId_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_tln_id%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_tln_id;
  END getTlnId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getIp_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_ip%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_ip;
  END getIp_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSessionId_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_session_id%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_session_id;
  END getSessionId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getStatus_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_status%TYPE IS
    l_row    signup%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.sgn_status;
  END getStatus_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setCreatedDate (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_created_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sgn_created_date := a_data.gFalse;
    l_ignoreRow.sgn_username := a_data.gTrue;
    l_ignoreRow.sgn_first_name := a_data.gTrue;
    l_ignoreRow.sgn_last_name := a_data.gTrue;
    l_ignoreRow.sgn_email := a_data.gTrue;
    l_ignoreRow.sgn_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sgn_tln_id := a_data.gTrue;
    l_ignoreRow.sgn_ip := a_data.gTrue;
    l_ignoreRow.sgn_session_id := a_data.gTrue;
    l_ignoreRow.sgn_status := a_data.gTrue;

    l_updateRow.sgn_created_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCreatedDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUsername (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_username%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sgn_created_date := a_data.gTrue;
    l_ignoreRow.sgn_username := a_data.gFalse;
    l_ignoreRow.sgn_first_name := a_data.gTrue;
    l_ignoreRow.sgn_last_name := a_data.gTrue;
    l_ignoreRow.sgn_email := a_data.gTrue;
    l_ignoreRow.sgn_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sgn_tln_id := a_data.gTrue;
    l_ignoreRow.sgn_ip := a_data.gTrue;
    l_ignoreRow.sgn_session_id := a_data.gTrue;
    l_ignoreRow.sgn_status := a_data.gTrue;

    l_updateRow.sgn_username := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsername;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setFirstName (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_first_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sgn_created_date := a_data.gTrue;
    l_ignoreRow.sgn_username := a_data.gTrue;
    l_ignoreRow.sgn_first_name := a_data.gFalse;
    l_ignoreRow.sgn_last_name := a_data.gTrue;
    l_ignoreRow.sgn_email := a_data.gTrue;
    l_ignoreRow.sgn_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sgn_tln_id := a_data.gTrue;
    l_ignoreRow.sgn_ip := a_data.gTrue;
    l_ignoreRow.sgn_session_id := a_data.gTrue;
    l_ignoreRow.sgn_status := a_data.gTrue;

    l_updateRow.sgn_first_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setFirstName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLastName (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_last_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sgn_created_date := a_data.gTrue;
    l_ignoreRow.sgn_username := a_data.gTrue;
    l_ignoreRow.sgn_first_name := a_data.gTrue;
    l_ignoreRow.sgn_last_name := a_data.gFalse;
    l_ignoreRow.sgn_email := a_data.gTrue;
    l_ignoreRow.sgn_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sgn_tln_id := a_data.gTrue;
    l_ignoreRow.sgn_ip := a_data.gTrue;
    l_ignoreRow.sgn_session_id := a_data.gTrue;
    l_ignoreRow.sgn_status := a_data.gTrue;

    l_updateRow.sgn_last_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLastName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEmail (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_email%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sgn_created_date := a_data.gTrue;
    l_ignoreRow.sgn_username := a_data.gTrue;
    l_ignoreRow.sgn_first_name := a_data.gTrue;
    l_ignoreRow.sgn_last_name := a_data.gTrue;
    l_ignoreRow.sgn_email := a_data.gFalse;
    l_ignoreRow.sgn_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sgn_tln_id := a_data.gTrue;
    l_ignoreRow.sgn_ip := a_data.gTrue;
    l_ignoreRow.sgn_session_id := a_data.gTrue;
    l_ignoreRow.sgn_status := a_data.gTrue;

    l_updateRow.sgn_email := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEmail;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTermsAcceptInd (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_terms_accept_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sgn_created_date := a_data.gTrue;
    l_ignoreRow.sgn_username := a_data.gTrue;
    l_ignoreRow.sgn_first_name := a_data.gTrue;
    l_ignoreRow.sgn_last_name := a_data.gTrue;
    l_ignoreRow.sgn_email := a_data.gTrue;
    l_ignoreRow.sgn_terms_accept_ind := a_data.gFalse;
    l_ignoreRow.sgn_tln_id := a_data.gTrue;
    l_ignoreRow.sgn_ip := a_data.gTrue;
    l_ignoreRow.sgn_session_id := a_data.gTrue;
    l_ignoreRow.sgn_status := a_data.gTrue;

    l_updateRow.sgn_terms_accept_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTermsAcceptInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTlnId (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_tln_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sgn_created_date := a_data.gTrue;
    l_ignoreRow.sgn_username := a_data.gTrue;
    l_ignoreRow.sgn_first_name := a_data.gTrue;
    l_ignoreRow.sgn_last_name := a_data.gTrue;
    l_ignoreRow.sgn_email := a_data.gTrue;
    l_ignoreRow.sgn_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sgn_tln_id := a_data.gFalse;
    l_ignoreRow.sgn_ip := a_data.gTrue;
    l_ignoreRow.sgn_session_id := a_data.gTrue;
    l_ignoreRow.sgn_status := a_data.gTrue;

    l_updateRow.sgn_tln_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTlnId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setIp (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_ip%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sgn_created_date := a_data.gTrue;
    l_ignoreRow.sgn_username := a_data.gTrue;
    l_ignoreRow.sgn_first_name := a_data.gTrue;
    l_ignoreRow.sgn_last_name := a_data.gTrue;
    l_ignoreRow.sgn_email := a_data.gTrue;
    l_ignoreRow.sgn_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sgn_tln_id := a_data.gTrue;
    l_ignoreRow.sgn_ip := a_data.gFalse;
    l_ignoreRow.sgn_session_id := a_data.gTrue;
    l_ignoreRow.sgn_status := a_data.gTrue;

    l_updateRow.sgn_ip := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setIp;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setSessionId (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_session_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sgn_created_date := a_data.gTrue;
    l_ignoreRow.sgn_username := a_data.gTrue;
    l_ignoreRow.sgn_first_name := a_data.gTrue;
    l_ignoreRow.sgn_last_name := a_data.gTrue;
    l_ignoreRow.sgn_email := a_data.gTrue;
    l_ignoreRow.sgn_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sgn_tln_id := a_data.gTrue;
    l_ignoreRow.sgn_ip := a_data.gTrue;
    l_ignoreRow.sgn_session_id := a_data.gFalse;
    l_ignoreRow.sgn_status := a_data.gTrue;

    l_updateRow.sgn_session_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setSessionId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setStatus (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_status%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.sgn_created_date := a_data.gTrue;
    l_ignoreRow.sgn_username := a_data.gTrue;
    l_ignoreRow.sgn_first_name := a_data.gTrue;
    l_ignoreRow.sgn_last_name := a_data.gTrue;
    l_ignoreRow.sgn_email := a_data.gTrue;
    l_ignoreRow.sgn_terms_accept_ind := a_data.gTrue;
    l_ignoreRow.sgn_tln_id := a_data.gTrue;
    l_ignoreRow.sgn_ip := a_data.gTrue;
    l_ignoreRow.sgn_session_id := a_data.gTrue;
    l_ignoreRow.sgn_status := a_data.gFalse;

    l_updateRow.sgn_status := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setStatus;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN signup.sgn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN signup.sgn_id%TYPE, 
                      p_created_date IN signup.sgn_created_date%TYPE,
                      p_username IN signup.sgn_username%TYPE,
                      p_first_name IN signup.sgn_first_name%TYPE,
                      p_last_name IN signup.sgn_last_name%TYPE,
                      p_email IN signup.sgn_email%TYPE,
                      p_terms_accept_ind IN signup.sgn_terms_accept_ind%TYPE,
                      p_tln_id IN signup.sgn_tln_id%TYPE,
                      p_ip IN signup.sgn_ip%TYPE,
                      p_session_id IN signup.sgn_session_id%TYPE,
                      p_status IN signup.sgn_status%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.sgn_created_date := p_created_date;
    l_row.sgn_username := p_username;
    l_row.sgn_first_name := p_first_name;
    l_row.sgn_last_name := p_last_name;
    l_row.sgn_email := p_email;
    l_row.sgn_terms_accept_ind := p_terms_accept_ind;
    l_row.sgn_tln_id := p_tln_id;
    l_row.sgn_ip := p_ip;
    l_row.sgn_session_id := p_session_id;
    l_row.sgn_status := p_status;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN signup%ROWTYPE, p_newPK OUT signup.sgn_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN signup.sgn_id%TYPE,
                      p_created_date IN signup.sgn_created_date%TYPE,
                      p_username IN signup.sgn_username%TYPE,
                      p_first_name IN signup.sgn_first_name%TYPE,
                      p_last_name IN signup.sgn_last_name%TYPE,
                      p_email IN signup.sgn_email%TYPE,
                      p_terms_accept_ind IN signup.sgn_terms_accept_ind%TYPE,
                      p_tln_id IN signup.sgn_tln_id%TYPE,
                      p_ip IN signup.sgn_ip%TYPE,
                      p_session_id IN signup.sgn_session_id%TYPE,
                      p_status IN signup.sgn_status%TYPE, p_newPK OUT signup.sgn_id%TYPE) IS
    l_row   signup%ROWTYPE;
  BEGIN
    l_row.sgn_id := p_id;
    l_row.sgn_created_date := p_created_date;
    l_row.sgn_username := p_username;
    l_row.sgn_first_name := p_first_name;
    l_row.sgn_last_name := p_last_name;
    l_row.sgn_email := p_email;
    l_row.sgn_terms_accept_ind := p_terms_accept_ind;
    l_row.sgn_tln_id := p_tln_id;
    l_row.sgn_ip := p_ip;
    l_row.sgn_session_id := p_session_id;
    l_row.sgn_status := p_status;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN signup.sgn_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN signup.sgn_id%TYPE) RETURN signup%ROWTYPE IS
    l_row    signup%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM signup
       WHERE sgn_id = p_pk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN signup.sgn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE signup
         SET 
             sgn_created_date = CASE NVL(p_ignore.sgn_created_date, a_data.gFalse) WHEN a_data.gTrue THEN sgn_created_date ELSE p_row.sgn_created_date END,
             sgn_username = CASE NVL(p_ignore.sgn_username, a_data.gFalse) WHEN a_data.gTrue THEN sgn_username ELSE p_row.sgn_username END,
             sgn_first_name = CASE NVL(p_ignore.sgn_first_name, a_data.gFalse) WHEN a_data.gTrue THEN sgn_first_name ELSE p_row.sgn_first_name END,
             sgn_last_name = CASE NVL(p_ignore.sgn_last_name, a_data.gFalse) WHEN a_data.gTrue THEN sgn_last_name ELSE p_row.sgn_last_name END,
             sgn_email = CASE NVL(p_ignore.sgn_email, a_data.gFalse) WHEN a_data.gTrue THEN sgn_email ELSE p_row.sgn_email END,
             sgn_terms_accept_ind = CASE NVL(p_ignore.sgn_terms_accept_ind, a_data.gFalse) WHEN a_data.gTrue THEN sgn_terms_accept_ind ELSE p_row.sgn_terms_accept_ind END,
             sgn_tln_id = CASE NVL(p_ignore.sgn_tln_id, a_data.gFalse) WHEN a_data.gTrue THEN sgn_tln_id ELSE p_row.sgn_tln_id END,
             sgn_ip = CASE NVL(p_ignore.sgn_ip, a_data.gFalse) WHEN a_data.gTrue THEN sgn_ip ELSE p_row.sgn_ip END,
             sgn_session_id = CASE NVL(p_ignore.sgn_session_id, a_data.gFalse) WHEN a_data.gTrue THEN sgn_session_id ELSE p_row.sgn_session_id END,
             sgn_status = CASE NVL(p_ignore.sgn_status, a_data.gFalse) WHEN a_data.gTrue THEN sgn_status ELSE p_row.sgn_status END
         WHERE sgn_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN signup.sgn_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE signup
       WHERE sgn_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN signup%ROWTYPE, p_newPk OUT signup.sgn_id%TYPE) IS
    l_pk    signup.sgn_id%TYPE;
  BEGIN 
    INSERT
      INTO signup
    VALUES p_row
    RETURNING sgn_id
         INTO p_newPk;
  END doInsert;

END t_Signup;
/
