CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_Languages IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN languages.lng_id%TYPE, p_uk IN languages.lng_code%TYPE DEFAULT NULL) RETURN languages%ROWTYPE;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN languages.lng_id%TYPE) RETURN languages%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN languages.lng_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN languages.lng_id%TYPE) RETURN VARCHAR2 IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.lng_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN languages.lng_code%TYPE) RETURN languages%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN languages.lng_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN languages.lng_code%TYPE) RETURN VARCHAR2 IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.lng_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN languages.lng_id%TYPE) RETURN languages.lng_id%TYPE IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.lng_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN languages.lng_code%TYPE) RETURN languages.lng_id%TYPE IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.lng_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN languages.lng_id%TYPE) RETURN languages.lng_code%TYPE IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.lng_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_UK (p_uk IN languages.lng_code%TYPE) RETURN languages.lng_code%TYPE IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.lng_code;
  END getCode_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN languages.lng_id%TYPE) RETURN languages.lng_name%TYPE IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.lng_name;
  END getName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_UK (p_uk IN languages.lng_code%TYPE) RETURN languages.lng_name%TYPE IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.lng_name;
  END getName_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getNativeName_PK (p_pk IN languages.lng_id%TYPE) RETURN languages.lng_native_name%TYPE IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.lng_native_name;
  END getNativeName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getNativeName_UK (p_uk IN languages.lng_code%TYPE) RETURN languages.lng_native_name%TYPE IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.lng_native_name;
  END getNativeName_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_PK (p_pk IN languages.lng_id%TYPE) RETURN languages.lng_enabled_ind%TYPE IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.lng_enabled_ind;
  END getEnabledInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_UK (p_uk IN languages.lng_code%TYPE) RETURN languages.lng_enabled_ind%TYPE IS
    l_row    languages%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.lng_enabled_ind;
  END getEnabledInd_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN languages.lng_id%TYPE, p_uk IN languages.lng_code%TYPE DEFAULT NULL) RETURN languages%ROWTYPE IS
    l_row    languages%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM languages
       WHERE lng_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM languages
       WHERE lng_code = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

END t_Languages;
/
