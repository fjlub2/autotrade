CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_UserInfo IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for user_info table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF user_info%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              ui_usr_id   user_info.ui_usr_id%TYPE,
                              ui_uif_id   user_info.ui_uif_id%TYPE,
                              ui_uio_id   user_info.ui_uio_id%TYPE,
                              ui_small_text   user_info.ui_small_text%TYPE,
                              ui_large_text   user_info.ui_large_text%TYPE,
                              ui_date   user_info.ui_date%TYPE,
                              ui_number   user_info.ui_number%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              ui_usr_id   VARCHAR2(1),
                              ui_uif_id   VARCHAR2(1),
                              ui_uio_id   VARCHAR2(1),
                              ui_small_text   VARCHAR2(1),
                              ui_large_text   VARCHAR2(1),
                              ui_date   VARCHAR2(1),
                              ui_number   VARCHAR2(1));

 /* Returns a row from the USER_INFO table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info%ROWTYPE;

 /* Pipes a row from the USER_INFO table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN user_info.ui_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_INFO table for the supplied PK */
  FUNCTION exists_PK (p_pk IN user_info.ui_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the USER_INFO table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info%ROWTYPE;

 /* Pipes a row from the USER_INFO table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_INFO table for the supplied UK */
  FUNCTION exists_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN VARCHAR2;

 /* Returns UI_ID from the USER_INFO table for the supplied PK */
  FUNCTION getId_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_id%TYPE;

 /* Returns UI_ID from the USER_INFO table for the supplied UK */
  FUNCTION getId_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_id%TYPE;

 /* Returns UI_USR_ID from the USER_INFO table for the supplied PK */
  FUNCTION getUsrId_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_usr_id%TYPE;

 /* Returns UI_USR_ID from the USER_INFO table for the supplied UK */
  FUNCTION getUsrId_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_usr_id%TYPE;

 /* Returns UI_UIF_ID from the USER_INFO table for the supplied PK */
  FUNCTION getUifId_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_uif_id%TYPE;

 /* Returns UI_UIF_ID from the USER_INFO table for the supplied UK */
  FUNCTION getUifId_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_uif_id%TYPE;

 /* Returns UI_UIO_ID from the USER_INFO table for the supplied PK */
  FUNCTION getUioId_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_uio_id%TYPE;

 /* Returns UI_UIO_ID from the USER_INFO table for the supplied UK */
  FUNCTION getUioId_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_uio_id%TYPE;

 /* Returns UI_SMALL_TEXT from the USER_INFO table for the supplied PK */
  FUNCTION getSmallText_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_small_text%TYPE;

 /* Returns UI_SMALL_TEXT from the USER_INFO table for the supplied UK */
  FUNCTION getSmallText_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_small_text%TYPE;

 /* Returns UI_LARGE_TEXT from the USER_INFO table for the supplied PK */
  FUNCTION getLargeText_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_large_text%TYPE;

 /* Returns UI_LARGE_TEXT from the USER_INFO table for the supplied UK */
  FUNCTION getLargeText_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_large_text%TYPE;

 /* Returns UI_DATE from the USER_INFO table for the supplied PK */
  FUNCTION getDate_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_date%TYPE;

 /* Returns UI_DATE from the USER_INFO table for the supplied UK */
  FUNCTION getDate_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_date%TYPE;

 /* Returns UI_NUMBER from the USER_INFO table for the supplied PK */
  FUNCTION getNumber_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_number%TYPE;

 /* Returns UI_NUMBER from the USER_INFO table for the supplied UK */
  FUNCTION getNumber_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_number%TYPE;

 /* Updates USER_INFO.UI_USR_ID to the supplied value for the supplied PK */
  PROCEDURE setUsrId (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_usr_id%TYPE);

 /* Updates USER_INFO.UI_UIF_ID to the supplied value for the supplied PK */
  PROCEDURE setUifId (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_uif_id%TYPE);

 /* Updates USER_INFO.UI_UIO_ID to the supplied value for the supplied PK */
  PROCEDURE setUioId (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_uio_id%TYPE);

 /* Updates USER_INFO.UI_SMALL_TEXT to the supplied value for the supplied PK */
  PROCEDURE setSmallText (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_small_text%TYPE);

 /* Updates USER_INFO.UI_LARGE_TEXT to the supplied value for the supplied PK */
  PROCEDURE setLargeText (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_large_text%TYPE);

 /* Updates USER_INFO.UI_DATE to the supplied value for the supplied PK */
  PROCEDURE setDate (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_date%TYPE);

 /* Updates USER_INFO.UI_NUMBER to the supplied value for the supplied PK */
  PROCEDURE setNumber (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_number%TYPE);

 /* Updates a row on the USER_INFO table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN user_info.ui_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the USER_INFO table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN user_info.ui_id%TYPE, 
                      p_usr_id IN user_info.ui_usr_id%TYPE,
                      p_uif_id IN user_info.ui_uif_id%TYPE,
                      p_uio_id IN user_info.ui_uio_id%TYPE,
                      p_small_text IN user_info.ui_small_text%TYPE,
                      p_large_text IN user_info.ui_large_text%TYPE,
                      p_date IN user_info.ui_date%TYPE,
                      p_number IN user_info.ui_number%TYPE );

 /* Inserts a row into the USER_INFO table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN user_info%ROWTYPE, p_newPk OUT user_info.ui_id%TYPE);

 /* Inserts a row into the USER_INFO table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN user_info.ui_id%TYPE,
                      p_usr_id IN user_info.ui_usr_id%TYPE,
                      p_uif_id IN user_info.ui_uif_id%TYPE,
                      p_uio_id IN user_info.ui_uio_id%TYPE,
                      p_small_text IN user_info.ui_small_text%TYPE,
                      p_large_text IN user_info.ui_large_text%TYPE,
                      p_date IN user_info.ui_date%TYPE,
                      p_number IN user_info.ui_number%TYPE, p_newPk OUT user_info.ui_id%TYPE);

 /* Deletes a row from the USER_INFO table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN user_info.ui_id%TYPE);

END t_UserInfo;
/
