CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_MessageText IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN message_text.mst_id%TYPE, p_uk IN message_text.mst_msc_id%TYPE DEFAULT NULL, p_uk2 IN message_text.mst_lng_id%TYPE DEFAULT NULL) RETURN message_text%ROWTYPE;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN message_text.mst_id%TYPE) RETURN message_text%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN message_text.mst_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN message_text.mst_id%TYPE) RETURN VARCHAR2 IS
    l_row    message_text%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.mst_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN message_text%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk, p_uk2 => p_uk2);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk, p_uk2 => p_uk2));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN VARCHAR2 IS
    l_row    message_text%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    IF l_row.mst_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN message_text.mst_id%TYPE) RETURN message_text.mst_id%TYPE IS
    l_row    message_text%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mst_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN message_text.mst_id%TYPE IS
    l_row    message_text%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.mst_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMscId_PK (p_pk IN message_text.mst_id%TYPE) RETURN message_text.mst_msc_id%TYPE IS
    l_row    message_text%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mst_msc_id;
  END getMscId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMscId_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN message_text.mst_msc_id%TYPE IS
    l_row    message_text%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.mst_msc_id;
  END getMscId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLngId_PK (p_pk IN message_text.mst_id%TYPE) RETURN message_text.mst_lng_id%TYPE IS
    l_row    message_text%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mst_lng_id;
  END getLngId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLngId_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN message_text.mst_lng_id%TYPE IS
    l_row    message_text%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.mst_lng_id;
  END getLngId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getText_PK (p_pk IN message_text.mst_id%TYPE) RETURN message_text.mst_text%TYPE IS
    l_row    message_text%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mst_text;
  END getText_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getText_UK (p_uk IN message_text.mst_msc_id%TYPE, p_uk2 IN message_text.mst_lng_id%TYPE) RETURN message_text.mst_text%TYPE IS
    l_row    message_text%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.mst_text;
  END getText_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN message_text.mst_id%TYPE, p_uk IN message_text.mst_msc_id%TYPE DEFAULT NULL, p_uk2 IN message_text.mst_lng_id%TYPE DEFAULT NULL) RETURN message_text%ROWTYPE IS
    l_row    message_text%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM message_text
       WHERE mst_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM message_text
       WHERE mst_msc_id = p_uk
         AND ((p_uk2 IS NOT NULL AND
               mst_lng_id = p_uk2) OR
              (p_uk2 IS NULL AND
               mst_lng_id IS NULL));
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

END t_MessageText;
/
