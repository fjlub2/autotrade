CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_UserApps IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for user_apps table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF user_apps%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              usa_usr_id   user_apps.usa_usr_id%TYPE,
                              usa_app_id   user_apps.usa_app_id%TYPE,
                              usa_enabled_ind   user_apps.usa_enabled_ind%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              usa_usr_id   VARCHAR2(1),
                              usa_app_id   VARCHAR2(1),
                              usa_enabled_ind   VARCHAR2(1));

 /* Returns a row from the USER_APPS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN user_apps.usa_id%TYPE) RETURN user_apps%ROWTYPE;

 /* Pipes a row from the USER_APPS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN user_apps.usa_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_APPS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN user_apps.usa_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the USER_APPS table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN user_apps%ROWTYPE;

 /* Pipes a row from the USER_APPS table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_APPS table for the supplied UK */
  FUNCTION exists_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN VARCHAR2;

 /* Returns USA_ID from the USER_APPS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN user_apps.usa_id%TYPE) RETURN user_apps.usa_id%TYPE;

 /* Returns USA_ID from the USER_APPS table for the supplied UK */
  FUNCTION getId_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN user_apps.usa_id%TYPE;

 /* Returns USA_USR_ID from the USER_APPS table for the supplied PK */
  FUNCTION getUsrId_PK (p_pk IN user_apps.usa_id%TYPE) RETURN user_apps.usa_usr_id%TYPE;

 /* Returns USA_USR_ID from the USER_APPS table for the supplied UK */
  FUNCTION getUsrId_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN user_apps.usa_usr_id%TYPE;

 /* Returns USA_APP_ID from the USER_APPS table for the supplied PK */
  FUNCTION getAppId_PK (p_pk IN user_apps.usa_id%TYPE) RETURN user_apps.usa_app_id%TYPE;

 /* Returns USA_APP_ID from the USER_APPS table for the supplied UK */
  FUNCTION getAppId_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN user_apps.usa_app_id%TYPE;

 /* Returns USA_ENABLED_IND from the USER_APPS table for the supplied PK */
  FUNCTION getEnabledInd_PK (p_pk IN user_apps.usa_id%TYPE) RETURN user_apps.usa_enabled_ind%TYPE;

 /* Returns USA_ENABLED_IND from the USER_APPS table for the supplied UK */
  FUNCTION getEnabledInd_UK (p_uk IN user_apps.usa_usr_id%TYPE, p_uk2 IN user_apps.usa_app_id%TYPE) RETURN user_apps.usa_enabled_ind%TYPE;

 /* Updates USER_APPS.USA_USR_ID to the supplied value for the supplied PK */
  PROCEDURE setUsrId (p_pk IN user_apps.usa_id%TYPE, p_val IN user_apps.usa_usr_id%TYPE);

 /* Updates USER_APPS.USA_APP_ID to the supplied value for the supplied PK */
  PROCEDURE setAppId (p_pk IN user_apps.usa_id%TYPE, p_val IN user_apps.usa_app_id%TYPE);

 /* Updates USER_APPS.USA_ENABLED_IND to the supplied value for the supplied PK */
  PROCEDURE setEnabledInd (p_pk IN user_apps.usa_id%TYPE, p_val IN user_apps.usa_enabled_ind%TYPE);

 /* Updates a row on the USER_APPS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN user_apps.usa_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the USER_APPS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN user_apps.usa_id%TYPE, 
                      p_usr_id IN user_apps.usa_usr_id%TYPE,
                      p_app_id IN user_apps.usa_app_id%TYPE,
                      p_enabled_ind IN user_apps.usa_enabled_ind%TYPE );

 /* Inserts a row into the USER_APPS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN user_apps%ROWTYPE, p_newPk OUT user_apps.usa_id%TYPE);

 /* Inserts a row into the USER_APPS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN user_apps.usa_id%TYPE,
                      p_usr_id IN user_apps.usa_usr_id%TYPE,
                      p_app_id IN user_apps.usa_app_id%TYPE,
                      p_enabled_ind IN user_apps.usa_enabled_ind%TYPE, p_newPk OUT user_apps.usa_id%TYPE);

 /* Deletes a row from the USER_APPS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN user_apps.usa_id%TYPE);

END t_UserApps;
/
