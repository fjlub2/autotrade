CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_UserInfo IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_info.ui_id%TYPE, p_uk IN user_info.ui_usr_id%TYPE DEFAULT NULL, p_uk2 IN user_info.ui_uif_id%TYPE DEFAULT NULL) RETURN user_info%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN user_info.ui_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN user_info.ui_id%TYPE);

  PROCEDURE doInsert (p_row IN user_info%ROWTYPE, p_newPk OUT user_info.ui_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN user_info.ui_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN user_info.ui_id%TYPE) RETURN VARCHAR2 IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.ui_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk, p_uk2 => p_uk2);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk, p_uk2 => p_uk2));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN VARCHAR2 IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    IF l_row.ui_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_id%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ui_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_id%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.ui_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_usr_id%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ui_usr_id;
  END getUsrId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_usr_id%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.ui_usr_id;
  END getUsrId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUifId_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_uif_id%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ui_uif_id;
  END getUifId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUifId_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_uif_id%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.ui_uif_id;
  END getUifId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUioId_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_uio_id%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ui_uio_id;
  END getUioId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUioId_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_uio_id%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.ui_uio_id;
  END getUioId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSmallText_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_small_text%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ui_small_text;
  END getSmallText_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSmallText_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_small_text%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.ui_small_text;
  END getSmallText_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLargeText_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_large_text%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ui_large_text;
  END getLargeText_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLargeText_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_large_text%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.ui_large_text;
  END getLargeText_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDate_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_date%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ui_date;
  END getDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDate_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_date%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.ui_date;
  END getDate_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getNumber_PK (p_pk IN user_info.ui_id%TYPE) RETURN user_info.ui_number%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ui_number;
  END getNumber_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getNumber_UK (p_uk IN user_info.ui_usr_id%TYPE, p_uk2 IN user_info.ui_uif_id%TYPE) RETURN user_info.ui_number%TYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.ui_number;
  END getNumber_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setUsrId (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_usr_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ui_usr_id := a_data.gFalse;
    l_ignoreRow.ui_uif_id := a_data.gTrue;
    l_ignoreRow.ui_uio_id := a_data.gTrue;
    l_ignoreRow.ui_small_text := a_data.gTrue;
    l_ignoreRow.ui_large_text := a_data.gTrue;
    l_ignoreRow.ui_date := a_data.gTrue;
    l_ignoreRow.ui_number := a_data.gTrue;

    l_updateRow.ui_usr_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsrId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUifId (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_uif_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ui_usr_id := a_data.gTrue;
    l_ignoreRow.ui_uif_id := a_data.gFalse;
    l_ignoreRow.ui_uio_id := a_data.gTrue;
    l_ignoreRow.ui_small_text := a_data.gTrue;
    l_ignoreRow.ui_large_text := a_data.gTrue;
    l_ignoreRow.ui_date := a_data.gTrue;
    l_ignoreRow.ui_number := a_data.gTrue;

    l_updateRow.ui_uif_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUifId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUioId (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_uio_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ui_usr_id := a_data.gTrue;
    l_ignoreRow.ui_uif_id := a_data.gTrue;
    l_ignoreRow.ui_uio_id := a_data.gFalse;
    l_ignoreRow.ui_small_text := a_data.gTrue;
    l_ignoreRow.ui_large_text := a_data.gTrue;
    l_ignoreRow.ui_date := a_data.gTrue;
    l_ignoreRow.ui_number := a_data.gTrue;

    l_updateRow.ui_uio_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUioId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setSmallText (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_small_text%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ui_usr_id := a_data.gTrue;
    l_ignoreRow.ui_uif_id := a_data.gTrue;
    l_ignoreRow.ui_uio_id := a_data.gTrue;
    l_ignoreRow.ui_small_text := a_data.gFalse;
    l_ignoreRow.ui_large_text := a_data.gTrue;
    l_ignoreRow.ui_date := a_data.gTrue;
    l_ignoreRow.ui_number := a_data.gTrue;

    l_updateRow.ui_small_text := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setSmallText;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLargeText (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_large_text%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ui_usr_id := a_data.gTrue;
    l_ignoreRow.ui_uif_id := a_data.gTrue;
    l_ignoreRow.ui_uio_id := a_data.gTrue;
    l_ignoreRow.ui_small_text := a_data.gTrue;
    l_ignoreRow.ui_large_text := a_data.gFalse;
    l_ignoreRow.ui_date := a_data.gTrue;
    l_ignoreRow.ui_number := a_data.gTrue;

    l_updateRow.ui_large_text := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLargeText;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDate (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ui_usr_id := a_data.gTrue;
    l_ignoreRow.ui_uif_id := a_data.gTrue;
    l_ignoreRow.ui_uio_id := a_data.gTrue;
    l_ignoreRow.ui_small_text := a_data.gTrue;
    l_ignoreRow.ui_large_text := a_data.gTrue;
    l_ignoreRow.ui_date := a_data.gFalse;
    l_ignoreRow.ui_number := a_data.gTrue;

    l_updateRow.ui_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setNumber (p_pk IN user_info.ui_id%TYPE, p_val IN user_info.ui_number%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ui_usr_id := a_data.gTrue;
    l_ignoreRow.ui_uif_id := a_data.gTrue;
    l_ignoreRow.ui_uio_id := a_data.gTrue;
    l_ignoreRow.ui_small_text := a_data.gTrue;
    l_ignoreRow.ui_large_text := a_data.gTrue;
    l_ignoreRow.ui_date := a_data.gTrue;
    l_ignoreRow.ui_number := a_data.gFalse;

    l_updateRow.ui_number := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setNumber;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_info.ui_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_info.ui_id%TYPE, 
                      p_usr_id IN user_info.ui_usr_id%TYPE,
                      p_uif_id IN user_info.ui_uif_id%TYPE,
                      p_uio_id IN user_info.ui_uio_id%TYPE,
                      p_small_text IN user_info.ui_small_text%TYPE,
                      p_large_text IN user_info.ui_large_text%TYPE,
                      p_date IN user_info.ui_date%TYPE,
                      p_number IN user_info.ui_number%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.ui_usr_id := p_usr_id;
    l_row.ui_uif_id := p_uif_id;
    l_row.ui_uio_id := p_uio_id;
    l_row.ui_small_text := p_small_text;
    l_row.ui_large_text := p_large_text;
    l_row.ui_date := p_date;
    l_row.ui_number := p_number;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN user_info%ROWTYPE, p_newPK OUT user_info.ui_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN user_info.ui_id%TYPE,
                      p_usr_id IN user_info.ui_usr_id%TYPE,
                      p_uif_id IN user_info.ui_uif_id%TYPE,
                      p_uio_id IN user_info.ui_uio_id%TYPE,
                      p_small_text IN user_info.ui_small_text%TYPE,
                      p_large_text IN user_info.ui_large_text%TYPE,
                      p_date IN user_info.ui_date%TYPE,
                      p_number IN user_info.ui_number%TYPE, p_newPK OUT user_info.ui_id%TYPE) IS
    l_row   user_info%ROWTYPE;
  BEGIN
    l_row.ui_id := p_id;
    l_row.ui_usr_id := p_usr_id;
    l_row.ui_uif_id := p_uif_id;
    l_row.ui_uio_id := p_uio_id;
    l_row.ui_small_text := p_small_text;
    l_row.ui_large_text := p_large_text;
    l_row.ui_date := p_date;
    l_row.ui_number := p_number;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN user_info.ui_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_info.ui_id%TYPE, p_uk IN user_info.ui_usr_id%TYPE DEFAULT NULL, p_uk2 IN user_info.ui_uif_id%TYPE DEFAULT NULL) RETURN user_info%ROWTYPE IS
    l_row    user_info%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_info
       WHERE ui_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_info
       WHERE ui_usr_id = p_uk
         AND ((p_uk2 IS NOT NULL AND
               ui_uif_id = p_uk2) OR
              (p_uk2 IS NULL AND
               ui_uif_id IS NULL));
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN user_info.ui_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE user_info
         SET 
             ui_usr_id = CASE NVL(p_ignore.ui_usr_id, a_data.gFalse) WHEN a_data.gTrue THEN ui_usr_id ELSE p_row.ui_usr_id END,
             ui_uif_id = CASE NVL(p_ignore.ui_uif_id, a_data.gFalse) WHEN a_data.gTrue THEN ui_uif_id ELSE p_row.ui_uif_id END,
             ui_uio_id = CASE NVL(p_ignore.ui_uio_id, a_data.gFalse) WHEN a_data.gTrue THEN ui_uio_id ELSE p_row.ui_uio_id END,
             ui_small_text = CASE NVL(p_ignore.ui_small_text, a_data.gFalse) WHEN a_data.gTrue THEN ui_small_text ELSE p_row.ui_small_text END,
             ui_large_text = CASE NVL(p_ignore.ui_large_text, a_data.gFalse) WHEN a_data.gTrue THEN ui_large_text ELSE p_row.ui_large_text END,
             ui_date = CASE NVL(p_ignore.ui_date, a_data.gFalse) WHEN a_data.gTrue THEN ui_date ELSE p_row.ui_date END,
             ui_number = CASE NVL(p_ignore.ui_number, a_data.gFalse) WHEN a_data.gTrue THEN ui_number ELSE p_row.ui_number END
         WHERE ui_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN user_info.ui_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE user_info
       WHERE ui_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN user_info%ROWTYPE, p_newPk OUT user_info.ui_id%TYPE) IS
    l_pk    user_info.ui_id%TYPE;
  BEGIN 
    INSERT
      INTO user_info
    VALUES p_row
    RETURNING ui_id
         INTO p_newPk;
  END doInsert;

END t_UserInfo;
/
