CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_UserInfoOptions IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for user_info_options table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF user_info_options%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              uio_uif_id   user_info_options.uio_uif_id%TYPE,
                              uio_return   user_info_options.uio_return%TYPE,
                              uio_display   user_info_options.uio_display%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              uio_uif_id   VARCHAR2(1),
                              uio_return   VARCHAR2(1),
                              uio_display   VARCHAR2(1));

 /* Returns a row from the USER_INFO_OPTIONS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN user_info_options%ROWTYPE;

 /* Pipes a row from the USER_INFO_OPTIONS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_INFO_OPTIONS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the USER_INFO_OPTIONS table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN user_info_options%ROWTYPE;

 /* Pipes a row from the USER_INFO_OPTIONS table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_INFO_OPTIONS table for the supplied UK */
  FUNCTION exists_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN VARCHAR2;

 /* Returns UIO_ID from the USER_INFO_OPTIONS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN user_info_options.uio_id%TYPE;

 /* Returns UIO_ID from the USER_INFO_OPTIONS table for the supplied UK */
  FUNCTION getId_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN user_info_options.uio_id%TYPE;

 /* Returns UIO_UIF_ID from the USER_INFO_OPTIONS table for the supplied PK */
  FUNCTION getUifId_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN user_info_options.uio_uif_id%TYPE;

 /* Returns UIO_UIF_ID from the USER_INFO_OPTIONS table for the supplied UK */
  FUNCTION getUifId_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN user_info_options.uio_uif_id%TYPE;

 /* Returns UIO_RETURN from the USER_INFO_OPTIONS table for the supplied PK */
  FUNCTION getReturn_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN user_info_options.uio_return%TYPE;

 /* Returns UIO_RETURN from the USER_INFO_OPTIONS table for the supplied UK */
  FUNCTION getReturn_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN user_info_options.uio_return%TYPE;

 /* Returns UIO_DISPLAY from the USER_INFO_OPTIONS table for the supplied PK */
  FUNCTION getDisplay_PK (p_pk IN user_info_options.uio_id%TYPE) RETURN user_info_options.uio_display%TYPE;

 /* Returns UIO_DISPLAY from the USER_INFO_OPTIONS table for the supplied UK */
  FUNCTION getDisplay_UK (p_uk IN user_info_options.uio_uif_id%TYPE, p_uk2 IN user_info_options.uio_return%TYPE) RETURN user_info_options.uio_display%TYPE;

 /* Updates USER_INFO_OPTIONS.UIO_UIF_ID to the supplied value for the supplied PK */
  PROCEDURE setUifId (p_pk IN user_info_options.uio_id%TYPE, p_val IN user_info_options.uio_uif_id%TYPE);

 /* Updates USER_INFO_OPTIONS.UIO_RETURN to the supplied value for the supplied PK */
  PROCEDURE setReturn (p_pk IN user_info_options.uio_id%TYPE, p_val IN user_info_options.uio_return%TYPE);

 /* Updates USER_INFO_OPTIONS.UIO_DISPLAY to the supplied value for the supplied PK */
  PROCEDURE setDisplay (p_pk IN user_info_options.uio_id%TYPE, p_val IN user_info_options.uio_display%TYPE);

 /* Updates a row on the USER_INFO_OPTIONS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN user_info_options.uio_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the USER_INFO_OPTIONS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN user_info_options.uio_id%TYPE, 
                      p_uif_id IN user_info_options.uio_uif_id%TYPE,
                      p_return IN user_info_options.uio_return%TYPE,
                      p_display IN user_info_options.uio_display%TYPE );

 /* Inserts a row into the USER_INFO_OPTIONS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN user_info_options%ROWTYPE, p_newPk OUT user_info_options.uio_id%TYPE);

 /* Inserts a row into the USER_INFO_OPTIONS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN user_info_options.uio_id%TYPE,
                      p_uif_id IN user_info_options.uio_uif_id%TYPE,
                      p_return IN user_info_options.uio_return%TYPE,
                      p_display IN user_info_options.uio_display%TYPE, p_newPk OUT user_info_options.uio_id%TYPE);

 /* Deletes a row from the USER_INFO_OPTIONS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN user_info_options.uio_id%TYPE);

END t_UserInfoOptions;
/
