CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_MailQueue IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for mail_queue table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF mail_queue%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              mlq_subject   mail_queue.mlq_subject%TYPE,
                              mlq_body   mail_queue.mlq_body%TYPE,
                              mlq_to   mail_queue.mlq_to%TYPE,
                              mlq_from   mail_queue.mlq_from%TYPE,
                              mlq_sent   mail_queue.mlq_sent%TYPE,
                              mlq_tries   mail_queue.mlq_tries%TYPE,
                              mlq_last_error   mail_queue.mlq_last_error%TYPE,
                              mlq_usr_id   mail_queue.mlq_usr_id%TYPE,
                              mlq_queued   mail_queue.mlq_queued%TYPE,
                              mlq_last_error_date   mail_queue.mlq_last_error_date%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              mlq_subject   VARCHAR2(1),
                              mlq_body   VARCHAR2(1),
                              mlq_to   VARCHAR2(1),
                              mlq_from   VARCHAR2(1),
                              mlq_sent   VARCHAR2(1),
                              mlq_tries   VARCHAR2(1),
                              mlq_last_error   VARCHAR2(1),
                              mlq_usr_id   VARCHAR2(1),
                              mlq_queued   VARCHAR2(1),
                              mlq_last_error_date   VARCHAR2(1));

 /* Returns a row from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue%ROWTYPE;

 /* Pipes a row from the MAIL_QUEUE table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the MAIL_QUEUE table for the supplied PK */
  FUNCTION exists_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN VARCHAR2;

 /* Returns MLQ_ID from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getId_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_id%TYPE;

 /* Returns MLQ_SUBJECT from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getSubject_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_subject%TYPE;

 /* Returns MLQ_BODY from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getBody_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_body%TYPE;

 /* Returns MLQ_TO from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getTo_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_to%TYPE;

 /* Returns MLQ_FROM from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getFrom_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_from%TYPE;

 /* Returns MLQ_SENT from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getSent_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_sent%TYPE;

 /* Returns MLQ_TRIES from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getTries_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_tries%TYPE;

 /* Returns MLQ_LAST_ERROR from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getLastError_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_last_error%TYPE;

 /* Returns MLQ_USR_ID from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getUsrId_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_usr_id%TYPE;

 /* Returns MLQ_QUEUED from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getQueued_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_queued%TYPE;

 /* Returns MLQ_LAST_ERROR_DATE from the MAIL_QUEUE table for the supplied PK */
  FUNCTION getLastErrorDate_PK (p_pk IN mail_queue.mlq_id%TYPE) RETURN mail_queue.mlq_last_error_date%TYPE;

 /* Updates MAIL_QUEUE.MLQ_SUBJECT to the supplied value for the supplied PK */
  PROCEDURE setSubject (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_subject%TYPE);

 /* Updates MAIL_QUEUE.MLQ_BODY to the supplied value for the supplied PK */
  PROCEDURE setBody (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_body%TYPE);

 /* Updates MAIL_QUEUE.MLQ_TO to the supplied value for the supplied PK */
  PROCEDURE setTo (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_to%TYPE);

 /* Updates MAIL_QUEUE.MLQ_FROM to the supplied value for the supplied PK */
  PROCEDURE setFrom (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_from%TYPE);

 /* Updates MAIL_QUEUE.MLQ_SENT to the supplied value for the supplied PK */
  PROCEDURE setSent (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_sent%TYPE);

 /* Updates MAIL_QUEUE.MLQ_TRIES to the supplied value for the supplied PK */
  PROCEDURE setTries (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_tries%TYPE);

 /* Updates MAIL_QUEUE.MLQ_LAST_ERROR to the supplied value for the supplied PK */
  PROCEDURE setLastError (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_last_error%TYPE);

 /* Updates MAIL_QUEUE.MLQ_USR_ID to the supplied value for the supplied PK */
  PROCEDURE setUsrId (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_usr_id%TYPE);

 /* Updates MAIL_QUEUE.MLQ_QUEUED to the supplied value for the supplied PK */
  PROCEDURE setQueued (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_queued%TYPE);

 /* Updates MAIL_QUEUE.MLQ_LAST_ERROR_DATE to the supplied value for the supplied PK */
  PROCEDURE setLastErrorDate (p_pk IN mail_queue.mlq_id%TYPE, p_val IN mail_queue.mlq_last_error_date%TYPE);

 /* Updates a row on the MAIL_QUEUE table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN mail_queue.mlq_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the MAIL_QUEUE table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN mail_queue.mlq_id%TYPE, 
                      p_subject IN mail_queue.mlq_subject%TYPE,
                      p_body IN mail_queue.mlq_body%TYPE,
                      p_to IN mail_queue.mlq_to%TYPE,
                      p_from IN mail_queue.mlq_from%TYPE,
                      p_sent IN mail_queue.mlq_sent%TYPE,
                      p_tries IN mail_queue.mlq_tries%TYPE,
                      p_last_error IN mail_queue.mlq_last_error%TYPE,
                      p_usr_id IN mail_queue.mlq_usr_id%TYPE,
                      p_queued IN mail_queue.mlq_queued%TYPE,
                      p_last_error_date IN mail_queue.mlq_last_error_date%TYPE );

 /* Inserts a row into the MAIL_QUEUE table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN mail_queue%ROWTYPE, p_newPk OUT mail_queue.mlq_id%TYPE);

 /* Inserts a row into the MAIL_QUEUE table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN mail_queue.mlq_id%TYPE,
                      p_subject IN mail_queue.mlq_subject%TYPE,
                      p_body IN mail_queue.mlq_body%TYPE,
                      p_to IN mail_queue.mlq_to%TYPE,
                      p_from IN mail_queue.mlq_from%TYPE,
                      p_sent IN mail_queue.mlq_sent%TYPE,
                      p_tries IN mail_queue.mlq_tries%TYPE,
                      p_last_error IN mail_queue.mlq_last_error%TYPE,
                      p_usr_id IN mail_queue.mlq_usr_id%TYPE,
                      p_queued IN mail_queue.mlq_queued%TYPE,
                      p_last_error_date IN mail_queue.mlq_last_error_date%TYPE, p_newPk OUT mail_queue.mlq_id%TYPE);

 /* Deletes a row from the MAIL_QUEUE table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN mail_queue.mlq_id%TYPE);

END t_MailQueue;
/
