CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_UserInfoFields IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for user_info_fields table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF user_info_fields%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              uif_label   user_info_fields.uif_label%TYPE,
                              uif_mandatory_ind   user_info_fields.uif_mandatory_ind%TYPE,
                              uif_type   user_info_fields.uif_type%TYPE,
                              uif_app_id   user_info_fields.uif_app_id%TYPE,
                              uif_display_order   user_info_fields.uif_display_order%TYPE,
                              uif_code   user_info_fields.uif_code%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              uif_label   VARCHAR2(1),
                              uif_mandatory_ind   VARCHAR2(1),
                              uif_type   VARCHAR2(1),
                              uif_app_id   VARCHAR2(1),
                              uif_display_order   VARCHAR2(1),
                              uif_code   VARCHAR2(1));

 /* Returns a row from the USER_INFO_FIELDS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields%ROWTYPE;

 /* Pipes a row from the USER_INFO_FIELDS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_INFO_FIELDS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the USER_INFO_FIELDS table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields%ROWTYPE;

 /* Pipes a row from the USER_INFO_FIELDS table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the USER_INFO_FIELDS table for the supplied UK */
  FUNCTION exists_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN VARCHAR2;

 /* Returns UIF_ID from the USER_INFO_FIELDS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_id%TYPE;

 /* Returns UIF_ID from the USER_INFO_FIELDS table for the supplied UK */
  FUNCTION getId_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_id%TYPE;

 /* Returns UIF_LABEL from the USER_INFO_FIELDS table for the supplied PK */
  FUNCTION getLabel_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_label%TYPE;

 /* Returns UIF_LABEL from the USER_INFO_FIELDS table for the supplied UK */
  FUNCTION getLabel_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_label%TYPE;

 /* Returns UIF_MANDATORY_IND from the USER_INFO_FIELDS table for the supplied PK */
  FUNCTION getMandatoryInd_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_mandatory_ind%TYPE;

 /* Returns UIF_MANDATORY_IND from the USER_INFO_FIELDS table for the supplied UK */
  FUNCTION getMandatoryInd_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_mandatory_ind%TYPE;

 /* Returns UIF_TYPE from the USER_INFO_FIELDS table for the supplied PK */
  FUNCTION getType_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_type%TYPE;

 /* Returns UIF_TYPE from the USER_INFO_FIELDS table for the supplied UK */
  FUNCTION getType_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_type%TYPE;

 /* Returns UIF_APP_ID from the USER_INFO_FIELDS table for the supplied PK */
  FUNCTION getAppId_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_app_id%TYPE;

 /* Returns UIF_APP_ID from the USER_INFO_FIELDS table for the supplied UK */
  FUNCTION getAppId_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_app_id%TYPE;

 /* Returns UIF_DISPLAY_ORDER from the USER_INFO_FIELDS table for the supplied PK */
  FUNCTION getDisplayOrder_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_display_order%TYPE;

 /* Returns UIF_DISPLAY_ORDER from the USER_INFO_FIELDS table for the supplied UK */
  FUNCTION getDisplayOrder_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_display_order%TYPE;

 /* Returns UIF_CODE from the USER_INFO_FIELDS table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_code%TYPE;

 /* Returns UIF_CODE from the USER_INFO_FIELDS table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_code%TYPE;

 /* Updates USER_INFO_FIELDS.UIF_LABEL to the supplied value for the supplied PK */
  PROCEDURE setLabel (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_label%TYPE);

 /* Updates USER_INFO_FIELDS.UIF_MANDATORY_IND to the supplied value for the supplied PK */
  PROCEDURE setMandatoryInd (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_mandatory_ind%TYPE);

 /* Updates USER_INFO_FIELDS.UIF_TYPE to the supplied value for the supplied PK */
  PROCEDURE setType (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_type%TYPE);

 /* Updates USER_INFO_FIELDS.UIF_APP_ID to the supplied value for the supplied PK */
  PROCEDURE setAppId (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_app_id%TYPE);

 /* Updates USER_INFO_FIELDS.UIF_DISPLAY_ORDER to the supplied value for the supplied PK */
  PROCEDURE setDisplayOrder (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_display_order%TYPE);

 /* Updates USER_INFO_FIELDS.UIF_CODE to the supplied value for the supplied PK */
  PROCEDURE setCode (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_code%TYPE);

 /* Updates a row on the USER_INFO_FIELDS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN user_info_fields.uif_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the USER_INFO_FIELDS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN user_info_fields.uif_id%TYPE, 
                      p_label IN user_info_fields.uif_label%TYPE,
                      p_mandatory_ind IN user_info_fields.uif_mandatory_ind%TYPE,
                      p_type IN user_info_fields.uif_type%TYPE,
                      p_app_id IN user_info_fields.uif_app_id%TYPE,
                      p_display_order IN user_info_fields.uif_display_order%TYPE,
                      p_code IN user_info_fields.uif_code%TYPE );

 /* Inserts a row into the USER_INFO_FIELDS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN user_info_fields%ROWTYPE, p_newPk OUT user_info_fields.uif_id%TYPE);

 /* Inserts a row into the USER_INFO_FIELDS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN user_info_fields.uif_id%TYPE,
                      p_label IN user_info_fields.uif_label%TYPE,
                      p_mandatory_ind IN user_info_fields.uif_mandatory_ind%TYPE,
                      p_type IN user_info_fields.uif_type%TYPE,
                      p_app_id IN user_info_fields.uif_app_id%TYPE,
                      p_display_order IN user_info_fields.uif_display_order%TYPE,
                      p_code IN user_info_fields.uif_code%TYPE, p_newPk OUT user_info_fields.uif_id%TYPE);

 /* Deletes a row from the USER_INFO_FIELDS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN user_info_fields.uif_id%TYPE);

END t_UserInfoFields;
/
