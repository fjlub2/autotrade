CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_TempLinks IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for temp_links table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF temp_links%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              tln_usr_id   temp_links.tln_usr_id%TYPE,
                              tln_link_code   temp_links.tln_link_code%TYPE,
                              tln_expiry_date   temp_links.tln_expiry_date%TYPE,
                              tln_used_ind   temp_links.tln_used_ind%TYPE,
                              tln_used_date   temp_links.tln_used_date%TYPE,
                              tln_type   temp_links.tln_type%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              tln_usr_id   VARCHAR2(1),
                              tln_link_code   VARCHAR2(1),
                              tln_expiry_date   VARCHAR2(1),
                              tln_used_ind   VARCHAR2(1),
                              tln_used_date   VARCHAR2(1),
                              tln_type   VARCHAR2(1));

 /* Returns a row from the TEMP_LINKS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links%ROWTYPE;

 /* Pipes a row from the TEMP_LINKS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN temp_links.tln_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the TEMP_LINKS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN temp_links.tln_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the TEMP_LINKS table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links%ROWTYPE;

 /* Pipes a row from the TEMP_LINKS table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the TEMP_LINKS table for the supplied UK */
  FUNCTION exists_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN VARCHAR2;

 /* Returns TLN_ID from the TEMP_LINKS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_id%TYPE;

 /* Returns TLN_ID from the TEMP_LINKS table for the supplied UK */
  FUNCTION getId_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_id%TYPE;

 /* Returns TLN_USR_ID from the TEMP_LINKS table for the supplied PK */
  FUNCTION getUsrId_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_usr_id%TYPE;

 /* Returns TLN_USR_ID from the TEMP_LINKS table for the supplied UK */
  FUNCTION getUsrId_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_usr_id%TYPE;

 /* Returns TLN_LINK_CODE from the TEMP_LINKS table for the supplied PK */
  FUNCTION getLinkCode_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_link_code%TYPE;

 /* Returns TLN_LINK_CODE from the TEMP_LINKS table for the supplied UK */
  FUNCTION getLinkCode_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_link_code%TYPE;

 /* Returns TLN_EXPIRY_DATE from the TEMP_LINKS table for the supplied PK */
  FUNCTION getExpiryDate_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_expiry_date%TYPE;

 /* Returns TLN_EXPIRY_DATE from the TEMP_LINKS table for the supplied UK */
  FUNCTION getExpiryDate_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_expiry_date%TYPE;

 /* Returns TLN_USED_IND from the TEMP_LINKS table for the supplied PK */
  FUNCTION getUsedInd_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_used_ind%TYPE;

 /* Returns TLN_USED_IND from the TEMP_LINKS table for the supplied UK */
  FUNCTION getUsedInd_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_used_ind%TYPE;

 /* Returns TLN_USED_DATE from the TEMP_LINKS table for the supplied PK */
  FUNCTION getUsedDate_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_used_date%TYPE;

 /* Returns TLN_USED_DATE from the TEMP_LINKS table for the supplied UK */
  FUNCTION getUsedDate_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_used_date%TYPE;

 /* Returns TLN_TYPE from the TEMP_LINKS table for the supplied PK */
  FUNCTION getType_PK (p_pk IN temp_links.tln_id%TYPE) RETURN temp_links.tln_type%TYPE;

 /* Returns TLN_TYPE from the TEMP_LINKS table for the supplied UK */
  FUNCTION getType_UK (p_uk IN temp_links.tln_link_code%TYPE) RETURN temp_links.tln_type%TYPE;

 /* Updates TEMP_LINKS.TLN_USR_ID to the supplied value for the supplied PK */
  PROCEDURE setUsrId (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_usr_id%TYPE);

 /* Updates TEMP_LINKS.TLN_LINK_CODE to the supplied value for the supplied PK */
  PROCEDURE setLinkCode (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_link_code%TYPE);

 /* Updates TEMP_LINKS.TLN_EXPIRY_DATE to the supplied value for the supplied PK */
  PROCEDURE setExpiryDate (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_expiry_date%TYPE);

 /* Updates TEMP_LINKS.TLN_USED_IND to the supplied value for the supplied PK */
  PROCEDURE setUsedInd (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_used_ind%TYPE);

 /* Updates TEMP_LINKS.TLN_USED_DATE to the supplied value for the supplied PK */
  PROCEDURE setUsedDate (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_used_date%TYPE);

 /* Updates TEMP_LINKS.TLN_TYPE to the supplied value for the supplied PK */
  PROCEDURE setType (p_pk IN temp_links.tln_id%TYPE, p_val IN temp_links.tln_type%TYPE);

 /* Updates a row on the TEMP_LINKS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN temp_links.tln_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the TEMP_LINKS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN temp_links.tln_id%TYPE, 
                      p_usr_id IN temp_links.tln_usr_id%TYPE,
                      p_link_code IN temp_links.tln_link_code%TYPE,
                      p_expiry_date IN temp_links.tln_expiry_date%TYPE,
                      p_used_ind IN temp_links.tln_used_ind%TYPE,
                      p_used_date IN temp_links.tln_used_date%TYPE,
                      p_type IN temp_links.tln_type%TYPE );

 /* Inserts a row into the TEMP_LINKS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN temp_links%ROWTYPE, p_newPk OUT temp_links.tln_id%TYPE);

 /* Inserts a row into the TEMP_LINKS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN temp_links.tln_id%TYPE,
                      p_usr_id IN temp_links.tln_usr_id%TYPE,
                      p_link_code IN temp_links.tln_link_code%TYPE,
                      p_expiry_date IN temp_links.tln_expiry_date%TYPE,
                      p_used_ind IN temp_links.tln_used_ind%TYPE,
                      p_used_date IN temp_links.tln_used_date%TYPE,
                      p_type IN temp_links.tln_type%TYPE, p_newPk OUT temp_links.tln_id%TYPE);

 /* Deletes a row from the TEMP_LINKS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN temp_links.tln_id%TYPE);

END t_TempLinks;
/
