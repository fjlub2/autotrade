CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_MessageLog2 IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for message_log_2 table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF message_log_2%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              mss_uss_id   message_log_2.mss_uss_id%TYPE,
                              mss_date   message_log_2.mss_date%TYPE,
                              mss_type   message_log_2.mss_type%TYPE,
                              mss_message   message_log_2.mss_message%TYPE,
                              mss_stack   message_log_2.mss_stack%TYPE,
                              mss_trace   message_log_2.mss_trace%TYPE,
                              mss_task_id   message_log_2.mss_task_id%TYPE,
                              mss_data   message_log_2.mss_data%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              mss_uss_id   VARCHAR2(1),
                              mss_date   VARCHAR2(1),
                              mss_type   VARCHAR2(1),
                              mss_message   VARCHAR2(1),
                              mss_stack   VARCHAR2(1),
                              mss_trace   VARCHAR2(1),
                              mss_task_id   VARCHAR2(1),
                              mss_data   VARCHAR2(1));

 /* Returns a row from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2%ROWTYPE;

 /* Pipes a row from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION exists_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN VARCHAR2;

 /* Returns MSS_ID from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION getId_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_id%TYPE;

 /* Returns MSS_USS_ID from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION getUssId_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_uss_id%TYPE;

 /* Returns MSS_DATE from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION getDate_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_date%TYPE;

 /* Returns MSS_TYPE from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION getType_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_type%TYPE;

 /* Returns MSS_MESSAGE from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION getMessage_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_message%TYPE;

 /* Returns MSS_STACK from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION getStack_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_stack%TYPE;

 /* Returns MSS_TRACE from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION getTrace_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_trace%TYPE;

 /* Returns MSS_TASK_ID from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION getTaskId_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_task_id%TYPE;

 /* Returns MSS_DATA from the MESSAGE_LOG_2 table for the supplied PK */
  FUNCTION getData_PK (p_pk IN message_log_2.mss_id%TYPE) RETURN message_log_2.mss_data%TYPE;

 /* Updates MESSAGE_LOG_2.MSS_USS_ID to the supplied value for the supplied PK */
  PROCEDURE setUssId (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_uss_id%TYPE);

 /* Updates MESSAGE_LOG_2.MSS_DATE to the supplied value for the supplied PK */
  PROCEDURE setDate (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_date%TYPE);

 /* Updates MESSAGE_LOG_2.MSS_TYPE to the supplied value for the supplied PK */
  PROCEDURE setType (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_type%TYPE);

 /* Updates MESSAGE_LOG_2.MSS_MESSAGE to the supplied value for the supplied PK */
  PROCEDURE setMessage (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_message%TYPE);

 /* Updates MESSAGE_LOG_2.MSS_STACK to the supplied value for the supplied PK */
  PROCEDURE setStack (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_stack%TYPE);

 /* Updates MESSAGE_LOG_2.MSS_TRACE to the supplied value for the supplied PK */
  PROCEDURE setTrace (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_trace%TYPE);

 /* Updates MESSAGE_LOG_2.MSS_TASK_ID to the supplied value for the supplied PK */
  PROCEDURE setTaskId (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_task_id%TYPE);

 /* Updates MESSAGE_LOG_2.MSS_DATA to the supplied value for the supplied PK */
  PROCEDURE setData (p_pk IN message_log_2.mss_id%TYPE, p_val IN message_log_2.mss_data%TYPE);

 /* Updates a row on the MESSAGE_LOG_2 table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN message_log_2.mss_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the MESSAGE_LOG_2 table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN message_log_2.mss_id%TYPE, 
                      p_uss_id IN message_log_2.mss_uss_id%TYPE,
                      p_date IN message_log_2.mss_date%TYPE,
                      p_type IN message_log_2.mss_type%TYPE,
                      p_message IN message_log_2.mss_message%TYPE,
                      p_stack IN message_log_2.mss_stack%TYPE,
                      p_trace IN message_log_2.mss_trace%TYPE,
                      p_task_id IN message_log_2.mss_task_id%TYPE,
                      p_data IN message_log_2.mss_data%TYPE );

 /* Inserts a row into the MESSAGE_LOG_2 table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN message_log_2%ROWTYPE, p_newPk OUT message_log_2.mss_id%TYPE);

 /* Inserts a row into the MESSAGE_LOG_2 table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN message_log_2.mss_id%TYPE,
                      p_uss_id IN message_log_2.mss_uss_id%TYPE,
                      p_date IN message_log_2.mss_date%TYPE,
                      p_type IN message_log_2.mss_type%TYPE,
                      p_message IN message_log_2.mss_message%TYPE,
                      p_stack IN message_log_2.mss_stack%TYPE,
                      p_trace IN message_log_2.mss_trace%TYPE,
                      p_task_id IN message_log_2.mss_task_id%TYPE,
                      p_data IN message_log_2.mss_data%TYPE, p_newPk OUT message_log_2.mss_id%TYPE);

 /* Deletes a row from the MESSAGE_LOG_2 table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN message_log_2.mss_id%TYPE);

END t_MessageLog2;
/
