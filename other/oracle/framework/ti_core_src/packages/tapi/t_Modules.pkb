CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_Modules IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN modules.mod_id%TYPE, p_uk IN modules.mod_code%TYPE DEFAULT NULL) RETURN modules%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN modules.mod_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN modules.mod_id%TYPE);

  PROCEDURE doInsert (p_row IN modules%ROWTYPE, p_newPk OUT modules.mod_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN modules.mod_id%TYPE) RETURN modules%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN modules.mod_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN modules.mod_id%TYPE) RETURN VARCHAR2 IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.mod_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN modules.mod_code%TYPE) RETURN modules%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN modules.mod_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN modules.mod_code%TYPE) RETURN VARCHAR2 IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.mod_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN modules.mod_id%TYPE) RETURN modules.mod_id%TYPE IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mod_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN modules.mod_code%TYPE) RETURN modules.mod_id%TYPE IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.mod_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAppId_PK (p_pk IN modules.mod_id%TYPE) RETURN modules.mod_app_id%TYPE IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mod_app_id;
  END getAppId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAppId_UK (p_uk IN modules.mod_code%TYPE) RETURN modules.mod_app_id%TYPE IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.mod_app_id;
  END getAppId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN modules.mod_id%TYPE) RETURN modules.mod_name%TYPE IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mod_name;
  END getName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_UK (p_uk IN modules.mod_code%TYPE) RETURN modules.mod_name%TYPE IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.mod_name;
  END getName_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_PK (p_pk IN modules.mod_id%TYPE) RETURN modules.mod_enabled_ind%TYPE IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mod_enabled_ind;
  END getEnabledInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getEnabledInd_UK (p_uk IN modules.mod_code%TYPE) RETURN modules.mod_enabled_ind%TYPE IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.mod_enabled_ind;
  END getEnabledInd_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN modules.mod_id%TYPE) RETURN modules.mod_code%TYPE IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.mod_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_UK (p_uk IN modules.mod_code%TYPE) RETURN modules.mod_code%TYPE IS
    l_row    modules%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.mod_code;
  END getCode_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setAppId (p_pk IN modules.mod_id%TYPE, p_val IN modules.mod_app_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mod_app_id := a_data.gFalse;
    l_ignoreRow.mod_name := a_data.gTrue;
    l_ignoreRow.mod_enabled_ind := a_data.gTrue;
    l_ignoreRow.mod_code := a_data.gTrue;

    l_updateRow.mod_app_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setAppId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setName (p_pk IN modules.mod_id%TYPE, p_val IN modules.mod_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mod_app_id := a_data.gTrue;
    l_ignoreRow.mod_name := a_data.gFalse;
    l_ignoreRow.mod_enabled_ind := a_data.gTrue;
    l_ignoreRow.mod_code := a_data.gTrue;

    l_updateRow.mod_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setEnabledInd (p_pk IN modules.mod_id%TYPE, p_val IN modules.mod_enabled_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mod_app_id := a_data.gTrue;
    l_ignoreRow.mod_name := a_data.gTrue;
    l_ignoreRow.mod_enabled_ind := a_data.gFalse;
    l_ignoreRow.mod_code := a_data.gTrue;

    l_updateRow.mod_enabled_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setEnabledInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCode (p_pk IN modules.mod_id%TYPE, p_val IN modules.mod_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.mod_app_id := a_data.gTrue;
    l_ignoreRow.mod_name := a_data.gTrue;
    l_ignoreRow.mod_enabled_ind := a_data.gTrue;
    l_ignoreRow.mod_code := a_data.gFalse;

    l_updateRow.mod_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN modules.mod_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN modules.mod_id%TYPE, 
                      p_app_id IN modules.mod_app_id%TYPE,
                      p_name IN modules.mod_name%TYPE,
                      p_enabled_ind IN modules.mod_enabled_ind%TYPE,
                      p_code IN modules.mod_code%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.mod_app_id := p_app_id;
    l_row.mod_name := p_name;
    l_row.mod_enabled_ind := p_enabled_ind;
    l_row.mod_code := p_code;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN modules%ROWTYPE, p_newPK OUT modules.mod_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN modules.mod_id%TYPE,
                      p_app_id IN modules.mod_app_id%TYPE,
                      p_name IN modules.mod_name%TYPE,
                      p_enabled_ind IN modules.mod_enabled_ind%TYPE,
                      p_code IN modules.mod_code%TYPE, p_newPK OUT modules.mod_id%TYPE) IS
    l_row   modules%ROWTYPE;
  BEGIN
    l_row.mod_id := p_id;
    l_row.mod_app_id := p_app_id;
    l_row.mod_name := p_name;
    l_row.mod_enabled_ind := p_enabled_ind;
    l_row.mod_code := p_code;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN modules.mod_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN modules.mod_id%TYPE, p_uk IN modules.mod_code%TYPE DEFAULT NULL) RETURN modules%ROWTYPE IS
    l_row    modules%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM modules
       WHERE mod_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM modules
       WHERE mod_code = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN modules.mod_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE modules
         SET 
             mod_app_id = CASE NVL(p_ignore.mod_app_id, a_data.gFalse) WHEN a_data.gTrue THEN mod_app_id ELSE p_row.mod_app_id END,
             mod_name = CASE NVL(p_ignore.mod_name, a_data.gFalse) WHEN a_data.gTrue THEN mod_name ELSE p_row.mod_name END,
             mod_enabled_ind = CASE NVL(p_ignore.mod_enabled_ind, a_data.gFalse) WHEN a_data.gTrue THEN mod_enabled_ind ELSE p_row.mod_enabled_ind END,
             mod_code = CASE NVL(p_ignore.mod_code, a_data.gFalse) WHEN a_data.gTrue THEN mod_code ELSE p_row.mod_code END
         WHERE mod_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN modules.mod_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE modules
       WHERE mod_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN modules%ROWTYPE, p_newPk OUT modules.mod_id%TYPE) IS
    l_pk    modules.mod_id%TYPE;
  BEGIN 
    INSERT
      INTO modules
    VALUES p_row
    RETURNING mod_id
         INTO p_newPk;
  END doInsert;

END t_Modules;
/
