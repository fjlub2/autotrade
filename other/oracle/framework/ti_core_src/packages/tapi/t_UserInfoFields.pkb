CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_UserInfoFields IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_info_fields.uif_id%TYPE, p_uk IN user_info_fields.uif_code%TYPE DEFAULT NULL) RETURN user_info_fields%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN user_info_fields.uif_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN user_info_fields.uif_id%TYPE);

  PROCEDURE doInsert (p_row IN user_info_fields%ROWTYPE, p_newPk OUT user_info_fields.uif_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN VARCHAR2 IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.uif_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN VARCHAR2 IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.uif_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_id%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uif_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_id%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uif_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLabel_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_label%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uif_label;
  END getLabel_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLabel_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_label%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uif_label;
  END getLabel_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMandatoryInd_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_mandatory_ind%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uif_mandatory_ind;
  END getMandatoryInd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMandatoryInd_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_mandatory_ind%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uif_mandatory_ind;
  END getMandatoryInd_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getType_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_type%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uif_type;
  END getType_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getType_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_type%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uif_type;
  END getType_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAppId_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_app_id%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uif_app_id;
  END getAppId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getAppId_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_app_id%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uif_app_id;
  END getAppId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDisplayOrder_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_display_order%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uif_display_order;
  END getDisplayOrder_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDisplayOrder_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_display_order%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uif_display_order;
  END getDisplayOrder_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN user_info_fields.uif_id%TYPE) RETURN user_info_fields.uif_code%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uif_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_UK (p_uk IN user_info_fields.uif_code%TYPE) RETURN user_info_fields.uif_code%TYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uif_code;
  END getCode_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setLabel (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_label%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uif_label := a_data.gFalse;
    l_ignoreRow.uif_mandatory_ind := a_data.gTrue;
    l_ignoreRow.uif_type := a_data.gTrue;
    l_ignoreRow.uif_app_id := a_data.gTrue;
    l_ignoreRow.uif_display_order := a_data.gTrue;
    l_ignoreRow.uif_code := a_data.gTrue;

    l_updateRow.uif_label := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLabel;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setMandatoryInd (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_mandatory_ind%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uif_label := a_data.gTrue;
    l_ignoreRow.uif_mandatory_ind := a_data.gFalse;
    l_ignoreRow.uif_type := a_data.gTrue;
    l_ignoreRow.uif_app_id := a_data.gTrue;
    l_ignoreRow.uif_display_order := a_data.gTrue;
    l_ignoreRow.uif_code := a_data.gTrue;

    l_updateRow.uif_mandatory_ind := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setMandatoryInd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setType (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_type%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uif_label := a_data.gTrue;
    l_ignoreRow.uif_mandatory_ind := a_data.gTrue;
    l_ignoreRow.uif_type := a_data.gFalse;
    l_ignoreRow.uif_app_id := a_data.gTrue;
    l_ignoreRow.uif_display_order := a_data.gTrue;
    l_ignoreRow.uif_code := a_data.gTrue;

    l_updateRow.uif_type := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setType;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setAppId (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_app_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uif_label := a_data.gTrue;
    l_ignoreRow.uif_mandatory_ind := a_data.gTrue;
    l_ignoreRow.uif_type := a_data.gTrue;
    l_ignoreRow.uif_app_id := a_data.gFalse;
    l_ignoreRow.uif_display_order := a_data.gTrue;
    l_ignoreRow.uif_code := a_data.gTrue;

    l_updateRow.uif_app_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setAppId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDisplayOrder (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_display_order%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uif_label := a_data.gTrue;
    l_ignoreRow.uif_mandatory_ind := a_data.gTrue;
    l_ignoreRow.uif_type := a_data.gTrue;
    l_ignoreRow.uif_app_id := a_data.gTrue;
    l_ignoreRow.uif_display_order := a_data.gFalse;
    l_ignoreRow.uif_code := a_data.gTrue;

    l_updateRow.uif_display_order := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDisplayOrder;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCode (p_pk IN user_info_fields.uif_id%TYPE, p_val IN user_info_fields.uif_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uif_label := a_data.gTrue;
    l_ignoreRow.uif_mandatory_ind := a_data.gTrue;
    l_ignoreRow.uif_type := a_data.gTrue;
    l_ignoreRow.uif_app_id := a_data.gTrue;
    l_ignoreRow.uif_display_order := a_data.gTrue;
    l_ignoreRow.uif_code := a_data.gFalse;

    l_updateRow.uif_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_info_fields.uif_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_info_fields.uif_id%TYPE, 
                      p_label IN user_info_fields.uif_label%TYPE,
                      p_mandatory_ind IN user_info_fields.uif_mandatory_ind%TYPE,
                      p_type IN user_info_fields.uif_type%TYPE,
                      p_app_id IN user_info_fields.uif_app_id%TYPE,
                      p_display_order IN user_info_fields.uif_display_order%TYPE,
                      p_code IN user_info_fields.uif_code%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.uif_label := p_label;
    l_row.uif_mandatory_ind := p_mandatory_ind;
    l_row.uif_type := p_type;
    l_row.uif_app_id := p_app_id;
    l_row.uif_display_order := p_display_order;
    l_row.uif_code := p_code;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN user_info_fields%ROWTYPE, p_newPK OUT user_info_fields.uif_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN user_info_fields.uif_id%TYPE,
                      p_label IN user_info_fields.uif_label%TYPE,
                      p_mandatory_ind IN user_info_fields.uif_mandatory_ind%TYPE,
                      p_type IN user_info_fields.uif_type%TYPE,
                      p_app_id IN user_info_fields.uif_app_id%TYPE,
                      p_display_order IN user_info_fields.uif_display_order%TYPE,
                      p_code IN user_info_fields.uif_code%TYPE, p_newPK OUT user_info_fields.uif_id%TYPE) IS
    l_row   user_info_fields%ROWTYPE;
  BEGIN
    l_row.uif_id := p_id;
    l_row.uif_label := p_label;
    l_row.uif_mandatory_ind := p_mandatory_ind;
    l_row.uif_type := p_type;
    l_row.uif_app_id := p_app_id;
    l_row.uif_display_order := p_display_order;
    l_row.uif_code := p_code;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN user_info_fields.uif_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_info_fields.uif_id%TYPE, p_uk IN user_info_fields.uif_code%TYPE DEFAULT NULL) RETURN user_info_fields%ROWTYPE IS
    l_row    user_info_fields%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_info_fields
       WHERE uif_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_info_fields
       WHERE uif_code = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN user_info_fields.uif_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE user_info_fields
         SET 
             uif_label = CASE NVL(p_ignore.uif_label, a_data.gFalse) WHEN a_data.gTrue THEN uif_label ELSE p_row.uif_label END,
             uif_mandatory_ind = CASE NVL(p_ignore.uif_mandatory_ind, a_data.gFalse) WHEN a_data.gTrue THEN uif_mandatory_ind ELSE p_row.uif_mandatory_ind END,
             uif_type = CASE NVL(p_ignore.uif_type, a_data.gFalse) WHEN a_data.gTrue THEN uif_type ELSE p_row.uif_type END,
             uif_app_id = CASE NVL(p_ignore.uif_app_id, a_data.gFalse) WHEN a_data.gTrue THEN uif_app_id ELSE p_row.uif_app_id END,
             uif_display_order = CASE NVL(p_ignore.uif_display_order, a_data.gFalse) WHEN a_data.gTrue THEN uif_display_order ELSE p_row.uif_display_order END,
             uif_code = CASE NVL(p_ignore.uif_code, a_data.gFalse) WHEN a_data.gTrue THEN uif_code ELSE p_row.uif_code END
         WHERE uif_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN user_info_fields.uif_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE user_info_fields
       WHERE uif_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN user_info_fields%ROWTYPE, p_newPk OUT user_info_fields.uif_id%TYPE) IS
    l_pk    user_info_fields.uif_id%TYPE;
  BEGIN 
    INSERT
      INTO user_info_fields
    VALUES p_row
    RETURNING uif_id
         INTO p_newPk;
  END doInsert;

END t_UserInfoFields;
/
