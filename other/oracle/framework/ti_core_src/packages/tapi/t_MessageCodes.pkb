CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_MessageCodes IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN message_codes.msc_id%TYPE, p_uk IN message_codes.msc_code%TYPE DEFAULT NULL, p_uk2 IN message_codes.msc_constraint_name%TYPE DEFAULT NULL) RETURN message_codes%ROWTYPE;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN message_codes.msc_id%TYPE) RETURN message_codes%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN message_codes.msc_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN message_codes.msc_id%TYPE) RETURN VARCHAR2 IS
    l_row    message_codes%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.msc_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN message_codes%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk, p_uk2 => p_uk2);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk, p_uk2 => p_uk2));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN VARCHAR2 IS
    l_row    message_codes%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    IF l_row.msc_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN message_codes.msc_id%TYPE) RETURN message_codes.msc_id%TYPE IS
    l_row    message_codes%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.msc_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN message_codes.msc_id%TYPE IS
    l_row    message_codes%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.msc_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN message_codes.msc_id%TYPE) RETURN message_codes.msc_code%TYPE IS
    l_row    message_codes%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.msc_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN message_codes.msc_code%TYPE IS
    l_row    message_codes%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.msc_code;
  END getCode_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDefaultText_PK (p_pk IN message_codes.msc_id%TYPE) RETURN message_codes.msc_default_text%TYPE IS
    l_row    message_codes%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.msc_default_text;
  END getDefaultText_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDefaultText_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN message_codes.msc_default_text%TYPE IS
    l_row    message_codes%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.msc_default_text;
  END getDefaultText_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getConstraintName_PK (p_pk IN message_codes.msc_id%TYPE) RETURN message_codes.msc_constraint_name%TYPE IS
    l_row    message_codes%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.msc_constraint_name;
  END getConstraintName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getConstraintName_UK (p_uk IN message_codes.msc_code%TYPE, p_uk2 IN message_codes.msc_constraint_name%TYPE) RETURN message_codes.msc_constraint_name%TYPE IS
    l_row    message_codes%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.msc_constraint_name;
  END getConstraintName_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN message_codes.msc_id%TYPE, p_uk IN message_codes.msc_code%TYPE DEFAULT NULL, p_uk2 IN message_codes.msc_constraint_name%TYPE DEFAULT NULL) RETURN message_codes%ROWTYPE IS
    l_row    message_codes%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM message_codes
       WHERE msc_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM message_codes
       WHERE msc_code = p_uk
         AND ((p_uk2 IS NOT NULL AND
               msc_constraint_name = p_uk2) OR
              (p_uk2 IS NULL AND
               msc_constraint_name IS NULL));
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

END t_MessageCodes;
/
