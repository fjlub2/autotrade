CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_UserSessions IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_sessions.uss_id%TYPE, p_uk IN user_sessions.uss_code%TYPE DEFAULT NULL) RETURN user_sessions%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN user_sessions.uss_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN user_sessions.uss_id%TYPE);

  PROCEDURE doInsert (p_row IN user_sessions%ROWTYPE, p_newPk OUT user_sessions.uss_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN VARCHAR2 IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.uss_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN VARCHAR2 IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.uss_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_id%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uss_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_id%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uss_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_usr_id%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uss_usr_id;
  END getUsrId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUsrId_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_usr_id%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uss_usr_id;
  END getUsrId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSessionStart_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_session_start%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uss_session_start;
  END getSessionStart_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSessionStart_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_session_start%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uss_session_start;
  END getSessionStart_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSessionId_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_session_id%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uss_session_id;
  END getSessionId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSessionId_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_session_id%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uss_session_id;
  END getSessionId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUserIp_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_user_ip%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uss_user_ip;
  END getUserIp_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getUserIp_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_user_ip%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uss_user_ip;
  END getUserIp_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSessionEnd_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_session_end%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uss_session_end;
  END getSessionEnd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getSessionEnd_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_session_end%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uss_session_end;
  END getSessionEnd_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLngId_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_lng_id%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uss_lng_id;
  END getLngId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLngId_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_lng_id%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uss_lng_id;
  END getLngId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_code%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uss_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_code%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uss_code;
  END getCode_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLastUsed_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_last_used%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uss_last_used;
  END getLastUsed_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getLastUsed_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_last_used%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uss_last_used;
  END getLastUsed_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTimezoneOffset_PK (p_pk IN user_sessions.uss_id%TYPE) RETURN user_sessions.uss_timezone_offset%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.uss_timezone_offset;
  END getTimezoneOffset_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getTimezoneOffset_UK (p_uk IN user_sessions.uss_code%TYPE) RETURN user_sessions.uss_timezone_offset%TYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.uss_timezone_offset;
  END getTimezoneOffset_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setUsrId (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_usr_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uss_usr_id := a_data.gFalse;
    l_ignoreRow.uss_session_start := a_data.gTrue;
    l_ignoreRow.uss_session_id := a_data.gTrue;
    l_ignoreRow.uss_user_ip := a_data.gTrue;
    l_ignoreRow.uss_session_end := a_data.gTrue;
    l_ignoreRow.uss_lng_id := a_data.gTrue;
    l_ignoreRow.uss_code := a_data.gTrue;
    l_ignoreRow.uss_last_used := a_data.gTrue;
    l_ignoreRow.uss_timezone_offset := a_data.gTrue;

    l_updateRow.uss_usr_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUsrId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setSessionStart (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_session_start%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uss_usr_id := a_data.gTrue;
    l_ignoreRow.uss_session_start := a_data.gFalse;
    l_ignoreRow.uss_session_id := a_data.gTrue;
    l_ignoreRow.uss_user_ip := a_data.gTrue;
    l_ignoreRow.uss_session_end := a_data.gTrue;
    l_ignoreRow.uss_lng_id := a_data.gTrue;
    l_ignoreRow.uss_code := a_data.gTrue;
    l_ignoreRow.uss_last_used := a_data.gTrue;
    l_ignoreRow.uss_timezone_offset := a_data.gTrue;

    l_updateRow.uss_session_start := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setSessionStart;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setSessionId (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_session_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uss_usr_id := a_data.gTrue;
    l_ignoreRow.uss_session_start := a_data.gTrue;
    l_ignoreRow.uss_session_id := a_data.gFalse;
    l_ignoreRow.uss_user_ip := a_data.gTrue;
    l_ignoreRow.uss_session_end := a_data.gTrue;
    l_ignoreRow.uss_lng_id := a_data.gTrue;
    l_ignoreRow.uss_code := a_data.gTrue;
    l_ignoreRow.uss_last_used := a_data.gTrue;
    l_ignoreRow.uss_timezone_offset := a_data.gTrue;

    l_updateRow.uss_session_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setSessionId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setUserIp (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_user_ip%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uss_usr_id := a_data.gTrue;
    l_ignoreRow.uss_session_start := a_data.gTrue;
    l_ignoreRow.uss_session_id := a_data.gTrue;
    l_ignoreRow.uss_user_ip := a_data.gFalse;
    l_ignoreRow.uss_session_end := a_data.gTrue;
    l_ignoreRow.uss_lng_id := a_data.gTrue;
    l_ignoreRow.uss_code := a_data.gTrue;
    l_ignoreRow.uss_last_used := a_data.gTrue;
    l_ignoreRow.uss_timezone_offset := a_data.gTrue;

    l_updateRow.uss_user_ip := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setUserIp;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setSessionEnd (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_session_end%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uss_usr_id := a_data.gTrue;
    l_ignoreRow.uss_session_start := a_data.gTrue;
    l_ignoreRow.uss_session_id := a_data.gTrue;
    l_ignoreRow.uss_user_ip := a_data.gTrue;
    l_ignoreRow.uss_session_end := a_data.gFalse;
    l_ignoreRow.uss_lng_id := a_data.gTrue;
    l_ignoreRow.uss_code := a_data.gTrue;
    l_ignoreRow.uss_last_used := a_data.gTrue;
    l_ignoreRow.uss_timezone_offset := a_data.gTrue;

    l_updateRow.uss_session_end := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setSessionEnd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLngId (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_lng_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uss_usr_id := a_data.gTrue;
    l_ignoreRow.uss_session_start := a_data.gTrue;
    l_ignoreRow.uss_session_id := a_data.gTrue;
    l_ignoreRow.uss_user_ip := a_data.gTrue;
    l_ignoreRow.uss_session_end := a_data.gTrue;
    l_ignoreRow.uss_lng_id := a_data.gFalse;
    l_ignoreRow.uss_code := a_data.gTrue;
    l_ignoreRow.uss_last_used := a_data.gTrue;
    l_ignoreRow.uss_timezone_offset := a_data.gTrue;

    l_updateRow.uss_lng_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLngId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCode (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uss_usr_id := a_data.gTrue;
    l_ignoreRow.uss_session_start := a_data.gTrue;
    l_ignoreRow.uss_session_id := a_data.gTrue;
    l_ignoreRow.uss_user_ip := a_data.gTrue;
    l_ignoreRow.uss_session_end := a_data.gTrue;
    l_ignoreRow.uss_lng_id := a_data.gTrue;
    l_ignoreRow.uss_code := a_data.gFalse;
    l_ignoreRow.uss_last_used := a_data.gTrue;
    l_ignoreRow.uss_timezone_offset := a_data.gTrue;

    l_updateRow.uss_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setLastUsed (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_last_used%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uss_usr_id := a_data.gTrue;
    l_ignoreRow.uss_session_start := a_data.gTrue;
    l_ignoreRow.uss_session_id := a_data.gTrue;
    l_ignoreRow.uss_user_ip := a_data.gTrue;
    l_ignoreRow.uss_session_end := a_data.gTrue;
    l_ignoreRow.uss_lng_id := a_data.gTrue;
    l_ignoreRow.uss_code := a_data.gTrue;
    l_ignoreRow.uss_last_used := a_data.gFalse;
    l_ignoreRow.uss_timezone_offset := a_data.gTrue;

    l_updateRow.uss_last_used := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setLastUsed;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setTimezoneOffset (p_pk IN user_sessions.uss_id%TYPE, p_val IN user_sessions.uss_timezone_offset%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.uss_usr_id := a_data.gTrue;
    l_ignoreRow.uss_session_start := a_data.gTrue;
    l_ignoreRow.uss_session_id := a_data.gTrue;
    l_ignoreRow.uss_user_ip := a_data.gTrue;
    l_ignoreRow.uss_session_end := a_data.gTrue;
    l_ignoreRow.uss_lng_id := a_data.gTrue;
    l_ignoreRow.uss_code := a_data.gTrue;
    l_ignoreRow.uss_last_used := a_data.gTrue;
    l_ignoreRow.uss_timezone_offset := a_data.gFalse;

    l_updateRow.uss_timezone_offset := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setTimezoneOffset;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_sessions.uss_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN user_sessions.uss_id%TYPE, 
                      p_usr_id IN user_sessions.uss_usr_id%TYPE,
                      p_session_start IN user_sessions.uss_session_start%TYPE,
                      p_session_id IN user_sessions.uss_session_id%TYPE,
                      p_user_ip IN user_sessions.uss_user_ip%TYPE,
                      p_session_end IN user_sessions.uss_session_end%TYPE,
                      p_lng_id IN user_sessions.uss_lng_id%TYPE,
                      p_code IN user_sessions.uss_code%TYPE,
                      p_last_used IN user_sessions.uss_last_used%TYPE,
                      p_timezone_offset IN user_sessions.uss_timezone_offset%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.uss_usr_id := p_usr_id;
    l_row.uss_session_start := p_session_start;
    l_row.uss_session_id := p_session_id;
    l_row.uss_user_ip := p_user_ip;
    l_row.uss_session_end := p_session_end;
    l_row.uss_lng_id := p_lng_id;
    l_row.uss_code := p_code;
    l_row.uss_last_used := p_last_used;
    l_row.uss_timezone_offset := p_timezone_offset;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN user_sessions%ROWTYPE, p_newPK OUT user_sessions.uss_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN user_sessions.uss_id%TYPE,
                      p_usr_id IN user_sessions.uss_usr_id%TYPE,
                      p_session_start IN user_sessions.uss_session_start%TYPE,
                      p_session_id IN user_sessions.uss_session_id%TYPE,
                      p_user_ip IN user_sessions.uss_user_ip%TYPE,
                      p_session_end IN user_sessions.uss_session_end%TYPE,
                      p_lng_id IN user_sessions.uss_lng_id%TYPE,
                      p_code IN user_sessions.uss_code%TYPE,
                      p_last_used IN user_sessions.uss_last_used%TYPE,
                      p_timezone_offset IN user_sessions.uss_timezone_offset%TYPE, p_newPK OUT user_sessions.uss_id%TYPE) IS
    l_row   user_sessions%ROWTYPE;
  BEGIN
    l_row.uss_id := p_id;
    l_row.uss_usr_id := p_usr_id;
    l_row.uss_session_start := p_session_start;
    l_row.uss_session_id := p_session_id;
    l_row.uss_user_ip := p_user_ip;
    l_row.uss_session_end := p_session_end;
    l_row.uss_lng_id := p_lng_id;
    l_row.uss_code := p_code;
    l_row.uss_last_used := p_last_used;
    l_row.uss_timezone_offset := p_timezone_offset;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN user_sessions.uss_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN user_sessions.uss_id%TYPE, p_uk IN user_sessions.uss_code%TYPE DEFAULT NULL) RETURN user_sessions%ROWTYPE IS
    l_row    user_sessions%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_sessions
       WHERE uss_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM user_sessions
       WHERE uss_code = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN user_sessions.uss_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE user_sessions
         SET 
             uss_usr_id = CASE NVL(p_ignore.uss_usr_id, a_data.gFalse) WHEN a_data.gTrue THEN uss_usr_id ELSE p_row.uss_usr_id END,
             uss_session_start = CASE NVL(p_ignore.uss_session_start, a_data.gFalse) WHEN a_data.gTrue THEN uss_session_start ELSE p_row.uss_session_start END,
             uss_session_id = CASE NVL(p_ignore.uss_session_id, a_data.gFalse) WHEN a_data.gTrue THEN uss_session_id ELSE p_row.uss_session_id END,
             uss_user_ip = CASE NVL(p_ignore.uss_user_ip, a_data.gFalse) WHEN a_data.gTrue THEN uss_user_ip ELSE p_row.uss_user_ip END,
             uss_session_end = CASE NVL(p_ignore.uss_session_end, a_data.gFalse) WHEN a_data.gTrue THEN uss_session_end ELSE p_row.uss_session_end END,
             uss_lng_id = CASE NVL(p_ignore.uss_lng_id, a_data.gFalse) WHEN a_data.gTrue THEN uss_lng_id ELSE p_row.uss_lng_id END,
             uss_code = CASE NVL(p_ignore.uss_code, a_data.gFalse) WHEN a_data.gTrue THEN uss_code ELSE p_row.uss_code END,
             uss_last_used = CASE NVL(p_ignore.uss_last_used, a_data.gFalse) WHEN a_data.gTrue THEN uss_last_used ELSE p_row.uss_last_used END,
             uss_timezone_offset = CASE NVL(p_ignore.uss_timezone_offset, a_data.gFalse) WHEN a_data.gTrue THEN uss_timezone_offset ELSE p_row.uss_timezone_offset END
         WHERE uss_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN user_sessions.uss_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE user_sessions
       WHERE uss_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN user_sessions%ROWTYPE, p_newPk OUT user_sessions.uss_id%TYPE) IS
    l_pk    user_sessions.uss_id%TYPE;
  BEGIN 
    INSERT
      INTO user_sessions
    VALUES p_row
    RETURNING uss_id
         INTO p_newPk;
  END doInsert;

END t_UserSessions;
/
