CREATE OR REPLACE PACKAGE TI_CORE_SRC.t_Signup IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for signup table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF signup%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              sgn_created_date   signup.sgn_created_date%TYPE,
                              sgn_username   signup.sgn_username%TYPE,
                              sgn_first_name   signup.sgn_first_name%TYPE,
                              sgn_last_name   signup.sgn_last_name%TYPE,
                              sgn_email   signup.sgn_email%TYPE,
                              sgn_terms_accept_ind   signup.sgn_terms_accept_ind%TYPE,
                              sgn_tln_id   signup.sgn_tln_id%TYPE,
                              sgn_ip   signup.sgn_ip%TYPE,
                              sgn_session_id   signup.sgn_session_id%TYPE,
                              sgn_status   signup.sgn_status%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              sgn_created_date   VARCHAR2(1),
                              sgn_username   VARCHAR2(1),
                              sgn_first_name   VARCHAR2(1),
                              sgn_last_name   VARCHAR2(1),
                              sgn_email   VARCHAR2(1),
                              sgn_terms_accept_ind   VARCHAR2(1),
                              sgn_tln_id   VARCHAR2(1),
                              sgn_ip   VARCHAR2(1),
                              sgn_session_id   VARCHAR2(1),
                              sgn_status   VARCHAR2(1));

 /* Returns a row from the SIGNUP table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup%ROWTYPE;

 /* Pipes a row from the SIGNUP table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN signup.sgn_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the SIGNUP table for the supplied PK */
  FUNCTION exists_PK (p_pk IN signup.sgn_id%TYPE) RETURN VARCHAR2;

 /* Returns SGN_ID from the SIGNUP table for the supplied PK */
  FUNCTION getId_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_id%TYPE;

 /* Returns SGN_CREATED_DATE from the SIGNUP table for the supplied PK */
  FUNCTION getCreatedDate_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_created_date%TYPE;

 /* Returns SGN_USERNAME from the SIGNUP table for the supplied PK */
  FUNCTION getUsername_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_username%TYPE;

 /* Returns SGN_FIRST_NAME from the SIGNUP table for the supplied PK */
  FUNCTION getFirstName_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_first_name%TYPE;

 /* Returns SGN_LAST_NAME from the SIGNUP table for the supplied PK */
  FUNCTION getLastName_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_last_name%TYPE;

 /* Returns SGN_EMAIL from the SIGNUP table for the supplied PK */
  FUNCTION getEmail_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_email%TYPE;

 /* Returns SGN_TERMS_ACCEPT_IND from the SIGNUP table for the supplied PK */
  FUNCTION getTermsAcceptInd_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_terms_accept_ind%TYPE;

 /* Returns SGN_TLN_ID from the SIGNUP table for the supplied PK */
  FUNCTION getTlnId_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_tln_id%TYPE;

 /* Returns SGN_IP from the SIGNUP table for the supplied PK */
  FUNCTION getIp_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_ip%TYPE;

 /* Returns SGN_SESSION_ID from the SIGNUP table for the supplied PK */
  FUNCTION getSessionId_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_session_id%TYPE;

 /* Returns SGN_STATUS from the SIGNUP table for the supplied PK */
  FUNCTION getStatus_PK (p_pk IN signup.sgn_id%TYPE) RETURN signup.sgn_status%TYPE;

 /* Updates SIGNUP.SGN_CREATED_DATE to the supplied value for the supplied PK */
  PROCEDURE setCreatedDate (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_created_date%TYPE);

 /* Updates SIGNUP.SGN_USERNAME to the supplied value for the supplied PK */
  PROCEDURE setUsername (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_username%TYPE);

 /* Updates SIGNUP.SGN_FIRST_NAME to the supplied value for the supplied PK */
  PROCEDURE setFirstName (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_first_name%TYPE);

 /* Updates SIGNUP.SGN_LAST_NAME to the supplied value for the supplied PK */
  PROCEDURE setLastName (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_last_name%TYPE);

 /* Updates SIGNUP.SGN_EMAIL to the supplied value for the supplied PK */
  PROCEDURE setEmail (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_email%TYPE);

 /* Updates SIGNUP.SGN_TERMS_ACCEPT_IND to the supplied value for the supplied PK */
  PROCEDURE setTermsAcceptInd (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_terms_accept_ind%TYPE);

 /* Updates SIGNUP.SGN_TLN_ID to the supplied value for the supplied PK */
  PROCEDURE setTlnId (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_tln_id%TYPE);

 /* Updates SIGNUP.SGN_IP to the supplied value for the supplied PK */
  PROCEDURE setIp (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_ip%TYPE);

 /* Updates SIGNUP.SGN_SESSION_ID to the supplied value for the supplied PK */
  PROCEDURE setSessionId (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_session_id%TYPE);

 /* Updates SIGNUP.SGN_STATUS to the supplied value for the supplied PK */
  PROCEDURE setStatus (p_pk IN signup.sgn_id%TYPE, p_val IN signup.sgn_status%TYPE);

 /* Updates a row on the SIGNUP table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN signup.sgn_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the SIGNUP table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN signup.sgn_id%TYPE, 
                      p_created_date IN signup.sgn_created_date%TYPE,
                      p_username IN signup.sgn_username%TYPE,
                      p_first_name IN signup.sgn_first_name%TYPE,
                      p_last_name IN signup.sgn_last_name%TYPE,
                      p_email IN signup.sgn_email%TYPE,
                      p_terms_accept_ind IN signup.sgn_terms_accept_ind%TYPE,
                      p_tln_id IN signup.sgn_tln_id%TYPE,
                      p_ip IN signup.sgn_ip%TYPE,
                      p_session_id IN signup.sgn_session_id%TYPE,
                      p_status IN signup.sgn_status%TYPE );

 /* Inserts a row into the SIGNUP table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN signup%ROWTYPE, p_newPk OUT signup.sgn_id%TYPE);

 /* Inserts a row into the SIGNUP table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN signup.sgn_id%TYPE,
                      p_created_date IN signup.sgn_created_date%TYPE,
                      p_username IN signup.sgn_username%TYPE,
                      p_first_name IN signup.sgn_first_name%TYPE,
                      p_last_name IN signup.sgn_last_name%TYPE,
                      p_email IN signup.sgn_email%TYPE,
                      p_terms_accept_ind IN signup.sgn_terms_accept_ind%TYPE,
                      p_tln_id IN signup.sgn_tln_id%TYPE,
                      p_ip IN signup.sgn_ip%TYPE,
                      p_session_id IN signup.sgn_session_id%TYPE,
                      p_status IN signup.sgn_status%TYPE, p_newPk OUT signup.sgn_id%TYPE);

 /* Deletes a row from the SIGNUP table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN signup.sgn_id%TYPE);

END t_Signup;
/
