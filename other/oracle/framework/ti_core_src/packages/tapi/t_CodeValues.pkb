CREATE OR REPLACE PACKAGE BODY TI_CORE_SRC.t_CodeValues IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE);

  PROCEDURE doInsert (p_row IN code_values%ROWTYPE, p_newPk OUT code_values.cv_domain%TYPE, p_newPk2 OUT code_values.cv_code%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk, p_pk2 => p_pk2);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk, p_pk2 => p_pk2));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN VARCHAR2 IS
    l_row    code_values%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk, p_pk2 => p_pk2);

    IF l_row.cv_domain IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDomain_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values.cv_domain%TYPE IS
    l_row    code_values%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk, p_pk2 => p_pk2);

    RETURN l_row.cv_domain;
  END getDomain_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values.cv_code%TYPE IS
    l_row    code_values%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk, p_pk2 => p_pk2);

    RETURN l_row.cv_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getValue_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values.cv_value%TYPE IS
    l_row    code_values%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk, p_pk2 => p_pk2);

    RETURN l_row.cv_value;
  END getValue_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values.cv_name%TYPE IS
    l_row    code_values%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk, p_pk2 => p_pk2);

    RETURN l_row.cv_name;
  END getName_PK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setValue (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE, p_val IN code_values.cv_value%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.cv_value := a_data.gFalse;
    l_ignoreRow.cv_name := a_data.gTrue;

    l_updateRow.cv_value := p_val;

    doUpdate(p_pk => p_pk, p_pk2 => p_pk2, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setValue;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setName (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE, p_val IN code_values.cv_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.cv_value := a_data.gTrue;
    l_ignoreRow.cv_name := a_data.gFalse;

    l_updateRow.cv_name := p_val;

    doUpdate(p_pk => p_pk, p_pk2 => p_pk2, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_pk2 => p_pk2, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE, 
                      p_value IN code_values.cv_value%TYPE,
                      p_name IN code_values.cv_name%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.cv_value := p_value;
    l_row.cv_name := p_name;

    doUpdate(p_pk => p_pk, p_pk2 => p_pk2, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN code_values%ROWTYPE, p_newPK OUT code_values.cv_domain%TYPE, p_newPk2 OUT code_values.cv_code%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk, p_newPk2 => p_newPk2);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_domain IN code_values.cv_domain%TYPE,
                      p_code IN code_values.cv_code%TYPE,
                      p_value IN code_values.cv_value%TYPE,
                      p_name IN code_values.cv_name%TYPE, p_newPK OUT code_values.cv_domain%TYPE, p_newPk2 OUT code_values.cv_code%TYPE) IS
    l_row   code_values%ROWTYPE;
  BEGIN
    l_row.cv_domain := p_domain;
    l_row.cv_code := p_code;
    l_row.cv_value := p_value;
    l_row.cv_name := p_name;

    doInsert(p_row => l_row, p_newPk => p_newPk, p_newPk2 => p_newPk2);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk, p_pk2 => p_pk2);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) RETURN code_values%ROWTYPE IS
    l_row    code_values%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM code_values
       WHERE cv_domain = p_pk AND cv_code = p_pk2;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE code_values
         SET 
             cv_value = CASE NVL(p_ignore.cv_value, a_data.gFalse) WHEN a_data.gTrue THEN cv_value ELSE p_row.cv_value END,
             cv_name = CASE NVL(p_ignore.cv_name, a_data.gFalse) WHEN a_data.gTrue THEN cv_name ELSE p_row.cv_name END
         WHERE cv_domain = p_pk AND cv_domain = p_pk2;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN code_values.cv_domain%TYPE, p_pk2 IN code_values.cv_code%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE code_values
       WHERE cv_domain = p_pk AND cv_domain = p_pk2;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN code_values%ROWTYPE, p_newPk OUT code_values.cv_domain%TYPE, p_newPk2 OUT code_values.cv_code%TYPE) IS
    l_pk    code_values.cv_domain%TYPE;
  BEGIN 
    INSERT
      INTO code_values
    VALUES p_row
    RETURNING cv_domain, cv_code
         INTO p_newPk, p_newPk2;
  END doInsert;

END t_CodeValues;
/
