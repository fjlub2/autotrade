-- CORE Data Grants
GRANT UNLIMITED TABLESPACE TO TI_CORE_DAT;
GRANT CREATE TABLE TO TI_CORE_DAT; -- needed for MVs
-------------------------------------------------------------------------------


-- CORE Source Grants ---------------------------------------------------------
GRANT EXECUTE ON DBMS_CRYPTO TO TI_CORE_SRC;
GRANT EXECUTE ON UTL_SMTP    TO TI_CORE_SRC;
GRANT EXECUTE ON UTL_TCP     TO TI_CORE_SRC;
-------------------------------------------------------------------------------


-- CORE API Grants    ---------------------------------------------------------

-------------------------------------------------------------------------------


-- CORE Management Grants -----------------------------------------------------

GRANT CREATE SESSION TO TI_CORE_MAN;
-------------------------------------------------------------------------------


-- Required by VPD in Oracle XE database --------------------------------------
GRANT EXECUTE ON TI_CORE_SRC.C_VPD TO TI_CORE_DAT WITH GRANT OPTION;
-------------------------------------------------------------------------------


--Row Level Security Stuff ----------------------------------------------------
GRANT CREATE ANY CONTEXT TO TI_CORE_SRC;
CREATE OR REPLACE CONTEXT TI_SES_CTX USING TI_CORE_SRC.C_SESSION;
CREATE OR REPLACE CONTEXT TI_SEC_CTX USING TI_CORE_SRC.C_SECURITY;

DECLARE
  l_alias VARCHAR2(10);
  
  PROCEDURE dropPolicy (pi_name  IN VARCHAR2,
                        pi_alias IN VARCHAR2) IS
    e_policy_not_there EXCEPTION;
    PRAGMA EXCEPTION_INIT(e_policy_not_there, -28102);
  BEGIN
    DBMS_RLS.DROP_POLICY(object_schema   => 'TI_CORE_DAT',
                         object_name     => pi_name,
                         policy_name     => 'TI_'||pi_alias||'_POL');
  EXCEPTION
    WHEN e_policy_not_there THEN
      NULL;
  END dropPolicy;
  
  PROCEDURE addPolicy (pi_name  IN VARCHAR2,
                       pi_alias IN VARCHAR2,
                       pi_func  IN VARCHAR2) IS
  BEGIN
    dropPolicy(pi_name  => pi_name,
               pi_alias => pi_alias);
    
    DBMS_RLS.ADD_POLICY(object_schema   => 'TI_CORE_DAT',
                        object_name     => pi_name,
                        policy_name     => 'TI_'||pi_alias||'_POL',
                        function_schema => 'TI_CORE_SRC',
                        policy_function => pi_func,
                        statement_types => 'SELECT,INSERT,UPDATE,DELETE');
  END addPolicy;
BEGIN
  addPolicy(pi_name => 'USERS',            pi_alias => 'USR', pi_func => 'c_VPD.userVPD');
  --addPolicy(pi_name => 'MAIL_QUEUE',       pi_alias => 'MLQ', pi_func => 'c_VPD.userVPD');
  addPolicy(pi_name => 'TEMP_LINKS',       pi_alias => 'TLN', pi_func => 'c_VPD.userVPD');
  addPolicy(pi_name => 'USER_APP_MODULES', pi_alias => 'UAM', pi_func => 'c_VPD.userVPD');
  addPolicy(pi_name => 'USER_APPS',        pi_alias => 'USA', pi_func => 'c_VPD.userVPD');
  addPolicy(pi_name => 'USER_INFO',        pi_alias => 'UI',  pi_func => 'c_VPD.userVPD');
  addPolicy(pi_name => 'USER_SESSIONS',    pi_alias => 'USS', pi_func => 'c_VPD.userVPD');

END;
/
-------------------------------------------------------------------------------


BEGIN
  BEGIN
    DBMS_NETWORK_ACL_ADMIN.DROP_ACL('email_acl.xml');
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;
  
  -- Create ACL
  DBMS_NETWORK_ACL_ADMIN.CREATE_ACL (acl         => 'email_acl.xml',
                                     description => 'Allow mail to be sent',
                                     principal   => 'TI_CORE_SRC',
                                     is_grant    => TRUE,
                                     privilege   => 'connect');
  
  -- Add Privilege

  DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE (acl       => 'email_acl.xml',
                                        principal => 'TI_CORE_SRC',
                                        is_grant  => TRUE,
                                        privilege => 'resolve');
                                    
  -- Assign ACL
  DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL(acl  => 'email_acl.xml',
                                    host => 'smtp.gmail.com',
                                    lower_port => 587,
                                    upper_port => 587);

  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    RAISE;
END;
/