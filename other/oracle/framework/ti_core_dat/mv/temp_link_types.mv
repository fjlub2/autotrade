DROP MATERIALIZED VIEW ti_core_dat.temp_link_types_mv;

CREATE MATERIALIZED VIEW ti_core_dat.temp_link_types_mv
  BUILD IMMEDIATE
  REFRESH FAST
  ON COMMIT
  AS (SELECT cv_code,
             cv_value,
             cv_name,
             cv_domain
        FROM code_values
       WHERE cv_domain = 'temp_link_types');

COMMENT ON MATERIALIZED VIEW ti_core_dat.temp_link_types_MV IS 'MV table for config_values - data sourced from the code_values table. Constrain against the UK if needed';

COMMENT ON COLUMN ti_core_dat.temp_link_types_MV.CV_CODE IS 'Code';
COMMENT ON COLUMN ti_core_dat.temp_link_types_MV.CV_VALUE IS 'Value';
COMMENT ON COLUMN ti_core_dat.temp_link_types_MV.CV_NAME IS 'Name';
COMMENT ON COLUMN ti_core_dat.temp_link_types_MV.CV_DOMAIN IS 'Domain';

ALTER TABLE ti_core_dat.temp_link_types_MV ADD CONSTRAINT temp_link_types_UK UNIQUE (CV_CODE);

CREATE OR REPLACE FORCE VIEW ti_core_dat.temp_link_types_vw
(
   CV_CODE,
   CV_VALUE,
   CV_NAME
)
AS
   SELECT
          cv_code,
          cv_value,
          cv_name
     FROM temp_link_types_mv;

COMMENT ON TABLE ti_core_dat.temp_link_types_vw IS 'View of config_values - data sourced from the code_values table via a MV';

COMMENT ON COLUMN ti_core_dat.temp_link_types_vw.CV_CODE IS 'Primary Key';
COMMENT ON COLUMN ti_core_dat.temp_link_types_vw.CV_VALUE IS 'Value';
COMMENT ON COLUMN ti_core_dat.temp_link_types_vw.CV_NAME IS 'Name';
