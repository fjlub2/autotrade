CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.user_apps_vw
(
   usa_id,
   usa_usr_id,
   usa_app_id,
   usa_enabled_ind
)
AS
   SELECT
          usa_id,
          usa_usr_id,
          usa_app_id,
          usa_enabled_ind
     FROM user_apps;

COMMENT ON TABLE TI_CORE_DAT.user_apps_vw IS 'List of Apps the user is allowed to access';

COMMENT ON COLUMN TI_CORE_DAT.user_apps_vw.usa_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.user_apps_vw.usa_usr_id IS 'User Id. FK to USERS';
COMMENT ON COLUMN TI_CORE_DAT.user_apps_vw.usa_app_id IS 'Application Id. FK to APPS';
COMMENT ON COLUMN TI_CORE_DAT.user_apps_vw.usa_enabled_ind IS 'Set to N to temporarily disable users access.';
