CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.user_info_options_vw
(
   uio_id,
   uio_uif_id,
   uio_return,
   uio_display
)
AS
   SELECT
          uio_id,
          uio_uif_id,
          uio_return,
          uio_display
     FROM user_info_options;

COMMENT ON TABLE TI_CORE_DAT.user_info_options_vw IS 'User Info Field Options';

COMMENT ON COLUMN TI_CORE_DAT.user_info_options_vw.uio_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.user_info_options_vw.uio_uif_id IS 'User Info Field Id. FK to USER_INFO_FIELDS';
COMMENT ON COLUMN TI_CORE_DAT.user_info_options_vw.uio_return IS 'Return Value';
COMMENT ON COLUMN TI_CORE_DAT.user_info_options_vw.uio_display IS 'Display Value';
