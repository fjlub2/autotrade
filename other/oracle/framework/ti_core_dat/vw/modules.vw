CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.modules_vw
(
   mod_id,
   mod_app_id,
   mod_name,
   mod_enabled_ind,
   mod_code
)
AS
   SELECT
          mod_id,
          mod_app_id,
          mod_name,
          mod_enabled_ind,
          mod_code
     FROM modules;

COMMENT ON TABLE TI_CORE_DAT.modules_vw IS 'Table to list all modules available within an app';

COMMENT ON COLUMN TI_CORE_DAT.modules_vw.mod_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.modules_vw.mod_app_id IS 'Application ID. FK to APPS';
COMMENT ON COLUMN TI_CORE_DAT.modules_vw.mod_name IS 'Module Name';
COMMENT ON COLUMN TI_CORE_DAT.modules_vw.mod_enabled_ind IS 'Is Module Enabled? Y/N';
COMMENT ON COLUMN TI_CORE_DAT.modules_vw.mod_code IS 'Module Code';
