CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.code_values_vw
(
   cv_domain,
   cv_code,
   cv_value,
   cv_name
)
AS
   SELECT
          cv_domain,
          cv_code,
          cv_value,
          cv_name
     FROM code_values;

COMMENT ON TABLE TI_CORE_DAT.code_values_vw IS 'Generic Codes table. Use ddlUtils.createDomainMV to create a constrainable MV for each domain';

COMMENT ON COLUMN TI_CORE_DAT.code_values_vw.cv_domain IS 'Domain Name';
COMMENT ON COLUMN TI_CORE_DAT.code_values_vw.cv_code IS 'Code';
COMMENT ON COLUMN TI_CORE_DAT.code_values_vw.cv_value IS 'Value';
COMMENT ON COLUMN TI_CORE_DAT.code_values_vw.cv_name IS 'Name';
