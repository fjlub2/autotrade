CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.temp_links_vw
(
   tln_id,
   tln_usr_id,
   tln_link_code,
   tln_expiry_date,
   tln_used_ind,
   tln_used_date,
   tln_type
)
AS
   SELECT
          tln_id,
          tln_usr_id,
          tln_link_code,
          tln_expiry_date,
          tln_used_ind,
          tln_used_date,
          tln_type
     FROM temp_links;

COMMENT ON TABLE TI_CORE_DAT.temp_links_vw IS 'Table to hold temporary links. E.g. for password resets via email';

COMMENT ON COLUMN TI_CORE_DAT.temp_links_vw.tln_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.temp_links_vw.tln_usr_id IS 'User that the link relates to';
COMMENT ON COLUMN TI_CORE_DAT.temp_links_vw.tln_link_code IS 'Code that is passed in the url of the link. Unique.';
COMMENT ON COLUMN TI_CORE_DAT.temp_links_vw.tln_expiry_date IS 'When does the link expire';
COMMENT ON COLUMN TI_CORE_DAT.temp_links_vw.tln_used_ind IS 'Has the link been successfully used. It can only be used once';
COMMENT ON COLUMN TI_CORE_DAT.temp_links_vw.tln_used_date IS 'When was the link successfully used';
COMMENT ON COLUMN TI_CORE_DAT.temp_links_vw.tln_type IS 'Type of link, e.g. password reset';
