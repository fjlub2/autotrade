CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.user_info_vw
(
   ui_id,
   ui_usr_id,
   ui_uif_id,
   ui_uio_id,
   ui_small_text,
   ui_large_text,
   ui_date,
   ui_number
)
AS
   SELECT
          ui_id,
          ui_usr_id,
          ui_uif_id,
          ui_uio_id,
          ui_small_text,
          ui_large_text,
          ui_date,
          ui_number
     FROM user_info;

COMMENT ON TABLE TI_CORE_DAT.user_info_vw IS 'User Attributes';

COMMENT ON COLUMN TI_CORE_DAT.user_info_vw.ui_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.user_info_vw.ui_usr_id IS 'User Id. FK to USERS';
COMMENT ON COLUMN TI_CORE_DAT.user_info_vw.ui_uif_id IS 'User Info Field ID. FK to USER_INFO_FIELDS';
COMMENT ON COLUMN TI_CORE_DAT.user_info_vw.ui_uio_id IS 'Value for selected option. FK to USER_INFO_OPTIONS';
COMMENT ON COLUMN TI_CORE_DAT.user_info_vw.ui_small_text IS 'Value for single line text fields.';
COMMENT ON COLUMN TI_CORE_DAT.user_info_vw.ui_large_text IS 'Value for text areas';
COMMENT ON COLUMN TI_CORE_DAT.user_info_vw.ui_date IS 'Value for data or date time fields';
COMMENT ON COLUMN TI_CORE_DAT.user_info_vw.ui_number IS 'Value for numeric fields';
