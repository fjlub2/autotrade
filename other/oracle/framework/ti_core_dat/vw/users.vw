CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.users_vw
(
   usr_id,
   usr_username,
   usr_first_name,
   usr_last_name,
   usr_email,
   usr_status,
   usr_password,
   usr_pwd_tries,
   usr_change_pw_on_logon,
   usr_lng_id,
   usr_message_level,
   usr_timezone_offset
)
AS
   SELECT
          usr_id,
          usr_username,
          usr_first_name,
          usr_last_name,
          usr_email,
          usr_status,
          usr_password,
          usr_pwd_tries,
          usr_change_pw_on_logon,
          usr_lng_id,
          usr_message_level,
          usr_timezone_offset
     FROM users;

COMMENT ON TABLE TI_CORE_DAT.users_vw IS 'Main user table';

COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_message_level IS 'Message logging level for this user. Set to null to use level from system config. Changes picked up at login.';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_username IS 'Uppercase username';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_first_name IS 'First Name';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_last_name IS 'First Name';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_email IS 'Email Address';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_status IS 'User Status = (A)ctive, (C)losed, (N)ot paid';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_password IS 'Users password - not decrytable';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_pwd_tries IS 'Number of password tries login not allowed after 3 tries';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_change_pw_on_logon IS 'If Y, user will be forced to change their password the next time they login';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_lng_id IS 'Optional language id. If null, the language from the browser will be used.';
COMMENT ON COLUMN TI_CORE_DAT.users_vw.usr_timezone_offset IS 'Users timezone offset in seconds';
