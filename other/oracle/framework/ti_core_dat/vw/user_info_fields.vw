CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.user_info_fields_vw
(
   uif_id,
   uif_label,
   uif_mandatory_ind,
   uif_type,
   uif_app_id,
   uif_display_order,
   uif_code
)
AS
   SELECT
          uif_id,
          uif_label,
          uif_mandatory_ind,
          uif_type,
          uif_app_id,
          uif_display_order,
          uif_code
     FROM user_info_fields;

COMMENT ON TABLE TI_CORE_DAT.user_info_fields_vw IS 'User Info Field Definitions';

COMMENT ON COLUMN TI_CORE_DAT.user_info_fields_vw.uif_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.user_info_fields_vw.uif_label IS 'Field Label';
COMMENT ON COLUMN TI_CORE_DAT.user_info_fields_vw.uif_mandatory_ind IS 'Is this field mandatory? Y/N';
COMMENT ON COLUMN TI_CORE_DAT.user_info_fields_vw.uif_type IS 'Data Type. FK to data_types code domain';
COMMENT ON COLUMN TI_CORE_DAT.user_info_fields_vw.uif_app_id IS 'Optional app specific field. FK to APPS';
COMMENT ON COLUMN TI_CORE_DAT.user_info_fields_vw.uif_display_order IS 'Order that fields should appear on the screen';
COMMENT ON COLUMN TI_CORE_DAT.user_info_fields_vw.uif_code IS 'Field Code';
