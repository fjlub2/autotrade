CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.languages_vw
(
   lng_id,
   lng_code,
   lng_name,
   lng_native_name,
   lng_enabled_ind
)
AS
   SELECT
          lng_id,
          lng_code,
          lng_name,
          lng_native_name,
          lng_enabled_ind
     FROM languages;

COMMENT ON TABLE TI_CORE_DAT.languages_vw IS 'Table to hold all language codes';

COMMENT ON COLUMN TI_CORE_DAT.languages_vw.lng_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.languages_vw.lng_code IS 'Language Code';
COMMENT ON COLUMN TI_CORE_DAT.languages_vw.lng_name IS 'English name for language';
COMMENT ON COLUMN TI_CORE_DAT.languages_vw.lng_native_name IS 'Native name for language';
COMMENT ON COLUMN TI_CORE_DAT.languages_vw.lng_enabled_ind IS 'Enabled? Y/N. Set to Y only when translations have been created';
