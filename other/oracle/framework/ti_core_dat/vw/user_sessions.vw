CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.user_sessions_vw
(
   uss_id,
   uss_usr_id,
   uss_session_start,
   uss_session_id,
   uss_user_ip,
   uss_session_end,
   uss_lng_id,
   uss_code,
   uss_last_used,
   uss_timezone_offset
)
AS
   SELECT
          uss_id,
          uss_usr_id,
          uss_session_start,
          uss_session_id,
          uss_user_ip,
          uss_session_end,
          uss_lng_id,
          uss_code,
          uss_last_used,
          uss_timezone_offset
     FROM user_sessions;

COMMENT ON TABLE TI_CORE_DAT.user_sessions_vw IS 'Table to hold details of user sessions - this table and child tables should be cleared out reguarly';

COMMENT ON COLUMN TI_CORE_DAT.user_sessions_vw.uss_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.user_sessions_vw.uss_usr_id IS 'User Id';
COMMENT ON COLUMN TI_CORE_DAT.user_sessions_vw.uss_session_start IS 'Login Timestamp';
COMMENT ON COLUMN TI_CORE_DAT.user_sessions_vw.uss_session_id IS 'Apex Session Id';
COMMENT ON COLUMN TI_CORE_DAT.user_sessions_vw.uss_user_ip IS 'User IP address';
COMMENT ON COLUMN TI_CORE_DAT.user_sessions_vw.uss_session_end IS 'Logout timestamp - wont be populated when a session times out';
COMMENT ON COLUMN TI_CORE_DAT.user_sessions_vw.uss_lng_id IS 'Browser language. Only set if found and language is enabled.';
COMMENT ON COLUMN TI_CORE_DAT.user_sessions_vw.uss_code IS 'Unique Session Code';
COMMENT ON COLUMN TI_CORE_DAT.user_sessions_vw.uss_last_used IS 'Session last used timestamp';
COMMENT ON COLUMN TI_CORE_DAT.user_sessions_vw.uss_timezone_offset IS 'Users Session timezone offset in seconds';
