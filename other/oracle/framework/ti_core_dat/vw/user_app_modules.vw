CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.user_app_modules_vw
(
   uam_id,
   uam_usa_id,
   uam_mod_id,
   uam_enabled_ind
)
AS
   SELECT
          uam_id,
          uam_usa_id,
          uam_mod_id,
          uam_enabled_ind
     FROM user_app_modules;

COMMENT ON TABLE TI_CORE_DAT.user_app_modules_vw IS 'List of app modules the user can access';

COMMENT ON COLUMN TI_CORE_DAT.user_app_modules_vw.uam_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.user_app_modules_vw.uam_usa_id IS 'User App ID. FK to USER_APPS';
COMMENT ON COLUMN TI_CORE_DAT.user_app_modules_vw.uam_mod_id IS 'Module ID. FK to MODULES';
COMMENT ON COLUMN TI_CORE_DAT.user_app_modules_vw.uam_enabled_ind IS 'Set to N to temporarily disable access to the module';
