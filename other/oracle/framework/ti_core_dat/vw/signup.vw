CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.signup_vw
(
   sgn_id,
   sgn_created_date,
   sgn_username,
   sgn_first_name,
   sgn_last_name,
   sgn_email,
   sgn_terms_accept_ind,
   sgn_tln_id,
   sgn_ip,
   sgn_session_id,
   sgn_status
)
AS
   SELECT
          sgn_id,
          sgn_created_date,
          sgn_username,
          sgn_first_name,
          sgn_last_name,
          sgn_email,
          sgn_terms_accept_ind,
          sgn_tln_id,
          sgn_ip,
          sgn_session_id,
          sgn_status
     FROM signup;

COMMENT ON TABLE TI_CORE_DAT.signup_vw IS 'Table to hold signup requests';

COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_created_date IS 'Date this record was created';
COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_username IS 'Username';
COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_first_name IS 'First Name';
COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_last_name IS 'Last Name';
COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_email IS 'Email';
COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_terms_accept_ind IS 'Have the terms been accepted';
COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_tln_id IS 'Temp Link ID. Used to verify email address. FK to TEMP_LINKS';
COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_ip IS 'IP Address';
COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_session_id IS 'Session ID';
COMMENT ON COLUMN TI_CORE_DAT.signup_vw.sgn_status IS 'Signup Status. (R)eplaced, (T)imed-out, Email (V)erified - user created, (C)reated.';
