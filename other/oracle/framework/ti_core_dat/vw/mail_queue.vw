CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.mail_queue_vw
(
   mlq_id,
   mlq_subject,
   mlq_body,
   mlq_to,
   mlq_from,
   mlq_sent,
   mlq_tries,
   mlq_last_error,
   mlq_usr_id,
   mlq_queued,
   mlq_last_error_date
)
AS
   SELECT
          mlq_id,
          mlq_subject,
          mlq_body,
          mlq_to,
          mlq_from,
          mlq_sent,
          mlq_tries,
          mlq_last_error,
          mlq_usr_id,
          mlq_queued,
          mlq_last_error_date
     FROM mail_queue;

COMMENT ON TABLE TI_CORE_DAT.mail_queue_vw IS 'Queue for outgoing emails. A job will pick these up and process them.';

COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_subject IS 'Message subject';
COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_body IS 'Encrypted message body';
COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_to IS 'comma separated list of recipient email addresses';
COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_from IS 'Senders email address';
COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_sent IS 'Date the message was sent';
COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_tries IS 'Number offailed attempts to send the email';
COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_last_error IS 'Last failure reason';
COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_usr_id IS 'Only populate if message is for a user';
COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_queued IS 'Date message was added to the queue';
COMMENT ON COLUMN TI_CORE_DAT.mail_queue_vw.mlq_last_error_date IS 'Date of last failed attempt. ';
