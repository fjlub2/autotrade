CREATE OR REPLACE FORCE VIEW TI_CORE_DAT.apps_vw
(
   app_id,
   app_name,
   app_enabled_ind,
   app_code
)
AS
   SELECT
          app_id,
          app_name,
          app_enabled_ind,
          app_code
     FROM apps;

COMMENT ON TABLE TI_CORE_DAT.apps_vw IS 'Table to hold application details';

COMMENT ON COLUMN TI_CORE_DAT.apps_vw.app_id IS 'Primary Key';
COMMENT ON COLUMN TI_CORE_DAT.apps_vw.app_name IS 'Application Name';
COMMENT ON COLUMN TI_CORE_DAT.apps_vw.app_enabled_ind IS 'Is the Application Enabled? Y/N';
COMMENT ON COLUMN TI_CORE_DAT.apps_vw.app_code        IS 'Application Code';
