---------------------------------------------
-- no grants to API or MAN schemas in here --
---------------------------------------------


-- need this to help with error tracing - do this on the base table or MV
GRANT REFERENCES ON TI_CORE_DAT.ADDRESS_TYPES_MV    TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.BANNED_PASSWORDS_MV TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.CAPTCHA_ICONS_MV    TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.CONFIG_VALUES_MV    TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.DATA_TYPES_MV       TO TI_CORE_SRC;

GRANT REFERENCES ON TI_CORE_DAT.APPS                TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.CODE_DOMAINS        TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.CODE_VALUES         TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.LANGUAGES           TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.MAIL_QUEUE          TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.MESSAGE_CODES       TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.MESSAGE_LOG_1       TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.MESSAGE_LOG_2       TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.MESSAGE_SUBSTITUTES TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.MESSAGE_TEXT        TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.MODULES             TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.SIGNUP              TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.TEMP_LINKS          TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.USERS               TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.USER_APPS           TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.USER_APP_MODULES    TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.USER_INFO           TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.USER_INFO_FIELDS    TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.USER_INFO_OPTIONS   TO TI_CORE_SRC;
GRANT REFERENCES ON TI_CORE_DAT.USER_SESSIONS       TO TI_CORE_SRC;



-- Views that won't be open to the API or MAN schemas. Ones with corresponding table api's will need SELECT, INSERT, UPDATE, DELETE.
-- Grant SELECT on the others - e.g. code table views.
GRANT SELECT                         ON TI_CORE_DAT.ADDRESS_TYPES_VW       TO TI_CORE_SRC;
GRANT SELECT                         ON TI_CORE_DAT.BANNED_PASSWORDS_VW    TO TI_CORE_SRC;
GRANT SELECT                         ON TI_CORE_DAT.CONFIG_VALUES_VW       TO TI_CORE_SRC;
GRANT SELECT                         ON TI_CORE_DAT.DATA_TYPES_VW          TO TI_CORE_SRC;
GRANT SELECT                         ON TI_CORE_DAT.CONFIG_VALUES_VW       TO TI_CORE_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON TI_CORE_DAT.CODE_VALUES_VW         TO TI_CORE_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON TI_CORE_DAT.MESSAGE_LOG_1_VW       TO TI_CORE_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON TI_CORE_DAT.MESSAGE_LOG_2_VW       TO TI_CORE_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON TI_CORE_DAT.MAIL_QUEUE_VW          TO TI_CORE_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON TI_CORE_DAT.USER_SESSIONS_VW       TO TI_CORE_SRC;
GRANT SELECT                         ON TI_CORE_DAT.MESSAGE_CODES_VW       TO TI_CORE_SRC;
GRANT SELECT                         ON TI_CORE_DAT.MESSAGE_TEXT_VW        TO TI_CORE_SRC;
GRANT SELECT                         ON TI_CORE_DAT.MESSAGE_SUBSTITUTES_VW TO TI_CORE_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON TI_CORE_DAT.SIGNUP_VW              TO TI_CORE_SRC;


-- Views that are open to API or MAN schemas - dont grant select here
GRANT INSERT, UPDATE, DELETE ON TI_CORE_DAT.APPS_VW              TO TI_CORE_SRC;
GRANT INSERT, UPDATE, DELETE ON TI_CORE_DAT.MODULES_VW           TO TI_CORE_SRC;
GRANT INSERT, UPDATE, DELETE ON TI_CORE_DAT.USERS_VW             TO TI_CORE_SRC;
GRANT INSERT, UPDATE, DELETE ON TI_CORE_DAT.USER_INFO_VW         TO TI_CORE_SRC;
GRANT INSERT, UPDATE, DELETE ON TI_CORE_DAT.USER_INFO_FIELDS_VW  TO TI_CORE_SRC;
GRANT INSERT, UPDATE, DELETE ON TI_CORE_DAT.USER_INFO_OPTIONS_VW TO TI_CORE_SRC;
GRANT INSERT, UPDATE, DELETE ON TI_CORE_DAT.USER_APPS_VW         TO TI_CORE_SRC;
GRANT INSERT, UPDATE, DELETE ON TI_CORE_DAT.USER_APP_MODULES_VW  TO TI_CORE_SRC;
GRANT INSERT, UPDATE, DELETE ON TI_CORE_DAT.TEMP_LINKS_VW        TO TI_CORE_SRC;




-- views that are open to API schema - grant select with grant option here so that API schema can pass on permissions
GRANT SELECT ON TI_CORE_DAT.APPS_VW                   TO TI_CORE_SRC WITH GRANT OPTION;
GRANT SELECT ON TI_CORE_DAT.MODULES_VW                TO TI_CORE_SRC WITH GRANT OPTION;
GRANT SELECT ON TI_CORE_DAT.USERS_VW                  TO TI_CORE_SRC WITH GRANT OPTION;
GRANT SELECT ON TI_CORE_DAT.USER_INFO_VW              TO TI_CORE_SRC WITH GRANT OPTION;
GRANT SELECT ON TI_CORE_DAT.USER_INFO_FIELDS_VW       TO TI_CORE_SRC WITH GRANT OPTION;
GRANT SELECT ON TI_CORE_DAT.USER_INFO_OPTIONS_VW      TO TI_CORE_SRC WITH GRANT OPTION;
GRANT SELECT ON TI_CORE_DAT.USER_APPS_VW              TO TI_CORE_SRC WITH GRANT OPTION;
GRANT SELECT ON TI_CORE_DAT.USER_APP_MODULES_VW       TO TI_CORE_SRC WITH GRANT OPTION;
GRANT SELECT ON TI_CORE_DAT.CAPTCHA_ICONS_VW          TO TI_CORE_SRC WITH GRANT OPTION;
GRANT SELECT ON TI_CORE_DAT.LANGUAGES_VW              TO TI_CORE_SRC WITH GRANT OPTION;
GRANT SELECT ON TI_CORE_DAT.TEMP_LINKS_VW             TO TI_CORE_SRC WITH GRANT OPTION;




-- sequences are normally covered by the aaa_POP_PK triggers, those that arent should be granted here
GRANT SELECT ON TI_CORE_DAT.MSS_TASK_SEQ TO TI_CORE_SRC;
GRANT SELECT ON TI_CORE_DAT.USS_CODE_SEQ TO TI_CORE_SRC;



-- grants on types
GRANT EXECUTE ON TI_CORE_DAT.TT_DATA TO TI_CORE_SRC WITH GRANT OPTION;
GRANT EXECUTE ON TI_CORE_DAT.TR_DATA TO TI_CORE_SRC WITH GRANT OPTION;




