CREATE OR REPLACE TYPE ti_core_dat.tr_data AS OBJECT (tag_name  VARCHAR2(4000),
                                                      tag_value VARCHAR2(4000));
                          
CREATE OR REPLACE TYPE ti_core_dat.tt_data AS TABLE OF ti_core_dat.tr_data;