CREATE OR REPLACE PACKAGE ss_src.a_rest IS
  /*********************************************************
   Package that returns data for the JSON REST API's
  **********************************************************/
  
  /* Returns the requested chart data in JSON format */
  FUNCTION getChartData (pi_exchange IN VARCHAR2,
                         pi_symbol   IN VARCHAR2,
                         pi_tframe   IN INTEGER, -- timeframe in minutes
                         pi_from     IN DATE,
                         pi_to       IN DATE) RETURN CLOB;

  /* Get Exponential Moving Average */
  FUNCTION getEma (pi_exchange IN VARCHAR2,
                   pi_symbol   IN VARCHAR2,
                   pi_tframe   IN INTEGER, -- timeframe in minutes
                   pi_from     IN DATE,
                   pi_to       IN DATE,
                   pi_periods  IN INTEGER) RETURN CLOB;
END a_rest;
/
