CREATE OR REPLACE PACKAGE ss_src.a_studies IS
  /*********************************************************
   Package that returns study data
  **********************************************************/

/* Returns data and close proice for the requested chart data */
  FUNCTION getChartData (pi_exchange IN VARCHAR2,
                         pi_symbol   IN VARCHAR2,
                         pi_tframe   IN INTEGER, -- timeframe in minutes
                         pi_from     IN DATE,
                         pi_to       IN DATE) RETURN tt_studyData;
                         
  /* Get Exponential Moving Average */
  FUNCTION getEma (pi_data     IN tt_studyData,
                   pi_exchange IN VARCHAR2,
                   pi_symbol   IN VARCHAR2,
                   pi_periods  IN INTEGER) RETURN tt_studyData;

  /* Get Exponential Moving Average */
  FUNCTION getEma (pi_exchange IN VARCHAR2,
                   pi_symbol   IN VARCHAR2,
                   pi_tframe   IN INTEGER, -- timeframe in minutes
                   pi_from     IN DATE,
                   pi_to       IN DATE,
                   pi_periods  IN INTEGER) RETURN tt_studyData;
                   
/* Get MACD figures */
  FUNCTION getMACD (pi_data        IN tt_studyData,
                    pi_exchange    IN VARCHAR2,
                    pi_symbol      IN VARCHAR2,
                    pi_lowPeriods  IN INTEGER DEFAULT 12,
                    pi_highPeriods IN INTEGER DEFAULT 26,
                    pi_macdPeriods IN INTEGER DEFAULT 9) RETURN tt_studyMACD;
END a_studies;
/
