CREATE OR REPLACE PACKAGE BODY ss_src.a_studies IS
  FUNCTION getChartData (pi_exchange IN VARCHAR2,
                         pi_symbol   IN VARCHAR2,
                         pi_tframe   IN INTEGER, -- timeframe in minutes
                         pi_from     IN DATE,
                         pi_to       IN DATE) RETURN tt_studyData IS
    l_data   tt_studyData;
    
    l_dexhId INTEGER;
    l_dsymId INTEGER;
    l_dtfrId INTEGER;
  BEGIN
    l_dexhId := t_dExchanges.getId_UK (p_uk => pi_exchange);
    IF l_dexhId IS NULL THEN
      RAISE_APPLICATION_ERROR (-20000,'Exchange Does Not Exist.');
    END IF;
  
    l_dsymId := t_dSymbols.getId_UK (p_uk => pi_symbol, p_uk2 => l_dexhId);
    IF l_dsymId IS NULL THEN
      RAISE_APPLICATION_ERROR (-20000,'Symbol Does Not Exist.');
    END IF;
    
    l_dtfrId := t_dTimeFrames.getId_UK (p_uk => pi_tframe);
    IF l_dtfrId IS NULL THEN
      RAISE_APPLICATION_ERROR (-20000,'Time Frame Does Not Exist.');
    END IF;
  
    -- check timeframe valid for symbol
  
    SELECT ot_studyData(fdat_date,
                        fdat_unix_ms,
                        fdat_close)
      BULK COLLECT INTO l_data
      FROM f_data
        JOIN d_symbols ON fdat_dsym_id = dsym_id
     WHERE dsym_code    =  pi_symbol
       AND fdat_ddat_id IN (SELECT ddat_id FROM d_dates      WHERE ddat_date BETWEEN pi_from AND pi_to)
       AND fdat_dtfr_id =  (SELECT dtfr_id FROM d_timeframes WHERE dtfr_mins = pi_tframe)
     ORDER BY fdat_date;
        
    RETURN l_data;
  END getChartData;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getEma (pi_data     IN tt_studyData,
                   pi_exchange IN VARCHAR2,
                   pi_symbol   IN VARCHAR2,
                   pi_periods  IN INTEGER) RETURN tt_studyData IS
    l_data      tt_studyData;
    l_firstEMA  NUMBER  := 0;
    l_exponent  NUMBER  := ROUND(2 / (pi_periods + 1),4);
    l_decimals  INTEGER := t_dSymbols.getDecimals_UK(p_uk => pi_symbol, p_uk2 => t_dExchanges.getId_UK (p_uk => pi_exchange));
  BEGIN
    l_data := pi_data;
    
    FOR i IN 1..l_data.COUNT LOOP
      IF i <= pi_periods THEN
        l_firstEMA := l_firstEMA + l_data(i).ts_value;
      ELSIF i = pi_periods + 1 THEN
        l_data(i).ts_value := ROUND(l_firstEMA / pi_periods, l_decimals);
      ELSE 
        l_data(i).ts_value := ROUND(l_data(i).ts_value * l_exponent + l_data(i-1).ts_value * (1 - l_exponent), l_decimals);
      END IF;
    END LOOP;
/*
    WITH
      params  AS (SELECT ROUND(2 / (pi_periods + 1),4) exponent FROM dual)
    SELECT ot_study(ts_date,
                    ts_unix_ms,
                    ema)
      BULK COLLECT INTO l_data
      FROM (SELECT ts_date,
                   ts_unix_ms,
                   ema
              FROM d_symbols
                CROSS JOIN TABLE(pi_data)
                CROSS JOIN params
             WHERE dsym_code = pi_symbol
             MODEL
               DIMENSION BY (ROW_NUMBER() OVER (ORDER BY ts_date) r)
               MEASURES (ts_date,
                         ts_close,
                         dsym_code,
                         dsym_decimals,
                         pi_periods noOfPeriods,
                         exponent,
                         ts_unix_ms,
                         0 ema)
               RULES (ema[r < noOfPeriods[1] + 1] = ts_close[cv()],                                                                               -- set EMA for rows 1..noOfPeriods to the current close price 
                      ema[    noOfPeriods[1] + 1] = ROUND(AVG(ts_close) [r BETWEEN 1 AND noOfPeriods[1]+1], dsym_decimals[cv()]),                 -- set EMA for row = noOfPeriods + 1 to the AVG of all previous close prices 
                      ema[r > noOfPeriods[1] + 1] = ROUND(ts_close[cv()] * exponent[1] + ema[cv()-1] * (1 - exponent[1]), dsym_decimals[cv()]))); -- set EMA for remaining rows = current close price * exponent + previous EMA * (1 - exponent)
  */
    RETURN l_data;
  END getEma;
  
  ----------------------------------------------------------------------------------------------------------------
 
  FUNCTION getEMA (pi_exchange IN VARCHAR2,
                   pi_symbol   IN VARCHAR2,
                   pi_tframe   IN INTEGER, -- timeframe in minutes
                   pi_from     IN DATE,
                   pi_to       IN DATE,
                   pi_periods  IN INTEGER) RETURN tt_studyData IS
    l_data tt_studyData;
    
    l_dexhId INTEGER;
    l_dsymId INTEGER;
    l_dtfrId INTEGER;
  BEGIN
    l_dexhId := t_dExchanges.getId_UK (p_uk => pi_exchange);
    IF l_dexhId IS NULL THEN
      RAISE_APPLICATION_ERROR (-20000,'Exchange Does Not Exist.');
    END IF;
      
    l_dtfrId := t_dTimeFrames.getId_UK (p_uk => pi_tframe);
    IF l_dtfrId IS NULL THEN
      RAISE_APPLICATION_ERROR (-20000,'Time Frame Does Not Exist.');
    END IF;
    
    l_dsymId := t_dSymbols.getId_UK (p_uk => pi_symbol, p_uk2 => l_dexhId);
    IF l_dsymId IS NULL THEN
      RAISE_APPLICATION_ERROR (-20000,'Symbol Does Not Exist.');
    END IF;
    
    -- check timeframe valid for symbol
    
    -- doing this instead of calling the other getEMA as it's quicker.
    WITH
      params  AS (SELECT ROUND(2 / (pi_periods + 1),4) exponent FROM dual)
    SELECT ot_studyData(fdat_date,
                        fdat_unix_ms,
                        ema)
      BULK COLLECT INTO l_data
      FROM (SELECT fdat_date,
                   fdat_unix_ms,
                   ema
              FROM f_data
                JOIN d_symbols ON fdat_dsym_id = dsym_id
                CROSS JOIN params
             WHERE dsym_code    =  pi_symbol
               AND fdat_ddat_id IN (SELECT ddat_id FROM d_dates      WHERE ddat_date BETWEEN pi_from AND pi_to)
               AND fdat_dtfr_id =  (SELECT dtfr_id FROM d_timeframes WHERE dtfr_mins = pi_tframe)
             MODEL
               DIMENSION BY (ROW_NUMBER() OVER (ORDER BY fdat_date) r)
               MEASURES (fdat_date,
                         fdat_close,
                         dsym_code,
                         dsym_decimals,
                         pi_periods noOfPeriods,
                         exponent,
                         fdat_unix_ms,
                         0 ema)
               RULES (ema[r < noOfPeriods[1] + 1] = fdat_close[cv()],                                                                               -- set EMA for rows 1..noOfPeriods to the current close price 
                      ema[    noOfPeriods[1] + 1] = ROUND(AVG(fdat_close) [r BETWEEN 1 AND noOfPeriods[1]+1], dsym_decimals[cv()]),                 -- set EMA for row = noOfPeriods + 1 to the AVG of all previous close prices 
                      ema[r > noOfPeriods[1] + 1] = ROUND(fdat_close[cv()] * exponent[1] + ema[cv()-1] * (1 - exponent[1]), dsym_decimals[cv()]))); -- set EMA for remaining rows = current close price * exponent + previous EMA * (1 - exponent)

    RETURN l_data;
  END getEMA;
  
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getMACD (pi_data        IN tt_studyData,
                    pi_exchange    IN VARCHAR2,
                    pi_symbol      IN VARCHAR2,
                    pi_lowPeriods  IN INTEGER DEFAULT 12,
                    pi_highPeriods IN INTEGER DEFAULT 26,
                    pi_macdPeriods IN INTEGER DEFAULT 9) RETURN tt_studyMACD IS
    l_data         tt_studyMACD := tt_studyMACD();
    l_firstLowEMA  NUMBER  := 0;
    l_firstHighEMA NUMBER  := 0;
    l_firstMacdEMA NUMBER  := 0;
    l_lowExponent  NUMBER  := ROUND(2 / (pi_lowPeriods  + 1),4);
    l_highExponent NUMBER  := ROUND(2 / (pi_highPeriods + 1),4);
    l_macdExponent NUMBER  := ROUND(2 / (pi_macdPeriods + 1),4);
    l_decimals     INTEGER := t_dSymbols.getDecimals_UK(p_uk => pi_symbol, p_uk2 => t_dExchanges.getId_UK (p_uk => pi_exchange));
    
    TYPE t_direction  IS TABLE OF CHAR(1) INDEX BY BINARY_INTEGER;
    l_direction       t_direction;
  BEGIN
    IF pi_lowPeriods IS NULL THEN
      RAISE_APPLICATION_ERROR (-20000,'Number of Periods must be specified for the Lower EMA.');
    END IF;
    
    IF pi_highPeriods IS NULL THEN
      RAISE_APPLICATION_ERROR (-20000,'Number of Periods must be specified for the Higher EMA.');
    END IF;
    
    IF pi_macdPeriods IS NULL THEN
      RAISE_APPLICATION_ERROR (-20000,'Number of Periods must be specified for the MACD EMA.');
    END IF;
  
    IF pi_highPeriods < pi_lowPeriods THEN
      RAISE_APPLICATION_ERROR (-20000,'Number of periods for the Higher EMA ('||pi_highPeriods||' must be greater than the number of periods for the Lower EMA ('||pi_lowPeriods||').');
    END IF;

    FOR i IN 1..pi_data.COUNT LOOP
      l_data.extend();
      
      l_data(i)            := ot_studyMACD();
      l_data(i).ts_date    := pi_data(i).ts_date;
      l_data(i).ts_unix_ms := pi_data(i).ts_unix_ms;
      l_data(i).ts_close   := pi_data(i).ts_value;
     
      -- calc Lower EMA
      IF i <= pi_lowPeriods THEN
        l_firstLowEMA        := l_firstLowEMA + pi_data(i).ts_value;
        l_data(i).ts_ema_low := pi_data(i).ts_value;
      ELSIF i = pi_lowPeriods + 1 THEN
        l_data(i).ts_ema_low := ROUND(l_firstLowEMA / pi_lowPeriods, l_decimals);
      ELSE 
        l_data(i).ts_ema_low := ROUND(pi_data(i).ts_value * l_lowExponent + l_data(i-1).ts_ema_low * (1 - l_lowExponent), l_decimals);
      END IF;
      
      -- calc Lower EMA
      IF i <= pi_highPeriods THEN
        l_firstHighEMA        := l_firstHighEMA + pi_data(i).ts_value;
        l_data(i).ts_ema_high := pi_data(i).ts_value;
      ELSIF i = pi_highPeriods + 1 THEN
        l_data(i).ts_ema_high := ROUND(l_firstHighEMA / pi_highPeriods, l_decimals);
      ELSE 
        l_data(i).ts_ema_high := ROUND(pi_data(i).ts_value * l_highExponent + l_data(i-1).ts_ema_high * (1 - l_highExponent), l_decimals);
      END IF;
      
      -- calc MACD
      l_data(i).ts_macd := l_data(i).ts_ema_low - l_data(i).ts_ema_high;
      
      -- calc MACD Signal (EMA)
      IF i <= pi_macdPeriods THEN
        l_firstMacdEMA           := l_firstMacdEMA + l_data(i).ts_macd;
        l_data(i).ts_macd_signal := l_data(i).ts_macd;
      ELSIF i = pi_macdPeriods + 1 THEN
        l_data(i).ts_macd_signal := ROUND(l_firstMacdEMA / pi_macdPeriods, l_decimals);
      ELSE 
        l_data(i).ts_macd_signal := ROUND(l_data(i).ts_macd * l_macdExponent + l_data(i-1).ts_macd_signal * (1 - l_macdExponent), l_decimals);
      END IF;
      
      -- calc MACD Histogram
      l_data(i).ts_macd_histogram := l_data(i).ts_macd - l_data(i).ts_macd_signal;
      
      -- calc Crossover
      IF l_data(i).ts_macd > l_data(i).ts_macd_signal THEN
        l_data(i).ts_direction := 'L';
      ELSIF l_data(i).ts_macd_signal > l_data(i).ts_macd THEN
        l_data(i).ts_direction := 'S';
      ELSE
        IF l_data.EXISTS(i - 1) THEN
          l_data(i).ts_direction := l_data(i - 1).ts_direction;
        END IF;
      END IF;

      /*
      IF i > 1 THEN
        IF l_data(i).ts_macd > l_data(i).ts_macd_signal THEN
          l_direction(i) := 'L';
        ELSIF l_data(i).ts_macd_signal > l_data(i).ts_macd THEN
          l_direction(i) := 'S';
        ELSE
          l_direction(i) := l_direction(i - 1);
        END IF;
      END IF;*/
      /*
      IF l_direction(i) IS NOT NULL THEN
        IF l_direction(i) != l_direction(i - 1) THEN
          l_data(i).ts_crossover := l_direction(i);
        END IF;
      END IF;*/
    END LOOP;
    
    RETURN l_data;
  END getMACD;
  
  ----------------------------------------------------------------------------------------------------------------
END a_studies;
/
