CREATE OR REPLACE PACKAGE BODY ss_src.a_rest IS

  FUNCTION getChartData (pi_exchange IN VARCHAR2,
                         pi_symbol   IN VARCHAR2,
                         pi_tframe   IN INTEGER, -- timeframe in minutes
                         pi_from     IN DATE,
                         pi_to       IN DATE) RETURN CLOB IS
    l_clob CLOB;
    
    l_dexhId INTEGER;
    l_dsymId INTEGER;
    l_dtfrId INTEGER;
  BEGIN
    l_dexhId := t_dExchanges.getId_UK (p_uk => pi_exchange);
    IF l_dexhId IS NULL THEN
      RETURN '{"errorMessages":["Exchange Does Not Exist"],"errors":{}}';
    END IF;
  
    l_dsymId := t_dSymbols.getId_UK (p_uk => pi_symbol, p_uk2 => l_dexhId);
    IF l_dsymId IS NULL THEN
      RETURN '{"errorMessages":["Symbol Does Not Exist"],"errors":{}}';
    END IF;
    
    l_dtfrId := t_dTimeFrames.getId_UK (p_uk => pi_tframe);
    IF l_dtfrId IS NULL THEN
      RETURN '{"errorMessages":["Time Frame Does Not Exist"],"errors":{}}';
    END IF;
  
    -- check timeframe valid for symbol
  
    SELECT '['||TRIM(',' FROM XMLAGG(XMLELEMENT(E,'['||fdat_unix_ms||','||fdat_open||','||fdat_high||','||fdat_low||','||fdat_close||','||fdat_volume||']'||',')).EXTRACT('//text()').getClobVal())||']'
      INTO l_clob
      FROM f_data
        JOIN d_symbols ON fdat_dsym_id = dsym_id
     WHERE dsym_code    =  pi_symbol
       AND fdat_ddat_id IN (SELECT ddat_id FROM d_dates      WHERE ddat_date BETWEEN pi_from AND pi_to)
       AND fdat_dtfr_id =  (SELECT dtfr_id FROM d_timeframes WHERE dtfr_mins = pi_tframe);
        
    RETURN l_clob;
  END getChartData;
  ----------------------------------------------------------------------------------------------------------------
  
  FUNCTION getEMA (pi_exchange IN VARCHAR2,
                   pi_symbol   IN VARCHAR2,
                   pi_tframe   IN INTEGER, -- timeframe in minutes
                   pi_from     IN DATE,
                   pi_to       IN DATE,
                   pi_periods  IN INTEGER) RETURN CLOB IS
    l_clob CLOB;
    
    l_dexhId INTEGER;
    l_dsymId INTEGER;
    l_dtfrId INTEGER;
  BEGIN
    l_dexhId := t_dExchanges.getId_UK (p_uk => pi_exchange);
    IF l_dexhId IS NULL THEN
      RETURN '{"errorMessages":["Exchange Does Not Exist"],"errors":{}}';
    END IF;
  
    l_dsymId := t_dSymbols.getId_UK (p_uk => pi_symbol, p_uk2 => l_dexhId);
    IF l_dsymId IS NULL THEN
      RETURN '{"errorMessages":["Symbol Does Not Exist"],"errors":{}}';
    END IF;
    
    l_dtfrId := t_dTimeFrames.getId_UK (p_uk => pi_tframe);
    IF l_dtfrId IS NULL THEN
      RETURN '{"errorMessages":["Time Frame Does Not Exist"],"errors":{}}';
    END IF;
    
    IF pi_periods IS NULL THEN
      RETURN '{"errorMessages":["Number of periods must be supplied"],"errors":{}}';
    END IF;
    
    -- check timeframe valid for symbol
    
    WITH
      params  AS (SELECT ROUND(2 / (pi_periods + 1),4) exponent FROM dual)
    SELECT '['||TRIM(',' FROM XMLAGG(XMLELEMENT(E,'['||fdat_unix_ms||','||ema||']'||',')).EXTRACT('//text()').getClobVal())||']'
      INTO l_clob
      FROM (SELECT fdat_unix_ms,
                   ema
              FROM f_data
                JOIN d_symbols ON fdat_dsym_id = dsym_id
                CROSS JOIN params
             WHERE dsym_code    =  pi_symbol
               AND fdat_ddat_id IN (SELECT ddat_id FROM d_dates      WHERE ddat_date BETWEEN pi_from AND pi_to)
               AND fdat_dtfr_id =  (SELECT dtfr_id FROM d_timeframes WHERE dtfr_mins = pi_tframe)
             MODEL
               PARTITION BY (fdat_dsym_id,
                             fdat_dtfr_id)
               DIMENSION BY (ROW_NUMBER() OVER (PARTITION BY fdat_dsym_id, fdat_dtfr_id ORDER BY fdat_date) r)
               MEASURES (fdat_date,
                         fdat_close,
                         dsym_code,
                         dsym_decimals,
                         pi_periods noOfPeriods,
                         exponent,
                         fdat_unix_ms,
                         0 ema)
               RULES (ema[r < noOfPeriods[1] + 1] = fdat_close[cv()],                                                                               -- set EMA for rows 1..noOfPeriods to the current close price 
                      ema[    noOfPeriods[1] + 1] = ROUND(AVG(fdat_close) [r BETWEEN 1 AND noOfPeriods[1]+1], dsym_decimals[cv()]),                 -- set EMA for row = noOfPeriods + 1 to the AVG of all previous close prices 
                      ema[r > noOfPeriods[1] + 1] = ROUND(fdat_close[cv()] * exponent[1] + ema[cv()-1] * (1 - exponent[1]), dsym_decimals[cv()]))); -- set EMA for remaining rows = current close price * exponent + previous EMA * (1 - exponent)
    
    RETURN l_clob;
  END getEMA;
END a_rest;
/
