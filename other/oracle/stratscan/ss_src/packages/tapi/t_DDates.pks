CREATE OR REPLACE PACKAGE SS_SRC.t_DDates IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for d_dates table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF d_dates%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              ddat_date   d_dates.ddat_date%TYPE,
                              ddat_day   d_dates.ddat_day%TYPE,
                              ddat_week   d_dates.ddat_week%TYPE,
                              ddat_month   d_dates.ddat_month%TYPE,
                              ddat_quarter   d_dates.ddat_quarter%TYPE,
                              ddat_year   d_dates.ddat_year%TYPE,
                              ddat_we   d_dates.ddat_we%TYPE,
                              ddat_wd   d_dates.ddat_wd%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              ddat_date   VARCHAR2(1),
                              ddat_day   VARCHAR2(1),
                              ddat_week   VARCHAR2(1),
                              ddat_month   VARCHAR2(1),
                              ddat_quarter   VARCHAR2(1),
                              ddat_year   VARCHAR2(1),
                              ddat_we   VARCHAR2(1),
                              ddat_wd   VARCHAR2(1));

 /* Returns a row from the D_DATES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates%ROWTYPE;

 /* Pipes a row from the D_DATES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the D_DATES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the D_DATES table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates%ROWTYPE;

 /* Pipes a row from the D_DATES table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the D_DATES table for the supplied UK */
  FUNCTION exists_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN VARCHAR2;

 /* Returns DDAT_ID from the D_DATES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_id%TYPE;

 /* Returns DDAT_ID from the D_DATES table for the supplied UK */
  FUNCTION getId_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_id%TYPE;

 /* Returns DDAT_DATE from the D_DATES table for the supplied PK */
  FUNCTION getDate_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_date%TYPE;

 /* Returns DDAT_DATE from the D_DATES table for the supplied UK */
  FUNCTION getDate_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_date%TYPE;

 /* Returns DDAT_DAY from the D_DATES table for the supplied PK */
  FUNCTION getDay_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_day%TYPE;

 /* Returns DDAT_DAY from the D_DATES table for the supplied UK */
  FUNCTION getDay_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_day%TYPE;

 /* Returns DDAT_WEEK from the D_DATES table for the supplied PK */
  FUNCTION getWeek_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_week%TYPE;

 /* Returns DDAT_WEEK from the D_DATES table for the supplied UK */
  FUNCTION getWeek_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_week%TYPE;

 /* Returns DDAT_MONTH from the D_DATES table for the supplied PK */
  FUNCTION getMonth_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_month%TYPE;

 /* Returns DDAT_MONTH from the D_DATES table for the supplied UK */
  FUNCTION getMonth_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_month%TYPE;

 /* Returns DDAT_QUARTER from the D_DATES table for the supplied PK */
  FUNCTION getQuarter_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_quarter%TYPE;

 /* Returns DDAT_QUARTER from the D_DATES table for the supplied UK */
  FUNCTION getQuarter_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_quarter%TYPE;

 /* Returns DDAT_YEAR from the D_DATES table for the supplied PK */
  FUNCTION getYear_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_year%TYPE;

 /* Returns DDAT_YEAR from the D_DATES table for the supplied UK */
  FUNCTION getYear_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_year%TYPE;

 /* Returns DDAT_WE from the D_DATES table for the supplied PK */
  FUNCTION getWe_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_we%TYPE;

 /* Returns DDAT_WE from the D_DATES table for the supplied UK */
  FUNCTION getWe_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_we%TYPE;

 /* Returns DDAT_WD from the D_DATES table for the supplied PK */
  FUNCTION getWd_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_wd%TYPE;

 /* Returns DDAT_WD from the D_DATES table for the supplied UK */
  FUNCTION getWd_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_wd%TYPE;

 /* Updates D_DATES.DDAT_DATE to the supplied value for the supplied PK */
  PROCEDURE setDate (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_date%TYPE);

 /* Updates D_DATES.DDAT_DAY to the supplied value for the supplied PK */
  PROCEDURE setDay (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_day%TYPE);

 /* Updates D_DATES.DDAT_WEEK to the supplied value for the supplied PK */
  PROCEDURE setWeek (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_week%TYPE);

 /* Updates D_DATES.DDAT_MONTH to the supplied value for the supplied PK */
  PROCEDURE setMonth (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_month%TYPE);

 /* Updates D_DATES.DDAT_QUARTER to the supplied value for the supplied PK */
  PROCEDURE setQuarter (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_quarter%TYPE);

 /* Updates D_DATES.DDAT_YEAR to the supplied value for the supplied PK */
  PROCEDURE setYear (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_year%TYPE);

 /* Updates D_DATES.DDAT_WE to the supplied value for the supplied PK */
  PROCEDURE setWe (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_we%TYPE);

 /* Updates D_DATES.DDAT_WD to the supplied value for the supplied PK */
  PROCEDURE setWd (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_wd%TYPE);

 /* Updates a row on the D_DATES table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN d_dates.ddat_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the D_DATES table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN d_dates.ddat_id%TYPE, 
                      p_date IN d_dates.ddat_date%TYPE,
                      p_day IN d_dates.ddat_day%TYPE,
                      p_week IN d_dates.ddat_week%TYPE,
                      p_month IN d_dates.ddat_month%TYPE,
                      p_quarter IN d_dates.ddat_quarter%TYPE,
                      p_year IN d_dates.ddat_year%TYPE,
                      p_we IN d_dates.ddat_we%TYPE,
                      p_wd IN d_dates.ddat_wd%TYPE );

 /* Inserts a row into the D_DATES table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN d_dates%ROWTYPE, p_newPk OUT d_dates.ddat_id%TYPE);

 /* Inserts a row into the D_DATES table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN d_dates.ddat_id%TYPE,
                      p_date IN d_dates.ddat_date%TYPE,
                      p_day IN d_dates.ddat_day%TYPE,
                      p_week IN d_dates.ddat_week%TYPE,
                      p_month IN d_dates.ddat_month%TYPE,
                      p_quarter IN d_dates.ddat_quarter%TYPE,
                      p_year IN d_dates.ddat_year%TYPE,
                      p_we IN d_dates.ddat_we%TYPE,
                      p_wd IN d_dates.ddat_wd%TYPE, p_newPk OUT d_dates.ddat_id%TYPE);

 /* Deletes a row from the D_DATES table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN d_dates.ddat_id%TYPE);

END t_DDates;
/
