CREATE OR REPLACE PACKAGE BODY SS_SRC.t_DSymbols IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN d_symbols.dsym_id%TYPE, p_uk IN d_symbols.dsym_code%TYPE DEFAULT NULL, p_uk2 IN d_symbols.dsym_dexh_id%TYPE DEFAULT NULL) RETURN d_symbols%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN d_symbols.dsym_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN d_symbols.dsym_id%TYPE);

  PROCEDURE doInsert (p_row IN d_symbols%ROWTYPE, p_newPk OUT d_symbols.dsym_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN VARCHAR2 IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.dsym_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk, p_uk2 => p_uk2);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk, p_uk2 => p_uk2));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN VARCHAR2 IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    IF l_row.dsym_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_id%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dsym_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_id%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.dsym_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDexhId_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_dexh_id%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dsym_dexh_id;
  END getDexhId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDexhId_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_dexh_id%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.dsym_dexh_id;
  END getDexhId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_code%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dsym_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_code%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.dsym_code;
  END getCode_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_name%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dsym_name;
  END getName_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getName_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_name%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.dsym_name;
  END getName_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDsygId_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_dsyg_id%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dsym_dsyg_id;
  END getDsygId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDsygId_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_dsyg_id%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.dsym_dsyg_id;
  END getDsygId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDmonId_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_dmon_id%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dsym_dmon_id;
  END getDmonId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDmonId_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_dmon_id%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.dsym_dmon_id;
  END getDmonId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getYear_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_year%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dsym_year;
  END getYear_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getYear_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_year%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.dsym_year;
  END getYear_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDecimals_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_decimals%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dsym_decimals;
  END getDecimals_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDecimals_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_decimals%TYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk, p_uk2 => p_uk2);

    RETURN l_row.dsym_decimals;
  END getDecimals_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setDexhId (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_dexh_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.dsym_dexh_id := a_data.gFalse;
    l_ignoreRow.dsym_code := a_data.gTrue;
    l_ignoreRow.dsym_name := a_data.gTrue;
    l_ignoreRow.dsym_dsyg_id := a_data.gTrue;
    l_ignoreRow.dsym_dmon_id := a_data.gTrue;
    l_ignoreRow.dsym_year := a_data.gTrue;
    l_ignoreRow.dsym_decimals := a_data.gTrue;

    l_updateRow.dsym_dexh_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDexhId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setCode (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.dsym_dexh_id := a_data.gTrue;
    l_ignoreRow.dsym_code := a_data.gFalse;
    l_ignoreRow.dsym_name := a_data.gTrue;
    l_ignoreRow.dsym_dsyg_id := a_data.gTrue;
    l_ignoreRow.dsym_dmon_id := a_data.gTrue;
    l_ignoreRow.dsym_year := a_data.gTrue;
    l_ignoreRow.dsym_decimals := a_data.gTrue;

    l_updateRow.dsym_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setName (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_name%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.dsym_dexh_id := a_data.gTrue;
    l_ignoreRow.dsym_code := a_data.gTrue;
    l_ignoreRow.dsym_name := a_data.gFalse;
    l_ignoreRow.dsym_dsyg_id := a_data.gTrue;
    l_ignoreRow.dsym_dmon_id := a_data.gTrue;
    l_ignoreRow.dsym_year := a_data.gTrue;
    l_ignoreRow.dsym_decimals := a_data.gTrue;

    l_updateRow.dsym_name := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setName;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDsygId (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_dsyg_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.dsym_dexh_id := a_data.gTrue;
    l_ignoreRow.dsym_code := a_data.gTrue;
    l_ignoreRow.dsym_name := a_data.gTrue;
    l_ignoreRow.dsym_dsyg_id := a_data.gFalse;
    l_ignoreRow.dsym_dmon_id := a_data.gTrue;
    l_ignoreRow.dsym_year := a_data.gTrue;
    l_ignoreRow.dsym_decimals := a_data.gTrue;

    l_updateRow.dsym_dsyg_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDsygId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDmonId (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_dmon_id%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.dsym_dexh_id := a_data.gTrue;
    l_ignoreRow.dsym_code := a_data.gTrue;
    l_ignoreRow.dsym_name := a_data.gTrue;
    l_ignoreRow.dsym_dsyg_id := a_data.gTrue;
    l_ignoreRow.dsym_dmon_id := a_data.gFalse;
    l_ignoreRow.dsym_year := a_data.gTrue;
    l_ignoreRow.dsym_decimals := a_data.gTrue;

    l_updateRow.dsym_dmon_id := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDmonId;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setYear (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_year%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.dsym_dexh_id := a_data.gTrue;
    l_ignoreRow.dsym_code := a_data.gTrue;
    l_ignoreRow.dsym_name := a_data.gTrue;
    l_ignoreRow.dsym_dsyg_id := a_data.gTrue;
    l_ignoreRow.dsym_dmon_id := a_data.gTrue;
    l_ignoreRow.dsym_year := a_data.gFalse;
    l_ignoreRow.dsym_decimals := a_data.gTrue;

    l_updateRow.dsym_year := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setYear;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDecimals (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_decimals%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.dsym_dexh_id := a_data.gTrue;
    l_ignoreRow.dsym_code := a_data.gTrue;
    l_ignoreRow.dsym_name := a_data.gTrue;
    l_ignoreRow.dsym_dsyg_id := a_data.gTrue;
    l_ignoreRow.dsym_dmon_id := a_data.gTrue;
    l_ignoreRow.dsym_year := a_data.gTrue;
    l_ignoreRow.dsym_decimals := a_data.gFalse;

    l_updateRow.dsym_decimals := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDecimals;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN d_symbols.dsym_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN d_symbols.dsym_id%TYPE, 
                      p_dexh_id IN d_symbols.dsym_dexh_id%TYPE,
                      p_code IN d_symbols.dsym_code%TYPE,
                      p_name IN d_symbols.dsym_name%TYPE,
                      p_dsyg_id IN d_symbols.dsym_dsyg_id%TYPE,
                      p_dmon_id IN d_symbols.dsym_dmon_id%TYPE,
                      p_year IN d_symbols.dsym_year%TYPE,
                      p_decimals IN d_symbols.dsym_decimals%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.dsym_dexh_id := p_dexh_id;
    l_row.dsym_code := p_code;
    l_row.dsym_name := p_name;
    l_row.dsym_dsyg_id := p_dsyg_id;
    l_row.dsym_dmon_id := p_dmon_id;
    l_row.dsym_year := p_year;
    l_row.dsym_decimals := p_decimals;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN d_symbols%ROWTYPE, p_newPK OUT d_symbols.dsym_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN d_symbols.dsym_id%TYPE,
                      p_dexh_id IN d_symbols.dsym_dexh_id%TYPE,
                      p_code IN d_symbols.dsym_code%TYPE,
                      p_name IN d_symbols.dsym_name%TYPE,
                      p_dsyg_id IN d_symbols.dsym_dsyg_id%TYPE,
                      p_dmon_id IN d_symbols.dsym_dmon_id%TYPE,
                      p_year IN d_symbols.dsym_year%TYPE,
                      p_decimals IN d_symbols.dsym_decimals%TYPE, p_newPK OUT d_symbols.dsym_id%TYPE) IS
    l_row   d_symbols%ROWTYPE;
  BEGIN
    l_row.dsym_id := p_id;
    l_row.dsym_dexh_id := p_dexh_id;
    l_row.dsym_code := p_code;
    l_row.dsym_name := p_name;
    l_row.dsym_dsyg_id := p_dsyg_id;
    l_row.dsym_dmon_id := p_dmon_id;
    l_row.dsym_year := p_year;
    l_row.dsym_decimals := p_decimals;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN d_symbols.dsym_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN d_symbols.dsym_id%TYPE, p_uk IN d_symbols.dsym_code%TYPE DEFAULT NULL, p_uk2 IN d_symbols.dsym_dexh_id%TYPE DEFAULT NULL) RETURN d_symbols%ROWTYPE IS
    l_row    d_symbols%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM d_symbols
       WHERE dsym_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM d_symbols
       WHERE dsym_code = p_uk
         AND ((p_uk2 IS NOT NULL AND
               dsym_dexh_id = p_uk2) OR
              (p_uk2 IS NULL AND
               dsym_dexh_id IS NULL));
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN d_symbols.dsym_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE d_symbols
         SET 
             dsym_dexh_id = CASE NVL(p_ignore.dsym_dexh_id, a_data.gFalse) WHEN a_data.gTrue THEN dsym_dexh_id ELSE p_row.dsym_dexh_id END,
             dsym_code = CASE NVL(p_ignore.dsym_code, a_data.gFalse) WHEN a_data.gTrue THEN dsym_code ELSE p_row.dsym_code END,
             dsym_name = CASE NVL(p_ignore.dsym_name, a_data.gFalse) WHEN a_data.gTrue THEN dsym_name ELSE p_row.dsym_name END,
             dsym_dsyg_id = CASE NVL(p_ignore.dsym_dsyg_id, a_data.gFalse) WHEN a_data.gTrue THEN dsym_dsyg_id ELSE p_row.dsym_dsyg_id END,
             dsym_dmon_id = CASE NVL(p_ignore.dsym_dmon_id, a_data.gFalse) WHEN a_data.gTrue THEN dsym_dmon_id ELSE p_row.dsym_dmon_id END,
             dsym_year = CASE NVL(p_ignore.dsym_year, a_data.gFalse) WHEN a_data.gTrue THEN dsym_year ELSE p_row.dsym_year END,
             dsym_decimals = CASE NVL(p_ignore.dsym_decimals, a_data.gFalse) WHEN a_data.gTrue THEN dsym_decimals ELSE p_row.dsym_decimals END
         WHERE dsym_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN d_symbols.dsym_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE d_symbols
       WHERE dsym_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN d_symbols%ROWTYPE, p_newPk OUT d_symbols.dsym_id%TYPE) IS
    l_pk    d_symbols.dsym_id%TYPE;
  BEGIN 
    INSERT
      INTO d_symbols
    VALUES p_row
    RETURNING dsym_id
         INTO p_newPk;
  END doInsert;

END t_DSymbols;
/
