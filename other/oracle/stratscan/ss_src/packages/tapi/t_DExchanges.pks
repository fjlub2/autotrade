CREATE OR REPLACE PACKAGE SS_SRC.t_DExchanges IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for d_exchanges table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF d_exchanges%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              dexh_code   d_exchanges.dexh_code%TYPE,
                              dexh_name   d_exchanges.dexh_name%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              dexh_code   VARCHAR2(1),
                              dexh_name   VARCHAR2(1));

 /* Returns a row from the D_EXCHANGES table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN d_exchanges.dexh_id%TYPE) RETURN d_exchanges%ROWTYPE;

 /* Pipes a row from the D_EXCHANGES table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN d_exchanges.dexh_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the D_EXCHANGES table for the supplied PK */
  FUNCTION exists_PK (p_pk IN d_exchanges.dexh_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the D_EXCHANGES table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN d_exchanges.dexh_code%TYPE) RETURN d_exchanges%ROWTYPE;

 /* Pipes a row from the D_EXCHANGES table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN d_exchanges.dexh_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the D_EXCHANGES table for the supplied UK */
  FUNCTION exists_UK (p_uk IN d_exchanges.dexh_code%TYPE) RETURN VARCHAR2;

 /* Returns DEXH_ID from the D_EXCHANGES table for the supplied PK */
  FUNCTION getId_PK (p_pk IN d_exchanges.dexh_id%TYPE) RETURN d_exchanges.dexh_id%TYPE;

 /* Returns DEXH_ID from the D_EXCHANGES table for the supplied UK */
  FUNCTION getId_UK (p_uk IN d_exchanges.dexh_code%TYPE) RETURN d_exchanges.dexh_id%TYPE;

 /* Returns DEXH_CODE from the D_EXCHANGES table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN d_exchanges.dexh_id%TYPE) RETURN d_exchanges.dexh_code%TYPE;

 /* Returns DEXH_CODE from the D_EXCHANGES table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN d_exchanges.dexh_code%TYPE) RETURN d_exchanges.dexh_code%TYPE;

 /* Returns DEXH_NAME from the D_EXCHANGES table for the supplied PK */
  FUNCTION getName_PK (p_pk IN d_exchanges.dexh_id%TYPE) RETURN d_exchanges.dexh_name%TYPE;

 /* Returns DEXH_NAME from the D_EXCHANGES table for the supplied UK */
  FUNCTION getName_UK (p_uk IN d_exchanges.dexh_code%TYPE) RETURN d_exchanges.dexh_name%TYPE;

 /* Updates D_EXCHANGES.DEXH_CODE to the supplied value for the supplied PK */
  PROCEDURE setCode (p_pk IN d_exchanges.dexh_id%TYPE, p_val IN d_exchanges.dexh_code%TYPE);

 /* Updates D_EXCHANGES.DEXH_NAME to the supplied value for the supplied PK */
  PROCEDURE setName (p_pk IN d_exchanges.dexh_id%TYPE, p_val IN d_exchanges.dexh_name%TYPE);

 /* Updates a row on the D_EXCHANGES table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN d_exchanges.dexh_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the D_EXCHANGES table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN d_exchanges.dexh_id%TYPE, 
                      p_code IN d_exchanges.dexh_code%TYPE,
                      p_name IN d_exchanges.dexh_name%TYPE );

 /* Inserts a row into the D_EXCHANGES table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN d_exchanges%ROWTYPE, p_newPk OUT d_exchanges.dexh_id%TYPE);

 /* Inserts a row into the D_EXCHANGES table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN d_exchanges.dexh_id%TYPE,
                      p_code IN d_exchanges.dexh_code%TYPE,
                      p_name IN d_exchanges.dexh_name%TYPE, p_newPk OUT d_exchanges.dexh_id%TYPE);

 /* Deletes a row from the D_EXCHANGES table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN d_exchanges.dexh_id%TYPE);

END t_DExchanges;
/
