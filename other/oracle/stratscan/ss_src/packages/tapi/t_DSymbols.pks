CREATE OR REPLACE PACKAGE SS_SRC.t_DSymbols IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for d_symbols table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF d_symbols%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              dsym_dexh_id   d_symbols.dsym_dexh_id%TYPE,
                              dsym_code   d_symbols.dsym_code%TYPE,
                              dsym_name   d_symbols.dsym_name%TYPE,
                              dsym_dsyg_id   d_symbols.dsym_dsyg_id%TYPE,
                              dsym_dmon_id   d_symbols.dsym_dmon_id%TYPE,
                              dsym_year   d_symbols.dsym_year%TYPE,
                              dsym_decimals   d_symbols.dsym_decimals%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              dsym_dexh_id   VARCHAR2(1),
                              dsym_code   VARCHAR2(1),
                              dsym_name   VARCHAR2(1),
                              dsym_dsyg_id   VARCHAR2(1),
                              dsym_dmon_id   VARCHAR2(1),
                              dsym_year   VARCHAR2(1),
                              dsym_decimals   VARCHAR2(1));

 /* Returns a row from the D_SYMBOLS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols%ROWTYPE;

 /* Pipes a row from the D_SYMBOLS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the D_SYMBOLS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the D_SYMBOLS table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols%ROWTYPE;

 /* Pipes a row from the D_SYMBOLS table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the D_SYMBOLS table for the supplied UK */
  FUNCTION exists_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN VARCHAR2;

 /* Returns DSYM_ID from the D_SYMBOLS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_id%TYPE;

 /* Returns DSYM_ID from the D_SYMBOLS table for the supplied UK */
  FUNCTION getId_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_id%TYPE;

 /* Returns DSYM_DEXH_ID from the D_SYMBOLS table for the supplied PK */
  FUNCTION getDexhId_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_dexh_id%TYPE;

 /* Returns DSYM_DEXH_ID from the D_SYMBOLS table for the supplied UK */
  FUNCTION getDexhId_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_dexh_id%TYPE;

 /* Returns DSYM_CODE from the D_SYMBOLS table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_code%TYPE;

 /* Returns DSYM_CODE from the D_SYMBOLS table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_code%TYPE;

 /* Returns DSYM_NAME from the D_SYMBOLS table for the supplied PK */
  FUNCTION getName_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_name%TYPE;

 /* Returns DSYM_NAME from the D_SYMBOLS table for the supplied UK */
  FUNCTION getName_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_name%TYPE;

 /* Returns DSYM_DSYG_ID from the D_SYMBOLS table for the supplied PK */
  FUNCTION getDsygId_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_dsyg_id%TYPE;

 /* Returns DSYM_DSYG_ID from the D_SYMBOLS table for the supplied UK */
  FUNCTION getDsygId_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_dsyg_id%TYPE;

 /* Returns DSYM_DMON_ID from the D_SYMBOLS table for the supplied PK */
  FUNCTION getDmonId_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_dmon_id%TYPE;

 /* Returns DSYM_DMON_ID from the D_SYMBOLS table for the supplied UK */
  FUNCTION getDmonId_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_dmon_id%TYPE;

 /* Returns DSYM_YEAR from the D_SYMBOLS table for the supplied PK */
  FUNCTION getYear_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_year%TYPE;

 /* Returns DSYM_YEAR from the D_SYMBOLS table for the supplied UK */
  FUNCTION getYear_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_year%TYPE;

 /* Returns DSYM_DECIMALS from the D_SYMBOLS table for the supplied PK */
  FUNCTION getDecimals_PK (p_pk IN d_symbols.dsym_id%TYPE) RETURN d_symbols.dsym_decimals%TYPE;

 /* Returns DSYM_DECIMALS from the D_SYMBOLS table for the supplied UK */
  FUNCTION getDecimals_UK (p_uk IN d_symbols.dsym_code%TYPE, p_uk2 IN d_symbols.dsym_dexh_id%TYPE) RETURN d_symbols.dsym_decimals%TYPE;

 /* Updates D_SYMBOLS.DSYM_DEXH_ID to the supplied value for the supplied PK */
  PROCEDURE setDexhId (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_dexh_id%TYPE);

 /* Updates D_SYMBOLS.DSYM_CODE to the supplied value for the supplied PK */
  PROCEDURE setCode (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_code%TYPE);

 /* Updates D_SYMBOLS.DSYM_NAME to the supplied value for the supplied PK */
  PROCEDURE setName (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_name%TYPE);

 /* Updates D_SYMBOLS.DSYM_DSYG_ID to the supplied value for the supplied PK */
  PROCEDURE setDsygId (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_dsyg_id%TYPE);

 /* Updates D_SYMBOLS.DSYM_DMON_ID to the supplied value for the supplied PK */
  PROCEDURE setDmonId (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_dmon_id%TYPE);

 /* Updates D_SYMBOLS.DSYM_YEAR to the supplied value for the supplied PK */
  PROCEDURE setYear (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_year%TYPE);

 /* Updates D_SYMBOLS.DSYM_DECIMALS to the supplied value for the supplied PK */
  PROCEDURE setDecimals (p_pk IN d_symbols.dsym_id%TYPE, p_val IN d_symbols.dsym_decimals%TYPE);

 /* Updates a row on the D_SYMBOLS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN d_symbols.dsym_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the D_SYMBOLS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN d_symbols.dsym_id%TYPE, 
                      p_dexh_id IN d_symbols.dsym_dexh_id%TYPE,
                      p_code IN d_symbols.dsym_code%TYPE,
                      p_name IN d_symbols.dsym_name%TYPE,
                      p_dsyg_id IN d_symbols.dsym_dsyg_id%TYPE,
                      p_dmon_id IN d_symbols.dsym_dmon_id%TYPE,
                      p_year IN d_symbols.dsym_year%TYPE,
                      p_decimals IN d_symbols.dsym_decimals%TYPE );

 /* Inserts a row into the D_SYMBOLS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN d_symbols%ROWTYPE, p_newPk OUT d_symbols.dsym_id%TYPE);

 /* Inserts a row into the D_SYMBOLS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN d_symbols.dsym_id%TYPE,
                      p_dexh_id IN d_symbols.dsym_dexh_id%TYPE,
                      p_code IN d_symbols.dsym_code%TYPE,
                      p_name IN d_symbols.dsym_name%TYPE,
                      p_dsyg_id IN d_symbols.dsym_dsyg_id%TYPE,
                      p_dmon_id IN d_symbols.dsym_dmon_id%TYPE,
                      p_year IN d_symbols.dsym_year%TYPE,
                      p_decimals IN d_symbols.dsym_decimals%TYPE, p_newPk OUT d_symbols.dsym_id%TYPE);

 /* Deletes a row from the D_SYMBOLS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN d_symbols.dsym_id%TYPE);

END t_DSymbols;
/
