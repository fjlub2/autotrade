CREATE OR REPLACE PACKAGE BODY SS_SRC.t_DMonths IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN d_months.dmon_id%TYPE, p_uk IN d_months.dmon_code%TYPE DEFAULT NULL) RETURN d_months%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN d_months.dmon_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN d_months.dmon_id%TYPE);

  PROCEDURE doInsert (p_row IN d_months%ROWTYPE, p_newPk OUT d_months.dmon_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN d_months.dmon_id%TYPE) RETURN d_months%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN d_months.dmon_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN d_months.dmon_id%TYPE) RETURN VARCHAR2 IS
    l_row    d_months%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.dmon_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN d_months.dmon_code%TYPE) RETURN d_months%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN d_months.dmon_code%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN d_months.dmon_code%TYPE) RETURN VARCHAR2 IS
    l_row    d_months%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.dmon_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN d_months.dmon_id%TYPE) RETURN d_months.dmon_id%TYPE IS
    l_row    d_months%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dmon_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN d_months.dmon_code%TYPE) RETURN d_months.dmon_id%TYPE IS
    l_row    d_months%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.dmon_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_PK (p_pk IN d_months.dmon_id%TYPE) RETURN d_months.dmon_code%TYPE IS
    l_row    d_months%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dmon_code;
  END getCode_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getCode_UK (p_uk IN d_months.dmon_code%TYPE) RETURN d_months.dmon_code%TYPE IS
    l_row    d_months%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.dmon_code;
  END getCode_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMm_PK (p_pk IN d_months.dmon_id%TYPE) RETURN d_months.dmon_mm%TYPE IS
    l_row    d_months%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.dmon_mm;
  END getMm_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMm_UK (p_uk IN d_months.dmon_code%TYPE) RETURN d_months.dmon_mm%TYPE IS
    l_row    d_months%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.dmon_mm;
  END getMm_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setCode (p_pk IN d_months.dmon_id%TYPE, p_val IN d_months.dmon_code%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.dmon_code := a_data.gFalse;
    l_ignoreRow.dmon_mm := a_data.gTrue;

    l_updateRow.dmon_code := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setCode;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setMm (p_pk IN d_months.dmon_id%TYPE, p_val IN d_months.dmon_mm%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.dmon_code := a_data.gTrue;
    l_ignoreRow.dmon_mm := a_data.gFalse;

    l_updateRow.dmon_mm := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setMm;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN d_months.dmon_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN d_months.dmon_id%TYPE, 
                      p_code IN d_months.dmon_code%TYPE,
                      p_mm IN d_months.dmon_mm%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.dmon_code := p_code;
    l_row.dmon_mm := p_mm;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN d_months%ROWTYPE, p_newPK OUT d_months.dmon_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN d_months.dmon_id%TYPE,
                      p_code IN d_months.dmon_code%TYPE,
                      p_mm IN d_months.dmon_mm%TYPE, p_newPK OUT d_months.dmon_id%TYPE) IS
    l_row   d_months%ROWTYPE;
  BEGIN
    l_row.dmon_id := p_id;
    l_row.dmon_code := p_code;
    l_row.dmon_mm := p_mm;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN d_months.dmon_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN d_months.dmon_id%TYPE, p_uk IN d_months.dmon_code%TYPE DEFAULT NULL) RETURN d_months%ROWTYPE IS
    l_row    d_months%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM d_months
       WHERE dmon_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM d_months
       WHERE dmon_code = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN d_months.dmon_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE d_months
         SET 
             dmon_code = CASE NVL(p_ignore.dmon_code, a_data.gFalse) WHEN a_data.gTrue THEN dmon_code ELSE p_row.dmon_code END,
             dmon_mm = CASE NVL(p_ignore.dmon_mm, a_data.gFalse) WHEN a_data.gTrue THEN dmon_mm ELSE p_row.dmon_mm END
         WHERE dmon_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN d_months.dmon_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE d_months
       WHERE dmon_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN d_months%ROWTYPE, p_newPk OUT d_months.dmon_id%TYPE) IS
    l_pk    d_months.dmon_id%TYPE;
  BEGIN 
    INSERT
      INTO d_months
    VALUES p_row
    RETURNING dmon_id
         INTO p_newPk;
  END doInsert;

END t_DMonths;
/
