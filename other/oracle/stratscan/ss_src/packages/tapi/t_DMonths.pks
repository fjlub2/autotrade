CREATE OR REPLACE PACKAGE SS_SRC.t_DMonths IS 
/* ----------------------------------------------------------------------------------------------------------
   Table API (TAPI) for d_months table

   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.
   Should also not be present in the live database.

   Auto generated package.
   ---------------------------------------------------------------------------------------------------------- */

 /* Type that is used to pipe out a row */
  TYPE g_Rows IS TABLE OF d_months%ROWTYPE;

 /* Type that is to be used when updating a row. Same as g_Rows but without the PK */
  TYPE g_updateRow IS RECORD (
                              dmon_code   d_months.dmon_code%TYPE,
                              dmon_mm   d_months.dmon_mm%TYPE);

 /* Type that is to be used to indicate which columns should not be updated when updating a row.
    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */
  TYPE g_updateIgnoreRow IS RECORD (
                              dmon_code   VARCHAR2(1),
                              dmon_mm   VARCHAR2(1));

 /* Returns a row from the D_MONTHS table for the supplied PK */
  FUNCTION getRow_PK (p_pk IN d_months.dmon_id%TYPE) RETURN d_months%ROWTYPE;

 /* Pipes a row from the D_MONTHS table for the supplied PK */
  FUNCTION pipeRow_PK (p_pk IN d_months.dmon_id%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the D_MONTHS table for the supplied PK */
  FUNCTION exists_PK (p_pk IN d_months.dmon_id%TYPE) RETURN VARCHAR2;

 /* Returns a row from the D_MONTHS table for the supplied UK */
  FUNCTION getRow_UK (p_uk IN d_months.dmon_code%TYPE) RETURN d_months%ROWTYPE;

 /* Pipes a row from the D_MONTHS table for the supplied UK */
  FUNCTION pipeRow_UK (p_uk IN d_months.dmon_code%TYPE) RETURN g_Rows PIPELINED;

 /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the D_MONTHS table for the supplied UK */
  FUNCTION exists_UK (p_uk IN d_months.dmon_code%TYPE) RETURN VARCHAR2;

 /* Returns DMON_ID from the D_MONTHS table for the supplied PK */
  FUNCTION getId_PK (p_pk IN d_months.dmon_id%TYPE) RETURN d_months.dmon_id%TYPE;

 /* Returns DMON_ID from the D_MONTHS table for the supplied UK */
  FUNCTION getId_UK (p_uk IN d_months.dmon_code%TYPE) RETURN d_months.dmon_id%TYPE;

 /* Returns DMON_CODE from the D_MONTHS table for the supplied PK */
  FUNCTION getCode_PK (p_pk IN d_months.dmon_id%TYPE) RETURN d_months.dmon_code%TYPE;

 /* Returns DMON_CODE from the D_MONTHS table for the supplied UK */
  FUNCTION getCode_UK (p_uk IN d_months.dmon_code%TYPE) RETURN d_months.dmon_code%TYPE;

 /* Returns DMON_MM from the D_MONTHS table for the supplied PK */
  FUNCTION getMm_PK (p_pk IN d_months.dmon_id%TYPE) RETURN d_months.dmon_mm%TYPE;

 /* Returns DMON_MM from the D_MONTHS table for the supplied UK */
  FUNCTION getMm_UK (p_uk IN d_months.dmon_code%TYPE) RETURN d_months.dmon_mm%TYPE;

 /* Updates D_MONTHS.DMON_CODE to the supplied value for the supplied PK */
  PROCEDURE setCode (p_pk IN d_months.dmon_id%TYPE, p_val IN d_months.dmon_code%TYPE);

 /* Updates D_MONTHS.DMON_MM to the supplied value for the supplied PK */
  PROCEDURE setMm (p_pk IN d_months.dmon_id%TYPE, p_val IN d_months.dmon_mm%TYPE);

 /* Updates a row on the D_MONTHS table using the supplied row type for the supplied PK.
    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default
    if you want all columns to be updated. */
  PROCEDURE updateRow (p_pk IN d_months.dmon_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);

 /* Updates a row on the D_MONTHS table using the supplied values for the supplied PK.
    All columns will be updated, even those supplied as NULL */
  PROCEDURE updateRow (p_pk IN d_months.dmon_id%TYPE, 
                      p_code IN d_months.dmon_code%TYPE,
                      p_mm IN d_months.dmon_mm%TYPE );

 /* Inserts a row into the D_MONTHS table using the supplied row type and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (p_row IN d_months%ROWTYPE, p_newPk OUT d_months.dmon_id%TYPE);

 /* Inserts a row into the D_MONTHS table using the supplied values and returns the new PK.
    Tables should have a trigger to populate the PK if it is supplied as NULL
    Column defaults will not be set for any columns supplied as NULL. */
  PROCEDURE insertRow (
                      p_id IN d_months.dmon_id%TYPE,
                      p_code IN d_months.dmon_code%TYPE,
                      p_mm IN d_months.dmon_mm%TYPE, p_newPk OUT d_months.dmon_id%TYPE);

 /* Deletes a row from the D_MONTHS table for the supplied PK */
  PROCEDURE deleteRow (p_pk IN d_months.dmon_id%TYPE);

END t_DMonths;
/
