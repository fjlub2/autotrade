CREATE OR REPLACE PACKAGE BODY SS_SRC.t_DDates IS 

 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN d_dates.ddat_id%TYPE, p_uk IN d_dates.ddat_date%TYPE DEFAULT NULL) RETURN d_dates%ROWTYPE;

  PROCEDURE doUpdate (p_pk IN d_dates.ddat_id%TYPE, p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);

  PROCEDURE doDelete (p_pk IN d_dates.ddat_id%TYPE);

  PROCEDURE doInsert (p_row IN d_dates%ROWTYPE, p_newPk OUT d_dates.ddat_id%TYPE);

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Getters - get values for supplied PK or UK
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => p_pk);
  END getRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_PK(p_pk => p_pk));

    RETURN;
  END pipeRow_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN VARCHAR2 IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    IF l_row.ddat_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getRow_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates%ROWTYPE IS
  BEGIN
    RETURN getRow(p_pk => NULL, p_uk => p_uk);
  END getRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION pipeRow_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN g_Rows PIPELINED IS
  BEGIN
    PIPE ROW (getRow_UK(p_uk => p_uk));

    RETURN;
  END pipeRow_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION exists_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN VARCHAR2 IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    IF l_row.ddat_id IS NOT NULL THEN
      RETURN a_data.gTrue;
    ELSE
      RETURN a_data.gFalse;
    END IF;
  END exists_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_id%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ddat_id;
  END getId_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getId_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_id%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ddat_id;
  END getId_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDate_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_date%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ddat_date;
  END getDate_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDate_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_date%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ddat_date;
  END getDate_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDay_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_day%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ddat_day;
  END getDay_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getDay_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_day%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ddat_day;
  END getDay_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getWeek_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_week%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ddat_week;
  END getWeek_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getWeek_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_week%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ddat_week;
  END getWeek_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMonth_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_month%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ddat_month;
  END getMonth_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getMonth_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_month%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ddat_month;
  END getMonth_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getQuarter_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_quarter%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ddat_quarter;
  END getQuarter_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getQuarter_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_quarter%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ddat_quarter;
  END getQuarter_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getYear_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_year%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ddat_year;
  END getYear_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getYear_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_year%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ddat_year;
  END getYear_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getWe_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_we%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ddat_we;
  END getWe_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getWe_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_we%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ddat_we;
  END getWe_UK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getWd_PK (p_pk IN d_dates.ddat_id%TYPE) RETURN d_dates.ddat_wd%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_PK(p_pk => p_pk);

    RETURN l_row.ddat_wd;
  END getWd_PK;

  ----------------------------------------------------------------------------------------------------------------

  FUNCTION getWd_UK (p_uk IN d_dates.ddat_date%TYPE) RETURN d_dates.ddat_wd%TYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN
    l_row := getRow_UK(p_uk => p_uk);

    RETURN l_row.ddat_wd;
  END getWd_UK;

 -- ----------------------------------------------------------------------------------------------------------
 -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed
 -- ----------------------------------------------------------------------------------------------------------

  PROCEDURE setDate (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_date%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ddat_date := a_data.gFalse;
    l_ignoreRow.ddat_day := a_data.gTrue;
    l_ignoreRow.ddat_week := a_data.gTrue;
    l_ignoreRow.ddat_month := a_data.gTrue;
    l_ignoreRow.ddat_quarter := a_data.gTrue;
    l_ignoreRow.ddat_year := a_data.gTrue;
    l_ignoreRow.ddat_we := a_data.gTrue;
    l_ignoreRow.ddat_wd := a_data.gTrue;

    l_updateRow.ddat_date := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setDay (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_day%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ddat_date := a_data.gTrue;
    l_ignoreRow.ddat_day := a_data.gFalse;
    l_ignoreRow.ddat_week := a_data.gTrue;
    l_ignoreRow.ddat_month := a_data.gTrue;
    l_ignoreRow.ddat_quarter := a_data.gTrue;
    l_ignoreRow.ddat_year := a_data.gTrue;
    l_ignoreRow.ddat_we := a_data.gTrue;
    l_ignoreRow.ddat_wd := a_data.gTrue;

    l_updateRow.ddat_day := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setDay;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setWeek (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_week%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ddat_date := a_data.gTrue;
    l_ignoreRow.ddat_day := a_data.gTrue;
    l_ignoreRow.ddat_week := a_data.gFalse;
    l_ignoreRow.ddat_month := a_data.gTrue;
    l_ignoreRow.ddat_quarter := a_data.gTrue;
    l_ignoreRow.ddat_year := a_data.gTrue;
    l_ignoreRow.ddat_we := a_data.gTrue;
    l_ignoreRow.ddat_wd := a_data.gTrue;

    l_updateRow.ddat_week := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setWeek;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setMonth (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_month%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ddat_date := a_data.gTrue;
    l_ignoreRow.ddat_day := a_data.gTrue;
    l_ignoreRow.ddat_week := a_data.gTrue;
    l_ignoreRow.ddat_month := a_data.gFalse;
    l_ignoreRow.ddat_quarter := a_data.gTrue;
    l_ignoreRow.ddat_year := a_data.gTrue;
    l_ignoreRow.ddat_we := a_data.gTrue;
    l_ignoreRow.ddat_wd := a_data.gTrue;

    l_updateRow.ddat_month := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setMonth;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setQuarter (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_quarter%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ddat_date := a_data.gTrue;
    l_ignoreRow.ddat_day := a_data.gTrue;
    l_ignoreRow.ddat_week := a_data.gTrue;
    l_ignoreRow.ddat_month := a_data.gTrue;
    l_ignoreRow.ddat_quarter := a_data.gFalse;
    l_ignoreRow.ddat_year := a_data.gTrue;
    l_ignoreRow.ddat_we := a_data.gTrue;
    l_ignoreRow.ddat_wd := a_data.gTrue;

    l_updateRow.ddat_quarter := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setQuarter;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setYear (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_year%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ddat_date := a_data.gTrue;
    l_ignoreRow.ddat_day := a_data.gTrue;
    l_ignoreRow.ddat_week := a_data.gTrue;
    l_ignoreRow.ddat_month := a_data.gTrue;
    l_ignoreRow.ddat_quarter := a_data.gTrue;
    l_ignoreRow.ddat_year := a_data.gFalse;
    l_ignoreRow.ddat_we := a_data.gTrue;
    l_ignoreRow.ddat_wd := a_data.gTrue;

    l_updateRow.ddat_year := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setYear;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setWe (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_we%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ddat_date := a_data.gTrue;
    l_ignoreRow.ddat_day := a_data.gTrue;
    l_ignoreRow.ddat_week := a_data.gTrue;
    l_ignoreRow.ddat_month := a_data.gTrue;
    l_ignoreRow.ddat_quarter := a_data.gTrue;
    l_ignoreRow.ddat_year := a_data.gTrue;
    l_ignoreRow.ddat_we := a_data.gFalse;
    l_ignoreRow.ddat_wd := a_data.gTrue;

    l_updateRow.ddat_we := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setWe;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE setWd (p_pk IN d_dates.ddat_id%TYPE, p_val IN d_dates.ddat_wd%TYPE) IS
    l_updateRow  g_updateRow       := NULL;
    l_ignoreRow  g_updateIgnoreRow := NULL;
  BEGIN 

    l_ignoreRow.ddat_date := a_data.gTrue;
    l_ignoreRow.ddat_day := a_data.gTrue;
    l_ignoreRow.ddat_week := a_data.gTrue;
    l_ignoreRow.ddat_month := a_data.gTrue;
    l_ignoreRow.ddat_quarter := a_data.gTrue;
    l_ignoreRow.ddat_year := a_data.gTrue;
    l_ignoreRow.ddat_we := a_data.gTrue;
    l_ignoreRow.ddat_wd := a_data.gFalse;

    l_updateRow.ddat_wd := p_val;

    doUpdate(p_pk => p_pk, p_row => l_updateRow, p_ignore => l_ignoreRow);
  END setWd;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN d_dates.ddat_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS
  BEGIN
    doUpdate(p_pk => p_pk, p_row => p_row, p_ignore => p_ignore);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE updateRow (p_pk IN d_dates.ddat_id%TYPE, 
                      p_date IN d_dates.ddat_date%TYPE,
                      p_day IN d_dates.ddat_day%TYPE,
                      p_week IN d_dates.ddat_week%TYPE,
                      p_month IN d_dates.ddat_month%TYPE,
                      p_quarter IN d_dates.ddat_quarter%TYPE,
                      p_year IN d_dates.ddat_year%TYPE,
                      p_we IN d_dates.ddat_we%TYPE,
                      p_wd IN d_dates.ddat_wd%TYPE ) IS
    l_row        g_updateRow := NULL;
  BEGIN
    l_row.ddat_date := p_date;
    l_row.ddat_day := p_day;
    l_row.ddat_week := p_week;
    l_row.ddat_month := p_month;
    l_row.ddat_quarter := p_quarter;
    l_row.ddat_year := p_year;
    l_row.ddat_we := p_we;
    l_row.ddat_wd := p_wd;

    doUpdate(p_pk => p_pk, p_row => l_row, p_ignore => NULL);
  END updateRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (p_row IN d_dates%ROWTYPE, p_newPK OUT d_dates.ddat_id%TYPE) IS
  BEGIN
    doInsert(p_row => p_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE insertRow (
                      p_id IN d_dates.ddat_id%TYPE,
                      p_date IN d_dates.ddat_date%TYPE,
                      p_day IN d_dates.ddat_day%TYPE,
                      p_week IN d_dates.ddat_week%TYPE,
                      p_month IN d_dates.ddat_month%TYPE,
                      p_quarter IN d_dates.ddat_quarter%TYPE,
                      p_year IN d_dates.ddat_year%TYPE,
                      p_we IN d_dates.ddat_we%TYPE,
                      p_wd IN d_dates.ddat_wd%TYPE, p_newPK OUT d_dates.ddat_id%TYPE) IS
    l_row   d_dates%ROWTYPE;
  BEGIN
    l_row.ddat_id := p_id;
    l_row.ddat_date := p_date;
    l_row.ddat_day := p_day;
    l_row.ddat_week := p_week;
    l_row.ddat_month := p_month;
    l_row.ddat_quarter := p_quarter;
    l_row.ddat_year := p_year;
    l_row.ddat_we := p_we;
    l_row.ddat_wd := p_wd;

    doInsert(p_row => l_row, p_newPk => p_newPk);
  END insertRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE deleteRow (p_pk IN d_dates.ddat_id%TYPE) IS
  BEGIN
    doDelete(p_pk => p_pk);
  END deleteRow;

 -- ----------------------------------------------------------------------------------------------------------
 -- Private functions / procedures
 -- ----------------------------------------------------------------------------------------------------------

  FUNCTION getRow (p_pk IN d_dates.ddat_id%TYPE, p_uk IN d_dates.ddat_date%TYPE DEFAULT NULL) RETURN d_dates%ROWTYPE IS
    l_row    d_dates%ROWTYPE;
  BEGIN 
    IF p_pk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM d_dates
       WHERE ddat_id = p_pk;
    ELSIF p_uk IS NOT NULL THEN
      SELECT *
        INTO l_row
        FROM d_dates
       WHERE ddat_date = p_uk;
    END IF;

    RETURN l_row;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END getRow;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doUpdate (p_pk IN d_dates.ddat_id%TYPE, p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      UPDATE d_dates
         SET 
             ddat_date = CASE NVL(p_ignore.ddat_date, a_data.gFalse) WHEN a_data.gTrue THEN ddat_date ELSE p_row.ddat_date END,
             ddat_day = CASE NVL(p_ignore.ddat_day, a_data.gFalse) WHEN a_data.gTrue THEN ddat_day ELSE p_row.ddat_day END,
             ddat_week = CASE NVL(p_ignore.ddat_week, a_data.gFalse) WHEN a_data.gTrue THEN ddat_week ELSE p_row.ddat_week END,
             ddat_month = CASE NVL(p_ignore.ddat_month, a_data.gFalse) WHEN a_data.gTrue THEN ddat_month ELSE p_row.ddat_month END,
             ddat_quarter = CASE NVL(p_ignore.ddat_quarter, a_data.gFalse) WHEN a_data.gTrue THEN ddat_quarter ELSE p_row.ddat_quarter END,
             ddat_year = CASE NVL(p_ignore.ddat_year, a_data.gFalse) WHEN a_data.gTrue THEN ddat_year ELSE p_row.ddat_year END,
             ddat_we = CASE NVL(p_ignore.ddat_we, a_data.gFalse) WHEN a_data.gTrue THEN ddat_we ELSE p_row.ddat_we END,
             ddat_wd = CASE NVL(p_ignore.ddat_wd, a_data.gFalse) WHEN a_data.gTrue THEN ddat_wd ELSE p_row.ddat_wd END
         WHERE ddat_id = p_pk;
    END IF;
  END doUpdate;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doDelete (p_pk IN d_dates.ddat_id%TYPE) IS
  BEGIN 
    IF p_pk IS NOT NULL THEN
      DELETE d_dates
       WHERE ddat_id = p_pk;
    END IF;
  END doDelete;

  ----------------------------------------------------------------------------------------------------------------

  PROCEDURE doInsert (p_row IN d_dates%ROWTYPE, p_newPk OUT d_dates.ddat_id%TYPE) IS
    l_pk    d_dates.ddat_id%TYPE;
  BEGIN 
    INSERT
      INTO d_dates
    VALUES p_row
    RETURNING ddat_id
         INTO p_newPk;
  END doInsert;

END t_DDates;
/
