GRANT SELECT, INSERT, UPDATE, DELETE ON D_DATES TO SS_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON D_EXCHANGES TO SS_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON D_MONTHS TO SS_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON D_SUPPLIERS TO SS_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON D_SYMBOLS TO SS_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON D_SYMBOL_GROUPS TO SS_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON D_SYMBOL_TIMEFRAMES TO SS_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON D_TIMEFRAMES TO SS_SRC;
GRANT SELECT, INSERT, UPDATE, DELETE ON F_DATA TO SS_SRC;

GRANT EXECUTE ON TT_STUDYDATA TO SS_SRC;
GRANT EXECUTE ON OT_STUDYDATA TO SS_SRC;
GRANT EXECUTE ON TT_STUDYMACD TO SS_SRC;
GRANT EXECUTE ON OT_STUDYMACD TO SS_SRC;