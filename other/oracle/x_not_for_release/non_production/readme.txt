The files in here should not be applied to a production environment.

This code is purely used to generate some common code, e.g. Table API's, Views, Code MV's.

The code should be applied only to data schemas.

After completion, this generation code should ideally be dropped - run script remove.sql to remove all of the objects.

