CREATE OR REPLACE PACKAGE ddlUtils AS
  /* Performs DDL functions - mainly generating code in a development environment
  
     Install in the DATA schema.
            
            ==========================================================
            *********** DO NOT INSTALL IN LIVE ENVIRONMENT ***********
            ==========================================================
     
     Any generated code will be created in the DATA_PUMP_DIR directory. 
     You'll need to manually grant write access to that directory to DAT 
     as it's not something that should be done in prod */
     
  /* Creates a table API package for the supplied table - apply in the SRC schma */
  PROCEDURE createTableAPI (p_table    IN VARCHAR2,
                            p_alias    IN VARCHAR2,
                            p_owner    IN VARCHAR2,
                            p_readOnly IN VARCHAR2 DEFAULT 'N');
                            
  /* Creates a read only table API package for the supplied table - apply in the SRC schma */
  PROCEDURE createTableAPI_RO (p_table  IN VARCHAR2,
                               p_alias  IN VARCHAR2,
                               p_owner  IN VARCHAR2);

  /* Creates a view for the supplied table - apply in the DATA schema */
  PROCEDURE createView (p_table  IN VARCHAR2,
                        p_owner  IN VARCHAR2);

END ddlUtils;
/
