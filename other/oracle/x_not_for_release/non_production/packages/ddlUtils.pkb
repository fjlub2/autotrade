CREATE OR REPLACE PACKAGE BODY ddlUtils AS
 -- ----------------------------------------------------------------------------------------------------------
 -- Forward declarations
 -- ----------------------------------------------------------------------------------------------------------
    PROCEDURE outputLine (p_handle IN UTL_FILE.FILE_TYPE,
                          p_line   IN VARCHAR2);
    
 -- ----------------------------------------------------------------------------------------------------------
 -- Public declarations
 -- ----------------------------------------------------------------------------------------------------------
  PROCEDURE createTableAPI (p_table    IN VARCHAR2,
                            p_alias    IN VARCHAR2,
                            p_owner    IN VARCHAR2,
                            p_readOnly IN VARCHAR2 DEFAULT 'N') IS
    l_line          VARCHAR2(32000);
    l_pkColName     VARCHAR2(40);
    l_pkColName2    VARCHAR2(40);
    l_ukColName     VARCHAR2(40);
    l_ukColName2    VARCHAR2(40);
    l_specHandle    UTL_FILE.FILE_TYPE;
    l_bodyHandle    UTL_FILE.FILE_TYPE;
    l_ignoreString  VARCHAR2(32000);
    l_updateParams  VARCHAR2(32000);
    l_updateCols    VARCHAR2(32000);
    l_insertParams  VARCHAR2(32000);
    l_insertCols    VARCHAR2(32000);
  BEGIN
    BEGIN
      SELECT LOWER(column_name)
        INTO l_pkColName
        FROM user_constraints    uc
          JOIN user_cons_columns ucc ON uc.constraint_name = ucc.constraint_name
       WHERE uc.table_name   = p_table
         AND constraint_type = 'P'
         AND NVL(position,1) = 1
         AND ROWNUM          = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_pkColName := NULL;
    END;
    
    IF l_pkColName IS NULL THEN
      RAISE NO_DATA_FOUND; -- No PK then exit
    END IF;
    
    IF l_pkColName IS NOT NULL THEN
      BEGIN
        SELECT LOWER(column_name)
          INTO l_pkColName2
          FROM user_constraints    uc
            JOIN user_cons_columns ucc ON uc.constraint_name = ucc.constraint_name
         WHERE uc.table_name   = p_table
           AND constraint_type = 'P'
           AND LOWER(column_name) != l_pkColName;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_pkColName2 := NULL;
        WHEN TOO_MANY_ROWS THEN -- don't want PK's with more than two column
          l_pkColName  := NULL;
          l_pkColName2 := NULL;
      END;
    END IF;

    BEGIN
      SELECT LOWER(column_name)
        INTO l_ukColName
        FROM user_constraints    uc
          JOIN user_cons_columns ucc ON uc.constraint_name = ucc.constraint_name
       WHERE uc.table_name   = p_table
         AND constraint_type = 'U'
         AND uc.constraint_name NOT LIKE 'SYS_%'
         AND NVL(position,1) = 1
         AND ROWNUM          = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_ukColName := NULL;
    END;
    
    IF l_ukColName IS NOT NULL THEN
      BEGIN
        SELECT LOWER(column_name)
          INTO l_ukColName2
          FROM user_constraints    uc
            JOIN user_cons_columns ucc ON uc.constraint_name = ucc.constraint_name
         WHERE uc.table_name   = p_table
           AND constraint_type = 'U'
           AND uc.constraint_name NOT LIKE 'SYS_%'
           AND LOWER(column_name) != l_ukColName;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_ukColName2 := NULL;
        WHEN TOO_MANY_ROWS THEN -- don't want UK's with more than two column
          l_ukColName  := NULL;
          l_ukColName2 := NULL;
      END;
    END IF;
    
    l_specHandle := UTL_FILE.FOPEN('DATA_PUMP_DIR', 't_'||REPLACE(INITCAP(p_table),'_')||'.pks', 'W');
    l_bodyHandle := UTL_FILE.FOPEN('DATA_PUMP_DIR', 't_'||REPLACE(INITCAP(p_table),'_')||'.pkb', 'W');

    outputLine(l_specHandle,'CREATE OR REPLACE PACKAGE '     ||p_owner||'.t_'||REPLACE(INITCAP(p_table),'_')||' IS ');
    outputLine(l_bodyHandle,'CREATE OR REPLACE PACKAGE BODY '||p_owner||'.t_'||REPLACE(INITCAP(p_table),'_')||' IS ');
    
    outputLine(l_specHandle,          '/* ----------------------------------------------------------------------------------------------------------'
                           ||CHR(10)||'   Table API (TAPI) for '||LOWER(p_table)||' table'
                           ||CHR(10)
                           ||CHR(10)||'   Should be applied on the SRC schema. No grants or synonyms should be applied to this package.'
                           ||CHR(10)||'   Should also not be present in the live database.'
                           ||CHR(10)
                           ||CHR(10)||'   Auto generated package.'
                           ||CHR(10)||'   ---------------------------------------------------------------------------------------------------------- */');
    
    outputLine(l_specHandle, CHR(10)||' /* Type that is used to pipe out a row */');
    outputLine(l_specHandle,          '  TYPE g_Rows IS TABLE OF '||LOWER(p_table)||'%ROWTYPE;');
    
    IF p_readOnly = 'N' THEN
      outputLine(l_specHandle, CHR(10)||' /* Type that is to be used when updating a row. Same as g_Rows but without the PK */');
      outputLine(l_specHandle,          '  TYPE g_updateRow IS RECORD (');
    
      l_line := NULL;
    
      FOR r_cols IN (SELECT LOWER(column_name) column_name
                       FROM user_tab_columns
                      WHERE table_name          = p_table
                        AND LOWER(column_name) NOT IN (l_pkColName,NVL(l_pkColName2,l_pkColName))
                      ORDER BY column_id) LOOP
        IF l_line IS NOT NULL THEN
          outputLine(l_specHandle, l_line||',');
        END IF;
      
        l_line := '                              '||r_cols.column_name||'   '||LOWER(p_table)||'.'||r_cols.column_name||'%TYPE';
      END LOOP;
      outputLine(l_specHandle, l_line||');');
    
      outputLine(l_specHandle, CHR(10)||' /* Type that is to be used to indicate which columns should not be updated when updating a row.');
      outputLine(l_specHandle,          '    Set to NULL or a_data.gFalse to update the column, a_data.gTrue to not update the column. */');
      outputLine(l_specHandle,          '  TYPE g_updateIgnoreRow IS RECORD (');
    
      l_line := NULL;
    
      FOR r_cols IN (SELECT LOWER(column_name) column_name
                       FROM user_tab_columns
                      WHERE table_name          = p_table
                        AND LOWER(column_name) NOT IN (l_pkColName,NVL(l_pkColName2,l_pkColName))
                      ORDER BY column_id) LOOP
        IF l_line IS NOT NULL THEN
          outputLine(l_specHandle, l_line||',');
        END IF;
      
        l_line := '                              '||r_cols.column_name||'   VARCHAR2(1)';
      END LOOP;
      outputLine(l_specHandle, l_line||');');
    END IF;
    
    -- forward declarations for private procedures / functions
    outputLine(l_bodyHandle, CHR(10)||' -- ----------------------------------------------------------------------------------------------------------');
    outputLine(l_bodyHandle,          ' -- Forward declarations');
    outputLine(l_bodyHandle,          ' -- ----------------------------------------------------------------------------------------------------------');
    
    outputLine(l_bodyHandle, CHR(10)||'  FUNCTION getRow (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||CASE WHEN l_ukColName IS NULL THEN NULL ELSE ', p_uk IN '||LOWER(p_table)||'.'||l_ukColName||'%TYPE DEFAULT NULL' END||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 IN '||LOWER(p_table)||'.'||l_ukColName2||'%TYPE DEFAULT NULL' END||') RETURN '||LOWER(p_table)||'%ROWTYPE;');
    
    IF p_readOnly = 'N' THEN
      outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE doUpdate (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||', p_row IN g_updateRow, p_ignore IN g_UpdateIgnoreRow);');
    
      outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE doDelete (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||');');
 
      outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE doInsert (p_row IN '||LOWER(p_table)||'%ROWTYPE, p_newPk OUT '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_newPk2 OUT '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||');');
    END IF;
    
    outputLine(l_bodyHandle, CHR(10)||' -- ----------------------------------------------------------------------------------------------------------');
    outputLine(l_bodyHandle,          ' -- Public Getters - get values for supplied PK or UK');
    outputLine(l_bodyHandle,          ' -- ----------------------------------------------------------------------------------------------------------');
    
    outputLine(l_specHandle, CHR(10)||' /* Returns a row from the '||p_table||' table for the supplied PK */');
    outputLine(l_specHandle,          '  FUNCTION getRow_PK (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') RETURN '||LOWER(p_table)||'%ROWTYPE;');
    outputLine(l_bodyHandle, CHR(10)||'  FUNCTION getRow_PK (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') RETURN '||LOWER(p_table)||'%ROWTYPE IS'
                           ||CHR(10)||'  BEGIN'
                           ||CHR(10)||'    RETURN getRow(p_pk => p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 => p_pk2' END||');'
                           ||CHR(10)||'  END getRow_PK;');
    outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
    
    outputLine(l_specHandle, CHR(10)||' /* Pipes a row from the '||p_table||' table for the supplied PK */');
    outputLine(l_specHandle,          '  FUNCTION pipeRow_PK (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') RETURN g_Rows PIPELINED;');
    outputLine(l_bodyHandle, CHR(10)||'  FUNCTION pipeRow_PK (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') RETURN g_Rows PIPELINED IS'
                           ||CHR(10)||'  BEGIN'
                           ||CHR(10)||'    PIPE ROW (getRow_PK(p_pk => p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 => p_pk2' END||'));'
                           ||CHR(10)
                           ||CHR(10)||'    RETURN;'
                           ||CHR(10)||'  END pipeRow_PK;');
    outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
      
    outputLine(l_specHandle, CHR(10)||' /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the '||p_table||' table for the supplied PK */');
    outputLine(l_specHandle,          '  FUNCTION exists_PK (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') RETURN VARCHAR2;');
    outputLine(l_bodyHandle, CHR(10)||'  FUNCTION exists_PK (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') RETURN VARCHAR2 IS'
                           ||CHR(10)||'    l_row    '||LOWER(p_table)||'%ROWTYPE;'
                           ||CHR(10)||'  BEGIN'
                           ||CHR(10)||'    l_row := getRow_PK(p_pk => p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 => p_pk2' END||');'
                           ||CHR(10)
                           ||CHR(10)||'    IF l_row.'||l_pkColName||' IS NOT NULL THEN'
                           ||CHR(10)||'      RETURN a_data.gTrue;'
                           ||CHR(10)||'    ELSE'
                           ||CHR(10)||'      RETURN a_data.gFalse;'
                           ||CHR(10)||'    END IF;'
                           ||CHR(10)||'  END exists_PK;');
    
    IF l_ukColName IS NOT NULL THEN
      outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
      outputLine(l_specHandle, CHR(10)||' /* Returns a row from the '||p_table||' table for the supplied UK */');
      outputLine(l_specHandle,          '  FUNCTION getRow_UK (p_uk IN '||LOWER(p_table)||'.'||l_ukColName||'%TYPE'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 IN '||LOWER(p_table)||'.'||l_ukColName2||'%TYPE'END||') RETURN '||LOWER(p_table)||'%ROWTYPE;');
      outputLine(l_bodyHandle, CHR(10)||'  FUNCTION getRow_UK (p_uk IN '||LOWER(p_table)||'.'||l_ukColName||'%TYPE'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 IN '||LOWER(p_table)||'.'||l_ukColName2||'%TYPE'END||') RETURN '||LOWER(p_table)||'%ROWTYPE IS'
                             ||CHR(10)||'  BEGIN'
                             ||CHR(10)||'    RETURN getRow(p_pk => NULL'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 => NULL' END||', p_uk => p_uk'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 => p_uk2' END||');'
                             ||CHR(10)||'  END getRow_UK;');
      outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
      
      outputLine(l_specHandle, CHR(10)||' /* Pipes a row from the '||p_table||' table for the supplied UK */');
      outputLine(l_specHandle,          '  FUNCTION pipeRow_UK (p_uk IN '||LOWER(p_table)||'.'||l_ukColName||'%TYPE'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 IN '||LOWER(p_table)||'.'||l_ukColName2||'%TYPE'END||') RETURN g_Rows PIPELINED;');
      outputLine(l_bodyHandle, CHR(10)||'  FUNCTION pipeRow_UK (p_uk IN '||LOWER(p_table)||'.'||l_ukColName||'%TYPE'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 IN '||LOWER(p_table)||'.'||l_ukColName2||'%TYPE'END||') RETURN g_Rows PIPELINED IS'
                             ||CHR(10)||'  BEGIN'
                             ||CHR(10)||'    PIPE ROW (getRow_UK(p_uk => p_uk'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 => p_uk2' END||'));'
                             ||CHR(10)
                             ||CHR(10)||'    RETURN;'
                             ||CHR(10)||'  END pipeRow_UK;');
      outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
      
      outputLine(l_specHandle, CHR(10)||' /* Returns a_data.gTrue or a_data.gFalse to indicate if a row is found on the '||p_table||' table for the supplied UK */');
      outputLine(l_specHandle,          '  FUNCTION exists_UK (p_uk IN '||LOWER(p_table)||'.'||l_ukColName||'%TYPE'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 IN '||LOWER(p_table)||'.'||l_ukColName2||'%TYPE'END||') RETURN VARCHAR2;');
      outputLine(l_bodyHandle, CHR(10)||'  FUNCTION exists_UK (p_uk IN '||LOWER(p_table)||'.'||l_ukColName||'%TYPE'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 IN '||LOWER(p_table)||'.'||l_ukColName2||'%TYPE'END||') RETURN VARCHAR2 IS'
                             ||CHR(10)||'    l_row    '||LOWER(p_table)||'%ROWTYPE;'
                             ||CHR(10)||'  BEGIN'
                             ||CHR(10)||'    l_row := getRow_UK(p_uk => p_uk'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 => p_uk2' END||');'
                             ||CHR(10)
                             ||CHR(10)||'    IF l_row.'||l_pkColName||' IS NOT NULL THEN'
                             ||CHR(10)||'      RETURN a_data.gTrue;'
                             ||CHR(10)||'    ELSE'
                             ||CHR(10)||'      RETURN a_data.gFalse;'
                             ||CHR(10)||'    END IF;'
                             ||CHR(10)||'  END exists_UK;');
    END IF;
    
    FOR r_cols IN (SELECT LOWER(column_name) column_name,
                          REPLACE(INITCAP(REGEXP_REPLACE(column_name,'^'||p_alias||'_*')),'_') column_alias,
                          data_type
                     FROM user_tab_columns
                    WHERE table_name = p_table
                    ORDER BY column_id) LOOP
      
      outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
      outputLine(l_specHandle, CHR(10)||' /* Returns '||UPPER(r_cols.column_name)||' from the '||p_table||' table for the supplied PK */');
      outputLine(l_specHandle,          '  FUNCTION get'||r_cols.column_alias||'_PK (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') RETURN '||LOWER(p_table)||'.'||r_cols.column_name||'%TYPE;');
      outputLine(l_bodyHandle, CHR(10)||'  FUNCTION get'||r_cols.column_alias||'_PK (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') RETURN '||LOWER(p_table)||'.'||r_cols.column_name||'%TYPE IS'
                             ||CHR(10)||'    l_row    '||LOWER(p_table)||'%ROWTYPE;'
                             ||CHR(10)||'  BEGIN'
                             ||CHR(10)||'    l_row := getRow_PK(p_pk => p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 => p_pk2' END||');'
                             ||CHR(10)
                             ||CHR(10)||'    RETURN l_row.'||r_cols.column_name||';'
                             ||CHR(10)||'  END get'||r_cols.column_alias||'_PK;');
        
      IF l_ukColName IS NOT NULL THEN
        outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
        outputLine(l_specHandle, CHR(10)||' /* Returns '||UPPER(r_cols.column_name)||' from the '||p_table||' table for the supplied UK */');
        outputLine(l_specHandle,          '  FUNCTION get'||r_cols.column_alias||'_UK (p_uk IN '||LOWER(p_table)||'.'||l_ukColName||'%TYPE'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 IN '||LOWER(p_table)||'.'||l_ukColName2||'%TYPE'END||') RETURN '||LOWER(p_table)||'.'||r_cols.column_name||'%TYPE;');
        outputLine(l_bodyHandle, CHR(10)||'  FUNCTION get'||r_cols.column_alias||'_UK (p_uk IN '||LOWER(p_table)||'.'||l_ukColName||'%TYPE'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 IN '||LOWER(p_table)||'.'||l_ukColName2||'%TYPE'END||') RETURN '||LOWER(p_table)||'.'||r_cols.column_name||'%TYPE IS'
                               ||CHR(10)||'    l_row    '||LOWER(p_table)||'%ROWTYPE;'
                               ||CHR(10)||'  BEGIN'
                               ||CHR(10)||'    l_row := getRow_UK(p_uk => p_uk'||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 => p_uk2' END||');'
                               ||CHR(10)
                               ||CHR(10)||'    RETURN l_row.'||r_cols.column_name||';'
                               ||CHR(10)||'  END get'||r_cols.column_alias||'_UK;');
      END IF;
    END LOOP;
    
    IF p_readOnly = 'N' THEN
      outputLine(l_bodyHandle, CHR(10)||' -- ----------------------------------------------------------------------------------------------------------');
      outputLine(l_bodyHandle,          ' -- Public Setters - Only avaliable for PK. Functions above can provide PK for supplied UK if needed');
      outputLine(l_bodyHandle,          ' -- ----------------------------------------------------------------------------------------------------------');
    
      FOR r_cols IN (SELECT LOWER(column_name) column_name,
                            REPLACE(INITCAP(REGEXP_REPLACE(column_name,'^'||p_alias||'_*')),'_') column_alias,
                            data_type,
                            (SELECT LISTAGG(CHR(10)||'    l_ignoreRow.'||LOWER(column_name)||' := '||CASE column_name WHEN atc.column_name THEN 'a_data.gFalse' ELSE 'a_data.gTrue' END,';') WITHIN GROUP (ORDER BY column_id)
                               FROM user_tab_columns
                              WHERE table_name          = p_table
                                AND LOWER(column_name) NOT IN (l_pkColName,NVL(l_pkColName2,l_pkColName))
                              GROUP BY table_name)||';' ignore_list
                       FROM user_tab_columns atc
                      WHERE table_name          = p_table
                        AND LOWER(column_name) NOT IN (l_pkColName,NVL(l_pkColName2,l_pkColName))
                      ORDER BY column_id) LOOP
      
        outputLine(l_specHandle, CHR(10)||' /* Updates '||p_table||'.'||UPPER(r_cols.column_name)||' to the supplied value for the supplied PK */');
        outputLine(l_specHandle,          '  PROCEDURE set'||r_cols.column_alias||' (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||', p_val IN '||LOWER(p_table)||'.'||r_cols.column_name||'%TYPE);');        
        outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE set'||r_cols.column_alias||' (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||', p_val IN '||LOWER(p_table)||'.'||r_cols.column_name||'%TYPE) IS'
                               ||CHR(10)||'    l_updateRow  g_updateRow       := NULL;' 
                               ||CHR(10)||'    l_ignoreRow  g_updateIgnoreRow := NULL;'
                               ||CHR(10)||'  BEGIN '
                               ||CHR(10)||r_cols.ignore_list
                               ||CHR(10)
                               ||CHR(10)||'    l_updateRow.'||r_cols.column_name||' := p_val;'
                               ||CHR(10)
                               ||CHR(10)||'    doUpdate(p_pk => p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 => p_pk2' END||', p_row => l_updateRow, p_ignore => l_ignoreRow);'
                               ||CHR(10)||'  END set'||r_cols.column_alias||';');
        outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
      END LOOP;
    
      outputLine(l_specHandle, CHR(10)||' /* Updates a row on the '||p_table||' table using the supplied row type for the supplied PK.');
      outputLine(l_specHandle,          '    Use the p_ignore parameter to indicate if some columns should not be indicated - allow it to default');
      outputLine(l_specHandle,          '    if you want all columns to be updated. */');
      outputLine(l_specHandle,          '  PROCEDURE updateRow (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||', p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL);');
      outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE updateRow (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||', p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow DEFAULT NULL) IS'
                             ||CHR(10)||'  BEGIN'
                             ||CHR(10)||'    doUpdate(p_pk => p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 => p_pk2' END||', p_row => p_row, p_ignore => p_ignore);'
                             ||CHR(10)||'  END updateRow;');
      outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
                           
      SELECT LISTAGG(CHR(10)||'                      p_'||LTRIM(LOWER(REGEXP_REPLACE(column_name,'^'||p_alias||'_*')),'_')||' IN '||LOWER(table_name)||'.'||LOWER(column_name)||'%TYPE',',') WITHIN GROUP (ORDER BY column_id),
             LISTAGG(CHR(10)||'    l_row.'||LOWER(column_name)||' := p_'||LTRIM(LOWER(REGEXP_REPLACE(column_name,'^'||p_alias||'_*')),'_')||';',NULL) WITHIN GROUP (ORDER BY column_id)
        INTO l_updateParams,
             l_updateCols
        FROM user_tab_columns
       WHERE table_name          = p_table
         AND LOWER(column_name) NOT IN (l_pkColName,NVL(l_pkColName2,l_pkColName))
       GROUP BY table_name;
                           
      outputLine(l_specHandle, CHR(10)||' /* Updates a row on the '||p_table||' table using the supplied values for the supplied PK.');
      outputLine(l_specHandle,          '    All columns will be updated, even those supplied as NULL */');
      outputLine(l_specHandle,          '  PROCEDURE updateRow (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||', '||l_updateParams||' );');
      outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE updateRow (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||', '||l_updateParams||' ) IS'
                             ||CHR(10)||'    l_row        g_updateRow := NULL;'
                             ||CHR(10)||'  BEGIN'
                                      ||l_updateCols
                             ||CHR(10)
                             ||CHR(10)||'    doUpdate(p_pk => p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 => p_pk2' END||', p_row => l_row, p_ignore => NULL);'
                             ||CHR(10)||'  END updateRow;');
      outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
      
      outputLine(l_specHandle, CHR(10)||' /* Inserts a row into the '||p_table||' table using the supplied row type and returns the new PK.');
      outputLine(l_specHandle,          '    Tables should have a trigger to populate the PK if it is supplied as NULL');
      outputLine(l_specHandle,          '    Column defaults will not be set for any columns supplied as NULL. */');
      outputLine(l_specHandle,          '  PROCEDURE insertRow (p_row IN '||LOWER(p_table)||'%ROWTYPE, p_newPk OUT '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_newPk2 OUT '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||');');
      outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE insertRow (p_row IN '||LOWER(p_table)||'%ROWTYPE, p_newPK OUT '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_newPk2 OUT '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') IS'
                             ||CHR(10)||'  BEGIN'
                             ||CHR(10)||'    doInsert(p_row => p_row, p_newPk => p_newPk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_newPk2 => p_newPk2' END||');'
                             ||CHR(10)||'  END insertRow;');
      outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
                           
      SELECT LISTAGG(CHR(10)||'                      p_'||LTRIM(LOWER(REGEXP_REPLACE(column_name,'^'||p_alias||'_*')),'_')||' IN '||LOWER(table_name)||'.'||LOWER(column_name)||'%TYPE',',') WITHIN GROUP (ORDER BY column_id),
             LISTAGG(CHR(10)||'    l_row.'||LOWER(column_name)||' := p_'||LTRIM(LOWER(REGEXP_REPLACE(column_name,'^'||p_alias||'_*')),'_')||';',NULL) WITHIN GROUP (ORDER BY column_id)
        INTO l_insertParams,
             l_insertCols
        FROM user_tab_columns
       WHERE table_name          = p_table
       GROUP BY table_name;
     
      outputLine(l_specHandle, CHR(10)||' /* Inserts a row into the '||p_table||' table using the supplied values and returns the new PK.');
      outputLine(l_specHandle,          '    Tables should have a trigger to populate the PK if it is supplied as NULL');
      outputLine(l_specHandle,          '    Column defaults will not be set for any columns supplied as NULL. */');
      outputLine(l_specHandle,          '  PROCEDURE insertRow ('||l_insertParams||', p_newPk OUT '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_newPk2 OUT '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||');');
      outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE insertRow ('||l_insertParams||', p_newPK OUT '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_newPk2 OUT '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') IS'
                             ||CHR(10)||'    l_row   '||LOWER(p_table)||'%ROWTYPE;'
                             ||CHR(10)||'  BEGIN'
                                      ||l_insertCols
                             ||CHR(10)
                             ||CHR(10)||'    doInsert(p_row => l_row, p_newPk => p_newPk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_newPk2 => p_newPk2' END||');'
                             ||CHR(10)||'  END insertRow;');
      outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
                            
      outputLine(l_specHandle, CHR(10)||' /* Deletes a row from the '||p_table||' table for the supplied PK */');
      outputLine(l_specHandle,          '  PROCEDURE deleteRow (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||');');
      outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE deleteRow (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') IS'
                             ||CHR(10)||'  BEGIN'
                             ||CHR(10)||'    doDelete(p_pk => p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 => p_pk2' END||');'
                             ||CHR(10)||'  END deleteRow;');
                             
    END IF;
    
    outputLine(l_bodyHandle, CHR(10)||' -- ----------------------------------------------------------------------------------------------------------');
    outputLine(l_bodyHandle,          ' -- Private functions / procedures');
    outputLine(l_bodyHandle,          ' -- ----------------------------------------------------------------------------------------------------------');
    
    outputLine(l_bodyHandle, CHR(10)||'  FUNCTION getRow (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||CASE WHEN l_ukColName IS NULL THEN NULL ELSE ', p_uk IN '||LOWER(p_table)||'.'||l_ukColName||'%TYPE DEFAULT NULL' END||CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 IN '||LOWER(p_table)||'.'||l_ukColName2||'%TYPE DEFAULT NULL' END||') RETURN '||LOWER(p_table)||'%ROWTYPE IS'
                           ||CHR(10)||'    l_row    '||LOWER(p_table)||'%ROWTYPE;'
                           ||CHR(10)||'  BEGIN '
                           ||CHR(10)||'    IF p_pk IS NOT NULL THEN'               
                           ||CHR(10)||'      SELECT *'
                           ||CHR(10)||'        INTO l_row'
                           ||CHR(10)||'        FROM '||LOWER(p_table)
                           ||CHR(10)||'       WHERE '||l_pkColName||' = p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ' AND '||l_pkColName2||' = p_pk2' END||';'
                           ||CASE WHEN l_ukColName IS NOT NULL AND l_ukColName2 IS NULL THEN
                             CHR(10)||'    ELSIF p_uk IS NOT NULL THEN'
                           ||CHR(10)||'      SELECT *'
                           ||CHR(10)||'        INTO l_row'
                           ||CHR(10)||'        FROM '||LOWER(p_table)
                           ||CHR(10)||'       WHERE '||l_ukColName||' = p_uk;'
                             ELSE NULL END
                           ||CASE WHEN l_ukColName IS NOT NULL AND l_ukColName2 IS NOT NULL THEN
                             CHR(10)||'    ELSIF p_uk IS NOT NULL THEN'
                           ||CHR(10)||'      SELECT *'
                           ||CHR(10)||'        INTO l_row'
                           ||CHR(10)||'        FROM '||LOWER(p_table)
                           ||CHR(10)||'       WHERE '||l_ukColName ||' = p_uk'
                           ||CHR(10)||'         AND ((p_uk2 IS NOT NULL AND'
                           ||CHR(10)||'               '||l_ukColName2||' = p_uk2) OR'
                           ||CHR(10)||'              (p_uk2 IS NULL AND'
                           ||CHR(10)||'               '||l_ukColName2||' IS NULL));'
                             ELSE NULL END
                           ||CHR(10)||'    END IF;'  
                           ||CHR(10)
                           ||CHR(10)||'    RETURN l_row;'
                           ||CHR(10)||'  EXCEPTION'
                           ||CHR(10)||'    WHEN NO_DATA_FOUND THEN'
                           ||CHR(10)||'      RETURN NULL;'
                           ||CHR(10)||'  END getRow;');
    outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
    
    IF p_readOnly = 'N' THEN
      l_line := CHR(10)||'  PROCEDURE doUpdate (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||', p_row IN g_updateRow, p_ignore IN g_updateIgnoreRow) IS'
              ||CHR(10)||'  BEGIN '
              ||CHR(10)||'    IF p_pk IS NOT NULL THEN' 
              ||CHR(10)||'      UPDATE '||LOWER(p_table)
              ||CHR(10)||'         SET ';
                
      FOR r_cols IN (SELECT LOWER(column_name) column_name
                       FROM user_tab_columns
                      WHERE table_name          = p_table
                        AND LOWER(column_name) NOT IN (l_pkColName,NVL(l_pkColName2,l_pkColName))
                      ORDER BY column_id) LOOP
        l_line := l_line
                ||CHR(10)||'             '||r_cols.column_name||' = CASE NVL(p_ignore.'||r_cols.column_name||', a_data.gFalse) WHEN a_data.gTrue THEN '||r_cols.column_name||' ELSE p_row.'||r_cols.column_name||' END,';
      END LOOP;
    
      l_line := RTRIM(l_line,',')
              ||CHR(10)||'         WHERE '||l_pkColName||' = p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ' AND '||l_pkColName||' = p_pk2' END||';'
              ||CHR(10)||'    END IF;'
              ||CHR(10)||'  END doUpdate;';
                                            
      outputLine(l_bodyHandle, l_line);
      outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
      
      outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE doDelete (p_pk IN '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 IN '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') IS'
                             ||CHR(10)||'  BEGIN '
                             ||CHR(10)||'    IF p_pk IS NOT NULL THEN'  
                             ||CHR(10)||'      DELETE '||LOWER(p_table)
                             ||CHR(10)||'       WHERE '||l_pkColName||' = p_pk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ' AND '||l_pkColName||' = p_pk2' END||';'
                             ||CHR(10)||'    END IF;'
                             ||CHR(10)||'  END doDelete;');
      outputLine(l_bodyHandle, CHR(10)||'  ----------------------------------------------------------------------------------------------------------------');
    
      outputLine(l_bodyHandle, CHR(10)||'  PROCEDURE doInsert (p_row IN '||LOWER(p_table)||'%ROWTYPE, p_newPk OUT '||LOWER(p_table)||'.'||l_pkColName||'%TYPE'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_newPk2 OUT '||LOWER(p_table)||'.'||l_pkColName2||'%TYPE' END||') IS'
                             ||CHR(10)||'    l_pk    '||LOWER(p_table)||'.'||l_pkColName||'%TYPE;'
                             ||CHR(10)||'  BEGIN '
                             ||CHR(10)||'    INSERT'
                             ||CHR(10)||'      INTO '||LOWER(p_table)
                             ||CHR(10)||'    VALUES p_row'
                             ||CHR(10)||'    RETURNING '||l_pkColName||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', '||l_pkColName2 END
                             ||CHR(10)||'         INTO p_newPk'||CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_newPk2' END||';'
                             ||CHR(10)||'  END doInsert;');
    END IF;
    
    outputLine(l_specHandle, CHR(10)||'END t_'||REPLACE(INITCAP(p_table),'_')||';'||CHR(10)||'/');
    outputLine(l_bodyHandle, CHR(10)||'END t_'||REPLACE(INITCAP(p_table),'_')||';'||CHR(10)||'/');
    
    UTL_FILE.FCLOSE(l_specHandle);
    UTL_FILE.FCLOSE(l_bodyHandle);
  END createTableAPI;
  
 -- ----------------------------------------------------------------------------------------------------------
  
  PROCEDURE createTableAPI_RO (p_table    IN VARCHAR2,
                               p_alias    IN VARCHAR2,
                               p_owner    IN VARCHAR2) IS
                            
  BEGIN
    createTableAPI (p_table    => p_table,
                    p_alias    => p_alias,
                    p_owner    => p_owner,
                    p_readOnly => 'Y');
  END createTableAPI_RO;
  
 -- ----------------------------------------------------------------------------------------------------------
  
  PROCEDURE createView (p_table IN VARCHAR2,
                        p_owner IN VARCHAR2) IS
    l_handle    UTL_FILE.FILE_TYPE;
    l_line      VARCHAR2(4000);
    l_usrCol    VARCHAR2(30);
  BEGIN
    l_handle := UTL_FILE.FOPEN('DATA_PUMP_DIR',LOWER(p_table)||'.vw', 'W');
    
    outputLine(l_handle, 'CREATE OR REPLACE FORCE VIEW '||p_owner||'.'||LOWER(p_table)||'_vw');
    outputLine(l_handle, '(');
    
    l_line := NULL;
    
    FOR r_cols IN (SELECT LOWER(column_name) column_name
                     FROM user_tab_columns
                    WHERE table_name = p_table
                    ORDER BY column_id) LOOP
      IF l_line IS NOT NULL THEN
        outputLine(l_handle, l_line||',');
      END IF;
      
      l_line := '   '||r_cols.column_name;
    END LOOP;
    
    outputLine(l_handle, l_line);
    
    outputLine(l_handle, ')');
    outputLine(l_handle, 'AS');
    outputLine(l_handle, '   SELECT');
    
    l_line := NULL;
    
    FOR r_cols IN (SELECT LOWER(column_name) column_name
                     FROM user_tab_columns
                    WHERE table_name = p_table
                    ORDER BY column_id) LOOP
      IF l_line IS NOT NULL THEN
        outputLine(l_handle, l_line||',');
      END IF;
      
      l_line := '          '||r_cols.column_name;
      
      IF r_cols.column_name LIKE '%usr_id' THEN
        l_usrCol := r_cols.column_name;
      END IF;
    END LOOP;
    
    outputLine(l_handle, l_line);
    
    IF l_usrCol IS NOT NULL THEN
      outputLine(l_handle, '     FROM '||LOWER(p_table));
      outputLine(l_handle, '    WHERE ti_core_src.c_VPD.userVPD_nonEE ('||l_usrCol||') = ''Y'';');
    ELSE
      outputLine(l_handle, '     FROM '||LOWER(p_table)||';');
    END IF;
    
    BEGIN
      SELECT comments
        INTO l_line
        FROM user_tab_comments
       WHERE table_name = p_table;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_line := 'View on table '||p_table;
    END;
    
    outputLine(l_handle, NULL);
    outputLine(l_handle, 'COMMENT ON TABLE '||p_owner||'.'||LOWER(p_table)||'_vw IS '''||l_line||''';');
    outputLine(l_handle, NULL);
    
    FOR r_coms IN (SELECT comments,
                          LOWER(column_name) column_name
                     FROM user_col_comments
                    WHERE table_name = p_table) LOOP
      outputLine(l_handle, 'COMMENT ON COLUMN '||p_owner||'.'||LOWER(p_table)||'_vw.'||r_coms.column_name||' IS '''||r_coms.comments||''';');
    END LOOP; 
    
    UTL_FILE.FCLOSE(l_handle);
  END createView;
  
 -- ----------------------------------------------------------------------------------------------------------
 -- Private declarations
 -- ----------------------------------------------------------------------------------------------------------
  PROCEDURE outputLine (p_handle IN UTL_FILE.FILE_TYPE,
                        p_line   IN VARCHAR2) IS
  BEGIN
    UTL_FILE.PUT_LINE(p_handle, p_line);
  END outputLine;
    
END ddlUtils;
/