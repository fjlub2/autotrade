BEGIN
  BEGIN
    DBMS_RLS.DROP_POLICY (
      object_schema    => 'DAT',
      object_name      => 'USERS',
      policy_name      => 'USR_POLICY');
      
    DBMS_RLS.DROP_POLICY (
      object_schema    => 'DAT',
      object_name      => 'USER_ADDRESSES',
      policy_name      => 'UAD_POLICY');
      
    DBMS_RLS.DROP_POLICY (
      object_schema    => 'DAT',
      object_name      => 'MY_GOALS',
      policy_name      => 'MYG_POLICY');
  EXCEPTION 
    WHEN OTHERS THEN
      NULL;
  END;
  
  DBMS_RLS.ADD_POLICY (
    object_schema    => 'DAT',
    object_name      => 'USERS',
    policy_name      => 'USR_POLICY',
    function_schema  => 'API',
    policy_function  => 'c_VPD.userVPD',
    statement_types  => 'SELECT, UPDATE, DELETE'
   );
   
  DBMS_RLS.ADD_POLICY (
    object_schema    => 'DAT',
    object_name      => 'USER_ADDRESSES',
    policy_name      => 'UAD_POLICY',
    function_schema  => 'API',
    policy_function  => 'c_VPD.userVPD',
    statement_types  => 'SELECT, INSERT, UPDATE, DELETE'
   );
   
  DBMS_RLS.ADD_POLICY (
    object_schema    => 'DAT',
    object_name      => 'MY_GOALS',
    policy_name      => 'MYG_POLICY',
    function_schema  => 'API',
    policy_function  => 'c_VPD.userVPD',
    statement_types  => 'SELECT, INSERT, UPDATE, DELETE'
   );
 END;
/