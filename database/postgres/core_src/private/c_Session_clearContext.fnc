CREATE OR REPLACE FUNCTION core_src."c_Session_clearContext" () RETURNS void AS
$BODY$
BEGIN
  SET SESSION session_context.usr_id            TO DEFAULT;
  SET SESSION session_context.uss_id            TO DEFAULT;
  SET SESSION session_context.lng_id            TO DEFAULT;
  SET SESSION session_context.zend_session_code TO DEFAULT;
  SET SESSION session_context.db_session_code   TO DEFAULT;
  SET SESSION session_context.message_level     TO DEFAULT;
  SET SESSION session_context.task_id           TO DEFAULT;
  
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;