CREATE OR REPLACE FUNCTION core_src."c_Message_getErrors_json" (p_taskId IN core_dat.message_log_1.mss_task_id%TYPE) RETURNS JSON AS
$BODY$
DECLARE
  l_json  JSON;
BEGIN
  SELECT ROW_TO_JSON(c)
    INTO l_json
    FROM (SELECT ROW_TO_JSON(b) "messages"
            FROM (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))) "items"
                    FROM (SELECT COALESCE(msc_html_code,core_tapi."t_MessageCodes_getHtmlCode_UK"('T0001')) "code",
                                 mss_message "message"
                            FROM core_src."c_Message_getErrors"(p_taskId := p_taskId)
                              LEFT JOIN core_dat.message_codes ON mss_msc_id = msc_id) a) b) c;
                              
  RETURN l_json;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
