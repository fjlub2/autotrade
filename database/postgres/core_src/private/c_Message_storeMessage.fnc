CREATE OR REPLACE FUNCTION core_src."c_Message_storeMessage" (p_message  IN core_dat.message_log_1.mss_message%TYPE,
                                                              p_ussId    IN core_dat.user_sessions.uss_id%TYPE,
                                                              p_type     IN core_dat.message_log_1.mss_type%TYPE,
                                                              p_data     IN core_dat.message_log_1.mss_data%TYPE    DEFAULT NULL,
                                                              p_debug    IN core_dat.message_log_1.mss_debug%TYPE   DEFAULT NULL,
                                                              p_context  IN core_dat.message_log_1.mss_context%TYPE DEFAULT NULL,
                                                              p_taskId   IN core_dat.message_log_1.mss_task_id%TYPE DEFAULT NULL,
                                                              p_code     IN core_dat.message_codes.msc_code%TYPE DEFAULT NULL) RETURNS void AS
$BODY$
DECLARE
  l_row    core_dat.message_log_1%ROWTYPE;
  l_mssId  core_dat.message_log_1.mss_id%TYPE;
BEGIN
  l_row.mss_uss_id  := p_ussId;
  l_row.mss_date    := NOW();
  l_row.mss_type    := p_type;
  l_row.mss_data    := p_data;
  l_row.mss_message := p_message;
  l_row.mss_debug   := p_debug;
  l_row.mss_task_id := p_taskId;
  l_row.mss_context := p_context;
  
  IF p_code IS NOT NULL THEN
    l_row.mss_msc_id := core_tapi."t_MessageCodes_getId_UK"(p_uk := p_code);  
  ELSE
    l_row.mss_msc_id := NULL;
  END IF;

  /* Want to use subtransactions when Autonomous Transactions are implemented in Postgres.*/
     
  --BEGIN SUBTRANSACTION;
    IF core_tapi."t_CodeValues_getValue_PK"('config_values', 'message_table') = '1' THEN
      l_mssId := core_tapi."t_MessageLog1_insertRow" (p_row := l_row);
    ELSE
      l_mssId := core_tapi."t_MessageLog2_insertRow" (p_row := l_row);
    END IF;
  --COMMIT SUBTRANSACTION;
  
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
