CREATE OR REPLACE FUNCTION core_src."c_Message_logMsgText" (p_message  IN core_dat.message_log_1.mss_message%TYPE,
                                                            p_type     IN core_dat.message_log_1.mss_type%TYPE,
                                                            p_data     IN core_dat.message_log_1.mss_data%TYPE    DEFAULT NULL,
                                                            p_debug    IN core_dat.message_log_1.mss_debug%TYPE   DEFAULT NULL,
                                                            p_taskId   IN core_dat.message_log_1.mss_task_id%TYPE DEFAULT NULL,
                                                            p_context  IN core_dat.message_log_1.mss_context%TYPE DEFAULT NULL,
                                                            p_logout   IN character                               DEFAULT 'N') RETURNS void AS
$BODY$
DECLARE
  l_level     core_dat.users.usr_message_level%TYPE;
  l_errorNo   text;
BEGIN
  l_level := core_src."c_Session_getCurrentMessageLevel"();

  /* Message Levels (in order of precedence):
       F = Fatal
       E = Error
       W = Warning
       I = Info
       D = Debug */

  IF l_level = 'F' AND 
     p_type IN ('D', 'I', 'W', 'E') THEN
    RETURN;
  ELSIF l_level = 'E' AND 
        p_type IN ('D', 'I', 'W') THEN
    RETURN;
  ELSIF l_level = 'W' AND 
        p_type IN ('D', 'I') THEN
    RETURN;
  ELSIF l_level = 'I' AND 
        p_type = 'D' THEN
    RETURN;
  END IF;

  PERFORM core_src."c_Message_storeMessage" (p_message  := p_message,
                                             p_ussId    := core_src."c_Session_getCurrentUssId"(),
                                             p_type     := p_type,
                                             p_data     := p_data,
                                             p_debug    := p_debug,
                                             p_context  := p_context,
                                             p_taskId   := p_taskId);

  IF COALESCE(p_logout,'N') = 'Y' THEN
    PERFORM core_src."c_Session_endSession"();
  END IF;
                                             
  IF p_type = 'F' THEN
    IF p_data IS NULL AND p_debug IS NULL THEN
      RAISE EXCEPTION USING ERRCODE = 'T0001', MESSAGE = l_message;
    ELSIF p_data IS NOT NULL AND p_debug IS NULL THEN
      RAISE EXCEPTION USING ERRCODE = 'T0001', MESSAGE = l_message, HINT = CAST(p_data AS text);
    ELSIF p_data IS NULL AND p_debug IS NOT NULL THEN
      RAISE EXCEPTION USING ERRCODE = 'T0001', MESSAGE = l_message, DETAIL = p_debug;
    ELSE
      RAISE EXCEPTION USING ERRCODE = 'T0001', MESSAGE = l_message, HINT = CAST(p_data AS text), DETAIL = p_debug;
    END IF;
  END IF;
  
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;