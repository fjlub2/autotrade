CREATE OR REPLACE FUNCTION core_src."c_Data_addTag"(p_name   IN text,
                                                    p_value  IN timestamp) RETURNS core_dat.message_log_1.mss_data%TYPE AS
$BODY$
BEGIN
  RETURN core_src."c_Data_addTag"(NULL, p_name := p_name, p_value := p_value);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;