CREATE OR REPLACE FUNCTION core_src."c_Data_addTag"(p_data   IN core_dat.message_log_1.mss_data%TYPE,
                                                    p_name   IN text,
                                                    p_value  IN text) RETURNS core_dat.message_log_1.mss_data%TYPE AS
$BODY$
BEGIN
  RETURN ARRAY_APPEND(p_data, CONCAT('(',p_name,',"',REPLACE(p_value,'"','\"'),'")')::core_dat.tr_data);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;