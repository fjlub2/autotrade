CREATE OR REPLACE FUNCTION core_src."c_Session_timeoutSessions"() RETURNS void AS
$BODY$
DECLARE
  l_timeOutPeriod  text;
  r_sessions       RECORD;
BEGIN
  l_timeOutPeriod := core_tapi."t_CodeValues_getValue_PK"('config_values', 'max_session_idle')||' hours';

  FOR r_sessions IN (SELECT uss_id
                       FROM core_dat.user_sessions
                      WHERE uss_last_used < localtimestamp - l_timeOutPeriod::interval
                        AND uss_session_end IS NULL) LOOP  
 
    PERFORM core_tapi."t_UserSessions_setSessionEnd" (p_pk  := r_sessions.uss_id,
                                                      p_val := localtimestamp);
       
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Session automatically timedout.',
                                            p_data    := core_src."c_Data_addTag"('USS_ID',r_sessions.uss_id),
                                            p_type    := 'I');
  END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;