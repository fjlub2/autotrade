CREATE OR REPLACE FUNCTION core_src."c_Message_errorsExist" (p_taskId IN core_dat.message_log_1.mss_task_id%TYPE) RETURNS char AS
$BODY$
DECLARE
  l_out   text;
BEGIN
  IF core_tapi."t_CodeValues_getValue_PK"('config_values', 'message_table') = '1' THEN
    SELECT 'Y'
      INTO STRICT l_out
      FROM core_dat.message_log_1 mss
     WHERE mss_task_id = p_taskId
       AND mss_type    = 'E'
     LIMIT 1;
  ELSE
    SELECT 'Y'
      INTO STRICT l_out
      FROM core_dat.message_log_2 mss
     WHERE mss_task_id = p_taskId
       AND mss_type    = 'E'
     LIMIT 1;
  END IF;
  
  RETURN 'Y';
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 'N';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
