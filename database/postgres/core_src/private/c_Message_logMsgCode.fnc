CREATE OR REPLACE FUNCTION core_src."c_Message_logMsgCode" (p_code     IN core_dat.message_codes.msc_code%TYPE,
                                                            p_type     IN core_dat.message_log_1.mss_type%TYPE,
                                                            p_data     IN core_dat.message_log_1.mss_data%TYPE    DEFAULT NULL,
                                                            p_vars     IN core_dat.message_log_1.mss_data%TYPE    DEFAULT NULL,
                                                            p_debug    IN core_dat.message_log_1.mss_debug%TYPE   DEFAULT NULL,
                                                            p_taskId   IN core_dat.message_log_1.mss_task_id%TYPE DEFAULT NULL,
                                                            p_logout   IN character                               DEFAULT 'N') RETURNS void AS
$BODY$
DECLARE
  l_message   core_dat.message_log_1.mss_message%TYPE;
  l_level     core_dat.users.usr_message_level%TYPE;
  l_errorNo   text;
BEGIN
  l_level := core_src."c_Session_getCurrentMessageLevel"();

  /* Message Levels (in order of precedence):
       F = Fatal
       E = Error
       W = Warning
       I = Info
       D = Debug */

  IF l_level = 'F' AND 
     p_type IN ('D', 'I', 'W', 'E') THEN
    RETURN;
  ELSIF l_level = 'E' AND 
        p_type IN ('D', 'I', 'W') THEN
    RETURN;
  ELSIF l_level = 'W' AND 
        p_type IN ('D', 'I') THEN
    RETURN;
  ELSIF l_level = 'I' AND 
        p_type = 'D' THEN
    RETURN;
  END IF;
    
  l_message := core_src."c_Message_getMessageText" (p_code    := p_code,
                                                    p_vars    := p_vars);

  PERFORM core_src."c_Message_storeMessage" (p_message  := l_message,
                                             p_ussId    := core_src."c_Session_getCurrentUssId"(),
                                             p_type     := p_type,
                                             p_data     := core_src."c_Data_addTag"(p_data,'ERROR_CODE',p_code),
                                             p_debug    := p_debug,
                                             p_taskId   := p_taskId,
                                             p_code     := p_code);

  IF COALESCE(p_logout,'N') = 'Y' THEN
    PERFORM core_src."c_Session_endSession"();
  END IF;
                                             
  IF p_type = 'F' THEN    
    IF p_debug IS NULL THEN
      RAISE EXCEPTION USING ERRCODE = p_code, MESSAGE = l_message, HINT = CAST(core_src."c_Data_addTag"(p_data,'ERROR_CODE',p_code) AS text);
    ELSE
      RAISE EXCEPTION USING ERRCODE = p_code, MESSAGE = l_message, HINT = CAST(core_src."c_Data_addTag"(p_data,'ERROR_CODE',p_code) AS text), DETAIL = p_debug;
    END IF;
  END IF;
  
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;