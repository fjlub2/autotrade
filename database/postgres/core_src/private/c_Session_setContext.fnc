CREATE OR REPLACE FUNCTION core_src."c_Session_setContext" (p_dbSessionCode IN core_dat.user_sessions.uss_db_session_code%TYPE) RETURNS INTEGER AS
$BODY$
DECLARE
  l_usrId           core_dat.users.usr_id%TYPE;
  l_ussId           core_dat.user_sessions.uss_id%TYPE;
  l_lngId           core_dat.languages.lng_id%TYPE;
  l_level           core_dat.code_values.cv_value%TYPE;
  l_end             core_dat.user_sessions.uss_session_end%TYPE;
  l_lastUsed        core_dat.user_sessions.uss_last_used%TYPE;
  l_timeOutPeriod   text;
  l_taskId          INTEGER;
BEGIN
  PERFORM core_src."c_Session_endSession"();
  
  l_taskId := core_src."c_Message_getTaskId"();
      
  BEGIN
    SELECT uss_usr_id,
           uss_id,
           COALESCE(usr_lng_id,uss_lng_id),
           COALESCE(usr_message_level,core_tapi."t_CodeValues_getValue_PK"('config_values', 'message_level')),
           uss_session_end,
           uss_last_used
      INTO 
    STRICT l_usrId,
           l_ussId,
           l_lngId,
           l_level,
           l_end,
           l_lastUsed
      FROM core_dat.user_sessions
        JOIN core_dat.users ON usr_id = uss_usr_id
     WHERE uss_db_session_code = p_dbSessionCode;
  
    IF l_end IS NOT NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0018', -- session expired
                                              p_type   := 'E',
                                              p_data   := core_src."c_Data_addTag"('USS_ID',l_ussId),
                                              p_logout := 'Y',
                                              p_taskId := l_taskId);
    ELSE  
      l_timeOutPeriod := core_tapi."t_CodeValues_getValue_PK"('config_values', 'max_session_idle')||' hours';
  
      IF l_lastUsed < localtimestamp - l_timeOutPeriod::interval THEN
        PERFORM core_tapi."t_UserSessions_setSessionEnd" (p_pk  := l_ussId,
                                                          p_val := localtimestamp);
       
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Session timedout automatically.',
                                                p_data    := core_src."c_Data_addTag"('USS_ID',l_ussId),
                                                p_type    := 'E',
                                                p_taskId  := l_taskId);
                                            
        EXECUTE CONCAT('SET SESSION session_context.uss_id            TO  ',l_ussId,';'); -- setting this to allow c_Session_validSession to work later on
      ELSE
        EXECUTE CONCAT('SET SESSION session_context.usr_id            TO  ',l_usrId,';');
        EXECUTE CONCAT('SET SESSION session_context.uss_id            TO  ',l_ussId,';');
  
        IF l_lngId IS NOT NULL THEN
          EXECUTE CONCAT('SET SESSION session_context.lng_id          TO  ',l_lngId,';');
        ELSE
          EXECUTE 'SET SESSION session_context.lng_id                 TO DEFAULT';
        END IF;
  
        EXECUTE CONCAT('SET SESSION session_context.db_session_code   TO "',p_dbSessionCode,'";');
        EXECUTE CONCAT('SET SESSION session_context.message_level     TO "',l_level,'";');
    
        EXECUTE CONCAT('SET SESSION session_context.task_id           TO  ',l_taskId,';');

        PERFORM core_tapi."t_UserSessions_setLastUsed"(p_pk := l_ussId, p_val := NOW()::timestamp);
      
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Session context set.',
                                                p_type    := 'I',
                                                p_data    := core_src."c_Data_addTag"('USS_ID',l_ussId),
                                                p_taskId  := l_taskId);
      END IF;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0008', -- not authenticated
                                              p_type   := 'E',
                                              p_data   := core_src."c_Data_addTag"('SESSION_CODE',p_dbSessionCode),
                                              p_logout := 'Y',
                                              p_taskId := l_taskId);
  END;
  
  RETURN l_taskId;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
