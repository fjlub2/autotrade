CREATE OR REPLACE FUNCTION core_src."c_Session_getCurrentLngId"() RETURNS core_dat.languages.lng_id%TYPE AS
$BODY$
DECLARE
  l_val  text;
BEGIN
  l_val := CURRENT_SETTING('session_context.lng_id');
  
  RETURN CASE WHEN l_val = '' THEN NULL::integer ELSE l_val::integer END;
EXCEPTION
  WHEN UNDEFINED_OBJECT THEN
    RETURN NULL;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;