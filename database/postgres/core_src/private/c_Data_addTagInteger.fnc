CREATE OR REPLACE FUNCTION core_src."c_Data_addTag"(p_data   IN core_dat.message_log_1.mss_data%TYPE,
                                                    p_name   IN text,
                                                    p_value  IN integer) RETURNS core_dat.message_log_1.mss_data%TYPE AS
$BODY$
BEGIN
  RETURN core_src."c_Data_addTag"(p_data := p_data, p_name := p_name, p_value := p_value::text);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;