CREATE OR REPLACE FUNCTION core_src."c_Session_validSession"() RETURNS void AS
$BODY$
DECLARE
  l_ussId   core_dat.user_sessions.uss_id%TYPE;
  l_end     core_dat.user_sessions.uss_session_end%TYPE;
BEGIN
  l_ussId := core_src."c_Session_getCurrentUssId"();
  
  SELECT uss_session_end
    INTO STRICT l_end
    FROM core_dat.user_sessions
   WHERE uss_id = l_ussId;
  
  IF l_end IS NOT NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0018', -- session expired
                                            p_type   := 'F',
                                            p_data   := core_src."c_Data_addTag"('USS_ID',l_ussId),
                                            p_logout := 'Y',
                                            p_taskId := core_src."c_Session_getCurrentTaskId"());
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0008', -- not authenticated
                                            p_type   := 'F',
                                            p_logout := 'Y',
                                            p_taskId := core_src."c_Session_getCurrentTaskId"());
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;