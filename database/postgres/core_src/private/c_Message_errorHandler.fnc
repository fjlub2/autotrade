CREATE OR REPLACE FUNCTION core_src."c_Message_errorHandler" (p_code    IN core_dat.message_codes.msc_code%TYPE,
                                                              p_message IN core_dat.message_codes.msc_default_text%TYPE,
                                                              p_data    IN core_dat.message_log_1.mss_data%TYPE   DEFAULT NULL,                 -- temporary parameter - remove when autonomous transactions are available
                                                              p_debug   IN core_dat.message_log_1.mss_debug%TYPE  DEFAULT NULL,                 -- temporary parameter - remove when autonomous transactions are available
                                                              p_context IN core_dat.message_log_1.mss_debug%TYPE  DEFAULT NULL) RETURNS json AS -- temporary parameter - remove when autonomous transactions are available
$BODY$
DECLARE
  l_json      json;
  l_htmlCode  core_dat.message_codes.msc_html_code%TYPE;
  l_data      core_dat.message_log_1.mss_data%TYPE;
BEGIN
  IF p_data IS NULL THEN
    l_data := core_src."c_Data_addTag"('ERROR_CODE',p_code);
  ELSE
    l_data := p_data;
  END IF;

  IF p_code LIKE 'T%' THEN
    l_htmlCode := core_tapi."t_MessageCodes_getHtmlCode_UK"(p_code);
    
    -- this is temporary until autonomous transaction code is added to c_Message_storeMessage
    PERFORM core_src."c_Message_logMsgText"(p_message := p_message,
                                            p_data    := l_data,
                                            p_type    := 'E',
                                            p_debug   := p_debug,
                                            p_context := p_context);
  END IF;
  
  IF l_htmlCode IS NULL THEN -- error not defined
    PERFORM core_src."c_Message_logMsgText"(p_message := p_message,
                                            p_type    := 'E',
                                            p_context := p_context);

    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "messages"
              FROM (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))) "items"
                      FROM (SELECT core_tapi."t_MessageCodes_getHtmlCode_UK"('T0001') code,
                                   core_src."c_Message_getMessageText"('T0001', NULL) message) a) b) c;
  ELSE
    l_json := CAST(CONCAT('{"messages":{"items":[{"code":',l_htmlCode,',"message":"',p_message,'"}]}}') AS json);
  END IF;

  RETURN l_json;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
