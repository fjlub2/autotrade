CREATE OR REPLACE FUNCTION core_src."c_Session_endSession"() RETURNS void AS
$BODY$
DECLARE
  l_ussId core_dat.user_sessions.uss_id%TYPE;
BEGIN
  l_ussId := core_src."c_Session_getCurrentUssId"();

  IF l_ussId IS NOT NULL THEN
    PERFORM core_tapi."t_UserSessions_setSessionEnd"(p_pk := l_ussId, p_val := localtimestamp);
    
    PERFORM core_src."c_Message_logMsgText" (p_message := 'Session ended',
                                             p_data    := core_src."c_Data_addTag"('USS_ID',l_ussId),
                                             p_type    := 'I');
  END IF;

  PERFORM core_src."c_Session_clearContext"();
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;