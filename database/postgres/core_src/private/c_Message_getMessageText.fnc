CREATE OR REPLACE FUNCTION core_src."c_Message_getMessageText" (p_code  IN text,
                                                                p_vars  IN core_dat.tr_data[]) RETURNS core_dat.message_codes.msc_default_text%TYPE AS
$BODY$
DECLARE
  l_out    core_dat.message_log_1.mss_message%TYPE;
  l_mscId  core_dat.message_codes.msc_id%TYPE;
  l_lngId  core_dat.languages.lng_id%TYPE;
  r_subs   RECORD;
BEGIN    
  IF p_code IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode" (p_code   := 'T0001',
                                             p_type   := 'E',
                                             p_debug  := 'p_code supplied as NULL to a_Message.getMessageText',
                                             p_data   := p_vars);
  END IF;
    
  l_mscId := core_tapi."t_MessageCodes_getId_UK"(p_uk  := p_code);
  IF l_mscId IS NULL THEN
    l_mscId := core_tapi."t_MessageCodes_getId_UK"(p_uk  := p_code);
  END IF;

  l_lngId := core_src."c_Session_getCurrentLngId"();
  
  IF l_lngId IS NOT NULL THEN
    l_out := core_tapi."t_MessageText_getText_UK"(p_uk  := l_mscId,
                                                  p_uk2 := l_lngId);
  END IF;
    
  IF l_out IS NULL THEN
    l_out := core_tapi."t_MessageCodes_getDefaultText_PK"(p_pk := l_mscId);
  END IF;
    
  FOR r_subs IN (SELECT msu_code, tag_name, tag_value
                   FROM core_dat.message_substitutes
                     FULL JOIN UNNEST(p_vars) ON msu_code = tag_name
                  WHERE (msu_msc_id = l_mscId OR
                         msu_msc_id IS NULL)) LOOP
    IF r_subs.msu_code IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode" (p_code   := 'T0001',
                                               p_type   := 'E',
                                               p_debug  := 'Substitution variable supplied that is not allowed in the message. '||r_subs.tag_name,
                                               p_data   := p_vars);
    ELSIF r_subs.tag_name IS NULL AND
          POSITION('<'||r_subs.msu_code||'>' IN l_out) != 0 THEN
      PERFORM core_src."c_Message_logMsgCode" (p_code   := 'T0001',
                                               p_type   := 'E',
                                               p_debug  := 'Substitution variable exists in message text, but no value has been supplied. '||r_subs.msu_code,
                                               p_data   := p_vars);
    END IF;
      
    l_out := REPLACE(l_out,'<'||r_subs.msu_code||'>',r_subs.tag_value);
  END LOOP;
    
  IF REGEXP_MATCHES(l_out, '[<]{1}[[:alnum:]_]+[>]{1}') IS NOT NULL THEN -- looks for unpopulated tags in the output string
    PERFORM core_src."c_Message_logMsgCode" (p_code   := 'T0001',
                                             p_type   := 'E',
                                             p_debug  := 'Some substitution vars were not populated for message code '||p_code,
                                             p_data   := p_vars);
  END IF;
    
  RETURN l_out;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
