CREATE OR REPLACE FUNCTION core_src."c_Session_getCurrentUssId"() RETURNS core_dat.user_sessions.uss_id%TYPE AS
$BODY$
DECLARE
  l_val  text;
BEGIN
  l_val := CURRENT_SETTING('session_context.uss_id');
  
  RETURN CASE WHEN l_val = '' THEN NULL::integer ELSE l_val::integer END;
EXCEPTION
  WHEN UNDEFINED_OBJECT THEN
    RETURN NULL;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;