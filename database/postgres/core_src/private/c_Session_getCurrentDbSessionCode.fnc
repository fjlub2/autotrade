CREATE OR REPLACE FUNCTION core_src."c_Session_getCurrentDbSessionCode"() RETURNS core_dat.user_sessions.uss_db_session_code%TYPE AS
$BODY$
BEGIN
  RETURN CURRENT_SETTING('session_context.db_session_code');
EXCEPTION
  WHEN UNDEFINED_OBJECT THEN
    RETURN NULL;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;