CREATE OR REPLACE FUNCTION core_src."c_Security_checkPassword" (p_usrId IN core_dat.users.usr_id%TYPE,
                                                                p_pw    IN text) RETURNS character AS
$BODY$
DECLARE
  l_current   bytea;
  l_pwdTries  core_dat.users.usr_pwd_tries%TYPE;
BEGIN
  l_pwdTries := core_tapi."t_Users_getPwdTries_PK" (p_pk := p_usrId);
  
  IF l_pwdTries > 3 THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T00005', -- account locked
                                            p_data   := core_src."c_Data_addTag"('USR_ID',p_usrId),
                                            p_type   := 'E',
                                            p_logout := 'N');
                                            
    RETURN 'N';
  END IF;
  
  l_current  := core_tapi."t_Users_getPassword_PK"(p_pk := p_usrId);

  IF REVERSE(ENCODE(l_current,'escape')) = CRYPT(CONCAT(p_usrId,p_pw),REVERSE(ENCODE(l_current,'escape'))) THEN
    PERFORM core_tapi."t_Users_setPwdTries" (p_pk  := p_usrId,
                                             p_val := 0::smallint);
    
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Supplied password is valid.',
                                            p_data    := core_src."c_Data_addTag"('USR_ID',p_usrId),
                                            p_type    := 'I');
    
    RETURN 'Y';
  ELSE
    IF l_pwdTries >= 3 THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T00005', -- account locked
                                              p_data   := core_src."c_Data_addTag"('USR_ID',p_usrId),
                                              p_type   := 'E',
                                              p_logout := 'N');
    ELSE
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Supplied password is invalid.',
                                              p_data    := core_src."c_Data_addTag"('USR_ID',p_usrId),
                                              p_type    := 'I');
    END IF;
    
    RETURN 'N';
  END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;