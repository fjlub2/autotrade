CREATE OR REPLACE FUNCTION core_src."c_Data_addTag"(p_data   IN core_dat.message_log_1.mss_data%TYPE,
                                                    p_name   IN text,
                                                    p_value  IN timestamp) RETURNS core_dat.message_log_1.mss_data%TYPE AS
$BODY$
BEGIN
  RETURN core_src."c_Data_addTag"(p_data := p_data, p_name := p_name, p_value := TO_CHAR(p_value, 'YYYY-MM-DD HH24:MI:SS'));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;