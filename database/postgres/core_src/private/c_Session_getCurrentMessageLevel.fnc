CREATE OR REPLACE FUNCTION core_src."c_Session_getCurrentMessageLevel"() RETURNS core_dat.users.usr_message_level%TYPE AS
$BODY$
BEGIN
  RETURN CURRENT_SETTING('session_context.message_level');
EXCEPTION
  WHEN UNDEFINED_OBJECT THEN
    RETURN NULL;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;