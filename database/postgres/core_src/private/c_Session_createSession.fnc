CREATE OR REPLACE FUNCTION core_src."c_Session_createSession"(p_user            IN core_dat.users.usr_username%TYPE,
                                                              p_pw              IN core_dat.users.usr_password%TYPE,
                                                              p_userIp          IN core_dat.user_sessions.uss_user_ip%TYPE,
                                                              p_lngCode         IN core_dat.languages.lng_code%TYPE,
                                                              p_platform        IN core_dat.user_sessions.uss_platform%TYPE,
                                                              p_referrer        IN core_dat.user_sessions.uss_referrer%TYPE,
                                                              p_agent           IN core_dat.user_sessions.uss_agent%TYPE) RETURNS core_dat.user_sessions.uss_db_session_code%TYPE AS
$BODY$
DECLARE
  l_row        core_dat.user_sessions;
  l_lngId      core_dat.languages.lng_id%TYPE;
  l_out        core_dat.user_sessions.uss_db_session_code%TYPE;
  l_ussId      core_dat.user_sessions.uss_id%TYPE;
  l_ussRow     core_dat.user_sessions%ROWTYPE;
  l_updateRow  core_tapi."t_Users_updateRow";
  l_includeRow core_tapi."t_Users_updateInclude";
  l_usrRow     core_dat.users%ROWTYPE;
  l_invalid    BOOLEAN;
BEGIN
  PERFORM core_src."c_Session_clearContext"();
  
  l_invalid := TRUE;
  
  l_usrRow := core_tapi."t_Users_getRow_UK"(p_uk := p_user);
  
  IF p_user            IS NOT NULL AND
     p_pw              IS NOT NULL AND 
     l_usrRow.usr_id   IS NOT NULL THEN
    IF core_src."c_Security_checkPassword"(p_usrId := l_usrRow.usr_id,
                                           p_pw    := p_pw) = 'Y' THEN 
      IF core_tapi."t_CodeValues_getValue_PK"('config_values', 'multi_session') = 'N' THEN
        --This UPDATE will terminate all other active sessions for the same user.
        UPDATE user_sessions
           SET uss_session_end  = NOW()
         WHERE uss_usr_id       = l_usrRow.usr_id
           AND uss_session_end IS NULL;
      END IF;
    
      -- work out users language
      l_lngId := core_tapi."t_Languages_getId_UK"(p_uk := SUBSTR(LOWER(p_lngCode),1,2));
    
      IF l_lngId   IS     NULL AND 
         p_lngCode IS NOT NULL THEN
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Language not found when starting session.',
                                                p_data    := core_src."c_Data_addTag"('LNG_CODE',p_lngCode),
                                                p_type    := 'I');
      END IF;
    
      SELECT CONCAT(NEXTVAL('core_dat.uss_code_seq')::text,string_agg(md5(random()::TEXT), ''))
        INTO l_out
        FROM generate_series(1, CEIL(128 / 32.)::integer);
      
      l_ussRow.uss_usr_id            := l_usrRow.usr_id;
      l_ussRow.uss_session_start     := NOW();
      l_ussRow.uss_user_ip           := p_userIp;
      l_ussRow.uss_lng_id            := l_lngId;
      l_ussRow.uss_db_session_code   := l_out;
      l_ussRow.uss_last_used         := NOW();
      l_ussRow.uss_platform          := p_platform;
      l_ussRow.uss_referrer          := p_referrer;
      l_ussRow.uss_agent             := p_agent;
  
      l_ussId := core_tapi."t_UserSessions_insertRow" (p_row := l_ussRow);
    
      PERFORM core_src."c_Session_setContext" (p_dbSessionCode := l_out);
    
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Session started.',
                                              p_data    := core_src."c_Data_addTag"('USR_ID',l_usrRow.usr_id),
                                              p_type    := 'I');
                                              
      l_updateRow.usr_last_successful_login   := NOW();
      l_updateRow.usr_prev_successful_login   := l_usrRow.usr_last_successful_login;
      l_updateRow.usr_total_successful_logins := l_usrRow.usr_total_successful_logins + 1;
      l_updateRow.usr_pwd_tries               := 0;
    
      l_includeRow.usr_last_successful_login   := 'Y';
      l_includeRow.usr_prev_successful_login   := 'Y';
      l_includeRow.usr_total_successful_logins := 'Y';
      l_includeRow.usr_pwd_tries               := 'Y';
    
      l_invalid := FALSE;
    ELSE
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Session not started.',
                                              p_data    := core_src."c_Data_addTag"('USR_ID',l_usrRow.usr_id),
                                              p_type    := 'I');
    
      l_updateRow.usr_last_failed_login   := NOW();
      l_updateRow.usr_total_failed_logins := l_usrRow.usr_total_failed_logins + 1;
      l_updateRow.usr_pwd_tries           := l_usrRow.usr_pwd_tries + 1;
    
      l_includeRow.usr_last_failed_login   := 'Y';
      l_includeRow.usr_total_failed_logins := 'Y';
      l_includeRow.usr_pwd_tries           := 'Y';
    END IF;
    
    PERFORM core_tapi."t_Users_updateRow"(p_pk := l_usrRow.usr_id, p_row := l_updateRow, p_include := l_includeRow);
  END IF;

  IF l_invalid THEN
    l_out := NULL;
    
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0007', -- invalid login credentials
                                            p_data   := core_src."c_Data_addTag"(
                                                          core_src."c_Data_addTag"(
                                                            core_src."c_Data_addTag"('USER_IP', p_userIp),
                                                                                   'USERNAME', p_user),
                                                                                 'USR_ID', l_usrRow.usr_id),
                                            p_type   := 'E');
  END IF;
  
  RETURN l_out;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;