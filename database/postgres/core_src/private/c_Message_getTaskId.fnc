CREATE OR REPLACE FUNCTION core_src."c_Message_getTaskId" () RETURNS integer AS
$BODY$
BEGIN        
  RETURN NEXTVAL('core_dat.mss_task_seq');
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
