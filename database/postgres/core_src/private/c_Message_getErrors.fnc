CREATE OR REPLACE FUNCTION core_src."c_Message_getErrors" (p_taskId IN core_dat.message_log_1.mss_task_id%TYPE) RETURNS SETOF core_dat.message_log_1 AS
$BODY$
BEGIN
  IF core_tapi."t_CodeValues_getValue_PK"('config_values', 'message_table') = '1' THEN
    RETURN QUERY
    SELECT mss.*
      FROM core_dat.message_log_1 mss
        LEFT JOIN core_dat.message_codes ON mss_msc_id = msc_id
     WHERE mss_task_id = p_taskId
       AND mss_type    = 'E';
  ELSE 
    RETURN QUERY
    SELECT mss.*
      FROM core_dat.message_log_2 mss
        LEFT JOIN core_dat.message_codes ON mss_msc_id = msc_id
     WHERE mss_task_id = p_taskId
       AND mss_type    = 'E';
  END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
