CREATE OR REPLACE FUNCTION core_src."c_Security_encryptPassword" (p_usrId IN core_dat.users.usr_id%TYPE,
                                                                  p_pw    IN text) RETURNS core_dat.users.usr_password%TYPE AS
$BODY$
DECLARE
  l_out     core_dat.users.usr_password%TYPE;
BEGIN
  l_out := DECODE(REVERSE(CRYPT(CONCAT(p_usrId,p_pw), gen_salt('bf',10))),'escape');
  
  RETURN l_out;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;