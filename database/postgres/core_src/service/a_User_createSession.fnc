CREATE OR REPLACE FUNCTION core_src."a_User_createSession"(p_user IN core_dat.users.usr_username%TYPE,
                                                           p_pw   IN core_dat.users.usr_password%TYPE,
                                                           p_body IN text) RETURNS json AS
$BODY$
DECLARE
  l_json    JSON;
  
  l_userIp           core_dat.user_sessions.uss_user_ip%TYPE;
  l_lngCode          core_dat.languages.lng_code%TYPE;
  l_platform         core_dat.user_sessions.uss_platform%TYPE;
  l_referrer         core_dat.user_sessions.uss_referrer%TYPE;
  l_agent            core_dat.user_sessions.uss_agent%TYPE;
  l_sessionCode      core_dat.user_sessions.uss_db_session_code%TYPE;
  l_body             JSON;
  l_taskId           integer;
  
  l_data     text;
  l_debug    text;
  l_context  text;
BEGIN
  l_taskId  := core_src."c_Message_getTaskId" ();
  
  IF COALESCE(p_user,'') = '' THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Username'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF COALESCE(p_pw,'') = '' THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Password'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  BEGIN
    l_body := p_body::json;
  EXCEPTION
    WHEN OTHERS THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0003', -- body invalid
                                              p_data   := core_src."c_Data_addTag"('BODY',p_body),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
  END;
  
  l_userIp          := json_extract_path_text(l_body,'userIp');
  l_lngCode         := json_extract_path_text(l_body,'userLocale');
  l_platform        := json_extract_path_text(l_body,'platform');
  l_referrer        := json_extract_path_text(l_body,'referrer');
  l_agent           := json_extract_path_text(l_body,'agent');
  
  IF COALESCE(l_userIp,'') = '' THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','User IP Address'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "messages"
              FROM (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))) "items"
                      FROM (SELECT COALESCE(msc_html_code,core_tapi."t_MessageCodes_getHtmlCode_UK"('T0001')) "code",
                                   mss_message "message"
                              FROM core_src."c_Message_getErrors"(p_taskId := l_taskId)
                                LEFT JOIN core_dat.message_codes ON mss_msc_id = msc_id) a) b) c;
    
    RETURN l_json;
  END IF;
  
  l_sessionCode := core_src."c_Session_createSession"(p_user,p_pw,l_userIp,l_lngCode,l_platform,l_referrer,l_agent);
  
  IF l_sessionCode IS NOT NULL THEN
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "sessionData",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT l_sessionCode "sessionCode",
                           core_tapi."t_Users_getId_UK"(p_uk := p_user) "userId") a) b;
  ELSE
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "messages"
              FROM (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))) "items"
                      FROM (SELECT core_tapi."t_MessageCodes_getHtmlCode_UK"('T0007') code,
                                   core_src."c_Message_getMessageText"('T0007', NULL) message) a) b) c;
  END IF;
            
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
