CREATE OR REPLACE FUNCTION core_src."a_Message_getMessageText" (p_code  IN text,
                                                                p_vars  IN core_dat.tr_data[]) RETURNS core_dat.message_codes.msc_default_text%TYPE AS
$BODY$
DECLARE
  l_out    core_dat.message_log_1.mss_message%TYPE;
BEGIN
  RETURN core_src."c_Message_getMessageText"(p_code := p_code, p_vars := p_vars);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
