CREATE OR REPLACE FUNCTION core_src."a_User_setContext"(p_sessionCode IN core_dat.user_sessions.uss_db_session_code%TYPE) RETURNS json AS
$BODY$
DECLARE
  l_json    JSON;
  
  l_userIp           core_dat.user_sessions.uss_user_ip%TYPE;
  l_lngCode          core_dat.languages.lng_code%TYPE;
  l_taskId           INTEGER;
  
  l_data     text;
  l_debug    text;
  l_context  text;
BEGIN
  l_taskId := core_src."c_Session_setContext" (p_dbSessionCode := p_sessionCode);
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE   
    l_json := NULL;
  END IF;
  
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
