-- read / write
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'users',               'core_tapi', 'usr', 'N', 'usr_seq');
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'user_sessions',       'core_tapi', 'uss', 'N', 'uss_seq');
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'temp_links',          'core_tapi', 'tln', 'N', 'tln_seq');
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'signup',              'core_tapi', 'sgn', 'N', 'sgn_seq');
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'message_log_1',       'core_tapi', 'mss', 'N', 'mss_seq');
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'message_log_2',       'core_tapi', 'mss', 'N', 'mss_seq');
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'mail_queue',          'core_tapi', 'mlq', 'N', 'mlq_seq');

-- read only
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'code_values',         'core_tapi', 'cv',  'N', NULL);
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'languages',           'core_tapi', 'lng', 'Y', NULL);
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'message_codes',       'core_tapi', 'msc', 'Y', NULL);
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'message_substitutes', 'core_tapi', 'msu', 'Y', NULL);
SELECT core_tapi."createTapi"('tradeignite', 'core_dat', 'message_text',        'core_tapi', 'mst', 'Y', NULL);