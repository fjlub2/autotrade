CREATE OR REPLACE FUNCTION core_tapi."createTapi"(pi_srcCatalog character varying, pi_srcSchema character varying, pi_srcTable character varying, pi_dstSchema character varying, pi_alias character varying, pi_readOnly character DEFAULT 'N', pi_seqName character DEFAULT NULL)
  RETURNS text AS
$BODY$
DECLARE
  l_out              text;
  l_temp             text;
  
  l_tableAlias       text;
  
  l_pkColName1       character varying;
  l_pkColName2       character varying;
  l_ukColName1       character varying;
  l_ukColName2       character varying;
  
  l_updateParams     text;
  l_updateCols       text;
  l_updateIncludes   text;
  l_insertParams     text;
  l_insertCols       text;
  
  r                  RECORD;
BEGIN
  SELECT REPLACE(INITCAP(REGEXP_REPLACE(pi_srcTable,'^_*','')),'_','')
    INTO l_tableAlias;

  SELECT column_name
    INTO l_pkColName1
    FROM information_schema.table_constraints tc
      JOIN information_schema.key_column_usage kc ON tc.constraint_catalog = kc.constraint_catalog
                                                 AND tc.constraint_schema  = kc.constraint_schema
                                                 AND tc.constraint_name    = kc.constraint_name
   WHERE tc.constraint_catalog = pi_srcCatalog
     AND tc.table_schema       = pi_srcSchema
     AND tc.table_name         = pi_srcTable
     AND constraint_type       = 'PRIMARY KEY'
     AND ordinal_position      = 1;

  IF NOT FOUND THEN
    RAISE EXCEPTION 'No primary key found';
  END IF;

  BEGIN
    SELECT column_name
      INTO STRICT l_pkColName2
      FROM information_schema.table_constraints tc
        JOIN information_schema.key_column_usage kc ON tc.constraint_catalog = kc.constraint_catalog
                                                   AND tc.constraint_schema  = kc.constraint_schema
                                                   AND tc.constraint_name    = kc.constraint_name
     WHERE tc.constraint_catalog = pi_srcCatalog
       AND tc.table_schema       = pi_srcSchema
       AND tc.table_name         = pi_srcTable
       AND constraint_type       = 'PRIMARY KEY'
       AND ordinal_position     != 1;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
    WHEN TOO_MANY_ROWS THEN
      RAISE EXCEPTION 'Primary key has more than 2 columns';
  END;
    

  SELECT column_name
    INTO l_ukColName1
    FROM information_schema.table_constraints tc
      JOIN information_schema.key_column_usage kc ON tc.constraint_catalog = kc.constraint_catalog
                                                 AND tc.constraint_schema  = kc.constraint_schema
                                                 AND tc.constraint_name    = kc.constraint_name
   WHERE tc.constraint_catalog = pi_srcCatalog
     AND tc.table_schema       = pi_srcSchema
     AND tc.table_name         = pi_srcTable
     AND constraint_type       = 'UNIQUE'
     AND ordinal_position      = 1;

  IF FOUND THEN
    BEGIN
      SELECT column_name
        INTO l_ukColName2
        FROM information_schema.table_constraints tc
          JOIN information_schema.key_column_usage kc ON tc.constraint_catalog = kc.constraint_catalog
                                                     AND tc.constraint_schema  = kc.constraint_schema
                                                     AND tc.constraint_name    = kc.constraint_name
       WHERE tc.constraint_catalog = pi_srcCatalog
         AND tc.table_schema       = pi_srcSchema
         AND tc.table_name         = pi_srcTable
         AND constraint_type       = 'UNIQUE'
         AND ordinal_position     != 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
      WHEN TOO_MANY_ROWS THEN -- Not interested in UK's with more than 2 columns
        l_ukColName1 := NULL;
        l_ukColName2 := NULL;
    END;
  END IF;
  
  -- drop all tapi code for the supplied table
  l_temp := core_tapi."dropTapi"(pi_schema := pi_dstSchema, pi_table := pi_srcTable);

  -- create getRow function
  l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_getRow"(p_pk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName1,'%TYPE',CASE WHEN l_pkColName2 IS NOT NULL THEN CONCAT(', p_pk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName2,'%TYPE') ELSE NULL END, CASE WHEN l_ukColName1 IS NOT NULL THEN CONCAT(', p_uk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_ukColName1,'%TYPE DEFAULT NULL',CASE WHEN l_ukColName2 IS NOT NULL THEN CONCAT(', p_uk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_ukColName2,'%TYPE DEFAULT NULL') ELSE NULL END) ELSE NULL END,') RETURNS ',pi_srcSchema,'.',pi_srcTable,' AS',
                  E'\n', '$','BODY$',
                  E'\n', 'DECLARE',
                  E'\n', '  l_row   ',pi_srcSchema,'.',pi_srcTable,';',
                  E'\n', 'BEGIN',
                  E'\n', '  IF p_pk IS NOT NULL THEN',
                  E'\n', '    SELECT *',
                  E'\n', '      INTO l_row',
                  E'\n', '      FROM ',pi_srcSchema,'.',pi_srcTable,
                  E'\n', '     WHERE ',l_pkColName1,' = p_pk',
                  CASE WHEN l_pkColName2 IS NOT NULL THEN
                  CONCAT(E'\n',
                         '       AND ',l_pkColName2,' = p_pk2;') ELSE ';' END,
                  CASE WHEN l_ukColName1 IS NOT NULL THEN
                  CONCAT(E'\n',
                         '  ELSIF p_uk IS NOT NULL THEN',
                  E'\n', '    SELECT *',
                  E'\n', '      INTO l_row',
                  E'\n', '      FROM ',pi_srcSchema,'.',pi_srcTable,
                  E'\n', '     WHERE ',l_ukColName1,' = p_uk',
                    CASE WHEN l_ukColName2 IS NOT NULL THEN
                    CONCAT(E'\n',
                         '       AND ',l_ukColName2,' = p_uk2;') ELSE ';' END) ELSE NULL END,
                  E'\n', '  END IF;',
                  E'\n', 
                  E'\n', '  RETURN l_row;',
                  E'\n', 'END;',
                  E'\n', '$','BODY$',
                  E'\n', '  LANGUAGE plpgsql;');

  EXECUTE l_out;
  
  -- create getRow_PK function
  l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_getRow_PK"(p_pk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName1,'%TYPE',CASE WHEN l_pkColName2 IS NOT NULL THEN CONCAT(', p_pk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName2,'%TYPE') ELSE NULL END,') RETURNS ',pi_srcSchema,'.',pi_srcTable,' AS',
                  E'\n', '$','BODY$',
                  E'\n', 'BEGIN',
                  E'\n', '  RETURN ',pi_dstSchema,'."t_',l_tableAlias,'_getRow"(p_pk := p_pk',CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 := p_pk2' END,');',
                  E'\n', 'END;',
                  E'\n', '$','BODY$',
                  E'\n', '  LANGUAGE plpgsql;');
                  
  EXECUTE l_out;
  
  -- create exists_PK function
  l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_exists_PK"(p_pk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName1,'%TYPE',CASE WHEN l_pkColName2 IS NOT NULL THEN CONCAT(', p_pk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName2,'%TYPE') ELSE NULL END,') RETURNS character AS',
                  E'\n', '$','BODY$',
                  E'\n', 'DECLARE',
                  E'\n', '  l_row   ',pi_srcSchema,'.',pi_srcTable,';',
                  E'\n', 'BEGIN',
                  E'\n', '  l_row := ',pi_dstSchema,'."t_',l_tableAlias,'_getRow"(p_pk := p_pk',CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 := p_pk2' END,');',
                  E'\n',
                  E'\n', '  IF l_row.',l_pkColName1,' IS NOT NULL THEN',
                  E'\n', '    RETURN ''Y'';',
                  E'\n', '  ELSE',
                  E'\n', '    RETURN ''N'';',
                  E'\n', '  END IF;',
                  E'\n', 'END;',
                  E'\n', '$','BODY$',
                  E'\n', '  LANGUAGE plpgsql;');
                  
  EXECUTE l_out;
  
  IF l_ukColName1 IS NOT NULL THEN
    -- create getRow_UK function
    l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_getRow_UK"(p_uk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_ukColName1,'%TYPE',CASE WHEN l_ukColName2 IS NOT NULL THEN CONCAT(', p_uk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_ukColName2,'%TYPE') ELSE NULL END,') RETURNS ',pi_srcSchema,'.',pi_srcTable,' AS',
                    E'\n', '$','BODY$',
                    E'\n', 'BEGIN',
                    E'\n', '  RETURN ',pi_dstSchema,'."t_',l_tableAlias,'_getRow"(p_pk := NULL',CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 := NULL' END,', p_uk := p_uk',CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 := p_uk2' END,');',
                    E'\n', 'END;',
                    E'\n', '$','BODY$',
                    E'\n', '  LANGUAGE plpgsql;');
                  
    EXECUTE l_out;
  
    -- create exists_UK function
    l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_exists_UK"(p_uk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_ukColName1,'%TYPE',CASE WHEN l_ukColName2 IS NOT NULL THEN CONCAT(', p_uk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_ukColName2,'%TYPE') ELSE NULL END,') RETURNS character AS',
                    E'\n', '$','BODY$',
                    E'\n', 'DECLARE',
                    E'\n', '  l_row   ',pi_srcSchema,'.',pi_srcTable,';',
                    E'\n', 'BEGIN',
                    E'\n', '  l_row := ',pi_dstSchema,'."t_',l_tableAlias,'_getRow"(p_pk := NULL',CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 := NULL' END,', p_uk := p_uk',CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 := p_uk2' END,');',
                    E'\n',
                    E'\n', '  IF l_row.',l_pkColName1,' IS NOT NULL THEN',
                    E'\n', '    RETURN ''Y'';',
                    E'\n', '  ELSE',
                    E'\n', '    RETURN ''N'';',
                    E'\n', '  END IF;',
                    E'\n', 'END;',
                    E'\n', '$','BODY$',
                    E'\n', '  LANGUAGE plpgsql;');
                  
    EXECUTE l_out;
  END IF;
  
  -- create getters
  FOR r IN (SELECT column_name,
                   REPLACE(INITCAP(REGEXP_REPLACE(column_name,'^'||pi_alias||'_*','')),'_','') column_alias
              FROM information_schema.columns
             WHERE table_catalog = pi_srcCatalog
               AND table_schema  = pi_srcSchema
               AND table_name    = pi_srcTable) LOOP
    l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_get',r.column_alias,'_PK"(p_pk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName1,'%TYPE',CASE WHEN l_pkColName2 IS NOT NULL THEN CONCAT(', p_pk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName2,'%TYPE') ELSE NULL END,') RETURNS ',pi_srcSchema,'.',pi_srcTable,'.',r.column_name,'%TYPE AS',
                    E'\n', '$','BODY$',
                    E'\n', 'DECLARE',
                    E'\n', '  l_row   ',pi_srcSchema,'.',pi_srcTable,';',
                    E'\n', 'BEGIN',
                    E'\n', '  l_row := ',pi_dstSchema,'."t_',l_tableAlias,'_getRow"(p_pk := p_pk',CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 := p_pk2' END,');',
                    E'\n',
                    E'\n', '  RETURN l_row.',r.column_name,';',
                    E'\n', 'END;',
                    E'\n', '$','BODY$',
                    E'\n', '  LANGUAGE plpgsql;');
                    
    EXECUTE l_out;
                    
    IF l_ukColName1 IS NOT NULL THEN
      l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_get',r.column_alias,'_UK"(p_uk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_ukColName1,'%TYPE',CASE WHEN l_ukColName2 IS NOT NULL THEN CONCAT(', p_uk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_ukColName2,'%TYPE') ELSE NULL END,') RETURNS ',pi_srcSchema,'.',pi_srcTable,'.',r.column_name,'%TYPE AS',
                      E'\n', '$','BODY$',
                      E'\n', 'DECLARE',
                      E'\n', '  l_row   ',pi_srcSchema,'.',pi_srcTable,';',
                      E'\n', 'BEGIN',
                      E'\n', '  l_row := ',pi_dstSchema,'."t_',l_tableAlias,'_getRow"(p_pk := NULL',CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 := NULL' END,', p_uk := p_uk',CASE WHEN l_ukColName2 IS NULL THEN NULL ELSE ', p_uk2 := p_uk2' END,');',
                      E'\n',
                      E'\n', '  RETURN l_row.',r.column_name,';',
                      E'\n', 'END;',
                      E'\n', '$','BODY$',
                      E'\n', '  LANGUAGE plpgsql;');
                      
      EXECUTE l_out;
    END IF;
  END LOOP;
  
  IF pi_readOnly = 'N' THEN
    -- create deleteRow function
    l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_deleteRow"(p_pk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName1,'%TYPE',CASE WHEN l_pkColName2 IS NOT NULL THEN CONCAT(', p_pk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName2,'%TYPE') ELSE NULL END,') RETURNS void AS',
                    E'\n', '$','BODY$',
                    E'\n', 'BEGIN',
                    E'\n', '  IF p_pk IS NOT NULL THEN',
                    E'\n', '    DELETE',
                    E'\n', '      FROM ',pi_srcSchema,'.',pi_srcTable,
                    E'\n', '     WHERE ',l_pkColName1,' = p_pk',
                    CASE WHEN l_pkColName2 IS NOT NULL THEN
                    CONCAT(E'\n',
                           '       AND ',l_pkColName2,' = p_pk2;') ELSE ';' END,
                    E'\n', '  END IF;',
                    E'\n',
                    E'\n', '  RETURN;',
                    E'\n', 'END;',
                    E'\n', '$','BODY$',
                    E'\n', '  LANGUAGE plpgsql;');
                      
    EXECUTE l_out;
    
    -- create insertRow function
    l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_insertRow"(p_row IN ',pi_srcSchema,'.',pi_srcTable,', p_newPk OUT ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName1,'%TYPE',CASE WHEN l_pkColName2 IS NOT NULL THEN CONCAT(', p_newPk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName2,'%TYPE') ELSE NULL END,') AS',
                    E'\n', '$','BODY$',
                    E'\n', 'BEGIN',
                    CASE WHEN pi_seqName IS NOT NULL AND l_pkColName2 IS NULL THEN
             CONCAT(E'\n', '  p_row.',l_pkColName1,' := COALESCE(p_row.',l_pkColName1,', NEXTVAL(''',pi_srcSchema,'.',pi_seqName,''')::integer);') ELSE NULL END,
                    E'\n',
                    E'\n', '  INSERT',
                    E'\n', '    INTO ',pi_srcSchema,'.',pi_srcTable,
                    E'\n', '   VALUES (p_row.*)',
                    E'\n', '  RETURNING ',l_pkColName1,CASE WHEN l_pkColName2 IS NOT NULL THEN ', l_pkColName2' ELSE NULL END,
                    E'\n', '       INTO p_newPk',CASE WHEN l_pkColName2 IS NOT NULL THEN ', p_newPk2;' ELSE ';' END,
                    E'\n',
                    E'\n', '  RETURN;',
                    E'\n', 'END;',
                    E'\n', '$','BODY$',
                    E'\n', '  LANGUAGE plpgsql;');
                      
    EXECUTE l_out;
    
    -- create update types
    l_out := CONCAT('CREATE TYPE ',pi_dstSchema,'."t_',l_tableAlias,'_updateRow" AS (');
    
    SELECT CONCAT(l_out,E'\n',string_agg(CONCAT('                              ',column_name,'   ',CASE WHEN udt_schema != 'pg_catalog' THEN CONCAT(udt_schema,'.',LTRIM(udt_name,'_'),' ',data_type) ELSE data_type END),E',\n'))
      INTO l_out
      FROM information_schema.columns
     WHERE table_catalog = pi_srcCatalog
       AND table_schema  = pi_srcSchema
       AND table_name    = pi_srcTable
       AND column_name NOT IN (l_pkColName1,COALESCE(l_pkColName2,l_pkColName1));
    
    l_out := CONCAT(l_out,');');

    EXECUTE l_out;
    
    l_out := CONCAT('CREATE TYPE ',pi_dstSchema,'."t_',l_tableAlias,'_updateInclude" AS (');
    
    SELECT CONCAT(l_out,E'\n',string_agg(CONCAT('                              ',column_name,'   character(1)'),E',\n'))
      INTO l_out
      FROM information_schema.columns
     WHERE table_catalog = pi_srcCatalog
       AND table_schema  = pi_srcSchema
       AND table_name    = pi_srcTable
       AND column_name NOT IN (l_pkColName1,COALESCE(l_pkColName2,l_pkColName1));
    
    l_out := CONCAT(l_out,');');

    EXECUTE l_out;
    
    -- create updateRow function with type input
    l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_updateRow"(p_pk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName1,'%TYPE',CASE WHEN l_pkColName2 IS NOT NULL THEN CONCAT(', p_pk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName2,'%TYPE') ELSE NULL END,', p_row IN ',pi_dstSchema,'."t_',l_tableAlias,'_updateRow", p_include IN ',pi_dstSchema,'."t_',l_tableAlias,'_updateInclude") RETURNS void AS',
                    E'\n', '$','BODY$',
                    E'\n', 'BEGIN',
                    E'\n', '  IF p_pk IS NOT NULL THEN',
                    E'\n', '    UPDATE ',pi_srcSchema,'.',pi_srcTable,
                    E'\n', '       SET ');

    SELECT CONCAT(l_out,E'\n',string_agg(CONCAT('             ',column_name,' = CASE COALESCE(p_include.',column_name,', ''N'') WHEN ''N'' THEN ',column_name,' ELSE p_row.',column_name,' END'),E',\n'))
      INTO l_out
      FROM information_schema.columns
     WHERE table_catalog = pi_srcCatalog
       AND table_schema  = pi_srcSchema
       AND table_name    = pi_srcTable
       AND column_name NOT IN (l_pkColName1,COALESCE(l_pkColName2,l_pkColName1));

    l_out := CONCAT(l_out,
                    E'\n', '     WHERE ',l_pkColName1,' = p_pk',
                    CASE WHEN l_pkColName2 IS NOT NULL THEN
                    CONCAT(E'\n',
                           '       AND ',l_pkColName2,' = p_pk2;') ELSE ';' END,
                    E'\n', '  END IF;',
                    E'\n',
                    E'\n', '  RETURN;',
                    E'\n', 'END;',
                    E'\n', '$','BODY$',
                    E'\n', '  LANGUAGE plpgsql;');
                      
    EXECUTE l_out;
    
    -- create updateRow function with column level inputs
    SELECT string_agg(CONCAT(E'\n','                      p_',REPLACE(INITCAP(REGEXP_REPLACE(column_name,'^'||pi_alias||'_*','')),'_',''),' IN ',pi_srcSchema,'.',pi_srcTable,'.',column_name,'%TYPE'),','),
           string_agg(CONCAT(E'\n','    l_updateRow.',column_name,' := p_',REPLACE(INITCAP(REGEXP_REPLACE(column_name,'^'||pi_alias||'_*','')),'_','')),';'),
           string_agg(CONCAT(E'\n','    l_includeRow.',column_name,' := ''Y'''),';')
      INTO l_updateParams,
           l_updateCols,
           l_updateIncludes
      FROM information_schema.columns
     WHERE table_catalog = pi_srcCatalog
       AND table_schema  = pi_srcSchema
       AND table_name    = pi_srcTable
       AND column_name NOT IN (l_pkColName1,COALESCE(l_pkColName2,l_pkColName1));
    
    l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_updateRow"(p_pk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName1,'%TYPE',CASE WHEN l_pkColName2 IS NOT NULL THEN CONCAT(', p_pk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName2,'%TYPE') ELSE NULL END,', ',l_updateParams,') RETURNS void AS',
                    E'\n', '$','BODY$',
                    E'\n', 'DECLARE',
                    E'\n', '  l_updateRow  ',pi_dstSchema,'."t_',l_tableAlias,'_updateRow";',
                    E'\n', '  l_includeRow ',pi_dstSchema,'."t_',l_tableAlias,'_updateInclude";',
                    E'\n', 'BEGIN',
                    E'\n', '  IF p_pk IS NOT NULL THEN',
                    E'\n',      l_updateCols,';',
                    E'\n',
                    E'\n',      l_updateIncludes,';',
                    E'\n',
                    E'\n', '    PERFORM ',pi_dstSchema,'."t_',l_tableAlias,'_updateRow"(p_pk := p_pk',CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 := p_pk2' END,', p_row := l_updateRow, p_include := l_includeRow);',
                    E'\n', '  END IF;',
                    E'\n',
                    E'\n', '  RETURN;',
                    E'\n', 'END;',
                    E'\n', '$','BODY$',
                    E'\n', '  LANGUAGE plpgsql;');
    
    EXECUTE l_out;
    
    -- create setters
    FOR r IN (SELECT column_name,
                     REPLACE(INITCAP(REGEXP_REPLACE(column_name,'^'||pi_alias||'_*','')),'_','') column_alias
                FROM information_schema.columns
               WHERE table_catalog = pi_srcCatalog
                 AND table_schema  = pi_srcSchema
                 AND table_name    = pi_srcTable
                 AND column_name NOT IN (l_pkColName1,COALESCE(l_pkColName2,l_pkColName1))) LOOP
      l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_set',r.column_alias,'"(p_pk IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName1,'%TYPE',CASE WHEN l_pkColName2 IS NOT NULL THEN CONCAT(', p_pk2 IN ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName2,'%TYPE') ELSE NULL END,', p_val IN ',pi_srcSchema,'.',pi_srcTable,'.',r.column_name,'%TYPE) RETURNS void AS',
                      E'\n', '$','BODY$',
                      E'\n', 'DECLARE',
                      E'\n', '  l_updateRow  ',pi_dstSchema,'."t_',l_tableAlias,'_updateRow";',
                      E'\n', '  l_includeRow ',pi_dstSchema,'."t_',l_tableAlias,'_updateInclude";',
                      E'\n', 'BEGIN',
                      E'\n', '  IF p_pk IS NOT NULL THEN',
                      E'\n', '    l_updateRow.',r.column_name,'  := p_val;',
                      E'\n', '    l_includeRow.',r.column_name,' := ''Y'';',
                      E'\n',
                      E'\n', '    PERFORM ',pi_dstSchema,'."t_',l_tableAlias,'_updateRow"(p_pk := p_pk',CASE WHEN l_pkColName2 IS NULL THEN NULL ELSE ', p_pk2 := p_pk2' END,', p_row := l_updateRow, p_include := l_includeRow);',
                      E'\n', '  END IF;',
                      E'\n',
                      E'\n', '  RETURN;',
                      E'\n', 'END;',
                      E'\n', '$','BODY$',
                      E'\n', '  LANGUAGE plpgsql;');
                    
      EXECUTE l_out;
    END LOOP;
    
    -- create insertRow function with column level inputs
    SELECT string_agg(CONCAT(E'\n','                      p_',REPLACE(INITCAP(REGEXP_REPLACE(column_name,'^'||pi_alias||'_*','')),'_',''),' IN ',pi_srcSchema,'.',pi_srcTable,'.',column_name,'%TYPE'),','),
           string_agg(CONCAT(E'\n','  l_insertRow.',column_name,' := p_',REPLACE(INITCAP(REGEXP_REPLACE(column_name,'^'||pi_alias||'_*','')),'_','')),';')
      INTO l_insertParams,
           l_insertCols
      FROM information_schema.columns
     WHERE table_catalog = pi_srcCatalog
       AND table_schema  = pi_srcSchema
       AND table_name    = pi_srcTable;
    
    l_out := CONCAT(       'CREATE FUNCTION ',pi_dstSchema,'."t_',l_tableAlias,'_insertRow"(',l_insertParams,', p_newPk OUT ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName1,'%TYPE',CASE WHEN l_pkColName2 IS NOT NULL THEN CONCAT(', p_newPk2 OUT ',pi_srcSchema,'.',pi_srcTable,'.',l_pkColName2,'%TYPE') ELSE NULL END,') AS',
                    E'\n', '$','BODY$',
                    E'\n', 'DECLARE',
                    E'\n', '  l_insertRow  ',pi_srcSchema,'.',pi_srcTable,';',
                    E'\n', 'BEGIN',
                    E'\n',    l_insertCols,';',
                    E'\n',
                    CASE WHEN pi_seqName IS NOT NULL AND l_pkColName2 IS NULL THEN
             CONCAT(E'\n', '  l_insertRow.',l_pkColName1,' := COALESCE(l_insertRow.',l_pkColName1,', NEXTVAL(''',pi_srcSchema,'.',pi_seqName,''')::integer);') ELSE NULL END,
                    E'\n',
                    E'\n', '  PERFORM ',pi_dstSchema,'."t_',l_tableAlias,'_insertRow"(p_row := l_insertRow, p_newPk := p_newPk',CASE WHEN l_pkColName2 IS NOT NULL THEN ', p_newPk2 := p_newPk2' ELSE NULL END,');',
                    E'\n',
                    E'\n', '  RETURN;',
                    E'\n', 'END;',
                    E'\n', '$','BODY$',
                    E'\n', '  LANGUAGE plpgsql;');
    
    EXECUTE l_out;
  END IF;
    
  RETURN 'Successfull';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
