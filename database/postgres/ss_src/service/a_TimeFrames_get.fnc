CREATE OR REPLACE FUNCTION ss_src."a_TimeFrames_get"(pi_exchange character varying, pi_contract character varying)
  RETURNS json AS
$BODY$
DECLARE
  l_json     JSON;
  l_dexhId   ss_dat.d_exchanges.dexh_id%TYPE;
  l_dsymId   ss_dat.d_symbols.dsym_id%TYPE;
  l_taskId   INTEGER;
  
  l_data     text;
  l_debug    text;
  l_context  text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Timeframes.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF COALESCE(pi_exchange,'') = '' THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Exchange'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  ELSE
    l_dexhId := ss_tapi."t_DExchanges_getId_UK"(p_uk := pi_exchange);
  
    IF l_dexhId IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','exchange'),
                                              p_data   := core_src."c_Data_addTag"('EXCHANGE_CODE',pi_exchange),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END IF;
  
  IF COALESCE(pi_contract,'') = '' THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  ELSE
    l_dsymId := ss_tapi."t_DSymbols_getId_UK"(p_uk  := pi_contract,
                                              p_uk2 := l_dexhId);
  
    IF l_dsymId IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','contract'),
                                              p_data   := core_src."c_Data_addTag"('CONTRACT_CODE',pi_contract),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE 
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "timeframes",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))) "items"
                      FROM (SELECT dtfr_mins::text "code",
                                   dtfr_name       "desc",
                                   REPLACE(LEAST(ROW_NUMBER() OVER (ORDER BY 1),6)::text,'6','') "recent"
                              FROM ss_dat.d_symbols
                                JOIN ss_dat.d_symbol_timeframes ON dsyt_dsym_id = dsym_id
                                JOIN ss_dat.d_timeframes        ON dsyt_dtfr_id = dtfr_id
                             WHERE dsym_id = l_dsymId
                             ORDER BY dtfr_mins DESC) a) b) c;
                           
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning timeframes.',
                                            p_data    := core_src."c_Data_addTag"('DSYM_ID',l_dsymId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
   END IF;
   
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql STABLE;
  
CREATE OR REPLACE FUNCTION ss_src."a_TimeFrames_get"(p_ustId ss_dat.user_strategies.ust_id%TYPE) RETURNS json AS
$BODY$
BEGIN
  RETURN ss_src."a_TimeFrames_get"(p_ustId := p_ustId, p_uctId := NULL, p_areaType := NULL);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
  
CREATE OR REPLACE FUNCTION ss_src."a_TimeFrames_get"(p_ustId    ss_dat.user_strategies.ust_id%TYPE,
                                                     p_areaType ss_dat.user_charts.uct_area_type%TYPE) RETURNS json AS
$BODY$
BEGIN
  RETURN ss_src."a_TimeFrames_get"(p_ustId := p_ustId, p_uctId := NULL, p_areaType := p_areaType);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
  
  
CREATE OR REPLACE FUNCTION ss_src."a_TimeFrames_get"(p_ustId    ss_dat.user_strategies.ust_id%TYPE,
                                                     p_uctId    ss_dat.user_charts.uct_id%TYPE,
                                                     p_areaType ss_dat.user_charts.uct_area_type%TYPE)
  RETURNS json AS
$BODY$
DECLARE
  l_json     JSON;
  l_uctRow   ss_dat.user_charts%ROWTYPE;
  l_taskId   INTEGER;
  
  l_data     text;
  l_debug    text;
  l_context  text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Strategy Timeframes.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF p_uctId IS NOT NULL THEN
    l_uctRow := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);
    
    IF l_uctRow.uct_id IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- chart doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    l_uctRow.uct_dtfr_id := -1;
    
    IF COALESCE(p_areaType,'') != '' THEN
      IF p_areaType NOT IN ('entry','exit') THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                                p_vars   := core_src."c_Data_addTag"('FIELD','area type'),
                                                p_data   := core_src."c_Data_addTag"('UCT_AREA_TYPE',p_areaType),
                                                p_type   := 'F',
                                                p_taskId := l_taskId);
      END IF;
      
      l_uctRow.uct_area_type := p_areaType;
    ELSE
      l_uctRow.uct_area_type := 'entry';
    END IF;
  END IF;
  
  IF p_ustId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Strategy'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  ELSE
    IF ss_tapi."t_UserStrategies_exists_PK"(p_pk := p_ustId) = 'N' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','strategy'),
                                              p_data   := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE 
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "timeframes",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))) "items"
                      FROM (SELECT dtfr_mins::text "code",
                                   dtfr_name       "desc",
                                   REPLACE(LEAST(ROW_NUMBER() OVER (ORDER BY 1),6)::text,'6','') "recent"
                              FROM ss_dat.user_strategies
                                JOIN ss_dat.d_symbol_timeframes ON dsyt_dsym_id = ust_dsym_id
                                JOIN ss_dat.d_timeframes        ON dsyt_dtfr_id = dtfr_id
                             WHERE ust_id = p_ustId
                               AND dtfr_id NOT IN (SELECT uct_dtfr_id
                                                     FROM ss_dat.user_charts
                                                    WHERE uct_ust_id    = ust_id
                                                      AND uct_dtfr_id  != l_uctRow.uct_dtfr_id
                                                      AND uct_area_type = l_uctRow.uct_area_type)
                             ORDER BY CASE dtfr_id WHEN l_uctRow.uct_dtfr_id THEN 1 ELSE 2 END,
                                      dtfr_mins DESC) a) b) c;
                           
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning strategy timeframes.',
                                            p_data    := core_src."c_Data_addTag"(
                                                           core_src."c_Data_addTag"('UST_ID',p_ustId),
                                                                                  'UCT_ID',p_uctId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
   END IF;
   
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;