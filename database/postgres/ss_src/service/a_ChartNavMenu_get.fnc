CREATE OR REPLACE FUNCTION ss_src."a_ChartNavMenu_get"(p_uctId ss_dat.user_charts.uct_id%TYPE)
  RETURNS json AS
$BODY$
DECLARE
  l_html      TEXT;
  l_json      JSON;

  l_taskId    INTEGER;
  l_text      TEXT;
  l_data      TEXT;
  l_debug     TEXT;
  l_context   TEXT;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Chart Nav Menu.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
                                          
  l_html := ss_src."c_ChartNavMenu_get"(p_uctId := p_uctId);
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "chartNavMenu",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT l_html "menuHTML") a) b;
   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Chart Nav Menu.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
   
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
  