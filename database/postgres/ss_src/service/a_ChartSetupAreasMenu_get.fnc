CREATE OR REPLACE FUNCTION ss_src."a_ChartSetupAreasMenu_get"(p_uctId ss_dat.user_charts.uct_id%TYPE)
  RETURNS json AS
$BODY$
DECLARE
  l_json     JSON;
  l_uctRow   ss_dat.user_charts%ROWTYPE;
  l_ustRow   ss_dat.user_strategies%ROWTYPE;

  l_taskId   INTEGER;
  
  l_text     TEXT;
  l_data     TEXT;
  l_debug    TEXT;
  l_context  TEXT;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Setup Area Menu.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF p_uctId IS NOT NULL THEN
    l_uctRow   := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);
    l_ustRow   := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := l_uctRow.uct_ust_id);
    
    IF l_uctRow.uct_id     IS NULL OR
       l_ustRow.ust_usr_id IS NULL OR
       l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- chart doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE  
    l_text := '<table class="setuparea_menu_detail" ><tr><td style="width:33%"><label for="setupAreaEntryFromlbl" id="setupAreaEntryFromlbl">'||core_src."a_Message_getMessageText"('T0020',NULL)||'</label></td></tr></table><div class="uiLightness"><table class="setupAreaEntryDetail"><tr><td><input type="text" id="setupAreaEntryFrom" name="setupAreaEntryFrom"';
    
    IF l_uctRow.uct_area_from IS NOT NULL THEN
      l_text := l_text||' value="'||TO_CHAR(l_uctRow.uct_area_from,'YYYY-MM-DD HH24:MI:SS')||'"';
    END IF;
    
    l_text := l_text||' disabled/></td><td><a href="#" onclick="setupAreaSelection(''Entry'', ''Input'', ''range'');"><img id="setupAreaEntryFromIcon" title="Highlight all points for your trade entry criteria" alt="busy..." src="../assets/default/images/content/SelectChart.svg" width="20" height="20"/></td><td style="padding-top: 6px"><input type="hidden" id="setupAreaEntryColor" value="';
    
    IF l_uctRow.uct_area_colour IS NOT NULL THEN
      l_text := l_text||l_uctRow.uct_area_colour;
    ELSE
      l_text := l_text||'#00B050';
    END IF;
    
    l_text := l_text||'" /></td></tr><tr><td><input type="text" id="setupAreaEntryTo" name="setupAreaEntryTo"';
    
    IF l_uctRow.uct_area_to IS NOT NULL THEN
      l_text := l_text||' value="'||TO_CHAR(l_uctRow.uct_area_to,'YYYY-MM-DD HH24:MI:SS')||'"';
    END IF;
    
    l_text := l_text||' disabled/></td><td colspan="2"><a href="#" onclick="setupAreaSelection(''Entry'', ''Delete'', ''range'');"><img id="ClearAreaEntry" title="Clear selected area" alt="busy..." src="../assets/default/images/content/remove.svg" width="20" height="20"/></a></td></tr></table></div><br><script type="text/javascript">$("#setupAreaEntryColor").colorpicker({showOn:"button"}) .on('change.color', function(evt, color){setupAreaSelection(''Entry'', ''Input'', ''colour'');});</script>';
      
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "chartSetupAreaMenu",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT TO_CHAR(l_uctRow.uct_area_from,'YYYY-MM-DD HH24:MI:SS') "areaFromDate",
                           TO_CHAR(l_uctRow.uct_area_to,'YYYY-MM-DD HH24:MI:SS') "areaToDate",
                           l_text "menuHTML") a) b;
   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Setup Area Menu.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
   END IF;
   
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
  