CREATE OR REPLACE FUNCTION ss_src."a_Products_get"(pi_exchange character varying)
  RETURNS json AS
$BODY$
DECLARE
  l_json     JSON;
  l_dexhId   ss_dat.d_exchanges.dexh_id%TYPE;
  l_taskId   INTEGER;
  
  l_data     text;
  l_debug    text;
  l_context  text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Products.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF pi_exchange IS NOT NULL THEN
    l_dexhId := ss_tapi."t_DExchanges_getId_UK"(p_uk := pi_exchange);
    
    IF l_dexhId IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','exchange'),
                                              p_data   := core_src."c_Data_addTag"('EXCHANGE_CODE',pi_exchange),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Exchange'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE 
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "products",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))) "items"
                      FROM (SELECT dsyg_code "code",
                                   dsyg_name "desc",
                                   REPLACE(LEAST(ROW_NUMBER() OVER (ORDER BY 1),6)::text,'6','') "recent"
                              FROM ss_dat.d_symbol_groups
                             WHERE dsyg_id IN (SELECT dsym_dsyg_id
                                                 FROM ss_dat.d_symbols
                                                WHERE dsym_dexh_id = l_dexhId)
                             ORDER BY dsyg_name) a) b) c;
                           
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning products.',
                                            p_data    := core_src."c_Data_addTag"('DEXH_ID',l_dexhId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql STABLE;