CREATE OR REPLACE FUNCTION ss_src."a_Strategy_put"(p_body IN text) RETURNS json AS
$BODY$
BEGIN
  RETURN ss_src."a_Strategy_put"(p_ustId := NULL, p_body := p_body);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ss_src."a_Strategy_put"(p_ustId ss_dat.user_strategies.ust_id%TYPE, p_body IN text) RETURNS json AS
$BODY$
DECLARE
  l_json           JSON;
  l_body           JSON;
  
  l_name           ss_dat.user_strategies.ust_name%TYPE;
  l_desc           ss_dat.user_strategies.ust_desc%TYPE;
  l_position       ss_dat.user_strategies.ust_position_code%TYPE;
  l_exchange       ss_dat.d_exchanges.dexh_code%TYPE;
  l_product        ss_dat.d_symbol_groups.dsyg_code%TYPE;
  l_contract       ss_dat.d_symbols.dsym_code%TYPE;
  l_syncInd        ss_dat.user_strategies.ust_sync_ind%TYPE;
  l_syncType       ss_dat.user_strategies.ust_sync_type%TYPE;

  l_ustId          ss_dat.user_strategies.ust_id%TYPE;
  l_taskId         INTEGER;
  
  l_data           TEXT;
  l_debug          TEXT;
  l_context        TEXT;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Putting Strategy.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  BEGIN
    l_body := p_body::json;
  EXCEPTION
    WHEN OTHERS THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0003', -- body invalid
                                              p_data   := core_src."c_Data_addTag"('BODY',p_body),
                                              p_type   := 'F',
                                              p_taskId := l_taskId);
  END;
            
  SELECT json_extract_path_text(strategy,'name'),
         json_extract_path_text(strategy,'desc'),
         json_extract_path_text(strategy,'position'),
         json_extract_path_text(strategy,'exchange'),
         json_extract_path_text(strategy,'product'),
         json_extract_path_text(strategy,'contract'),
         json_extract_path_text(strategy,'syncInd'),
         json_extract_path_text(strategy,'syncType')
    INTO l_name,
         l_desc,
         l_position,
         l_exchange,
         l_product,
         l_contract,
         l_syncInd,
         l_syncType
    FROM (SELECT l_body strategy) a;

  l_ustId := ss_src."c_Strategy_store"(p_ustId     := p_ustId,
                                       p_name      := l_name,
                                       p_desc      := l_desc,
                                       p_position  := l_position,
                                       p_exchange  := l_exchange,
                                       p_product   := l_product,
                                       p_contract  := l_contract,
                                       p_syncInd   := l_syncInd,
                                       p_syncType  := l_syncType);
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE   
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "strategy",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT l_ustId "strategyId") a) b;
  END IF;
                 
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;