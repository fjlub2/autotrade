CREATE OR REPLACE FUNCTION ss_src."a_ChartSetupArea_put"(p_uctId  ss_dat.user_charts.uct_id%TYPE,
                                                         p_body   text) RETURNS json AS
$BODY$
DECLARE
  l_json           JSON;
  l_body           JSONB;
  
  l_from           ss_dat.user_charts.uct_area_from%TYPE;
  l_to             ss_dat.user_charts.uct_area_to%TYPE;
  l_colour         ss_dat.user_charts.uct_area_colour%TYPE;
  
  l_taskId         INTEGER;
  l_data           TEXT;
  l_debug          TEXT;
  l_context        TEXT;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Putting Chart Setup Area.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);

  BEGIN
    l_body := p_body::jsonb;
  EXCEPTION
    WHEN OTHERS THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0003', -- body invalid
                                              p_type   := 'F',
                                              p_taskId := l_taskId);
  END;
  
  SELECT CASE chart ? 'from' WHEN true THEN TIMESTAMP 'epoch' + (chart ->> 'from')::numeric/1000 * INTERVAL '1 second' ELSE 'infinity'::timestamp END,
         CASE chart ? 'to' WHEN true THEN TIMESTAMP 'epoch' + (chart ->> 'to')::numeric/1000 * INTERVAL '1 second' ELSE 'infinity'::timestamp END,
         chart ->> 'colour'
    INTO l_from,
         l_to,
         l_colour
    FROM (SELECT l_body chart) a;
 
  PERFORM ss_src."c_ChartSetupArea_store"(p_uctId  := p_uctId,
                                          p_from   := l_from,
                                          p_to     := l_to,
                                          p_colour := l_colour);
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE   
    l_json := '{"messages":{"items":[{"code":200,"message":"OK"}]}}'::json;
  END IF;

  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;