CREATE OR REPLACE FUNCTION ss_src."a_Strategy_delete"(pi_ustId ss_dat.user_strategies.ust_id%TYPE) RETURNS json AS
$BODY$
DECLARE
  l_json    JSON;
  l_taskId  INTEGER;
  
  l_data    text;
  l_debug   text;
  l_context text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Deleting Strategy.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  PERFORM ss_src."c_Strategy_delete"(pi_ustId := pi_ustId);
  
  IF core_src."c_Message_errorsExist" (p_taskId := core_src."c_Session_getCurrentTaskId"()) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := core_src."c_Session_getCurrentTaskId"());
  ELSE  
    l_json := '{"messages":{"items":[{"code":200,"message":"OK"}]}}'::json;  
    
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Deleted user strategy.',
                                            p_data    := core_src."c_Data_addTag"('UST_ID',pi_ustId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;