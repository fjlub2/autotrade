CREATE OR REPLACE FUNCTION ss_src."a_ChartData_get"(p_uctId ss_dat.user_charts.uct_id%TYPE) RETURNS json AS
$BODY$
BEGIN
  RETURN ss_src."a_ChartData_get"(p_uctId := p_uctId, p_criteria := 'N');
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ss_src."a_ChartData_get"(p_uctId     ss_dat.user_charts.uct_id%TYPE,
                                                    p_criteria  CHAR)
  RETURNS json AS
$BODY$
DECLARE
  l_mainJson  JSON;
  l_studyJson JSON;
  l_ustRow    ss_dat.user_strategies%ROWTYPE;
  l_uctRow    ss_dat.user_charts%ROWTYPE;
  l_dsymRow   ss_dat.d_symbols%ROWTYPE;
  l_ct        ss_dat.chart_types.cht_chart_type%TYPE;
  l_taskId    INTEGER;
  l_from      ss_dat.user_charts.uct_from%TYPE;
  l_to        ss_dat.user_charts.uct_to%TYPE;
  
  l_data      text;
  l_debug     text;
  l_context   text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Chart Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF p_uctId IS NOT NULL THEN
    l_uctRow := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);
        
    IF l_uctRow.uct_id IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- chart doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    ELSE
      l_ustRow := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := l_uctRow.uct_ust_id);
      
      IF l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- strategy doesnt exist
                                                p_vars   := core_src."c_Data_addTag"('FIELD','strategy'),
                                                p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
      ELSE
        IF l_ustRow.ust_dsym_id IS NULL THEN
          PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0006', -- chart not complete
                                                  p_data   := core_src."c_Data_addTag"('UST_ID',l_ustRow.ust_id),
                                                  p_type   := 'E',
                                                  p_taskId := l_taskId);
        END IF;
  
        IF l_uctRow.uct_dtfr_id IS NULL OR
           l_uctRow.uct_from    IS NULL OR
           l_uctRow.uct_to      IS NULL THEN
          PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0006', -- chart not complete
                                                  p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                                  p_type   := 'E',
                                                  p_taskId := l_taskId);
        END IF;
      END IF;
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF COALESCE(p_criteria,'N') NOT IN ('Y','N') THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                            p_vars   := core_src."c_Data_addTag"('FIELD','criteria'),
                                            p_data   := core_src."c_Data_addTag"('CRITERIA',p_criteria),
                                            p_type   := 'F',
                                            p_taskId := l_taskId);
  END IF;
    
  l_ct      := ss_tapi."t_ChartTypes_getChartType_PK"(p_pk := l_uctRow.uct_cht_id);
  l_dsymRow := ss_tapi."t_DSymbols_getRow_PK"(p_pk := l_ustRow.ust_dsym_id);

  IF COALESCE(p_criteria,'') = 'Y' THEN
    l_from := l_uctRow.uct_area_from;
    l_to   := l_uctRow.uct_area_to;
  ELSE
    l_from := l_uctRow.uct_from;
    l_to   := l_uctRow.uct_to;
  END IF;

  l_studyJson := ss_src."c_StudyData_All_get"(p_uctId  := l_uctRow.uct_id,
                                              p_dsymId := l_ustRow.ust_dsym_id,
                                              p_dtfrId := l_uctRow.uct_dtfr_id,
                                              p_from   := l_from,
                                              p_to     := l_to);
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_mainJson := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE  
    SELECT ROW_TO_JSON(d)
      INTO l_mainJson
      FROM (SELECT ROW_TO_JSON(a) "chartSettings",
                   ROW_TO_JSON(c) "chartData",
                   l_studyJson    "studies",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT l_uctRow.uct_id "chartId",
                           dtfr_chart_default_period  "chartDefaultPeriod",
                           dtfr_chart_max_period      "chartMaxPeriod",
                           (SELECT JSON_AGG(ROW_TO_JSON(e))
                              FROM (SELECT chp_period "period",
                                           chp_count  "count", 
                                           chp_name   "label",
                                           CASE WHEN l_uctRow.uct_chp_id IS NOT NULL THEN
                                             CASE WHEN chp_id = l_uctRow.uct_chp_id THEN true ELSE false END
                                             ELSE CASE tcp_default_ind WHEN 'Y' THEN true ELSE false END END "selected"
                                      FROM ss_dat.timeframe_chart_periods
                                        JOIN ss_dat.chart_periods ON tcp_chp_id = chp_id
                                     WHERE tcp_dtfr_id = dtfr_id
                                     ORDER BY chp_order) e) "chartPeriods",
                           l_ct                         "chartType",
                           l_dsymRow.dsym_name          "chartTitle",
                           l_uctRow.uct_show_volume     "showVolume",
                           l_uctRow.uct_positive_colour "positiveColour",
                           l_uctRow.uct_negative_colour "negativeColour",
                           l_uctRow.uct_volume_colour   "volumeColour",
                           l_ustRow.ust_sync_ind        "syncInd",
                           l_ustRow.ust_sync_type       "syncType",
                           l_dsymRow.dsym_decimals      "decimals",
                           CASE WHEN l_uctRow.uct_from > dsyt_min_date THEN 'N' ELSE 'Y' END "atMinDate",
                           CASE WHEN l_uctRow.uct_to < dsyt_max_date THEN 'N' ELSE 'Y' END "atMaxDate",
                           EXTRACT(EPOCH FROM l_uctRow.uct_sync_from_date) * 1000 "syncFromDate",
                           EXTRACT(EPOCH FROM l_uctRow.uct_sync_to_date) * 1000 "syncToDate",
                           EXTRACT(EPOCH FROM l_uctRow.uct_area_from) * 1000 "areaFromDate",
                           EXTRACT(EPOCH FROM l_uctRow.uct_area_to) * 1000 "areaToDate",
                           core_src."a_Message_getMessageText"(CASE l_uctRow.uct_area_type WHEN 'entry' THEN 'T0020' ELSE 'T0021' END,NULL) "areaLabel",
                           l_uctRow.uct_area_colour "areaColour",
                           l_uctRow.uct_area_type "areaType"
                      FROM ss_dat.d_timeframes
                        JOIN ss_dat.d_symbol_timeframes ON dsyt_dsym_id = l_ustRow.ust_dsym_id AND dsyt_dtfr_id = dtfr_id
                     WHERE dtfr_id = l_uctRow.uct_dtfr_id) a
                CROSS
                 JOIN (SELECT COALESCE(JSON_AGG(TO_JSON(b)),'[]') "items"
                         FROM (SELECT TO_JSON(fdat_json)
                                 FROM ss_dat.f_data
                                WHERE fdat_dsym_id = l_ustRow.ust_dsym_id
                                  AND fdat_date BETWEEN l_from AND l_to
                                  AND fdat_dtfr_id = l_uctRow.uct_dtfr_id
                                ORDER BY fdat_date) b) c) d;
                                
    PERFORM ss_tapi."t_UserStrategies_setDefaultUctId"(p_pk := l_ustRow.ust_id, p_val := p_uctId);
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning chart data.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN l_mainJson;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;


