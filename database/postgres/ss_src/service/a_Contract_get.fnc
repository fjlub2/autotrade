CREATE OR REPLACE FUNCTION ss_src."a_Contract_get"(pi_exchange  ss_dat.d_exchanges.dexh_code%TYPE,
                                                   pi_contract  ss_dat.d_symbols.dsym_code%TYPE)
  RETURNS json AS
$BODY$
DECLARE
  l_json     JSON;
  l_dsymId   ss_dat.d_symbols.dsym_id%TYPE;
  l_dexhId   ss_dat.d_exchanges.dexh_id%TYPE;
  l_taskId   INTEGER;
    
  l_data     text;
  l_debug    text;
  l_context  text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Contract.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF COALESCE(pi_exchange,'') != '' THEN
    l_dexhId := ss_tapi."t_DExchanges_getId_UK"(p_uk := pi_exchange);
    
    IF l_dexhId IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','exchange'),
                                              p_data   := core_src."c_Data_addTag"('EXCHANGE_CODE',pi_exchange),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Exchange'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF COALESCE(pi_contract,'') != '' THEN
    l_dsymId := ss_tapi."t_DSymbols_getId_UK"(p_uk := pi_contract, p_uk2 := l_dexhId);
    
    IF l_dsymId IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- contract doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','contract'),
                                              p_data   := core_src."c_Data_addTag"('DSYM_CODE',pi_contract),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE 
    SELECT ROW_TO_JSON(b)
      INTO l_json             
      FROM (SELECT ROW_TO_JSON(a) "contract",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT dsym_code "code",
                           dsym_name "desc",
                           dsym_year "year",
                           (SELECT dmon_mm FROM ss_dat.d_months WHERE dmon_id = dsym_dmon_id) "month"
                      FROM ss_dat.d_symbols
                     WHERE dsym_id = l_dsymId
                     ORDER BY dsym_code DESC) a) b;
                           
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning contract.',
                                            p_data    := core_src."c_Data_addTag"('DSYM_ID',l_dsymId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql STABLE;