CREATE OR REPLACE FUNCTION ss_src."a_StudiesUsed_get"(p_uctId ss_dat.user_charts.uct_id%TYPE) RETURNS json AS
$BODY$
DECLARE
  l_json    JSON;
  l_usrId   core_dat.users.usr_id%TYPE;
  l_ustId   ss_dat.user_strategies.ust_id%TYPE;
  l_taskId  INTEGER;
  
  l_data    text;
  l_debug   text;
  l_context text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Used Studies.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);

  IF p_uctId IS NOT NULL THEN
    l_ustId := ss_tapi."t_UserCharts_getUstId_PK"(p_pk := p_uctId);
    l_usrId := ss_tapi."t_UserStrategies_getUsrId_PK"(p_pk := l_ustId);
    
    
    IF l_usrId IS NULL OR
       l_usrId != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;  
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "studies",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT COALESCE(ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))),'[]') "items"
                      FROM (SELECT dstd_code "code",
                                   ss_src."c_StudyName_get"(p_ucsId := ucs_id) "desc",
                                   ucs_id     "studyId",
                                   ucs_colour "colour",
                                   ucs_name   "studyName",
                                   ss_src."c_StudyOptions_get"(p_dstdId := NULL, p_ucsId := ucs_id, p_uctId := p_uctId) "studyHTML",
                                   COALESCE((SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(d)))
                                               FROM (SELECT param_code       "code",
                                                            param_value_text "valueText",
                                                            param_value_int  "valueInt",
                                                            param_type       "type"
                                                       FROM ss_src."c_StudyParams_get"(p_ucsId := ucs_id)) d),'[]') "params"
                              FROM ss_dat.d_studies
                                JOIN ss_dat.user_chart_studies ON ucs_dstd_id = dstd_id
                             WHERE ucs_uct_id       = p_uctId
                               AND dstd_enabled_ind = 'Y'
                             ORDER BY dstd_name) a) b) c;
                             
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Used Studies.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                            p_type    := 'I',
                                            p_taskId := l_taskId);
  END IF;
  
  RETURN l_json;  
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;