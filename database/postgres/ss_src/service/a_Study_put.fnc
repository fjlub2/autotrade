CREATE OR REPLACE FUNCTION ss_src."a_Study_put"(p_uctId     IN ss_dat.user_charts.uct_id%TYPE,
                                                p_studyCode IN ss_dat.d_studies.dstd_code%TYPE,
                                                p_body      IN text) RETURNS json AS
$BODY$
BEGIN
  RETURN ss_src."a_Study_put"(p_uctId := p_uctId, p_ucsId := NULL, p_studyCode := p_studyCode, p_body := p_body);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ss_src."a_Study_put"(p_uctId     IN ss_dat.user_charts.uct_id%TYPE,
                                                p_ucsId     IN ss_dat.user_chart_studies.ucs_id%TYPE,
                                                p_studyCode IN ss_dat.d_studies.dstd_code%TYPE,
                                                p_body      IN text) RETURNS json AS
$BODY$
DECLARE
  l_json           JSON;
  l_body           JSON;
  
  l_type           ss_dat.chart_types.cht_code%TYPE;
  l_panel          ss_dat.user_chart_studies.ucs_chart_panel%TYPE;
  l_colour         ss_dat.user_chart_studies.ucs_colour%TYPE;
  l_name           ss_dat.user_chart_studies.ucs_name%TYPE;
  l_params         JSON;

  l_ucsId          ss_dat.user_chart_studies.ucs_id%TYPE;
  l_taskId         INTEGER;
  
  l_data           TEXT;
  l_debug          TEXT;
  l_context        TEXT;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Putting Study.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  BEGIN
    l_body := p_body::json;
  EXCEPTION
    WHEN OTHERS THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0003', -- body invalid
                                              p_data   := core_src."c_Data_addTag"('BODY',p_body),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
  END;
            
  SELECT json_extract_path_text(study,'chartType'),
         json_extract_path_text(study,'panel'),
         json_extract_path_text(study,'colour'),
         json_extract_path_text(study,'params'),
         json_extract_path_text(study,'name')
    INTO l_type,
         l_panel,
         l_colour,
         l_params,
         l_name
    FROM (SELECT l_body study) a;

  l_ucsId := ss_src."c_Study_store"(p_uctId     := p_uctId,
                                    p_ucsId     := p_ucsId,
                                    p_studyCode := p_studyCode,
                                    p_chartType := l_type,
                                    p_panel     := l_panel,
                                    p_colour    := COALESCE(l_colour,'#33CC33'),
                                    p_params    := l_params,
                                    p_name     := l_name);
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE   
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "study",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT l_ucsId "studyId") a) b;
  END IF;
                 
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;