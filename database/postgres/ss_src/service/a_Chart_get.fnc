CREATE OR REPLACE FUNCTION ss_src."a_Chart_get"(p_uctId ss_dat.user_charts.uct_id%TYPE) RETURNS json AS
$BODY$
DECLARE
  l_json    JSON;
  l_usrId   core_dat.users.usr_id%TYPE;
  l_ustId   ss_dat.user_strategies.ust_id%TYPE;
  l_taskId  INTEGER;
  
  l_data    text;
  l_debug   text;
  l_context text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Chart.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);

  IF p_uctId IS NOT NULL THEN
    l_ustId := ss_tapi."t_UserCharts_getUstId_PK"(p_pk := p_uctId);
    l_usrId := ss_tapi."t_UserStrategies_getUsrId_PK"(p_pk := l_ustId);
    
    
    IF l_usrId IS NULL OR
       l_usrId != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;  
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "strategy",
                   ROW_TO_JSON(b) "chart",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT ust_id "strategyId",
                           ust_name "name",
                           ust_desc "desc",
                           ust_position_code "position",
                           dexh_code "exchange",
                           dsyg_code "product",
                           dsym_code "contract",
                           TO_CHAR(ust_updated,'YYYY-MM-DD HH24:MI:SS') "updated",
                           ust_ust_id"draftStrategyId",
                           ust_live "live",
                           ust_sync_ind  "syncInd",
                           ust_sync_type "syncType",
                           dsym_decimals "decimals"
                      FROM ss_dat.user_strategies
                       JOIN ss_dat.d_symbols       ON dsym_id = ust_dsym_id
                       JOIN ss_dat.d_exchanges     ON dexh_id = dsym_dexh_id
                  LEFT JOIN ss_dat.d_symbol_groups ON dsyg_id = dsym_dsyg_id
                    WHERE ust_id = l_ustId) a
                CROSS
                 JOIN (SELECT uct_id "chartId",
                              cht_code "chartType",
                              dtfr_mins::TEXT "timeframeCode",
                              dtfr_name       "timeframeDesc",
                              TO_CHAR(uct_from,'YYYY-MM-DD HH24:MI:SS') "fromDate",
                              TO_CHAR(uct_to,'YYYY-MM-DD HH24:MI:SS')   "toDate",
                              uct_show_volume "showVolume",
                              uct_positive_colour "positiveColour",
                              uct_negative_colour "negativeColour",
                              uct_volume_colour   "volumeColour",
                              CASE WHEN uct_from > dsyt_min_date THEN 'N' ELSE 'Y' END "atMinDate",
                              CASE WHEN uct_to < dsyt_max_date THEN 'N' ELSE 'Y' END "atMaxDate",
                              TO_CHAR(uct_sync_from_date,'YYYY-MM-DD HH24:MI:SS') "syncFromDate",
                              TO_CHAR(uct_sync_to_date,'YYYY-MM-DD HH24:MI:SS') "syncToDate",
                              TO_CHAR(uct_area_from,'YYYY-MM-DD HH24:MI:SS') "areaFromDate",
                              TO_CHAR(uct_area_to,'YYYY-MM-DD HH24:MI:SS') "areaToDate",
                              uct_area_colour "areaColour",
                              uct_area_type "areaType",
                              core_src."a_Message_getMessageText"(CASE uct_area_type WHEN 'entry' THEN 'T0020' ELSE 'T0021' END,NULL) "areaLabel",
                              TO_CHAR(dsyt_min_date,'YYYY-MM-DD HH24:MI:SS') "contractFrom",
                              TO_CHAR(dsyt_max_date,'YYYY-MM-DD HH24:MI:SS') "contractTo"
                         FROM ss_dat.user_charts
                           JOIN ss_dat.chart_types         ON uct_cht_id  = cht_id
                           JOIN ss_dat.d_timeframes        ON uct_dtfr_id = dtfr_id
                           JOIN ss_dat.user_strategies     ON uct_ust_id  = ust_id
                           JOIN ss_dat.d_symbols           ON dsym_id     = ust_dsym_id
                           JOIN ss_dat.d_symbol_timeframes ON dsyt_dsym_id = dsym_id AND dsyt_dtfr_id = dtfr_id
                        WHERE uct_id = p_uctId) b) c;
                             
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning chart.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                            p_type    := 'I',
                                            p_taskId := l_taskId);
  END IF;
  
  RETURN l_json;  
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;