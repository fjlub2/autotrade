CREATE OR REPLACE FUNCTION ss_src."a_ChartMenu_get"() RETURNS JSON AS
$BODY$
BEGIN
  RETURN ss_src."a_ChartMenu_get"(p_ustId := NULL, p_uctId := NULL, p_mini := 'N');
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ss_src."a_ChartMenu_get"(p_mini CHAR(1)) RETURNS JSON AS
$BODY$
BEGIN
  RETURN ss_src."a_ChartMenu_get"(p_ustId := NULL, p_uctId := NULL, p_mini := p_mini);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ss_src."a_ChartMenu_get"(p_ustId ss_dat.user_strategies.ust_id%TYPE,
                                                    p_mini  CHAR(1)) RETURNS JSON AS
$BODY$
BEGIN
  RETURN ss_src."a_ChartMenu_get"(p_ustId := p_ustId, p_uctId := NULL, p_mini := p_mini);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ss_src."a_ChartMenu_get" (p_ustId ss_dat.user_strategies.ust_id%TYPE,
                                                     p_uctId ss_dat.user_charts.uct_id%TYPE,
                                                     p_mini  CHAR(1)) RETURNS JSON AS
$BODY$
DECLARE
  l_json    JSON;
  l_usrId   core_dat.users.usr_id%TYPE;
  l_ustRow  ss_dat.user_strategies%ROWTYPE;
  l_uctRow  ss_dat.user_charts%ROWTYPE;
  l_taskId  INTEGER;
  r_charts  RECORD;
  l_uctId   ss_dat.user_charts.uct_id%TYPE;
  l_count   INTEGER := 0;
  
  l_data    text;
  l_debug   text;
  l_context text;
  l_text    text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Chart Menu.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);

  IF p_uctId IS NOT NULL THEN
    IF p_ustId IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                              p_vars   := core_src."c_Data_addTag"('FIELD','Strategy'),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    ELSE
      l_ustRow := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := p_ustId);
      l_uctRow := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);

      IF l_ustRow.ust_id IS NULL OR
         l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                                p_vars   := core_src."c_Data_addTag"('FIELD','strategy'),
                                                p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('UCT_ID',p_uctId),'UST_ID',p_ustId),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
      END IF;
   
      IF p_ustId != ss_tapi."t_UserCharts_getUstId_PK"(p_pk := p_uctId) THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                                p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                                p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('UCT_ID',p_uctId),'UST_ID',p_ustId),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
      END IF;
    END IF;
  ELSIF COALESCE(p_mini,'N') = 'Y' AND
        p_uctId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  ELSIF p_ustId IS NOT NULL THEN
    l_ustRow := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := p_ustId);
    
    IF l_ustRow.ust_id IS NULL OR
       l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','strategy'),
                                              p_data   := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE
    l_text := '<tr class="tr_menu_static"><td id="menu_strategy_name" class="strategyNameMenu" title="'||COALESCE(l_ustRow.ust_desc,'')||'"><label for="strategy_name">'||COALESCE(l_ustRow.ust_name,'')||'</label></td>';
    
    IF COALESCE(p_mini,'N') != 'Y' THEN
      l_text := l_text || '<td class="add" id="menu_add_item" onclick="menu_add();" onmouseover="hover_add();" onmouseout="unhover_add();"><img id="add_image" src="../assets/default/images/menu/plus_c.svg" title="Add a Timeframe"></td>';
    END IF;
    
    IF p_ustId IS NOT NULL THEN      
      FOR r_charts IN (SELECT dtfr_name,
                              uct_id,
                              CASE WHEN l_ustRow.ust_default_uct_id IS NOT NULL THEN CASE l_ustRow.ust_default_uct_id WHEN uct_id THEN 'Y' ELSE 'N' END
                                   ELSE CASE keepOne WHEN 1 THEN 'Y' ELSE 'N' END END default_chart
                         FROM (SELECT dtfr_name,
                                      uct_id,
                                      ROW_NUMBER() OVER (ORDER BY dtfr_mins DESC) keepOne,
                                      dtfr_mins
                                 FROM ss_dat.user_charts
                                   JOIN ss_dat.d_timeframes ON uct_dtfr_id = dtfr_id
                                WHERE uct_ust_id = p_ustId) a
                        ORDER BY dtfr_mins) LOOP
        l_text := l_text || '<td onclick="saved_chart_onclick(this)" class="charts" id="chart_id'||r_charts.uct_id||
                            '" style="'||CASE WHEN (p_uctId IS NULL AND r_charts.default_chart = 'Y') OR (p_uctId IS NOT NULL AND r_charts.uct_id = p_uctId) THEN 'color:#ffffff; background:#5f6f81"'
                                                                        ELSE 'color:#5f6f81; background:#ffffff"' END||'><b>'||r_charts.dtfr_name||'</b></td>';
        IF p_uctId IS NOT NULL THEN
          l_uctId := p_uctId;
        ELSIF r_charts.default_chart = 'Y' THEN
          l_uctId := r_charts.uct_id;
        END IF;
        
        l_count := l_count + 1;
      END LOOP;
    END IF;
    
    IF COALESCE(p_mini,'N') = 'Y' THEN
      l_text := l_text || '</tr>';
    ELSE
      l_text := l_text || '<td class="menu" id="menu_drop_item" onclick="menu_drop();" onmouseover="hover_menu();" onmouseout="unhover_menu();"><img id="list_image" src="../assets/default/images/menu/list_c.svg" title="Show/Hide Menu"></td></tr>';
    END IF;
                             
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "chartMenu",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT l_uctId "defaultChartId", l_count "chartCount", l_text "menuHTML") a) b;
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Chart Menu',
                                            p_data    := core_src."c_Data_addTag"(core_src."c_Data_addTag"('UCT_ID',p_uctId),'UST_ID',p_ustId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  
  END IF;
  
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;