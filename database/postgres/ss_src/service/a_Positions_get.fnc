CREATE OR REPLACE FUNCTION ss_src."a_Positions_get"()
  RETURNS json AS
$BODY$
DECLARE
  l_json     JSON;
  l_taskId   INTEGER;
  
  l_data     text;
  l_debug    text;
  l_context  text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Positions.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF core_src."c_Message_errorsExist" (p_taskId := core_src."c_Session_getCurrentTaskId"()) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := core_src."c_Session_getCurrentTaskId"());
  ELSE 
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "positions",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))) "items"
                      FROM (SELECT cv_code "code",
                                   cv_name "desc",
                                   REPLACE(LEAST(ROW_NUMBER() OVER (ORDER BY 1),6)::text,'6','') "recent"
                              FROM core_dat.positions_mv
                             ORDER BY cv_name) a) b) c;

    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning positions.',
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql STABLE;

