CREATE OR REPLACE FUNCTION ss_src."a_ChartTypes_get"() RETURNS json AS
$BODY$
BEGIN
  RETURN ss_src."a_ChartTypes_get"(p_uctId := NULL);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ss_src."a_ChartTypes_get"(p_uctId ss_dat.user_charts.uct_id%TYPE)
  RETURNS json AS
$BODY$
DECLARE
  l_json     JSON;
  l_taskId   INTEGER;
  l_chtId    ss_dat.chart_types.cht_id%TYPE;
  
  l_data     text;
  l_debug    text;
  l_context  text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Chart Types.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
                                          
  IF p_uctId IS NOT NULL THEN
    l_chtId := ss_tapi."t_UserCharts_getChtId_PK"(p_pk := p_uctId);
    
    IF l_chtId IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- chart doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    l_chtId := -1;
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := core_src."c_Session_getCurrentTaskId"()) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := core_src."c_Session_getCurrentTaskId"());
  ELSE 
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "chart_types",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))) "items"
                      FROM (SELECT cht_code "code",
                                   cht_name "desc",
                                   REPLACE(LEAST(ROW_NUMBER() OVER (ORDER BY 1),6)::text,'6','') "recent"
                              FROM ss_dat.chart_types
                             WHERE cht_main_ind = 'Y'
                             ORDER BY CASE cht_id WHEN l_chtId THEN 1 ELSE 2 END,
                                      cht_order) a) b) c;

    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning strategy chart types.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql STABLE;

