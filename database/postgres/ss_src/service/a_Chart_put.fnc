CREATE OR REPLACE FUNCTION ss_src."a_Chart_put"(p_ustId  ss_dat.user_strategies.ust_id%TYPE,
                                                p_body   text) RETURNS json AS
$BODY$
BEGIN
  RETURN ss_src."a_Chart_put"(p_ustId := p_ustId, p_uctId := NULL, p_body := p_body);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ss_src."a_Chart_put"(p_ustId  ss_dat.user_strategies.ust_id%TYPE,
                                                p_uctId  ss_dat.user_charts.uct_id%TYPE,
                                                p_body   text) RETURNS json AS
$BODY$
DECLARE
  l_json           JSON;
  l_body           JSON;
  
  l_timeframe      ss_dat.d_timeframes.dtfr_mins%TYPE;
  l_chartType      ss_dat.chart_types.cht_code%TYPE;
  l_volume         ss_dat.user_charts.uct_show_volume%TYPE;
  l_positiveColour ss_dat.user_charts.uct_positive_colour%TYPE;
  l_negativeColour ss_dat.user_charts.uct_negative_colour%TYPE;
  l_volumeColour   ss_dat.user_charts.uct_volume_colour%TYPE;
  l_areaType       ss_dat.user_charts.uct_area_type%TYPE;

  l_uctId          ss_dat.user_charts.uct_id%TYPE;
  l_taskId         INTEGER;
  
  l_data           TEXT;
  l_debug          TEXT;
  l_context        TEXT;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Putting Chart.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  BEGIN
    l_body := p_body::json;
  EXCEPTION
    WHEN OTHERS THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0003', -- body invalid
                                              p_data   := core_src."c_Data_addTag"('BODY',p_body),
                                              p_type   := 'F',
                                              p_taskId := l_taskId);
  END;
  
  SELECT json_extract_path_text(chart,'timeframe')::smallint,
         json_extract_path_text(chart,'chartType'),
         json_extract_path_text(chart,'showVolume'),
         json_extract_path_text(chart,'positiveColour'),
         json_extract_path_text(chart,'negativeColour'),
         json_extract_path_text(chart,'volumeColour'),
         json_extract_path_text(chart,'areaType')
    INTO l_timeframe,
         l_chartType,
         l_volume,
         l_positiveColour,
         l_negativeColour,
         l_volumeColour,
         l_areaType
    FROM (SELECT l_body chart) a;

  l_uctId := ss_src."c_Chart_store"(p_ustId          := p_ustId,
                                    p_uctId          := p_uctId,
                                    p_timeframe      := l_timeframe,
                                    p_chartType      := l_chartType,
                                    p_showVolume     := l_volume,
                                    p_positiveColour := COALESCE(l_positiveColour,'#33CC33'),
                                    p_negativeColour := COALESCE(l_negativeColour,'#FF0000'),
                                    p_volumeColour   := COALESCE(l_volumeColour,  '#69BFFF'),
                                    p_areaType       := COALESCE(l_areaType,      'entry'));
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE   
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "chart",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT l_uctId "chartId") a) b;
  END IF;
                 
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;