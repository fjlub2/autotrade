CREATE OR REPLACE FUNCTION ss_src."a_Strategy_get"(p_ustId ss_dat.user_strategies.ust_id%TYPE) RETURNS json AS
$BODY$
DECLARE
  l_json    JSON;
  l_usrId   core_dat.users.usr_id%TYPE;
  l_taskId  INTEGER;
  
  l_data    text;
  l_debug   text;
  l_context text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Strategy.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF p_ustId IS NOT NULL THEN
    l_usrId := ss_tapi."t_UserStrategies_getUsrId_PK"(p_pk := p_ustId);
    
    IF l_usrId IS NULL OR
       l_usrId != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- strategy doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','strategy'),
                                              p_data   := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;  
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Strategy'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE 
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "strategy",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT ust_id "strategyId",
                           ust_name "name",
                           ust_desc "desc",
                           ust_position_code "position",
                           dexh_code "exchange",
                           dsyg_code "product",
                           dsym_code "contract",
                           TO_CHAR(ust_updated,'YYYY-MM-DD HH24:MI:SS') "updated",
                           ust_ust_id"draftStrategyId",
                           ust_live "live",
                           ust_sync_ind  "syncInd",
                           ust_sync_type "syncType",
                           dsym_decimals "decimals"
                      FROM ss_dat.user_strategies
                       JOIN ss_dat.d_symbols       ON dsym_id = ust_dsym_id
                       JOIN ss_dat.d_exchanges     ON dexh_id = dsym_dexh_id
                  LEFT JOIN ss_dat.d_symbol_groups ON dsyg_id = dsym_dsyg_id
                    WHERE ust_id = p_ustId) a) b;
                             
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning strategy details.',
                                            p_data    := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                            p_type    := 'I',
                                            p_taskId := l_taskId);
  END IF;
  
  RETURN l_json;  
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;