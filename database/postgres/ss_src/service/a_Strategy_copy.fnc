CREATE OR REPLACE FUNCTION ss_src."a_Strategy_copy"(p_ustId ss_dat.user_strategies.ust_id%TYPE, p_draft IN text) RETURNS json AS
$BODY$
DECLARE
  l_json           JSON;
  l_ustId          ss_dat.user_strategies.ust_id%TYPE;
  l_taskId  INTEGER;
  
  l_data           TEXT;
  l_debug          TEXT;
  l_context        TEXT;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Copying Strategy.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  l_ustId := ss_src."c_Strategy_copy"(p_ustId := p_ustId, p_draft := p_draft);

  IF core_src."c_Message_errorsExist" (p_taskId := core_src."c_Session_getCurrentTaskId"()) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := core_src."c_Session_getCurrentTaskId"());
  ELSE  
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "strategy",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT l_ustId "strategyId") a) b;
              
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Copied strategy.',
                                            p_data    := core_src."c_Data_addTag"(core_src."c_Data_addTag"('FROM_UST_ID',p_ustId), 'TO_UST_ID', l_ustId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
                 
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;