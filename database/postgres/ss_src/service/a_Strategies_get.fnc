CREATE OR REPLACE FUNCTION ss_src."a_Strategies_get"(p_live     ss_dat.user_strategies.ust_live%TYPE,
                                                     p_sEcho    INTEGER,
                                                     p_sortCol  INTEGER,
                                                     p_sortDir  TEXT,
                                                     p_sSearch  TEXT) RETURNS json AS
$BODY$
DECLARE
  l_json    JSON;
  l_usrId   core_dat.users.usr_id%TYPE;
  l_taskId  INTEGER;
  
  l_data    text;
  l_debug   text;
  l_context text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Strategies.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF COALESCE(p_live,'') = '' THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Live Indicator'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  ELSIF p_live NOT IN ('Y','N') THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- invalid value
                                            p_vars   := core_src."c_Data_addTag"('FIELD','live indicator'),
                                            p_data   := core_src."c_Data_addTag"('DTFR_MINS',p_live),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_usrId := core_src."c_Session_getCurrentUsrId"();
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE 
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "strategies",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT p_sEcho "sEcho",
                           COUNT(1) "iTotalRecords",
                           COUNT(1) "iTotalDisplayRecords",
                           ARRAY_TO_JSON(COALESCE(ARRAY_AGG(a),'{}')) "items"
                      FROM (SELECT ust_id "strategyId",
                                   ust_name "name",
                                   ust_desc "desc",
                                   ust_position_code "position",
                                   dexh_code "exchange",
                                   dsyg_code "product",
                                   dsym_code "contract",
                                   TO_CHAR(ust_updated,'YYYY-MM-DD HH24:MI:SS') "updated",
                                   ust_ust_id    "draftStrategyId",
                                   ust_sync_ind  "syncInd",
                                   ust_sync_type "syncType",
                                   dsym_decimals "decimals"
                              FROM ss_dat.user_strategies
                               JOIN ss_dat.d_symbols       ON dsym_id = ust_dsym_id
                               JOIN ss_dat.d_exchanges     ON dexh_id = dsym_dexh_id
                          LEFT JOIN ss_dat.d_symbol_groups ON dsyg_id = dsym_dsyg_id
                            WHERE ust_usr_id = l_usrId
                              AND ust_live   = p_live
                              AND EXISTS (SELECT 1
                                            FROM ss_dat.user_charts
                                           WHERE uct_ust_id = ust_id)
                              AND (COALESCE(p_sSearch,'') = '' OR
                                   (COALESCE(p_sSearch,'') != '' AND
                                    COALESCE(ust_name,'')||COALESCE(ust_desc,'')||COALESCE(ust_position_code,'')||dexh_code||dsyg_code||dsym_code||TO_CHAR(ust_updated,'YYYY-MM-DD HH24:MI:SS') ILIKE '%'||p_sSearch||'%'))
                            ORDER BY CASE WHEN p_sortDir = 'asc' THEN 
                                       CASE WHEN p_sortCol = 0 THEN ust_name
                                            WHEN p_sortCol = 1 THEN ust_desc
                                            WHEN p_sortCol = 2 THEN ust_position_code
                                            WHEN p_sortCol = 3 THEN dexh_code
                                            WHEN p_sortCol = 4 THEN dsyg_code
                                            WHEN p_sortCol = 5 THEN dsym_code
                                            WHEN p_sortCol = 6 THEN TO_CHAR(ust_updated,'YYYY-MM-DD HH24:MI:SS')
                                            ELSE ust_name END
                                        ELSE 'a' END ASC,
                                     CASE WHEN p_sortDir = 'desc' THEN 
                                       CASE WHEN p_sortCol = 0 THEN ust_name
                                            WHEN p_sortCol = 1 THEN ust_desc
                                            WHEN p_sortCol = 2 THEN ust_position_code
                                            WHEN p_sortCol = 3 THEN dexh_code
                                            WHEN p_sortCol = 4 THEN dsyg_code
                                            WHEN p_sortCol = 5 THEN dsym_code
                                            WHEN p_sortCol = 6 THEN TO_CHAR(ust_updated,'YYYY-MM-DD HH24:MI:SS')
                                            ELSE ust_name END
                                        ELSE 'b' END DESC) a) b) c;
                             
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning strategies.',
                                            p_data    := core_src."c_Data_addTag"('USR_ID',l_usrId),
                                            p_type    := 'I',
                                            p_taskId := l_taskId);
  END IF;
  
  RETURN l_json;  
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;