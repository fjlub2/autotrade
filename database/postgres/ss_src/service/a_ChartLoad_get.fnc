CREATE OR REPLACE FUNCTION ss_src."a_ChartLoad_get"() RETURNS text AS
$BODY$
BEGIN
  RETURN ss_src."a_ChartLoad_get"(p_type := 'M');
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION ss_src."a_ChartLoad_get" (p_type CHAR(1)) RETURNS text AS
$BODY$
DECLARE
  l_text      text;
  l_json      JSON;
  l_ustRow    ss_dat.user_strategies%ROWTYPE;
  l_uctRow    ss_dat.user_charts%ROWTYPE;
  l_dtfrRow   ss_dat.d_timeframes%ROWTYPE;
  l_mins      ss_dat.d_timeframes.dtfr_mins%TYPE;
  l_type      ss_dat.chart_types.cht_code%TYPE;
  l_ct        text;
  l_tf        text;
  l_taskId    INTEGER;
  
  l_data      text;
  l_debug     text;
  l_context   text;
BEGIN
  /* p_type: (M)ain Chart, (C)riteria  */
  
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Chart Load Command.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
    l_text := l_json::text;
  ELSE
    l_text :=
   '<script>
      var chart;
      var g_volumePanelExists = false;

      var g_volumePanel;
      var g_studies = [];
      var g_studyChildren = [];
      var g_chartId;
        
      var g_currentAreaType;
      var g_currentAreaLabel;
      var g_areaValues = new Array(2);
      var g_resettingGuides = 0;
      var g_cursorPosition;
      var g_cursorPanel;';
      
    IF p_type = 'M' THEN
      l_text := l_text||'
      var g_syncDate = 1;
      var g_syncType = "start";
      var g_chartScrolled = 0;

      var g_mouseDown = 0;
      document.body.onmousedown = function() { 
        g_mouseDown = 1;
      }
      document.body.onmouseup = function() {
        g_mouseDown = 0;
        if (g_chartScrolled) {
          if (chart.chartCreated) {
            chart_sync(chart.startDate,chart.endDate);
            resetValueGuides (chart.startDate,chart.endDate);
          }
        }
        g_chartScrolled = 0;
      }';
    END IF;

    l_text := l_text||'
      // prepare the main chart
      function loadChart() {
        chart = new AmCharts.AmStockChart();
        AmCharts.useUTC = true;
        chart.animationPlayed         = false;
        chart.mouseWheelScrollEnabled = false;
        chart.addClassNames           = true;
        chart.classNamePrefix         = "ti";
        chart.pathToImages            = "<IMAGES_PATH>";

        // Main Chart Dataset
        var l_dataSet = new AmCharts.DataSet();
        l_dataSet.fieldMappings = [{fromField:"o", toField:"o"},
                                   {fromField:"h", toField:"h"},
                                   {fromField:"l", toField:"l"},
                                   {fromField:"c", toField:"c"},
                                   {fromField:"v", toField:"v"}];
        l_dataSet.categoryField = "d";

        // Add all datasets to the chart
        chart.dataSets = [l_dataSet];

        // Create the main panel
        var l_mainPanel = new AmCharts.StockPanel();
        l_mainPanel.percentHeight    = 70;
        l_mainPanel.showCategoryAxis = true;
        l_mainPanel.id               = "mainPanel";

        // Main Chart
        var l_graph = new AmCharts.StockGraph();
        l_graph.id                        = "mainGraph";
      //l_graph.balloonText               = "Open:<b>[[open]]</b><br>High:<b>[[high]]</b><br>Low:<b>[[low]]</b><br>Close:<b>[[close]]</b><br>";
      //l_graph.showBalloonAt             = "c";
        l_graph.showBalloon               = false;
        l_graph.valueField                = "c";
        l_graph.closeField                = "c";
        l_graph.openField                 = "o";
        l_graph.highField                 = "h";
        l_graph.lowField                  = "l";
        l_graph.useDataSetColors          = false;
        l_graph.fillAlphas                = 1;
        l_graph.lineThickness             = 1;
        l_graph.lineColor                 = "#33CC33";
        l_graph.fillColors                = "#33CC33";
        l_graph.negativeFillColors        = "#FF0000";
        l_graph.negativeLineColor         = "#FF0000";
        l_graph.connect                   = true;
        l_graph.legendValueText           = "Open: [[o]] High: [[h]] Low:[[l]] Close: [[c]]";

        l_mainPanel.addStockGraph(l_graph);

        // Legend for each chart on main panel
        var l_legend = new AmCharts.StockLegend();
        l_legend.valueTextRegular   = undefined;
        l_legend.rollOverGraphAlpha = 1;
        l_legend.equalWidths        = false;
        l_legend.reversedOrder      = true;
        l_legend.backgroundColor    = "#E9E9E9";
        l_legend.backgroundAlpha    = 1;
        l_mainPanel.stockLegend     = l_legend;

        // Add all panels to the chart
        chart.panels = [l_mainPanel];

        // Global Panel Settings
        var l_panelsSettings = new AmCharts.PanelsSettings();
        l_panelsSettings.startDuration         = 1;
        l_panelsSettings.recalculateToPercents = "never";
        chart.panelsSettings = l_panelsSettings;
        
        // Value (Y) Axis Settings
        var l_valueAxesSettings = new AmCharts.ValueAxesSettings();
        l_valueAxesSettings.position      = "right";
        l_valueAxesSettings.dashLength    = 5;
        l_valueAxesSettings.showLastLabel = true;
        chart.valueAxesSettings = l_valueAxesSettings;
                   
      
        var l_chartCursorSettings = new AmCharts.ChartCursorSettings();
        l_chartCursorSettings.categoryBalloonEnabled  = true;
        l_chartCursorSettings.zoomable                = false;
        l_chartCursorSettings.valueLineBalloonEnabled = true;
        l_chartCursorSettings.valueLineEnabled        = true;';
        
     IF p_type = 'M' THEN
       l_text := l_text||'
        l_chartCursorSettings.pan                     = true;';
     ELSIF p_type = 'C' THEN
       l_text := l_text||'
        l_chartCursorSettings.pan                     = false;';
     END IF;
          
     l_text := l_text||'
        l_chartCursorSettings.cursorColor             = "#FF6600";
        l_chartCursorSettings.cursorAlpha             = 1;
        l_chartCursorSettings.categoryBalloonDateFormats = [{period: "mm",   format: "DD MMM YYYY JJ:NN"},
                                                            {period: "hh",   format: "DD MMM YYYY JJ:NN"},
                                                            {period: "DD",   format: "DD MMM YYYY"},
                                                            {period: "WW",   format: "DD MMM YYYY"},
                                                            {period: "MM",   format: "MMM YYYY"},
                                                            {period: "YYYY", format: "YYYY"}];
        chart.chartCursorSettings = l_chartCursorSettings;';

     IF p_type = 'M' THEN
       l_text := l_text||'
        // Scrollbar Settings
        var l_chartScrollbarSettings = new AmCharts.ChartScrollbarSettings();
        l_chartScrollbarSettings.graph                   = "mainGraph";
        l_chartScrollbarSettings.graphType               = "line";
        l_chartScrollbarSettings.resizeEnabled           = false;
        l_chartScrollbarSettings.updateOnReleaseOnly     = true;
        l_chartScrollbarSettings.scrollDuration          = 1;
        l_chartScrollbarSettings.position                = "top";
        l_chartScrollbarSettings.scrollbarHeight         = 25;
        l_chartScrollbarSettings.selectedBackgroundColor = "#FF9933";
        l_chartScrollbarSettings.selectedGraphFillColor  = "#FF6600";
        chart.chartScrollbarSettings = l_chartScrollbarSettings;';
     ELSIF p_type = 'C' THEN
       l_text := l_text||'      
        // Scrollbar Settings
        var l_chartScrollbarSettings = new AmCharts.ChartScrollbarSettings();
        l_chartScrollbarSettings.enabled = false;
        chart.chartScrollbarSettings = l_chartScrollbarSettings;';
     END IF;
     
     l_text := l_text||'
        // Ballon Settings - off for now
        var l_balloon = chart.balloon;
        l_balloon.fillAlpha = 0.3;
        chart.balloon = l_balloon;
      };

      // final part of the main chart load. Doing it this way makes the chart load a lot faster
      function refreshChart(p_chartData) {
        // scrap the chart if it already exists
        clearChart();

        g_studies = [];
        g_studyChildren = [];
        var l_stddiv = 0;
        
        var l_data           = p_chartData.chart_data;
        var l_minPeriod      = p_chartData.chartSettings.chartDefaultPeriod;
        var l_maxPeriod      = p_chartData.chartSettings.chartMaxPeriod;
        var l_type           = p_chartData.chartSettings.chartType;
        var l_title          = p_chartData.chartSettings.chartTitle;
        var l_studyData      = p_chartData.studySettings;
        var l_showVolume     = p_chartData.chartSettings.showVolume;
        var l_positiveColour = p_chartData.chartSettings.positiveColour;
        var l_negativeColour = p_chartData.chartSettings.negativeColour;
        var l_decimals       = p_chartData.chartSettings.decimals;
        var l_volumeColour   = p_chartData.chartSettings.volumeColour;
        g_chartId            = p_chartData.chartSettings.chartId;';
        
     IF p_type = 'M' THEN
       l_text := l_text||'
        var l_periods        = p_chartData.chartSettings.chartPeriods;
        var l_syncType       = p_chartData.chartSettings.syncType;

        if (l_syncType != undefined) {
          g_syncType = l_syncType;
        }
        
        chart.chartScrollbarSettings.usePeriod = l_maxPeriod;';
     END IF;
     
     l_text := l_text||'
        
        if (l_showVolume == "Y") {
          showVolume(0, l_volumeColour);
        }
        
        var l_colorField;
        var l_guides = [];
        var l_trends = [];
        
        var l_categoryAxis = new AmCharts.CategoryAxis();
        l_categoryAxis.parseDates = true;
        l_categoryAxis.minPeriod  = l_minPeriod;
        chart.panels[0].categoryAxis = l_categoryAxis;
        chart.panels[0].allLabels    = [{"text":l_title, "bold":true, "align":"center", "alpha":0.5, "y":10, "size":13}];
        
        // merge study data into the main dataset and change dates to date objects
        if (l_studyData.length > 0) {
          for(var i = 0; i < l_data.length; i++) {
            l_data[i].d = new Date(l_data[i].d);
            
            for(var j = 0; j < l_studyData.length; j++) {
              for(var p = 0; p < l_studyData[j].study.studyData.length; p++) {
                if (typeof l_studyData[j].study.studyData[p].values[i] != ''undefined'' && l_studyData[j].study.studyData[p].values[i].v != null) {
                  l_data[i]["study" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code] = l_studyData[j].study.studyData[p].values[i].v;
        
                  if (typeof l_studyData[j].study.studyData[p].values[i].bf != ''undefined'') {
                    l_data[i]["study" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code + "_from"] = l_studyData[j].study.studyData[p].values[i].bf;
                  }
          
                  if (typeof l_studyData[j].study.studyData[p].values[i].bt != ''undefined'') {
                    l_data[i]["study" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code + "_to"] = l_studyData[j].study.studyData[p].values[i].bt;
                  }
              
                  if (typeof l_studyData[j].study.studyData[p].values[i].c != ''undefined'') {
                    l_data[i]["colour" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code] = l_studyData[j].study.studyData[p].values[i].c;
                  }
                }
              }
            }
          }

          for(var j = 0; j < l_studyData.length; j++) {
            var l_studyVals = [];
            
            for(var p = 0; p < l_studyData[j].study.studyData.length; p++) {
              chart.dataSets[0].fieldMappings.push({fromField:"study" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code, toField:"study" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code});
        
              if (l_studyData[j].study.studyData[p].values.length > 0) {
                if (typeof l_studyData[j].study.studyData[p].values[0].bf != ''undefined'') {
                  l_stddiv = 1;
                  chart.dataSets[0].fieldMappings.push({fromField:"study" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code + "_from", toField:"study" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code + "_from"});
                }
         
                if (typeof l_studyData[j].study.studyData[p].values[0].bt != ''undefined'') {
                  chart.dataSets[0].fieldMappings.push({fromField:"study" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code + "_to", toField:"study" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code + "_to"});
                }
            
                if (typeof l_studyData[j].study.studyData[p].values[0].c != ''undefined'') {
                  chart.dataSets[0].fieldMappings.push({fromField:"colour" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code, toField:"colour" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code});
                  l_colorField = "colour" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code;
                } else {
                  l_colorField = undefined;
                }
              } else {
                l_colorField = undefined;
              }
              
              l_studyVals.push ({field:"study" + l_studyData[j].study.studyId + l_studyData[j].study.studyData[p].code,name:l_studyData[j].study.studyData[p].name,chartType:l_studyData[j].study.studyData[p].overrideChartType,colourField:l_colorField,band:l_stddiv});
            }
            
            l_guides = [];
            
            if (l_studyData[j].study.studyGuides.length > 0) {
              if (l_studyData[j].study.studySettings.guideType == "C") {
                for(var k = 0; k < l_studyData[j].study.studyGuides.length; k++) {
                  l_guides.push({id:"guide_study" + l_studyData[j].study.studyId + "_" + k, date:new Date(l_studyData[j].study.studyGuides[k].d), toDate:new Date(l_studyData[j].study.studyGuides[k].dt), dashLength:2, lineThickness:1, lineAlpha:0.5, lineColor:l_studyData[j].study.studySettings.colour, fillAlpha:0.2, fillColor:l_studyData[j].study.studySettings.colour, inside:true, label:l_studyData[j].study.studyGuides[k].l, position: "bottom", color:"#4C4C4C"});
                }
              } else if (l_studyData[j].study.studySettings.guideType == "V") {
                for(var k = 0; k < l_studyData[j].study.studyGuides.length; k++) {
                  l_guides.push({id:"guide_v_study" + l_studyData[j].study.studyId + "_" + k, date:new Date(l_studyData[j].study.studyGuides[k].d), toDate:new Date(l_studyData[j].study.studyGuides[k].dt), value:l_studyData[j].study.studyGuides[k].v, toValue:l_studyData[j].study.studyGuides[k].vt, dashLength:0, lineThickness:0, lineAlpha:0, lineColor:l_studyData[j].study.studySettings.colour, fillAlpha:0.2, fillColor:l_studyData[j].study.studySettings.colour, inside:true, label:l_studyData[j].study.studyGuides[k].l, position: "bottom", color:"#4C4C4C"});
                }
              } else if (l_studyData[j].study.studySettings.guideType == "S") {
                for(var k = 0; k < l_studyData[j].study.studyGuides.length; k++) {
                  l_guides.push({id:"guide_s_study" + l_studyData[j].study.studyId + "_" + k, value:l_studyData[j].study.studyGuides[k].v, dashLength:2, lineThickness:1, lineAlpha:1, lineColor:l_studyData[j].study.studySettings.colour, fillAlpha:0.2, fillColor:l_studyData[j].study.studySettings.colour, inside:true, label:l_studyData[j].study.studyGuides[k].l, position: "left", color:"#4C4C4C"});
                }
              }
            }
            
            l_trends = [];

            if (l_studyData[j].study.studyTrends.length > 0) {
              for(var k = 0; k < l_studyData[j].study.studyTrends.length; k++) {
                l_trends.push({id:"trend_study" + l_studyData[j].study.studyId + "_" + k, initialDate:new Date(l_studyData[j].study.studyTrends[k].d), finalDate:new Date(l_studyData[j].study.studyTrends[k].dt), initialValue:l_studyData[j].study.studyTrends[k].v, finalValue:l_studyData[j].study.studyTrends[k].vt, lineColor:l_studyData[j].study.studySettings.colour});
              }
            }

            addGraph(l_studyData[j].study.studyId, l_studyData[j].study.studySettings.studyType, l_studyData[j].study.studySettings.studyTitle, l_studyData[j].study.studySettings.showOn, l_studyData[j].study.studySettings.colour, l_volumeColour, l_studyVals, l_studyData[j].study.studySettings.guideType, l_guides, l_trends, l_studyData[j].study.studySettings.minValue, l_studyData[j].study.studySettings.maxValue);
          }       
        } else {
          for(var i = 0; i < l_data.length; i++) {
            l_data[i].d = new Date(l_data[i].d);
          }
        }
        
        var l_categoryAxesSettings = new AmCharts.CategoryAxesSettings();
        l_categoryAxesSettings.equalSpacing   = true;
        l_categoryAxesSettings.maxSeries      = 0;
        l_categoryAxesSettings.dashLength     = 5;
        l_categoryAxesSettings.minPeriod      = l_minPeriod;
        l_categoryAxesSettings.groupToPeriods = [l_minPeriod,l_maxPeriod];
        l_categoryAxesSettings.dateFormats    = [{period: "mm",   format: "JJ:NN"},
                                                 {period: "hh",   format: "JJ:NN"},
                                                 {period: "DD",   format: "DD MMM"},
                                                 {period: "MM",   format: "MMM YYYY"},
                                                 {period: "YYYY", format: "MMM YYYY"}];
        chart.categoryAxesSettings = l_categoryAxesSettings;';
     
     l_text := l_text||'
        chart.panels[0].stockGraphs[0].type            = l_type;
        chart.panels[0].stockGraphs[0].title           = "Value";
        chart.panels[0].stockGraphs[0].proCandlesticks = false;
        
        if (l_type == ''smoothedLine'' || l_type == ''line'') {
          chart.panels[0].stockGraphs[0].fillAlphas = 0;
        } else {
          chart.panels[0].stockGraphs[0].fillAlphas = 1;
        }
        
        if (typeof l_positiveColour != ''undefined'') {
          chart.panels[0].stockGraphs[0].lineColor = l_positiveColour;
          
          if (l_type == ''smoothedLine'' || l_type == ''line'') {
            chart.panels[0].stockGraphs[0].fillColors = undefined;
          } else {
            chart.panels[0].stockGraphs[0].fillColors = l_positiveColour;
          }
        }
        
        if (typeof l_negativeColour != ''undefined'') {
          chart.panels[0].stockGraphs[0].negativeLineColor = l_negativeColour;
          
          if (l_type == ''smoothedLine'' || l_type == ''line'') {
            chart.panels[0].stockGraphs[0].negativeFillColors = undefined;
          } else {
            chart.panels[0].stockGraphs[0].negativeFillColors = l_negativeColour;
          }
        }
        
        var l_valueAxis = new AmCharts.ValueAxis();
        if (typeof l_decimals != ''undefined'') {
          l_valueAxis.precision = l_decimals;
        } else {
          l_valueAxis.precision = 4;
        }
        chart.panels[0].valueAxes[0] = l_valueAxis;
        
        if (l_type == "candlestick") {
          chart.panels[0].stockGraphs[0].proCandlesticks = true;
        }
        
        chart.dataSets[0].dataProvider = l_data;';
        
    IF p_type = 'M' THEN
      l_text := l_text||'
        if (p_chartData.chartSettings.areaFromDate != undefined) {
          if (p_chartData.chartSettings.areaType == "entry") { 
            addAreaGuide("Entry", new Date(p_chartData.chartSettings.areaFromDate), new Date(p_chartData.chartSettings.areaToDate), p_chartData.chartSettings.areaColour, p_chartData.chartSettings.areaLabel);
          } else {
            addAreaGuide("Exit", new Date(p_chartData.chartSettings.areaFromDate), new Date(p_chartData.chartSettings.exitToDate), p_chartData.chartSettings.areaColour, p_chartData.chartSettings.areaLabel);
          }
        }';
    END IF;

    l_text := l_text||'
        chart.write("chart");
        
        addStaticGuides();';
        
    IF p_type = 'C' THEN
      l_text := l_text||'
        resetValueGuides (chart.startDate,chart.endDate);';
    END IF;

    IF p_type = 'M' THEN
      l_text := l_text||'
        chart.addListener("zoomed", handleScroll);';
    END IF;
    
    l_text := l_text||'
        chart.panels[0].stockLegend.addListener("hideItem", handleLegend);
        chart.panels[0].stockLegend.addListener("showItem", handleLegend);
        adjChartHeight();';
        
    IF p_type = 'M' THEN
      l_text := l_text||'
        htmlChartNavigation(g_chartId);';
    END IF;

    l_text := l_text||'
        listenForClicks();
        showCategoryBalloon("mainPanel");
      };
        
      // returns the index position of the study
      function findStudy (p_studyId) {
        for(var i = 0; i < g_studies.length; i++){
          if (g_studies[i].id == p_studyId) {
            return i;
          }
        }
      }
        
      // returns the index position of the panel
      function findPanel (p_panelId) {
        for(var i = 0; i < chart.panels.length; i++){
          if (chart.panels[i].id == p_panelId) {
            return i;
          }
        }
      }

      // add new graph and optionally panel to the chart
      function addGraph(p_studyId, p_type, p_title, p_showOn, p_colour, p_volumeColour, p_studyVals, p_guideType, p_guides, p_trends, p_minValue, p_maxValue) {
        var l_studyPanel;
        var l_panelPos;
        var l_panelOwner = false;
        var l_chartType;
        var l_colour;
        var l_title = p_title;
                
        for(var i = 0; i < p_studyVals.length; i++){
          if (p_studyVals[i].chartType != undefined) {
            l_chartType = p_studyVals[i].chartType;
          } else {
            l_chartType = p_type;
          }
          
          if (p_showOn == "new") {
            l_title = p_studyVals[i].name;
          }
          
          if (p_studyVals[i].colour != undefined) {
            l_colour = p_studyVals[i].colour;
          } else {
            l_colour = p_colour;
          }
        
          // Prepare the graph
          var l_graph = new AmCharts.StockGraph();
          l_graph.id               = p_studyVals[i].field;
          l_graph.valueField       = p_studyVals[i].field;
          l_graph.showBalloon      = false;
          l_graph.type             = l_chartType;
          l_graph.title            = l_title;
          l_graph.lineThickness    = 1;
          l_graph.fillAlphas       = 0;
          l_graph.useDataSetColors = false;
          l_graph.legendValueText  = "[[" + p_studyVals[i].field + "]]";
          l_graph.lineColor        = l_colour;
          l_graph.fillColors       = l_colour;
          l_graph.connect          = true;
         
          if (p_studyVals[i].colourField) {
            l_graph.lineColorField = p_studyVals[i].colourField;
            l_graph.fillColorsField= p_studyVals[i].colourField;
          }

          if (l_chartType == "column") {
            l_graph.fillAlphas     = 0.8;
            l_graph.lineThickness  = 1;
          }
          
          if (i==0) {
            // Add a new panel when required
            if (p_showOn == "new") {
              l_studyPanel = new AmCharts.StockPanel();
              l_studyPanel.showCategoryAxis = true;
              l_studyPanel.addStockGraph(l_graph);
              l_studyPanel.id = "studyPanel" + p_studyId;
              l_studyPanel.usePrefixes = true;
              l_studyPanel.allLabels = [{"text":p_title, "bold":true, "align":"center", "alpha":0.5, "y":10, "size":13}];

              // add a legend for the new panel
              var l_legend = new AmCharts.StockLegend();
              l_legend.valueTextRegular = undefined;
              l_legend.backgroundColor  = "#E9E9E9";
              l_legend.backgroundAlpha  = 1;
              l_studyPanel.stockLegend = l_legend;

              l_panelPos = chart.panels.push(l_studyPanel) - 1;
              l_panelOwner = true;

              chart.panels[l_panelPos].stockLegend.addListener("hideItem", handleLegend);
              chart.panels[l_panelPos].stockLegend.addListener("showItem", handleLegend);
            } else if (p_showOn == "volume") {
              if (!(g_volumePanelExists)) {
                showVolume(0, p_volumeColour);
              }

              chart.panels[1].addStockGraph(l_graph);
              l_panelPos = 1;
            } else {
              // add the graph to the existing panel
              chart.panels[0].addStockGraph(l_graph);
              l_panelPos = 0;
            }

            if (p_guideType == "C") {
              chart.panels[l_panelPos].categoryAxis.guides = chart.panels[l_panelPos].categoryAxis.guides.concat(p_guides);
            }
        
            chart.panels[l_panelPos].trendLines = chart.panels[l_panelPos].trendLines.concat(p_trends);

            // store some of our own settings for this study so we can re-use them later on
            g_studies.push({id:p_studyId, panelId:chart.panels[l_panelPos].id,panelData:l_studyPanel,graphData:l_graph,panelType:p_showOn,panelOwner:l_panelOwner,parent:null,guides:p_guides,trends:p_trends,guideType:p_guideType,staticGuidesSet:0,minValue:p_minValue,maxValue:p_maxValue});
          } else {
            chart.panels[l_panelPos].addStockGraph(l_graph);
          }

          if (p_studyVals[i].band == "1") {
            var l_stddivFrom = new AmCharts.StockGraph();
            l_stddivFrom.id               = p_studyVals[i].field + "_from";
            l_stddivFrom.valueField       = p_studyVals[i].field + "_from";
            l_stddivFrom.showBalloon      = false;
            l_stddivFrom.visibleInLegend  = false;
            l_stddivFrom.connect          = true;
            l_stddivFrom.type             = "smoothedLine";
            l_stddivFrom.fillAlphas       = 0;
            l_stddivFrom.lineAlpha        = 0;
            l_stddivFrom.useDataSetColors = false;
            l_stddivFrom.lineColor        = l_colour;
            l_stddivFrom.fillColors       = l_colour;
          
            chart.panels[l_panelPos].addStockGraph(l_stddivFrom);
            g_studyChildren.push({parentId:p_studyId, graphData:l_stddivFrom});
          
            var l_stddivTo = new AmCharts.StockGraph();
            l_stddivTo.id               = p_studyVals[i].field + "_to";
            l_stddivTo.fillToGraph      = p_studyVals[i].field + "_from";
            l_stddivTo.valueField       = p_studyVals[i].field + "_to";
            l_stddivTo.showBalloon      = false;
            l_stddivTo.visibleInLegend  = false;
            l_stddivTo.connect          = true;
            l_stddivTo.type             = "smoothedLine";
            l_stddivTo.fillAlphas       = 0.4;
            l_stddivTo.lineAlpha        = 0;
            l_stddivTo.useDataSetColors = false;
            l_stddivTo.lineColor        = l_colour;
            l_stddivTo.fillColors       = l_colour;

            chart.panels[l_panelPos].addStockGraph(l_stddivTo);
            g_studyChildren.push({parentId:p_studyId, graphData:l_stddivTo});
          }
        }
      }
        
      // resize the charts. Main chart = 75% of the visible window, studies = 25% each. Volume is always the first study.
      function adjChartHeight() {
        if (chart.chartCreated) {
          var l_windowHeight = window.innerHeight;
          var l_origHeight = window.innerHeight;
          var l_panelCount = chart.panels.length;';
          
    IF p_type = 'M' THEN
      l_text := l_text||'
          l_windowHeight = l_windowHeight - 60; // remove the scrollbar';
    END IF;

    l_text := l_text||' 
          if (l_panelCount > 2) { // remove menu and bottom buttons
            l_windowHeight = l_windowHeight - 78;
            l_origHeight = l_origHeight - 64;
          } else {
            l_windowHeight = l_windowHeight - 107;
            l_origHeight = l_origHeight - 100;
          }
          
          l_windowHeight = l_windowHeight - (22 * l_panelCount); // remove padding
                  
          var l_panelPct = Math.round(100 / (l_panelCount + 2) * 100) / 100;

          chart.panels[0].percentHeight = l_panelPct * 3;

          if (l_panelCount > 1) {
            for (var i = 1; i < l_panelCount; i++) {
              chart.panels[i].percentHeight = l_panelPct;
            }
          }

          if (l_panelCount > 2) {
            document.getElementById("chart").style.height = Math.round(l_windowHeight / 4 * (l_panelCount + 2)) + "px";
            document.getElementById("content").style.height = Math.round(l_origHeight + (l_windowHeight / 4 * (l_panelCount - 2))) + "px";
          } else {
            document.getElementById("chart").style.height = l_windowHeight + "px";
            document.getElementById("content").style.height = l_origHeight + "px";
          }

          validateChart("N");
        }
      }

      function showVolume (p_refresh, p_colour) {
        if (!(g_volumePanelExists)) {
          // Create the volume panel
          g_volumePanel = new AmCharts.StockPanel();
          g_volumePanel.percentHeight    = 30;
          g_volumePanel.id               = "volumePanel";
          g_volumePanel.usePrefixes      = true;
          g_volumePanel.showCategoryAxis = true;
          g_volumePanel.allLabels        = [{"text":"Volume", "bold":true, "align":"center", "alpha":0.5, "y":10, "size":13}];
          
          // Volume Graph
          var l_vGraph = new AmCharts.StockGraph();
          l_vGraph.id               = "volumeGraph";
       // l_vGraph.balloonText      = "Volume:<b>[[volume]]</b>";
       // l_vGraph.showBalloonAt    = "v";
          l_vGraph.showBalloon      = false;
          l_vGraph.valueField       = "v";
          l_vGraph.type             = "column";
          l_vGraph.connect          = true;
          l_vGraph.fillAlphas       = 1;
          l_vGraph.useDataSetColors = false;
          l_vGraph.title            = "Value";
          l_vGraph.legendValueText  = "[[v]]";
          
          if (typeof p_colour != ''undefined'') {
            l_vGraph.lineColor        = p_colour;
            l_vGraph.fillColors       = p_colour;
          } else {
            l_vGraph.lineColor        = "#69BFFF";
            l_vGraph.fillColors       = "#69BFFF";
          }

          g_volumePanel.addStockGraph(l_vGraph);

          // add a legend for the new panel
          var l_vLegend = new AmCharts.StockLegend();
          l_vLegend.valueTextRegular   = undefined;
          l_vLegend.rollOverGraphAlpha = 1;
          l_vLegend.equalWidths        = false;
          l_vLegend.reversedOrder      = true;
          l_vLegend.backgroundColor    = "#E9E9E9";
          l_vLegend.backgroundAlpha    = 1;
          g_volumePanel.stockLegend = l_vLegend;

          // add the panel to the chart
          chart.addPanelAt(g_volumePanel, 1);
          
          chart.panels[1].stockLegend.addListener("hideItem", handleLegend);
          chart.panels[1].stockLegend.addListener("showItem", handleLegend);

          g_volumePanelExists = true;
          
          if (p_refresh == 1) {
            chart.validateData();';
            
    IF p_type = 'M' THEN
      l_text := l_text||'
            htmlChartNavigation(g_chartId);';
    END IF;

    l_text := l_text||'
            listenForClicks();
          }
        }
      }
      
      function clearChart () {
        if (chart.chartCreated) {
          document.getElementById("chart").innerHTML = "";
          loadChart();
          g_volumePanelExists = false;
        }
      }';
      
    IF p_type = 'M' THEN
      l_text := l_text||'
      // remove a graph from the chart
      function removeStockGraph(p_studyIndex) {
        var l_panelId = g_studies[p_studyIndex].panelId;
        var l_studyId = g_studies[p_studyIndex].id;
        
        if (g_studies[p_studyIndex].panelOwner) {
          var i = g_studies.length;
          while(i--){
            if (g_studies[i].panelId == l_panelId && g_studies[i].id != l_studyId) {
              var j = g_studyChildren.length;
              while(j--){
                if (g_studyChildren[j].parentId == g_studies[i].id) {
                  chart.panels[findPanel(g_studies[p_studyIndex].panelId)].removeStockGraph(g_studyChildren[j].graphData);
                  g_studyChildren.splice(j, 1);
                }
              }
              
              g_studies.splice(i, 1);
            }
          }
        }
        
        var k = g_studyChildren.length;
        while(k--){
          if (g_studyChildren[k].parentId == l_studyId) {
            chart.panels[findPanel(g_studies[p_studyIndex].panelId)].removeStockGraph(g_studyChildren[k].graphData);
            g_studyChildren.splice(k, 1);
          }
        }
        
        var l = chart.panels[findPanel(g_studies[p_studyIndex].panelId)].categoryAxis.guides.length;
        while(l--){
          if (chart.panels[findPanel(g_studies[p_studyIndex].panelId)].categoryAxis.guides[l].id.indexOf("guide_study" + l_studyId + "_") != -1) {
            chart.panels[findPanel(g_studies[p_studyIndex].panelId)].categoryAxis.guides.splice(l, 1);
          }
        }
        
        var l = chart.panels[findPanel(g_studies[p_studyIndex].panelId)].valueAxes[0].guides.length;
        while(l--){
          if (chart.panels[findPanel(g_studies[p_studyIndex].panelId)].valueAxes[0].guides[l].id.indexOf("guide_v_study" + l_studyId + "_") != -1 || chart.panels[findPanel(g_studies[p_studyIndex].panelId)].valueAxes[0].guides[l].id.indexOf("guide_s_study" + l_studyId + "_") != -1) {
            chart.panels[findPanel(g_studies[p_studyIndex].panelId)].valueAxes[0].guides.splice(l, 1);
          }
        }
        
        var l = chart.panels[findPanel(g_studies[p_studyIndex].panelId)].trendLines.length;
        while(l--){
          if (chart.panels[findPanel(g_studies[p_studyIndex].panelId)].trendLines[l].id.indexOf("trend_study" + l_studyId + "_") != -1) {
            chart.panels[findPanel(g_studies[p_studyIndex].panelId)].trendLines.splice(l, 1);
          }
        }

        chart.panels[findPanel(g_studies[p_studyIndex].panelId)].removeStockGraph(g_studies[p_studyIndex].graphData);
      }
      
      // add a study to the chart
      function addStudy(p_studyData) {  
        // delete the study if it already exists
        if (findStudy (p_studyData.studyId) != undefined) { 
          removeStudy(p_studyData.studyId);
        };

        var l_stddiv = 0;
        var l_colorField;
        var l_guides = [];
        var l_trends = [];
        var l_studyVals = [];
        
        // Prepare the dataset
        for(var i = 0; i < chart.dataSets[0].dataProvider.length; i++) {
          for(var p = 0; p < p_studyData.studyData.length; p++) {
            if (typeof p_studyData.studyData[p].values[i] != ''undefined'' && p_studyData.studyData[p].values[i].v != null) {
              chart.dataSets[0].dataProvider[i]["study" + p_studyData.studyId + p_studyData.studyData[p].code] = p_studyData.studyData[p].values[i].v;

              if (typeof p_studyData.studyData[p].values[i].bf != ''undefined'') {
                chart.dataSets[0].dataProvider[i]["study" + p_studyData.studyId + p_studyData.studyData[p].code + "_from"] = p_studyData.studyData[p].values[i].bf;
              }
          
              if (typeof p_studyData.studyData[p].values[i].bt != ''undefined'') {
                chart.dataSets[0].dataProvider[i]["study" + p_studyData.studyId + p_studyData.studyData[p].code + "_to"] = p_studyData.studyData[p].values[i].bt;
              }
          
              if (typeof p_studyData.studyData[p].values[i].c != ''undefined'') {
                chart.dataSets[0].dataProvider[i]["colour" + p_studyData.studyId + p_studyData.studyData[p].code] = p_studyData.studyData[p].values[i].c;
              }
            }
          }
        }
        
        for(var p = 0; p < p_studyData.studyData.length; p++) {          
          chart.dataSets[0].fieldMappings.push({fromField:"study" + p_studyData.studyId + p_studyData.studyData[p].code, toField:"study" + p_studyData.studyId + p_studyData.studyData[p].code});
        
          if (p_studyData.studyData[p].values.length > 0) {
            if (typeof p_studyData.studyData[p].values[0].bf != ''undefined'') {
              l_stddiv = 1;
              chart.dataSets[0].fieldMappings.push({fromField:"study" + p_studyData.studyId + p_studyData.studyData[p].code + "_from", toField:"study" + p_studyData.studyId + p_studyData.studyData[p].code + "_from"});
            }
        
            if (typeof p_studyData.studyData[p].values[0].bt != ''undefined'') {
              chart.dataSets[0].fieldMappings.push({fromField:"study" + p_studyData.studyId + p_studyData.studyData[p].code + "_to", toField:"study" + p_studyData.studyId + p_studyData.studyData[p].code + "_to"});
            }
        
            if (typeof p_studyData.studyData[p].values[0].c != ''undefined'') {
              chart.dataSets[0].fieldMappings.push({fromField:"colour" + p_studyData.studyId + p_studyData.studyData[p].code, toField:"colour" + p_studyData.studyId + p_studyData.studyData[p].code});
              l_colorField = "colour" + p_studyData.studyId + p_studyData.studyData[p].code;
            } else {
              l_colorField = undefined;
            }
          } else {
            l_colorField = undefined;
          }
          
          l_studyVals.push ({field:"study" + p_studyData.studyId + p_studyData.studyData[p].code,name:p_studyData.studyData[p].name,chartType:p_studyData.studyData[p].overrideChartType,colourField:l_colorField,band:l_stddiv});
        }
        
        l_guides = [];
            
        if (p_studyData.studyGuides.length > 0) {
          if (p_studyData.studySettings.guideType == "C") {
            for(var k = 0; k < p_studyData.studyGuides.length; k++) {
              l_guides.push({id:"guide_study" + p_studyData.studyId + "_" + k, date:new Date(p_studyData.studyGuides[k].d), toDate:new Date(p_studyData.studyGuides[k].dt), dashLength:2, lineThickness:1, lineAlpha:0.8, lineColor:p_studyData.studySettings.colour, fillAlpha:0.3, fillColor:p_studyData.studySettings.colour, inside:true, label:p_studyData.studyGuides[k].l, position: "bottom", color:"#4C4C4C"});
            }
          } else if (p_studyData.studySettings.guideType == "V") {
            for(var k = 0; k < p_studyData.studyGuides.length; k++) {
              l_guides.push({id:"guide_v_study" + p_studyData.studyId + "_" + k, date:new Date(p_studyData.studyGuides[k].d), toDate:new Date(p_studyData.studyGuides[k].dt), value:p_studyData.studyGuides[k].v, toValue:p_studyData.studyGuides[k].vt, dashLength:0, lineThickness:0, lineAlpha:0, lineColor:p_studyData.studySettings.colour, fillAlpha:0.3, fillColor:p_studyData.studySettings.colour, inside:true, label:p_studyData.studyGuides[k].l, position: "bottom", color:"#4C4C4C"});
            }
          } else if (p_studyData.studySettings.guideType == "S") {
            for(var k = 0; k < p_studyData.studyGuides.length; k++) {
              l_guides.push({id:"guide_s_study" + p_studyData.studyId + "_" + k, value:p_studyData.studyGuides[k].v, dashLength:2, lineThickness:1, lineAlpha:1, lineColor:p_studyData.studySettings.colour, fillAlpha:0.2, fillColor:p_studyData.studySettings.colour, inside:true, label:p_studyData.studyGuides[k].l, position: "left", color:"#4C4C4C"});
            }
          }
        }

        l_trends = [];

        if (p_studyData.studyTrends.length > 0) {
          for(var k = 0; k < p_studyData.studyTrends.length; k++) {
            l_trends.push({id:"trend_study" + p_studyData.studyId + "_" + k, initialDate:new Date(p_studyData.studyTrends[k].d), finalDate:new Date(p_studyData.studyTrends[k].dt), initialValue:p_studyData.studyTrends[k].v, finalValue:p_studyData.studyTrends[k].vt, lineColor:p_studyData.studySettings.colour});
          }
        }
        
        chart.validateData();
        
        addGraph(p_studyData.studyId, p_studyData.studySettings.studyType, p_studyData.studySettings.studyTitle, p_studyData.studySettings.showOn, p_studyData.studySettings.colour, p_studyData.studySettings.volumeColour, l_studyVals, p_studyData.studySettings.guideType, l_guides, l_trends, p_studyData.studySettings.minValue, p_studyData.studySettings.maxValue);
        
        // resize the charts
        if (!(p_studyData.studySettings.showOn == "main")) {
          adjChartHeight();
        }

        // refresh the screen
        validateChart("Y");
        
        if ("0" in g_areaValues) {
          addAreaGuide("Entry", g_areaValues[0].from, g_areaValues[0].to, g_areaValues[0].colour, g_areaValues[0].label);
        }
        if ("1" in g_areaValues) {
          addAreaGuide("Exit", g_areaValues[1].from, g_areaValues[1].to, g_areaValues[1].colour, g_areaValues[1].label);
        }
        
        addStaticGuides();
        resetValueGuides (chart.startDate,chart.endDate);
      }
      
      // remove a study from the chart
      function removeStudy(p_studyId) {
        var l_studyIndex = findStudy(p_studyId);
          
        if (l_studyIndex != undefined) {
          // remove graph
          removeStockGraph(l_studyIndex);
          
          // remove the panel if the study owns it
          if (g_studies[l_studyIndex].panelOwner) {
            chart.removePanel(g_studies[l_studyIndex].panelData);
            adjChartHeight();
          }
          
          g_studies.splice(l_studyIndex, 1);
        
          // refresh the chart
          validateChart("Y");
        }
      }
      
    // jump to a date range
      function goToDates(p_fromDate, p_toDate) {
        var l_from;
        var l_to;
        
        if (p_fromDate instanceof Date) {
          l_from = p_fromDate;
        } else {
          l_from = new Date(p_fromDate);
        }
        
        if (p_toDate instanceof Date) {
          l_to = p_toDate;
        } else {
          l_to = new Date(p_toDate);
        }
        
        g_syncDate = 0;

        if (l_from != undefined && l_to != undefined) {
          chart.zoom (l_from, l_to);
        }
        
        resetValueGuides(l_from, l_to);
        g_syncDate = 1;
      }
      
      // keep tabs on scroll, zoom and period change events
      function handleScroll(event) {
        if (g_syncDate == 1 && g_resettingGuides != 1) {
          if(g_mouseDown){
            g_chartScrolled = 1;
          } else {
            if (chart.chartCreated) {
              chart_sync(chart.startDate,chart.endDate);
              resetValueGuides (chart.startDate,chart.endDate);
            }
          }
        }
      }
        
      function hideVolume () {
        if (g_volumePanelExists) {
          // remove any volume studies
          for(var i = 0; i < g_studies.length; i++){
            if (g_studies[i].panelType == "volume") {
              removeStudy(g_studies[i].id);
              studiesRemoved = 1;
            }
          }

          // remove the panel
          chart.removePanel(g_volumePanel);

          // resize the screen
          adjChartHeight();

          // refresh the screen
          validateChart("Y");

          g_volumePanelExists = false;
        }
      }
      
      function selectRange (p_type, p_colour, p_label) {
        for(var i = 0; i < chart.chartCursors.length; i++){
          chart.chartCursors[i].pan = false;
          chart.chartCursors[i].zoomable = true;
          chart.chartCursors[i].selectWithoutZooming = true;
          if (p_colour != undefined) {
            chart.chartCursors[i].cursorColor = p_colour;
          }
          
          chart.chartCursors[i].addListener("selected", handleSelected);
        }
        
        g_currentAreaType = p_type;
        g_currentAreaLabel = p_label;
        
        chart.validateData();
      }
      
      function handleSelected (event) {
        var l_colour;

        for(var i = 0; i < chart.chartCursors.length; i++){
          chart.chartCursors[i].pan = true;
          chart.chartCursors[i].zoomable = false;
          chart.chartCursors[i].selectWithoutZooming = false;
          chart.chartCursors[i].removeListener(chart.chartCursors[i], "selected", handleSelected);
          l_colour = chart.chartCursors[i].cursorColor;
        }

        for(var i = 0; i < chart.chartCursors.length; i++){
          chart.chartCursors[i].cursorColor = "#FF6600";
        }
        
        addAreaGuide(g_currentAreaType, chart.dataSets[0].dataProvider[event.start].d, chart.dataSets[0].dataProvider[event.end].d, l_colour, g_currentAreaLabel);
        
        postSetupAreaSelection (chart.dataSets[0].dataProvider[event.start].d, chart.dataSets[0].dataProvider[event.end].d, g_currentAreaType, l_colour);
      }
      
      function setPeriod (p_period, p_count) {
        var l_from;
        var l_to;
        var l_curHours;
          
        if (p_period == "hh") {
          l_curHours = p_count;
        } else if (p_period == "DD") {
          l_curHours = p_count * 24;
        } else if (p_period == "WW") {
          l_curHours = p_count * 24 * 7;
        } else if (p_period == "MM") {
          l_curHours = p_count * 24 * 30;
        } else if (p_period == "YYYY") {
          l_curHours = p_count * 24 * 365;
        } else {
          l_curHours = 1;
        }
        
        if (g_syncType == "start") {
          l_from = chart.startDate;
          l_to = new Date(l_from.valueOf());
          l_to.setUTCHours(+l_to.getUTCHours()+ +l_curHours);
        } else if (g_syncType == "end") {
          l_to   = chart.endDate;
          l_from = new Date(l_to.valueOf());
          l_from.setHours(+l_from.getHours()- +l_curHours);
        } else if (g_syncType == "centre") {
          l_from = new Date(((+chart.startDate.getTime() + +chart.endDate.getTime()) / 2).valueOf());
          l_from = new Date(l_from.setHours(+l_from.getHours() - (+l_curHours / 2)));
          l_to = new Date(((+chart.startDate.getTime() + +chart.endDate.getTime()) / 2).valueOf());
          l_to = new Date(l_to.setHours(+l_to.getHours() + (+l_curHours / 2)));
        }
          
        if (l_to > chart.lastDate) {
           l_from = new Date(l_from - (l_to - chart.lastDate));
           l_to = chart.lastDate;
        }
        
        if (l_from < chart.firstDate) {
          l_from = chart.firstDate;
          l_to = new Date(l_to + (chart.firstDate - l_from));
        }
                
        goToDates(l_from,l_to);
        chart_sync(l_from,l_to);
        resetValueGuides (l_from,l_to);
      }';
    END IF;
      
    l_text := l_text || '
      function colourAreaGuide(p_type, p_colour) {
        var i;
        
        if (p_type == "Entry") {i=0;}
        else {i=1;}
        
        addAreaGuide(p_type, g_areaValues[i].from, g_areaValues[i].to, p_colour, g_areaValues[i].label);
      }
      
      function addAreaGuide(p_type, p_from, p_to, p_colour, p_label) {
        var l_guide;
        var l_label = p_label;
        var k;
        
        if (p_type == "Entry") {k=0;}
        else {k=1;}
                  
        g_areaValues[k] = {from:p_from,to:p_to,colour:p_colour,label:p_label};
        
        for(var i = 0; i < chart.panels.length; i++){
          var j = chart.panels[i].categoryAxis.guides.length;
          while(j--){
            if (chart.panels[i].categoryAxis.guides[j].id == "guide_" + p_type) {
              chart.panels[i].categoryAxis.guides.splice(j, 1);
            }
          }
          
          if (i == 0) {
            l_label = p_label;
          } else {
            l_label = "";
          }
          
          if (p_from != undefined && p_to != undefined) {
            l_guide = [];
            l_guide.push({id:"guide_" + p_type, date:p_from, toDate:p_to, dashLength:2, lineThickness:1, lineAlpha:0.5, lineColor:p_colour, fillAlpha:0.2, fillColor:p_colour, inside:true, label:l_label, position: "bottom", color:"#4C4C4C"});
            chart.panels[i].categoryAxis.guides = chart.panels[i].categoryAxis.guides.concat(l_guide);
          }
        }
        if (chart.chartCreated) {
          chart.validateData();';
    IF p_type = 'M' THEN
      l_text := l_text||'
          htmlChartNavigation(g_chartId);';
    END IF;
    
    l_text := l_text||'
        }
      }
      
      function handleLegend(event) {
        if (!(event.dataItem.id == "mainGraph" || event.dataItem.id == "volumeGraph")) {
          var l_studyIndex = findStudy(event.dataItem.id);
                  
          if (l_studyIndex != undefined) { 
            var l_panelIndex = findPanel(g_studies[l_studyIndex].panelId);
            
            if (event.type == "showItem") {
              if (g_studies[l_studyIndex].guides != undefined) {
                if (g_studies[l_studyIndex].guideType == "C") {
                  chart.panels[l_panelIndex].categoryAxis.guides = chart.panels[l_panelIndex].categoryAxis.guides.concat(g_studies[l_studyIndex].guides);
                } else if (g_studies[l_studyIndex].guideType == "V") {
                  resetValueGuides (chart.startDate,chart.endDate);
                } else if (g_studies[l_studyIndex].guideType == "S") {
                  addStaticGuides();
                }
              }
              
              var j = g_studyChildren.length;
              while(j--){
                if (g_studyChildren[j].parentId == g_studies[l_studyIndex].id) {
                  chart.panels[l_panelIndex].showGraph(g_studyChildren[j].graphData);
                }
              }
              
              if (g_studies[l_studyIndex].trends != undefined) {
                chart.panels[l_panelIndex].trendLines = chart.panels[l_panelIndex].trendLines.concat(g_studies[l_studyIndex].trends);
              }
            } else if (event.type == "hideItem") {
              if (g_studies[l_studyIndex].guides != undefined) {
                if (g_studies[l_studyIndex].guideType == "C") {
                  var l = chart.panels[l_panelIndex].categoryAxis.guides.length;
                  while(l--){
                    if (chart.panels[l_panelIndex].categoryAxis.guides[l].id.indexOf("guide_study" + g_studies[l_studyIndex].id + "_") != -1) {
                      chart.panels[l_panelIndex].categoryAxis.guides.splice(l, 1);
                    }
                  }
                } else if (g_studies[l_studyIndex].guideType == "V" || g_studies[l_studyIndex].guideType == "S") {
                  var l = chart.panels[l_panelIndex].valueAxes[0].guides.length;
                  while(l--){
                    if (chart.panels[l_panelIndex].valueAxes[0].guides[l].id.indexOf("guide_v_study" + g_studies[l_studyIndex].id + "_") != -1 || chart.panels[l_panelIndex].valueAxes[0].guides[l].id.indexOf("guide_s_study" + g_studies[l_studyIndex].id + "_") != -1) {
                      chart.panels[l_panelIndex].valueAxes[0].guides.splice(l, 1);
                    }
                  } 
                  g_studies[l_studyIndex].staticGuidesSet = 0;
                }
              }

              var j = g_studyChildren.length;
              while(j--){
                if (g_studyChildren[j].parentId == g_studies[l_studyIndex].id) {
                  chart.panels[l_panelIndex].hideGraph(g_studyChildren[j].graphData);
                }
              }
              
              if (g_studies[l_studyIndex].trends != undefined) {
                var l = chart.panels[l_panelIndex].trendLines.length;
                while(l--){
                  if (chart.panels[l_panelIndex].trendLines[l].id.indexOf("trend_study" + g_studies[l_studyIndex].id + "_") != -1) {
                    chart.panels[l_panelIndex].trendLines.splice(l, 1);
                  }
                }
              }
            }
          }
        }
      }
      
      function addStaticGuides () {
        g_resettingGuides = 1;
        
        var l_studyIndex;
        var l_panel;
        var l_changes = 0;
        
        for(var i = 0; i < g_studies.length; i++) {
          if (g_studies[i].guideType == "S") {
            l_studyIndex = findStudy(g_studies[i].id);
            l_panel = findPanel(g_studies[l_studyIndex].panelId);
            
            if (g_studies[i].minValue != undefined) {
              chart.panels[l_panel].valueAxes[0].minimum = g_studies[i].minValue;
            }
            
            if (g_studies[i].maxValue != undefined) {
              chart.panels[l_panel].valueAxes[0].maximum = g_studies[i].maxValue;
            }
          
            if (g_studies[i].guides.length > 0) {
              for(var j = 0; j < g_studies[i].guides.length; j++) {
                if (g_studies[i].staticGuidesSet == 0) {
                  l_changes == 1;
                  chart.panels[l_panel].valueAxes[0].guides = chart.panels[l_panel].valueAxes[0].guides.concat({id:g_studies[i].guides[j].id, value:g_studies[i].guides[j].value, dashLength:g_studies[i].guides[j].dashLength, lineThickness:g_studies[i].guides[j].lineThickness, lineAlpha:g_studies[i].guides[j].lineAlpha, lineColor:g_studies[i].guides[j].lineColor, fillAlpha:g_studies[i].guides[j].fillAlpha, fillColor:g_studies[i].guides[j].fillColor, inside:g_studies[i].guides[j].inside, label:g_studies[i].guides[j].label, position:g_studies[i].guides[j].position, color:g_studies[i].guides[j].color});
                }
              }
              
              g_studies[i].staticGuidesSet = 1;
            }
          }
        }
        
        if (chart.chartCreated && l_changes == 1) {';
    IF p_type = 'M' THEN
      l_text := l_text||'
          validateChart("Y");';
    ELSE
      l_text := l_text||'
          validateChart("N");';
    END IF;
    
    l_text := l_text||'
        }

        g_resettingGuides = 0;
      }
      
      function resetValueGuides (p_from,p_to) {      
        g_resettingGuides = 1;
        
        var l_studyIndex;
        var l_panel;
        var l_changes = 0;
        
        for(var i = 0; i < g_studies.length; i++) {
          if (g_studies[i].guideType == "V") { 
            if (g_studies[i].guides.length > 0) {
              l_studyIndex = findStudy(g_studies[i].id);
              l_panel = findPanel(g_studies[l_studyIndex].panelId);
              
              var l = chart.panels[l_panel].valueAxes[0].guides.length;
              while(l--){
                if (chart.panels[l_panel].valueAxes[0].guides[l].id.indexOf("guide_v_study" + g_studies[i].id + "_") != -1) {
                  l_changes = 1;
                  chart.panels[l_panel].valueAxes[0].guides.splice(l, 1);
                }
              }
           
              for(var j = 0; j < g_studies[i].guides.length; j++) {
                if ((p_from.getTime() >= g_studies[i].guides[j].date.getTime() && p_from.getTime() <= g_studies[i].guides[j].toDate.getTime()) || (p_to.getTime() >= g_studies[i].guides[j].date.getTime() && p_to.getTime() <= g_studies[i].guides[j].toDate.getTime())) {
                  l_changes = 1;
                  chart.panels[l_panel].valueAxes[0].guides = chart.panels[l_panel].valueAxes[0].guides.concat({id:g_studies[i].guides[j].id, value:g_studies[i].guides[j].value, toValue:g_studies[i].guides[j].toValue, dashLength:g_studies[i].guides[j].dashLength, lineThickness:g_studies[i].guides[j].lineThickness, lineAlpha:g_studies[i].guides[j].lineAlpha, lineColor:g_studies[i].guides[j].lineColor, fillAlpha:g_studies[i].guides[j].fillAlpha, fillColor:g_studies[i].guides[j].fillColor, inside:g_studies[i].guides[j].inside, label:g_studies[i].guides[j].label, position:g_studies[i].guides[j].position, color:g_studies[i].guides[j].color});
                }
              }
            }
          }
        }
        
        if (chart.chartCreated && l_changes == 1) {
          chart.validateData();';

    l_text := l_text||'
          }
        
        g_resettingGuides = 0;
      }

      listenForClicks = function () {
        for(var i in chart.chartCursors){
          chart.chartCursors[i].removeListener(chart.chartCursors[i],"changed", handleMove);
          chart.chartCursors[i].addListener("changed", handleMove);
        }';
        
    IF p_type = 'C' THEN
      l_text := l_text||'
      for(var i in chart.panels) {
          chart.panels[i].chartDiv.onclick = handleClick;
        }';
    END IF;

    l_text := l_text||'
      }
      
      handleMove = function (event) {
        if ( undefined === event.index) {
          showCategoryBalloon();
        } else {
          g_cursorPosition = event.chart.dataProvider[event.index].d;
          showCategoryBalloon(event.chart.id,event.chart.dataProvider[event.index].d);
        }
      }';
      
    IF p_type = 'C' THEN
      l_text := l_text||'
      handleClick = function (event) {
        console.log("Click Event: " + g_cursorPosition.toUTCString());
      }';
    END IF;

    l_text := l_text||'
      showCategoryBalloon = function (p_panel,p_position) {
        if ((p_panel != undefined && g_cursorPanel == undefined) || (p_panel == undefined && g_cursorPanel != undefined) || p_panel != g_cursorPanel) {
          for(var i in chart.chartCursors) {
            chart.chartCursors[i].animationDuration = 0;
            chart.chartCursors[i].categoryBalloon.animationDuration = 0;
            chart.chartCursors[i].categoryBalloon.fadeOutDuration = 0;
            chart.chartCursors[i].vaBalloon.fadeOutDuration = 0;
              
            if (chart.chartCursors[i].chart.id == p_panel) {
              chart.chartCursors[i].categoryBalloonEnabled = true;
              if (p_position != undefined) {
                chart.chartCursors[i].showCursorAt(p_position);
              }
            } else {
              chart.chartCursors[i].categoryBalloonEnabled = false;
            }
          }

          g_cursorPanel = p_panel;
        }
      }
      
      validateChart = function (p_refreshNav) {
        chart.validateNow();
        listenForClicks();
        
        if (p_refreshNav == "Y") {
          htmlChartNavigation(g_chartId);
        }
        
        showCategoryBalloon();
      }';
    
    l_text := l_text||'
    </script>';
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Chart Load Command.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  
  END IF;
  
  RETURN l_text;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;