CREATE OR REPLACE FUNCTION ss_src."a_StudyMenu_get"(p_uctId  ss_dat.user_charts.uct_id%TYPE) RETURNS JSON AS
$BODY$
DECLARE
  l_json    JSON;
  l_uctRow  ss_dat.user_charts%ROWTYPE;
  l_ustRow  ss_dat.user_strategies%ROWTYPE;
  l_hide    TEXT;
  l_group   TEXT;
  l_sub     TEXT;
  
  l_taskId  INTEGER;
  
  l_data    text;
  l_debug   text;
  l_context text;
  l_text    text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Study Menu.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);

  IF p_uctId IS NOT NULL THEN
    l_uctRow := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);
    l_ustRow := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := l_uctRow.uct_ust_id);
    
    IF l_uctRow.uct_id IS NULL OR
       l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE
    l_text := '<head><style> .ASMMenu li a h3 {background: url("../assets/default/images/content/right.png") no-repeat 100%;font-weight: normal !important;font-size:16px !important;} .ASMMenu li a {text-decoration:none;} .ASMHomeHEADERHREF {right:0; position:absolute; padding-right:5px; top: 3px;} .ASMHomeMAIN, .ASMHomeHOME ul{list-style-type: none; margin: 0 0 0px 0px !important;} .ASMHomeHEADER{list-style-type: none; margin: 0 0 0px 0px !important;} .ASMHomeMAIN li {line-height:50px; width:100%;} .ASMHomeMAIN li :hover {background-color: #E7EAEC;} .ASMSubMenu li :hover {background-color: #E7EAEC;} .ASMSubMenu {margin: 0px;} .ASMSubMenu li {list-style-type: none;} </style></head><script type="text/javascript">';
  
    WITH studies AS (SELECT dstd_stg_id,
                            dstd_name,
                            '<li id="'||dstd_code||'"><a href="#"><h3>'||dstd_name||'</h3></a></li>' study_html
                       FROM ss_dat.d_studies
                      WHERE dstd_enabled_ind = 'Y'
                        AND dstd_id NOT IN (SELECT ucs_dstd_id
                                              FROM ss_dat.user_chart_studies
                                             WHERE ucs_uct_id = p_uctId
                                               AND dstd_multi_add = 'N'))
    SELECT STRING_AGG(a.hide_html,' ' ORDER BY stg_order),
           STRING_AGG(a.group_html,'' ORDER BY stg_order),
           STRING_AGG(a.sub_html,'' ORDER BY stg_order)
      INTO l_hide,
           l_group,
           l_sub
      FROM (SELECT '$( "#ASM'||stg_code||'" ).hide();' hide_html,
                   '<li id="ASM'||stg_code||'MAIN"><a href="#"><h3>'||stg_name||'</h3></a></li>' group_html,
                   '<ul id="ASM'||stg_code||'" class="ASMSubMenu">'||studies_html||'</ul>' sub_html,
                   stg_order
              FROM (SELECT stg_code,
                           stg_name,
                           stg_order,
                           (SELECT STRING_AGG(study_html,'' ORDER BY dstd_name) FROM studies WHERE stg_id = dstd_stg_id) studies_html
                      FROM ss_dat.study_groups
                        JOIN studies ON stg_id = dstd_stg_id
                    UNION
                    SELECT 'RECENT' stg_code,
                           'Recently Used' stg_name,
                           -1 stg_order,
                           (SELECT STRING_AGG(study_html,'' ORDER BY dstd_name)
                              FROM (SELECT '<li id="'||dstd_code||'"><a href="#"><h3>'||dstd_name||'</h3></a></li>' study_html,
                                           dstd_name
                                      FROM ss_dat.d_studies
                                     WHERE dstd_enabled_ind = 'Y'
                                       AND dstd_id IN (SELECT ucs_dstd_id
                                                         FROM ss_dat.user_chart_studies
                                                        WHERE ucs_uct_id IN (SELECT uct_id
                                                                               FROM ss_dat.user_charts
                                                                                 JOIN ss_dat.user_strategies ON ust_id = uct_ust_id
                                                                              WHERE ust_usr_id = l_ustRow.ust_usr_id)
                                                          AND dstd_id NOT IN (SELECT ucs_dstd_id
                                                                                FROM ss_dat.user_chart_studies
                                                                                  JOIN ss_dat.d_studies ON ucs_dstd_id = dstd_id
                                                                               WHERE ucs_uct_id = p_uctId
                                                                                 AND dstd_multi_add = 'N')
                                                        GROUP BY ucs_dstd_id
                                                        ORDER BY MAX(ucs_id) DESC
                                                        LIMIT 5)) d) c) b) a;
  
    l_text := l_text||' '||l_hide||' $("#parentMenu").val(''Home''); function ASMHome() {$( "#add_study_details" ).hide(); '||l_hide;
  
    l_text := l_text||' '||'var parentMenu = $("#parentMenu").val(); if (parentMenu == ''Home'') {$( "#ASMHomeHEADER" ).hide(); $( "#ASMHomeMAIN" ).hide(); $( "#studies_menu" ).show();} else if (parentMenu == ''subMenu'') {$("#parentMenu").val(''Home''); $("#ASMHomeTITLE").html(''Add Study''); $( "#ASMHomeMAIN" ).show();} else {$("#parentMenu").val(''subMenu''); var sumMenuName = $("#parentMenu").attr("name"); $("#ASMHomeTITLE").html(sumMenuName); $( ''#''+parentMenu ).show();}} $("#ASMHomeMAIN li").not(''.emptyMessage'').click(function() {var menuId = this.id; var subMenuTitle = $(''#''+menuId+'' h3'').html(); var listId = menuId.replace(/MAIN/i, ""); if(listId !== ''ASMHome''){$("#parentMenu").val(''subMenu''); $("#ASMHomeMAIN" ).hide(); $("#ASMHomeTITLE").html(subMenuTitle); $("#"+listId+"").show(); }}); $(".ASMSubMenu li").not(''.emptyMessage'').click(function() {var menuId = this.id; var subMenuTitle = $(''#''+menuId+'' h3'').html(); var MenuTitle = $("#ASMHomeTITLE").html(); $("#ASMHomeTITLE").html(subMenuTitle); $( ".ASMSubMenu" ).hide(); var parentMenuId = $(this).parent().attr(''id''); $("#parentMenu").val(parentMenuId); $("#parentMenu").attr(''name'', MenuTitle); var add_study = this.id; $("#ASMHomeTITLE").attr(''name'', add_study); create_study_code_HTMLmenu(add_study);}); </script><div class="ASMMenu"><input type="hidden" name="parentMenu" id="parentMenu" value="" /> <ul id="ASMHomeHEADER" class="ASMHomeHEADER"><li id="ASMHomeHOME" class="ASMHomeMAIN"><h3 id="ASMHomeTITLE">Add Study</h3><a href="#" class="ASMHomeHEADERHREF" onclick="ASMHome();"><img id="ASMHomeIMG" title="Slide back home" alt="busy..." src="../assets/default/images/content/home.svg" width="20" height="20"/></a></li><hr><br></ul>';
  
    l_text := l_text||'<ul id="ASMHomeMAIN" class="ASMHomeMAIN">'||l_group||'</ul>'||l_sub;
    
    l_text := l_text||' '||'<div id="add_study_details" class="add_study_details" ></div></div>';
                 
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(a) "studyMenu",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT l_text "menuHTML") a) b;
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Study Menu',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  
  END IF;
  
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;