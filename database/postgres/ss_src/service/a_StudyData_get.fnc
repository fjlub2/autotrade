CREATE OR REPLACE FUNCTION ss_src."a_StudyData_get"(p_ucsId ss_dat.user_chart_studies.ucs_id%TYPE)
  RETURNS json AS
$BODY$
DECLARE
  l_json      JSON;
  l_ustRow    ss_dat.user_strategies%ROWTYPE;
  l_uctRow    ss_dat.user_charts%ROWTYPE;
  l_ucsRow    ss_dat.user_chart_studies%ROWTYPE;
  l_ct        ss_dat.chart_types.cht_chart_type%TYPE;
  l_taskId    INTEGER;
  
  l_data      text;
  l_debug     text;
  l_context   text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Study Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF p_ucsId IS NOT NULL THEN
    l_ucsRow := ss_tapi."t_UserChartStudies_getRow_PK"(p_pk := p_ucsId);
        
    IF l_ucsRow.ucs_id IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','study'),
                                              p_data   := core_src."c_Data_addTag"('UCS_ID',p_ucsId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    ELSE
      l_uctRow := ss_tapi."t_UserCharts_getRow_PK"(p_pk := l_ucsRow.ucs_uct_id);
      
      l_ustRow := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := l_uctRow.uct_ust_id);
      
      IF l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- strategy doesnt exist
                                                p_vars   := core_src."c_Data_addTag"('FIELD','strategy'),
                                                p_data   := core_src."c_Data_addTag"('UCS_ID',p_ucsId),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
      ELSE
        IF l_ustRow.ust_dsym_id IS NULL THEN
          PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0006', -- chart not complete
                                                  p_data   := core_src."c_Data_addTag"('UCS_ID',p_ucsId),
                                                  p_type   := 'E',
                                                  p_taskId := l_taskId);
        END IF;
  
        IF l_uctRow.uct_dtfr_id IS NULL OR
           l_uctRow.uct_from    IS NULL OR
           l_uctRow.uct_to      IS NULL THEN
          PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0006', -- chart not complete
                                                  p_data   := core_src."c_Data_addTag"('UCS_ID',p_ucsId),
                                                  p_type   := 'E',
                                                  p_taskId := l_taskId);
        END IF;
      END IF;
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  l_json := ss_src."c_StudyData_get"(p_ucsId  := l_ucsRow.ucs_id,
                                     p_dstdId := l_ucsRow.ucs_dstd_id,
                                     p_chtId  := l_ucsRow.ucs_cht_id,
                                     p_showOn := l_ucsRow.ucs_chart_panel,
                                     p_dsymId := l_ustRow.ust_dsym_id,
                                     p_dtfrId := l_uctRow.uct_dtfr_id,
                                     p_from   := l_uctRow.uct_from,
                                     p_to     := l_uctRow.uct_to,
                                     p_uctId  := l_uctRow.uct_id,
                                     p_name   := l_ucsRow.ucs_name);
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE                            
    SELECT ROW_TO_JSON(b)
      INTO l_json
      FROM (SELECT l_json "study",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages") b;
              
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning study data.',
                                            p_data    := core_src."c_Data_addTag"('UCS_ID',p_ucsId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;


