CREATE OR REPLACE FUNCTION ss_src."a_Contracts_get"(pi_product character varying)
  RETURNS json AS
$BODY$
DECLARE
  l_json     JSON;
  l_dsygId   ss_dat.d_symbol_groups.dsyg_id%TYPE;
  l_taskId   INTEGER;
    
  l_data     text;
  l_debug    text;
  l_context  text;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Contracts.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF COALESCE(pi_product,'') != '' THEN
    l_dsygId := ss_tapi."t_DSymbolGroups_getId_UK"(p_uk := pi_product);
    
    IF l_dsygId IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- product doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','product'),
                                              p_data   := core_src."c_Data_addTag"('PRODUCT_CODE',pi_product),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Product'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := core_src."c_Message_getErrors_json" (p_taskId := l_taskId);
  ELSE 
    SELECT ROW_TO_JSON(c)
      INTO l_json
      FROM (SELECT ROW_TO_JSON(b) "contracts",
                   '{"items":[{"code":200,"message":"OK"}]}'::json "messages"
              FROM (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a))) "items"
                      FROM (SELECT dsym_code "code",
                                   dsym_name "desc",
                                   dsym_year "year",
                                   (SELECT dmon_mm FROM ss_dat.d_months WHERE dmon_id = dsym_dmon_id) "month",
                                   REPLACE(LEAST(ROW_NUMBER() OVER (ORDER BY 1),6)::text,'6','') "recent"
                              FROM ss_dat.d_symbols
                             WHERE dsym_dsyg_id = l_dsygId
                             ORDER BY dsym_code DESC) a) b) c;
                           
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning contracts.',
                                            p_data    := core_src."c_Data_addTag"('DSYD_ID',l_dsygId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN l_json;
EXCEPTION
  WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS l_debug   = PG_EXCEPTION_DETAIL,
                            l_data    = PG_EXCEPTION_HINT,
                            l_context = PG_EXCEPTION_CONTEXT;
                            
    l_debug := CASE l_debug WHEN '' THEN NULL ELSE l_debug END;
    l_data  := CASE l_data  WHEN '' THEN NULL ELSE l_data  END;
    
    RETURN core_src."c_Message_errorHandler"(p_code    := SQLSTATE,
                                             p_message := SQLERRM,
                                             p_data    := CAST(l_data AS core_dat.tr_data[]),
                                             p_debug   := l_debug,
                                             p_context := l_context);
END;
$BODY$
  LANGUAGE plpgsql STABLE;