CREATE OR REPLACE FUNCTION ss_src."c_buildStudy_EMA"() RETURNS text AS
$BODY$
DECLARE
  l_firstEMA_o   ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_firstEMA_h   ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_firstEMA_l   ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_firstEMA_c   ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_val_o        ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_val_h        ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_val_l        ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_val_c        ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_prevVal_o    ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_prevVal_h    ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_prevVal_l    ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_prevVal_c    ss_dat.f_study_EMA.fsem_value_close%TYPE;

  r_rec        RECORD;
  r_periods    RECORD;
BEGIN                         
  FOR r_periods IN (WITH prevData AS (SELECT fsem_dsym_id        pd_dsym_id,
                                             fsem_dtfr_id        pd_dtfr_id,
                                             fsem_period_dprv_id pd_period_dprv_id,
                                             fsem_date           pd_fsem_date,
                                             fsem_value_close    pd_fsem_value_close,
                                             fsem_value_open     pd_fsem_value_open,
                                             fsem_value_high     pd_fsem_value_high,
                                             fsem_value_low      pd_fsem_value_low
                                        FROM (SELECT fsem_dsym_id,
                                                     fsem_dtfr_id,
                                                     fsem_period_dprv_id,
                                                     fsem_date,
                                                     fsem_value_close,
                                                     fsem_value_open,
                                                     fsem_value_high,
                                                     fsem_value_low,
                                                     ROW_NUMBER() OVER w1 AS keep_one
                                                FROM ss_dat.f_study_EMA
                                              WINDOW
                                                w1 AS (PARTITION BY fsem_dsym_id, fsem_dtfr_id, fsem_period_dprv_id ORDER BY fsem_date DESC)) a
                                       WHERE keep_one = 1)
                    SELECT dsym_id,
                           dsym_decimals,
                           dtfr_id,
                           dprv_id,
                           ROUND(2::numeric / (dprv_value_int::numeric + 1::numeric),4) exponent,
                           dprv_value_int::numeric periods,
                           pd_fsem_date,
                           pd_fsem_value_close,
                           pd_fsem_value_open,
                           pd_fsem_value_high,
                           pd_fsem_value_low
                      FROM ss_dat.d_exchanges
                       JOIN ss_dat.d_symbols           ON dsym_dexh_id = dexh_id
                       JOIN ss_dat.d_symbol_timeframes ON dsyt_dsym_id = dsym_id
                       JOIN ss_dat.d_timeframes        ON dsyt_dtfr_id = dtfr_id
                       CROSS
                       JOIN ss_dat.d_studies
                       JOIN ss_dat.d_study_parameters ON dstp_dstd_id = dstd_id
                       JOIN ss_dat.d_parameter_types  ON dstp_dprt_id = dprt_id
                       JOIN ss_dat.d_parameter_values ON dprv_dprt_id = dprt_id
                       LEFT
                       JOIN prevData ON pd_dsym_id        = dsym_id AND
                                        pd_dtfr_id        = dtfr_id AND
                                        pd_period_dprv_id = dprv_id
                    WHERE dstd_code = 'EMA'
                      AND dprt_code = 'PERIOD') LOOP
    l_firstEMA_o := 0;
    l_firstEMA_h := 0;
    l_firstEMA_l := 0;
    l_firstEMA_c := 0;

    r_periods.pd_fsem_date := COALESCE(r_periods.pd_fsem_date,timestamp '-infinity');
  
    FOR r_rec IN (SELECT fdat_unix_ms,
                         fdat_date,
                         fdat_close,
                         fdat_open,
                         fdat_high,
                         fdat_low,
                         fdat_ddat_id,
                         ROW_NUMBER() OVER (ORDER BY fdat_date) rownum
                    FROM ss_dat.f_data
                   WHERE fdat_dsym_id = r_periods.dsym_id
                     AND fdat_ddat_id IN (SELECT ddat_id FROM ss_dat.d_dates WHERE ddat_date BETWEEN r_periods.pd_fsem_date::date AND now()::date)
                     AND fdat_dtfr_id = r_periods.dtfr_id
                     AND fdat_date    > r_periods.pd_fsem_date
                   ORDER BY fdat_date) LOOP
      IF r_periods.pd_fsem_value_close IS NULL THEN
        IF r_rec.rownum <= r_periods.periods THEN
          l_firstEMA_c := l_firstEMA_c + r_rec.fdat_close;
          l_val_c      := r_rec.fdat_close;
        ELSIF r_rec.rownum = r_periods.periods + 1 THEN
          l_val_c := ROUND(l_firstEMA_c / r_periods.periods, r_periods.dsym_decimals);
        ELSE
          l_val_c := ROUND(r_rec.fdat_close * r_periods.exponent + l_prevVal_c * (1::numeric - r_periods.exponent), r_periods.dsym_decimals);
        END IF;
      ELSE
        IF r_rec.rownum = 1 THEN
          l_prevVal_c := r_periods.pd_fsem_value_close;
        END IF;
  
        l_val_c := ROUND(r_rec.fdat_close * r_periods.exponent + l_prevVal_c * (1::numeric - r_periods.exponent), r_periods.dsym_decimals);
      END IF;

      l_prevVal_c := l_val_c;
      
      IF r_periods.pd_fsem_value_open IS NULL THEN
        IF r_rec.rownum <= r_periods.periods THEN
          l_firstEMA_o := l_firstEMA_o + r_rec.fdat_open;
          l_val_o      := r_rec.fdat_open;
        ELSIF r_rec.rownum = r_periods.periods + 1 THEN
          l_val_o := ROUND(l_firstEMA_o / r_periods.periods, r_periods.dsym_decimals);
        ELSE
          l_val_o := ROUND(r_rec.fdat_open * r_periods.exponent + l_prevVal_o * (1::numeric - r_periods.exponent), r_periods.dsym_decimals);
        END IF;
      ELSE
        IF r_rec.rownum = 1 THEN
          l_prevVal_o := r_periods.pd_fsem_value_open;
        END IF;
  
        l_val_o := ROUND(r_rec.fdat_open * r_periods.exponent + l_prevVal_o * (1::numeric - r_periods.exponent), r_periods.dsym_decimals);
      END IF;

      l_prevVal_o := l_val_o;
      
      IF r_periods.pd_fsem_value_high IS NULL THEN
        IF r_rec.rownum <= r_periods.periods THEN
          l_firstEMA_h := l_firstEMA_h + r_rec.fdat_high;
          l_val_h      := r_rec.fdat_high;
        ELSIF r_rec.rownum = r_periods.periods + 1 THEN
          l_val_h := ROUND(l_firstEMA_h / r_periods.periods, r_periods.dsym_decimals);
        ELSE
          l_val_h := ROUND(r_rec.fdat_high * r_periods.exponent + l_prevVal_h * (1::numeric - r_periods.exponent), r_periods.dsym_decimals);
        END IF;
      ELSE
        IF r_rec.rownum = 1 THEN
          l_prevVal_h := r_periods.pd_fsem_value_high;
        END IF;
  
        l_val_h := ROUND(r_rec.fdat_high * r_periods.exponent + l_prevVal_h * (1::numeric - r_periods.exponent), r_periods.dsym_decimals);
      END IF;

      l_prevVal_h := l_val_h;
      
      IF r_periods.pd_fsem_value_low IS NULL THEN
        IF r_rec.rownum <= r_periods.periods THEN
          l_firstEMA_l := l_firstEMA_l + r_rec.fdat_low;
          l_val_l      := r_rec.fdat_low;
        ELSIF r_rec.rownum = r_periods.periods + 1 THEN
          l_val_l := ROUND(l_firstEMA_l / r_periods.periods, r_periods.dsym_decimals);
        ELSE
          l_val_l := ROUND(r_rec.fdat_low * r_periods.exponent + l_prevVal_l * (1::numeric - r_periods.exponent), r_periods.dsym_decimals);
        END IF;
      ELSE
        IF r_rec.rownum = 1 THEN
          l_prevVal_l := r_periods.pd_fsem_value_low;
        END IF;
  
        l_val_l := ROUND(r_rec.fdat_low * r_periods.exponent + l_prevVal_l * (1::numeric - r_periods.exponent), r_periods.dsym_decimals);
      END IF;

      l_prevVal_l := l_val_l;
  
      INSERT INTO ss_dat.f_study_EMA (fsem_id,
                                      fsem_dsym_id,
                                      fsem_ddat_id,
                                      fsem_dtfr_id,
                                      fsem_period_dprv_id,
                                      fsem_date,
                                      fsem_value_close,
                                      fsem_value_open,
                                      fsem_value_high,
                                      fsem_value_low,
                                      fsem_unix_ms,
                                      fsem_data_date,
                                      fsem_json_close,
                                      fsem_json_high,
                                      fsem_json_low,
                                      fsem_json_open)
                               SELECT NEXTVAL('ss_dat.fsem_seq')::integer,
                                      r_periods.dsym_id,
                                      r_rec.fdat_ddat_id,
                                      r_periods.dtfr_id,
                                      r_periods.dprv_id,
                                      r_rec.fdat_date,
                                      l_val_c,
                                      l_val_o,
                                      l_val_h,
                                      l_val_l,
                                      r_rec.fdat_unix_ms,
                                      now(),
                                      ROW_TO_JSON(c),
                                      ROW_TO_JSON(h),
                                      ROW_TO_JSON(l),
                                      ROW_TO_JSON(o)
                                 FROM (SELECT TO_CHAR(r_rec.fdat_date,'DD-MM-YYYY HH24:MI') "date", l_val_c "value") c,
                                      (SELECT TO_CHAR(r_rec.fdat_date,'DD-MM-YYYY HH24:MI') "date", l_val_h "value") h,
                                      (SELECT TO_CHAR(r_rec.fdat_date,'DD-MM-YYYY HH24:MI') "date", l_val_l "value") l,
                                      (SELECT TO_CHAR(r_rec.fdat_date,'DD-MM-YYYY HH24:MI') "date", l_val_o "value") o;
    END LOOP;
  END LOOP;

  RETURN 'Success';
END;
$BODY$
  LANGUAGE plpgsql;
