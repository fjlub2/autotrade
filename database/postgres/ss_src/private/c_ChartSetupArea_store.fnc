CREATE OR REPLACE FUNCTION ss_src."c_ChartSetupArea_store"(p_uctId      ss_dat.user_charts.uct_id%TYPE,
                                                           p_from       ss_dat.user_charts.uct_area_from%TYPE,
                                                           p_to         ss_dat.user_charts.uct_area_to%TYPE,
                                                           p_colour     ss_dat.user_charts.uct_area_colour%TYPE) RETURNS void AS
$BODY$
DECLARE
  l_ustRow         ss_dat.user_strategies%ROWTYPE;
  l_uctRow         ss_dat.user_charts%ROWTYPE;
  l_updateUctRow   ss_tapi."t_UserCharts_updateRow";
  l_includeUctRow  ss_tapi."t_UserCharts_updateInclude";
  
  l_taskId         INTEGER;
  l_uctChanges     INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  IF p_uctId IS NOT NULL THEN
    l_uctRow := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);
    l_ustRow := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := l_uctRow.uct_ust_id);
    
    IF l_uctRow.uct_id     IS NULL OR
       l_ustRow.ust_usr_id IS NULL OR
       l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF (p_from IS NULL AND p_to IS NOT NULL) OR (isfinite(p_from) AND NOT isfinite(p_to)) THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','From Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF (p_from IS NOT NULL AND p_to IS NULL) OR (NOT isfinite(p_from) AND isfinite(p_to)) THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','To Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF COALESCE(p_colour,'') != '' THEN
    IF LENGTH(p_colour) != 7 OR
       p_colour NOT LIKE '#%' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                              p_vars   := core_src."c_Data_addTag"('FIELD','colour'),
                                              p_data   := core_src."c_Data_addTag"('COLOUR',p_colour),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    IF p_from IS NOT NULL AND p_to IS NOT NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                              p_vars   := core_src."c_Data_addTag"('FIELD','Colour'),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN -- don't want to continue if there were any errors with the input parameters.
    RETURN;
  END IF;

  l_uctChanges := 0;

  IF isfinite(COALESCE(p_from,now())) THEN
    IF (l_uctRow.uct_area_from IS NULL AND p_from IS NOT NULL) OR 
       (l_uctRow.uct_area_from IS NOT NULL AND p_from IS NULL) OR
       (l_uctRow.uct_area_from IS NOT NULL AND p_from IS NOT NULL AND l_uctRow.uct_area_from != p_from) THEN
      l_updateUctRow.uct_area_from  := p_from;
      l_includeUctRow.uct_area_from := 'Y';
      l_uctChanges                  := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_area_from),
                                                                                        'NEW',p_from),
                                                                                    'FIELD','UCT_AREA_FROM'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
  END IF;
    
  IF isfinite(COALESCE(p_to,now())) THEN
    IF (l_uctRow.uct_area_to IS NULL AND p_to IS NOT NULL) OR 
       (l_uctRow.uct_area_to IS NOT NULL AND p_to IS NULL) OR
       (l_uctRow.uct_area_to IS NOT NULL AND p_to IS NOT NULL AND l_uctRow.uct_area_to != p_to) THEN
      l_updateUctRow.uct_area_to  := p_to;
      l_includeUctRow.uct_area_to := 'Y';
      l_uctChanges                := 1;
        
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_area_to),
                                                                                        'NEW',p_to),
                                                                                    'FIELD','UCT_AREA_TO'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
  END IF;
    
  IF (l_uctRow.uct_area_colour IS NULL AND p_colour IS NOT NULL) OR 
     (l_uctRow.uct_area_colour IS NOT NULL AND p_colour IS NULL) OR
     (l_uctRow.uct_area_colour IS NOT NULL AND p_colour IS NOT NULL AND l_uctRow.uct_area_colour != p_colour) THEN
    l_updateUctRow.uct_area_colour  := p_colour;
    l_includeUctRow.uct_area_colour := 'Y';
    l_uctChanges                    := 1;
      
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                            p_data    := core_src."c_Data_addTag"(
                                                             core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                          'OLD',l_uctRow.uct_area_colour),
                                                                                      'NEW',p_colour),
                                                                                  'FIELD','UCT_AREA_COLOUR'),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  IF l_uctChanges = 1 THEN
    PERFORM ss_tapi."t_UserCharts_updateRow"(p_pk := l_uctRow.uct_id, p_row := l_updateUctRow, p_include := l_includeUctRow);
                                              
    PERFORM ss_tapi."t_UserStrategies_setUpdated"(p_pk := l_uctRow.uct_ust_id, p_val := localtimestamp);
      
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Updated chart setup area.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  ELSE
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Request made to update chart setup area, but no changes identified',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);    
  END IF;
                 
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;