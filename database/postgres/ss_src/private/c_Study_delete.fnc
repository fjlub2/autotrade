CREATE OR REPLACE FUNCTION ss_src."c_Study_delete"(p_ucsId ss_dat.user_chart_studies.ucs_id%TYPE) RETURNS void AS
$BODY$
DECLARE
  l_usrId   core_dat.users.usr_id%TYPE;

  l_taskId INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  IF p_ucsId IS NOT NULL THEN
    l_usrId := ss_tapi."t_UserStrategies_getUsrId_PK"(p_pk := ss_tapi."t_UserCharts_getUstId_PK"(p_pk := ss_tapi."t_UserChartStudies_getUctId_PK"(p_pk := p_ucsId)));
    
    IF l_usrId IS NULL OR
       l_usrId != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','study'),
                                              p_data   := core_src."c_Data_addTag"('UCS_ID',p_ucsId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;  
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'N' THEN
    DELETE
      FROM ss_dat.action_items
     WHERE ai_ucs_id_left = p_ucsId;
     
    DELETE
      FROM ss_dat.action_items
     WHERE ai_ucs_id_right1 = p_ucsId;
     
    DELETE
      FROM ss_dat.action_items
     WHERE ai_ucs_id_right2 = p_ucsId;
     
    DELETE 
      FROM ss_dat.user_chart_study_params
     WHERE usp_ucs_id = p_ucsId;

    PERFORM ss_tapi."t_UserChartStudies_deleteRow"(p_pk := p_ucsId);
  END IF;
  
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;