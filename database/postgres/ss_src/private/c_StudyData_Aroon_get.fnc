CREATE OR REPLACE FUNCTION ss_src."c_StudyData_Aroon_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE,
                                                          p_dsymId ss_dat.d_symbols.dsym_id%TYPE,
                                                          p_dtfrId ss_dat.d_timeframes.dtfr_id%TYPE,
                                                          p_from   ss_dat.user_charts.uct_from%TYPE,
                                                          p_to     ss_dat.user_charts.uct_to%TYPE)
  RETURNS ss_dat.t_studyVals_return AS
$BODY$
DECLARE
  l_preFrom   ss_dat.f_data.fdat_date%TYPE;
  l_json      JSON;
  l_guides    JSON;
  l_taskId    INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Aroon Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
                                          
  IF p_ucsId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
                                          
  IF p_dsymId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_dtfrId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Time Frame'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_from IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','From Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_to IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','To Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := NULL;
  ELSE
    SELECT MIN(fdat_date)
      INTO l_preFrom
      FROM (SELECT fdat_date
              FROM ss_dat.f_data
             WHERE fdat_dsym_id = p_dsymId
               AND fdat_dtfr_id = p_dtfrId
               AND fdat_date < p_from
             ORDER BY fdat_date DESC
             LIMIT 25) a;

    l_preFrom := COALESCE(l_preFrom,p_from);

    WITH
      chartData AS (SELECT ROW_NUMBER() OVER w1 rownum,
                           fdat_date,
                           fdat_unix_ms,
                           fdat_high,
                           fdat_low,
                           MAX(fdat_high) OVER w2 maxHigh,
                           MIN(fdat_low) OVER w2 minLow
                     FROM ss_dat.f_data
             WHERE fdat_dsym_id = p_dsymId
               AND fdat_date BETWEEN l_preFrom AND p_to
               AND fdat_dtfr_id = p_dtfrId
            WINDOW w1 AS (ORDER BY fdat_date),
                   w2 AS (ORDER BY fdat_date ROWS BETWEEN 25 PRECEDING AND CURRENT ROW)),
      highWorkings AS (SELECT a.rownum - b.rownum highDiff,
                              a.fdat_date theDate,
                              a.fdat_unix_ms highDate,
                              ROW_NUMBER() OVER w1 highKeep
                         FROM chartData a
                           JOIN chartData b ON b.rownum BETWEEN a.rownum - 25 AND a.rownum AND b.fdat_high = a.maxHigh
                       WINDOW w1 AS (PARTITION BY a.rownum ORDER BY b.rownum DESC)),
      lowWorkings AS (SELECT a.rownum - b.rownum lowDiff,
                             a.fdat_unix_ms lowDate,
                             ROW_NUMBER() OVER w1 lowKeep
                        FROM chartData a
                          JOIN chartData b ON b.rownum BETWEEN a.rownum - 25 AND a.rownum AND b.fdat_low = a.minLow
                      WINDOW w1 AS (PARTITION BY a.rownum ORDER BY b.rownum DESC))
    SELECT JSON_AGG(ROW_TO_JSON(c))
      INTO l_json  
      FROM (SELECT chv_code       "code",
                   chv_name       "name",
                   cht_chart_type "overrideChartType",
                   (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a)))
                      FROM (SELECT highDate "d",
                                   (100 * (25 - highDiff) / 25) - (100 * (25 - lowDiff) / 25) "v"
                              FROM highWorkings JOIN lowWorkings ON highDate = lowDate
                             WHERE lowKeep = 1 AND highKeep = 1
                               AND theDate BETWEEN p_from AND p_to
                             ORDER BY highDate) a) "values"
              FROM ss_dat.chart_values
                LEFT JOIN ss_dat.chart_types ON chv_cht_id = cht_id
             WHERE chv_dstd_id = (SELECT ucs_dstd_id FROM ss_dat.user_chart_studies WHERE ucs_id = p_ucsId)
               AND chv_code = 'AROON') c;
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Aroon Data.',
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  l_guides := '[{"v":50,"l":"Strong Bullish Trend"},{"v":-50,"l":"Strong Bearish Trend"}]'::json;
  
  RETURN (l_json,l_guides,NULL)::ss_dat.t_studyVals_return;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;
