CREATE OR REPLACE FUNCTION ss_src."c_Study_store"(p_uctId     ss_dat.user_charts.uct_id%TYPE,
                                                  p_ucsId     ss_dat.user_chart_studies.ucs_id%TYPE,
                                                  p_studyCode ss_dat.d_studies.dstd_code%TYPE,
                                                  p_chartType ss_dat.chart_types.cht_code%TYPE,
                                                  p_panel     ss_dat.user_chart_studies.ucs_chart_panel%TYPE,
                                                  p_colour    ss_dat.user_chart_studies.ucs_colour%TYPE,
                                                  p_params    JSON,
                                                  p_name      ss_dat.user_chart_studies.ucs_name%TYPE) RETURNS ss_dat.user_chart_studies.ucs_id%TYPE AS
$BODY$
DECLARE
  l_uctId          ss_dat.user_charts.uct_id%TYPE;
  l_chtId          ss_dat.chart_types.cht_id%TYPE;
  l_ucsId          ss_dat.user_chart_studies.ucs_id%TYPE;
  l_uspId          ss_dat.user_chart_study_params.usp_id%TYPE;
  
  l_cnt            INTEGER;
  
  l_ucsRow         ss_dat.user_chart_studies%ROWTYPE;
  l_uctRow         ss_dat.user_charts%ROWTYPE;
  l_uspRow         ss_dat.user_chart_study_params%ROWTYPE;
  l_dstdRow        ss_dat.d_studies%ROWTYPE;
  l_updateUcsRow   ss_tapi."t_UserChartStudies_updateRow";
  l_updateUspRow   ss_tapi."t_UserChartStudyParams_updateRow";
  l_includeUcsRow  ss_tapi."t_UserChartStudies_updateInclude";
  l_includeUspRow  ss_tapi."t_UserChartStudyParams_updateInclude";
  
  l_dprvId         ss_dat.d_parameter_values.dprv_id%TYPE;
  
  r_params         RECORD;
  l_param_text     TEXT;
  l_param_int      INTEGER;
  
  l_taskId         INTEGER;
  l_changes        INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  IF p_ucsId IS NOT NULL THEN
    l_ucsRow := ss_tapi."t_UserChartStudies_getRow_PK"(p_pk := p_ucsId);
    
    IF l_ucsRow.ucs_id IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','user study'),
                                              p_data   := core_src."c_Data_addTag"('UCS_ID',p_ucsId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);

      RETURN NULL;
    END IF;
  END IF;
   
  IF COALESCE(p_studyCode,'') != '' THEN
    l_dstdRow := ss_tapi."t_DStudies_getRow_UK"(p_uk := p_studyCode);
    
    IF l_dstdRow.dstd_id IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','study'),
                                              p_data   := core_src."c_Data_addTag"('DSTD_CODE',p_studyCode),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    ELSIF l_ucsRow.ucs_id IS NOT NULL AND l_ucsRow.ucs_dstd_id != l_dstdRow.dstd_id THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option, study id can't be updated
                                              p_vars   := core_src."c_Data_addTag"('FIELD','study'),
                                              p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('DSTD_ID',l_dstdRow.dstd_id),'UCS_ID',l_ucsRow.ucs_id),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    ELSE
      IF p_uctId IS NOT NULL THEN
        l_uctRow := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);
    
        IF l_uctRow.uct_id IS NULL THEN
          PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                                  p_vars   := core_src."c_Data_addTag"('FIELD','user chart'),
                                                  p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                                  p_type   := 'E',
                                                  p_taskId := l_taskId);
        ELSIF ss_tapi."t_UserStrategies_getUsrId_PK"(p_pk := l_uctRow.uct_ust_id) != core_src."c_Session_getCurrentUsrId"() OR
              l_ucsRow.ucs_uct_id != l_uctRow.uct_id THEN
          PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                                  p_vars   := core_src."c_Data_addTag"('FIELD','user chart'),
                                                  p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                                  p_type   := 'E',
                                                  p_taskId := l_taskId);
        ELSE          
          IF COALESCE(p_chartType,'') != '' THEN
            l_chtId := ss_tapi."t_ChartTypes_getId_UK"(p_uk := p_chartType);
    
            IF l_chtId IS NULL THEN
              PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                                      p_vars   := core_src."c_Data_addTag"('FIELD','chart type'),
                                                      p_data   := core_src."c_Data_addTag"('CHT_CODE',p_chartType),
                                                      p_type   := 'E',
                                                      p_taskId := l_taskId);
            ELSIF ss_tapi."t_StudyChartTypes_exists_UK" (p_uk  := l_dstdRow.dstd_id,
                                                         p_uk2 := l_chtId) = 'N' THEN
              PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                                      p_vars   := core_src."c_Data_addTag"('FIELD','chart type'),
                                                      p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('DSTD_ID',l_dstdRow.dstd_id),'CHT_ID',l_chtId),
                                                      p_type   := 'E',
                                                      p_taskId := l_taskId);
            END IF;
          ELSE
            PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                                    p_vars   := core_src."c_Data_addTag"('FIELD','Chart Type'),
                                                    p_type   := 'E',
                                                    p_taskId := l_taskId);
          END IF;
          
          IF COALESCE(p_colour,'') != '' THEN
            IF LENGTH(p_colour) != 7 AND
               p_colour NOT LIKE '#%' THEN
              PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                                      p_vars   := core_src."c_Data_addTag"('FIELD','colour'),
                                                      p_data   := core_src."c_Data_addTag"('UCS_COLOUR',p_colour),
                                                      p_type   := 'E',
                                                      p_taskId := l_taskId);
            END IF;
          ELSE
            PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                                    p_vars   := core_src."c_Data_addTag"('FIELD','Colour'),
                                                    p_type   := 'E',
                                                    p_taskId := l_taskId);
          END IF;
  
          IF COALESCE(p_panel,'') != '' THEN
            IF core_tapi."t_CodeValues_exists_PK"(p_pk  := 'chart_panels',
                                                  p_pk2 := p_panel) = 'N' THEN
              PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                                      p_vars   := core_src."c_Data_addTag"('FIELD','chart panel'),
                                                      p_data   := core_src."c_Data_addTag"('PANEL',p_panel),
                                                      p_type   := 'E',
                                                      p_taskId := l_taskId);
            ELSIF ss_tapi."t_StudyChartPanels_exists_UK" (p_uk  := l_dstdRow.dstd_id,
                                                          p_uk2 := p_panel) = 'N' THEN
              PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                                      p_vars   := core_src."c_Data_addTag"('FIELD','chart panel'),
                                                      p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('DSTD_ID',l_dstdRow.dstd_id),'PANEL',p_panel),
                                                      p_type   := 'E',
                                                      p_taskId := l_taskId);
            END IF;
          ELSE
            PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                                    p_vars   := core_src."c_Data_addTag"('FIELD','Panel'),
                                                    p_type   := 'E',
                                                    p_taskId := l_taskId);
          END IF;
        END IF;
      ELSE
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                                p_vars   := core_src."c_Data_addTag"('FIELD','User Chart'),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
      END IF;
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  -- check parameters have been supplied
  FOR r_params IN (SELECT dprt_id,
                          dprt_code,
                          dprt_datatype,
                          dprt_name,
                          dprt_freetext,
                          dstp_check_unique
                     FROM ss_dat.d_study_parameters
                       JOIN ss_dat.d_parameter_types ON dprt_id = dstp_dprt_id
                    WHERE dstp_dstd_id = l_dstdRow.dstd_id) LOOP
    BEGIN
      SELECT json_extract_path_text(value,'valueText'),
             json_extract_path_text(value,'valueInt')::integer
        INTO l_param_text,
             l_param_int
        FROM json_array_elements(p_params)
       WHERE json_extract_path_text(value,'code') = r_params.dprt_code;
       
      IF (r_params.dprt_datatype = 'integer' AND
          l_param_int IS NULL) OR 
         (r_params.dprt_datatype = 'text' AND
          l_param_text IS NULL) THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                                p_vars   := core_src."c_Data_addTag"('FIELD',r_params.dprt_name),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
      ELSE
        IF r_params.dprt_freetext = 'N' THEN
          SELECT dprv_id
            INTO l_dprvId
            FROM ss_dat.d_parameter_values
           WHERE dprv_dprt_id   = r_params.dprt_id
             AND ((r_params.dprt_datatype = 'integer' AND 
                   dprv_value_int = l_param_int) OR
                  (r_params.dprt_datatype = 'text' AND 
                   dprv_value_text = l_param_text));
               
          IF l_dprvId IS NULL THEN
            PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                                    p_vars   := core_src."c_Data_addTag"('FIELD',r_params.dprt_name),
                                                    p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"(core_src."c_Data_addTag"('DPRV_VALUE_INT',l_param_int),'DPRV_VALUE_TEXT',l_param_text),'DPRT_ID',r_params.dprt_id),
                                                    p_type   := 'E',
                                                    p_taskId := l_taskId);
          END IF;
        END IF;
      END IF;
      
      IF l_ucsRow.ucs_id IS NULL AND
         l_dstdRow.dstd_multi_add = 'Y' AND
         r_params.dstp_check_unique = 'Y' THEN
        SELECT COUNT(1)
          INTO l_cnt
          FROM ss_dat.user_chart_studies
            JOIN ss_dat.user_chart_study_params ON usp_ucs_id = ucs_id
         WHERE ucs_uct_id  = p_uctId
           AND ucs_dstd_id = l_dstdRow.dstd_id
           AND usp_dprt_id = r_params.dprt_id
           AND ((r_params.dprt_datatype = 'integer' AND 
                 usp_value_int = l_param_int) OR
                (r_params.dprt_datatype = 'text' AND 
                 usp_value_text = l_param_text));
               
        IF l_cnt = 1 THEN
          PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0011', -- duplicate
                                                  p_vars   := core_src."c_Data_addTag"('FIELD','study'),
                                                  p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"(core_src."c_Data_addTag"(core_src."c_Data_addTag"(core_src."c_Data_addTag"('DSTD_ID',l_dstdRow.dstd_id),'UCT_ID',p_uctId),'DPRV_VALUE_INT',l_param_int),'DPRV_VALUE_TEXT',l_param_text),'DPRT_ID',r_params.dprt_id),
                                                  p_type   := 'E',
                                                  p_taskId := l_taskId);
        END IF;
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0003', -- body not valid
                                                p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('PARAMS',p_params::text),'DPRT_CODE',r_params.dprt_code),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
    END;
  END LOOP;
  
  IF COALESCE(p_name,'') = '' THEN
    IF LENGTH(p_name) > 30 THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0022', -- too long
                                              p_vars   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('FIELD','name'),'LENGTH','30'),
                                              p_data   := core_src."c_Data_addTag"('UCS_NAME',p_name),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END IF;
  
  IF l_ucsRow.ucs_id IS NULL AND
     l_dstdRow.dstd_multi_add = 'N' THEN -- check for duplicates
    SELECT COUNT(1)
      INTO l_cnt
      FROM ss_dat.user_chart_studies
     WHERE ucs_uct_id  = p_uctId
       AND ucs_dstd_id = l_dstdRow.dstd_id;
        
    IF l_cnt != 0 THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0011', -- duplicate
                                              p_vars   := core_src."c_Data_addTag"('FIELD','study'),
                                              p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('DSTD_ID',l_dstdRow.dstd_id),'UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN -- don't want to continue if there were any errors with the input parameters.
    RETURN NULL;
  END IF;
  
  IF l_ucsRow.ucs_id IS NULL THEN
    l_ucsRow.ucs_uct_id      := p_uctId;
    l_ucsRow.ucs_dstd_id     := l_dstdRow.dstd_id;
    l_ucsRow.ucs_cht_id      := l_chtId;
    l_ucsRow.ucs_chart_panel := p_panel;
    l_ucsRow.ucs_colour      := p_colour;
    l_ucsRow.ucs_name        := p_name;
    l_ucsRow.ucs_order       := 1;
  
    l_ucsId := ss_tapi."t_UserChartStudies_insertRow" (p_row := l_ucsRow);
        
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Stored new study.',
                                            p_data    := core_src."c_Data_addTag"('UCS_ID',l_ucsId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
    
    FOR r_params IN (SELECT dprt_code,
                            dprt_datatype,
                            dprt_id
                       FROM ss_dat.d_study_parameters
                         JOIN ss_dat.d_parameter_types ON dprt_id = dstp_dprt_id
                      WHERE dstp_dstd_id = l_dstdRow.dstd_id) LOOP
      l_uspRow.usp_ucs_id  := l_ucsId;
      l_uspRow.usp_dprt_id := r_params.dprt_id;

      SELECT json_extract_path_text(value,'valueText'),
             json_extract_path_text(value,'valueInt')
        INTO l_param_text,
             l_param_int
        FROM json_array_elements(p_params)
       WHERE json_extract_path_text(value,'code') = r_params.dprt_code;
       
      IF r_params.dprt_datatype = 'integer' THEN
        l_uspRow.usp_value_int  := l_param_int;
        l_uspRow.usp_value_text := NULL;
      ELSIF r_params.dprt_datatype = 'text' THEN
        l_uspRow.usp_value_int  := NULL;
        l_uspRow.usp_value_text := l_param_text;
      END IF;
  
      l_uspId := ss_tapi."t_UserChartStudyParams_insertRow" (p_row := l_uspRow);
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Stored new study param.',
                                              p_data    := core_src."c_Data_addTag"('USP_ID',l_uspId),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END LOOP;
    
    l_changes := 1;
    
    PERFORM ss_tapi."t_UserStrategies_setUpdated"(p_pk := l_uctRow.uct_ust_id, p_val := localtimestamp);
  ELSE
    l_changes := 0;

    -- not allowed to update ucs_uct_id or ucs_dstd_id

    -- will deal with changes to ucs_order later

    IF l_ucsRow.ucs_cht_id != l_chtId THEN
      l_updateUcsRow.ucs_cht_id  := l_chtId;
      l_includeUcsRow.ucs_cht_id := 'Y';
      l_changes                  := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing study value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCS_ID',l_ucsRow.ucs_id),
                                                                                            'OLD',l_ucsRow.ucs_cht_id),
                                                                                        'NEW',l_chtId),
                                                                                    'FIELD','UCS_CHT_ID'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
    
    IF l_ucsRow.ucs_chart_panel != p_panel THEN
      l_updateUcsRow.ucs_chart_panel  := p_panel;
      l_includeUcsRow.ucs_chart_panel := 'Y';
      l_changes                       := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing study value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCS_ID',l_ucsRow.ucs_id),
                                                                                            'OLD',l_ucsRow.ucs_chart_panel),
                                                                                        'NEW',p_panel),
                                                                                    'FIELD','UCS_CHART_PANEL'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF; 
    
    IF l_ucsRow.ucs_colour != p_colour THEN
      l_updateUcsRow.ucs_colour  := p_colour;
      l_includeUcsRow.ucs_colour := 'Y';
      l_changes                  := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing study value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCS_ID',l_ucsRow.ucs_id),
                                                                                            'OLD',l_ucsRow.ucs_colour),
                                                                                        'NEW',p_colour),
                                                                                    'FIELD','UCS_COLOUR'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
    
    IF COALESCE(l_ucsRow.ucs_name,'') != COALESCE(p_name,'') THEN
      l_updateUcsRow.ucs_name  := p_name;
      l_includeUcsRow.ucs_name := 'Y';
      l_changes                := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing study value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCS_ID',l_ucsRow.ucs_id),
                                                                                            'OLD',l_ucsRow.ucs_name),
                                                                                        'NEW',p_name),
                                                                                    'FIELD','UCS_NAME'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF; 

    IF l_changes = 1 THEN
      PERFORM ss_tapi."t_UserChartStudies_updateRow"(p_pk := l_ucsRow.ucs_id, p_row := l_updateUcsRow, p_include := l_includeUcsRow);
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Updated study.',
                                              p_data    := core_src."c_Data_addTag"('UCS_ID',l_ucsRow.ucs_id),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
        
    FOR r_params IN (SELECT dprt_code,
                            dprt_datatype,
                            usp_id,
                            usp_value_int,
                            usp_value_text
                       FROM ss_dat.user_chart_study_params
                         JOIN ss_dat.d_parameter_types ON dprt_id = usp_dprt_id
                      WHERE usp_ucs_id = l_ucsRow.ucs_id) LOOP
      SELECT json_extract_path_text(value,'valueText'),
             json_extract_path_text(value,'valueInt')
        INTO l_param_text,
             l_param_int
        FROM json_array_elements(p_params)
       WHERE json_extract_path_text(value,'code') = r_params.dprt_code;
       
      IF r_params.dprt_datatype  = 'integer'   AND
         r_params.usp_value_int != l_param_int THEN
        l_updateUspRow.usp_value_int   := l_param_int;
        l_updateUspRow.usp_value_text  := NULL;
        l_includeUspRow.usp_value_int  := 'Y';
        l_includeUspRow.usp_value_text := 'Y';
        l_changes                      := 2;
        
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing study value.',
                                                p_data    := core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"(
                                                                         core_src."c_Data_addTag"('USP_ID',r_params.usp_id),
                                                                                              'OLD',r_params.usp_value_int),
                                                                                          'NEW',l_param_int),
                                                                                      'FIELD','USP_VALUE_INT'),
                                                p_type    := 'I',
                                                p_taskId  := l_taskId);
      ELSIF r_params.dprt_datatype   = 'text'       AND
            r_params.usp_value_text != l_param_text THEN
        l_updateUspRow.usp_value_text  := l_param_text;
        l_updateUspRow.usp_value_int   := NULL;
        l_includeUspRow.usp_value_int  := 'Y';
        l_includeUspRow.usp_value_text := 'Y';
        l_changes                      := 2;
        
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing study value.',
                                                p_data    := core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"(
                                                                         core_src."c_Data_addTag"('USP_ID',r_params.usp_id),
                                                                                              'OLD',r_params.usp_value_text),
                                                                                          'NEW',l_param_text),
                                                                                      'FIELD','USP_VALUE_TEXT'),
                                                p_type    := 'I',
                                                p_taskId  := l_taskId);
      END IF;
  
      IF l_changes = 2 THEN
        PERFORM ss_tapi."t_UserChartStudyParams_updateRow"(p_pk := r_params.usp_id, p_row := l_updateUspRow, p_include := l_includeUspRow);

        PERFORM core_src."c_Message_logMsgText"(p_message := 'Updated study param.',
                                                p_data    := core_src."c_Data_addTag"('USP_ID',r_params.usp_id),
                                                p_type    := 'I',
                                                p_taskId  := l_taskId);
        l_changes := 1;
      END IF;
    END LOOP;
    
    IF l_changes = 0 THEN
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Request made to update study options, but no changes identified',
                                              p_data    := core_src."c_Data_addTag"('UCS_ID',l_ucsRow.ucs_id),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    ELSE
      PERFORM ss_tapi."t_UserStrategies_setUpdated"(p_pk := l_uctRow.uct_ust_id, p_val := localtimestamp);
    END IF;
    
    l_ucsId := l_ucsRow.ucs_id;
  END IF;
  
  IF l_changes != 0 AND
     p_panel    = 'volume' AND
     ss_tapi."t_UserCharts_getShowVolume_PK"(p_pk := p_uctId) = 'N' THEN
    PERFORM ss_tapi."t_UserCharts_setShowVolume"  (p_pk := p_uctId, p_val := 'Y');
    
    IF l_uctRow.uct_volume_colour IS NULL THEN
      PERFORM ss_tapi."t_UserCharts_setVolumeColour"(p_pk := p_uctId, p_val := '#69BFFF');
    END IF;
     
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Updated chart to show the volume panel.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  FOR r_params IN (SELECT dprt_code,
                          dprt_datatype,
                          dprt_name
                     FROM ss_dat.d_study_parameters
                       JOIN ss_dat.d_parameter_types ON dprt_id = dstp_dprt_id
                    WHERE dstp_dstd_id = l_dstdRow.dstd_id) LOOP
    SELECT json_extract_path_text(value,'valueText'),
           json_extract_path_text(value,'valueInt')
      INTO l_param_text,
           l_param_int
      FROM json_array_elements(p_params)
     WHERE json_extract_path_text(value,'code') = r_params.dprt_code;
       
    IF (r_params.dprt_datatype = 'integer' AND
        l_param_int IS NULL) OR 
       (r_params.dprt_datatype = 'text' AND
        l_param_text IS NULL) THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                              p_vars   := core_src."c_Data_addTag"('FIELD',r_params.dprt_name),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END LOOP;
                 
  RETURN l_ucsId;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;