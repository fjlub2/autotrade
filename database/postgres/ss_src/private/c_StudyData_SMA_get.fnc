CREATE OR REPLACE FUNCTION ss_src."c_StudyData_SMA_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE,
                                                        p_dsymId ss_dat.d_symbols.dsym_id%TYPE,
                                                        p_dtfrId ss_dat.d_timeframes.dtfr_id%TYPE,
                                                        p_from   ss_dat.user_charts.uct_from%TYPE,
                                                        p_to     ss_dat.user_charts.uct_to%TYPE)
  RETURNS ss_dat.t_studyvals_return AS
$BODY$
DECLARE
  l_period       INTEGER;
  l_json_vals    JSON;
  l_json_guides  JSON;
  l_inbreak      INTEGER := 0;
  l_taskId       INTEGER;
  l_breakPeriods INTEGER;
  l_breakPct     INTEGER;
  l_breakPctOk   INTEGER;
  l_breakCount   INTEGER := 0;
  l_breakFrom    ss_dat.f_data.fdat_unix_ms%TYPE;
  l_label        TEXT;
  l_fromVal      ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_shift        INTEGER;
  l_prevVal      ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_mins         ss_dat.d_timeframes.dtfr_mins%TYPE;
  l_dsymRow      ss_dat.d_symbols%ROWTYPE;
  l_basedOn      TEXT;
  r_rec          RECORD;
  l_preFrom      ss_dat.f_data.fdat_date%TYPE;
  l_vals         ss_dat.t_studyVals[];
  l_valsGuide    ss_dat.t_studyVals_guide[];
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Study SMA Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
                                          
  IF p_ucsId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
                                          
  IF p_dsymId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_dtfrId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Time Frame'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_from IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','From Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_to IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','To Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_period := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'PERIOD');
  
  IF l_period IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('PERIOD','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_basedOn := ss_src."c_StudyParamText_get"(p_ucsId := p_ucsId, p_param := 'BASED_ON');
  
  IF l_basedOn IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('BASED_ON','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_breakPeriods := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'BREAK_PERIOD');
  
  IF l_breakPeriods IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('BREAK_PERIOD','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_breakPct := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'BREAK_PCT');
  
  IF l_breakPct IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('BREAK_PCT','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_shift := COALESCE(ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'SHIFT'),0);
  
  l_mins := ss_tapi."t_DTimeframes_getMins_PK"(p_pk := p_dtfrId);

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json_vals   := NULL;
    l_json_guides := NULL;
  ELSE
    l_dsymRow := ss_tapi."t_DSymbols_getRow_PK"(p_pk := p_dsymId);
    
    /*
    SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a)))
      INTO l_json  
      FROM (SELECT fdat_unix_ms "date",
                   ROUND(avg_val,l_decimals) "value"
              FROM (SELECT fdat_date,
                           fdat_unix_ms,
                           AVG(CASE l_basedOn WHEN 'close' THEN fdat_close
                                              WHEN 'high'  THEN fdat_high
                                              WHEN 'low'   THEN fdat_low
                                              WHEN 'open'  THEN fdat_open END) OVER w1 avg_val
                      FROM ss_dat.f_data
                     WHERE fdat_dsym_id = p_dsymId
                       AND fdat_ddat_id IN (SELECT ddat_id FROM ss_dat.d_dates WHERE ddat_date BETWEEN p_from AND p_to)
                       AND fdat_dtfr_id = p_dtfrId
                    WINDOW w1 AS (ORDER BY fdat_date ROWS BETWEEN l_period PRECEDING AND CURRENT ROW)) b
             ORDER BY fdat_date) a;*/
             
    SELECT MIN(fdat_date)
      INTO l_preFrom
      FROM (SELECT fdat_date
              FROM ss_dat.f_data
             WHERE fdat_dsym_id = p_dsymId
               AND fdat_dtfr_id = p_dtfrId
               AND fdat_date < p_from
             ORDER BY fdat_date DESC
             LIMIT l_period) a;

    l_preFrom := COALESCE(l_preFrom,p_from);
             
    FOR r_rec IN (SELECT fdat_unix_ms,
                         ROUND(LAG(CASE WHEN avgRow >= l_period THEN avg_val ELSE NULL END,l_shift) OVER w2,l_dsymRow.dsym_decimals) avg_val,
                         price,
                         ROW_NUMBER() OVER w2 - GREATEST(l_shift,0) rownum
                    FROM (SELECT fdat_date,
                                 fdat_unix_ms,
                                 AVG(CASE l_basedOn WHEN 'close' THEN fdat_close
                                                    WHEN 'high'  THEN fdat_high
                                                    WHEN 'low'   THEN fdat_low
                                                    WHEN 'open'  THEN fdat_open END) OVER w1 avg_val,
                                 CASE l_basedOn WHEN 'close' THEN fdat_close
                                                WHEN 'high'  THEN fdat_high
                                                WHEN 'low'   THEN fdat_low
                                                WHEN 'open'  THEN fdat_open END price,
                                 ROW_NUMBER() OVER w1 avgRow
                            FROM ss_dat.f_data
                           WHERE fdat_dsym_id = p_dsymId
                             AND fdat_date BETWEEN l_preFrom AND p_to
                             AND fdat_dtfr_id = p_dtfrId
                          WINDOW w1 AS (ORDER BY fdat_date ROWS BETWEEN l_period PRECEDING AND CURRENT ROW)) b
                   WHERE fdat_date BETWEEN p_from AND p_to
                  WINDOW w2 AS (ORDER BY fdat_date)
                   ORDER BY fdat_date) LOOP
      IF l_breakPeriods > 0 OR
         l_breakPct     > 0 THEN
        IF r_rec.avg_val IS NOT NULL THEN
          IF r_rec.avg_val > r_rec.price THEN 
            IF l_breakCount < 0 THEN
              l_breakCount := 1;
            
              IF l_breakPct > 0 THEN
                l_breakPctOk := 0;
              END IF;
            ELSE 
              l_breakCount := l_breakCount + 1;
            END IF;
          ELSIF r_rec.avg_val < r_rec.price THEN
            IF l_breakCount > 0 THEN
              l_breakCount := -1;
               
              IF l_breakPct > 0 THEN
                l_breakPctOk := 0;
              END IF;
            ELSE 
              l_breakCount := l_breakCount - 1;
            END IF;
          ELSE
            l_breakCount := 0;
            
            IF l_breakPct > 0 THEN
              l_breakPctOk := 0;
            END IF;
          END IF;
          
          IF l_breakPct > 0 AND l_breakPctOk = 0 THEN
            IF l_breakPct <= ABS(ROUND(((r_rec.avg_val - r_rec.price) / ((r_rec.avg_val + r_rec.price) / 2)) * 1000,4)) THEN
              l_breakPctOk := 1;
            ELSE
              l_breakPctOk := 0;
            END IF;
          ELSE
            l_breakPctOk := 1;
          END IF;
          
          IF (l_breakPeriods  =  0 OR
              (l_breakPeriods >  0 AND
               l_breakPeriods <= ABS(l_breakCount))) AND
             l_breakPctOk = 1 THEN
            IF l_inbreak = 0 THEN
              l_breakFrom := r_rec.fdat_unix_ms;
              l_fromVal   := r_rec.price;
              l_inbreak   := 1;
            END IF;
          ELSE
            IF l_breakFrom IS NOT NULL THEN
              --l_label := TO_CHAR((r_rec.price - l_fromVal) / l_dsymRow.dsym_tick_size,'FMS99999999990');
              l_label := NULL;
        
              l_valsGuide := array_append(l_valsGuide, (l_breakFrom,r_rec.fdat_unix_ms,l_label,NULL,NULL)::ss_dat.t_studyVals_guide);
            END IF;
             
            l_inbreak   := 0;              
            l_breakFrom := NULL;
            l_label     := NULL;
          END IF;
        END IF;
      END IF;
      
      IF l_shift > 0 AND 
         r_rec.rownum = 1 THEN
        FOR i IN 1..l_shift LOOP
          l_vals[i] := (l_vals[i].date,r_rec.avg_val)::ss_dat.t_studyVals;
        END LOOP;
      END IF;

      IF l_shift < 0 AND 
         r_rec.avg_val IS NULL THEN
        r_rec.avg_val := l_prevVal;
      ELSE
        l_prevVal := r_rec.avg_val;
      END IF;
          
      l_vals := array_append(l_vals, (r_rec.fdat_unix_ms,r_rec.avg_val)::ss_dat.t_studyVals);
    END LOOP;
    
    IF l_breakFrom IS NOT NULL THEN
      --l_label := TO_CHAR((r_rec.price - l_fromVal) / l_dsymRow.dsym_tick_size,'FMS99999999990');
      l_label := NULL;
      
      l_valsGuide := array_append(l_valsGuide, (l_breakFrom,r_rec.fdat_unix_ms,l_label,NULL,NULL)::ss_dat.t_studyVals_guide);
    END IF;
    
    IF l_breakPeriods > 0 OR
       l_breakPct     > 0 THEN
      l_json_guides := array_to_json(l_valsGuide);
    ELSE
      l_json_guides := NULL;
    END IF;

    SELECT JSON_AGG(ROW_TO_JSON(z))
      INTO l_json_vals  
      FROM (SELECT chv_code       "code",
                   chv_name       "name",
                   cht_chart_type "overrideChartType",
                   array_to_json(l_vals) "values"
              FROM ss_dat.chart_values
                LEFT JOIN ss_dat.chart_types ON chv_cht_id = cht_id
             WHERE chv_dstd_id = (SELECT ucs_dstd_id FROM ss_dat.user_chart_studies WHERE ucs_id = p_ucsId)
               AND chv_code = 'SMA') z;
                      
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Study SMA Data.',
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN (l_json_vals,l_json_guides,NULL)::ss_dat.t_studyVals_return;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;
