CREATE OR REPLACE FUNCTION ss_src."c_StudyData_RSI_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE,
                                                        p_dsymId ss_dat.d_symbols.dsym_id%TYPE,
                                                        p_dtfrId ss_dat.d_timeframes.dtfr_id%TYPE,
                                                        p_from   ss_dat.user_charts.uct_from%TYPE,
                                                        p_to     ss_dat.user_charts.uct_to%TYPE)
  RETURNS ss_dat.t_studyVals_return AS
$BODY$
DECLARE
  l_preFrom     ss_dat.f_data.fdat_date%TYPE;
  l_period      INTEGER;
  r_rec         RECORD;
  l_firstEMA_d  ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_val_d       ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_prevVal_d   ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_firstEMA_u  ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_val_u       ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_prevVal_u   ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_rsi         ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_dsymRow     ss_dat.d_symbols%ROWTYPE;
  l_exponent    ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_vals        ss_dat.t_studyVals[];
  l_json_vals   JSON;
  l_guides      JSON;
  l_taskId      INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting RSI Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
                                          
  IF p_ucsId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
                                          
  IF p_dsymId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_dtfrId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Time Frame'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_from IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','From Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_to IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','To Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_period := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'RSI_PERIOD');
  
  IF l_period IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('RSI_PERIOD','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json_vals   := NULL;
  ELSE
    l_dsymRow := ss_tapi."t_DSymbols_getRow_PK"(p_pk := p_dsymId);
      
    l_exponent := ROUND(2::numeric / (l_period::numeric + 1::numeric),4);
      
    SELECT MIN(fdat_date)
      INTO l_preFrom
      FROM (SELECT fdat_date
              FROM ss_dat.f_data
             WHERE fdat_dsym_id = p_dsymId
               AND fdat_dtfr_id = p_dtfrId
               AND fdat_date < p_from
             ORDER BY fdat_date DESC
             LIMIT l_period) a;

    l_preFrom := COALESCE(l_preFrom,p_from);
    
    FOR r_rec IN (SELECT fdat_unix_ms,
                         ROW_NUMBER() OVER w1 rownum,
                         fdat_date,
                         CASE WHEN LAG(fdat_close) OVER w1 < fdat_close THEN fdat_close - LAG(fdat_close) OVER w1 ELSE 0 END u,
                         CASE WHEN LAG(fdat_close) OVER w1 > fdat_close THEN LAG(fdat_close) OVER w1 - fdat_close ELSE 0 END d
                    FROM ss_dat.f_data
                   WHERE fdat_dsym_id = p_dsymId
                     AND fdat_date BETWEEN l_preFrom AND p_to
                     AND fdat_dtfr_id = p_dtfrId
                  WINDOW w1 AS (ORDER BY fdat_date)
                   ORDER BY fdat_date) LOOP
      IF r_rec.d IS NOT NULL THEN
        IF r_rec.rownum <= l_period THEN
          l_firstEMA_d := COALESCE(l_firstEMA_d,0) + r_rec.d;
          l_val_d      := r_rec.d;
        ELSIF r_rec.rownum = l_period + 1 THEN
          l_val_d := ROUND(l_firstEMA_d / l_period, l_dsymRow.dsym_decimals);
        ELSE
          l_val_d := ROUND(r_rec.d * l_exponent + l_prevVal_d * (1::numeric - l_exponent), l_dsymRow.dsym_decimals);
        END IF;
      END IF;
      
      IF r_rec.u IS NOT NULL THEN
        IF r_rec.rownum <= l_period THEN
          l_firstEMA_u := COALESCE(l_firstEMA_u,0) + r_rec.u;
          l_val_u      := r_rec.u;
        ELSIF r_rec.rownum = l_period + 1 THEN
          l_val_u := ROUND(l_firstEMA_u / l_period, l_dsymRow.dsym_decimals);
        ELSE
          l_val_u := ROUND(r_rec.u * l_exponent + l_prevVal_u * (1::numeric - l_exponent), l_dsymRow.dsym_decimals);
        END IF;
      END IF;
        
      IF r_rec.rownum < l_period THEN
        l_val_d := r_rec.d;
        l_val_u := r_rec.u;
      END IF;

      IF l_val_d != 0 THEN
        l_rsi := ROUND(100 - (100 / (1 + (l_val_u / l_val_d))));
      ELSE
        l_rsi := 100;
      END IF;

      IF r_rec.fdat_date >= p_from THEN
        l_vals := array_append(l_vals, (r_rec.fdat_unix_ms,l_rsi)::ss_dat.t_studyVals);
      END IF;

      l_prevVal_d := l_val_d;
      l_prevVal_u := l_val_u;
    END LOOP;
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning RSI Data.',
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  SELECT JSON_AGG(ROW_TO_JSON(z))
    INTO l_json_vals  
    FROM (SELECT chv_code       "code",
                 chv_name       "name",
                 cht_chart_type "overrideChartType",
                 array_to_json(l_vals) "values"
            FROM ss_dat.chart_values
              LEFT JOIN ss_dat.chart_types ON chv_cht_id = cht_id
           WHERE chv_dstd_id = (SELECT ucs_dstd_id FROM ss_dat.user_chart_studies WHERE ucs_id = p_ucsId)
             AND chv_code = 'RSI') z;
  
  l_guides := '[{"v":70,"l":"Overvalued"},{"v":30,"l":"Undervalued"}]'::json;
  
  RETURN (l_json_vals,l_guides,NULL)::ss_dat.t_studyVals_return;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;
