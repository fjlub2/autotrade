CREATE OR REPLACE FUNCTION ss_src."c_Chart_store"(p_ustId          ss_dat.user_strategies.ust_id%TYPE,
                                                  p_uctId          ss_dat.user_charts.uct_id%TYPE,
                                                  p_timeframe      ss_dat.d_timeframes.dtfr_mins%TYPE,
                                                  p_chartType      ss_dat.chart_types.cht_code%TYPE,
                                                  p_showVolume     ss_dat.user_charts.uct_show_volume%TYPE,
                                                  p_positiveColour ss_dat.user_charts.uct_positive_colour%TYPE,
                                                  p_negativeColour ss_dat.user_charts.uct_negative_colour%TYPE,
                                                  p_volumeColour   ss_dat.user_charts.uct_volume_colour%TYPE,
                                                  p_areaType       ss_dat.user_charts.uct_area_type%TYPE) RETURNS ss_dat.user_charts.uct_id%TYPE AS
$BODY$
DECLARE
  l_dsymId         ss_dat.d_symbols.dsym_id%TYPE;
  l_dtfrId         ss_dat.d_timeframes.dtfr_id%TYPE;
  l_uctId          ss_dat.user_charts.uct_id%TYPE;
  l_uctId2         ss_dat.user_charts.uct_id%TYPE;
  l_chtId          ss_dat.chart_types.cht_id%TYPE;
  l_syncFrom       ss_dat.user_charts.uct_sync_from_date%TYPE;
  l_syncTo         ss_dat.user_charts.uct_sync_to_date%TYPE;
  l_from           ss_dat.user_charts.uct_from%TYPE;
  l_to             ss_dat.user_charts.uct_to%TYPE;
  l_window         TEXT;
  l_ustRow         ss_dat.user_strategies%ROWTYPE;
  l_uctRow         ss_dat.user_charts%ROWTYPE;
  l_dtfrRow        ss_dat.d_timeframes%ROWTYPE;
  l_dsymRow        ss_dat.d_symbols%ROWTYPE;
  l_dsytRow        ss_dat.d_symbol_timeframes%ROWTYPE;
  l_updateUctRow   ss_tapi."t_UserCharts_updateRow";
  l_includeUctRow  ss_tapi."t_UserCharts_updateInclude";
  l_chpId          ss_dat.chart_periods.chp_id%TYPE;
  l_hours          ss_dat.chart_periods.chp_hours%TYPE;
  l_hours2         ss_dat.chart_periods.chp_hours%TYPE;
  l_oldMonths      ss_dat.d_timeframes.dtfr_max_months%TYPE;
  l_centreDate     TIMESTAMP;
  r_rec            RECORD;
  
  l_taskId         INTEGER;
  l_uctChanges     INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  IF p_timeframe IS NOT NULL THEN
    l_dtfrRow := ss_tapi."t_DTimeframes_getRow_UK"(p_uk := p_timeframe);
    
    IF l_dtfrRow.dtfr_id IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- timeframe doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','timeframe'),
                                              p_data   := core_src."c_Data_addTag"('DTFR_MINS',p_timeframe),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Timeframe'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF p_ustId IS NOT NULL THEN
    l_ustRow := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := p_ustId);
    
    IF l_ustRow.ust_usr_id IS NULL OR
       l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code    := 'T0004', -- strategy doesnt exist
                                              p_vars    := core_src."c_Data_addTag"('FIELD','strategy'),
                                              p_data    := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                              p_type    := 'E',
                                              p_taskId  := l_taskId);
    ELSIF l_ustRow.ust_live = 'Y' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code    := 'T0010', -- cant edit a live strategy
                                              p_data    := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                              p_type    := 'E',
                                              p_taskId  := l_taskId);
    END IF;
  END IF;
  
  IF p_uctId IS NOT NULL THEN
    l_uctRow := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);
    
    IF l_uctRow.uct_id IS NULL OR
       l_uctRow.uct_ust_id != p_ustId THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END IF;
  
  IF l_dtfrRow.dtfr_id IS NOT NULL THEN
    l_dsytRow := ss_tapi."t_DSymbolTimeframes_getRow_PK" (p_pk  := l_ustRow.ust_dsym_id, p_pk2 := l_dtfrRow.dtfr_id);
    
    IF l_dsytRow.dsyt_dsym_id IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                              p_vars   := core_src."c_Data_addTag"('FIELD','timeframe'),
                                              p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('DTFR_ID',l_dtfrRow.dtfr_id),'DSYM_ID',l_ustRow.ust_dsym_id),
                                              p_type   := 'F',
                                              p_taskId := l_taskId);
    END IF;
  END IF;
  
  IF COALESCE(p_showVolume,'') != '' THEN
    IF p_showVolume NOT IN ('N','Y') THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                              p_vars   := core_src."c_Data_addTag"('FIELD','show volume'),
                                              p_data   := core_src."c_Data_addTag"('UCT_SHOW_VOLUME',p_showVolume),
                                              p_type   := 'F',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Show Volume'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF COALESCE(p_areaType,'') != '' THEN
    IF p_areaType NOT IN ('entry','exit') THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                              p_vars   := core_src."c_Data_addTag"('FIELD','area type'),
                                              p_data   := core_src."c_Data_addTag"('UCT_AREA_TYPE',p_areaType),
                                              p_type   := 'F',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Area Type'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF COALESCE(p_chartType,'') != '' THEN
    l_chtId := ss_tapi."t_ChartTypes_getId_UK"(p_uk := p_chartType);
    
    IF l_chtId IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart type'),
                                              p_data   := core_src."c_Data_addTag"('CHT_CODE',p_chartType),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart Type'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF COALESCE(p_positiveColour,'') != '' THEN
    IF LENGTH(p_positiveColour) != 7 OR
       p_positiveColour NOT LIKE '#%' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                              p_vars   := core_src."c_Data_addTag"('FIELD','positive colour'),
                                              p_data   := core_src."c_Data_addTag"('UCT_POSITIVE_COLOUR',p_positiveColour),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Positive Colour'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF COALESCE(p_negativeColour,'') != '' THEN
    IF LENGTH(p_negativeColour) != 7 OR
       p_negativeColour NOT LIKE '#%' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                              p_vars   := core_src."c_Data_addTag"('FIELD','negative colour'),
                                              p_data   := core_src."c_Data_addTag"('UCT_NEGATIVE_COLOUR',p_negativeColour),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Negative Colour'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF COALESCE(p_volumeColour,'') != '' THEN
    IF LENGTH(p_volumeColour) != 7 OR
       p_volumeColour NOT LIKE '#%' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                              p_vars   := core_src."c_Data_addTag"('FIELD','volume colour'),
                                              p_data   := core_src."c_Data_addTag"('UCT_VOLUME_COLOUR',p_volumeColour),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Volume Colour'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF l_dtfrRow.dtfr_id IS NOT NULL THEN
    l_uctId2 := ss_tapi."t_UserCharts_getId_UK"(p_uk := p_ustId, p_uk2 := l_dtfrRow.dtfr_id, p_uk3 := p_areaType);
    
    IF (l_uctId2 IS NOT NULL AND
        p_uctId  IS     NULL) OR
       (l_uctId2 IS NOT NULL AND
        p_uctId  IS NOT NULL AND
        l_uctId2 != p_uctId) THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0011', -- duplicate timeframe
                                              p_vars   := core_src."c_Data_addTag"('FIELD','timeframe'),
                                              p_data   := core_src."c_Data_addTag"('DTFR_ID',l_dtfrRow.dtfr_id),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN -- don't want to continue if there were any errors with the input parameters.
    RETURN NULL;
  END IF;
  
  l_dsymRow   := ss_tapi."t_DSymbols_getRow_PK"(p_pk := l_ustRow.ust_dsym_id);
  l_oldMonths := ss_tapi."t_DTimeframes_getMaxMonths_PK"(p_pk := l_uctRow.uct_dtfr_id);
  
  IF l_uctRow.uct_id IS NULL OR 
    (l_uctRow.uct_id IS NOT NULL AND
     l_uctRow.uct_dtfr_id != l_dtfrRow.dtfr_id AND
     l_dtfrRow.dtfr_max_months != l_oldMonths) THEN
    IF l_uctRow.uct_id IS NOT NULL AND
       l_uctRow.uct_dtfr_id != l_dtfrRow.dtfr_id AND
       l_dtfrRow.dtfr_max_months != l_oldMonths THEN
      SELECT chp_id,
             chp_hours
        INTO l_chpId,
             l_hours
        FROM ss_dat.timeframe_chart_periods
          JOIN ss_dat.chart_periods ON tcp_chp_id = chp_id
       WHERE chp_id      = l_uctRow.uct_chp_id
         AND tcp_dtfr_id = l_dtfrRow.dtfr_id;
    END IF;
    
    IF l_chpId IS NULL THEN
      SELECT chp_id,
             chp_hours
        INTO l_chpId,
             l_hours
        FROM ss_dat.timeframe_chart_periods
          JOIN ss_dat.chart_periods ON tcp_chp_id = chp_id
       WHERE tcp_dtfr_id     = l_dtfrRow.dtfr_id
         AND tcp_default_ind = 'Y';
    END IF;
     
    IF l_ustRow.ust_sync_ind = 'Y' OR
       l_uctRow.uct_id IS NOT NULL THEN
      IF l_uctRow.uct_id IS NOT NULL THEN
        l_syncTo   := l_uctRow.uct_sync_to_date;
        l_syncFrom := l_uctRow.uct_sync_from_date;
        l_to       := l_uctRow.uct_to;
        l_from     := l_uctRow.uct_from;
        
        l_window := (l_dtfrRow.dtfr_max_months::NUMERIC - l_oldMonths::NUMERIC) / 2||' months';

        l_to       := l_to + l_window::INTERVAL;
        l_from     := l_from - l_window::INTERVAL;
      ELSE
        SELECT uct_sync_from_date,
               uct_sync_to_date,
               uct_from,
               uct_to,
               chp_hours
          INTO l_syncFrom,
               l_syncTo,
               l_from,
               l_to,
               l_hours2
          FROM ss_dat.user_charts
            JOIN ss_dat.chart_periods ON chp_id = uct_chp_id
         WHERE uct_ust_id = l_ustRow.ust_id
         LIMIT 1;
         
        IF l_hours2 IS NOT NULL AND
           l_hours != l_hours2 THEN
          l_syncFrom := l_syncFrom - (((l_hours2 - l_hours) / 2)||' hours')::INTERVAL;
          l_syncTo   := l_syncTo + (((l_hours2 - l_hours) / 2)||' hours')::INTERVAL;
        END IF;
        
        l_window := (l_dtfrRow.dtfr_max_months::NUMERIC - l_oldMonths::NUMERIC) / 2||' months';
	
	l_to   := l_to + l_window::INTERVAL;
        l_from := l_from - l_window::INTERVAL;
      END IF;
      
      IF l_from < l_dsytRow.dsyt_min_date THEN
        l_from := l_dsytRow.dsyt_min_date;
        l_to   := LEAST(l_from + (l_dtfrRow.dtfr_max_months||' months')::INTERVAL,l_dsytRow.dsyt_max_date);
      END IF;
          
      IF l_to > l_dsytRow.dsyt_max_date THEN
        l_to   := l_dsytRow.dsyt_max_date;
        l_from := GREATEST(l_to - (l_dtfrRow.dtfr_max_months||' months')::INTERVAL,l_dsytRow.dsyt_min_date);
      END IF;
      
      l_syncTo   := COALESCE(l_syncTo,l_dsytRow.dsyt_max_date);
      l_syncFrom := COALESCE(l_syncFrom,l_syncTo - (l_hours||' hours')::INTERVAL);
      l_to       := COALESCE(l_to,l_dsytRow.dsyt_max_date);
      l_from     := COALESCE(l_from,GREATEST(l_to - (l_dtfrRow.dtfr_max_months||' months')::INTERVAL,l_dsytRow.dsyt_min_date));
     
      IF l_ustRow.ust_sync_type = 'start' THEN
        l_syncTo   := l_syncFrom + (l_hours||' hours')::INTERVAL;
      ELSIF l_ustRow.ust_sync_type = 'end' THEN
        l_syncFrom := l_syncTo - (l_hours||' hours')::INTERVAL;
      ELSE -- centre
        l_centreDate := l_syncFrom + ((l_syncTo - l_syncFrom) / 2);
        l_syncTo     := l_centreDate + ((l_hours / 2)||' hours')::INTERVAL;
        l_syncFrom   := l_centreDate - ((l_hours / 2)||' hours')::INTERVAL;
      END IF;

      IF l_syncFrom < l_dsytRow.dsyt_min_date THEN
        l_syncFrom := l_dsytRow.dsyt_min_date;
        l_syncTo   := LEAST(l_syncFrom + (l_hours||' hours')::INTERVAL,l_dsytRow.dsyt_max_date);
      END IF;
          
      IF l_syncTo > l_dsytRow.dsyt_max_date THEN
        l_syncTo   := l_dsytRow.dsyt_max_date;
        l_syncFrom := GREATEST(l_syncTo - (l_hours||' hours')::INTERVAL,l_dsytRow.dsyt_min_date);
      END IF;
      
      IF l_syncFrom NOT BETWEEN l_from AND l_to OR
         l_syncTo   NOT BETWEEN l_from AND l_to THEN
        l_window := (l_dtfrRow.dtfr_max_months::NUMERIC / 2)||' months';
     
        IF l_syncFrom NOT BETWEEN l_from AND l_to THEN
          IF l_syncFrom + l_window::INTERVAL > l_dsytRow.dsyt_max_date THEN
            l_window := l_dtfrRow.dtfr_max_months||' months';
            l_to     := l_dsytRow.dsyt_max_date;
            l_from   := GREATEST(l_to - l_window::INTERVAL,l_dsytRow.dsyt_min_date);
          ELSIF l_syncFrom - l_window::INTERVAL < l_dsytRow.dsyt_min_date THEN
            l_window := l_dtfrRow.dtfr_max_months||' months';
            l_from   := l_dsytRow.dsyt_min_date;
            l_to     := LEAST(l_from + l_window::INTERVAL,l_dsytRow.dsyt_max_date);
          ELSE
            l_from   := GREATEST(l_syncFrom - l_window::INTERVAL,l_dsytRow.dsyt_min_date);
            l_to     := LEAST(l_syncFrom + l_window::INTERVAL,l_dsytRow.dsyt_max_date);
          END IF;
        END IF;
         
        IF l_syncTo NOT BETWEEN l_from AND l_to THEN
          IF l_syncTo + l_window::INTERVAL > l_dsytRow.dsyt_max_date THEN
            l_window := l_dtfrRow.dtfr_max_months||' months';
            l_to     := l_dsytRow.dsyt_max_date;
            l_from   := GREATEST(l_to - l_window::INTERVAL,l_dsytRow.dsyt_min_date);
          ELSIF l_syncTo - l_window::INTERVAL < l_dsytRow.dsyt_min_date THEN
            l_window := l_dtfrRow.dtfr_max_months||' months';
            l_from   := l_dsytRow.dsyt_min_date;
            l_to     := LEAST(l_from + l_window::INTERVAL,l_dsytRow.dsyt_max_date);
          ELSE
            l_from   := GREATEST(l_syncTo - l_window::INTERVAL,l_dsytRow.dsyt_min_date);
            l_to     := LEAST(l_syncTo + l_window::INTERVAL,l_dsytRow.dsyt_max_date);
          END IF;
        END IF;
      END IF;    
    ELSE
      l_syncTo   := l_dsytRow.dsyt_max_date;
      l_syncFrom := l_syncTo - (l_hours||' hours')::INTERVAL;
      l_window   := l_dtfrRow.dtfr_max_months||' months';
      l_to       := l_dsytRow.dsyt_max_date;
      l_from     := GREATEST(l_to - l_window::INTERVAL,l_dsytRow.dsyt_min_date);
    END IF;
  ELSE
    l_syncFrom := l_uctRow.uct_sync_from_date;
    l_syncTo   := l_uctRow.uct_sync_to_date;
    l_from     := l_uctRow.uct_from;
    l_to       := l_uctRow.uct_to;
    l_chpId    := l_uctRow.uct_chp_id;
  END IF;
  
  IF l_uctRow.uct_id IS NULL THEN      
    l_uctRow.uct_dtfr_id         := l_dtfrRow.dtfr_id;
    l_uctRow.uct_from            := l_from;
    l_uctRow.uct_to              := l_to;
    l_uctRow.uct_ust_id          := p_ustId;
    l_uctRow.uct_cht_id          := l_chtId;
    l_uctRow.uct_show_volume     := p_showVolume;
    l_uctRow.uct_positive_colour := p_positiveColour;
    l_uctRow.uct_negative_colour := p_negativeColour;
    l_uctRow.uct_volume_colour   := p_volumeColour;
    l_uctRow.uct_sync_from_date  := l_syncFrom;
    l_uctRow.uct_sync_to_date    := l_syncTo;
    l_uctRow.uct_chp_id          := l_chpId;
    l_uctRow.uct_area_type       := p_areaType;
  
    l_uctId := ss_tapi."t_UserCharts_insertRow" (p_row := l_uctRow);
    
    PERFORM ss_tapi."t_UserStrategies_setUpdated"(p_pk := p_ustId, p_val := localtimestamp);
    PERFORM ss_tapi."t_UserStrategies_setDefaultUctId"(p_pk := p_ustId, p_val := l_uctRow.uct_id);
    
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Stored new chart.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',l_uctId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  ELSE
    l_uctChanges := 0;

    IF l_uctRow.uct_dtfr_id != l_dtfrRow.dtfr_id THEN
      l_updateUctRow.uct_dtfr_id  := l_dtfrRow.dtfr_id;
      l_includeUctRow.uct_dtfr_id := 'Y';
      l_uctChanges                := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_dtfr_id),
                                                                                        'NEW',l_dtfrId),
                                                                                    'FIELD','UCT_DTFR_ID'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
    
    IF l_uctRow.uct_from IS NULL OR
       l_uctRow.uct_from != l_from THEN
      l_updateUctRow.uct_from  := l_from;
      l_includeUctRow.uct_from := 'Y';
      l_uctChanges             := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_from),
                                                                                        'NEW',l_from),
                                                                                    'FIELD','UCT_FROM'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
  
    IF l_uctRow.uct_to IS NULL OR
       l_uctRow.uct_to != l_to THEN
      l_updateUctRow.uct_to  := l_to;
      l_includeUctRow.uct_to := 'Y';
      l_uctChanges           := 1;
        
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_to),
                                                                                        'NEW',l_to),
                                                                                    'FIELD','UCT_TO'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
    
    IF l_uctRow.uct_sync_from_date IS NULL OR
       l_uctRow.uct_sync_from_date != l_syncFrom THEN
      l_updateUctRow.uct_sync_from_date  := l_syncFrom;
      l_includeUctRow.uct_sync_from_date := 'Y';
      l_uctChanges                       := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_sync_from_date),
                                                                                        'NEW',l_syncFrom),
                                                                                    'FIELD','UCT_SYNC_FROM_DATE'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
  
    IF l_uctRow.uct_sync_to_date IS NULL OR
       l_uctRow.uct_sync_to_date != l_syncTo THEN
      l_updateUctRow.uct_sync_to_date  := l_syncTo;
      l_includeUctRow.uct_sync_to_date := 'Y';
      l_uctChanges                     := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_sync_to_date),
                                                                                        'NEW',l_syncTo),
                                                                                    'FIELD','UCT_SYNC_TO_DATE'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
    
    IF l_uctRow.uct_chp_id != l_chpId THEN
      l_updateUctRow.uct_chp_id  := l_chpId;
      l_includeUctRow.uct_chp_id := 'Y';
      l_uctChanges               := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_chp_id),
                                                                                        'NEW',l_chpId),
                                                                                    'FIELD','UCT_CHP_ID'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF; 
    
    IF l_uctRow.uct_cht_id != l_chtId THEN
      l_updateUctRow.uct_cht_id  := l_chtId;
      l_includeUctRow.uct_cht_id := 'Y';
      l_uctChanges               := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_cht_id),
                                                                                        'NEW',l_chtId),
                                                                                    'FIELD','UCT_CHT_ID'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF; 
    
    IF l_uctRow.uct_show_volume != p_showVolume THEN
      l_updateUctRow.uct_show_volume  := p_showVolume;
      l_includeUctRow.uct_show_volume := 'Y';
      l_uctChanges                    := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_show_volume),
                                                                                        'NEW',p_showVolume),
                                                                                    'FIELD','UCT_SHOW_VOLUME'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
    
    IF l_uctRow.uct_positive_colour != p_positiveColour THEN
      l_updateUctRow.uct_positive_colour  := p_positiveColour;
      l_includeUctRow.uct_positive_colour := 'Y';
      l_uctChanges                        := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_positive_colour),
                                                                                        'NEW',p_positiveColour),
                                                                                    'FIELD','UCT_POSITIVE_COLOUR'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
    
    IF l_uctRow.uct_negative_colour != p_negativeColour THEN
      l_updateUctRow.uct_negative_colour  := p_negativeColour;
      l_includeUctRow.uct_negative_colour := 'Y';
      l_uctChanges                        := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_negative_colour),
                                                                                        'NEW',p_negativeColour),
                                                                                    'FIELD','UCT_NEGATIVE_COLOUR'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
    
    IF l_uctRow.uct_volume_colour != p_volumeColour THEN
      l_updateUctRow.uct_volume_colour  := p_volumeColour;
      l_includeUctRow.uct_volume_colour := 'Y';
      l_uctChanges                      := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                            'OLD',l_uctRow.uct_volume_colour),
                                                                                        'NEW',p_volumeColour),
                                                                                    'FIELD','UCT_VOLUME_COLOUR'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;

    IF l_uctChanges = 1 THEN
      PERFORM ss_tapi."t_UserCharts_updateRow"(p_pk := l_uctRow.uct_id, p_row := l_updateUctRow, p_include := l_includeUctRow);
                                              
      PERFORM ss_tapi."t_UserStrategies_setUpdated"(p_pk := p_ustId, p_val := localtimestamp);
      PERFORM ss_tapi."t_UserStrategies_setDefaultUctId"(p_pk := p_ustId, p_val := l_uctRow.uct_id);
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Updated chart.',
                                              p_data    := core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    ELSE
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Request made to update chart options, but no changes identified',
                                              p_data    := core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);    
    END IF;
    
    IF p_showVolume = 'N' AND l_uctRow.uct_show_volume != p_showVolume THEN
      FOR r_rec IN (SELECT ucs_id
                      FROM ss_dat.user_chart_studies
                     WHERE ucs_uct_id = l_uctRow.uct_id
                       AND ucs_chart_panel = 'volume') LOOP
        PERFORM ss_src."c_Study_delete"(p_ucsId := r_rec.ucs_id);
        
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Removed volume study as volume panel has been hidden.',
                                                p_data    := core_src."c_Data_addTag"('UCS_ID',r_rec.ucs_id),
                                                p_type    := 'I',
                                                p_taskId  := l_taskId);
      END LOOP;
    END IF;
    
    l_uctId := l_uctRow.uct_id;
  END IF;
                 
  RETURN l_uctId;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;