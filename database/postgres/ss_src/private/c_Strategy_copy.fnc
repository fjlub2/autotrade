CREATE OR REPLACE FUNCTION ss_src."c_Strategy_copy"(p_ustId ss_dat.user_strategies.ust_id%TYPE, p_draft IN text) RETURNS ss_dat.user_strategies.ust_id%TYPE AS
$BODY$
DECLARE
  l_json           JSON;
  
  l_draft          TEXT;  
  l_ustRow         ss_dat.user_strategies%ROWTYPE;
  l_ustId          ss_dat.user_strategies.ust_id%TYPE;
  l_uctRow         ss_dat.user_charts%ROWTYPE;
  l_uctId          ss_dat.user_charts.uct_id%TYPE;
  r_charts         RECORD;
  l_taskId         INTEGER;
  
  l_data           TEXT;
  l_debug          TEXT;
  l_context        TEXT;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  IF p_ustId IS NOT NULL THEN
    l_ustRow := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := p_ustId);
    
    IF l_ustRow.ust_usr_id IS NULL OR
       l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- strategy doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','strategy'),
                                              p_data   := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Strategy'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_draft := p_draft;
  
  IF COALESCE(l_draft,'') != '' THEN 
    IF l_draft NOT IN ('Y', 'N') THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','draft'),
                                              p_data   := core_src."c_Data_addTag"('DRAFT',l_draft),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    l_draft := 'N';
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN -- don't want to continue if there were any errors with the input parameters.
    RETURN NULL;
  END IF;
  
  IF l_ustRow.ust_live = 'N' THEN -- can only create a draft on a live strategy
    l_draft := 'N';
  END IF;
    
  IF COALESCE(l_ustRow.ust_name,'') != '' THEN
    l_ustRow.ust_name   := l_ustRow.ust_name||' (copy)';
  ELSE
    l_ustRow.ust_name   := NULL;
  END IF;
  
  l_ustRow.ust_id       := NULL;
  l_ustRow.ust_live     := 'N';
  l_ustRow.ust_updated  := NOW();
  
  IF l_draft = 'Y' THEN
    l_ustRow.ust_ust_id := p_ustId;
  ELSE
    l_ustRow.ust_ust_id := NULL;
  END IF;
  
  l_ustId := ss_tapi."t_UserStrategies_insertRow" (p_row := l_ustRow);
  
  l_uctRow.uct_id     := NULL;
  l_uctRow.uct_ust_id := l_ustId;
  
  FOR r_charts IN (SELECT uct_id,
                          uct_dtfr_id,
                          uct_from,
                          uct_to,
                          uct_cht_id
                   FROM ss_dat.user_charts
                  WHERE uct_ust_id = p_ustId) LOOP
    l_uctRow.uct_dtfr_id := r_charts.uct_dtfr_id;
    l_uctRow.uct_from    := r_charts.uct_from;
    l_uctRow.uct_to      := r_charts.uct_to;
    l_uctRow.uct_cht_id  := r_charts.uct_cht_id;
    
    l_uctId := ss_tapi."t_UserCharts_insertRow" (p_row := l_uctRow);
    
    -- add code to copy strategies
  END LOOP;
    
  RETURN l_ustId;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;