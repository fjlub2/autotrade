CREATE OR REPLACE FUNCTION ss_src."c_StudyData_Resistance_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE,
                                                               p_dsymId ss_dat.d_symbols.dsym_id%TYPE,
                                                               p_dtfrId ss_dat.d_timeframes.dtfr_id%TYPE,
                                                               p_from   ss_dat.user_charts.uct_from%TYPE,
                                                               p_to     ss_dat.user_charts.uct_to%TYPE)
  RETURNS ss_dat.t_studyvals_return AS
$BODY$
DECLARE
  l_maxPeriod    INTEGER;
  l_minPeriod    INTEGER;
  l_ticks        INTEGER;
  l_peaks        INTEGER;
  l_tolerance    ss_dat.f_data.fdat_close%TYPE;
  l_dsymRow      ss_dat.d_symbols%ROWTYPE;
  l_json         JSON;
  l_taskId       INTEGER;
  l_basedOn      TEXT;
  r_rec          RECORD;
  l_preFrom      ss_dat.f_data.fdat_date%TYPE;
  l_postTo       ss_dat.f_data.fdat_date%TYPE;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Study Resistance Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
                                          
  IF p_ucsId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
                                          
  IF p_dsymId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_dtfrId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Time Frame'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_from IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','From Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_to IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','To Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_maxPeriod := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'MAX_PERIODS');
  
  IF l_maxPeriod IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('MAX_PERIODS','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_minPeriod := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'MIN_PERIODS');
  
  IF l_minPeriod IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('MIN_PERIODS','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_ticks := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'TICK_TOLERANCE');
  
  IF l_ticks IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('TICK_TOLERANCE','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_peaks := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'PEAKS');
  
  IF l_peaks IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('PEAKS','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_basedOn := ss_src."c_StudyParamText_get"(p_ucsId := p_ucsId, p_param := 'BASED_ON');
  
  IF l_basedOn IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('BASED_ON','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := NULL;
  ELSE
    l_dsymRow := ss_tapi."t_DSymbols_getRow_PK"(p_pk := p_dsymId);
    
    l_tolerance := l_dsymRow.dsym_tick_size * l_ticks::NUMERIC;
    
    SELECT MIN(fdat_date)
      INTO l_preFrom
      FROM (SELECT fdat_date
              FROM ss_dat.f_data
             WHERE fdat_dsym_id = p_dsymId
               AND fdat_dtfr_id = p_dtfrId
               AND fdat_date < p_from
             ORDER BY fdat_date DESC
             LIMIT l_maxPeriod) a;
             
    SELECT MAX(fdat_date)
      INTO l_postTo
      FROM (SELECT fdat_date
              FROM ss_dat.f_data
             WHERE fdat_dsym_id = p_dsymId
               AND fdat_dtfr_id = p_dtfrId
               AND fdat_date > p_to
             ORDER BY fdat_date
             LIMIT l_maxPeriod) a;

    l_preFrom := COALESCE(l_preFrom,p_from);
    l_postTo  := COALESCE(l_postTo, p_to);

    WITH peaks AS (SELECT fdat_unix_ms,
                          price,
                          first_date
                     FROM (SELECT fdat_unix_ms,
                                  price,
                                  CASE WHEN price >= LAG(price,1) OVER w1 AND price >= LEAD(price,1) OVER w1 THEN 1 ELSE NULL END peak,
                                  LAG(fdat_unix_ms, l_maxPeriod, min_date) OVER w1 first_date
                             FROM (SELECT fdat_unix_ms,
                                          CASE l_basedOn WHEN 'close' THEN fdat_close
                                                         WHEN 'high'  THEN fdat_high
                                                         WHEN 'low'   THEN fdat_low
                                                         WHEN 'open'  THEN fdat_open END price,
                                          MIN(fdat_unix_ms) OVER w1 min_date
                                     FROM ss_dat.f_data
                                    WHERE fdat_dsym_id = p_dsymId
                                      AND fdat_dtfr_id = p_dtfrId
                                      AND fdat_date BETWEEN l_preFrom AND p_to
                                   WINDOW w1 AS (ORDER BY fdat_unix_ms)) d
                           WINDOW w1 AS (ORDER BY fdat_unix_ms)) c
                    WHERE peak = 1)
    SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a)))
      INTO l_json  
      FROM (SELECT GREATEST(MIN(fdat_unix_ms), EXTRACT(EPOCH FROM p_from) * 1000) "d",
                   LEAST(MAX(fdat_unix_ms), EXTRACT(EPOCH FROM p_to) * 1000) "dt",
                   MIN(peak_price) "v",
                   MAX(peak_price) "vt"
              FROM (SELECT p2.fdat_unix_ms,
                           p2.price peak_price,
                           COUNT(1) OVER (PARTITION BY p1.fdat_unix_ms) peak_count,
                           RANK() OVER (ORDER BY p1.fdat_unix_ms) peak_group
                      FROM peaks p1
                        JOIN peaks p2 ON p2.fdat_unix_ms BETWEEN p1.first_date AND p1.fdat_unix_ms AND p2.price BETWEEN p1.price - l_tolerance AND p1.price + l_tolerance) b
             WHERE peak_count >= l_peaks
             GROUP BY peak_group) a;
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Resistance Data.',
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN (NULL,l_json,NULL)::ss_dat.t_studyVals_return;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;
