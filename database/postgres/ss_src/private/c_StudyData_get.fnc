CREATE OR REPLACE FUNCTION ss_src."c_StudyData_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE,
                                                    p_dstdId ss_dat.d_studies.dstd_id%TYPE,
                                                    p_chtId  ss_dat.chart_types.cht_id%TYPE,
                                                    p_showOn ss_dat.user_chart_studies.ucs_chart_panel%TYPE,
                                                    p_dsymId ss_dat.d_symbols.dsym_id%TYPE,
                                                    p_dtfrId ss_dat.d_timeframes.dtfr_id%TYPE,
                                                    p_from   ss_dat.user_charts.uct_from%TYPE,
                                                    p_to     ss_dat.user_charts.uct_to%TYPE,
                                                    p_uctId  ss_dat.user_charts.uct_id%TYPE,
                                                    p_name   ss_dat.user_chart_studies.ucs_name%TYPE)
  RETURNS json AS
$BODY$
DECLARE
  l_studyVals ss_dat.t_studyvals_return;
  l_data      JSON;
  l_guides    JSON;
  l_trends    JSON;
  l_json      JSON;
  l_params    JSON;
  l_dstdRow   ss_dat.d_studies%ROWTYPE;
  l_title     TEXT;
  l_ct        ss_dat.chart_types.cht_chart_type%TYPE;
  l_taskId    INTEGER;
  l_period    ss_dat.user_chart_study_params.usp_value_int%TYPE;
  l_volColour ss_dat.user_charts.uct_volume_colour%TYPE;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Preparing Study Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF p_dsymId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_dtfrId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Time Frame'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_from IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','From Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_to IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','To Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_ucsId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','User Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_dstdId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_chtId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart Type'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_showOn IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart Panel'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_dstdRow := ss_tapi."t_DStudies_getRow_PK"(p_pk := p_dstdId);
  l_ct      := ss_tapi."t_ChartTypes_getChartType_PK"(p_pk := p_chtId);
  l_title   := l_dstdRow.dstd_name;
  
  IF l_dstdRow.dstd_code = 'RELVOL' THEN
    l_studyVals := ss_src."c_StudyData_RelVol_get"(p_ucsId := p_ucsId, p_dsymId := p_dsymId, p_dtfrId := p_dtfrId, p_from := p_from, p_to := p_to);
  ELSEIF l_dstdRow.dstd_code = 'SMA' THEN
    l_studyVals := ss_src."c_StudyData_SMA_get"(p_ucsId := p_ucsId, p_dsymId := p_dsymId, p_dtfrId := p_dtfrId, p_from := p_from, p_to := p_to);
  ELSEIF l_dstdRow.dstd_code = 'EMA' THEN
    l_studyVals := ss_src."c_StudyData_EMA_get"(p_ucsId := p_ucsId, p_dsymId := p_dsymId, p_dtfrId := p_dtfrId, p_from := p_from, p_to := p_to);
  ELSEIF l_dstdRow.dstd_code = 'BB' THEN
    l_studyVals := ss_src."c_StudyData_BB_get"(p_ucsId := p_ucsId, p_dsymId := p_dsymId, p_dtfrId := p_dtfrId, p_from := p_from, p_to := p_to);
/*  ELSEIF l_dstdRow.dstd_code = 'SUPPORT' THEN
    l_studyVals := ss_src."c_StudyData_Support_get"(p_ucsId := p_ucsId, p_dsymId := p_dsymId, p_dtfrId := p_dtfrId, p_from := p_from, p_to := p_to);*/
  ELSEIF l_dstdRow.dstd_code = 'RESIST' THEN
    l_studyVals := ss_src."c_StudyData_Resistance_get"(p_ucsId := p_ucsId, p_dsymId := p_dsymId, p_dtfrId := p_dtfrId, p_from := p_from, p_to := p_to);
  ELSEIF l_dstdRow.dstd_code = 'ACCDIST' THEN
    l_studyVals := ss_src."c_StudyData_AccDist_get"(p_ucsId := p_ucsId, p_dsymId := p_dsymId, p_dtfrId := p_dtfrId, p_from := p_from, p_to := p_to);
  ELSEIF l_dstdRow.dstd_code = 'AROON' THEN
    l_studyVals := ss_src."c_StudyData_Aroon_get"(p_ucsId := p_ucsId, p_dsymId := p_dsymId, p_dtfrId := p_dtfrId, p_from := p_from, p_to := p_to);
  ELSEIF l_dstdRow.dstd_code = 'RSI' THEN
    l_studyVals := ss_src."c_StudyData_RSI_get"(p_ucsId := p_ucsId, p_dsymId := p_dsymId, p_dtfrId := p_dtfrId, p_from := p_from, p_to := p_to);
  ELSEIF l_dstdRow.dstd_code = 'MACD' THEN
    l_studyVals := ss_src."c_StudyData_MACD_get"(p_ucsId := p_ucsId, p_dsymId := p_dsymId, p_dtfrId := p_dtfrId, p_from := p_from, p_to := p_to);
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                            p_vars   := core_src."c_Data_addTag"('FIELD','study'),
                                            p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('UCS_ID',p_ucsId),'DSTD_CODE',l_dstdRow.dstd_code),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := NULL;
  ELSE
    l_title := ss_src."c_StudyName_get"(p_ucsId := p_ucsId);
    
    l_data   := l_studyVals.vals;
    l_guides := l_studyVals.guides;
    l_trends := l_studyVals.trends;
    
    IF p_showOn = 'volume' THEN
      l_volColour := ss_tapi."t_UserCharts_getVolumeColour_PK"(p_pk := p_uctId);
    ELSE
      l_volColour := NULL;
    END IF;
    
    l_guides := COALESCE(l_guides,'[]');
    l_data   := COALESCE(l_data,  '[]');
    l_trends := COALESCE(l_trends,  '[]');
    
    SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a)))
      INTO l_params
      FROM (SELECT param_code       "code",
                   param_value_text "valueText",
                   param_value_int  "valueInt",
                   param_type       "type"
              FROM ss_src."c_StudyParams_get"(p_ucsId := p_ucsId)) a;
              
    l_params := COALESCE(l_params,'[]');
                 
    SELECT ROW_TO_JSON(f)
      INTO l_json
      FROM (SELECT p_ucsId        "studyId",
                   ROW_TO_JSON(a) "studySettings",
                   l_data "studyData",
                   l_guides "studyGuides",
                   l_trends "studyTrends"
              FROM (SELECT l_ct     "studyType",
                           l_title  "studyTitle",
                           p_name   "studyName",
                           ss_tapi."t_UserChartStudies_getColour_PK"(p_pk := p_ucsId) "colour",
                           p_showOn "showOn",
                           l_volColour "volumeColour",
                           l_dstdRow.dstd_guide_type "guideType",
                           l_dstdRow.dstd_min_value "minValue",
                           l_dstdRow.dstd_max_value "maxValue",
                           l_params "params") a) f;
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Study data prepared.',
                                            p_data    := core_src."c_Data_addTag"('UCS_ID',p_ucsId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN l_json;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;


