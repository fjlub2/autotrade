CREATE OR REPLACE FUNCTION ss_src."c_Strategy_delete"(pi_ustId ss_dat.user_strategies.ust_id%TYPE) RETURNS void AS
$BODY$
DECLARE
  l_taskId INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  IF pi_ustId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Strategy'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
    RETURN;
  END IF;
  
  IF ss_tapi."t_UserStrategies_getUsrId_PK"(p_pk := pi_ustId) != core_src."c_Session_getCurrentUsrId"() THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- strategy doesnt exist
                                            p_vars   := core_src."c_Data_addTag"('FIELD','strategy'),
                                            p_data   := core_src."c_Data_addTag"('UST_ID',pi_ustId),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
    RETURN;
  END IF;
  
  DELETE
    FROM ss_dat.action_items
   WHERE ai_aig_id IN (SELECT aig_id
                         FROM ss_dat.action_item_groups
                        WHERE aig_ust_id = pi_ustId);
                        
  DELETE
    FROM ss_dat.action_item_groups
   WHERE aig_ust_id = pi_ustId;
    
  
  DELETE 
    FROM ss_dat.user_chart_study_params
   WHERE usp_ucs_id IN (SELECT ucs_id
                          FROM ss_dat.user_chart_studies
                         WHERE ucs_uct_id IN (SELECT uct_id
                                                FROM ss_dat.user_charts
                                               WHERE uct_ust_id = pi_ustId));
                         
  DELETE
    FROM ss_dat.user_chart_studies
   WHERE ucs_uct_id IN (SELECT uct_id
                          FROM ss_dat.user_charts
                         WHERE uct_ust_id = pi_ustId);
                         
  PERFORM ss_tapi."t_UserStrategies_setDefaultUctId"(p_pk := pi_ustId, p_val := NULL);
                         
  DELETE 
    FROM ss_dat.user_charts
   WHERE uct_ust_id = pi_ustId;
   
  PERFORM ss_tapi."t_UserStrategies_deleteRow"(p_pk := pi_ustId);
  
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;