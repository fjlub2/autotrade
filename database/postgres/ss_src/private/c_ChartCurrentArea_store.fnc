CREATE OR REPLACE FUNCTION ss_src."c_ChartCurrentArea_store"(p_uctId      ss_dat.user_charts.uct_id%TYPE,
                                                             p_type       TEXT) RETURNS void AS
$BODY$
DECLARE
  l_ustRow         ss_dat.user_strategies%ROWTYPE;
  l_uctRow         ss_dat.user_charts%ROWTYPE;
  l_updateUctRow   ss_tapi."t_UserCharts_updateRow";
  l_includeUctRow  ss_tapi."t_UserCharts_updateInclude";
  
  l_taskId         INTEGER;
  l_uctChanges     INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  IF p_uctId IS NOT NULL THEN
    l_uctRow := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);
    l_ustRow := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := l_uctRow.uct_ust_id);
    
    IF l_uctRow.uct_id     IS NULL OR
       l_ustRow.ust_usr_id IS NULL OR
       l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_type IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Type'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  ELSIF p_type NOT IN ('entry','exit') THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                            p_vars   := core_src."c_Data_addTag"('FIELD','type'),
                                            p_data   := core_src."c_Data_addTag"('TYPE',p_type),
                                            p_type   := 'F',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN -- don't want to continue if there were any errors with the input parameters.
    RETURN;
  END IF;

  l_uctChanges := 0;
    
  IF l_uctRow.uct_current_area IS NULL OR 
     (l_uctRow.uct_current_area IS NOT NULL AND
      l_uctRow.uct_current_area != p_type) THEN
    l_updateUctRow.uct_current_area  := p_type;
    l_includeUctRow.uct_current_area := 'Y';
    l_uctChanges                     := 1;
      
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                            p_data    := core_src."c_Data_addTag"(
                                                             core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                          'OLD',l_uctRow.uct_current_area),
                                                                                      'NEW',p_type),
                                                                                  'FIELD','UCT_CURRENT_AREA'),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  IF l_uctChanges = 1 THEN
    PERFORM ss_tapi."t_UserCharts_updateRow"(p_pk := l_uctRow.uct_id, p_row := l_updateUctRow, p_include := l_includeUctRow);
                                              
    PERFORM ss_tapi."t_UserStrategies_setUpdated"(p_pk := l_uctRow.uct_ust_id, p_val := localtimestamp);
      
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Updated chart current area.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  ELSE
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Request made to update chart current area, but no changes identified',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);    
  END IF;
                 
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;