CREATE OR REPLACE FUNCTION ss_src."c_ChartNavMenu_get"(p_uctId ss_dat.user_charts.uct_id%TYPE)
  RETURNS TEXT AS
$BODY$
DECLARE
  l_uctRow    ss_dat.user_charts%ROWTYPE;
  l_ustRow    ss_dat.user_strategies%ROWTYPE;
  l_dsytRow   ss_dat.d_symbol_timeframes%ROWTYPE;
  l_interval  ss_dat.d_timeframes.dtfr_nav_months%TYPE;
  l_chpId     ss_dat.chart_periods.chp_id%TYPE;
  l_chpName   ss_dat.chart_periods.chp_name%TYPE;
  l_areaType  TEXT;
  l_months    INTEGER;
  l_taskId    INTEGER;
  l_selectors TEXT;
  l_text      TEXT;
  l_data      TEXT;
  l_debug     TEXT;
  l_context   TEXT;
BEGIN
  PERFORM core_src."c_Session_validSession"();
  
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Chart Nav Menu HTML.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF p_uctId IS NOT NULL THEN
    l_uctRow   := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);
    l_ustRow   := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := l_uctRow.uct_ust_id);
    l_dsytRow  := ss_tapi."t_DSymbolTimeframes_getRow_PK"(p_pk := l_ustRow.ust_dsym_id, p_pk2 := l_uctRow.uct_dtfr_id);
    l_interval := ss_tapi."t_DTimeframes_getNavMonths_PK"(p_pk := l_uctRow.uct_dtfr_id);
    
    IF l_uctRow.uct_id     IS NULL OR
       l_ustRow.ust_usr_id IS NULL OR
       l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- chart doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_text := NULL;
  ELSE
    l_text := '<script type="text/javascript">$( "#TiZoomBarOptions" ).hide(); $( "#navDatepicker" ).datepicker({showOn: "button", buttonImage: "../assets/default/images/amchart/calendar.svg", buttonImageOnly: true, buttonText: "Jump to date", changeMonth: true, changeYear: true,';
    
    l_text := l_text||'yearRange: "'||TO_CHAR(l_dsytRow.dsyt_min_date,'YYYY')||':'||TO_CHAR(l_dsytRow.dsyt_max_date,'YYYY')||'",';
    l_text := l_text||'minDate:new Date("'||TO_CHAR(l_dsytRow.dsyt_min_date,'YYYY/MM/DD')||'"),';
    l_text := l_text||'maxDate:new Date("'||TO_CHAR(l_dsytRow.dsyt_max_date,'YYYY/MM/DD')||'"),';
    
    l_text := l_text||'beforeShow: function(input, inst) {$("#ui-datepicker-div").addClass(''Smoothness''); }, onSelect: function(date) {var theDate = new Date(Date.parse($(this).datepicker("getDate")));var dateFormatted = $.datepicker.formatDate("yy-mm-dd", theDate);chartNavigation(''null'', dateFormatted)}}); function displayZoomBarOptions() {if ($("#TiZoomBarOptions").is(":hidden")) {$( "#TiZoomBarOptions" ).show(); hover_search();} else {$( "#TiZoomBarOptions" ).hide(); unhover_search();}} function chartNavigation(p_direction, p_date) {var chartId = $("#chart_id").val();block_page();$.ajax({type: "POST",url: "./chartNavigation",data: "navDirection="+ p_direction+"&navDate="+ p_date+"&chartId="+ chartId,dataType: ''json''}).done(function(data) {if (data.error_code != 200){alert(data.error_msg);$("#error_code").val(data.error_code);return true;}else{get_chart_data(chartId);}});}</script><table class="TiChartNavBar"><tr><td><table class="TiZoomBar"><tr><td class="searchTD" onclick="displayZoomBarOptions();" onmouseover="hover_search();" onmouseout="unhover_search();"><img style="vertical-align: middle;" id="search_image" src="../assets/default/images/amchart/search.svg" title="Zoom" width="20px" height="auto"></td>';
    
    IF l_uctRow.uct_chp_id IS NOT NULL THEN
      l_chpId := l_uctRow.uct_chp_id;
    
      SELECT chp_name
        INTO l_chpName
        FROM ss_dat.chart_periods
       WHERE chp_id = l_uctRow.uct_chp_id;
    ELSE
      SELECT chp_id, chp_name
        INTO l_chpId, l_chpName
        FROM ss_dat.timeframe_chart_periods
          JOIN ss_dat.chart_periods ON tcp_chp_id = chp_id
       WHERE tcp_dtfr_id     = l_uctRow.uct_dtfr_id
         AND tcp_default_ind = 'Y';
    END IF;
    
    l_text := l_text||'<td class="PeriodSelected">'||l_chpName||'</td></tr><tr class="trZoomBarOptions"><td><div id="TiZoomBarOptions"><table class="TiZoomBarOptions" onmouseleave="unhover_zoom();">';
    
    SELECT STRING_AGG('<tr><td onclick="setPeriod('''||chp_period||''','''||chp_count||''');">'||chp_name||'</td></tr>','' ORDER BY chp_order)
      INTO l_selectors
      FROM ss_dat.timeframe_chart_periods
        JOIN ss_dat.chart_periods ON tcp_chp_id = chp_id
    WHERE tcp_dtfr_id = l_uctRow.uct_dtfr_id
      AND chp_id     != l_chpId;
       
    l_text := l_text||l_selectors||'</table></div></td></tr></table></td><td style="padding-left: 30px"></td><td><table class="TiScrollBar"><tbody><tr>';

    IF l_uctRow.uct_from > l_dsytRow.dsyt_min_date THEN
      l_text := l_text||'<td class="nav_backward" onclick="chartNavigation(''B'', ''null'');"" onmouseover="hover_backward();" onmouseout="unhover_backward();"><img style="vertical-align: middle;" id="backward_image" src="../assets/default/images/amchart/backward_c.svg" title="Back '||l_interval||CASE l_interval WHEN 1 THEN ' month' ELSE ' months' END||'"></td>';
    END IF;
    
    IF l_uctRow.uct_to < l_dsytRow.dsyt_max_date THEN
      l_text := l_text||'<td class="nav_forward" onclick="chartNavigation(''F'', ''null'');" onmouseover="hover_forward();" onmouseout="unhover_forward();"><img style="vertical-align: middle;" id="forward_image" src="../assets/default/images/amchart/forward_c.svg" title="Forward '||l_interval||CASE l_interval WHEN 1 THEN ' month' ELSE ' months' END||'"></td>';
    END IF;
    
    IF l_uctRow.uct_from > l_dsytRow.dsyt_min_date OR
       l_uctRow.uct_to < l_dsytRow.dsyt_max_date THEN
      l_text := l_text||'<td><input type="hidden" id="navDatepicker"></td>';
    END IF;
    
    IF l_uctRow.uct_area_from IS NOT NULL THEN
      IF l_uctRow.uct_area_type = 'entry' THEN
        l_areaType := 'Entry';
      ELSE
        l_areaType := 'Exit';
      END IF;
      
      IF l_uctRow.uct_area_from BETWEEN l_uctRow.uct_sync_from_date AND l_uctRow.uct_sync_to_date OR
         l_uctRow.uct_area_to   BETWEEN l_uctRow.uct_sync_from_date AND l_uctRow.uct_sync_to_date THEN
        l_text := l_text||'<td id="navmenuAreaEntry" onclick="goToDates('||EXTRACT(EPOCH FROM l_uctRow.uct_area_from) * 1000||', '||EXTRACT(EPOCH FROM l_uctRow.uct_area_to) * 1000||');" class="nav_menu_area" style="background-color:#5f6f81;color:#FFFFFF;" onmouseover="hover_navmenuAreaEntry();" onmouseout="hover_navmenuAreaEntry();">'||l_areaType||'</td>';
      ELSE
        l_text := l_text||'<td id="navmenuAreaEntry" onclick="goToDates('||EXTRACT(EPOCH FROM l_uctRow.uct_area_from) * 1000||', '||EXTRACT(EPOCH FROM l_uctRow.uct_area_to) * 1000||');" class="nav_menu_area" style="background-color:#FFFFFF;color:#5f6f81;" onmouseover="hover_navmenuAreaEntry();" onmouseout="unhover_navmenuAreaEntry();">'||l_areaType||'</td>';
      END IF;
    END IF;
    
    l_text := l_text||'</tr></tbody></table></td></tr></table>';
   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Chart Nav Menu HTML.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
   END IF;
   
  RETURN l_text;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
