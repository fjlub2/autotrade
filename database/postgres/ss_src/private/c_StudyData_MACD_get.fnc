CREATE OR REPLACE FUNCTION ss_src."c_StudyData_MACD_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE,
                                                         p_dsymId ss_dat.d_symbols.dsym_id%TYPE,
                                                         p_dtfrId ss_dat.d_timeframes.dtfr_id%TYPE,
                                                         p_from   ss_dat.user_charts.uct_from%TYPE,
                                                         p_to     ss_dat.user_charts.uct_to%TYPE)
  RETURNS ss_dat.t_studyVals_return AS
$BODY$
DECLARE
  l_preFrom      ss_dat.f_data.fdat_date%TYPE;
  l_period1      INTEGER;
  l_period2      INTEGER;
  l_periodFast   INTEGER;
  l_periodSlow   INTEGER;
  l_periodSig    INTEGER;
  r_rec          RECORD;
  l_sigStart     INTEGER;
  l_firstEMAFast ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_valFast      ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_prevValFast  ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_firstEMASlow ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_valSlow      ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_prevValSlow  ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_firstEMASig  ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_prevValSig   ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_MACDLine     ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_MACDSignal   ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_MACDHist     ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_dsymRow      ss_dat.d_symbols%ROWTYPE;
  l_exponentFast ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_exponentSlow ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_exponentSig  ss_dat.f_study_EMA.fsem_value_close%TYPE;
  l_vals_Line    ss_dat.t_studyVals[];
  l_vals_Signal  ss_dat.t_studyVals[];
  l_vals_Hist    ss_dat.t_studyVals[];
  l_json_vals    JSON;
  l_taskId       INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting MACD Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
                                          
  IF p_ucsId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
                                          
  IF p_dsymId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_dtfrId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Time Frame'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_from IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','From Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_to IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','To Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_period1 := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'MACD_PERIOD1');
  
  IF l_period1 IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('MACD_PERIOD1','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_period2 := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'MACD_PERIOD2');
  
  IF l_period2 IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('MACD_PERIOD2','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_periodSig := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'MACD_PERIOD_SIGNAL');
  
  IF l_periodSig IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('MACD_PERIOD_SIGNAL','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json_vals   := NULL;
  ELSE
    l_periodFast := LEAST   (l_period1, l_period2);
    l_periodSlow := GREATEST(l_period1, l_period2);
  
    l_dsymRow := ss_tapi."t_DSymbols_getRow_PK"(p_pk := p_dsymId);
      
    l_exponentFast := ROUND(2::numeric / (l_periodFast::numeric + 1::numeric),4);
    l_exponentSlow := ROUND(2::numeric / (l_periodSlow::numeric + 1::numeric),4);
    l_exponentSig  := ROUND(2::numeric / (l_periodSig::numeric  + 1::numeric),4);
    
    l_sigStart  := l_periodSlow + l_periodSig;
      
    SELECT MIN(fdat_date)
      INTO l_preFrom
      FROM (SELECT fdat_date
              FROM ss_dat.f_data
             WHERE fdat_dsym_id = p_dsymId
               AND fdat_dtfr_id = p_dtfrId
               AND fdat_date < p_from
             ORDER BY fdat_date DESC
             LIMIT l_sigStart) a;

    l_preFrom := COALESCE(l_preFrom,p_from);
    
    FOR r_rec IN (SELECT fdat_unix_ms,
                         ROW_NUMBER() OVER w1 rownum,
                         fdat_date,
                         fdat_close price
                    FROM ss_dat.f_data
                   WHERE fdat_dsym_id = p_dsymId
                     AND fdat_date BETWEEN l_preFrom AND p_to
                     AND fdat_dtfr_id = p_dtfrId
                  WINDOW w1 AS (ORDER BY fdat_date)
                   ORDER BY fdat_date) LOOP
      IF r_rec.price IS NOT NULL THEN
        IF r_rec.rownum <= l_periodFast THEN
          l_firstEMAFast := COALESCE(l_firstEMAFast,0) + r_rec.price;
          l_valFast      := r_rec.price;
        ELSIF r_rec.rownum = l_periodFast + 1 THEN
          l_valFast := ROUND(l_firstEMAFast / l_periodFast, l_dsymRow.dsym_decimals);
        ELSE
          l_valFast := ROUND(r_rec.price * l_exponentFast + l_prevValFast * (1::numeric - l_exponentFast), l_dsymRow.dsym_decimals);
        END IF;
        
        IF r_rec.rownum <= l_periodSlow THEN
          l_firstEMASlow := COALESCE(l_firstEMASlow,0) + r_rec.price;
          l_valSlow      := r_rec.price;
        ELSIF r_rec.rownum = l_periodSlow + 1 THEN
          l_valSlow := ROUND(l_firstEMASlow / l_periodSlow, l_dsymRow.dsym_decimals);
        ELSE
          l_valSlow := ROUND(r_rec.price * l_exponentSlow + l_prevValSlow * (1::numeric - l_exponentSlow), l_dsymRow.dsym_decimals);
        END IF;
      END IF;
        
      IF r_rec.rownum < l_periodFast THEN
        l_valFast := r_rec.price;
      END IF;
      
      IF r_rec.rownum < l_periodSlow THEN
        l_valSlow := r_rec.price;
      END IF;
      
      l_MACDLine := l_valFast - l_valSlow;
      
      IF r_rec.rownum <= l_sigStart THEN
        l_firstEMASig := COALESCE(l_firstEMASig,0) + l_MACDLine;
        l_MACDSignal  := l_MACDLine;
      ELSIF r_rec.rownum = l_sigStart + 1 THEN
        l_MACDSignal := ROUND(l_firstEMASig / l_periodSig, l_dsymRow.dsym_decimals);
      ELSE
        l_MACDSignal := ROUND(l_MACDLine * l_exponentSig + l_prevValSig * (1::numeric - l_exponentSig), l_dsymRow.dsym_decimals);
      END IF;
      
      IF r_rec.rownum < l_sigStart THEN
        l_MACDSignal := l_MACDLine;
      END IF;
      
      l_MACDHist := l_MACDLine - l_MACDSignal;

      IF r_rec.fdat_date >= p_from THEN
        l_vals_Line   := array_append(l_vals_Line,   (r_rec.fdat_unix_ms,l_MACDLine)::ss_dat.t_studyVals);
        l_vals_Signal := array_append(l_vals_Signal, (r_rec.fdat_unix_ms,l_MACDSignal)::ss_dat.t_studyVals);
        l_vals_Hist   := array_append(l_vals_Hist,   (r_rec.fdat_unix_ms,l_MACDHist)::ss_dat.t_studyVals);
      END IF;

      l_prevValFast := l_valFast;
      l_prevValSlow := l_valSlow;
      l_prevValSig  := l_MACDSignal;
    END LOOP;
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning MACD Data.',
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
    
  SELECT JSON_AGG(ROW_TO_JSON(z))
    INTO l_json_vals  
    FROM (SELECT chv_code       "code",
                 chv_name       "name",
                 cht_chart_type "overrideChartType",
                 CASE chv_code WHEN 'MACDLINE'   THEN array_to_json(l_vals_Line)
                               WHEN 'MACDSIGNAL' THEN array_to_json(l_vals_Signal)
                               WHEN 'MACDHIST'   THEN array_to_json(l_vals_Hist) END "values"
            FROM ss_dat.chart_values
              LEFT JOIN ss_dat.chart_types ON chv_cht_id = cht_id
           WHERE chv_dstd_id = (SELECT ucs_dstd_id FROM ss_dat.user_chart_studies WHERE ucs_id = p_ucsId)
             AND chv_code IN ('MACDSIGNAL','MACDLINE','MACDHIST')) z;
  
  RETURN (l_json_vals,NULL,NULL)::ss_dat.t_studyVals_return;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;
