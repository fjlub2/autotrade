CREATE OR REPLACE FUNCTION ss_src."c_StudyData_AccDist_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE,
                                                            p_dsymId ss_dat.d_symbols.dsym_id%TYPE,
                                                            p_dtfrId ss_dat.d_timeframes.dtfr_id%TYPE,
                                                            p_from   ss_dat.user_charts.uct_from%TYPE,
                                                            p_to     ss_dat.user_charts.uct_to%TYPE)
  RETURNS ss_dat.t_studyVals_return AS
$BODY$
DECLARE
  l_json      JSON;
  l_taskId    INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Accumulation / Distrubution Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
                                          
  IF p_ucsId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
                                          
  IF p_dsymId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_dtfrId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Time Frame'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_from IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','From Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_to IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','To Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := NULL;
  ELSE          
    SELECT JSON_AGG(ROW_TO_JSON(c))
      INTO l_json  
      FROM (SELECT chv_code       "code",
                   chv_name       "name",
                   cht_chart_type "overrideChartType",
                   (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a)))
                      FROM (SELECT fdat_unix_ms    "d",
                                   SUM(ad) OVER w1 "v"
                              FROM (SELECT fdat_unix_ms,
                                           ROUND(((fdat_close - fdat_low) - (fdat_high - fdat_close)) / (fdat_high - fdat_low) * fdat_volume) ad
                                      FROM ss_dat.f_data
                                     WHERE fdat_dsym_id = p_dsymId
                                       AND fdat_date BETWEEN p_from AND p_to
                                       AND fdat_dtfr_id = p_dtfrId) b
                            WINDOW w1 AS (ORDER BY fdat_unix_ms ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)) a) "values"
              FROM ss_dat.chart_values
                LEFT JOIN ss_dat.chart_types ON chv_cht_id = cht_id
             WHERE chv_dstd_id = (SELECT ucs_dstd_id FROM ss_dat.user_chart_studies WHERE ucs_id = p_ucsId)
               AND chv_code    = 'ACCDIST') c;
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning  Accumulation / Distrubution Data.',
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN (l_json,NULL,NULL)::ss_dat.t_studyVals_return;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;
