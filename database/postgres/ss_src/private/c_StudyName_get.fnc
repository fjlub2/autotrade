CREATE OR REPLACE FUNCTION ss_src."c_StudyName_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE)
  RETURNS TEXT AS
$BODY$
DECLARE
  l_name      TEXT;
  l_ucsRow    ss_dat.user_chart_studies%ROWTYPE;
  l_dstdRow   ss_dat.d_studies%ROWTYPE;
  
  l_taskId    INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Study Name.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  l_ucsRow  := ss_tapi."t_UserChartStudies_getRow_PK"(p_pk := p_ucsId);
  
  IF COALESCE(l_ucsRow.ucs_name,'') = '' THEN
    l_dstdRow := ss_tapi."t_DStudies_getRow_PK"(p_pk := l_ucsRow.ucs_dstd_id);
                                          
    IF l_dstdRow.dstd_code IN ('EMA','RELVOL','SMA') THEN
      l_name := l_dstdRow.dstd_name || ' ('||ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'PERIOD')||' periods)';
    ELSIF l_dstdRow.dstd_code = 'BB' THEN
      l_name := l_dstdRow.dstd_name || ' ('||ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'PERIOD')||' periods, '||TO_CHAR(ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'STDDIV_PCT')::numeric / 100,'999D9')||' stddiv)';
    ELSE
      l_name := l_dstdRow.dstd_name;
    END IF;
  ELSE
    l_name := l_ucsRow.ucs_name;
  END IF;
  
  RETURN l_name;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;


