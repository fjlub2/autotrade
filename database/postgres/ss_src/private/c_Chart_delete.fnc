CREATE OR REPLACE FUNCTION ss_src."c_Chart_delete"(p_uctId ss_dat.user_charts.uct_id%TYPE) RETURNS void AS
$BODY$
DECLARE
  l_usrId   core_dat.users.usr_id%TYPE;
  l_ustId   ss_dat.user_strategies.ust_id%TYPE;

  l_taskId INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  IF p_uctId IS NOT NULL THEN
    l_ustId := ss_tapi."t_UserCharts_getUstId_PK"(p_pk := p_uctId);
    l_usrId := ss_tapi."t_UserStrategies_getUsrId_PK"(p_pk := l_ustId);
    
    IF l_usrId IS NULL OR
       l_usrId != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;  
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'N' THEN
    DELETE
      FROM ss_dat.action_items
     WHERE ai_uct_id = p_uctId;
  
    DELETE 
      FROM ss_dat.user_chart_study_params
     WHERE usp_ucs_id IN (SELECT ucs_id
                            FROM ss_dat.user_chart_studies
                           WHERE ucs_uct_id = p_uctId);
                         
    DELETE
      FROM ss_dat.user_chart_studies
     WHERE ucs_uct_id = p_uctId;
   
    PERFORM ss_tapi."t_UserStrategies_setDefaultUctId"(p_pk := l_ustId, p_val := NULL);
    PERFORM ss_tapi."t_UserCharts_deleteRow"(p_pk := p_uctId);
  END IF;
  
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;