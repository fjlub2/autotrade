CREATE OR REPLACE FUNCTION ss_src."c_ChartNav_store"(p_uctId     ss_dat.user_charts.uct_id%TYPE,
                                                     p_date      ss_dat.user_charts.uct_from%TYPE,
                                                     p_direction TEXT) RETURNS void AS
$BODY$
DECLARE
  l_ustRow         ss_dat.user_strategies%ROWTYPE;
  l_uctRow         ss_dat.user_charts%ROWTYPE;
  l_dtfrRow        ss_dat.d_timeframes%ROWTYPE;
  l_dsytRow        ss_dat.d_symbol_timeframes%ROWTYPE;
  l_from           ss_dat.user_charts.uct_from%TYPE;
  l_to             ss_dat.user_charts.uct_to%TYPE;
  l_syncFrom       ss_dat.user_charts.uct_sync_from_date%TYPE;
  l_syncTo         ss_dat.user_charts.uct_sync_to_date%TYPE;
  l_relSyncFrom    ss_dat.user_charts.uct_sync_from_date%TYPE;
  l_relSyncTo      ss_dat.user_charts.uct_sync_to_date%TYPE;
  l_updateUctRow   ss_tapi."t_UserCharts_updateRow";
  l_includeUctRow  ss_tapi."t_UserCharts_updateInclude";
  r_rec            RECORD;
  l_chpId          ss_dat.chart_periods.chp_id%TYPE;
  l_months         TEXT;
  l_window         TEXT;
  l_hours          ss_dat.chart_periods.chp_hours%TYPE;
  l_centreDate     TIMESTAMP;
  
  l_taskId         INTEGER;
  l_uctChanges     INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  IF p_uctId IS NOT NULL THEN
    l_uctRow  := ss_tapi."t_UserCharts_getRow_PK"(p_pk := p_uctId);
    l_ustRow  := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := l_uctRow.uct_ust_id);
    l_dtfrRow := ss_tapi."t_DTimeframes_getRow_PK"(p_pk := l_uctRow.uct_dtfr_id);
    l_dsytRow := ss_tapi."t_DSymbolTimeframes_getRow_PK"(p_pk := l_ustRow.ust_dsym_id, p_pk2 := l_uctRow.uct_dtfr_id);
    
    IF l_uctRow.uct_id     IS NULL OR
       l_ustRow.ust_usr_id IS NULL OR
       l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart'),
                                              p_data   := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_date      IS NULL AND
     p_direction IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Period or Direction'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  ELSE
    IF p_direction IS NOT NULL AND
       p_direction NOT IN ('F','B') THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                              p_vars   := core_src."c_Data_addTag"('FIELD','direction'),
                                              p_data   := core_src."c_Data_addTag"('DIRECTION',p_direction),
                                              p_type   := 'F');
    END IF;
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN -- don't want to continue if there were any errors with the input parameters.
    RETURN;
  END IF;
  

  l_months := l_dtfrRow.dtfr_nav_months||' months';
  l_window := l_dtfrRow.dtfr_max_months||' months';
    
  IF p_date IS NOT NULL THEN
    l_from := p_date;
  ELSE
    IF p_direction = 'F' THEN
      l_from := l_uctRow.uct_from + l_months::INTERVAL;
    ELSE
      l_from := l_uctRow.uct_from - l_months::INTERVAL;
    END IF;
  END IF;
  
  IF l_from < l_dsytRow.dsyt_min_date THEN
    l_from := l_dsytRow.dsyt_min_date;
    l_to   := l_from + l_window::INTERVAL;
  ELSIF l_from + l_window::INTERVAL > l_dsytRow.dsyt_max_date THEN
    l_to   := l_dsytRow.dsyt_max_date;
    l_from := l_to - l_window::INTERVAL;
  ELSE 
    l_to := l_from + l_window::INTERVAL;
  END IF;
  
  IF l_uctRow.uct_sync_from_date NOT BETWEEN l_from AND l_to OR 
     l_uctRow.uct_sync_to_date   NOT BETWEEN l_from AND l_to THEN
    l_hours := ss_tapi."t_ChartPeriods_getHours_PK"(p_pk := l_uctRow.uct_chp_id);
    
    IF p_date IS NOT NULL OR p_direction = 'F' THEN
      l_syncFrom := l_from;
      l_syncTo   := l_syncFrom + (l_hours||' hours')::INTERVAL;
    ELSE
      l_syncTo   := l_to;
      l_syncFrom := l_syncTo - (l_hours||' hours')::INTERVAL;
    END IF;
  ELSE
    l_syncFrom := l_uctRow.uct_sync_from_date;
    l_syncTo   := l_uctRow.uct_sync_to_date;
  END IF;
  
  l_uctChanges := 0;

  IF l_uctRow.uct_from IS NULL OR
     l_uctRow.uct_from != l_from THEN
    l_updateUctRow.uct_from  := l_from;
    l_includeUctRow.uct_from := 'Y';
    l_uctChanges             := 1;
      
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                            p_data    := core_src."c_Data_addTag"(
                                                             core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                          'OLD',l_uctRow.uct_from),
                                                                                      'NEW',l_from),
                                                                                  'FIELD','UCT_FROM'),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  IF l_uctRow.uct_to IS NULL OR
     l_uctRow.uct_to != l_to THEN
    l_updateUctRow.uct_to  := l_to;
    l_includeUctRow.uct_to := 'Y';
    l_uctChanges           := 1;
      
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                            p_data    := core_src."c_Data_addTag"(
                                                             core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                          'OLD',l_uctRow.uct_to),
                                                                                      'NEW',l_to),
                                                                                  'FIELD','UCT_TO'),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  IF l_uctRow.uct_sync_from_date IS NULL OR
     l_uctRow.uct_sync_from_date != l_syncFrom THEN
    l_updateUctRow.uct_sync_from_date  := l_syncFrom;
    l_includeUctRow.uct_sync_from_date := 'Y';
    l_uctChanges                       := 1;
      
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                            p_data    := core_src."c_Data_addTag"(
                                                             core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                          'OLD',l_uctRow.uct_sync_from_date),
                                                                                      'NEW',l_syncFrom),
                                                                                  'FIELD','UCT_SYNC_FROM_DATE'),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  IF l_uctRow.uct_sync_to_date IS NULL OR
     l_uctRow.uct_sync_to_date != l_syncTo THEN
    l_updateUctRow.uct_sync_to_date  := l_syncTo;
    l_includeUctRow.uct_sync_to_date := 'Y';
    l_uctChanges                     := 1;
      
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value.',
                                            p_data    := core_src."c_Data_addTag"(
                                                             core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                                                                          'OLD',l_uctRow.uct_sync_to_date),
                                                                                      'NEW',l_syncTo),
                                                                                  'FIELD','UCT_SYNC_TO_DATE'),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;

  IF l_uctChanges = 1 THEN
    PERFORM ss_tapi."t_UserCharts_updateRow"(p_pk := l_uctRow.uct_id, p_row := l_updateUctRow, p_include := l_includeUctRow);
                                              
    PERFORM ss_tapi."t_UserStrategies_setUpdated"(p_pk := l_uctRow.uct_ust_id, p_val := localtimestamp);
      
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Updated chart navigation.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  ELSE
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Request made to update chart navigation, but no changes identified',
                                            p_data    := core_src."c_Data_addTag"(core_src."c_Data_addTag"(core_src."c_Data_addTag"('UCT_ID',l_uctRow.uct_id),'NAV_DATE',p_date),'DIRECTION',p_direction),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);    
  END IF;

  IF l_ustRow.ust_sync_ind = 'Y' THEN
    FOR r_rec IN (SELECT uct_id,
                         uct_sync_from_date,
                         uct_sync_to_date,
                         uct_from,
                         uct_to,
                         dtfr_max_months,
                         chp_hours
                    FROM ss_dat.user_charts
                      JOIN ss_dat.d_timeframes ON dtfr_id = uct_dtfr_id
                      JOIN ss_dat.chart_periods ON chp_id = uct_chp_id
                   WHERE uct_ust_id = l_ustRow.ust_id
                     AND uct_id    != p_uctId) LOOP
      l_uctChanges                       := 0;
      l_includeUctRow.uct_from           := 'N';
      l_includeUctRow.uct_to             := 'N';
      l_includeUctRow.uct_sync_from_date := 'N';
      l_includeUctRow.uct_sync_to_date   := 'N';
      
      IF l_ustRow.ust_sync_type = 'start' THEN
        l_relSyncFrom := l_syncFrom;
        l_relSyncTo   := l_syncFrom + (r_rec.chp_hours||' hours')::INTERVAL;
      ELSIF l_ustRow.ust_sync_type = 'end' THEN
        l_relSyncTo   := l_syncTo;
        l_relSyncFrom := l_syncTo - (r_rec.chp_hours||' hours')::INTERVAL;
      ELSE -- centre
        l_centreDate := l_syncFrom + ((l_syncTo - l_syncFrom) / 2);
        
        l_relSyncTo   := l_centreDate + ((r_rec.chp_hours / 2)||' hours')::INTERVAL;
        l_relSyncFrom := l_centreDate - ((r_rec.chp_hours / 2)||' hours')::INTERVAL;
      END IF;
      
      l_relSyncFrom := GREATEST(l_relSyncFrom,l_dsytRow.dsyt_min_date);
      l_relSyncTo   := LEAST(l_relSyncTo,l_dsytRow.dsyt_max_date);
      
      IF r_rec.uct_sync_from_date IS NULL OR
         l_relSyncFrom != r_rec.uct_sync_from_date THEN
        l_updateUctRow.uct_sync_from_date  := l_relSyncFrom;
        l_includeUctRow.uct_sync_from_date := 'Y';
        l_uctChanges                       := 1;
                                                
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value on related chart.',
                                                p_data    := core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"(
                                                                         core_src."c_Data_addTag"('UCT_ID',r_rec.uct_id),
                                                                                              'OLD',r_rec.uct_sync_from_date),
                                                                                          'NEW',l_relSyncFrom),
                                                                                      'FIELD','UCT_SYNC_FROM_DATE'),
                                                p_type    := 'I',
                                                p_taskId  := l_taskId);
      END IF;
      
      IF r_rec.uct_sync_to_date IS NULL OR
         l_relSyncTo != r_rec.uct_sync_to_date THEN
        l_updateUctRow.uct_sync_to_date  := l_relSyncTo;
        l_includeUctRow.uct_sync_to_date := 'Y';
        l_uctChanges                     := 1;
                                                
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value on related chart.',
                                                p_data    := core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"(
                                                                         core_src."c_Data_addTag"('UCT_ID',r_rec.uct_id),
                                                                                              'OLD',r_rec.uct_sync_to_date),
                                                                                          'NEW',l_relSyncTo),
                                                                                      'FIELD','UCT_SYNC_TO_DATE'),
                                                p_type    := 'I',
                                                p_taskId  := l_taskId);
      END IF;
      
      IF l_relSyncFrom NOT BETWEEN r_rec.uct_from AND r_rec.uct_to OR
         l_relSyncTo   NOT BETWEEN r_rec.uct_from AND r_rec.uct_to THEN
        l_window := (r_rec.dtfr_max_months::NUMERIC / 2)||' months';
      
        IF l_relSyncFrom NOT BETWEEN r_rec.uct_from AND r_rec.uct_to THEN
          IF l_relSyncFrom + l_window::INTERVAL > l_dsytRow.dsyt_max_date THEN
            l_window := r_rec.dtfr_max_months||' months';
            l_to     := l_dsytRow.dsyt_max_date;
            l_from   := GREATEST(l_to - l_window::INTERVAL,l_dsytRow.dsyt_min_date);
          ELSIF l_relSyncFrom - l_window::INTERVAL < l_dsytRow.dsyt_min_date THEN
            l_window := r_rec.dtfr_max_months||' months';
            l_from   := l_dsytRow.dsyt_min_date;
            l_to     := LEAST(l_from + l_window::INTERVAL,l_dsytRow.dsyt_max_date);
          ELSE
            l_from   := GREATEST(l_relSyncFrom - l_window::INTERVAL,l_dsytRow.dsyt_min_date);
            l_to     := LEAST(l_relSyncFrom + l_window::INTERVAL,l_dsytRow.dsyt_max_date);
          END IF;
        END IF;
        
        IF l_relSyncTo NOT BETWEEN r_rec.uct_from AND r_rec.uct_to THEN
          IF l_relSyncTo + l_window::INTERVAL > l_dsytRow.dsyt_max_date THEN
            l_window := r_rec.dtfr_max_months||' months';
            l_to     := l_dsytRow.dsyt_max_date;
            l_from   := GREATEST(l_to - l_window::INTERVAL,l_dsytRow.dsyt_min_date);
          ELSIF l_relSyncTo - l_window::INTERVAL < l_dsytRow.dsyt_min_date THEN
            l_window := r_rec.dtfr_max_months||' months';
            l_from   := l_dsytRow.dsyt_min_date;
            l_to     := LEAST(l_from + l_window::INTERVAL,l_dsytRow.dsyt_max_date);
          ELSE
            l_from   := GREATEST(l_relSyncTo - l_window::INTERVAL,l_dsytRow.dsyt_min_date);
            l_to     := LEAST(l_relSyncTo + l_window::INTERVAL,l_dsytRow.dsyt_max_date);
          END IF;
        END IF;
      
        IF r_rec.uct_from IS NULL OR
           r_rec.uct_from != l_from THEN
          l_updateUctRow.uct_from  := l_from;
          l_includeUctRow.uct_from := 'Y';
          l_uctChanges             := 1;
      
          PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value on related chart.',
                                                  p_data    := core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"(
                                                                           core_src."c_Data_addTag"('UCT_ID',r_rec.uct_id),
                                                                                                'OLD',r_rec.uct_from),
                                                                                            'NEW',l_from),
                                                                                        'FIELD','UCT_FROM'),
                                                  p_type    := 'I',
                                                  p_taskId  := l_taskId);
        END IF;
  
        IF r_rec.uct_to IS NULL OR
           r_rec.uct_to != l_to THEN
          l_updateUctRow.uct_to  := l_to;
          l_includeUctRow.uct_to := 'Y';
          l_uctChanges           := 1;
      
          PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing chart value on related chart.',
                                                  p_data    := core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"(
                                                                           core_src."c_Data_addTag"('UCT_ID',r_rec.uct_id),
                                                                                                'OLD',r_rec.uct_to),
                                                                                            'NEW',l_to),
                                                                                        'FIELD','UCT_TO'),
                                                  p_type    := 'I',
                                                  p_taskId  := l_taskId);
        END IF;
      END IF;
      
      IF l_uctChanges = 1 THEN
        PERFORM ss_tapi."t_UserCharts_updateRow"(p_pk := r_rec.uct_id, p_row := l_updateUctRow, p_include := l_includeUctRow);
      
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Updated related chart.',
                                                p_data    := core_src."c_Data_addTag"('UCT_ID',r_rec.uct_id),
                                                p_type    := 'I',
                                                p_taskId  := l_taskId);
      END IF;
    END LOOP;
  END IF;
                 
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;