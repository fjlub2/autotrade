CREATE OR REPLACE FUNCTION ss_src."c_StudyData_All_get"(p_uctId  ss_dat.user_charts.uct_id%TYPE,
                                                        p_dsymId ss_dat.d_symbols.dsym_id%TYPE,
                                                        p_dtfrId ss_dat.d_timeframes.dtfr_id%TYPE,
                                                        p_from   ss_dat.user_charts.uct_from%TYPE,
                                                        p_to     ss_dat.user_charts.uct_to%TYPE)
  RETURNS json AS
$BODY$
DECLARE
  l_data      JSON;
  l_json      JSON;
  l_dstdRow   ss_dat.d_studies%ROWTYPE;
  l_ct        ss_dat.chart_types.cht_chart_type%TYPE;
  l_taskId    INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Preparing Chart Studies Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  IF p_dsymId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_dtfrId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Time Frame'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_from IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','From Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_to IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','To Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_uctId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := NULL;
  ELSE
    SELECT ARRAY_TO_JSON(COALESCE(ARRAY_AGG(ROW_TO_JSON(a)),'{}'))
      INTO l_json
      FROM (SELECT ss_src."c_StudyData_get"(p_ucsId  := ucs_id,
                                            p_dstdId := ucs_dstd_id,
                                            p_chtId  := ucs_cht_id,
                                            p_showOn := ucs_chart_panel,
                                            p_dsymId := p_dsymId,
                                            p_dtfrId := p_dtfrId,
                                            p_from   := p_from,
                                            p_to     := p_to,
                                            p_uctId  := p_uctId,
                                            p_name   := ucs_name) "study"
              FROM ss_dat.user_chart_studies
             WHERE ucs_uct_id = p_uctId
             ORDER BY ucs_order) a;
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Chart Studies data prepared.',
                                            p_data    := core_src."c_Data_addTag"('UCT_ID',p_uctId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN l_json;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;


