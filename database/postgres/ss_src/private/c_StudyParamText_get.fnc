CREATE OR REPLACE FUNCTION ss_src."c_StudyParamText_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE,
                                                         p_param  ss_dat.d_parameter_types.dprt_code%TYPE)
  RETURNS ss_dat.user_chart_study_params.usp_value_text%TYPE AS
$BODY$
DECLARE
  l_taskId    INTEGER;
  l_value     ss_dat.user_chart_study_params.usp_value_text%TYPE;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
    
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning study param.',
                                          p_data    := core_src."c_Data_addTag"(core_src."c_Data_addTag"('UCS_ID',p_ucsId),'DPRT_CODE',p_param),
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
  
  SELECT param_value_text
    INTO l_value
    FROM ss_src."c_StudyParams_get"(p_ucsId := p_ucsId)
   WHERE param_code = p_param;

  RETURN l_value;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;