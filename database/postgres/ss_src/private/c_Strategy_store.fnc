CREATE OR REPLACE FUNCTION ss_src."c_Strategy_store"(p_ustId    IN ss_dat.user_strategies.ust_id%TYPE,
                                                     p_name     IN ss_dat.user_strategies.ust_name%TYPE,
                                                     p_desc     IN ss_dat.user_strategies.ust_desc%TYPE,
                                                     p_position IN ss_dat.user_strategies.ust_position_code%TYPE,
                                                     p_exchange IN ss_dat.d_exchanges.dexh_code%TYPE,
                                                     p_product  IN ss_dat.d_symbol_groups.dsyg_code%TYPE,
                                                     p_contract IN ss_dat.d_symbols.dsym_code%TYPE,
                                                     p_syncInd  IN ss_dat.user_strategies.ust_sync_ind%TYPE,
                                                     p_syncType IN ss_dat.user_strategies.ust_sync_type%TYPE) RETURNS ss_dat.user_strategies.ust_id%TYPE AS
$BODY$
DECLARE
  l_dexhId         ss_dat.d_exchanges.dexh_id%TYPE;
  l_dsygId         ss_dat.d_symbol_groups.dsyg_id%TYPE;
  l_dsygId2        ss_dat.d_symbol_groups.dsyg_id%TYPE;
  l_dsymId         ss_dat.d_symbols.dsym_id%TYPE;
  
  l_ustRow         ss_dat.user_strategies%ROWTYPE;
  l_updateUstRow   ss_tapi."t_UserStrategies_updateRow";
  l_includeUstRow  ss_tapi."t_UserStrategies_updateInclude";
  l_ustId          ss_dat.user_strategies.ust_id%TYPE;
  l_syncType       ss_dat.user_strategies.ust_sync_type%TYPE;
  r_rec            RECORD;
  
  l_taskId         INTEGER;
  l_ustChanges     INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  IF p_ustId IS NOT NULL THEN
    l_ustRow := ss_tapi."t_UserStrategies_getRow_PK"(p_pk := p_ustId);
    
    IF l_ustRow.ust_usr_id IS NULL OR
       l_ustRow.ust_usr_id != core_src."c_Session_getCurrentUsrId"() THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code    := 'T0004', -- strategy doesnt exist
                                              p_vars    := core_src."c_Data_addTag"('FIELD','strategy'),
                                              p_data    := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                              p_type    := 'E',
                                              p_taskId  := l_taskId);
    ELSIF l_ustRow.ust_live = 'Y' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code    := 'T0010', -- cant edit a live strategy
                                              p_data    := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                              p_type    := 'E',
                                              p_taskId  := l_taskId);
    END IF;
  END IF;
  
  IF p_position IS NOT NULL THEN
    IF core_tapi."t_CodeValues_exists_PK"(p_pk := 'positions', p_pk2 := p_position) = 'N' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','position'),
                                              p_data   := core_src."c_Data_addTag"('POSITION_CODE',p_position),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  END IF;

  IF p_exchange IS NOT NULL THEN
    l_dexhId := ss_tapi."t_DExchanges_getId_UK"(p_uk := p_exchange);
    
    IF l_dexhId IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','exchange'),
                                              p_data   := core_src."c_Data_addTag"('EXCHANGE_CODE',p_exchange),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSIF p_ustId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Exchange'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF l_dexhId IS NOT NULL THEN
    IF COALESCE(p_contract,'') != '' THEN
      l_dsymId := ss_tapi."t_DSymbols_getId_UK"(p_uk  := p_contract,
                                                p_uk2 := l_dexhId);
    
      IF l_dsymId IS NULL THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                                p_vars   := core_src."c_Data_addTag"('FIELD','contract'),
                                                p_data   := core_src."c_Data_addTag"('CONTRACT_CODE',p_contract),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
      END IF;
    ELSE
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                              p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;

    IF COALESCE(p_product,'') != '' THEN
      l_dsygId := ss_tapi."t_DSymbolGroups_getId_UK"(p_uk := p_product);
     
      IF l_dsygId IS NULL THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- product doesnt exist
                                                p_vars   := core_src."c_Data_addTag"('FIELD','product'),
                                                p_data   := core_src."c_Data_addTag"('PRODUCT_CODE',p_product),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
      END IF;
    ELSE
      l_dsygId := NULL;
    END IF;
    
    IF l_dsymId IS NOT NULL AND 
       l_dsygId IS NOT NULL THEN
      l_dsygId2 := ss_tapi."t_DSymbols_getDsygId_PK" (p_pk := l_dsymId);
  
      IF l_dsygId2 IS NULL OR
         l_dsygId != l_dsygId2 THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                                p_vars   := core_src."c_Data_addTag"('FIELD','product'),
                                                p_data   := core_src."c_Data_addTag"(core_src."c_Data_addTag"('DSYG_ID',l_dsygId),'DSYM_ID',l_dsymId),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
      END IF;
    END IF;
    
    IF COALESCE(p_syncInd,'') = '' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                              p_vars   := core_src."c_Data_addTag"('FIELD','Synchronise Ind'),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    ELSIF p_syncInd NOT IN ('Y','N') THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                              p_vars   := core_src."c_Data_addTag"('FIELD','synchronise ind'),
                                              p_data   := core_src."c_Data_addTag"('UST_SYNC_IND',p_syncInd),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    ELSIF p_syncInd = 'Y' AND COALESCE(p_syncType,'') = '' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                              p_vars   := core_src."c_Data_addTag"('FIELD','Synchronise Type'),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
    
    IF COALESCE(p_syncType,'') = '' THEN
      l_syncType := 'end';
    ELSE
      l_syncType := p_syncType;
    END IF;
    
    IF core_tapi."t_CodeValues_exists_PK" (p_pk  := 'chart_sync_types',
                                           p_pk2 := l_syncType) = 'N' THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0009', -- not a valid option
                                              p_vars   := core_src."c_Data_addTag"('FIELD','synchronise type'),
                                              p_data   := core_src."c_Data_addTag"('UST_SYNC_TYPE',l_syncType),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
  ELSE
    l_dsymId := NULL;
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN -- don't want to continue if there were any errors with the input parameters.
    RETURN NULL;
  END IF;

  IF p_ustId IS NULL THEN
    l_ustRow.ust_usr_id        := core_src."c_Session_getCurrentUsrId"();
    l_ustRow.ust_updated       := NOW();
    l_ustRow.ust_name          := p_name;
    l_ustRow.ust_desc          := p_desc;
    l_ustRow.ust_position_code := p_position;
    l_ustRow.ust_dsym_id       := l_dsymId;
    l_ustRow.ust_live          := 'N';
    l_ustRow.ust_sync_ind      := p_syncInd;
    l_ustRow.ust_sync_type     := l_syncType;
  
    l_ustId := ss_tapi."t_UserStrategies_insertRow" (p_row := l_ustRow);
    
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Stored new strategy.',
                                            p_data    := core_src."c_Data_addTag"('UST_ID',l_ustId),
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  ELSE
    l_ustChanges := 0;
    
    IF p_name IS NOT NULL THEN
      IF p_name != l_ustRow.ust_name OR
         l_ustRow.ust_name IS NULL THEN
        l_updateUstRow.ust_name  := p_name;
        l_includeUstRow.ust_name := 'Y';
        l_ustChanges             := 1;
        
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing strategy value.',
                                                p_data    := core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"(
                                                                         core_src."c_Data_addTag"('UST_ID',l_ustRow.ust_id),
                                                                                              'OLD',l_ustRow.ust_name),
                                                                                          'NEW',p_name),
                                                                                      'FIELD','UST_NAME'),
                                                p_type    := 'I',
                                                p_taskId  := l_taskId);
      END IF;
    END IF;
    
    IF p_desc IS NOT NULL THEN
      IF p_desc != l_ustRow.ust_desc OR
         l_ustRow.ust_desc IS NULL THEN
        l_updateUstRow.ust_desc  := p_desc;
        l_includeUstRow.ust_desc := 'Y';
        l_ustChanges             := 1;
      
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing strategy value.',
                                                p_data    := core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"(
                                                                         core_src."c_Data_addTag"('UST_ID',l_ustRow.ust_id),
                                                                                              'OLD',l_ustRow.ust_desc),
                                                                                          'NEW',p_desc),
                                                                                      'FIELD','UST_DESC'),
                                                p_type    := 'I',
                                                p_taskId  := l_taskId);
      END IF;
    END IF;

    IF p_position IS NOT NULL THEN
      IF l_ustRow.ust_position_code != p_position THEN
        l_updateUstRow.ust_position_code  := p_position;
        l_includeUstRow.ust_position_code := 'Y';
        l_ustChanges                      := 1;
      
        PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing strategy value.',
                                                p_data    := core_src."c_Data_addTag"(
                                                                 core_src."c_Data_addTag"(
                                                                     core_src."c_Data_addTag"(
                                                                         core_src."c_Data_addTag"('UST_ID',l_ustRow.ust_id),
                                                                                              'OLD',l_ustRow.ust_position_code),
                                                                                          'NEW',p_position),
                                                                                      'FIELD','UST_POSITION_CODE'),
                                                p_type    := 'I',
                                                p_taskId  := l_taskId);
      END IF;
    END IF;    

    IF l_dsymId IS NOT NULL AND
       l_ustRow.ust_dsym_id != l_dsymId THEN
      l_updateUstRow.ust_dsym_id  := l_dsymId;
      l_includeUstRow.ust_dsym_id := 'Y';
      l_ustChanges                := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing strategy value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UST_ID',l_ustRow.ust_id),
                                                                                            'OLD',l_ustRow.ust_dsym_id),
                                                                                        'NEW',l_dsymId),
                                                                                    'FIELD','UST_DSYM_ID'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
    
    IF l_ustRow.ust_sync_ind != p_syncInd THEN
      l_updateUstRow.ust_sync_ind  := p_syncInd;
      l_includeUstRow.ust_sync_ind := 'Y';
      l_ustChanges                 := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing strategy value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UST_ID',l_ustRow.ust_id),
                                                                                            'OLD',l_ustRow.ust_sync_ind),
                                                                                        'NEW',p_syncInd),
                                                                                    'FIELD','UST_SYNC_IND'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;
    
    IF l_ustRow.ust_sync_type != l_syncType THEN
      l_updateUstRow.ust_sync_type  := l_syncType;
      l_includeUstRow.ust_sync_type := 'Y';
      l_ustChanges                 := 1;
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Changing strategy value.',
                                              p_data    := core_src."c_Data_addTag"(
                                                               core_src."c_Data_addTag"(
                                                                   core_src."c_Data_addTag"(
                                                                       core_src."c_Data_addTag"('UST_ID',l_ustRow.ust_id),
                                                                                            'OLD',l_ustRow.ust_sync_type),
                                                                                        'NEW',l_syncType),
                                                                                    'FIELD','UST_SYNC_TYPE'),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
    END IF;

    IF l_ustChanges = 1 THEN
      l_updateUstRow.ust_updated  := NOW();
      l_includeUstRow.ust_updated := 'Y';

      PERFORM ss_tapi."t_UserStrategies_updateRow"(p_pk := p_ustId, p_row := l_updateUstRow, p_include := l_includeUstRow);
      
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Updated strategy.',
                                              p_data    := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);
                                              
      IF l_dsymId IS NOT NULL AND
         l_ustRow.ust_dsym_id != l_dsymId THEN
        FOR r_rec IN (SELECT uct_id,
                             dsyt_max_date - (chp_hours||' hours')::INTERVAL sync_from,
                             dsyt_max_date sync_to,
                             GREATEST(dsyt_max_date - (dtfr_max_months||' months')::INTERVAL, dsyt_min_date) from_date,
                             dsyt_max_date to_date
                        FROM ss_dat.user_charts
                          JOIN ss_dat.d_timeframes        ON dtfr_id = uct_dtfr_id
                          JOIN ss_dat.d_symbol_timeframes ON dtfr_id = dsyt_dtfr_id AND dsyt_dsym_id = l_dsymId
                          JOIN ss_dat.chart_periods       ON chp_id  = uct_chp_id
                       WHERE uct_ust_id = p_ustId) LOOP
          UPDATE ss_dat.user_charts
             SET uct_area_from      = NULL,
                 uct_area_to        = NULL,
                 uct_sync_from_date = r_rec.sync_from,
                 uct_sync_to_date   = r_rec.sync_to,
                 uct_from           = r_rec.from_date,
                 uct_to             = r_rec.to_date
           WHERE uct_ust_id = p_ustId;
         
          PERFORM core_src."c_Message_logMsgText"(p_message := 'Cleared setup areas and updated sync dates',
                                                  p_data    := core_src."c_Data_addTag"('UCT_ID',r_rec.uct_id),
                                                  p_type    := 'I',
                                                  p_taskId  := l_taskId); 
        END LOOP;
      END IF;
    ELSE
      PERFORM core_src."c_Message_logMsgText"(p_message := 'Request made to update strategy options, but no changes identified',
                                              p_data    := core_src."c_Data_addTag"('UST_ID',p_ustId),
                                              p_type    := 'I',
                                              p_taskId  := l_taskId);    
    END IF;
    
    l_ustId := p_ustId;
  END IF;
                 
  RETURN l_ustId;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;