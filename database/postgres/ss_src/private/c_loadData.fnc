CREATE OR REPLACE FUNCTION ss_src."c_loadData"(p_dsymId ss_dat.d_symbols.dsym_id%TYPE,
                                               p_dsupId ss_dat.d_suppliers.dsup_id%TYPE,
                                               p_file   TEXT)
  RETURNS text AS
$BODY$
DECLARE
  l_sql           TEXT;
  l_minDate       DATE;
  l_maxDate       DATE;
--l_noPeriods     NUMERIC(15,10);
  l_noDays        INTEGER;
  l_dataDate      TIMESTAMP;
  l_secsInPeriod  INTEGER;
  l_periodsInDay  NUMERIC(15,10);
  r_mins          RECORD;
BEGIN
  IF p_file IS NOT NULL THEN
    TRUNCATE TABLE ss_dat.temp_intraday;

    l_sql := 'COPY ss_dat.temp_intraday(tid_date, tid_time, tid_open, tid_high, tid_low, tid_last, tid_volume)'--, tid_trades, tid_bid, tid_ask)
           ||'FROM '''||p_file||''''
           ||'WITH DELIMITER '','''
           ||'CSV HEADER';
            
    EXECUTE l_sql;

    UPDATE ss_dat.temp_intraday SET tid_date_time_utc = tid_date + tid_time;
  END IF;
  
  DELETE 
    FROM ss_dat.f_study_ema
   WHERE fsem_dsym_id = p_dsymId;
  
  DELETE 
    FROM ss_dat.f_data
   WHERE fdat_dsym_id = p_dsymId;
  
  -- loop around the timeframes for this symbol
  FOR r_mins IN (SELECT dtfr_id,
                        dtfr_mins
                   FROM ss_dat.d_symbol_timeframes
                     JOIN ss_dat.d_timeframes ON dtfr_id = dsyt_dtfr_id
                  WHERE dsyt_dsym_id = p_dsymId) LOOP
    WITH 
      range AS (SELECT MIN(tid_date) min_date, MAX(tid_date) + 1 max_date
                  FROM ss_dat.temp_intraday)
    SELECT min_date,
           max_date,
         --((max_date - min_date) * 1440::NUMERIC / r_mins.dtfr_mins) noPeriods,
           (max_date - min_date) + 1 noDays,
           r_mins.dtfr_mins * 60,
           1440::NUMERIC / r_mins.dtfr_mins
      INTO l_minDate,
           l_maxDate,
         --l_noPeriods,
           l_noDays,
           l_secsInPeriod,
           l_periodsInDay
      FROM range;
      
    -- populate base dates
    INSERT INTO ss_dat.d_dates (ddat_date,
                                ddat_day,
                                ddat_week,
                                ddat_month,
                                ddat_quarter,
                                ddat_year,
                                ddat_we,
                                ddat_wd)
                                WITH
                                  RECURSIVE t(n) AS (VALUES (1)
              	                                     UNION ALL
              	                                     SELECT n + 1 FROM t WHERE n < l_noDays)
                                SELECT theDate,
                                       TO_CHAR(theDate,'ID')::SMALLINT,
                                       TO_CHAR(theDate,'WW')::SMALLINT,
                                       TO_CHAR(theDate,'MM')::SMALLINT,
                                       TO_CHAR(theDate,'Q')::SMALLINT,
                                       TO_CHAR(theDate,'YYYY')::SMALLINT,
                                       CASE WHEN TO_CHAR(theDate,'ID') IN ('6','7') THEN 'Y' ELSE 'N' END,
                                       CASE WHEN TO_CHAR(theDate,'ID') IN ('6','7') THEN 'N' ELSE 'Y' END
                                  FROM (SELECT l_minDate + n - 1 theDate
                                          FROM t) a
                                 WHERE NOT EXISTS (SELECT 1
                                                     FROM ss_dat.d_dates
                                                    WHERE ddat_date = theDate);
                    
    l_dataDate := LOCALTIMESTAMP;
    
    -- populate main data
    INSERT INTO ss_dat.f_data (fdat_id,
                               fdat_dsym_id,
                               fdat_ddat_id,
                               fdat_dtfr_id,
                               fdat_date,
                               fdat_open,
                               fdat_high,
                               fdat_low,
                               fdat_close,
                               fdat_volume,
                               fdat_trades,
                               fdat_bid,
                               fdat_ask,
                               fdat_unix_ms,
                               fdat_dsup_id,
                               fdat_data_date,
                               fdat_json)
                        SELECT NEXTVAL('ss_dat.fdat_seq')::INTEGER,
                               p_dsymId,
                               (SELECT ddat_id FROM ss_dat.d_dates WHERE ddat_date = tid_date),
                               r_mins.dtfr_id,
                               period_date_time,
                               open,
                               high,
                               low,
                               close,
                               volume,
                               trades,
                               bid,
                               ask,
                               unix_ms,
                               p_dsupId,
                               l_dataDate,
                               (SELECT ROW_TO_JSON(x) "items"
			          FROM (SELECT unix_ms "d",
			                       open "o",
			                       high "h",
			                       low "l",
                                               close "c",
                                               volume "v") x)
                          FROM (SELECT FIRST_VALUE(tid_open) OVER w2 AS open,
                                       LAST_VALUE(tid_last)  OVER w2 AS close,
                                       MAX(tid_high)         OVER w1 AS high,
                                       MIN(tid_low)          OVER w1 AS low,
                                       SUM(tid_volume)       OVER w1 AS volume,
                                       SUM(tid_trades)       OVER w1 AS trades,
                                       SUM(tid_bid)          OVER w1 AS bid,
                                       SUM(tid_ask)          OVER w1 AS ask,
                                       ROW_NUMBER()          OVER w1 AS keep_one,
                                       period_date_time,
                                       tid_date,
                                       EXTRACT(EPOCH FROM period_date_time) * 1000 unix_ms
                                  FROM (SELECT tid_date + (FLOOR(TO_CHAR(tid_date_time_utc,'SSSS')::NUMERIC / l_secsInPeriod) / l_periodsInDay) * '1 day'::INTERVAL period_date_time,
                                               tid_open,
                                               tid_high,
                                               tid_low,
                                               tid_last,
                                               tid_volume,
                                               tid_trades,
                                               tid_bid,
                                               tid_ask,
                                               tid_date_time_utc,
                                               tid_date
                                          FROM ss_dat.temp_intraday) a
                                WINDOW
                                  w1 AS (PARTITION BY period_date_time),
                                  w2 AS (PARTITION BY period_date_time ORDER BY tid_date_time_utc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)) b
                         WHERE keep_one = 1;
  END LOOP;
  
  -- update the new min and max dates for the symbol  
  WITH dates AS (SELECT fdat_dtfr_id,
                        MIN(fdat_date) min_date,
                        MAX(fdat_date) max_date
                   FROM ss_dat.f_data
                  WHERE fdat_dsym_id = p_dsymId
                  GROUP BY fdat_dtfr_id)
  UPDATE ss_dat.d_symbol_timeframes
     SET dsyt_min_date = (SELECT min_date FROM dates WHERE fdat_dtfr_id = dsyt_dtfr_id),
         dsyt_max_date = (SELECT max_date FROM dates WHERE fdat_dtfr_id = dsyt_dtfr_id)
   WHERE dsyt_dsym_id = p_dsymId;
   
  UPDATE ss_dat.d_symbols
     SET dsym_min_date = (SELECT MIN(fdat_date) FROM ss_dat.f_data WHERE fdat_dsym_id = p_dsymId),
         dsym_max_date = (SELECT MAX(fdat_date) FROM ss_dat.f_data WHERE fdat_dsym_id = p_dsymId)
   WHERE dsym_id = p_dsymId;
  
  RETURN 'Successful';
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
  
  