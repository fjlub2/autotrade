CREATE OR REPLACE FUNCTION ss_src."c_StudyOptions_get"(p_dstdId ss_dat.d_studies.dstd_id%TYPE,
                                                       p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE,
                                                       p_uctId  ss_dat.user_charts.uct_id%TYPE)
  RETURNS TEXT AS
$BODY$
DECLARE
  l_html       TEXT;
  l_ucsRow     ss_dat.user_chart_studies%ROWTYPE;
  l_dstdRow    ss_dat.d_studies%ROWTYPE;
  l_panels     TEXT;
  l_chartTypes TEXT;
  l_select     TEXT;
  l_values     TEXT;
  l_submit     TEXT;
  l_a          TEXT := '';
  l_b          TEXT := '';
  l_c          TEXT := '';
  l_d          TEXT := '';
  l_count      INTEGER;
  l_ct         ss_dat.chart_types.cht_code%TYPE;
  l_dprvId     ss_dat.d_parameter_values.dprv_id%TYPE;
  l_param_text TEXT;
  l_param_int  INTEGER;
  r_params     RECORD;
  l_colour     ss_dat.user_chart_studies.ucs_colour%TYPE;
  
  l_taskId    INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Study Options.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);

  IF p_ucsId IS NOT NULL THEN
    l_ucsRow  := ss_tapi."t_UserChartStudies_getRow_PK"(p_pk := p_ucsId);
    
    IF l_ucsRow.ucs_id IS NULL THEN
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                              p_vars   := core_src."c_Data_addTag"('FIELD','chart study'),
                                              p_data   := core_src."c_Data_addTag"('UCS_ID',p_ucsId),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    ELSE
      l_ct      := ss_tapi."t_ChartTypes_getCode_PK"(p_pk := l_ucsRow.ucs_cht_id);
      l_dstdRow := ss_tapi."t_DStudies_getRow_PK"(p_pk := l_ucsRow.ucs_dstd_id);
      l_colour  := l_ucsRow.ucs_colour;
    END IF;
  ELSE
    IF p_dstdId IS NOT NULL THEN
      l_dstdRow := ss_tapi."t_DStudies_getRow_PK"(p_pk := p_dstdId);
      
      IF l_dstdRow.dstd_id IS NULL THEN
        PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0004', -- value doesnt exist
                                                p_vars   := core_src."c_Data_addTag"('FIELD','study'),
                                                p_data   := core_src."c_Data_addTag"('DSTD_ID',p_dstdId),
                                                p_type   := 'E',
                                                p_taskId := l_taskId);
      END IF;
    ELSE
      PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                              p_vars   := core_src."c_Data_addTag"('FIELD','Study'),
                                              p_type   := 'E',
                                              p_taskId := l_taskId);
    END IF;
    
    l_colour  := '#92CDDC';
  END IF;
  
  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_html := NULL;
  ELSE 
    SELECT STRING_AGG(a.html,''),
           COUNT(1)
      INTO l_chartTypes,
           l_count
      FROM (SELECT '<option value = "'||cht_code||'"'||CASE WHEN p_ucsId IS NULL THEN CASE ROW_NUMBER() OVER (ORDER BY sct_order) WHEN 1 THEN ' selected' ELSE '' END
                                                            ELSE CASE cht_code WHEN l_ct THEN ' selected' ELSE '' END
                                                       END||'>'||cht_name||'</option>' html
              FROM ss_dat.study_chart_types
                JOIN ss_dat.chart_types ON sct_cht_id = cht_id
             WHERE sct_dstd_id = l_dstdRow.dstd_id
             ORDER BY sct_order) a;
           
    IF l_count = 0 THEN
      NULL; --hide option or make it text only?
    END IF;
 
    SELECT STRING_AGG(a.html,''),
           COUNT(1)
      INTO l_panels,
           l_count
      FROM (SELECT '<option value = "'||cv_code||'"'||CASE WHEN p_ucsId IS NULL THEN CASE ROW_NUMBER() OVER (ORDER BY scp_order) WHEN 1 THEN ' selected' ELSE '' END
                                                           ELSE CASE cv_code WHEN l_ucsRow.ucs_chart_panel THEN ' selected' ELSE '' END
                                                      END||'>'||cv_name||'</option>' html
              FROM ss_dat.study_chart_panels
                JOIN core_dat.chart_panels_mv ON scp_chart_panel = cv_code
             WHERE scp_dstd_id = l_dstdRow.dstd_id
             ORDER BY scp_order) a;
           
    IF l_count = 0 THEN
      NULL; --hide option or make it text only?
    END IF;
    
    l_html := '<h3>Display Options</h3><hr><br><table width="87%" style="margin-left:18px; margin-right:18px"><tr><td><label>Display Area</label></td></tr></table><div class="uiLightness"><table width="87%" style="margin-left:18px; margin-right:18px"><tr><td><select id="DispArea" style="width:216px">'||l_panels||'</select></td><td><input type="hidden" id="StudyColor" value="'||l_colour||'" /></td></tr></table></div><table width="87%" style="margin-left:18px; margin-right:18px"><tr><td><label>Chart Type</label></td></tr><tr><td><select id="ChartType" style="width:250px">'||l_chartTypes||'</select></td></tr><tr><td><label>Study Name</label></td></tr><tr><td><input type="text" id="StudyName" maxlength="30" value="'||COALESCE(l_ucsRow.ucs_name,'')||'" style="width:250px"></td></tr></table><br>';

    l_select := '<h3>'||l_dstdRow.dstd_name||' Settings</h3><hr><table style="margin-top:15px; margin-left:18px">';

    l_count := 0;
    
    FOR r_params IN (SELECT dprt_id,
                            dprt_code,
                            dprt_name,
                            dprt_datatype,
                            dstp_check_unique,
                            dprt_freetext
                       FROM ss_dat.d_study_parameters
                         JOIN ss_dat.d_parameter_types ON dprt_id = dstp_dprt_id
                      WHERE dstp_dstd_id = l_dstdRow.dstd_id 
                      ORDER BY dstp_order) LOOP
      l_count := 1;
      
      l_select := l_select||'<tr><td><label>'||r_params.dprt_name||'</label></td></tr><tr><td>';
      
      IF r_params.dprt_freetext = 'N' THEN
        l_select := l_select||'<select id="'||r_params.dprt_code||'ComboBox">';
      ELSE
        l_select := l_select||'<input list="'||r_params.dprt_code||'" name="'||r_params.dprt_code||'ComboBox" id="'||r_params.dprt_code||'ComboBox" value="<<defaultval>>"><datalist id="'||r_params.dprt_code||'">';
      END IF;
    
      IF p_ucsId IS NOT NULL THEN
        IF r_params.dprt_datatype = 'integer' THEN
          l_param_int := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := r_params.dprt_code);
        ELSE
          l_param_text := ss_src."c_StudyParamText_get"(p_ucsId := p_ucsId, p_param := r_params.dprt_code);
        END IF;
      
        SELECT dprv_id
          INTO l_dprvId
          FROM ss_dat.d_parameter_values
         WHERE dprv_dprt_id   = r_params.dprt_id
           AND ((r_params.dprt_datatype = 'integer' AND 
                 dprv_value_int = l_param_int) OR
                (r_params.dprt_datatype = 'text' AND 
                 dprv_value_text = l_param_text));
      ELSE
        l_dprvId := NULL;
      END IF;
    
      SELECT STRING_AGG(a.html,'')
        INTO l_values
        FROM (SELECT CASE r_params.dprt_freetext WHEN 'N' THEN '<option value="'||CASE r_params.dprt_datatype WHEN 'integer' THEN dprv_value_int::text ELSE dprv_value_text END||'" '||CASE WHEN p_ucsId IS NULL THEN CASE ROW_NUMBER() OVER (ORDER BY dprv_order) WHEN 1 THEN 'selected' ELSE '' END
                                                                                                                                                                                            ELSE CASE l_dprvId WHEN dprv_id THEN 'selected' ELSE '' END
                                                                                                                                                                                       END||'>'||dprv_name||'</option>'
                                                 ELSE '<option value="'||CASE r_params.dprt_datatype WHEN 'integer' THEN dprv_value_int::text ELSE dprv_value_text END||'">'||dprv_name||'</option>'
                                                 END html
                FROM ss_dat.d_parameter_values
               WHERE dprv_dprt_id = r_params.dprt_id
                 AND COALESCE(dprv_value_text,
                              dprv_value_int::TEXT) NOT IN (SELECT COALESCE(usp_value_text, usp_value_int::TEXT)
                                                              FROM ss_dat.user_chart_study_params
                                                             WHERE usp_dprt_id = dprv_dprt_id
                                                               AND r_params.dstp_check_unique = 'Y'
                                                               AND usp_ucs_id IN (SELECT ucs_id
                                                                                    FROM ss_dat.user_chart_studies
                                                                                   WHERE ucs_uct_id  = p_uctId
                                                                                     AND ucs_dstd_id = l_dstdRow.dstd_id
                                                                                     AND (p_ucsId IS NULL OR
                                                                                          ucs_id != p_ucsId)))
               ORDER BY dprv_order) a;
      
      l_select := l_select||l_values;
      
      IF r_params.dprt_freetext = 'N' THEN
        l_select := l_select||'</select>';
      ELSE
        l_select := l_select||'</datalist>';
        
        IF r_params.dprt_datatype = 'integer' AND l_param_int IS NOT NULL THEN
          l_select := REPLACE(l_select,'<<defaultval>>',l_param_int::TEXT);
        ELSIF r_params.dprt_datatype = 'text' AND l_param_text IS NOT NULL THEN
          l_select := REPLACE(l_select,'<<defaultval>>',l_param_text);
        ELSE
          l_select := REPLACE(l_select,'<<defaultval>>','');
        END IF;
      END IF;
      
      l_select := l_select||'</td></tr>';
      
      IF r_params.dprt_freetext = 'N' THEN
        l_a := l_a||' $("#'||r_params.dprt_code||'ComboBox").scombobox({fullMatch: true});';
        l_b := l_b||' var '||r_params.dprt_code||' = $("#'||r_params.dprt_code||'ComboBox option:selected").val();';
      ELSE
        l_b := l_b||' var '||r_params.dprt_code||' = $("#'||r_params.dprt_code||'ComboBox").val();';
      END IF;
  
      l_c := l_c||'+"&'||r_params.dprt_code||'="+'||r_params.dprt_code;
    END LOOP;
    
    IF l_ucsRow.ucs_id IS NULL THEN
      l_d := '$("#ASMHomeTITLE").attr("name");';
    ELSE 
      l_d := '$("#studies").val();';
    END IF;
    
    IF l_count = 1 THEN
      l_html := l_html||' '||l_select||'</table>';
    END IF;
    
    l_html := l_html||
      '<table class="add_studies_menu_btn">'||
        '<tr>'||
          '<td>'||
            '<button class="SubmitBtn" type="button" id="save_studies_btn" onclick="save_study_btn_click()">Save</button>'||
          '</td>'||
          '<td align="right">'||
            '<button class="SubmitBtn" type="button" id="cancel_studies_btn" onclick="cancel_studies_btn_click()">Cancel</button>'||
          '</td>'||
        '</tr>'||
      '</table>'||
      '<script type="text/javascript">'||l_a||' $("#StudyColor").colorpicker({showOn: "button"}); var inputs = document.querySelectorAll(''input[list]''); for (var i = 0; i < inputs.length; i++) {inputs[i].addEventListener(''focusout'', function() {var v_value = $(this).val(); var pattern = /^[0-9]+$/; if(!v_value.match(pattern)){alert(''Integer Only'');$( this ).focus();}});} function save_study_btn_click() {$("#add_studies_menu_busy").css("visibility", "visible"); $("#studies_menu_busy").css("visibility", "visible"); var StudyId = $("#study_id").val(); var ChartId = $("#chart_id").val(); var Study = '||l_d||' var DispArea = $("#DispArea").val(); var ChartType = $("#ChartType").val(); var StudyName = $("#StudyName").val(); var StudyColor = $("#StudyColor").val(); '||l_b||' $.ajax({type: "POST",url: "./save_study",data: "chart_id="+ChartId+"&StudyId="+StudyId+"&study="+Study+"&display_area="+DispArea+"&chart_type="+ChartType+"&study_name="+StudyName+"&StudyColor="+StudyColor'||l_c||',dataType: "json"}).done(function(data) {if (data.error_code != 200){alert(data.error_msg); $("#error_code").val(data.error_code); $("#add_studies_menu_busy").css("visibility", "hidden"); $("#studies_menu_busy").css("visibility", "hidden"); $("#study_id").val(''''); return true;} else {$("#studies_menu_detail").children().remove(); $("#studies_menu_detail").append(data.Used_studies); $("#add_studies_menu_busy").css("visibility", "hidden"); $("#studies_menu_busy").css("visibility", "hidden"); $("#study_id").val(''''); var unblock_page = "Y";menu_drop_item(unblock_page);var StudyId = data.StudyId;get_study_data(StudyId);}});}</script>';
  END IF;
  
  RETURN l_html;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;