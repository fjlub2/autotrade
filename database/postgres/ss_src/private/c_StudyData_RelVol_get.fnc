CREATE OR REPLACE FUNCTION ss_src."c_StudyData_RelVol_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE,
                                                           p_dsymId ss_dat.d_symbols.dsym_id%TYPE,
                                                           p_dtfrId ss_dat.d_timeframes.dtfr_id%TYPE,
                                                           p_from   ss_dat.user_charts.uct_from%TYPE,
                                                           p_to     ss_dat.user_charts.uct_to%TYPE)
  RETURNS ss_dat.t_studyVals_return AS
$BODY$
DECLARE
  l_period    INTEGER;
  l_stddiv    NUMERIC(12,2);
  l_preFrom   ss_dat.f_data.fdat_date%TYPE;
  l_json      JSON;
  l_taskId    INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Getting Relative Volume Data.',
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
                                          
  IF p_ucsId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Chart Study'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
                                          
  IF p_dsymId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Contract'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_dtfrId IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','Time Frame'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_from IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','From Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  IF p_to IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0002', -- value not supplied
                                            p_vars   := core_src."c_Data_addTag"('FIELD','To Date'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_period := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'PERIOD');
  
  IF l_period IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('PERIOD','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;
  
  l_stddiv := ss_src."c_StudyParamInt_get"(p_ucsId := p_ucsId, p_param := 'STDDIV_PCT')::numeric / 100;
  
  IF l_stddiv IS NULL THEN
    PERFORM core_src."c_Message_logMsgCode"(p_code   := 'T0012', -- study options not complete
                                            p_data   := core_src."c_Data_addTag"('STDDIV_PCT','Not Found'),
                                            p_type   := 'E',
                                            p_taskId := l_taskId);
  END IF;

  IF core_src."c_Message_errorsExist" (p_taskId := l_taskId) = 'Y' THEN
    l_json := NULL;
  ELSE       
    SELECT MIN(fdat_date)
      INTO l_preFrom
      FROM (SELECT fdat_date
              FROM ss_dat.f_data
             WHERE fdat_dsym_id = p_dsymId
               AND fdat_dtfr_id = p_dtfrId
               AND fdat_date < p_from
             ORDER BY fdat_date DESC
             LIMIT l_period) a;

    l_preFrom := COALESCE(l_preFrom,p_from);
             
    SELECT JSON_AGG(ROW_TO_JSON(z))
      INTO l_json  
      FROM (SELECT chv_code       "code",
                   chv_name       "name",
                   cht_chart_type "overrideChartType",
                   (SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(a)))
                      FROM (SELECT fdat_unix_ms "d",
                                   CASE WHEN avgRow >= l_period THEN ROUND(avg_vol,2) ELSE NULL END "v",
                                   CASE WHEN avgRow >= l_period THEN GREATEST(ROUND(avg_vol - (stddev_vol * l_stddiv) / 2,2),0) ELSE NULL END  "bf",
                                   CASE WHEN avgRow >= l_period THEN ROUND(avg_vol + (stddev_vol * l_stddiv) / 2,2) ELSE NULL END  "bt"
                              FROM (SELECT fdat_date,
                                           fdat_unix_ms,
                                           fdat_volume,
                                           AVG(fdat_volume) OVER w1 avg_vol,
                                           STDDEV(fdat_volume) OVER w1 stddev_vol,
                                           ROW_NUMBER() OVER w1 avgRow
                                      FROM ss_dat.f_data
                                     WHERE fdat_dsym_id = p_dsymId
                                       AND fdat_date BETWEEN l_preFrom AND p_to
                                       AND fdat_dtfr_id = p_dtfrId
                                    WINDOW w1 AS (ORDER BY fdat_date ROWS BETWEEN l_period PRECEDING AND CURRENT ROW)) b
                             WHERE fdat_date BETWEEN p_from AND p_to
                             ORDER BY fdat_date) a) "values"
              FROM ss_dat.chart_values
                LEFT JOIN ss_dat.chart_types ON chv_cht_id = cht_id
             WHERE chv_dstd_id = (SELECT ucs_dstd_id FROM ss_dat.user_chart_studies WHERE ucs_id = p_ucsId)
               AND chv_code = 'RELVOL') z;
                   
    PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning Study Volume Data.',
                                            p_type    := 'I',
                                            p_taskId  := l_taskId);
  END IF;
  
  RETURN (l_json,NULL,NULL)::ss_dat.t_studyVals_return;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;
