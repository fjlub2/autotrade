CREATE OR REPLACE FUNCTION ss_src."c_StudyParams_get"(p_ucsId  ss_dat.user_chart_studies.ucs_id%TYPE)
  RETURNS SETOF ss_dat.t_studyParams AS
$BODY$
DECLARE
  l_taskId    INTEGER;
BEGIN
  l_taskId := core_src."c_Session_getCurrentTaskId"();
  
  PERFORM core_src."c_Message_logMsgText"(p_message := 'Returning study params.',
                                          p_data    := core_src."c_Data_addTag"('UCS_ID',p_ucsId),
                                          p_type    := 'I',
                                          p_taskId  := l_taskId);
                                            
  RETURN QUERY
  SELECT dprt_code,
         usp_value_text,
         usp_value_int,
         dprt_datatype
    FROM ss_dat.user_chart_study_params
      JOIN ss_dat.d_parameter_types ON dprt_id = usp_dprt_id
   WHERE usp_ucs_id = p_ucsId;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE;
