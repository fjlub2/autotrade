-- read / write
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'user_chart_studies',      'ss_tapi', 'ucs',  'N', 'ucs_seq');
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'user_chart_study_params', 'ss_tapi', 'usp',  'N', 'usp_seq');
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'user_charts',             'ss_tapi', 'uct',  'N', 'uct_seq');
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'user_strategies',         'ss_tapi', 'ust',  'N', 'ust_seq');
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'action_items',            'ss_tapi', 'ai',   'N', 'ai_seq');
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'action_item_groups',      'ss_tapi', 'aig',  'N', 'aig_seq');


-- read only
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'chart_periods',          'ss_tapi', 'chp',  'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'timeframe_chart_periods','ss_tapi', 'tcp',  'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'chart_types',            'ss_tapi', 'cht',  'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_suppliers',            'ss_tapi', 'dsup', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_exchanges',            'ss_tapi', 'dexh', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_months',               'ss_tapi', 'dmon', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_parameter_types',      'ss_tapi', 'dprt', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_parameter_values',     'ss_tapi', 'dprv', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_studies',              'ss_tapi', 'dstd', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_study_parameters',     'ss_tapi', 'dstp', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_symbol_groups',        'ss_tapi', 'dsyg', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_symbol_timeframes',    'ss_tapi', 'dsyt', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_dates',                'ss_tapi', 'ddat', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_symbols',              'ss_tapi', 'dsym', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'd_timeframes',           'ss_tapi', 'dtfr', 'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'study_chart_types',      'ss_tapi', 'sct',  'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'study_chart_panels',     'ss_tapi', 'scp',  'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'study_groups',           'ss_tapi', 'stg',  'Y', NULL);


SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'operators',              'ss_tapi', 'op',   'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'properties',             'ss_tapi', 'prp',  'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'property_operators',     'ss_tapi', 'pro',  'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'comparable_chart_values','ss_tapi', 'ccv',  'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'chart_values',           'ss_tapi', 'chv',  'Y', NULL);
SELECT ss_tapi."createTapi"('tradeignite', 'ss_dat', 'chart_value_properties', 'ss_tapi', 'cvp',  'Y', NULL);