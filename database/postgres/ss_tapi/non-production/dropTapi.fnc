CREATE OR REPLACE FUNCTION ss_tapi."dropTapi"(pi_schema character varying, pi_table character varying)
  RETURNS text AS
$BODY$
DECLARE
  l_sql              text;
  l_cnt1             text;
  l_cnt2             text;
  l_tableAlias       text;
BEGIN
  SELECT REPLACE(INITCAP(REGEXP_REPLACE(pi_table,'^_*','')),'_','')
    INTO l_tableAlias;

  SELECT string_agg('DROP '||CASE p.proisagg WHEN true THEN 'AGGREGATE ' ELSE 'FUNCTION ' END|| quote_ident(n.nspname) || '.' || quote_ident(p.proname)|| '(' || pg_catalog.pg_get_function_identity_arguments(p.oid) || ');' ,E'\n'),
         COUNT(1)
    INTO l_sql,
         l_cnt1
    FROM pg_catalog.pg_proc p
      JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
   WHERE n.nspname    = pi_schema
     AND (p.proname LIKE CONCAT('t_',pi_table,    '_%') OR
          p.proname LIKE CONCAT('t_',l_tableAlias,'_%'));

  IF l_sql IS NOT NULL THEN
    EXECUTE l_sql;
  END IF;
  
  SELECT string_agg('DROP TYPE '||pi_schema||'."'||user_defined_type_name||'";' ,E'\n'),
         COUNT(1)
    INTO l_sql,
         l_cnt2
    FROM information_schema.user_defined_types
   WHERE user_defined_type_schema  = pi_schema
     AND (user_defined_type_name LIKE CONCAT('t_',pi_table,    '_%') OR 
          user_defined_type_name LIKE CONCAT('t_',l_tableAlias,'_%')); 

  IF l_sql IS NOT NULL THEN
    EXECUTE l_sql;
  END IF;
    
  RETURN CONCAT('Dropped ',COALESCE(l_cnt1,'0'),' functions and ',COALESCE(l_cnt2,'0'),' types.');
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;