/*
 
 TODO: Create a class of these methods
 
 menu_add_item is not available in criteria page 
  
 submit_criteria            Critea   Submit
 submit_chart_analysis      Analysis Submit  
  
 */

function block_page() {
	
	 $('#chart_area').block({overlayCSS: {backgroundColor: '#C8C8C8 '}, message: null}); 
	 $('#menu_add_item').block({overlayCSS: {backgroundColor: '#C8C8C8 '}, message: null});
	 $('#menu_strategy_name').block({overlayCSS: {backgroundColor: '#C8C8C8 '}, message: null});
	 $('#submit_chart_analysis').block({overlayCSS: {backgroundColor: '#C8C8C8 '}, message: null});	
	 $('#submit_criteria').block({overlayCSS: {backgroundColor: '#C8C8C8 '}, message: null});
	 $('#menuSpacer').block({overlayCSS: {backgroundColor: '#C8C8C8 '}, message: null});
	 $('.sidepanel').block({overlayCSS: {backgroundColor: '#C8C8C8 '}, message: null});
	 $("body").css("overflow", "hidden");
}

function unblock_page(p_Page) {
	
	switch(p_Page) {
	
		case 'analysis':
		
			if ($("#menu_drop").is(":hidden")) {
				unblock();
			}		
	
		break;

		case 'criteria':
			
			unblock();	
	
		break;	
	
	}
}

function unblock() {
	$('#chart_area').unblock({ message: null });
	$('#menu_add_item').unblock({ message: null });
	$('#menu_strategy_name').unblock({ message: null });
	$('#submit_chart_analysis').unblock({ message: null });	
	$('#submit_criteria').unblock({ message: null });
	$('#menuSpacer').unblock({ message: null });
	$('.sidepanel').unblock({ message: null });
	$("body").css("overflow", "auto");
}

