/*
 

  
 */

function reset_screen_size(p_Event, p_Page){

	var v_height = Math.max($(window).height());
	var v_height_slider = v_height - 71;
	var v_height_chart  = v_height - 100;
	var v_height_dynamicMenu  = v_height_slider - 110;
	var v_amCarts = Math.max($('.ti-stock-div').height());	
	
	switch(p_Page) {
	
		case 'strategies':
			
			$(".slider_td").css({height:v_height_slider});
    	
		break;	
    
		case 'analysis':
    	
			var v_height_studiesMenu  = v_height - 77;
			
			$(".slider_td").css({height:v_height_slider});
    	
			$(".analysisMenu").css({'height':v_height_dynamicMenu});
			$(".studies_menu").css({'max-height':v_height_studiesMenu});
    	
        break;	
        
		case 'criteria':
			
			v_height_chart = v_height_chart - 5;
			if(v_amCarts != 0) {
				$(".sidepanel").css({height:v_height_chart});
			} else {
				$(".sidepanel").css({height:v_height_chart});
			}
			
        break;
        
		case 'identify':
			
			$(".slider_td").css({height:v_height_slider});
			
        break;
    } 

	if(v_amCarts != 0) {
		$(".chart").css({height:v_amCarts});
		$(".content").css({height:v_height_chart});
	} else {
		$(".chart").css({height:v_height_chart});
		$(".content").css({height:v_height_chart});
	}

	if (p_Event == 'resizeTrigger') {
		adjChartHeight();
	}
}

