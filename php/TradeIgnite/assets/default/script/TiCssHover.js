/*
 
This is where all the hover CSS functionailty is
  
 */

	function hover_search() {
		$("#search_image").attr('src', '../assets/default/images/amchart/search_hover.svg');
		$(".searchTD").css({background: "#5f6f81"});
	}

	function unhover_search() {
		if ($("#TiZoomBarOptions").is(":hidden")) {
			$("#search_image").attr('src', '../assets/default/images/amchart/search.svg');
			$(".searchTD").css({background: "#ffffff"});
		}
	}			

	function hover_backward() {
		$("#backward_image").attr('src', '../assets/default/images/amchart/backward_w.svg');
		$(".nav_backward").css({background: "#5f6f81"});
	}

	function unhover_backward() {
		$("#backward_image").attr('src','../assets/default/images/amchart/backward_c.svg');
		$(".nav_backward").css({background: "#ffffff"});
	}

	function hover_forward() {
		$("#forward_image").attr('src', '../assets/default/images/amchart/forward_w.svg');
		$(".nav_forward").css({background: "#5f6f81"});
	}

	function unhover_forward() {
		$("#forward_image").attr('src', '../assets/default/images/amchart/forward_c.svg');
		$(".nav_forward").css({background: "#FFFFFF"});
	}

	function hover_navmenuAreaEntry(){
		$("#navmenuAreaEntry").css({background: "#5f6f81", color: "#FFFFFF"});
	}

	function unhover_navmenuAreaEntry(){
		$("#navmenuAreaEntry").css({background: "#FFFFFF", color: "#5f6f81"});
	}

	function hover_navmenuAreaExit(){
		$("#navmenuAreaExit").css({background: "#5f6f81", color: "#FFFFFF"});
	}

	function unhover_navmenuAreaExit(){
		$("#navmenuAreaExit").css({background: "#FFFFFF", color: "#5f6f81"});
	}
	
	function unhover_zoom() {		
		$( "#TiZoomBarOptions" ).hide();
		unhover_search();
	}
	
	function hover_add() {

		var data = $('#menu_add_item').data();
		
		if (data["blockUI.isBlocked"] !== 1){	
			$("#add_image").attr('src', '../assets/default/images/menu/plus_w.svg');
			$(".add").css({background: "#5f6f81"});
		}	
	}
	
	function unhover_add() {

		var data = $('#menu_add_item').data();
		
		if (data["blockUI.isBlocked"] !== 1){			
			$("#add_image").attr('src', '../assets/default/images/menu/plus_c.svg');
			$(".add").css({	background: "#ffffff"});
		}
	}
	function hover_menu() {

		if ($("#menu_drop").is(":hidden")) {
			$("#list_image").attr('src', '../assets/default/images/menu/list_w.svg');
			$(".menu").css({background: "#5f6f81"});
		} 	
	}
	function unhover_menu() {

		if ($("#menu_drop").is(":hidden")) {
			$("#list_image").attr('src', '../assets/default/images/menu/list_c.svg');
			$(".menu").css({background: "#ffffff"});
		}
	}
	function hover_strategy_image() {

		if ($("#strategy_menu").is(":hidden")) {
			$("#strategy_image").attr('src', '../assets/default/images/menu/strategy_selected.png');
			$(".menu_strategy_image").css({background: "#5f6f81"});
		}
	}
	function unhover_strategy_image() {

		if ($("#strategy_menu").is(":hidden")) {
			$("#strategy_image").attr('src', '../assets/default/images/menu/strategy_unselected.png');
			$(".menu_strategy_image").css({background: "#ffffff"});
		}
	}
	function hover_chart_image() {

		var strategy_id = $("#strategy_id").val();

		if (strategy_id !== '') {
			if ($("#chart_menu").is(":hidden")) {
				$("#chart_image").attr('src', '../assets/default/images/menu/chart_selected.png');
				$(".menu_chart_image").css({background: "#5f6f81"});
			}
		}
	}
	function unhover_chart_image() {

		if ($("#chart_menu").is(":hidden")) {

			$("#chart_image").attr('src', '../assets/default/images/menu/chart_unselected.png');
			$(".menu_chart_image").css({background: "#ffffff"});
		}
	}
	function hover_studies_image() {

		var chartId    = $("#chart_id").val();
		
		if (chartId !== '') {
			if ($("#studies_menu").is(":hidden")) {
				$("#studies_image").attr('src', '../assets/default/images/menu/studies_selected.png');
				$(".menu_studies_image").css({background: "#5f6f81"});
			}
		}
	}
	function unhover_studies_image() {

		if ($("#studies_menu").is(":hidden") && $("#addStudiesMenu").is(":hidden")) {
			
			$("#studies_image").attr('src', '../assets/default/images/menu/studies_unselected.png');
			$(".menu_studies_image").css({background: "#ffffff"});
	 
			$("#add_study_details").children().remove(); 
		}	
	}
	function hover_entryarea_image() {

		var chartId    = $("#chart_id").val();
		
		if (chartId !== '') {
			
			if ($("#setuparea_menu").is(":hidden")) {
				$("#entryarea_image").attr('src', '../assets/default/images/menu/entry_area_selected.png');
				$(".menu_entryarea_image").css({background: "#5f6f81"});
			}
		}
	}
	function unhover_entryarea_image() {

		if ($("#setuparea_menu").is(":hidden")){		
			$("#entryarea_image").attr('src', '../assets/default/images/menu/entry_area_unselected.png');
			$(".menu_entryarea_image").css({background: "#ffffff"});
		}	
	}

	function hover_cog() {
		$("#cog_image").attr('src', '../assets/default/images/menu/cog_w.svg');
	}
	function unhover_cog() {
		$("#cog_image").attr('src', '../assets/default/images/menu/cog_c.svg');
	}
	function hover_exit() {
		$("#exit_image").attr('src', '../assets/default/images/menu/exit_w.svg');
	}
	function unhover_exit() {
		$("#exit_image").attr('src', '../assets/default/images/menu/exit_c.svg');
	}

	function hover_EntryExit(ele) {
		
		var id = ele.id;
	
		if($('#'+id).hasClass('EntryExitUnActive')){
			$('#'+id).css({background: "#5f6f81"});
			$('#'+id).css({color: "#ffffff"});
		}
	}
	
	function unhover_EntryExit(ele) {
		
		var id = ele.id;
		
		if($('#'+id).hasClass('EntryExitUnActive')){
			$('#'+id).css({background: "#ffffff"});
			$('#'+id).css({color: "#5f6f81"});
		}	
	}