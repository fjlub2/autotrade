<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

  class Auth_model extends CI_Model {
  	
  	function __construct()
	{
		 parent::__construct();	
	}
	
	function GetLanguageCodeISO6391()
	{
		$hi_code = "";
		$hi_quof = 0;
		$langs = explode(",",$_SERVER['HTTP_ACCEPT_LANGUAGE']);
		foreach($langs as $lang)
		{
			list($codelang,$quoficient) = explode(";",$lang.';');
			if($quoficient == NULL) $quoficient = 1;
			if($quoficient > $hi_quof)
			{
				$hi_code = substr($codelang,0,2);
				$hi_quof = $quoficient;
			}
		}
		return $hi_code;
	}
  	
  	function authenticate_credentials($parameters, &$return) {
  		
  		Unirest::timeout(10);
  		$response = Unirest::post($parameters['auth_api'], array( "Accept" => "application/json" ), json_encode($parameters['user_agent']), $parameters['username'], $parameters['password']);
  		
  		$code_response_json = $response->code;
  		
  		if ($code_response_json == 200) {  		
  		
  			$response_json = $response->raw_body;		
			$response_array = json_decode($response_json, true);
			
			//Bug in session API. Extra layer 'items' for sucesful return.
			//$return['auth_code'] = $response_array['messages'][0]['code'];
			//$return['auth_msg'] = $response_array['messages'][0]['message'];
			
			$return['auth_code'] = $response_array['messages']['items'][0]['code'];
			$return['auth_msg'] = $response_array['messages']['items'][0]['message'];
			
			if ($return['auth_code'] == 200)
			{
				$return['session_detail'] = array(
					'user_id'           => $response_array['sessionData']['userId'],
					'db_session_id'     => $response_array['sessionData']['sessionCode']
				);
			}
			else 
			{
				$return['session_detail'] = array(
						'user_id'           => '',
						'db_session_id'     => ''
				);
			}			
  		}
		else {
			
			$return['auth_code'] = 403;
			$return['auth_msg']  = 'Attempting to access restricted content.';			
		}		
  	} 
  }  	
?>