<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stratscan_model extends CI_Model {
	
/**
 * TradeIgnite
 *
 * @package		stratscan_model.php
 * @author		Frans Lubbe
 * @copyright	Copyright (c) 2014, TradeIgnite Pty Ltd
 * @license
 * @link		http://tradeignite.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------
/**
 * StratScan Model File
 *
 * DB model loaded by controller stratscan
 *
 * @package		StratScan
 * @subpackage	Model
 * @category	Front-controller
 * @author		TradeIgnite Dev Team
 * @link		http://tradeignite.com
 */

/*
 * ------------------------------------------------------
 *  Load the models objects
 * ------------------------------------------------------
 */
function __construct() {
	parent::__construct();	
}
/*
 * ----------------------------------------------------------------------
 *  Returns the list of users save strategies 
 *  Form: My Strategies
 * ----------------------------------------------------------------------
 */	
function get_strategies($parameters, &$strategies, &$error) {
	
	$error = null;
	
	$response = Unirest::get($parameters['strategies_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	//$response_header_code = 404;
	
	if($response_header_code == 200){
	
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);
		
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){	
				$error['code']    = $option->code;
				$error['message'] = $option->message;		
			}
		} else {
			log_message('error', '(StratScanModel get_strategies()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel get_strategies()). API: '.$parameters['strategies_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		
		if ($error['code'] !== 200) {
			log_message('error', '(StratScanModel get_strategies()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel get_strategies()). API: '.$parameters['strategies_api']);
			log_message('error', '(StratScanModel get_strategies()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$strategies = '{"sEcho":1,"iTotalRecords":0,"iTotalDisplayRecords":0,"items":[]}';		
		}else {
			$strategies = json_encode($response_array->strategies);
		}
	
	}else{
		log_message('error', '(StratScanModel get_strategies()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel get_strategies()). API: '.$parameters['strategies_api']);
		$strategies = '{"sEcho":1,"iTotalRecords":0,"iTotalDisplayRecords":0,"items":[]}';
	}
}
/*
 * ----------------------------------------------------------------------
 *  Returns the list of chart types
 *  Form: chart options
 * ----------------------------------------------------------------------
 */
function chart_types_options($parameters, &$chart_type_opt, &$sel_chart_type_opt, &$error) {

	$error = null;

	$response = Unirest::get($parameters['chart_types_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json = $response->raw_body;	
		$response_array = json_decode($response_json);
		
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel chart_types_options()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel chart_types_options()). API: '.$parameters['chart_types_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		
		if ($error['code'] == 200) {
			
			if(is_array($response_array->chart_types->items) AND !empty($response_array->chart_types->items)){
				foreach($response_array->chart_types->items as $option){
			
					if($option->recent == 1) {
						$sel_chart_type_opt = $option->code;
					}

					$chart_type_opt[$option->code] = $option->desc;
				}
			}else{
				log_message('error', '(StratScanModel chart_types_options()). Array is empty. API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
				log_message('error', '(StratScanModel chart_types_options()). API: '.$parameters['chart_types_api']);
				$chart_type_opt[0] = 'No Chart Types Available';
				$sel_chart_type_opt = 0;
			}
		
		}else{
			log_message('error', '(StratScanModel chart_types_options()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel chart_types_options()). API: '.$parameters['chart_types_api']);
			log_message('error', '(StratScanModel chart_types_options()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$chart_type_opt[0] = 'No Chart Types Available';
			$sel_chart_type_opt = 0;
		}
	
	}else{
		log_message('error', '(StratScanModel chart_types_options()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel chart_types_options()). API: '.$parameters['chart_types_api']);
		$error['code']      = $response_header_code;
		$error['message']   = 'Unirest Error Code: '.$response_header_code;
		$chart_type_opt[0]  = 'No Chart Types Available';
		$sel_chart_type_opt = 0;
	}
}
/*
 * ----------------------------------------------------------------------
 *  Returns the list of exchanges
 *  Form: chart options
 * ----------------------------------------------------------------------
 */
function exchange_options($parameters, &$exchange_opt, &$sel_exchange_opt,  &$error) {
	 
	$error = null;

	$response = Unirest::get($parameters['exchange_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel exchange_options()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel exchange_options()). API: '.$parameters['exchange_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		if ($error['code'] == 200) {
			if(is_array($response_array->exchanges->items) AND !empty($response_array->exchanges->items)){		
				foreach($response_array->exchanges->items as $option){

					if($option->recent == 1) {
						$sel_exchange_opt = $option->code;
					}
					$exchange_opt[$option->code] = $option->desc;
				}
			}else{
				log_message('error', '(StratScanModel exchange_options()). Array is empty. API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
				log_message('error', '(StratScanModel exchange_options()). API: '.$parameters['exchange_api']);
				$exchange_opt[0] = 'No Exchange Available';
				$sel_exchange_opt  = 0;
			}
		} else {
			log_message('error', '(StratScanModel exchange_options()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel exchange_options()). API: '.$parameters['exchange_api']);
			log_message('error', '(StratScanModel exchange_options()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$chart_type_opt[0] = 'No Chart Types Available';
			$sel_exchange_opt  = 0;
		}
		
	}else {
		log_message('error', '(StratScanModel exchange_options()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel exchange_options()). API: '.$parameters['exchange_api']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$exchange_opt[0]   = 'No Exchange Available';
		$sel_exchange_opt  = 0;
	}	 
}
/*
 * ----------------------------------------------------------------------
 *  Returns the list of products
 *  Form: chart options
 * ----------------------------------------------------------------------
 */
function product_options($parameters, &$product_opt, &$sel_product_opt, &$sel_product_array, &$error) {

	$error = null;

	$response = Unirest::get($parameters['products_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel product_options()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel product_options()). API: '.$parameters['products_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
	
		if ($error['code'] == 200) {
			if(is_array($response_array->products->items) AND !empty($response_array->products->items)){		
				foreach($response_array->products->items as $option){
		 	
					if($option->recent == 1) {
						$sel_product_opt = $option->code;
						$sel_product_array[$option->code] = $option->desc;
					}
					$product_opt[$option->code] = $option->desc;		 
				}
			}else{
				log_message('error', '(StratScanModel product_options()). Array is empty. API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
				log_message('error', '(StratScanModel product_options()). API: '.$parameters['products_api']);
				$product_opt[0]    = 'No Products Available';
				$sel_product_opt   = 0;
				$sel_product_array[0] = 'No Contracts Available';
			}
		}else{
			log_message('error', '(StratScanModel product_options()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel product_options()). API: '.$parameters['products_api']);
			log_message('error', '(StratScanModel product_options()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$product_opt[0]    = 'No Products Available';
			$sel_product_opt   = 0;
			$sel_product_array[0] = 'No Contracts Available';
		}
	
	}else {
		log_message('error', '(StratScanModel product_options()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel product_options()). API: '.$parameters['products_api']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$product_opt[0]    = 'No Products Available';
		$sel_product_opt   = 0;
		$sel_product_array[0] = 'No Contracts Available';
	}		
}
/*
 * ----------------------------------------------------------------------
 *  Returns the list of contracts based on the product selection
 *  Form: chart options
 * ----------------------------------------------------------------------
 */
function contracts_options($parameters, &$contract_opt, &$sel_contract_opt, &$sel_contract_array, &$error) {

	$error = null;

	$response = Unirest::get($parameters['contracts_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
	
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel contracts_options()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel contracts_options()). API: '.$parameters['contracts_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		if ($error['code'] == 200) {
			if(is_array($response_array->contracts->items) AND !empty($response_array->contracts->items)){
				foreach($response_array->contracts->items as $option){

					if($option->recent == 1) {
						$sel_contract_opt                  = $option->code;
						$sel_contract_array[$option->code] = $option->desc;
					}
			
					$contract_opt[$option->code] = $option->desc;
				}
			}else {
				log_message('error', '(StratScanModel contracts_options()). Array is empty. API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
				log_message('error', '(StratScanModel contracts_options()). API: '.$parameters['contracts_api']);
				$contract_opt[0]       = 'No Contracts Available';
				$sel_contract_opt      = 0;
				$sel_contract_array[0] = 'No Contracts Available';
			}
		}else{
			log_message('error', '(StratScanModel contracts_options()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel contracts_options()). API: '.$parameters['contracts_api']);
			log_message('error', '(StratScanModel contracts_options()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$contract_opt[0]       = 'No Contracts Available';
			$sel_contract_opt      = 0;
			$sel_contract_array[0] = 'No Contracts Available';
		}
	}else {
		log_message('error', '(StratScanModel contracts_options()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel contracts_options()). API: '.$parameters['contracts_api']);
		$error['code']         = $response_header_code;
		$error['message']      = 'Unirest Error Code: '.$response_header_code;
		$contract_opt[0]       = 'No Contracts Available';
		$sel_contract_opt      = 0;
		$sel_contract_array[0] = 'No Contracts Available';
	}		
}
/*
 * ----------------------------------------------------------------------
 *  Returns the saved strategy values
 *  Form: chart options (edit strategy)
 * ----------------------------------------------------------------------
 */
function return_strategy_chart_options($parameters, &$saved_strategy_options, &$saved_chart_options, &$error) {
	 
	$error = array();
		
	$response = Unirest::get($parameters['charts_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);
	 
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
		 		$error['code']    = $option->code;
				$error['message'] = $option->message;		 
			}
		} else {
			log_message('error', '(StratScanModel return_strategy_chart_options()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel return_strategy_chart_options()). API: '.$parameters['charts_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		
		if ($error['code'] == 200) {	
			$saved_strategy_options = $response_array->strategy;
			$saved_chart_options = $response_array->charts;
		}else{
			log_message('error', '(StratScanModel return_strategy_chart_options()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel return_strategy_chart_options()). API: '.$parameters['charts_api']);
			log_message('error', '(StratScanModel return_strategy_chart_options()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$saved_strategy_options = array();
			$saved_chart_options    = array();
		}
	}else {
		log_message('error', '(StratScanModel return_strategy_chart_options()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel return_strategy_chart_options()). API: '.$parameters['charts_api']);
		$error['code']          = $response_header_code;
		$error['message']       = 'Unirest Error Code: '.$response_header_code;
		$saved_strategy_options = array();
		$saved_chart_options    = array();
	}
}
/*
 * ----------------------------------------------------------------------
 *  Returns the list of timeframe options
 *  Form: chart options 
 * ----------------------------------------------------------------------
 */
function strategy_timeframe_options($parameters, &$timeframe_opt, &$sel_timeframe_opt, &$sel_timeframe_array, &$error){

	$error = null;
	$response = Unirest::get($parameters['strategy_timeframe_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel strategy_timeframe_options()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel strategy_timeframe_options()). API: '.$parameters['strategy_timeframe_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		if ($error['code'] == 200) {
	
			if(is_array($response_array->timeframes->items) AND !empty($response_array->timeframes->items)){
				$i = 0;
				foreach($response_array->timeframes->items as  $option){

					if ($i == 0) {
						$sel_timeframe_opt = $option->code;
						$sel_timeframe_array[$option->code] = $option->desc;
					}
					$timeframe_opt[$option->code] = $option->desc;
					$i++;

				}
			}else {
				log_message('error', '(StratScanModel strategy_timeframe_options()). Array is empty. API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
				log_message('error', '(StratScanModel strategy_timeframe_options()). API: '.$parameters['strategy_timeframe_api']);
				$timeframe_opt[0]       = 'No TimeFrame Available';
				$sel_timeframe_opt      = 0;
				$sel_timeframe_array[0] = 'No TimeFrame Available';
			}
		}else{
			log_message('error', '(StratScanModel strategy_timeframe_options()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel strategy_timeframe_options()). API: '.$parameters['strategy_timeframe_api']);
			log_message('error', '(StratScanModel strategy_timeframe_options()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$timeframe_opt[0]       = 'No TimeFrame Available';
			$sel_timeframe_opt      = 0;
			$sel_timeframe_array[0] = 'No TimeFrame Available';
		}
	}else{
		log_message('error', '(StratScanModel strategy_timeframe_options()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel strategy_timeframe_options()). API: '.$parameters['strategy_timeframe_api']);
		$error['code']          = $response_header_code;
		$error['message']       = 'Unirest Error Code: '.$response_header_code;
		$timeframe_opt[0]       = 'No TimeFrame Available';
		$sel_timeframe_opt      = 0;
		$sel_timeframe_array[0] = 'No TimeFrame Available';
	}	
}
/*
 * ----------------------------------------------------------------------
 *  Returns the list of available studies
 *  Form: chart options
 * ----------------------------------------------------------------------
 */
function studies_options($parameters, &$studies_opt, &$sel_studies_opt, &$sel_studies_array, &$error) {
	
	$error = null;
	
	$response = Unirest::get($parameters['unused_studies_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
	
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);
	
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel studies_options()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel studies_options()). API: '.$parameters['unused_studies_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		
		if ($error['code'] == 200) {
			
			if(is_array($response_array->studies->items) AND !empty($response_array->studies->items)){			
				foreach($response_array->studies->items as $option){
					$sel_studies_opt = 0;		
					$sel_studies_array[0] = 'Click to Select';
					$studies_opt[$option->code] = $option->desc;	
				}
			} else {
				log_message('error', '(StratScanModel studies_options()). Array is empty. API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
				log_message('error', '(StratScanModel studies_options()). API: '.$parameters['unused_studies_api']);
				$studies_opt[0]       = 'No Studies Available';
				$sel_studies_opt      = 0;
				$sel_studies_array[0] = 'No Studies Available';
			}
		}else{
			log_message('error', '(StratScanModel studies_options()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel studies_options()). API: '.$parameters['unused_studies_api']);
			log_message('error', '(StratScanModel studies_options()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$studies_opt[0]       = 'No Studies Available';
			$sel_studies_opt      = 0;
			$sel_studies_array[0] = 'No Studies Available';
		}
	}else{
		log_message('error', '(StratScanModel studies_options()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel studies_options()). API: '.$parameters['unused_studies_api']);
		$error['code']        = $response_header_code;
		$error['message']     = 'Unirest Error Code: '.$response_header_code;
		$studies_opt[0]       = 'No Studies Available';
		$sel_studies_opt      = 0;
		$sel_studies_array[0] = 'No Studies Available';
	}	
}
/*
 * ----------------------------------------------------------------------
 *  Returns the list of studies already added by user
 *  Form: chart options
 * ----------------------------------------------------------------------
 */
function used_studies_detail($parameters, &$used_studies_detail, &$error){

	$error = null;
		
	$response = Unirest::get($parameters['used_studies_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json  = $response->raw_body;
		$response_array = json_decode($response_json);
	
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel used_studies_detail()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel used_studies_detail()). API: '.$parameters['used_studies_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		if ($error['code'] == 200) {
		
			$used_studies_detail        = $response_array->studies->items;
		} else {
			log_message('error', '(StratScanModel used_studies_detail()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel used_studies_detail()). API: '.$parameters['used_studies_api']);
			log_message('error', '(StratScanModel used_studies_detail()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$used_studies_detail  = array();
		}	
	} else {
		log_message('error', '(StratScanModel used_studies_detail()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel used_studies_detail()). API: '.$parameters['used_studies_api']);
		$error['code']        = $response_header_code;
		$error['message']     = 'Unirest Error Code: '.$response_header_code;
		$used_studies_detail  = array();
	}
}
/*
 * ----------------------------------------------------------------------
 *  Get the framework code for the charting. Building the initial library
 *  Form: On initial load of Chart Analysis
 * ----------------------------------------------------------------------
*/
function get_chart_code($parameters, &$chart_code, &$error) {

	$error = null;
		
	$response = Unirest::get($parameters['chartload_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){	
		$chart_code        = $response->raw_body;
		$error['code']     = 200;
		$error['message']  = 'Success';
	} else {
		log_message('error', '(StratScanModel get_chart_code()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel get_chart_code()). API: '.$parameters['chartload_api']);
		log_message('error', '(StratScanModel get_chart_code()). Unirest Error Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$chart_code        = null;		
	}
}
/*
 * ----------------------------------------------------------------------
 *  Get the contract details
 *  Form: chart options
 * ----------------------------------------------------------------------
*/
function contract_details($parameters, &$min_date, &$max_date, &$error) {
		
	$error = null;
	 
	$response = Unirest::get($parameters['contract_detail_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);
	 
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){

			foreach($response_array->messages->items as $option){
		 		$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel contract_details()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel contract_details()). API: '.$parameters['contract_detail_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}	
		if ($error['code'] == 200) {		
			$min_date = $response_array->contract->min_date;
			$max_date = $response_array->contract->max_date;
		}else {
			log_message('error', '(StratScanModel contract_details()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel contract_details()). API: '.$parameters['contract_detail_api']);
			log_message('error', '(StratScanModel contract_details()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$min_date = 'N/A';
			$max_date = 'N/A';
		}		
	}else{
		log_message('error', '(StratScanModel contract_details()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel contract_details()). API: '.$parameters['contract_detail_api']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$min_date          = 'N/A';
		$max_date          = 'N/A';
	}		
}
/*
 * ----------------------------------------------------------------------
 *  Save Strategy
 *  Form: chart options
 * ----------------------------------------------------------------------
*/
function save_strategy($parameters, &$strategy_id ,&$error) {

	$error = null;
	
	$response = Unirest::put($parameters['strategy_api'], array( "Accept" => "application/json" ), json_encode($parameters['strategy_detail']), $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel save_strategy()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel save_strategy()). API: '.$parameters['strategy_api']);
			log_message('error', '(StratScanModel save_strategy()). API Body: '.json_encode($parameters['strategy_detail']));
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		if ($error['code'] == 200) {
			$strategy_id = $response_array->strategy->strategyId;
		} else {
			log_message('error', '(StratScanModel save_strategy()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel save_strategy()). API: '.$parameters['strategy_api']);
			log_message('error', '(StratScanModel save_strategy()). API Body: '.json_encode($parameters['strategy_detail']));
			log_message('error', '(StratScanModel save_strategy()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$strategy_id = null;
		}
	}else{
		log_message('error', '(StratScanModel save_strategy()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel save_strategy()). API: '.$parameters['strategy_api']);
		log_message('error', '(StratScanModel save_strategy()). API Body: '.json_encode($parameters['strategy_detail']));
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$strategy_id = null;		
	}
}
/*
 * ----------------------------------------------------------------------
 *  Save Chart
 *  Form: chart options
 * ----------------------------------------------------------------------
*/
function save_chart($parameters, &$chart_id ,&$error) {
	 
	$error = null;
	
	$response = Unirest::put($parameters['chart_api'], array( "Accept" => "application/json" ), json_encode($parameters['chart_detail']), $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
	
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);
		
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
		 		$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel save_chart()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel save_chart()). API: '.$parameters['chart_api']);
			log_message('error', '(StratScanModel save_chart()). API Body: '.json_encode($parameters['chart_detail']));
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		if ($error['code'] == 200) {
			$chart_id = $response_array->chart->chartId;
		}else {
			log_message('error', '(StratScanModel save_chart()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel save_chart()). API: '.$parameters['chart_api']);
			log_message('error', '(StratScanModel save_chart()). API Body: '.json_encode($parameters['chart_detail']));
			log_message('error', '(StratScanModel save_chart()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$chart_id = null;
		}
	} else {
		log_message('error', '(StratScanModel save_chart()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel save_chart()). API: '.$parameters['chart_api']);
		log_message('error', '(StratScanModel save_chart()). API Body: '.json_encode($parameters['chart_detail']));
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$chart_id = null;		
	}
}
/*
 * ----------------------------------------------------------------------
 *  Return chart options ( sets all the form dropdown menus )
 *  Form: chart options
 * ----------------------------------------------------------------------
*/
function return_chart_options($parameters, &$saved_chart_options, &$saved_strategy_options,&$error) {

	$error = null;
		
	$response = Unirest::get($parameters['chart_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel return_chart_options()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel return_chart_options()). API: '.$parameters['chart_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}	
		if ($error['code'] == 200) {
			$saved_chart_options = $response_array->chart;
			$saved_strategy_options = $response_array->strategy;
		} else {
			log_message('error', '(StratScanModel return_chart_options()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel return_chart_options()). API: '.$parameters['chart_api']);
			log_message('error', '(StratScanModel return_chart_options()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$saved_chart_options    = null;
			$saved_strategy_options = null;
		}
	} else {
		log_message('error', '(StratScanModel return_chart_options()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel return_chart_options()). API: '.$parameters['chart_api']);
		$error['code']          = $response_header_code;
		$error['message']       = 'Unirest Error Code: '.$response_header_code;
		$saved_chart_options    = null;
		$saved_strategy_options = null;
	}
}
/*
 * ----------------------------------------------------------------------
 *  return_strategy_options ( returns strategy options )
 *  Form: chart options
 * ----------------------------------------------------------------------
*/
function return_strategy_options($parameters, &$saved_strategy_options, &$error){

	$error = null;

	$response = Unirest::get($parameters['strategy_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel return_strategy_options()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel return_strategy_options()). API: '.$parameters['strategy_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		if ($error['code'] == 200) {		
			$saved_strategy_options = $response_array->strategy;
		} else {
			log_message('error', '(StratScanModel return_strategy_options()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel return_strategy_options()). API: '.$parameters['strategy_api']);
			log_message('error', '(StratScanModel return_strategy_options()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$saved_strategy_options = null;
		}
		
	} else {
		log_message('error', '(StratScanModel return_strategy_options()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel return_strategy_options()). API: '.$parameters['strategy_api']);
		$error['code']          = $response_header_code;
		$error['message']       = 'Unirest Error Code: '.$response_header_code;
		$saved_strategy_options = null;
	}
}
/*
 * ----------------------------------------------------------------------
 *  Delete Strategy
 *  Strategy Summary Page
 * ----------------------------------------------------------------------
*/
function delete_strategy($parameters, &$error) {

	$error = null;
		
	$response = Unirest::delete($parameters['strategy_api'], null, null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
	
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);
		
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel delete_strategy()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel delete_strategy()). API: '.$parameters['strategy_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		if ($error['code'] !== 200) {
			log_message('error', '(StratScanModel delete_strategy()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel delete_strategy()). API: '.$parameters['strategy_api']);
			log_message('error', '(StratScanModel delete_strategy()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
		}

	} else {
		log_message('error', '(StratScanModel delete_strategy()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel delete_strategy()). API: '.$parameters['strategy_api']);
		$error['code']          = $response_header_code;
		$error['message']       = 'Unirest Error Code: '.$response_header_code;
	}
}
/*
 * ----------------------------------------------------------------------
 *  Returns the data to populate chart framework with the data
 *  Chart Analysis Page
 * ----------------------------------------------------------------------
*/
function get_chart_data($parameters, &$chart_data, &$chart_settings, &$studies,&$error) {
	 
	$error = null;
		
	$response = Unirest::get($parameters['chartdata_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);
		
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel get_chart_data()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel get_chart_data()). API: '.$parameters['chartdata_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		if ($error['code'] == 200) {
			$chart_data     = $response_array->chartData->items;
			$chart_settings = $response_array->chartSettings;
			$studies        = $response_array->studies;
		} else {
			log_message('error', '(StratScanModel get_chart_data()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel get_chart_data()). API: '.$parameters['chartdata_api']);
			log_message('error', '(StratScanModel get_chart_data()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$chart_data        = null;
			$chart_settings    = null;
			$studies           = null;
		}		
	} else {
		log_message('error', '(StratScanModel get_chart_data()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel get_chart_data()). API: '.$parameters['chartdata_api']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$chart_data        = null;
		$chart_settings    = null;
		$studies           = null;
	}
}
/*
 * -------------------------------------------------------------------------
 *  Returns the study data to populate chart framework with the study data
 *  Chart Analysis Page
 * -------------------------------------------------------------------------
*/
function get_study_data($parameters, &$studies, &$error) {
		
	$error = null;
		
	$response = Unirest::get($parameters['studydata_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json  = $response->raw_body;
		$response_array = json_decode($response_json);
		
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel get_study_data()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel get_study_data()). API: '.$parameters['studydata_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		
		if ($error['code'] == 200) {
			$studies        = $response_array->study;
		} else {
			log_message('error', '(StratScanModel get_study_data()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel get_study_data()). API: '.$parameters['studydata_api']);
			log_message('error', '(StratScanModel get_study_data()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel get_study_data()). ApiURL: '.$parameters['studydata_api']);
			$studies           = null;       
		}
	
	} else {
		log_message('error', '(StratScanModel get_study_data()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel get_study_data()). API: '.$parameters['studydata_api']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$studies           = null;
	}
}
/*
 * -------------------------------------------------------------------------
 *  Saves selected study parameters to the Chart Id
 *  Study menu returned from DB
 * -------------------------------------------------------------------------
*/
function save_study($parameters, &$studyId, &$error) {

	$error = null;
	
	$response = Unirest::put($parameters['SaveUpdate_study_api'], array( "Accept" => "application/json" ), json_encode($parameters['study_detail']), $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
	
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel save_study()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel save_study()). API: '.$parameters['SaveUpdate_study_api']);
			log_message('error', '(StratScanModel save_study()). API Data: '.json_encode($parameters['study_detail']));
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';				
		}
		
		if ($error['code'] == 200) {
			$studyId = $response_array->study->studyId;
		}else {
			log_message('error', '(StratScanModel save_study()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel save_study()). API: '.$parameters['SaveUpdate_study_api']);
			log_message('error', '(StratScanModel save_study()). API Data: '.json_encode($parameters['study_detail']));
			log_message('error', '(StratScanModel save_study()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			$studyId           = null;
		}		
	} else {
		log_message('error', '(StratScanModel save_study()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel save_study()). API: '.$parameters['SaveUpdate_study_api']);
		log_message('error', '(StratScanModel save_study()). API Data: '.json_encode($parameters['study_detail']));
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$studyId           = null;		
	}
}
/*
 * -------------------------------------------------------------------------
 *  Removes s saved study from the Chart ID
 *  Button on the study menu
 * -------------------------------------------------------------------------
*/
function remove_study($parameters, &$error) {

	$error = null;
		
	$response = Unirest::delete($parameters['del_study_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
		
		$response_json  = $response->raw_body;
		$response_array = json_decode($response_json);
		
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel remove_study()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel remove_study()). API: '.$parameters['del_study_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
		
		if ($error['code'] !== 200) {
			log_message('error', '(StratScanModel remove_study()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel remove_study()). API: '.$parameters['del_study_api']);
			log_message('error', '(StratScanModel remove_study()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
		}
		
	} else {
		
		log_message('error', '(StratScanModel remove_study()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel remove_study()). API: '.$parameters['del_study_api']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		
	}		
}
/*
 * -------------------------------------------------------------------------
 *  Returns the study specific study menu in html form to buils the menu
 *  Call on study drop down select
 * -------------------------------------------------------------------------
*/
function get_study_code_HTMLmenu($parameters, &$study_code_HTML, &$error) {

	$error = null;

	$response = Unirest::get($parameters['unused_studies_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){

		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel get_study_code_HTMLmenu()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel get_study_code_HTMLmenu()). API: '.$parameters['unused_studies_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}

		if ($error['code'] == 200) {
				
			if(is_array($response_array->studies->items) AND !empty($response_array->studies->items)){
				
				$found = 0;
				foreach($response_array->studies->items as $option){					
					if ($option->code  == $parameters['study_code']){
						$found = 1;
						$study_code_HTML = $option->studyHTML;
					}
				}
				
				if ($found == 0) {
					$error['code']    = 404;
					$error['message'] = 'Drop down code did not match API';
					$study_code_HTML = '<p>Error has occurred.</p>
						                <p>Please contact you systems administrator.</p>';
					log_message('error', '(StratScanModel get_study_code_HTMLmenu()). Drop down code did not match API.');
					log_message('error', '(StratScanModel get_study_code_HTMLmenu()). API: '.$parameters['unused_studies_api']);
				}
				
			} else {
				log_message('error', '(StratScanModel get_study_code_HTMLmenu()). Array is empty. API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
				log_message('error', '(StratScanModel get_study_code_HTMLmenu()). API: '.$parameters['unused_studies_api']);
				$study_code_HTML = '<p>Error has occurred.</p>
						            <p>Please contact you systems administrator.</p>';
			}
		}else{
			log_message('error', '(StratScanModel get_study_code_HTMLmenu()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel get_study_code_HTMLmenu()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel get_study_code_HTMLmenu()). API: '.$parameters['unused_studies_api']);
			$study_code_HTML = '<p>Error has occurred.</p>
						        <p>Please contact you systems administrator.</p>';
		}
	}else{
		log_message('error', '(StratScanModel get_study_code_HTMLmenu()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel get_study_code_HTMLmenu()). API: '.$parameters['unused_studies_api']);
		$error['code']        = $response_header_code;
		$error['message']     = 'Unirest Error Code: '.$response_header_code;
		$study_code_HTML      = '<p>Error has occurred.</p>
						         <p>Please contact you systems administrator.</p>';
	}
}
/*
 * -------------------------------------------------------------------------
 *  Returns the study specific study parameters to be passed in for the save study
 *  Call on save study
 * -------------------------------------------------------------------------
*/
function save_study_code_APIparams($parameters, &$study_code_APIparams, &$error) {

	$error = null;

	$response = Unirest::get($parameters['studies_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;

	if($response_header_code == 200){

		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel save_study_code_APIparams()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel save_study_code_APIparams()). API: '.$parameters['studies_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}

		if ($error['code'] == 200) {

			if(is_array($response_array->studies->items) AND !empty($response_array->studies->items)){				
				foreach($response_array->studies->items as $option){
					if ($option->code == $parameters['study']) {
						$study_code_APIparams = $option->params;
					}
				}
				
			} else {
				log_message('error', '(StratScanModel save_study_code_APIparams()). Array is empty. API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
				log_message('error', '(StratScanModel save_study_code_APIparams()). API: '.$parameters['studies_api']);
				$study_code_APIparams = null;
			}
		}else{
			log_message('error', '(StratScanModel save_study_code_APIparams()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel save_study_code_APIparams()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel save_study_code_APIparams()). API: '.$parameters['studies_api']);
			$study_code_APIparams = null;
		}
	}else{
		log_message('error', '(StratScanModel save_study_code_APIparams()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel save_study_code_APIparams()). API: '.$parameters['studies_api']);
		$error['code']        = $response_header_code;
		$error['message']     = 'Unirest Error Code: '.$response_header_code;
		$study_code_APIparams = null;
	}
}
/*
 * -------------------------------------------------------------------------
*  Sync chart position
*  Calls from the chart when moved around
* -------------------------------------------------------------------------
*/
function chart_sync($parameters, &$vNavHtml, &$error) {

	$error = null;
	
	$response = Unirest::put($parameters['chart_sync_api'], array( "Accept" => "application/json" ), json_encode($parameters['chart_sync_detail']), $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;

	if($response_header_code == 200){

		$response_json  = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel chart_sync()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel chart_sync()). API: '.$parameters['chart_sync_api']);
			log_message('error', '(StratScanModel chart_sync()). API Data: '.json_encode($parameters['chart_sync_detail']));
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}

		if ($error['code'] == 200) {
			$vNavHtml       = $response_array->chartNavMenu->menuHTML;
		}else {
			log_message('error', '(StratScanModel chart_sync()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel chart_sync()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel chart_sync()). API: '.$parameters['chart_sync_api']);
			log_message('error', '(StratScanModel chart_sync()). API Data: '.json_encode($parameters['chart_sync_detail']));
			$vNavHtml        = null;
		}

	} else {

		log_message('error', '(StratScanModel chart_sync()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel chart_sync()). API: '.$parameters['chart_sync_api']);
		log_message('error', '(StratScanModel chart_sync()). API Data: '.json_encode($parameters['chart_sync_detail']));
		$vNavHtml          = null;
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;

	}
}
/*
 * ----------------------------------------------------------------------
 * Deletes a saved chart
 * Form: Study menu trash can
 * ----------------------------------------------------------------------
*/
function DeleteChartId($parameters, &$error) {
	
	$error = null;
	
	$response = Unirest::delete($parameters['chart_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
	
		$response_json  = $response->raw_body;
		$response_array = json_decode($response_json);
	
		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel DeleteChartId()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel DeleteChartId()). API: '.$parameters['chart_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}
	
		if ($error['code'] !== 200) {
			log_message('error', '(StratScanModel DeleteChartId()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel DeleteChartId()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel DeleteChartId()). API: '.$parameters['chart_api']);
		}
	
	} else {
	
		log_message('error', '(StratScanModel DeleteChartId()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel DeleteChartId()). API: '.$parameters['chart_api']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
	
	}	
}
/*
 * ----------------------------------------------------------------------
* Returns the timeframe menu for saved charts. Also orders the chart timeframes
* Form: Chart Options
* ----------------------------------------------------------------------
*/
function ChartMenu($parameters, &$chartId, &$menuHTML, &$menuChartCount, &$error) {

	$error = null;
	
	$response = Unirest::get($parameters['chart_menu_api'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
	
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel ChartMenu()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel ChartMenu()). API: '.$parameters['chart_menu_api']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';				
		}
		
		if ($error['code'] == 200) {
			$chartId       = $response_array->chartMenu->defaultChartId;
			$menuHTML       = $response_array->chartMenu->menuHTML;
			$menuChartCount = $response_array->chartMenu->chartCount;
		}else {
			log_message('error', '(StratScanModel ChartMenu()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel ChartMenu()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel ChartMenu()). API: '.$parameters['chart_menu_api']);
			$chartId        = null;
			$menuHTML       = null;
			$menuChartCount = null;
		}		
	} else {
		log_message('error', '(StratScanModel ChartMenu()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel ChartMenu()). API: '.$parameters['chart_menu_api']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$chartId        = null;
		$menuHTML       = null;
		$menuChartCount = null;
	}
}
/*
 * ----------------------------------------------------------------------
 * Navigates the chart window
 * Form: analysis
 * ----------------------------------------------------------------------
*/
function chartNavigation($parameters, &$error) {

	$error = null;
    
	$response = Unirest::put($parameters['chart_nav_api'], array( "Accept" => "application/json" ), json_encode($parameters['chartNavigation_detail']), $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;

	if($response_header_code == 200){

		$response_json  = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel chartNavigation()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel chartNavigation()). API: '.$parameters['chart_nav_api']);
			log_message('error', '(StratScanModel chartNavigation()). API Body: '.json_encode($parameters['chartNavigation_detail']));
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}

		if ($error['code'] !== 200) {
			log_message('error', '(StratScanModel chartNavigation()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel chartNavigation()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel chartNavigation()). API: '.$parameters['chart_nav_api']);
			log_message('error', '(StratScanModel chartNavigation()). API Body: '.json_encode($parameters['chartNavigation_detail']));
		}

	} else {

		log_message('error', '(StratScanModel chartNavigation()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel chartNavigation()). API: '.$parameters['chart_nav_api']);
		log_message('error', '(StratScanModel chartNavigation()). API Body: '.json_encode($parameters['chartNavigation_detail']));
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
	}
}
/*
 * -------------------------------------------------------------------------
*  Returns the navigation menu for the charts
*  Call after chart refresh
* -------------------------------------------------------------------------
*/
function htmlChartNavigation($parameters, &$vNavHtml, &$error) {

	$error = null;
	
	$response = Unirest::get($parameters['chartNavHtmlApi'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;
	
	if($response_header_code == 200){
	
		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel htmlChartNavigation()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel htmlChartNavigation()). API: '.$parameters['chartNavHtmlApi']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';				
		}
		
		if ($error['code'] == 200) {
			$vNavHtml       = $response_array->chartNavMenu->menuHTML;
		}else {
			log_message('error', '(StratScanModel htmlChartNavigation()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel htmlChartNavigation()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel htmlChartNavigation()). API: '.$parameters['chartNavHtmlApi']);
			$vNavHtml        = null;
		}		
	} else {
		log_message('error', '(StratScanModel htmlChartNavigation()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel htmlChartNavigation()). API: '.$parameters['chartNavHtmlApi']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$vNavHtml        = null;
	}
}
/*
 * ----------------------------------------------------------------------
* The API PUT for the area selection menu
* Form: Saving trade criteria area selection to the database.
* ----------------------------------------------------------------------
*/
function postSetupAreaSelection($parameters, &$error) {

	$error = null;
	
	$response = Unirest::put($parameters['setupAreaApi'], array( "Accept" => "application/json" ), json_encode($parameters['setupAreaApi_detail']), $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;

	if($response_header_code == 200){

		$response_json  = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel postSetupAreaSelection()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel postSetupAreaSelection()). API: '.$parameters['setupAreaApi']);
			log_message('error', '(StratScanModel postSetupAreaSelection()). API Body: '.json_encode($parameters['setupAreaApi_detail']));
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}

		if ($error['code'] !== 200) {
			log_message('error', '(StratScanModel postSetupAreaSelection()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel postSetupAreaSelection()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel postSetupAreaSelection()). API: '.$parameters['setupAreaApi']);
			log_message('error', '(StratScanModel postSetupAreaSelection()). API Body: '.json_encode($parameters['setupAreaApi_detail']));
		}

	} else {

		log_message('error', '(StratScanModel postSetupAreaSelection()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel postSetupAreaSelection()). API: '.$parameters['setupAreaApi']);
		log_message('error', '(StratScanModel postSetupAreaSelection()). API Body: '.json_encode($parameters['setupAreaApi_detail']));
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
	}
}
/*
 * -------------------------------------------------------------------------
 *  Returns the setup area menu html
 *  
 * -------------------------------------------------------------------------
*/
function setupAreasMenu($parameters, &$areaFromDate, &$areaToDate, &$menuHTML, &$error) {

	$error = null;

	$response = Unirest::get($parameters['setupAreasMenuApi'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;

	if($response_header_code == 200){

		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel setupAreasMenu()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel setupAreasMenu()). API: '.$parameters['setupAreasMenuApi']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}

		if ($error['code'] == 200) {
			$areaFromDate    = $response_array->chartSetupAreaMenu->areaFromDate;
			$areaToDate      = $response_array->chartSetupAreaMenu->areaToDate;
			$menuHTML         = $response_array->chartSetupAreaMenu->menuHTML;
			
		}else {
			log_message('error', '(StratScanModel setupAreasMenu()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel setupAreasMenu()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel setupAreasMenu()). API: '.$parameters['setupAreasMenuApi']);
			$areaFromDate    = '';
			$areaToDate      = '';
			$menuHTML         = '';
			
		}
	} else {
		log_message('error', '(StratScanModel setupAreasMenu()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel setupAreasMenu()). API: '.$parameters['setupAreasMenuApi']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$areaFromDate     = '';
		$areaToDate       = '';
		$menuHTML          = '';		
	}
}
/*
 * -------------------------------------------------------------------------
*  Returns the study menu
*  Call on initial page load, add study, delete study
* -------------------------------------------------------------------------
*/
function htmladdStudyMenu($parameters, &$vAddStudyMenuHtml, &$error) {

	$error = null;

	$response = Unirest::get($parameters['addStudyMenuApi'], array( "Accept" => "application/json" ), null, $parameters['user_id'], $parameters['session_id']);
	$response_header_code = $response->code;

	if($response_header_code == 200){

		$response_json = $response->raw_body;
		$response_array = json_decode($response_json);

		if(is_array($response_array->messages->items) AND !empty($response_array->messages->items)){
			foreach($response_array->messages->items as $option){
				$error['code']    = $option->code;
				$error['message'] = $option->message;
			}
		} else {
			log_message('error', '(StratScanModel htmladdStudyMenu()). Message->Items Array is empty.');
			log_message('error', '(StratScanModel htmladdStudyMenu()). API: '.$parameters['addStudyMenuApi']);
			$error['code']    = 204;
			$error['message'] = 'Message->Items Array is empty';
		}

		if ($error['code'] == 200) {
			$vAddStudyMenuHtml       = $response_array->studyMenu->menuHTML;
		}else {
			log_message('error', '(StratScanModel htmladdStudyMenu()). API Reponse Error Code:'.$error['code'].' API Reponse Error: '.$error['message']);
			log_message('error', '(StratScanModel htmladdStudyMenu()). API Reponse Error: '.$error['message'].' Session: '.$parameters['user_id'].' - '.$parameters['session_id']);
			log_message('error', '(StratScanModel htmladdStudyMenu()). API: '.$parameters['addStudyMenuApi']);
			$vAddStudyMenuHtml        = null;
		}
	} else {
		log_message('error', '(StratScanModel htmladdStudyMenu()). Unirest Error Code - '.$response_header_code);
		log_message('error', '(StratScanModel htmladdStudyMenu()). API: '.$parameters['addStudyMenuApi']);
		$error['code']     = $response_header_code;
		$error['message']  = 'Unirest Error Code: '.$response_header_code;
		$vAddStudyMenuHtml        = null;
	}
}

}
/* End of file stratscan_model.php */
/* Location: ./application/models/stratscan_model.php */