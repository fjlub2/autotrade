<head>
<link rel="stylesheet" type="text/css" href="..\assets\default\css\criteria_style.css" />
</head>

<script type="text/javascript">

$(function() {

	block_page();

	reset_screen_size('pageLoad', 'criteria');

  	$(window).resize(function() {
		reset_screen_size('resizeTrigger', 'criteria');
	});		

	loadChart();
	var chartId = $("#chart_id").val();
	get_chart_data(chartId, 'entry');
	
});

function saved_chart_onclick(e){
	var error_code = $("#error_code").val();

	if (error_code == 200) {

		var InputVal    = e.id;
		var Chartid     = InputVal.replace(/[chart_id]/g, "");
		var StrategyId  = $("#strategy_id").val();

		$(".charts").css({background: "#ffffff",color:"#5f6f81"});
		$(e).css({background: "#5f6f81",color:"#ffffff"});

		alert('Pressed StrategyId: '+StrategyId+' ChartId: '+Chartid);
		
	} else {
		alert('Error has occurred. Please contact you systems administrator.');
	}
}

function setupAreaMenu_onclick(setupArea){
	alert('Get data for '+setupArea+' chart');
}

</script>

<html>    
<form action="../stratscan/identify" name="identify" id="identify" method="post"> 

<input type="hidden" name="strategy_id"     id="strategy_id"  value="<?php echo $strategy_id ?>" />
<input type="hidden" name="chart_id"        id="chart_id"     value="<?php echo $chart_id ?>"    />
<input type="hidden" name="error_code"      id="error_code"   value="<?php echo $error_code?>"   />
	
<table class="main_menu" id="main_menu" style='width:100%'>
	<?php echo $menu?>
	<tr>
    <?php echo $menu_spacer?>
	</tr>	
</table>	

</html>