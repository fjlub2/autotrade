<?=$other_jscript?>

<script type="text/javascript">

function get_chart_data(chart_id, setupArea){

	var criteria = 'Y';
	
	$.ajax({
		type: "POST",
		url: "<?=base_url ()?>stratscan/get_chart_data",
		data: "chart_id="+chart_id+"&criteria="+criteria,
		dataType: 'json'
	})
	.done(function(data) {

		if (data.error_code != 200){

			alert(data.error_msg);
			$("#error_code").val(data.error_code);

			return true;

		}else{
	       	refreshChart(data);  
	       	unblock_page('criteria');     	        	
        } 
    });	
}

function editCriteriaCondition(){

	alert('edit criteria condition');
}

function deleteCriteriaCondition(){

	alert('delete criteria condition');
}

function addCriteriaCondition(){

	alert('add criteria condition');
}

</script>
	
<html>

	<div id="main">
	
		<div id="chart_area" class="chart_area">
			<div id="chart" class="chart"></div>
		</div>

		<div class="sidepanel">
		
			<table class="layout">
				<tr>
					<td>
			
						<table class="condition">
							<tr>
								<td colspan="7" class="conditionElements">
									<all><allbld>ALL</allbld> of the following conditions</all>	
								</td>
							</tr>
							<tr class="conditionRow">
								<td class="treeColumn">								
									<div class="conditionTreeTop"></div>									
									<div class="conditionTreeBottom"></div>										
								</td>
								<td class="conditionColumn">
									<table>
										<tr>
											<td class="timeframeColumn">
												30m
											</td>
											<td class="conditionDashColumn">									
												<div class="conditionDash"></div>																				
											</td>
											<td class="StudyValueColumn">
												Price	
											</td>
											<td class="OperatorColumn">
												has	
											</td>
											<td class="ComparatorColumn">
												Wide Spread	
											</td>
										</tr>
									</table>
								</td>
								<td class="editCriteriaCondition">
									<a href="#" onclick="editCriteriaCondition();"><img src="../assets/default/images/content/pencil.svg" alt="edit" width="17" height="17"></a>
								</td>
								<td class="deleteCriteriaCondition">
									<a href="#" onclick="deleteCriteriaCondition();"><img src="../assets/default/images/content/remove.svg" alt="edit" width="17" height="17"></a>
								</td>
							</tr>
							<tr class="conditionRow">
								<td class="treeColumn">								
									<div class="conditionTreeTop"></div>									
									<div class="conditionTreeBottom"></div>										
								</td>
								<td class="conditionColumn">
									<table>
										<tr>
											<td class="timeframeColumn">
												30m
											</td>
											<td class="conditionDashColumn">									
												<div class="conditionDash"></div>																				
											</td>
											<td class="StudyValueColumn">
												Close Price	
											</td>
											<td class="OperatorColumn">
												<	
											</td>
											<td class="StudyValueColumn">
												Open Price	
											</td>
										</tr>	
									</table>
								</td>
								<td class="editCriteriaCondition">
									<a href="#" onclick="editCriteriaCondition();"><img src="../assets/default/images/content/pencil.svg" alt="edit" width="17" height="17"></a>
								</td>
								<td class="deleteCriteriaCondition">
									<a href="#" onclick="deleteCriteriaCondition();"><img src="../assets/default/images/content/remove.svg" alt="edit" width="17" height="17"></a>
								</td>	
							</tr>
							<tr class="conditionRow">
								<td class="treeColumn">								
									<div class="conditionTreeTop"></div>									
									<div class="conditionTreeBottom"></div>										
								</td>
								<td class="conditionColumn">
									<table>
										<tr>
											<td class="timeframeColumn">
												30m
											</td>
											<td class="conditionDashColumn">									
												<div class="conditionDash"></div>																				
											</td>
											<td class="StudyValueColumn">
												Relative Volume	
											</td>
											<td class="OperatorColumn">
												=	
											</td>
											<td class="ComparatorColumn">
												Extremely High
											</td>
										</tr>
									</table>
								</td>
								<td class="editCriteriaCondition">
									<a href="#" onclick="editCriteriaCondition();"><img src="../assets/default/images/content/pencil.svg" alt="edit" width="17" height="17"></a>
								</td>
								<td class="deleteCriteriaCondition">
									<a href="#" onclick="deleteCriteriaCondition();"><img src="../assets/default/images/content/remove.svg" alt="edit" width="17" height="17"></a>
								</td>								
							</tr>
							<tr>
								<td>								
									<div class="addConditionTreeTop"></div>	
									<div class="addConditionTreeBottom"></div>									
								</td>
								<td colspan="6" class="addConditionLink">
									<a href="#" onclick="addCriteriaCondition();">add condition</a>
								</td>

							</tr>																	
						</table>
						
					</td>
				</tr>
				<tr>
					<td>			

						<table class="condition">
							<tr>
								<td colspan="7" class="conditionElements">
									<all><allbld>ALL</allbld> of the following conditions</all>	
								</td>
							</tr>
							<tr class="conditionRow">
								<td class="treeColumn">								
									<div class="conditionTreeTop"></div>									
									<div class="conditionTreeBottom"></div>										
								</td>
								<td class="conditionColumn">
									<table>
										<tr>
											<td class="timeframeColumn">
												30m
											</td>
											<td class="conditionDashColumn">									
												<div class="conditionDash"></div>																					
											</td>
											<td class="StudyValueColumn">
												Relative Volume	
											</td>
											<td class="OperatorColumn">
												=	
											</td>
											<td class="ComparatorColumn">
												Very Low	
											</td>
										</tr>
									</table>
								</td>
								<td class="editCriteriaCondition">
									<a href="#" onclick="editCriteriaCondition();"><img src="../assets/default/images/content/pencil.svg" alt="edit" width="17" height="17"></a>
								</td>
								<td class="deleteCriteriaCondition">
									<a href="#" onclick="deleteCriteriaCondition();"><img src="../assets/default/images/content/remove.svg" alt="edit" width="17" height="17"></a>
								</td>
							</tr>
							<tr>
								<td>								
									<div class="addConditionTreeTop"></div>	
									<div class="addConditionTreeBottom"></div>									
								</td>
								<td colspan="6" class="addConditionLink">
									<a href="#" onclick="addCriteriaCondition();">add condition</a>
								</td>

							</tr>																	
						</table>
											
					
					</td>
				</tr>
				<tr>
					<td>			

						<table class="condition">
							<tr>
								<td colspan="7" class="conditionElements">
									<all><allbld>ALL</allbld> of the following conditions</all>	
								</td>
							</tr>
							<tr class="conditionRow">
								<td class="treeColumn">								
									<div class="conditionTreeTop"></div>									
									<div class="conditionTreeBottom"></div>										
								</td>
								<td class="ANYconditionColumn" colspan="5">
									<any><anybld>ANY</anybld> of the following conditions</any>	
									
									<table>
										<tr class="conditionRow">
											<td class="treeColumn">								
												<div class="ANYconditionTreeTop"></div>									
												<div class="ANYconditionTreeBottom"></div>										
											</td>
											<td class="conditionColumn">
											
												<table>
													<tr>
														<td class="timeframeColumn">
															1h
														</td>
														<td class="conditionDashColumn">									
															<div class="ANYconditionDash"></div>																								
														</td>
														<td class="StudyValueColumn">
															EMA 9 Period	
														</td>
														<td class="OperatorColumn">
															=	
														</td>
														<td class="ComparatorColumn">
															SMA 200 Period	
														</td>
													</tr>
												</table>
											</td>
											<td class="editCriteriaCondition">
												<a href="#" onclick="editCriteriaCondition();"><img src="../assets/default/images/content/pencil.svg" alt="edit" width="17" height="17"></a>
											</td>
											<td class="deleteCriteriaCondition">
												<a href="#" onclick="deleteCriteriaCondition();"><img src="../assets/default/images/content/remove.svg" alt="edit" width="17" height="17"></a>
											</td>
										</tr>
										<tr class="conditionRow">
											<td class="treeColumn">								
												<div class="ANYconditionTreeTop"></div>									
												<div class="ANYconditionTreeBottom"></div>										
											</td>
											<td class="conditionColumn">
											
												<table>
													<tr>
														<td class="timeframeColumn">
															4h
														</td>
														<td class="conditionDashColumn">									
															<div class="ANYconditionDash"></div>															
														</td>
														<td class="StudyValueColumn">
															EMA 26 Period
														</td>
														<td class="OperatorColumn">
															<img src="../assets/default/images/criteria/x-over-down.png" height="15" width="auto"   >	
														</td>
														<td class="ComparatorColumn">
															SMA 200 Period	
														</td>
													</tr>
												</table>
											</td>
											<td class="editCriteriaCondition">
												<a href="#" onclick="editCriteriaCondition();"><img src="../assets/default/images/content/pencil.svg" alt="edit"></a>
											</td>
											<td class="deleteCriteriaCondition">
												<a href="#" onclick="deleteCriteriaCondition();"><img src="../assets/default/images/content/remove.svg" alt="edit" width="17" height="17"></a>
											</td>
										</tr>
										<tr>
											<td>								
												<div class="ANYaddConditionTreeTop"></div>	
												<div class="ANYaddConditionTreeBottom"></div>									
											</td>
											<td colspan="6" class="addConditionLink">
												<a href="#" onclick="addCriteriaCondition();">add condition</a>
											</td>
										</tr>
									</table>
									
								</td>
							</tr>
							<tr class="conditionRow">
								<td class="treeColumn">								
									<div class="conditionTreeTop"></div>									
									<div class="conditionTreeBottom"></div>										
								</td>
								<td class="conditionColumn">
									<table>
										<tr>
											<td class="timeframeColumn">
												4h
											</td>
											<td class="conditionDashColumn">									
												<div class="conditionDash"></div>																					
											</td>
											<td class="StudyValueColumn">
												Close Price	
											</td>
											<td class="OperatorColumn">
												<	
											</td>
											<td class="ComparatorColumn">
												SMA 200 Period	
											</td>
										</tr>
									</table>
								</td>
								<td class="editCriteriaCondition">
									<a href="#" onclick="editCriteriaCondition();"><img src="../assets/default/images/content/pencil.svg" alt="edit" width="17" height="17"></a>
								</td>
								<td class="deleteCriteriaCondition">
									<a href="#" onclick="deleteCriteriaCondition();"><img src="../assets/default/images/content/remove.svg" alt="edit" width="17" height="17"></a>
								</td>
							</tr>
							<tr>
								<td>								
									<div class="addConditionTreeTop"></div>	
									<div class="addConditionTreeBottom"></div>									
								</td>
								<td colspan="6" class="addConditionLink">
									<a href="#" onclick="addCriteriaCondition();">add condition</a>
								</td>

							</tr>																	
						</table>				
					</td>
				</tr>				
			</table> 			
			
		</div>	
	
	</div>
	
	<table id="submit_criteria">
  		<tr>
    		<td><input class="SubmitBtn" type="submit" formaction="../stratscan/strategies" value="Cancel" name="cancel"></td>    
    		<td style="text-align:right"><input class="SubmitBtn" type="submit" value="Submit" name="submit"></td>
  		</tr>
	</table>
	
</html>



