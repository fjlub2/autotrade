<script type="text/javascript">

$(document).ready(function() {	

	data_table = $('#example').dataTable({
		"sAjaxSource": "<?=base_url()?>stratscan/get_strategies",
		"sAjaxDataProp": "items",
		"aoColumns": 	[
							{ "mData": "name"},
							{ "mData": "desc"},
							{ "mData": "position"},
							{ "mData": "exchange"},
							{ "mData": "product" },
							{ "mData": "contract"},
							{ "mData": "updated"},
							{ "mData": "strategyId"}
    					],
    	"aoColumnDefs": [
    				    	{ "aTargets": [7],
    				          "mData": "strategyId",
    				          "mRender": function (data, type, row) {
    				                        return '<a href="#" onclick="edit_strategy('+ data +');"><img src="../assets/default/images/content/pencil.svg" alt="edit" width="20" height="20"></a>';
    				                     }
    				         },
    				         { "aTargets": [8],
          				          "mData": "strategyId",
          				          "mRender": function (data, type, row) {
          				                        return '<a href="#" onclick="delete_strategy('+ data +');"><img src="../assets/default/images/content/remove.svg" alt="delete" width="20" height="20"></a>';
          				                 }
          				     }
    				    ],	
		"bJQueryUI": true,
		"bProcessing": true,
		"bPaginate": false,
		"bScrollCollapse": true,
		"bScrollAutoCss": true,
		"sPaginationType": "full_numbers",
		"sDom": 'R<"H"lfr>t<"F"ip<',
	    "bAutoWidth": false,
		"iDisplayLength": 25,
	    "bDestroy": true,
	    "bServerSide": true,
	    "sPaginationType": "full_numbers",
	    "oLanguage": {
	        "sEmptyTable": "No Strategies Found"
	    }
	});
});

var data_table;

function edit_strategy(strategy_id) {
	$("#strategy_id").val(strategy_id);
	document.analysis.submit();
}

function delete_strategy(strategy_id){

	$.ajax({
		type: "POST",
		url: "<?=base_url ()?>stratscan/delete_strategy",
		data: "strategy_id="+ strategy_id,
		dataType: 'json'
	})
	.done(function(data ) {

		if (data.error_code != 200){
			alert(data.error_msg);
			data_table.fnDraw();
			return true;
		} else {
			data_table.fnDraw();
			return true;
		}		
	});		
}

</script>

<html>
	
	<form action="../stratscan/analysis" name="analysis" method="post">
	
	<input type="hidden" name="strategy_id" id="strategy_id" value="" />
	
	<div id="page" style="width:99%; margin-top:30px; padding-left:8px">
 		<div id="demo">
			
			<table class="display" id="example">	
    			<thead>
        			<tr>
            			<th>Name</th>
            			<th>Description</th>
            			<th>Position</th>
   			       		<th>Exchange</th>
   			         	<th>Product</th>
   			         	<th>Contract</th>
    			     	<th>Updated</th>
            			<th></th>  
            			<th></th>  
        			</tr>
    			</thead>
    			<tbody>
        
    			</tbody>
			</table>

		</div>
	</div>
  	</form>	
</html>