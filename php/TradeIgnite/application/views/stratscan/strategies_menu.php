<head>

<link rel="stylesheet" type="text/css" href="..\assets\default\css\strategies_style.css" />
</head>

<script type="text/javascript">

$(function() {

	reset_screen_size('pageLoad', 'strategies');
	$(window).resize(function() {
		reset_screen_size('pageLoad', 'strategies');
	});	

	$( "#menu_drop" ).hide();	
		
	$( "#menu_drop_item" ).click(function() {		
		
		$( "#menu_drop" ).toggle( 'slide', {direction: 'up'}, 100, 

			function(){
					
				if ($("#menu_drop").is(":hidden")) {
					
					$(".slider_td").css({
						 border:"none",
						 background:"none"
					});
						
 			 	} else {

 			 		$(".slider_td").css({
						 border:"1px solid #c6d0da",
						 background:"#ffffff"
					});	  
				}				    
			}
		);		
	});
});

</script>

<html>         

	<table class="main_menu" id="main_menu" style='width:100%'>
		<tr class="tr_menu_static">
			<td id="logo"><img id="logo_image" style="height: 58px" src="../assets/default/images/header/ti_name_black.png" title="TradeIgnite"/></td>
			<td class="add" id="menu_add_item" onmouseover="hover_add();" onmouseout="unhover_add();" onclick="window.location.href = '<?=base_url()?>stratscan/analysis'"><img id="add_image" src="../assets/default/images/menu/plus_c.svg" title="Create New Strategy"/></td>			
			<td class="menu" id="menu_drop_item" onmouseover="hover_menu();" onmouseout="unhover_menu();"><img id="list_image" src="../assets/default/images/menu/list_c.svg" title="Show/Hide Menu"/></td>
		</tr>
		<tr>
			<td style="border:none"></td>
			<td style="border:none"></td>
			<td class="slider_td" style="border:none;">
			
				<div id="menu_drop" class="menu_drop">
		
					<table style="height: 100%; width:100%">
						<tr style="height: 80%"></tr>
						<tr style="height: 4%">
							<td class="menu_cog" style="border:none; padding:10px" onmouseover="hover_cog();" onmouseout="unhover_cog();">
								<img id="cog_image" src="../assets/default/images/menu/cog_c.svg" title="Settings"/>
							</td>
						</tr>
						<tr style="height: 4%">
							<td class="menu_exit" style="border:none; padding:10px" onmouseover="hover_exit();" onmouseout="unhover_exit();" onclick="window.location.href = '<?=base_url()?>stratscan/logout'">
								<img id="exit_image" src="../assets/default/images/menu/exit_c.svg" title="Logout"/>
							</td>
						</tr>
					</table>
		
				</div>
	
			</td>
		</tr>
	</table>	
   	
</html>