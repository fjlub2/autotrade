<html>
<?php echo form_open('stratscan/identify_validation')?>
<?php echo form_hidden($hidden_strat_id);?>
<br>
<?php echo validation_errors(); ?>
<br>
<table style="margin-left:8px">
	<tr>
		<td>
			<label for="Strat_Name">Strategy Name:</label>
		</td>
		<td>
            <input type="text" id="Strat_Name" name="Strat_Name"  maxlength="50" size="50" value="<?php echo set_value('Strat_Name', $strategy_name); ?>" />
        </td>
	</tr>
	<tr>
		<td>
			<label for="Strat_Desc">Strategy Description:</label>
		</td>
		<td>
            <textarea cols="153" rows="5" id="Strat_Desc" name="Strat_Desc"><?php echo set_value('Strat_Desc', $strategy_desc); ?></textarea>
        </td>
	</tr>
</table>
<br>
<input style="margin-left:3px" type="submit" value="Submit">
  	
</html>