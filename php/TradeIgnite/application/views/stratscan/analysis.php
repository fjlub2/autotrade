<?=$other_jscript?>
<script type="text/javascript">

$(function() {

    $( "#setupsAreaValidationDialog" ).dialog({
        modal: true,
        dialogClass: "Smoothness",
        autoOpen: false,
        create: function() {
            $(".ui-dialog-titlebar-close").click(function(e) {
                unblock_page('analysis');  
            });
        }
    });    
});

function get_chart_data(chart_id){

	var criteria = 'N';
	
	$.ajax({
		type: "POST",
		url: "<?=base_url ()?>stratscan/get_chart_data",
		data: "chart_id="+ chart_id+"&criteria="+criteria,
		dataType: 'json'
	})
	.done(function(data) {

		if (data.error_code != 200){

			alert(data.error_msg);
			$("#error_code").val(data.error_code);

			return true;

		}else{

        	var v_syncFromDate     = data.chartSettings.syncFromDate; 
        	var v_syncToDate       = data.chartSettings.syncToDate; 
        	  	  
        	refreshChart( data );
         	
        	if (v_syncFromDate !== null){
        		var p_syncFromDate = new Date(v_syncFromDate);  
        		var p_syncToDate   = new Date(v_syncToDate);  
        		goToDates(p_syncFromDate, p_syncToDate);
        	} 

    		unblock_page('analysis');         	        	
        } 
    });	
}

function get_study_data(study_id){
	
	$.ajax({
		type: "POST",
		url: "<?=base_url ()?>stratscan/get_study_data",
		data: "study_id="+ study_id,
		dataType: 'json'
	})
	.done(function(data) {

		if (data.error_code != 200){

			alert(data.error_msg);
			$("#error_code").val(data.error_code);

			return true;

		}else{
			
			var arr_study_settings = data.studySettings;
			var p_studyId = arr_study_settings['studyId'];
        	var p_type    = arr_study_settings['studySettings']['studyType'];
       		var p_title   = arr_study_settings['studySettings']['studyTitle'];
      		var p_data    = arr_study_settings;
       		var p_showOn  = arr_study_settings['studySettings']['showOn'];
       		var p_color   = arr_study_settings['studySettings']['colour'];

       		addStudy(p_data);

       		var ChartId     = $("#chart_id").val();
 
       		unblock_page('analysis'); 
      		return true; 
		}      	       	         
	});	
}

function chart_sync(p_startDate, p_endDate) {

	var chartId    = $("#chart_id").val();
	
	if(chartId != '') {

		var v_startDate = p_startDate.getTime();
		var v_endDate   = p_endDate.getTime();

		$.ajax({
				type: "POST",
				url: "<?=base_url ()?>stratscan/chart_sync",
				data: "chartId="+chartId+"&startDate="+v_startDate+"&endDate="+v_endDate,
				dataType: 'json'
		})
		.done(function(data ) {

			if (data.error_code != 200){
				alert(data.error_msg);
				$("#error_code").val(data.error_code);
				return true;
			} else {
				$('#TiNavMenu').children().remove();
				$('#TiNavMenu').append(data.NavHtml);	
				return true;
			}		 						
		});
	}	 
}

function htmlChartNavigation(p_chart_id){	

	$.ajax({
		type: "POST",
		 url: "<?=base_url ()?>stratscan/htmlChartNavigation",
        data: "chartId="+ p_chart_id,
	dataType: 'json'
	})
	.done(function(data) {
		
		if (data.error_code != 200){

			alert(data.error_msg);
			$("#error_code").val(data.error_code);
			return true;

		}else{	

			$('#TiNavMenu').children().remove();
			$('#TiNavMenu').append(data.NavHtml);			
			return true;
		}  	     	       	         
	});  
}

function btnSubmitClick() {

	var strategyId = $("#strategy_id").val();
	
	$.ajax({
		type: "POST",
		 url: "<?=base_url ()?>stratscan/analysisPageValidate",
        data: "strategyId="+ strategyId,
	dataType: 'json'
	})
	.done(function(data) {
		if (data.error_code != 200){
			alert(data.error_msg);
			$("#error_code").val(data.error_code);
			return true;
		}else{		

			if(data.setupAreaValidation == 'false') {
				block_page();
				$('#setupsAreaValidationDialog').children().remove();
				$('#setupsAreaValidationDialog').append(data.setupAreadialogMessageHtml);	
				$("#setupsAreaValidationDialog" ).dialog( "open" );
			} else {
				$("#criteria").submit();
			}			
		}  	     	       	         
	}); 
};

</script>

<div id="chart_area" class="chart_area">
	<div id="TiNavMenu"></div>
	<div id="chart" class="chart"></div>	
</div>

<div id="setupsAreaValidationDialog" title="Missing Entry Area"></div>

<table id="submit_chart_analysis">
  			<tr>
    			<td><input class="SubmitBtn" type="submit" formaction="../stratscan/strategies" value="Cancel" name="cancel"></td>
    		    <td style="text-align:right"><input class="SubmitBtn" type="button" onclick="btnSubmitClick()" value="Submit" id="btnSubmit" name="btnSubmit"></td>
  			</tr>
		</table>

</form>







    
