<head>
<link rel="stylesheet" type="text/css" href="..\assets\default\css\analysis_style.css" />
</head>

<script type="text/javascript">

$(function() {

	reset_screen_size('pageLoad', 'analysis');

  	$(window).resize(function() {
		reset_screen_size('resizeTrigger', 'analysis');
	});		

	block_page();

 	$( "#menu_drop" ).hide();
	$( "#strategy_menu" ).hide();
	$( "#chart_menu" ).hide();
	$( "#studies_menu" ).hide();
	$( "#addStudiesMenu" ).hide();
	$( "#setuparea_menu" ).hide();

	$("#strategy_menu_busy").css("visibility", "hidden");
	$("#studies_menu_busy").css("visibility", "hidden");
	$("#setuparea_menu_busy").css("visibility", "hidden");

	$("#UpBarsColor").colorpicker({showOn:"button"});
	$("#DownBarsColor").colorpicker({showOn:"button"});
	$("#volumeColor").colorpicker({showOn:"button"});

	//build the chart javascript 
	loadChart();
	
	//Edit page 
	//Strategy ID will be populated on page load 
	var strategy_id_val = $("#strategy_id").val();

	if (strategy_id_val == '') {
		//Force user navigation on strategy creation
		open_strategy_menu();			
	} else {
		$("#saved_ind").val('Y');
		var chart_id_val = $("#chart_id").val();		
		get_chart_data(chart_id_val);
	}
	
	$( "#strategy_menu_click" ).click(function() {		

		$( "#chart_menu" ).hide();
		$( "#studies_menu" ).hide();
		$( "#addStudiesMenu" ).hide();
		$( "#setuparea_menu" ).hide();

		var error_code = $("#error_code").val();

		if (error_code == 200) {

			strategy_id = $("#strategy_id").val();
			chart_id    = $("#chart_id").val();
			getset_all_dropdown_options(strategy_id, chart_id);
				
			unhover_chart_image();
			unhover_studies_image();
			unhover_entryarea_image();
		
			if ($("#strategy_menu").is(":hidden")) {

				$("#strategy_image").attr('src', '../assets/default/images/menu/strategy_selected.png');
				$(".menu_strategy_image").css({
					background: "#5f6f81"
				});

			} else {

				$("#strategy_image").attr('src', '../assets/default/images/menu/strategy_unselected.png');
				$(".menu_strategy_image").css({
					background: "#ffffff"
				});
	   	 	}	    		
			$( "#strategy_menu" ).toggle( 'slide', {direction: 'right'}, 100 );	
		} else {
			alert('Error has occurred. Please contact you systems administrator.');
		}	
	});	

	$( "#chart_menu_click" ).click(function() {

		var error_code = $("#error_code").val();

		if (error_code == 200) {

			var strategy_id = $("#strategy_id").val();
		
			if (strategy_id == '') {
				alert('Please save a strategy')
			} else {

				chart_id    = $("#chart_id").val();
				getset_all_dropdown_options(strategy_id, chart_id);
			
				$( "#strategy_menu" ).hide();
				$( "#studies_menu" ).hide();
				$( "#addStudiesMenu" ).hide();
				$( "#setuparea_menu" ).hide();
				unhover_strategy_image();
				unhover_studies_image();
				unhover_entryarea_image();

				if ($("#chart_menu").is(":hidden")) {

					$("#chart_image").attr('src', '../assets/default/images/menu/chart_selected.png');
					$(".menu_chart_image").css({
						background: "#5f6f81"
					});

				} else {
	
					$("#chart_image").attr('src', '../assets/default/images/menu/chart_unselected.png');
					$(".menu_chart_image").css({
						background: "#ffffff"
					});
		   	 }
				$( "#chart_menu" ).toggle( 'slide', {direction: 'right'}, 100 );
			}
		} else {
			alert('Error has occurred. Please contact you systems administrator.');
		}
	});	

	$( "#studies_menu_click" ).click(function() {

		var error_code = $("#error_code").val();

		if (error_code == 200) {

			var strategy_id = $("#strategy_id").val();
		
			if (strategy_id == '') {
				alert('Please save a strategy')
			} else {

				var chart_id = $("#chart_id").val();

				if (chart_id == '') {
					alert('Please save a chart first');
				} else {

					getset_all_dropdown_options(strategy_id, chart_id);
					$( "#strategy_menu" ).hide();
					$( "#chart_menu" ).hide();
					$( "#addStudiesMenu" ).hide();
					$( "#setuparea_menu" ).hide();
					unhover_chart_image();
					unhover_strategy_image();
					unhover_entryarea_image();

					if ($("#studies_menu").is(":hidden")) {

						$("#studies_image").attr('src', '../assets/default/images/menu/studies_selected.png');
						$(".menu_studies_image").css({background: "#5f6f81"});

					} else {
	
						$("#studies_image").attr('src', '../assets/default/images/menu/studies_unselected.png');
						$(".menu_studies_image").css({background: "#ffffff"});
		  	 		}
					$( "#studies_menu" ).toggle( 'slide', {direction: 'right'}, 100 );
				}
			}
		} else {
			alert('Error has occurred. Please contact you systems administrator.');
		}
	});	

	$( "#entryarea_menu_click" ).click(function() {

		var error_code = $("#error_code").val();

		if (error_code == 200) {

			var strategy_id = $("#strategy_id").val();
		
			if (strategy_id == '') {
				alert('Please save a strategy')
			} else {

				var chart_id = $("#chart_id").val();

				if (chart_id == '') {
					alert('Please save a chart first');
				} else {

					getset_all_dropdown_options(strategy_id, chart_id);
					$( "#strategy_menu" ).hide();
					$( "#chart_menu" ).hide();
					$( "#studies_menu" ).hide();
					$( "#addStudiesMenu" ).hide();
					unhover_chart_image();
					unhover_strategy_image();
					unhover_studies_image();

					if ($("#setuparea_menu").is(":hidden")) {

						$("#entryarea_image").attr('src', '../assets/default/images/menu/entry_area_selected.png');
						$(".menu_entryarea_image").css({background: "#5f6f81"});

					} else {

						$("#entryarea_image").attr('src', '../assets/default/images/menu/entry_area_unselected.png');
						$(".menu_entryarea_image").css({background: "#ffffff"});
		  	 		}
					$( "#setuparea_menu" ).toggle( 'slide', {direction: 'right'}, 100 );
				}
			}
		} else {
			alert('Error has occurred. Please contact you systems administrator.');
		}
	});					
});	

function menu_drop() {	
	var unblock_page = 'Y';
	menu_drop_item(unblock_page);
};	

function menu_add() {

	var error_code = $("#error_code").val();

	if (error_code == 200) {
		
		var data = $('#menu_add_item').data();		
		
		if (data["blockUI.isBlocked"] !== 1){	

			unhover_add();			
			$("#page").children().remove();
		
			var strategy_id_val = $("#strategy_id").val();
		
			$.ajax({
				type: "POST",
				url: "<?=base_url ()?>stratscan/get_newchart_timeframe_opt",
				data: "strategy_id="+ strategy_id_val,
				dataType: 'json'
			})
			.done(function(data ) {

				if (data.error_code != 200){

					alert(data.error_msg);
					$("#error_code").val(data.error_code);
					
					$("#timeframe").children().remove();
					$("#timeframe").append(data.timeframe_data);
		       		return true;

				} else {

					$("#timeframe").children().remove();
					$("#timeframe").append(data.timeframe_data);

					block_page();
				
					$( "#menu_drop" ).hide();
					$( "#strategy_menu" ).hide();
					$( "#chart_menu" ).hide();
					$( "#studies_menu" ).hide();
					$( "#addStudiesMenu" ).hide();

					$("#saved_ind").val('N');
					open_chart_menu();

					$("#chart_id").val('');			
					return true;
				}		
			});			
		}	
	}else {
		alert('Error has occurred. Please contact you systems administrator. ');
	}
};	


function menu_drop_item(unblock_page_ind){ 

	var strategy_id_val = $("#strategy_id").val();
 	var saved_ind       = $("#saved_ind").val();

	if (strategy_id_val == ''){
		alert('Please save a strategy first.')
	}else {

		if (saved_ind == 'N'){
			alert('Please save a chart first.')
		} else {
			$( "#strategy_menu" ).hide();	
			$( "#chart_menu" ).hide();
			$( "#studies_menu" ).hide();
			$( "#setuparea_menu" ).hide();
			$( "#addStudiesMenu" ).hide();				
			unhover_chart_image();
			unhover_strategy_image();
			unhover_studies_image();	
			unhover_entryarea_image();		
	
			$( "#menu_drop" ).toggle( 'slide', {direction: 'up'}, 100, function(){
			
				if ($("#menu_drop").is(":hidden")) {
					$(".slider_td").css({border:"none",	background:"none"});
					unhover_menu();	

					if (unblock_page_ind == 'Y'){
						unblock_page('analysis');			
					}					
				
		 		} else {		
 			
		 			block_page();
		 			$(".slider_td").css({ border:"1px solid #c6d0da", background:"#ffffff"});
				}		    
			});
		}
	}				
}

function open_strategy_menu() {
	
	$("#menu_drop" ).show();
	$("#strategy_menu").show();
	$("#chart_menu").hide();

	$(".slider_td").css({border:"1px solid #c6d0da", background:"#ffffff"});
	$(".menu_strategy_image").css({background: "#5f6f81"});
	$(".menu").css({background: "#5f6f81"});
	
	$("#strategy_image").attr('src', '../assets/default/images/menu/strategy_selected.png');
	$("#list_image").attr('src', '../assets/default/images/menu/list_w.svg');			
}

function open_chart_menu() {
	
	$("#menu_drop" ).show();
	$("#strategy_menu").hide();
	$("#chart_menu").show();
	
	unhover_strategy_image();
	unhover_studies_image();

	$(".slider_td").css({border:"1px solid #c6d0da", background:"#ffffff"});
	$(".menu_chart_image").css({background: "#5f6f81"});
	$(".menu").css({background: "#5f6f81"});

	$("#chart_image").attr('src', '../assets/default/images/menu/chart_selected.png');
	$("#list_image").attr('src', '../assets/default/images/menu/list_w.svg');			
}

function open_studies_menu() {
	
	$("#menu_drop" ).show();
	$("#strategy_menu").hide();
	$("#chart_menu").hide();
	$("#addStudiesMenu" ).hide();
	
	unhover_strategy_image();
	unhover_chart_image();
	$("#studies_menu").show();

	$(".slider_td").css({border:"1px solid #c6d0da", background:"#ffffff"});
	$(".menu_studies_image").css({background: "#5f6f81"});
	$(".menu").css({background: "#5f6f81"});	

	$("#studies_image").attr('src', '../assets/default/images/menu/studies_selected.png');
	$("#list_image").attr('src', '../assets/default/images/menu/list_w.svg');		
}
function open_setupArea_menu() {
	
	$("#menu_drop" ).show();
	$( "#setuparea_menu" ).show();
	
	$(".slider_td").css({border:"1px solid #c6d0da", background:"#ffffff"});
	$(".menu").css({background: "#5f6f81"});

	$("#entryarea_image").attr('src', '../assets/default/images/menu/entry_area_selected.png');
	$(".menu_entryarea_image").css({background: "#5f6f81"});

	$("#list_image").attr('src', '../assets/default/images/menu/list_w.svg');			
}

function exchange_onChange() {

	$("#strategy_menu_busy").css("visibility", "visible");
	var exchange_val = $("#exchange").val();
	
	$.ajax({
		type: "POST",
		url: "<?=base_url ()?>stratscan/ajax_exchange_change",
		data: "exchange_dd_value="+ exchange_val,
		dataType: 'json'
	})
	.done(function(data ) {
		
		if (data.error_code != 200){

			alert(data.error_msg);
			$("#error_code").val(data.error_code);
			
			$("#products").children().remove();
			$("#products").append(data.products);

			$("#contracts").children().remove();
			$("#contracts").append(data.contracts);

			//$("#min_date").text(data.contract_min_date);
			//$("#max_date").text(data.contract_max_date);

			$("#strategy_menu_busy").css("visibility", "hidden");
	        return true;

		} else {
			
			$("#products").children().remove();
			$("#products").append(data.products);

			$("#contracts").children().remove();
			$("#contracts").append(data.contracts);
		
//			$("#min_date").text(data.contract_min_date);
//			$("#max_date").text(data.contract_max_date);

			$("#strategy_menu_busy").css("visibility", "hidden");
			return true;	
		}		
						
	});	
}

function products_onChange() {

	$("#strategy_menu_busy").css("visibility", "visible");
	var products_val = $("#products").val();
	
	$.ajax({
		type: "POST",
		url: "<?=base_url ()?>stratscan/ajax_products_change",
		data: "products_dd_value="+ products_val,
		dataType: 'json'
	})
	.done(function(data ) {
		
		if (data.error_code != 200){

			alert(data.error_msg);
			$("#error_code").val(data.error_code);
			
			$("#contracts").children().remove();
			$("#contracts").append(data.data);

			//$("#min_date").text(data.contract_min_date);
			//$("#max_date").text(data.contract_max_date);

			$("#strategy_menu_busy").css("visibility", "hidden");
	        return true;

		} else {
			
			$("#contracts").children().remove();
			$("#contracts").append(data.data);

			//$("#min_date").text(data.contract_min_date);
			//$("#max_date").text(data.contract_max_date);

			$("#strategy_menu_busy").css("visibility", "hidden");
			return true;	
		}		
						
	});	
}

function contracts_onChange() {

	$("#strategy_menu_busy").css("visibility", "visible");	

	var contracts_val = $("#contracts").val();
	var exchange_val = $("#exchange").val();

	$.ajax({
		type: "POST",
		url: "<?=base_url ()?>stratscan/ajax_contracts_change",
		data: "contracts_dd_value="+ contracts_val+"&exchange_dd_value="+ exchange_val,
		dataType: 'json'
	})
	.done(function(data ) {

		if (data.error_code != 200){

			alert(data.error_msg);
			$("#error_code").val(data.error_code);

			//$("#min_date").text(data.contract_min_date);
			//$("#max_date").text(data.contract_max_date);

			$("#strategy_menu_busy").css("visibility", "hidden");
	        return true;

		} else {

			//$("#min_date").text(data.contract_min_date);
			//$("#max_date").text(data.contract_max_date);
			
			$("#strategy_menu_busy").css("visibility", "hidden");
			return true;			
		}		
	});	
}

function save_strategy_btn_click() {

	var error_code = $("#error_code").val();

	if (error_code == 200) {
	
		$("#strategy_menu_busy").css("visibility", "visible");

		if ($('#sync_option').is(":checked")) {
			var sync_opt    = 'Y';
			var sync_type    = $("#sync_type").val();		
		}else {
			var sync_opt    = 'N';
			var sync_type    = $("#sync_type").val();		
		}

		var exchange_val    = $("#exchange").val();
		var products_val    = $("#products").val();
		var contracts_val   = $("#contracts").val();
		var strategy_id_val = $("#strategy_id").val();
		var chart_id_val    = $("#chart_id").val();
		
		$.ajax({
			type: "POST",
			url: "<?=base_url()?>stratscan/save_strategy",
			data: "exchange_dd_value="+exchange_val+"&products_dd_value="+products_val+"&contracts_dd_value="+contracts_val+"&strategy_id_val="+strategy_id_val+"&chart_id_val="+chart_id_val+"&sync_opt="+sync_opt+"&sync_type="+sync_type,
			dataType: 'json'
		})
		.done(function(data ) {

			if (data.error_code != 200){

				alert(data.error_msg);
				$("#error_code").val(data.error_code);

				$("#strategy_id").val(data.strategy_id);
				$("#saved_ind").val('N');

				$("#timeframe").children().remove();
				$("#timeframe").append(data.timeframe_data);

				$("#strategy_menu_busy").css("visibility", "hidden");
			
	        	return true;

			} else {

				$("#strategy_id").val(data.strategy_id);
				
				$("#timeframe").children().remove();
				$("#timeframe").append(data.timeframe_data);

				$("#strategy_menu_busy").css("visibility", "hidden");

				var saved_ind   = $("#saved_ind").val();

				if (saved_ind == 'N') {
		    		open_chart_menu();
		    	} else {		    		
		    		menu_drop();	
		    		get_chart_data(chart_id_val);	
		    	}				
				
				return true;
			}
		});	
	} else {
		alert('Error has occurred. Please contact you systems administrator.');
		$("#strategy_menu_busy").css("visibility", "hidden");
	}
}

function save_chart_btn_click() {

	var error_code = $("#error_code").val();

	$("#saved_ind").val('Y');
	var unblock_page = 'N';
	menu_drop_item(unblock_page);		
	
	if (error_code == 200) {

		var strategy_id_val = $("#strategy_id").val();
		var timeframe_val   = $("#timeframe").val();
		var chart_type_val  = $("#chart_type").val();
		var themes_val      = $("#themes").val();
		var min_date_val    = $("#min_date").text();
		var max_date_val    = $("#max_date").text();
		var chart_id_val    = $("#chart_id").val();
		var disp_vol        = $('input:radio[name=disp_volume]:checked').val();
		var UpBarsColor     = $("#UpBarsColor").val();
		var DownBarsColor   = $("#DownBarsColor").val();
		var volumeColor     = $("#volumeColor").val();

		$.ajax({
			type: "POST",
			url: "<?=base_url ()?>stratscan/save_chart",
			data: "strategy_id="+ strategy_id_val+"&timeframe="+ timeframe_val+"&chart_type="+ chart_type_val+"&themes="+ themes_val+"&min_date="+ min_date_val+"&max_date="+ max_date_val+"&chart_id="+ chart_id_val+"&disp_vol="+ disp_vol+"&UpBarsColor="+ UpBarsColor+"&DownBarsColor="+ DownBarsColor+"&volumeColor="+ volumeColor,
			dataType: 'json'
		})
		.done(function(data ) {	

			if (data.error_code != 200){

				alert(data.error_msg);
				$("#error_code").val(data.error_code);

				$("#chart_id").val('');
				$("#saved_ind").val('N');

				$("#timeframe").children().remove();
				$("#timeframe").append(data.timeframe_data);

	        	return true;

			} else {
				
				$(".tr_menu_static").remove();				
				$(".main_menu").prepend(data.menu);
				block_page();
				if (data.ChartMethod == 'SAVE') {
					var $secondRow = $("#main_menu tr:eq(1)");	
					$secondRow.find('td:eq(1)').after('<td id="spacer" style="border:none"></td>');	
				}									
			
				$("#chart_id").val(data.chart_id);							
				$("#timeframe").children().remove();
				$("#timeframe").append(data.timeframe_data);
			
				get_chart_data(data.chart_id);
				SetupAreaMenu(data.chart_id);	
									
				return true;
			}		
		});	
	} else {
		alert('Error has occurred. Please contact you systems administrator.');
	}	
}

function saved_chart_onclick(e)
{
	block_page();
	
	var error_code = $("#error_code").val();

	$("#setupsAreaValidationDialog" ).dialog( "close" );

	if (error_code == 200) {
		
		var InputVal    = e.id;
		var Chartid     = InputVal.replace(/[chart_id]/g, "");
		var StrategyId  = $("#strategy_id").val();

		get_chart_data(Chartid);		
		SetupAreaMenu(Chartid);			
		getset_all_dropdown_options (StrategyId, Chartid);
			
		$(".charts").css({background: "#ffffff",color:"#5f6f81"});
		$(e).css({background: "#5f6f81",color:"#ffffff"});

	} else {
		alert('Error has occurred. Please contact you systems administrator.');
	}
}

function getset_all_dropdown_options (StrategyId, chartId) {

	$("#strategy_menu_busy").css("visibility", "visible");
	$("#studies_menu_busy").css("visibility", "visible");
	$("#setuparea_menu_busy").css("visibility", "visible");

	$("#study_id").val('');
	$("#edit_study").children().remove();
	
	$.ajax({
		type: "POST",
		url: "<?= base_url ()?>stratscan/get_menu_options",
		data: "strategy_id="+ StrategyId+"&chart_id="+ chartId,
		dataType: 'json'
	})
	.done(function(data ) {
		
		if (chartId == '') {

			if (data.error_code != 200){

				alert(data.error_msg);
				$("#error_code").val(data.error_code);

			} else {

				$("#products").children().remove();
				$("#products").append(data.product_opt);
				
				$("#contracts").children().remove();
				$("#contracts").append(data.contract_opt);

				if (data.syncInd == 'Y'){
					$('#sync_option').prop('checked', true);
				}else{
					$('#sync_option').prop('checked', false);
				}

				$("#sync_type").val(data.syncType);
				$("#exchange").val(data.exchange);
				$("#products").val(data.product);
				$("#contracts").val(data.contract);
				$("#studies_menu_detail").children().remove(); 
				$("#studies_menu_detail").append(data.used_studies);

				$("#timeframe").children().remove();
				$("#timeframe").append(data.timeframe_data); 	
			}			

			$("#strategy_menu_busy").css("visibility", "hidden");
			$("#studies_menu_busy").css("visibility", "hidden");
			$("#setuparea_menu_busy").css("visibility", "hidden");
			return true;
			
		} else {

			if (data.error_code != 200){

				alert(data.error_msg);
				$("#error_code").val(data.error_code);

			} else {

				$("#products").children().remove();
				$("#products").append(data.product_opt);
				
				$("#contracts").children().remove();
				$("#contracts").append(data.contract_opt);
			
				$("#chart_id").val(data.chart_id);

				if (data.syncInd == 'Y'){
					$('#sync_option').prop('checked', true);
				}else{
					$('#sync_option').prop('checked', false);
				}

				$("#sync_type").val(data.syncType);
				$("#exchange").val(data.exchange);
				$("#products").val(data.product);
				$("#contracts").val(data.contract);
				$("#min_date").val(data.min_date);
				$("#max_date").val(data.max_date_range);
				$("#chart_type").val(data.chartType);
				$('input[name=disp_volume][value='+data.showVolume+']').prop('checked', true);
				$("#volumeColor").colorpicker({color: data.volumeColour});	
				$("#UpBarsColor").colorpicker({color: data.positiveColour});
				$("#DownBarsColor").colorpicker({color: data.negativeColour});	

				$("#timeframe").children().remove();
				$("#timeframe").append(data.timeframe_data);

				$("#studies_menu_detail").children().remove(); 
				$("#studies_menu_detail").append(data.used_studies); 		
			}
			$("#strategy_menu_busy").css("visibility", "hidden");
			$("#studies_menu_busy").css("visibility", "hidden");
			$("#setuparea_menu_busy").css("visibility", "hidden");
			return true;
		}			
	});
}

function cancel_strategy_btn_click() {

	var saved_ind   = $("#saved_ind").val();	
	
	if (saved_ind == 'N') {
		window.location.href = "<?=base_url ()?>stratscan/strategies";
	} else {
		menu_drop();		
	}
}

function cancel_chart_btn_click() {	

	var saved_ind = $("#saved_ind").val();
	
	 if (saved_ind == 'Y') {		 
		 menu_drop();
	 }else{
		 $( "#chart_menu" ).hide();
		 unhover_chart_image();
		 open_strategy_menu();
	 }	
}

function cancel_studies_btn_click() {	
	 unblock_page('analysis');
	 menu_drop();
}

function next_click_submit() {

	var error_code = $("#error_code").val();

	if (error_code == 200) {
		document.ChartAnalysis.submit();
	} else {
		alert('Error has occurred. Please contact you systems administrator.');
	}
}

function showAddStudiesMenu(){
	 
	var chartId = $("#chart_id").val();
	$( "#studies_menu" ).hide();

		$.ajax({
			type: "POST",
			url: "<?= base_url ()?>stratscan/showAddStudiesMenu",
			data: "chartId="+ chartId,
			dataType: 'json'
		})
		.done(function(data ) {

			$("#addStudiesMenu").children().remove();
			$("#addStudiesMenu").append(data.AddStudyMenuHtml);
	 		$("#addStudiesMenu").show();
		});
 }

 function create_study_code_HTMLmenu(add_study){
	 $("#add_study_details").children().remove(); 
	
	 var chart_id    = $("#chart_id").val();
	 
	 if (add_study !== '0') {

			$.ajax({
				type: "POST",
				url: "<?=base_url ()?>stratscan/get_study_code_HTMLmenu",
				data: "study_code="+ add_study+"&chart_id="+ chart_id,
				dataType: 'json'
			})
			.done(function(data ) {

				$("#add_study_details").append(data.study_code_HTML);
				$( "#add_study_details" ).show();
				return true;						
			});		
	 }	
 }

 function remove_study(study_id){

	 $("#studies_menu_busy").css("visibility", "visible");
	 var chartId    = $("#chart_id").val();
	 var StrategyId  = $("#strategy_id").val();
	 
	 $.ajax({
			type: "POST",
			url: "<?=base_url ()?>stratscan/remove_study",
			data: "studyId="+ study_id,
			dataType: 'json'
	 })
	 .done(function(data ) {

		var unblock_page = 'Y';
		menu_drop_item(unblock_page);

		removeStudy(study_id);
		getset_all_dropdown_options(StrategyId, chartId);
	 	$("#studies_menu_busy").css("visibility", "hidden");
	 	return true;					
	 });		 
 } 

function edit_study_link(p_StudyId) {

	$("#studies_menu_busy").css("visibility", "visible");
	$("#edit_study").children().remove();
	var chartId    = $("#chart_id").val();

	$.ajax({
		type: "POST",
		url: "<?=base_url ()?>stratscan/edit_study",
		data: "StudyId="+p_StudyId+"&chartId="+chartId,
		dataType: 'json'
	})
	.done(function(data ) {

		if (data.error_code != 200){
			alert(data.error_msg);
			$("#error_code").val(data.error_code);			
		} else {
			$("#edit_study").children().remove();
			$("#edit_study").append(data.html);	
			$("#study_id").val(p_StudyId);					
		}	
		$("#studies_menu_busy").css("visibility", "hidden");	 						
	});
}

//p_currentChart == 'true'  ( Then the chart is being deleted from the menu "Chart" , p_chartId is not populated ) 
//p_currentChart == 'false' ( Then the chart is being deleted from setupsAreaValidationDialog  , p_chartId is populated )
function DeleteChartId(p_currentChart, p_chartId){

	var StrategyId  = $("#strategy_id").val();

	if (p_currentChart == 'true') {
		var ChartId     = $("#chart_id").val();
	}else {
		var ChartId     = p_chartId;
		$("#setupsAreaValidationDialog" ).dialog( "close" );
	}
	
	if (ChartId == ''){
		alert('No Chart has been saved yet.');
	} else {

		var r = confirm("Do you want to delete this chart?");
		if (r == true) {
			
			$.ajax({
				type: "POST",
				url: "<?=base_url ()?>stratscan/DeleteChartId",
				data: "StrategyId="+StrategyId+"&ChartId="+ChartId,
				dataType: 'json'
			})
			.done(function(data ) {
	
				if (data.error_code != 200){
					alert(data.error_msg);
					$("#error_code").val(data.error_code);			
				} else {

					$(".tr_menu_static").remove();
					$(".main_menu").prepend(data.menu);
					block_page();

					$("#spacer").remove();

					if(data.ChartId != null) {

						if (p_currentChart == 'true') {
							var unblock_page = 'N';
							menu_drop_item(unblock_page);
						}
						$("#chart_id").val(data.ChartId);
						get_chart_data(data.ChartId);
						getset_all_dropdown_options (StrategyId, data.ChartId);
						
					} else {						
						getset_all_dropdown_options (StrategyId, '');
						clearChart();	
						
						if (p_currentChart == 'false') {
							var unblock_page = 'N';
							menu_drop_item(unblock_page);
							open_chart_menu();
						}	

						$("#chart_id").val('');
						$("#saved_ind").val('N');
										
					}								
				}					 						
			});
		} 			
	}	
}

function setupAreaSelection(p_type, p_function, p_selection){

	switch(p_type) {
    	case 'Entry':
			var setupAreaEntryFromlbl = $("#setupAreaEntryFromlbl").text();
    		var setupAreaEntryColor   = $("#setupAreaEntryColor").val();
            
    		if(p_function == 'Delete'){ 
    			$("#setupAreaEntryFrom").val('');
    			$("#setupAreaEntryTo").val('');
    			addAreaGuide(p_type);
    			postSetupAreaSelection('', '', p_type, '');
    		}else{

        		if (p_selection == 'range') {
        			var unblock_page = 'Y';
        			menu_drop_item(unblock_page);          			       			     		
        			selectRange(p_type, setupAreaEntryColor, setupAreaEntryFromlbl);        
        		} else {

        	   		var setupAreaEntryFrom   = $("#setupAreaEntryFrom").val();

        	   		if (setupAreaEntryFrom == ''){            	   		
            			var unblock_page = 'Y';
            			menu_drop_item(unblock_page);    		        		
            			selectRange(p_type, setupAreaEntryColor, setupAreaEntryFromlbl);
        	   		}else { 
                		colourAreaGuide(p_type, setupAreaEntryColor)
             			postSetupAreaSelection('ColorOnly', 'ColorOnly', p_type, setupAreaEntryColor);	
	       	   		} 				
        		}
			}	
        	break;        	
	}	
}

function SetupAreaMenu(p_chart_id){

	$("#setuparea_menu_busy").css("visibility", "visible");

	$.ajax({
		type: "POST",
		 url: "<?=base_url ()?>stratscan/SetupAreasMenu",
        data: "chartId="+ p_chart_id,
	dataType: 'json'
	})
	.done(function(data) {
		if (data.error_code != 200){
			alert(data.error_msg);
			$("#error_code").val(data.error_code);
			return true;
		}else{	
			$('#SetupAreaMenuHtml').children().remove();
			$('#SetupAreaMenuHtml').append(data.menuHTML);
		}  	     	       	         
	}); 
	
}

function postSetupAreaSelection (p_fromDate, p_toDate, p_areaType, p_colour){

	var ChartId    = $("#chart_id").val();

	if(p_fromDate == '' || p_fromDate === undefined) {
		var f_fromDate = '';
		var f_toDate   = '';		
	} else {
		block_page();
		if(p_fromDate == 'ColorOnly') {
			var f_fromDate = p_fromDate;
			var f_toDate   = p_toDate;		
		}else{
			var f_fromDate = p_fromDate.getTime();
			var f_toDate   = p_toDate.getTime();
		}	
	}

	$.ajax({
		type: "POST",
		url: "<?=base_url ()?>stratscan/postSetupAreaSelection",
		data: "chartId="+ChartId+"&fromDate="+f_fromDate+"&toDate="+f_toDate+"&colour="+p_colour+"&type="+p_areaType,
		dataType: 'json'
	})
	.done(function(data ) { 

		if (data.error_code != 200){
			alert(data.error_msg);
			$("#error_code").val(data.error_code);			
		} else {

			switch(p_areaType) {
    			case 'Entry':	
					$("#setupAreaEntryFrom").val(data.fromDate);
					$("#setupAreaEntryTo").val(data.toDate);
				    break;
		}
			open_setupArea_menu();
		}		 	
	});	
}

</script>

<html>    
<form action="../stratscan/criteria" name="criteria" id="criteria" method="post">  
   
<input type="hidden" name="error_code"  id="error_code"  value="<?php echo $error_code?>" />
<input type="hidden" name="strategy_id" id="strategy_id" value="<?php echo $strategy_id?>"/>
<input type="hidden" name="chart_id"    id="chart_id"    value="<?php echo $chart_id?>" />
<input type="hidden" name="saved_ind"   id="saved_ind"   value="<?php echo $saved_ind?>" />
<input type="hidden" name="study_id"    id="study_id"    value="" />
	
	<table class="main_menu" id="main_menu" style='width:100%'>
		<tr class="tr_menu_static">
			<td id="menu_strategy_name" title="<?php echo $strategy_desc?>"><label style="font-size:20px; color:#5f6f81; padding-right:7px" for="strategy_name"><?php echo $strategy_name?></label></td>
			<td class="add" id="menu_add_item" onclick="menu_add();" onmouseover="hover_add();" onmouseout="unhover_add();"><img id="add_image" src="../assets/default/images/menu/plus_c.svg" title="Add TimeFrame"/></td>			
			 <?php echo $menu?>	
			<td class="menu" id="menu_drop_item" onclick="menu_drop();" onmouseover="hover_menu();" onmouseout="unhover_menu();"><img id="list_image" src="../assets/default/images/menu/list_c.svg" title="Show/Hide Menu"/></td>
		</tr>
		<tr >
			<td style="border:none" ></td>	
			<td style="border:none" ></td>
			 <?php echo $menu_spacer?>	
			<td class="slider_td" style="border:none;">
			
				<div id="menu_drop" class="menu_drop">
		
					<table class="analysisMenu" >
						<tr style="height: 10%" >
							<td class="menu_strategy_image" id="strategy_menu_click" onmouseover="hover_strategy_image();" onmouseout="unhover_strategy_image();" style="border:none; padding:10px;" >
								<img id="strategy_image" src="../assets/default/images/menu/strategy_unselected.png" style="width:21px; height:auto"/>
							</td>
						</tr>
						<tr style="height: 10%">
							<td class="menu_chart_image" id="chart_menu_click" onmouseover="hover_chart_image();" onmouseout="unhover_chart_image();" style="border:none">
								<img id="chart_image" src="../assets/default/images/menu/chart_unselected.png" style="width:21px; height:auto; padding-top:10px; padding-bottom:10px"/>
							</td>
						</tr>
						<tr style="height: 10%">
							<td class="menu_studies_image" id="studies_menu_click" onmouseover="hover_studies_image();" onmouseout="unhover_studies_image();" style="border:none">
								<img id="studies_image" src="../assets/default/images/menu/studies_unselected.png" style="width:21px; height:auto; padding-top:10px; padding-bottom:10px"/>
							</td>
						</tr>
						<tr style="height: 10%">
							<td class="menu_entryarea_image" id="entryarea_menu_click" onmouseover="hover_entryarea_image();" onmouseout="unhover_entryarea_image();" style="border:none">
								<img id="entryarea_image" src="../assets/default/images/menu/entry_area_unselected.png" style="width:21px; height:auto; padding-top:10px; padding-bottom:10px"/>
							</td>
						</tr>
						<tr style="height: 60%"> </tr>
					</table>
					
					<table id="analysisStaticMenu" class="analysisStaticMenu">
						<tr>
							<td class="menu_cog" style="border:none; padding:10px" onmouseover="hover_cog();" onmouseout="unhover_cog();">
								<img id="cog_image" src="../assets/default/images/menu/cog_c.svg" title="Settings"/>
							</td>
						</tr>
						<tr>
							<td class="menu_exit" style="border:none; padding:10px" onmouseover="hover_exit();" onmouseout="unhover_exit();" onclick="window.location.href = '<?=base_url()?>stratscan/logout'">
								<img id="exit_image" src="../assets/default/images/menu/exit_c.svg" title="Logout"/>
							</td>
						</tr>
					</table>
		
				</div>
	
			</td>
			
		</tr>
	</table>
	
	<div id="strategy_menu" class="strategy_menu" >	    
	
		<table class="strategy_menu_title">
			<tr>
				<td>
					<h3>Strategy Options</h3>
				</td>
				<td align="right">
					<img id="strategy_menu_busy" title="busy" alt="busy..." src="../assets/default/images/content/ajax-loader.gif" style="margin-bottom:9px"/>
				</td>
			</tr>
		</table>
		
		<hr>
		<br>
		
		<table class="strategy_menu_detail">
			<tr>
			    <td>
					<label for="exchange">Exchange:</label>
				</td>				
			</tr>
			<tr>
			    <td>
					<?php echo form_dropdown('exchange', $exchange_opt, $sel_exchange_opt, $exchange_dd_js);?>
				</td>				
			</tr>
			<tr>
				<td>
					<label for="products">Products:</label>
				</td>							
			</tr>
			<tr>
				<td colspan="2"> 					
					<?php echo form_dropdown('products', $product_opt, $sel_product_opt, $products_dd_js);?>
				</td>				
			</tr>
			<tr>
			 	<td>
					<label for="contracts">Contracts:</label>
				</td>							
			</tr>
			<tr>
				<td colspan="2">
					<?php echo form_dropdown('contracts', $contract_opt, $sel_contract_opt ,$contracts_dd_js);?>
				</td>			
			</tr>
		</table>
		
		<br>
		
		<table class="strategy_menu_title">
			<tr>
				<td>
					<h3>Synchronize Charts</h3>
				</td>
			</tr>
		</table>
		
		<hr>
		<br>
		
		<table class="strategy_menu_detail">
			<tr>
		    	<td>
					<input type="checkbox" id="sync_option" name="sync_option" checked> <label style="display:inline">Synchronize Chart Positions</label>
				</td>				
			</tr>
			<tr>
				<td>
					<label for=sync_type style="margin-top:10px">Synchronize Type:</label>
				</td>
			</tr>
			<tr>
				<td>
					<select id="sync_type" style="width:250px">
  						<option value="start">Start</option>
  						<option value="centre">Centre</option>
  						<option value="end">End</option>
					</select>
				</td>
			</tr>
		</table>
			
		<br>
		
		<table class="strategy_menu_btn">
			<tr>
				<td>
					<button class="SubmitBtn" type="button" id="save_strategy_btn" onclick="save_strategy_btn_click()">Save</button>
				</td>
				<td align="right">
					<button class="SubmitBtn" type="button" id="cancel_strategy_btn" onclick="cancel_strategy_btn_click()">Cancel</button>
				</td>
			</tr>
		</table>
		
	</div>	
	
	<div id="chart_menu" class="chart_menu">
	
		<table class="chart_menu_title">
			<tr>
				<td>
					<h3>Chart Data</h3>
				</td>
				<td align="right">
					<a href="#" onclick="DeleteChartId('true');"><img id="DelChartId" title="Delete Chart" alt="busy..." src="../assets/default/images/content/remove.svg" width="20" height="20"/></a>
				</td>
			</tr>
		</table>
		
	    <hr>
		<br>
		
		
		<table class="chart_menu_detail">
			<tr>
			 	<td>
					<label for="timeframe">Time Frame:</label>
				</td>				
			</tr>
			<tr>
				<td>
					<?php echo form_dropdown('timeframe', $timeframe_opt, $sel_timeframe_opt, $timeframe_dd_js);?>
				</td>
			</tr>
		</table>
		
		<div class="uiLightness">
		<table class="chart_menu_detail" style="width:100%;">
			<tr>
			 	<td width=33%>
					<label for="disp_volume">Volume:</label>
				</td>	
				<td width=10%>
					<input type="hidden" id="volumeColor" value="#69BFFF"/>
				</td>			
			</tr>
			<tr>
				<td>
					<input type="radio" name="disp_volume" value='Y'> Show Volume<br>
					<input type="radio" name="disp_volume" value='N' checked> Hide Volume
				</td>
			</tr>
		</table>
		</div>
		
		<br>
		<h3>Visual Options</h3>
		<hr>
		<br>
		
		<table class="chart_menu_detail">
			<tr>
			 	<td>
					<label for="chart_type">Chart Type:</label>
				</td>			
			</tr>
			<tr>
				<td>
					<?php echo form_dropdown('chart_type', $chart_type_opt, $sel_chart_type_opt, $chart_type_dd_js);?>
				</td>
			</tr>
		</table>
		
		<div class="uiLightness">
		<table class="chart_menu_detail" style="width:100%;">
			<tr>
				<td width=33% style="text-align:right">
					<label for="UpBarsColor">Up Bars</label>
				</td>
				<td width=10%>
					<input type="hidden" id="UpBarsColor" value="#33CC33" />
				</td>
			</tr>
			<tr>
				<td width=33% style="text-align:right">
					<label for="DownBarsColor">Down Bars</label>
				</td>
				<td width=10%>
					<input type="hidden" id="DownBarsColor" value="#FF0000" />
				</td>
			</tr>
		</table>
		</div>
		
		<table class="chart_menu_detail">
			<tr>
			 	<td>
					<label for="Themes">Themes:</label>
				</td>	
			</tr>
			<tr>
				<td>
					<?php echo form_dropdown('themes', $theme_opt, $sel_theme_opt, $theme_dd_js);?>
				</td>
			</tr>			
		</table>
				
		<br>		
		<table class="chart_menu_btn">
			<tr>
				<td>
					<button class="SubmitBtn" type="button" id="save_chart_btn" onclick="save_chart_btn_click()">Save</button>
				</td>
				<td align="right">
					<button class="SubmitBtn" type="button" id="cancel_chart_btn" onclick="cancel_chart_btn_click()">Cancel</button>
				</td>
			</tr>
		</table>		
	</div>	
	
	<div id="studies_menu" class="studies_menu">
		
		<table class="studies_menu_title">
			<tr>
				<td>
					<h3>Existing Studies</h3>
				</td>
				<td align="right">
					<img id="studies_menu_busy" title="busy" alt="busy..." src="../assets/default/images/content/ajax-loader.gif" style="margin-bottom:9px"/>
				</td>
			</tr>
		</table>
	    <hr>
	    <br>	
	    <div id="studies_menu_detail"><?php echo $studies_menu_detail?></div>
	    <br>
	    <table class="studies_menu_btn">
			<tr>
				<td>
					<button class="SubmitBtn" type="button" onclick="showAddStudiesMenu()">Add</button>
				</td>
				<td align="right">
					<button class="SubmitBtn" type="button" id="cancel_studies_btn" onclick="cancel_studies_btn_click()">Cancel</button>
				</td>
			</tr>
		</table>
		
		<br>
		<div id="edit_study" >
		</div>			
	</div>	    
	<div id="addStudiesMenu" class="addStudiesMenu">
	
			   
	</div>
	<div id="setuparea_menu" class="setuparea_menu">	
		<table class="setuparea_menu_title">
			<tr>
				<td>
					<h3>Trade Setup Area</h3>
				</td>
				<td align="right">
					<img id="setuparea_menu_busy" title="busy" alt="busy..." src="../assets/default/images/content/ajax-loader.gif" style="margin-bottom:9px"/>
				</td>
			</tr>
		</table>
	    <hr>
	    <br>
	    <div id="SetupAreaMenuHtml"><?php echo $menuHTML?></div>	
	</div>	
</html>