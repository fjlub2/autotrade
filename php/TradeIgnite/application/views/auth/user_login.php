<html>
<br>

<?php echo form_open('auth')?>

<h1>Login</h1>
<br>
<?php echo validation_errors(); ?>
<br>
<table style="width: 70%">
	<tr>
		<td style="width: 10%">
			<label for="username">User Name:</label>
		</td>
		<td style="width: 90%">
            <input type="text" id="auth_username" name="auth_username" style="width:30%" value="<?php echo set_value('auth_username'); ?>" />
        </td>
	</tr>
	<tr>
		<td style="width: 10%">
			<label for="password">Password:</label>
		</td>
		<td style="width: 90%">
            <input type="password" id="auth_password" name="auth_password" style="width:30%">
        </td>
	</tr>
	<tr>
    	<td colspan="3">
            <input type="checkbox">
            <label class="check" for="checkbox">Keep me logged in</label>
        </td>
   	</tr>
</table>
<br>
<input type="submit" value="Login">

</html>



