<!DOCTYPE html>

      <html lang="en-US">

<link rel="icon" 
      type="image/png" 
      href="assets\default\images\header\favicon.ico">

   <head>
   
   		<meta http-equiv="Content-Type" content="text/html;"/>
		<meta http-equiv="Content-Language" content="en-us" />
		<meta name="ROBOTS" content="NONE" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
   
      	<title>TradeIgnite</title>
      	
      	<link rel="stylesheet" type="text/css" href="assets\default\css\default_style.css" />
      	<link rel="stylesheet" type="text/css" href="assets\default\css\bootstrap.min.css" />
      	<link rel="stylesheet" type="text/css" href="assets\default\css\jquery-ui.css" />    
     
   </head>
   
   
   
   <body>
   
      <div id="wrapper">
      
         <div id="header">
      			
            <?php echo $header ?>
            
         </div>
         
         <div id="main">
         
            <div id="content">
               
               <div class="post">
                  <?php echo $content ?>
               </div>
               
            </div>
            
            <div id="sidebar">
               <?php echo $sidebar ?>
            </div>
            
         </div>
         
         <div id="footer">
            <?php echo $footer ?>
         </div>
         
      </div>
      
   </body>
   
</html>