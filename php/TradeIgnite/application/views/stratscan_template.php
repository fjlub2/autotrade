<!DOCTYPE html>

      <html lang="en-US">

<link rel="icon" 
      type="image/png" 
      href="..\assets\default\images\header\favicon.ico">

   <head>
   
   		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="Content-Language" content="en-us" />
		<meta name="ROBOTS" content="NONE" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
   
      	<title>StratScan</title>
      	
      	<link rel="stylesheet" type="text/css" href="..\assets\default\css\stratscan_style.css" />
      	<link rel="stylesheet" type="text/css" href="..\assets\default\css\bootstrap.min.css" />
       	<link rel="stylesheet" type="text/css" href="..\assets\default\css\jquery.dataTables.css" />
      	<link rel="stylesheet" type="text/css" href="..\assets\default\css\amCharts.css" />
      	<link rel="stylesheet" type="text/css" href="..\assets\default\css\jquery.scombobox.css" />
      	<link rel="stylesheet" type="text/css" href="..\assets\default\css\colorpicker.min.css" />
      	<link rel="stylesheet" type="text/css" href="..\assets\default\css\ThemeUiLightness.css" />
      	<link rel="stylesheet" type="text/css" href="..\assets\default\css\ThemeSmoothness.css" /> 
      	<link rel="stylesheet" type="text/css" href="..\assets\default\css\jquery-ui.css" />
      	
       	<script type="text/javascript" src="..\assets\default\script\jquery.js"></script>
        <script type="text/javascript" src="..\assets\default\script\jquery-ui.min.js"></script>  
        <script type="text/javascript" src="..\assets\default\script\blockUI.js"></script> 
        <script type="text/javascript" src="..\assets\default\script\jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="..\assets\default\script\jquery.scombobox.min.js"></script> 
        <script type="text/javascript" src="..\assets\default\script\missed.js"></script> 
        <script type="text/javascript" src="..\assets\default\script\jquery.easing.min.js"></script> 
        <script type="text/javascript" src="..\assets\default\script\colorpicker.min.js"></script> 
        
        <!-- AM Charts -->
        <script type="text/javascript" src="..\assets\default\script\amcharts.js"></script>
        <script type="text/javascript" src="..\assets\default\script\serial.js"></script>
        <script type="text/javascript" src="..\assets\default\script\amstock.js"></script> 
        <script type="text/javascript" src="..\assets\default\script\responsive.min.js" ></script> 
        
        <!-- TradeIgnite libraries -->
        <script src="..\assets\default\script\TiBlockUnblockPage.js"></script>
        <script src="..\assets\default\script\TiResize.js"></script>
        <script src="..\assets\default\script\TiCssHover.js"></script>
           
   </head>
   
   <body>     
      
         <div id="header">			
			       
            <?php echo $header ?>
            
         </div>
         
         <div id="main" class="overlay">
         
            <div id="content" class="content">               
               
                 <?php echo $content ?>
                              
            </div>
                       
         </div>
         
   </body>
   
</html>