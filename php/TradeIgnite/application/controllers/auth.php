<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

/**
 * Default controller loaded by routes configuration
 *
 * @package		auth.php
 * @author		Frans Lubbe
 * @copyright	Copyright (c) 2014, TradeIgnite Pty Ltd
 * @license
 * @link		http://tradeignite.com
 * @since		Version 1.0
 * @filesource
 */	
	
var $username;
/*
 * ------------------------------------------------------
 *  Load the controllers objects
 * ------------------------------------------------------
 */
function __construct() {
	
	parent::__construct();
		
	$this->config->load ( 'auth_config' );
	$this->load->model ( 'auth_model' );
	$this->load->library('user_agent');			
}
/*
 * ------------------------------------------------------
 *  Build login page incl. validation
 * ------------------------------------------------------
 */	
public function index() {

	if ($this->form_validation->run('user_login') == FALSE) {
		$this->template->write_view('content', 'auth/user_login');
		$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);
	}else {
		redirect('/stratscan/strategies', 'return');
	}
}
/*
 * ------------------------------------------------------
 *  Form validation call
 *  Validation array 'user_login', field 'auth_username'
 * ------------------------------------------------------
 */	
public function authenticate_username($str) {
	$this->username = $str;		
}
/*
 * ------------------------------------------------------
 *  Form validation call
 *  Builds client machine parameters
 *  Validation array 'user_login', field 'auth_password'
 * ------------------------------------------------------
 */	
public function authenticate_credentials($str) {

	$out_params = array();
	$data['auth_api'] = $this->config->item ( 'auth_api' );
		
	if ($this->agent->is_browser())	{
		$agent = $this->agent->browser().' '.$this->agent->version();
	} elseif ($this->agent->is_robot()) {
		$agent = $this->agent->robot();
	} elseif ($this->agent->is_mobile()) {
		$agent = $this->agent->mobile();
	} else {
		$agent = 'Unidentified User Agent';
	}
		
	$userLocale = $this->auth_model->GetLanguageCodeISO6391();
		
	$data['user_agent'] = Array(
		"agent"      => $agent,
		"platform"   => $this->agent->platform(),
		"referrer"   => $this->agent->referrer(),
		"userIp"     => $_SERVER["REMOTE_ADDR"],
		"userLocale" => $userLocale
	);
		
	$data['username'] = $this->username;
	$data['password'] = $str;
		
	$this->auth_model->authenticate_credentials ( $data, $out_params);		
		
	if ($out_params['auth_code'] == 200) {
		$this->session->set_userdata($out_params['session_detail']);
		return TRUE;			
	} else {
		$this->form_validation->set_message('authenticate_credentials', $out_params['auth_msg']);
		return FALSE;
	}
		
}
}
/* End of file auth.php */
/* Location: ./application/controllers/auth.php */