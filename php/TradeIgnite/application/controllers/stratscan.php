<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stratscan extends CI_Controller {

/**
 * TradeIgnite
 *
 * @package		stratscan.php
 * @author		Frans Lubbe
 * @copyright	Copyright (c) 2014, TradeIgnite Pty Ltd
 * @license		
 * @link		http://tradeignite.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------
/**
 * StratScan Controller File
 *
 * Controller loaded by auth.php controller.
 *
 * @package		StratScan
 * @subpackage	Controller
 * @category	Front-controller
 * @author		TradeIgnite Dev Team
 * @link		http://tradeignite.com
 */

/*
 * ------------------------------------------------------
 *  Load the controllers objects
 * ------------------------------------------------------
 */
function __construct() {
	
	parent::__construct();
	
	$this->config->load ( 'stratscan_config' );
	$this->load->model ( 'stratscan_model' );
	$this->template->set_template('stratscan');
}
/*
 * ------------------------------------------------------
 *  Build My Strategies Page
 *  Displays list of users strategies 
 * ------------------------------------------------------
 */	
function strategies() {

	$this->template->write_view('header', 'stratscan/strategies_menu');
	$this->template->write_view('content', 'stratscan/strategies');
	$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);	
}
/*
 * ----------------------------------------------------------------------
 *  Returns the list of users save strategies for the My Strategies Page
 * ----------------------------------------------------------------------
 */	
function get_strategies() {		
	
	$error = null;
		
	$data['live']    = 'N';
	$data['sEcho']   = $this->input->get('sEcho'); 
	$data['sortCol'] = $this->input->get('iSortCol_0');
	$data['sortDir'] = $this->input->get('sSortDir_0');
	$data['sSearch'] = $this->input->get('sSearch');
	
	$data['strategies_api']       = $this->config->item ( 'strategies_api' );
		
	$patterns     [0] = '/<live>/';
	$replacements [0] = $data['live'];
	$patterns     [1] = '/<sEcho>/';
	$replacements [1] = $data['sEcho'];
	$patterns     [2] = '/<sortCol>/';
	$replacements [2] = $data['sortCol'];
	$patterns     [3] = '/<sortDir>/';
	$replacements [3] = $data['sortDir'];
	$patterns     [4] = '/<sSearch>/';
	$replacements [4] = $data['sSearch'];
						
	$data['strategies_api'] = preg_replace ( $patterns, $replacements, $data['strategies_api'] );					
				
	$data['session_id'] = $this->session->userdata('db_session_id');
	$data['user_id'] = $this->session->userdata('user_id');
	
	$this->stratscan_model->get_strategies( $data, $data['strategies'], $error);
	
	$response = $data['strategies'];
	$this->output->set_output($response);		
}
/*
 * ----------------------------------------------------------------------
 *  Builds Chart Options page
 *  Gathers data for all items on page
 * ----------------------------------------------------------------------
 */	
function analysis () {
		
	$error                  = null;
	$data['menu']           = null;
	$data['menu_spacer']    = null;
	$data['strategy_name']  = 'Draft Strategy';
	$data['strategy_desc']  = null;
	$data['other_jscript']  = null;
	$data['saved_ind']      = 'N';
	$data['error_code']     = 200;
	$study_found            = 'N';

	$data['chart_types_api']      = $this->config->item ( 'chart_types_api' );
	$data['exchange_api']         = $this->config->item ( 'exchange_api' );
	$data['products_api']         = $this->config->item ( 'products_api' );
	$data['contracts_api']        = $this->config->item ( 'contracts_api' );
	$data['delete_session_api']   = $this->config->item ( 'delete_session_api' );
		
	$data['session_id'] = $this->session->userdata('db_session_id');
	$data['user_id'] = $this->session->userdata('user_id');
		
	$data['chart_type_dd_js'] = 'id="chart_type" style="width:250px"';
	$this->stratscan_model->chart_types_options ( $data, $data['chart_type_opt'], $data['sel_chart_type_opt'], $error);
		
	if ($error['code'] !== 200) {
		show_error($error['message'] , $error['code'] );
	}
		
	$data['exchange_dd_js'] = 'id="exchange" style="width:250px" onChange="exchange_onChange();"';
	$this->stratscan_model->exchange_options ( $data, $data['exchange_opt'], $data['sel_exchange_opt'], $error);
		
	if ($error['code'] !== 200) {
		show_error($error['message'] , $error['code'] );
	}
		
	$data['products_api']   = $data['products_api'].$data['sel_exchange_opt'];
	$data['products_dd_js'] = 'id="products" style="width:250px" onChange="products_onChange();"';
	$this->stratscan_model->product_options ( $data, $data['product_opt'], $data['sel_product_opt'], $data['sel_product_array'], $error);
		
	if ($error['code'] !== 200) {
		show_error($error['message'] , $error['code'] );
	}
			
	$data['contracts_api'] = $data['contracts_api'].$data['sel_product_opt'];
	$data['contracts_dd_js'] = 'id="contracts" style="width:250px" onChange="contracts_onChange();"';
	$this->stratscan_model->contracts_options   ( $data, $data['contract_opt'], $data['sel_contract_opt'], $data['sel_contract_array'], $error);
		
	if ($error['code'] !== 200) {
		show_error($error['message'] , $error['code'] );
	}
		
	$data['timeframe_opt']       = array();
	$data['sel_timeframe_opt']   = array();
	$data['sel_timeframe_array'] = array();
	$data['timeframe_dd_js']     = 'id="timeframe" style="width:250px"';
		
	$data['studies_opt']       = array();
	$data['sel_studies_opt']   = array();
	$data['sel_studies_array'] = array();
			
	$data['studies_menu_detail'] = '';
	
	$data['menuHTML'] = '<table class="setuparea_menu_detail" >
	   		<tr>			
				<td style="width:33%">
		           <label for="setupAreaEntryFromlbl" id="setupAreaEntryFromlbl">Select entry setup area:</label>
		        </td>
			</tr>
		</table>
		<div class="uiLightness">
		<table class="setupAreaEntryDetail">
			<tr>			
				<td>
		           <input type="text" id="setupAreaEntryFrom" name="setupAreaEntryFrom" disabled/>
		        </td>
		        <td>   
		           <a href="#" onclick="setupAreaSelection(\'Entry\', \'Input\', \'range\');"><img id="setupAreaEntryFromIcon" title="Highlight all points for your trade entry criteria" alt="busy..." src="../assets/default/images/content/SelectChart.svg" width="20" height="20"/> 
		        </td>
		           <td style="padding-top: 6px"><input type="hidden" id="setupAreaEntryColor" value="#33CC33" />
				</td>
			</tr>
			<tr>			
				<td>
		           <input type="text" id="setupAreaEntryTo" name="setupAreaEntryTo" disabled/>
				</td>
				<td colspan="2">
			       <a href="#" onclick="setupAreaSelection(\'Entry\', \'Delete\', \'range\');"><img id="ClearAreaEntry" title="Clear selected area" alt="busy..." src="../assets/default/images/content/remove.svg" width="20" height="20"/></a> 
				</td>
			</tr>	
		</table>
		</div>
		<br>';
		
	//Edit strategy code, Populate the drop downs with selected values 
	$data['strategy_id']  = $this->input->post ( 'strategy_id' );
	$data['chart_id'] = null;
				
	if ($data['strategy_id'] != '') {
		
		$saved_strategy_options = null;
		$saved_chart_options    = null;
		
		$data['saved_ind']      = 'Y';
			
		$data['charts_api'] = $this->config->item ( 'charts_api' );
		$data['charts_api'] = $data['charts_api'].$data['strategy_id'];
			
		$this->stratscan_model->return_strategy_chart_options  ( $data, $saved_strategy_options, $saved_chart_options, $error);

		if ($error['code'] !== 200) {
			show_error($error['message'] , $error['code'] );
		}

		$data['strategy_name']    = $saved_strategy_options->name;
		$data['strategy_desc']    = $saved_strategy_options->desc;
		$data['sel_exchange_opt'] = $saved_strategy_options->exchange;
		$data['sel_product_opt']  = $saved_strategy_options->product;
			
		$data['contract_opt']     = null;
		$data['contracts_api']    = $this->config->item ( 'contracts_api' );
		$data['contracts_api']    = $data['contracts_api'].$data['sel_product_opt'];
		$data['contracts_dd_js']  = 'id="contracts" style="width:250px" onChange="contracts_onChange();"';
			
		$this->stratscan_model->contracts_options ($data, $data['contract_opt'], $data['sel_contract_opt'], $data['sel_contract_array'], $error);

		if ($error['code'] !== 200) {
			show_error($error['message'] , $error['code'] );
		}
		
		$data['sel_contract_opt'] = $saved_strategy_options->contract;

		//Chart Options
		foreach($saved_chart_options->items as $option){

			if ($option->lastViewed == 'Y') {
					
				$data['chart_id']           = $option->chartId;
				$data['sel_chart_type_opt'] = $option->chartType;
				$data['saved_timeframe']    = $option->timeframeDesc;
				$data['menu']               = $data['menu'].'<td onclick="saved_chart_onclick(this)" class="charts" id="chart_id'.$data['chart_id'].'" style="color:#ffffff; background:#5f6f81"><b>'.$data['saved_timeframe'].'</b></td>';
				$data['menu_spacer']        = $data['menu_spacer'].'<td id="spacer" style="border:none" ></td>';
					
				$data['strategy_timeframe_api']         = $this->config->item ( 'strategy_timeframe_api' ).'&chartId=<sel_chartId>';
					
				$patterns     [0] = '/<strategy_id>/';
				$replacements [0] = $data['strategy_id'];
				$patterns     [1] = '/<sel_chartId>/';
				$replacements [1] = $data['chart_id'];
						
				$data['strategy_timeframe_api'] = preg_replace ( $patterns, $replacements, $data['strategy_timeframe_api'] );
				$data['timeframe_dd_js']        = 'id="timeframe" style="width:250px"';
				$this->stratscan_model->strategy_timeframe_options ( $data, $data['timeframe_opt'], $data['sel_timeframe_opt'], $data['sel_timeframe_array'], $error);
				
				if ($error['code'] !== 200) {
					show_error($error['message'] , $error['code'] );
				}
					
				$data['used_studies_api']  = $this->config->item ( 'used_studies_api' );
				$data['used_studies_api']  = $data['used_studies_api'].$data['chart_id'];

				$data['studies_menu_detail'] = '<table width=93% class="studies_menu_detail">';
				$this->stratscan_model->used_studies_detail ( $data, $data['used_studies_detail'], $error);
				
				if ($error['code'] !== 200) {
					show_error($error['message'] , $error['code'] );
				}
				
				if(is_array($data['used_studies_detail'])){
					foreach( $data['used_studies_detail'] as $option ) {
						$study_found = 'Y';
						$data['studies_menu_detail'] .= '<tr>
															<td class="studies_menu_detail_name"><a href="#" onclick="edit_study_link('.$option->studyId.');"><p>'.$option->desc.'</p></a></td>
															<td><span style="background:'.$option->colour.'">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
															<td align="right"><a href="#" onclick="remove_study('.$option->studyId.');"><img src="../assets/default/images/content/remove.svg" alt="delete" width="20" height="20"></a></td>
														</tr>';
					}
					if ($study_found == 'N'){
						$data['studies_menu_detail'] .= '<tr><td><p>No saved studies.</p></td></tr>';
					}
				}
					
				$data['studies_menu_detail'] .= '</table>';
				
				//Setup Area menu HTML
				$data['setupAreasMenuApi'] = $this->config->item ( 'setupAreasMenuApi' );
				
				$patterns [0]           = '/<chartId>/';
				$replacements [0]       = $data['chart_id'];
				$data['setupAreasMenuApi'] = preg_replace ( $patterns, $replacements, $data['setupAreasMenuApi'] );
				
				$this->stratscan_model->setupAreasMenu ( $data, $data['areaFromDate'], $data['areaToDate'], $data['menuHTML'], $error);

			} else {
				$data['chart_id_other']  = $option->chartId;
				$data['saved_timeframe'] = $option->timeframeDesc;
				$data['menu']            = $data['menu'].'<td onclick="saved_chart_onclick(this)" class="charts" id="chart_id'.$data['chart_id_other'].'" style="color:#5f6f81; background:#ffffff"><b>'.$data['saved_timeframe'].'</b></td>';
				$data['menu_spacer']     = $data['menu_spacer'].'<td id="spacer" style="border:none;" ></td>';					
			}
		}
	}
	
	$data['chartload_api'] = $this->config->item ( 'chartload_api' );
			
	$this->stratscan_model->get_chart_code($data, $data['chart_code'], $error);
	
	if ($error['code'] !== 200) {
		show_error($error['message'] , $error['code'] );
	}
			
	$data['other_jscript'] = $data['chart_code'];
	$patterns     [0] = '/<IMAGES_PATH>/';
	$replacements [0] = '../assets/default/images/amchart/';
			
	$data['other_jscript'] = preg_replace ( $patterns, $replacements, $data['other_jscript'] );
	
	$data['theme_opt'] = array(
		'default' => 'Default',
		'black'   => 'Black',
		'blue'    => 'Blue',
		'grey'    => 'Grey',
		'white'   => 'White');
		
	$data['sel_theme_opt'] = 'default';
	$data['theme_dd_js'] = 'id="theme" style="width:250px"';

	$this->template->write_view('content', 'stratscan/analysis', $data);
	$this->template->write_view('header', 'stratscan/analysis_menu', $data);		
	$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);		
}
/*
 * ----------------------------------------------------------------------
*  Exchange drop down change (ajax)
*  Form: Chart Options
* ----------------------------------------------------------------------
*/
function ajax_exchange_change() {

	$error = null;
	$response = null;

	$data['session_id']           = $this->session->userdata('db_session_id');
	$data['user_id']              = $this->session->userdata('user_id');
	
	$data['sel_exchange_opt'] = $this->input->post ( 'exchange_dd_value' );
	$data['products_api']   = $this->config->item ( 'products_api' );
	$data['products_api']   = $data['products_api'].$data['sel_exchange_opt'];
	
	$this->stratscan_model->product_options ( $data, $data['product_opt'], $data['sel_product_opt'], $data['sel_product_array'], $error);
	
	foreach( $data['sel_product_array'] as $key => $value ) {
		$products = '<option value="' . $key . '" class="dynamic" selected="selected">' . $value . '</option>';
	}
	
	$data['product_opt'] = array_diff($data['product_opt'],$data['sel_product_array']);
	
	foreach( $data['product_opt'] as $key => $value ) {
		$products .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
	}
	
	$data['contracts_api']   = $this->config->item ( 'contracts_api' );
	$data['contracts_api']   = $data['contracts_api'].$data['sel_product_opt'];

	$this->stratscan_model->contracts_options   ( $data, $data['contract_opt'], $data['sel_contract_opt'], $data['sel_contract_array'], $error);

	foreach( $data['sel_contract_array'] as $key => $value ) {
		$contracts = '<option value="' . $key . '" class="dynamic" selected="selected">' . $value . '</option>';
	}

	$data['contract_opt'] = array_diff($data['contract_opt'],$data['sel_contract_array']);

	foreach( $data['contract_opt'] as $key => $value ) {
		$contracts .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
	}

	$output = json_encode(array('error_code'         => $error['code'],
			'error_msg'          => $error['message'],
			'products'           => $products,
			'contracts'          => $contracts));

	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
 *  Products drop down change (ajax)
 *  Form: Chart Options
 * ----------------------------------------------------------------------
 */
function ajax_products_change() {
		
	$error = null;
	$response = null;
		
	$data['sel_product_opt'] = $this->input->post ( 'products_dd_value' );
		
	$data['session_id']           = $this->session->userdata('db_session_id');
	$data['user_id']              = $this->session->userdata('user_id');
		
	$data['contracts_api']   = $this->config->item ( 'contracts_api' );
	$data['contracts_api']   = $data['contracts_api'].$data['sel_product_opt'];		
		
	$this->stratscan_model->contracts_options   ( $data, $data['contract_opt'], $data['sel_contract_opt'], $data['sel_contract_array'], $error);

	foreach( $data['sel_contract_array'] as $key => $value ) {
		$response = '<option value="' . $key . '" class="dynamic" selected="selected">' . $value . '</option>';
	}
	
	$data['contract_opt'] = array_diff($data['contract_opt'],$data['sel_contract_array']);
		
	foreach( $data['contract_opt'] as $key => $value ) {
    	$response .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
    }    	
    
    $output = json_encode(array('error_code'         => $error['code'], 
	 			                'error_msg'          => $error['message'],
				                'data'               => $response));	
		
	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
 *  Contracts drop down change (ajax)
 *  Form: Chart Options
 * ----------------------------------------------------------------------
*/	
function ajax_contracts_change() {
	
	$error = null;
	$response = null;
		
	$data['sel_contract_opt'] = $this->input->post ( 'contracts_dd_value' );
	$data['sel_exchange_opt'] = $this->input->post ( 'exchange_dd_value' );
	
	$data['session_id']           = $this->session->userdata('db_session_id');
	$data['user_id']              = $this->session->userdata('user_id');		
	
	$data['contract_detail_api']  = $this->config->item ( 'contract_detail_api' );
		
	$patterns     [0] = '/<sel_exchange>/';
	$replacements [0] = $data['sel_exchange_opt'];
	$patterns     [1] = '/<sel_contract>/';
	$replacements [1] = $data['sel_contract_opt'];
		
	$data['contract_detail_api'] = preg_replace ( $patterns, $replacements, $data['contract_detail_api'] );
		
	$this->stratscan_model->contract_details ( $data, $data['min_date'], $data['max_date'], $error);
			
	$output =  json_encode(array('error_code'         => $error['code'],
				                 'error_msg'          => $error['message'],
				                 'contract_min_date'  => $data['min_date'],
				                 'contract_max_date'  => $data['max_date']
	));	

	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
 *  Save Strategy on Strategy Menu
 *  Form: Chart Options
 * ----------------------------------------------------------------------
*/		
function save_strategy() {
		
	$error = null;
	$response = null;
	$data['strategy_id'] = null;
		
	$data['sel_exchange_opt'] = $this->input->post ( 'exchange_dd_value' );
	$data['sel_product_opt']  = $this->input->post ( 'products_dd_value' );
	$data['sel_contract_opt'] = $this->input->post ( 'contracts_dd_value' );
	$data['strategy_id_val']  = $this->input->post ( 'strategy_id_val' );
	$data['chart_id_val']     = $this->input->post ( 'chart_id_val' );
	$data['sync_opt']         = $this->input->post ( 'sync_opt' );
	$data['sync_type']        = $this->input->post ( 'sync_type' );
		
	$data['strategy_api']         = $this->config->item ( 'init_strategy_api' );
	$data['session_id']           = $this->session->userdata('db_session_id');
	$data['user_id']              = $this->session->userdata('user_id');
		
	$data['strategy_detail'] = Array(
	    	"name"       => null,
			"desc"       => null,
			"position"   => null,
			"exchange"   => $data['sel_exchange_opt'],
			"product"    => $data['sel_product_opt'],
			"contract"   => $data['sel_contract_opt'],
			"syncInd"    => $data['sync_opt'],
			"syncType"   => $data['sync_type']);
		
	if ($data['strategy_id_val'] !== '') {
			
		$data['strategy_api']    = $this->config->item ( 'strategy_api' );
			
		$patterns     [0] = '/<strategyId>/';
		$replacements [0] = $data['strategy_id_val'];
			
		$data['strategy_api'] = preg_replace ( $patterns, $replacements, $data['strategy_api'] );
	}
		
	$this->stratscan_model->save_strategy ( $data, $data['strategy_id'], $error);
	
	if ($error['code'] == 200) {

		// Builds Timeframe API
		if ($data['chart_id_val'] !== '') {
			
			$data['strategy_timeframe_api']         = $this->config->item ( 'strategy_timeframe_api' ).'&chartId=<sel_chartId>';
				
			$patterns     [0] = '/<strategy_id>/';
			$replacements [0] = $data['strategy_id'];
			$patterns     [1] = '/<sel_chartId>/';
			$replacements [1] = $data['chart_id_val'];
					
			$data['strategy_timeframe_api'] = preg_replace ( $patterns, $replacements, $data['strategy_timeframe_api'] );				
		} else {
				
			$data['strategy_timeframe_api']         = $this->config->item ( 'strategy_timeframe_api' );
				
			$patterns     [0] = '/<strategy_id>/';
			$replacements [0] = $data['strategy_id'];
					
			$data['strategy_timeframe_api'] = preg_replace ( $patterns, $replacements, $data['strategy_timeframe_api'] );
		}			
			
		$this->stratscan_model->strategy_timeframe_options ( $data, $data['timeframe_opt'], $data['sel_timeframe_opt'], $data['sel_timeframe_array'], $error);
		
		if ($error['code'] !== 200) {
			$data['strategy_id'] = null;
		}
		
		foreach( $data['sel_timeframe_array'] as $key => $value ) {			
			$response = '<option value="' . $key . '" class="dynamic" selected="selected">' . $value . '</option>';
		}
		
		$data['timeframe_opt'] = array_diff($data['timeframe_opt'],$data['sel_timeframe_array']);
			
		foreach( $data['timeframe_opt'] as $key => $value ) {
			$response .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
		}		
	}				
	$output = json_encode(array('error_code'         => $error['code'],
		 		                'error_msg'          => $error['message'],
				                'strategy_id'        => $data['strategy_id'],
				                'timeframe_data'     => $response
	));	
	$this->output->set_output($output);		
}
/*
 * ----------------------------------------------------------------------
 *  Save Chart on Strategy Menu
 *  Form: Chart Options
 * ----------------------------------------------------------------------
*/	
function save_chart() {
	
	$error                = null;
	$response             = null;
	$saved_timeframe_desc = null;
	
	$data['strategy_id']    = $this->input->post ( 'strategy_id' );
	$data['timeframe']      = $this->input->post ( 'timeframe' );
	$data['chart_type']     = $this->input->post ( 'chart_type' );
	$data['themes']         = $this->input->post ( 'themes' );
	$data['min_date']       = $this->input->post ( 'min_date' );
	$data['max_date']       = $this->input->post ( 'max_date' );
	$data['chart_id']       = $this->input->post ( 'chart_id' );
	$data['disp_vol']       = $this->input->post ( 'disp_vol' );
	$data['UpBarsColor']    = $this->input->post ( 'UpBarsColor' );
	$data['DownBarsColor']  = $this->input->post ( 'DownBarsColor' );
	$data['volumeColor']    = $this->input->post ( 'volumeColor' );
		
	$data['chart_api']      = $this->config->item ( 'chart_api' );
	$data['session_id']     = $this->session->userdata('db_session_id');
	$data['user_id']        = $this->session->userdata('user_id');
		
	$data['chart_api'] = $data['chart_api'].'?strategyId='.$data['strategy_id'];
		
	if ($data['chart_id'] !== '') {		
		$data['chart_api']        = $data['chart_api'].'&chartId='.$data['chart_id'];	
		$data['ChartMethod'] = 'UPDATE';
	} else {
		$data['ChartMethod'] = 'SAVE';
	}		
		
	$data['chart_detail'] = Array(
			"timeframe"      => $data['timeframe'],
			"chartType"      => $data['chart_type'],
			"showVolume"     => $data['disp_vol'],
			"positiveColour" => $data['UpBarsColor'],
			"negativeColour" => $data['DownBarsColor'],
			"volumeColour"   => $data['volumeColor']
	);
		
	$this->stratscan_model->save_chart ( $data, $data['chart_id'], $error);
	
	if ($error['code'] == 200) {
	
		$data['strategy_timeframe_api']         = $this->config->item ( 'strategy_timeframe_api' ).'&chartId=<sel_chartId>';			
						
		$patterns     [0] = '/<strategy_id>/';
		$replacements [0] = $data['strategy_id'];
		$patterns     [1] = '/<sel_chartId>/';
		$replacements [1] = $data['chart_id'];
				
		$data['strategy_timeframe_api'] = preg_replace ( $patterns, $replacements, $data['strategy_timeframe_api'] );
			
		$this->stratscan_model->strategy_timeframe_options ( $data, $data['timeframe_opt'], $data['sel_timeframe_opt'], $data['sel_timeframe_array'], $error);

		foreach( $data['sel_timeframe_array'] as $key => $value ) {
			
			$response = '<option value="' . $key . '" class="dynamic" selected="selected">' . $value . '</option>';
			$saved_timeframe_desc = $value;
		}
			
		$data['timeframe_opt'] = array_diff($data['timeframe_opt'],$data['sel_timeframe_array']);
			
		foreach( $data['timeframe_opt'] as $key => $value ) {
			$response .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
		}
		
		$data['chart_menu_api'] = $this->config->item ( 'chart_menu' );
		$data['chart_menu_api'] = $data['chart_menu_api'].'?strategyId=<strategyId>&chartId=<chartId>';
		
		$patterns [0]           = '/<strategyId>/';
		$replacements [0]       = $data['strategy_id'];
		$patterns [1]           = '/<chartId>/';
		$replacements [1]       = $data['chart_id'];
		
		$data['chart_menu_api'] = preg_replace ( $patterns, $replacements, $data['chart_menu_api'] );
		
		$this->stratscan_model->ChartMenu ( $data, $data['ChartId'], $data['menuHTML'], $data['menuChartCount'], $error);
		
	}	
						
	$output = json_encode(array(
				'error_code'         => $error['code'],
				'error_msg'          => $error['message'],
				'chart_id'           => $data['ChartId'],
				'saved_timeframe'    => $saved_timeframe_desc,
			    'ChartMethod'        => $data['ChartMethod'],
				'menu'               => $data['menuHTML'],
				'timeframe_data'     => $response
	));			
	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
 *  AJAX call on menu add item click (new Timeframe)
 *  Form: Chart Options
 * ----------------------------------------------------------------------
*/	
function get_newchart_timeframe_opt() {
		
	$error = null;
				
	$data['strategy_id'] = $this->input->post ( 'strategy_id' );
		
	$data['session_id']           = $this->session->userdata('db_session_id');
	$data['user_id']              = $this->session->userdata('user_id');
		
	$data['strategy_timeframe_api']         = $this->config->item ( 'strategy_timeframe_api' );
		
	$patterns     [0] = '/<strategy_id>/';
	$replacements [0] = $data['strategy_id'];
				
	$data['strategy_timeframe_api'] = preg_replace ( $patterns, $replacements, $data['strategy_timeframe_api'] );
		
	$this->stratscan_model->strategy_timeframe_options ( $data, $data['timeframe_opt'], $data['sel_timeframe_opt'], $data['sel_timeframe_array'], $error);
		
	foreach( $data['sel_timeframe_array'] as $key => $value ) {
					
		$response = '<option value="' . $key . '" class="dynamic" selected="selected">' . $value . '</option>';
		$saved_timeframe_desc = $value;
	}
	$data['timeframe_opt'] = array_diff($data['timeframe_opt'],$data['sel_timeframe_array']);
	
	foreach( $data['timeframe_opt'] as $key => $value ) {
		$response .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
	}		
			
	$output = json_encode(array(
			'error_code'         => $error['code'],
			'error_msg'          => $error['message'],
			'timeframe_data'     => $response
	));
	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
 *  AJAX call 
 *  Resets all the form data
 *  Form: Chart Options
 * ----------------------------------------------------------------------
*/
function get_menu_options(){
		
	$error = null;
	$saved_chart_options = array();
	$saved_strategy_options = array();
	$product_opt = null;
	$contract_opt = null;
	$response = null;
	$data['studies_menu_detail'] = null;
	$study_found = 'N';
		
	$data['strategy_id'] = $this->input->post ( 'strategy_id' );
	$data['chart_id']    = $this->input->post ( 'chart_id' );
		
	$data['session_id']  = $this->session->userdata('db_session_id');
	$data['user_id']     = $this->session->userdata('user_id');
		
	//Both Chart and strategy has been saved. Get all menu options
	if ($data['chart_id'] != ''){

		$data['chart_api'] = $this->config->item ( 'chart_api' );
		$data['chart_api'] = $data['chart_api'].'?chartId='.$data['chart_id'];
		
		$this->stratscan_model->return_chart_options($data, $saved_chart_options, $saved_strategy_options ,$error);
		
		if ($error['code'] == 200) {
				
			$data['strategy_timeframe_api']  = $this->config->item ( 'strategy_timeframe_api' ).'&chartId=<sel_chartId>';

			$patterns     [0] = '/<strategy_id>/';
			$replacements [0] = $data['strategy_id'];
			$patterns     [1] = '/<sel_chartId>/';
			$replacements [1] = $data['chart_id'];
		
			$data['strategy_timeframe_api'] = preg_replace ( $patterns, $replacements, $data['strategy_timeframe_api'] );
		
			$this->stratscan_model->strategy_timeframe_options ( $data, $data['timeframe_opt'], $data['sel_timeframe_opt'], $data['sel_timeframe_array'], $error);
		
			if ($error['code'] == 200) {
			
				foreach( $data['sel_timeframe_array'] as $key => $value ) {					
					$response = '<option value="' . $key . '" class="dynamic" selected="selected">' . $value . '</option>';
					$saved_timeframe_desc = $value;
				}
				
				$data['timeframe_opt'] = array_diff($data['timeframe_opt'],$data['sel_timeframe_array']);
				
				foreach( $data['timeframe_opt'] as $key => $value ) {		
					$response .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
				}	

				$data['products_api']   = $this->config->item ( 'products_api' );
				$data['products_api']   = $data['products_api'].$saved_strategy_options->exchange;				
				$data['products_dd_js'] = 'id="products" style="width:250px" onChange="products_onChange();"';
				$this->stratscan_model->product_options ( $data, $data['product_opt'], $data['sel_product_opt'], $data['sel_product_array'], $error);
		
				if ($error['code'] == 200) {
					
					foreach( $data['product_opt'] as $key => $value ) {
						$product_opt .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
					}
					
					$data['contracts_api']    = $this->config->item ( 'contracts_api' );
					$data['contracts_api']    = $data['contracts_api'].$saved_strategy_options->product;
					$data['contracts_dd_js']  = 'id="contracts" style="width:250px" onChange="contracts_onChange();"';
			
					$this->stratscan_model->contracts_options ($data, $data['contract_opt'], $data['sel_contract_opt'], $data['sel_contract_array'], $error);
			
					if ($error['code'] == 200) {
						
						foreach( $data['contract_opt'] as $key => $value ) {
							$contract_opt .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
						}
			
						$data['used_studies_api']  = $this->config->item ( 'used_studies_api' );
						$data['used_studies_api']  = $data['used_studies_api'].$data['chart_id'];
			
						$data['studies_menu_detail'] = '<table width=93% class="studies_menu_detail">';
						$this->stratscan_model->used_studies_detail ( $data, $data['used_studies_detail'], $error);
						
						if(is_array($data['used_studies_detail'])){
							
							foreach( $data['used_studies_detail'] as $option ) {
								$study_found = 'Y';
								$data['studies_menu_detail'] .= '<tr>
									                           	 	<td class="studies_menu_detail_name"><a href="#" onclick="edit_study_link('.$option->studyId.');"><p>'.$option->desc.'</p></a></td>
																	<td><span style="background:'.$option->colour.'">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
																	<td align="right"><a href="#" onclick="remove_study('.$option->studyId.');"><img src="../assets/default/images/content/remove.svg" alt="delete" width="20" height="20"></a></td>
																</tr>';
							}
							
							if ($study_found == 'N'){
								$data['studies_menu_detail'] .= '<tr><td><p>No saved studies.</p></td></tr>';
							}
						}				
						$data['studies_menu_detail'] .= '</table>';
					}
				}				
			}			
		}
		$output = json_encode(array(
			'error_code'         => $error['code'],
			'error_msg'          => $error['message'],
			'syncInd'            => (isset($saved_strategy_options->syncInd)? $saved_strategy_options->syncInd : null ),
			'syncType'           => (isset($saved_strategy_options->syncType)? $saved_strategy_options->syncType : null ),
			'exchange'           => (isset($saved_strategy_options->exchange)? $saved_strategy_options->exchange : null ),
			'product_opt'        => $product_opt,
			'product'            => (isset($saved_strategy_options->product)? $saved_strategy_options->product : null ),
			'contract_opt'       => $contract_opt,
			'contract'           => (isset($saved_strategy_options->contract)? $saved_strategy_options->contract : null ),
			'min_date_range'     => (isset($saved_chart_options->fromDate)? $saved_chart_options->fromDate : null ),
			'max_date_range'     => (isset($saved_chart_options->toDate)? $saved_chart_options->toDate : null ),
			'chart_id'           => (isset($saved_chart_options->chartId)? $saved_chart_options->chartId : null ),
			'timeframe_data'     => $response,
			'chartType'          => (isset($saved_chart_options->chartType)? $saved_chart_options->chartType : null ),
			'showVolume'         => (isset($saved_chart_options->showVolume)? $saved_chart_options->showVolume : null ),
			'positiveColour'     => (isset($saved_chart_options->positiveColour)? $saved_chart_options->positiveColour : null ),
			'negativeColour'     => (isset($saved_chart_options->negativeColour)? $saved_chart_options->negativeColour : null ),
			'volumeColour'       => (isset($saved_chart_options->volumeColour)? $saved_chart_options->volumeColour : null ),
			'used_studies'       => $data['studies_menu_detail']
		));	
	//Only strategy has been saved. Get deravitive options	
	} else {
						
		$data['strategy_api'] = $this->config->item ( 'strategy_api' );
			
		$patterns     [0] = '/<strategyId>/';
		$replacements [0] = $data['strategy_id'];
			
		$data['strategy_api'] = preg_replace ( $patterns, $replacements, $data['strategy_api'] );
			
		$this->stratscan_model->return_strategy_options($data, $saved_strategy_options, $error);
		
		if ($error['code'] == 200) {
			
			$data['products_api']   = $this->config->item ( 'products_api' );
			$data['products_api']   = $data['products_api'].$saved_strategy_options->exchange;
			$data['products_dd_js'] = 'id="products" style="width:250px" onChange="products_onChange();"';
			$this->stratscan_model->product_options ( $data, $data['product_opt'], $data['sel_product_opt'], $data['sel_product_array'], $error);
			
			foreach( $data['product_opt'] as $key => $value ) {
				$product_opt .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
			}					

			$data['contract_opt']     = null;
			$data['contracts_api']    = $this->config->item ( 'contracts_api' );
			$data['contracts_api']    = $data['contracts_api'].$saved_strategy_options->product;
			$data['contracts_dd_js']  = 'id="contracts" style="width:250px" onChange="contracts_onChange();"';
				
			$this->stratscan_model->contracts_options ($data, $data['contract_opt'], $data['sel_contract_opt'], $data['sel_contract_array'], $error);
			$contract_opt = null;
			foreach( $data['contract_opt'] as $key => $value ) {
				$contract_opt .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
    		}  
    		
    		$data['strategy_timeframe_api']         = $this->config->item ( 'strategy_timeframe_api' );
    		
    		$patterns     [0] = '/<strategy_id>/';
    		$replacements [0] = $data['strategy_id'];
    			
    		$data['strategy_timeframe_api'] = preg_replace ( $patterns, $replacements, $data['strategy_timeframe_api'] );
    			
    		$this->stratscan_model->strategy_timeframe_options ( $data, $data['timeframe_opt'], $data['sel_timeframe_opt'], $data['sel_timeframe_array'], $error);
    		
    		foreach( $data['sel_timeframe_array'] as $key => $value ) {
    			$response = '<option value="' . $key . '" class="dynamic" selected="selected">' . $value . '</option>';
    		}
    		
    		$data['timeframe_opt'] = array_diff($data['timeframe_opt'],$data['sel_timeframe_array']);
    			
    		foreach( $data['timeframe_opt'] as $key => $value ) {
    			$response .= '<option value="' . $key . '" class="dynamic">' . $value . '</option>';
    		}

    		$data['studies_menu_detail'] = '';    		
		}
			
		$output = json_encode(array(
				'error_code'         => $error['code'],
				'error_msg'          => $error['message'],
				'syncInd'            => (isset($saved_strategy_options->syncInd)? $saved_strategy_options->syncInd : null ),
				'syncType'           => (isset($saved_strategy_options->syncType)? $saved_strategy_options->syncType : null ),
				'exchange'           => (isset($saved_strategy_options->exchange)? $saved_strategy_options->exchange : null ),
				'product_opt'        => $product_opt,
				'product'            => (isset($saved_strategy_options->product)? $saved_strategy_options->product : null ),
				'contract_opt'       => $contract_opt,
				'contract'           => (isset($saved_strategy_options->contract)? $saved_strategy_options->contract : null ),
				'timeframe_data'     => $response,
				'used_studies'       => $data['studies_menu_detail']
		));	
	}		
		
	$this->output->set_output($output);	
}

/*
 * ----------------------------------------------------------------------
 *  Delete icon press
 *  Deletes Strategy from list
 *  Form: Strategy Summary Page
 * ----------------------------------------------------------------------
*/	
function delete_strategy(){
		
	$error = null;
		
	$data['strategy_id'] = $this->input->post ( 'strategy_id' );
		
	$data['session_id']  = $this->session->userdata('db_session_id');
	$data['user_id']     = $this->session->userdata('user_id');
		
	$data['strategy_api'] = $this->config->item ( 'strategy_api' );
		
	$patterns     [0] = '/<strategyId>/';
	$replacements [0] = $data['strategy_id'];
		
	$data['strategy_api'] = preg_replace ( $patterns, $replacements, $data['strategy_api'] );
	
	$this->stratscan_model->delete_strategy($data, $error);
	
	$output = json_encode(array('error_code'         => $error['code'],
							    'error_msg'          => $error['message'] ));		
		
	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
 * Populates chart framework with data
 * Form: Chart Analysis, also references in Chart Analysis Menu
 * ----------------------------------------------------------------------
*/	
function get_chart_data (){
		
	$error = null;
		
	$data['chart_id'] = $this->input->post ( 'chart_id' );
	$data['criteria'] = $this->input->post ( 'criteria' );	
			
	$data['chartdata_api'] = $this->config->item ( 'chartdata_api' );
	$data['chartdata_api'] = $data['chartdata_api'].$data['chart_id'];
	
	if($data['criteria'] == 'Y') {
		$data['chartdata_api'] = $data['chartdata_api'].'&criteria='.$data['criteria'];
	}
		
	$data['session_id']    = $this->session->userdata('db_session_id');
	$data['user_id']       = $this->session->userdata('user_id');
			
	$this->stratscan_model->get_chart_data($data, $data['chart_data'], $data['chart_settings'], $data['study_settings'], $error);

	$output = json_encode(array('chart_data'      => $data['chart_data'],
			                    'chartSettings'   => $data['chart_settings'],
			                    'studySettings'   => $data['study_settings'],
								'error_code'      => $error['code'],
							    'error_msg'       => $error['message']));
		
	$this->output->set_output($output);		
}
/*
 * ----------------------------------------------------------------------
 * Populates chart framework with study data
 * Form: Chart Analysis
 * ----------------------------------------------------------------------
*/	
function get_study_data (){
	
	$error = null;
	
	$data['study_id']      = $this->input->post ( 'study_id' );
	
	$data['studydata_api'] = $this->config->item ( 'studydata_api' );
	$patterns [0]          = '/<studyId>/';
	$replacements [0]      = $data['study_id'];				
	$data['studydata_api'] = preg_replace ( $patterns, $replacements, $data['studydata_api'] );
	
	$data['session_id']    = $this->session->userdata('db_session_id');
	$data['user_id']       = $this->session->userdata('user_id');
			
	$this->stratscan_model->get_study_data($data, $data['study_settings'], $error);
	
	$output = json_encode(array('studySettings'   => $data['study_settings'],
								'error_code'      => $error['code'],
							    'error_msg'       => $error['message']));
	
	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
 * Populates studies menu with valid options
 * Form: Chart Analysis Menu
 * ----------------------------------------------------------------------
*/	
function get_study_code_HTMLmenu() {
		
	$error = null;
		
	$data['study_code'] = $this->input->post ( 'study_code' );
	$data['chart_id']   = $this->input->post ( 'chart_id' );
	
	$data['session_id']    = $this->session->userdata('db_session_id');
	$data['user_id']       = $this->session->userdata('user_id');
		
	$data['unused_studies_api']  = $this->config->item ( 'unused_studies_api' );
	$data['unused_studies_api']  = $data['unused_studies_api'].$data['chart_id'];
	
	$this->stratscan_model->get_study_code_HTMLmenu($data, $data['study_code_HTML'], $error);

	$output = json_encode(array('study_code_HTML'  => $data['study_code_HTML'] ));
		
	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
 * Saves selected study parameters 
 * Form: Study menu returned from DB
 * ----------------------------------------------------------------------
*/	
function save_study() {
		
	$error                       = null;
	$i                           = null;
	$data['StudyId']             = null;
	$data['studies_menu_detail'] = null;
	$study_found                 = 'N';
		
	$data['chart_id']      = $this->input->post ( 'chart_id' );
	$data['StudyId']      = $this->input->post ( 'StudyId' );
	$data['study']         = $this->input->post ( 'study' );
	$data['display_area']  = $this->input->post ( 'display_area' );
	$data['chart_type']    = $this->input->post ( 'chart_type' );
	$data['StudyColor']    = $this->input->post ( 'StudyColor' );
	$data['StudyName']    = $this->input->post ( 'study_name' );

	if ($data['StudyId'] == ''){
		
		$data['put_study_api'] = $this->config->item ( 'put_study_api' );
		
		$patterns     [0]      = '/<chartId>/';
		$replacements [0]      = $data['chart_id'];
		$patterns     [1]      = '/<study>/';
		$replacements [1]      = $data['study'];
		
		$data['SaveUpdate_study_api'] = preg_replace ( $patterns, $replacements, $data['put_study_api'] );
		
		$data['unused_studies_api']  = $this->config->item ( 'unused_studies_api' );
		$data['studies_api']  = $data['unused_studies_api'].$data['chart_id'];
		
	} else {
		
		$data['edit_study_api'] = $this->config->item ( 'edit_study_api' );
		
		$patterns     [0]      = '/<chartId>/';
		$replacements [0]      = $data['chart_id'];
		$patterns     [1]      = '/<studyId>/';
		$replacements [1]      = $data['StudyId'];
		$patterns     [2]      = '/<study>/';
		$replacements [2]      = $data['study'];
		
		$data['SaveUpdate_study_api'] = preg_replace ( $patterns, $replacements, $data['edit_study_api'] );
		
		$data['used_studies_api']  = $this->config->item ( 'used_studies_api' );
		$data['studies_api']  = $data['used_studies_api'].$data['chart_id'];
	}
	
	$data['session_id']    = $this->session->userdata('db_session_id');
	$data['user_id']       = $this->session->userdata('user_id');	
		
	$data['study_detail'] = Array(
		'chartType'    => $data['chart_type'],
		'panel'        => $data['display_area'],
		'colour'       => $data['StudyColor'],
		'name'         => $data['StudyName'],
		'params'       => array());	
	
	$this->stratscan_model->save_study_code_APIparams($data, $data['study_code_APIparams'], $error);	
	
	if ($error['code'] == 200) {
		
		$i = 0;
		
		foreach( $data['study_code_APIparams'] as $option ) {
			
			$data[$option->code]    = $this->input->post ( $option->code );	
			$data['var_type']       = $option->type;
			
			if ($data['var_type'] == 'integer') {
		   			
				$data['study_params'][$i] = array(
					'code'         => $option->code,
					'valueInt'     => $data[$option->code],
					'valueText'    => null);
				
			} else if ($data['var_type'] == 'text') {
				
				$data['study_params'][$i] = array(
					'code'         => $option->code,
					'valueInt'     => null,
					'valueText'    => $data[$option->code]);
				
			} else if ($data['var_type'] == '') {
				$data['study_params'][$i] = array(
					'code'         => $option->code,
					'valueInt'     => null,
					'valueText'    => null);
			}

			array_push($data['study_detail']['params'],$data['study_params'][$i]);
						
			$i ++;
		}
		
		$this->stratscan_model->save_study($data, $data['StudyId'], $error);
	
		if ($error['code'] == 200) {
		
			$data['used_studies_api']   = $this->config->item ( 'used_studies_api' );
			$data['used_studies_api']   = $data['used_studies_api'].$data['chart_id'];
		
			$data['studies_menu_detail'] = '<table width=93% class="studies_menu_detail">';
	
			$this->stratscan_model->used_studies_detail ( $data, $data['used_studies_detail'], $error);
		
			if(is_array($data['used_studies_detail'])){
				
				foreach( $data['used_studies_detail'] as $option ) {
					$study_found = 'Y';
					$data['studies_menu_detail'] .= '<tr>
														<td class="studies_menu_detail_name"><a href="#" onclick="edit_study_link('.$option->studyId.');"><p>'.$option->desc.'</p></a></td>
														<td><span style="background:'.$option->colour.'">&nbsp;&nbsp;&nbsp;&nbsp;</span></td>														
														<td align="right"><a href="#" onclick="remove_study('.$option->studyId.');"><img src="../assets/default/images/content/remove.svg" alt="delete" width="20" height="20"></a></td>																
													</tr>';
				}
				if ($study_found == 'N'){
					$data['studies_menu_detail'] .= '<tr><td><p>No saved studies.</p></td></tr>';
				}
			}
			
			$data['studies_menu_detail'] .= '</table>';			
		}
	} else {
		
		$data['studies_menu_detail'] = '<p>Error has occurred.</p>
						                <p>Please contact you systems administrator.</p>';
		
	}	
				
	$output = json_encode(array(
		'error_code'         => $error['code'],
		'error_msg'          => $error['message'],
		'StudyId'            => $data['StudyId'],
		'Used_studies'       => $data['studies_menu_detail']
	));
		
	$this->output->set_output($output);		
}
/*
 * ----------------------------------------------------------------------
 * Removes an already saved study Id from the Chart Id
 * Form: Study menu returned from DB
 * ----------------------------------------------------------------------
*/	
function remove_study() {
		
	$error = null;
		
	$data['study_id']      = $this->input->post ( 'studyId' );
		
	$data['session_id']    = $this->session->userdata('db_session_id');
	$data['user_id']       = $this->session->userdata('user_id');
		
	$data['del_study_api'] = $this->config->item ( 'del_study_api' );
		
	$patterns [0]          = '/<studyId>/';
	$replacements [0]      = $data['study_id'];
	$data['del_study_api'] = preg_replace ( $patterns, $replacements, $data['del_study_api'] );
		
	$this->stratscan_model->remove_study($data, $error);
		
	$output = json_encode(array(
		'study_id'     => $data['study_id'],
		'error_code'   => $error['code'],
		'error_msg'    => $error['message']
	));
		
	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
* Saves chart positions as well as zoom
* Form: ChartAnalysis
* ----------------------------------------------------------------------
*/
function chart_sync() {
	
	$error = null;
	
	$data['chart_id']       = $this->input->post ( 'chartId' );
	$data['startDate']      = $this->input->post ( 'startDate' );
	$data['endDate']        = $this->input->post ( 'endDate' );
		
	$data['session_id']     = $this->session->userdata('db_session_id');
	$data['user_id']        = $this->session->userdata('user_id');
	
	$data['chart_sync_api'] = $this->config->item ( 'chart_sync_api' );
	
	$patterns [0]           = '/<chartId>/';
	$replacements [0]       = $data['chart_id'];
	$data['chart_sync_api'] = preg_replace ( $patterns, $replacements, $data['chart_sync_api'] );
	
	$data['chart_sync_detail'] = Array(
			"syncDateFrom"    => $data['startDate'],
			"syncDateTo"      => $data['endDate'] );
	
	$this->stratscan_model->chart_sync($data, $data['NavHtml'], $error);
	
	$output = json_encode(array(
			'NavHtml'      => $data['NavHtml'],
			'error_code'   => $error['code'],
			'error_msg'    => $error['message']
	));
	
	$this->output->set_output($output);
	
}
/*
 * ----------------------------------------------------------------------
 * Saves the alterd study
 * Form: Study Edit menu
 * ----------------------------------------------------------------------
*/
function edit_study() {
	
	$error = null;
	$data['session_id']        = $this->session->userdata('db_session_id');
	$data['user_id']           = $this->session->userdata('user_id');
	
	$data['StudyId']           = $this->input->post ( 'StudyId' );
	$data['chartId']           = $this->input->post ( 'chartId' );
	
	$data['used_studies_api']  = $this->config->item ( 'used_studies_api' );
	$data['used_studies_api']  = $data['used_studies_api'].$data['chartId'];	
	
	$this->stratscan_model->used_studies_detail ( $data, $data['used_studies_detail'], $error);
	
	if(is_array($data['used_studies_detail'])){
		foreach( $data['used_studies_detail'] as $option ) {
			if($option->studyId == $data['StudyId']) {
				$html = $option->studyHTML;
				$html .= '<input type="hidden" name="studies" id="studies" value="'.$option->code.'" />';
			}			
		}					
	}	
	
	$output = json_encode(array(
			'error_code'   => $error['code'],
			'error_msg'    => $error['message'],
			'html'         => $html,
	));
	
	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
 * Deletes a saved chart
 * Form: Study menu trash can
 * ----------------------------------------------------------------------
*/
function DeleteChartId() {
	
	$error = null;
	$data['session_id']  = $this->session->userdata('db_session_id');
	$data['user_id']     = $this->session->userdata('user_id');
	
	$data['StrategyId']  = $this->input->post ( 'StrategyId' );
	$data['ChartId']     = $this->input->post ( 'ChartId' );
	
	$data['chart_api']   = $this->config->item ( 'chart_api' );	
	$data['chart_api']   = $data['chart_api'].'?chartId='.$data['ChartId'];
	
	$this->stratscan_model->DeleteChartId ( $data, $error);
	
	$data['chart_menu_api'] = $this->config->item ( 'chart_menu' );
	$data['chart_menu_api'] = $data['chart_menu_api'].'?strategyId=<strategyId>';
	
	$patterns [0]           = '/<strategyId>/';
	$replacements [0]       = $data['StrategyId'];
	$data['chart_menu_api'] = preg_replace ( $patterns, $replacements, $data['chart_menu_api'] );
	
	$this->stratscan_model->ChartMenu ( $data, $data['ChartId'], $data['menuHTML'], $data['menuChartCount'], $error);
		
	$output = json_encode(array(
			'error_code'   => $error['code'],
			'error_msg'    => $error['message'],
			'menu'         => $data['menuHTML'],
			'ChartId'      => $data['ChartId']
	));
	
	$this->output->set_output($output);	
}

function htmlChartNavigation() {
	
	$error = null;
	$vNavHtml = null;
	
	$data['session_id']      = $this->session->userdata('db_session_id');
	$data['user_id']         = $this->session->userdata('user_id');
	
	$data['chartId']         = $this->input->post ( 'chartId' );
	
	$data['chartNavHtmlApi'] = $this->config->item ( 'chartNavHtmlApi' );
	
	$patterns [0]             = '/<chartId>/';
	$replacements [0]         = $data['chartId'];
	$data['chartNavHtmlApi']  = preg_replace ( $patterns, $replacements, $data['chartNavHtmlApi'] );
	
	$this->stratscan_model->htmlChartNavigation ( $data, $vNavHtml, $error);
		
	$output = json_encode(array(
			'error_code'   => $error['code'],
			'error_msg'    => $error['message'],
			'NavHtml'      => $vNavHtml
	));
	
	$this->output->set_output($output);
}
/*
 * ----------------------------------------------------------------------
* The forward and backward image clicks for chart navigation
* Form: nalysis chart screen
* ----------------------------------------------------------------------
*/
function chartNavigation() {
	
	$error = null;
	$data['session_id']    = $this->session->userdata('db_session_id');
	$data['user_id']       = $this->session->userdata('user_id');
	
	$data['chartId']       = $this->input->post ( 'chartId' );
	$data['navDirection']  = $this->input->post ( 'navDirection' );
	$data['navDate']       = $this->input->post ( 'navDate' );
	
	$data['chart_nav_api'] = $this->config->item ( 'chart_nav' );
	
	$patterns [0]           = '/<chartId>/';
	$replacements [0]       = $data['chartId'];
	$data['chart_nav_api']  = preg_replace ( $patterns, $replacements, $data['chart_nav_api'] );
	
	if ($data['navDirection'] == 'null'){

		$data['chartNavigation_detail'] = Array(
			"navDate"      => $data['navDate'],
			"direction"    => null );	
	}else{
		$data['chartNavigation_detail'] = Array(
			"navDate"      => null,
			"direction"    => $data['navDirection'] );
	}
	
	$this->stratscan_model->chartNavigation ( $data, $error);	
		
	$output = json_encode(array(
			'error_code'      => $error['code'],
			'error_msg'       => $error['message']));		
	
	$this->output->set_output($output);	
}
/*
 * ----------------------------------------------------------------------
 * The API PUT for the area selection menu
 * Form: Saving trade criteria area selection to the database.
 * ----------------------------------------------------------------------
*/
function postSetupAreaSelection() {
	
	$error = null;
	$data['session_id']    = $this->session->userdata('db_session_id');
	$data['user_id']       = $this->session->userdata('user_id');
	
	$data['chartId']       = $this->input->post ( 'chartId' );
	$data['fromDate']      = $this->input->post ( 'fromDate' );
	$data['toDate']        = $this->input->post ( 'toDate' );
	$data['colour']        = $this->input->post ( 'colour' );
	$data['type']          = $this->input->post ( 'type' );
	
	$data['setupAreaApi']  = $this->config->item ( 'setupAreaApi' );
	
	$patterns [0]          = '/<chartId>/';
	$replacements [0]      = $data['chartId'];
	$data['setupAreaApi']  = preg_replace ( $patterns, $replacements, $data['setupAreaApi'] );
	
	if ($data['fromDate'] == '') {

		$data['setupAreaApi_detail'] = Array(
				"from"      => null,
				"to"        => null,
				"colour"    => null,
				"type"      => $data['type']);	
			
	} else if ($data['fromDate'] == 'ColorOnly') {
		
		$data['setupAreaApi_detail'] = Array(
				"colour"    => $data['colour'],
				"type"      => $data['type']);
		
	}else{
		
		$data['setupAreaApi_detail'] = Array(
					"from"      => $data['fromDate'],
					"to"        => $data['toDate'],
				    "colour"    => $data['colour'],
				    "type"      => $data['type']);
	}
		
	$this->stratscan_model->postSetupAreaSelection ( $data, $error);

	if ($error['code'] == 200) {
			
		$data['setupAreasMenuApi'] = $this->config->item ( 'setupAreasMenuApi' );
		$patterns [0]         	  = '/<chartId>/';
		$replacements [0]         = $data['chartId'];
		$data['setupAreasMenuApi'] = preg_replace ( $patterns, $replacements, $data['setupAreasMenuApi'] );
	
		$this->stratscan_model->setupAreasMenu ( $data, $data['areaFromDate'], $data['areaToDate'], $data['menuHTML'], $error);

		$data['fFromDate'] = $data['areaFromDate'];
		$data['fToDate']   = $data['areaToDate'];

	} else {
		$data['fFromDate'] = '';
		$data['fToDate']   = '';
	}
	
	$output = json_encode(array(
			'error_code'    => $error['code'],
			'error_msg'     => $error['message'],
			'fromDate'      => $data['fFromDate'],
			'toDate'        => $data['fToDate']
	));
	
	$this->output->set_output($output);	
}
/*
 * ----------------------------------------------------------------------
 * Identifies the setup area values for each saved chart, and returns 
 * the html to populate the menu
 * Called on saved_chart_onclick(this); 
 * ----------------------------------------------------------------------
*/
function SetupAreasMenu(){

	$error = null;
	$data['session_id']       = $this->session->userdata('db_session_id');
	$data['user_id']          = $this->session->userdata('user_id');
	
	$data['chartId']          = $this->input->post ( 'chartId' );
	$data['setupAreasMenuApi'] = $this->config->item ( 'setupAreasMenuApi' );

	$patterns [0]         	  = '/<chartId>/';
    $replacements [0]         = $data['chartId'];
    $data['setupAreasMenuApi'] = preg_replace ( $patterns, $replacements, $data['setupAreasMenuApi'] );

    $this->stratscan_model->setupAreasMenu ( $data, $data['areaFromDate'], $data['areaToDate'], $data['menuHTML'], $error);
       
    $output = json_encode(array(
    		'error_code'   => $error['code'],
    		'error_msg'    => $error['message'],
    		'menuHTML'     => $data['menuHTML']
    ));
    
    $this->output->set_output($output);
}

function showAddStudiesMenu() {
	
	$error = null;
	$data['session_id']       = $this->session->userdata('db_session_id');
	$data['user_id']          = $this->session->userdata('user_id');
	
	$data['chartId']          = $this->input->post ( 'chartId' );
	$data['addStudyMenuApi']  = $this->config->item ( 'addStudyMenuApi' );

	$patterns [0]         	  = '/<chartId>/';
	$replacements [0]         = $data['chartId'];
	$data['addStudyMenuApi']  = preg_replace ( $patterns, $replacements, $data['addStudyMenuApi'] );
	
	$this->stratscan_model->htmladdStudyMenu ( $data, $data['AddStudyMenuHtml'], $error);
	
	$output = json_encode(array(
		'error_code'       => $error['code'],
		'error_msg'        => $error['message'],
		'AddStudyMenuHtml' => $data['AddStudyMenuHtml']
	));
	
	$this->output->set_output($output);
	
}
/*
 * ----------------------------------------------------------------------
 * analysisPageValidate
 * Validates the criteria page upon form submission
 * ----------------------------------------------------------------------
 */

function analysisPageValidate() {
	
	$error                              = null;
	$saved_strategy_options             = null;
	$saved_chart_options                = null;
	$data['setupAreaValidation']        = 'true';
	$data['setupAreadialogMessage']     = '';
	$data['setupAreadialogMessageHtml'] = '';
	
	$data['session_id']     = $this->session->userdata('db_session_id');
	$data['user_id']        = $this->session->userdata('user_id');
	
	$data['strategyId']     = $this->input->post ( 'strategyId' );
	
	$data['charts_api']     = $this->config->item ( 'charts_api' );
	$data['charts_api']     = $data['charts_api'].$data['strategyId'];
	
	$this->stratscan_model->return_strategy_chart_options  ( $data, $saved_strategy_options, $saved_chart_options, $error);
	
	foreach($saved_chart_options->items as $option){
	
		if ($option->areaFromDate == '') {
			
			$data['setupAreaValidation'] = 'false';
			
			$data['setupAreadialogMessage'] .= '
				<tr>	
					<td>
						<a style="color: #0088cc;" href="#" onclick="saved_chart_onclick(chart_id'.$option->chartId.')">
							<b>'.$option->timeframeDesc.'</b>
						</a>
					</td>
					<td align="right">
						<a href="#" onclick="DeleteChartId(\'false\', '.$option->chartId.')">
							<img id="DelChartId" title="Delete Chart" alt="busy..." src="../assets/default/images/content/remove.svg" width="17" height="17">
						</a>
					</td>		
				</tr>		
			';
		}
	}
	
	if ($data['setupAreaValidation'] == 'false') {
		
		$data['setupAreadialogMessageHtml'] = '
				<p>
      				<span style="float:left; margin:0 7px 50px 0;"></span>
      				The following timeframes require an Entry area before continuing:
  				</p>
  				<table style="width: 80%; margin-left:auto; margin-right:auto;">
					<tbody>'.	
						$data['setupAreadialogMessage']
					.'</tbody>
  				</table>
		';	  
	}

	$output = json_encode(array(
			'error_code'                 => $error['code'],
			'error_msg'                  => $error['message'],
			'setupAreaValidation'        => $data['setupAreaValidation'],
			'setupAreadialogMessageHtml' => $data['setupAreadialogMessageHtml']
	));
	
	$this->output->set_output($output);	
}

/*
 * ----------------------------------------------------------------------
 * Submit of form analysis
 * Load initial criteria page
 * ----------------------------------------------------------------------
 */
function criteria() {

	$error = null;
	
	$data['error_code']  = 200;

	$data['strategy_id'] = $this->input->post ( 'strategy_id' );
	$data['chart_id']    = $this->input->post ( 'chart_id' );

	$data['session_id']  = $this->session->userdata('db_session_id');
	$data['user_id']     = $this->session->userdata('user_id');

	/* Build menu for criteria page */
	$data['chart_menu'] = $this->config->item ( 'chart_menu' );
	$data['chart_menu'] = $data['chart_menu'].'?strategyId=<strategyId>&chartId=<chartId>&mini=<mini>';
	
	$patterns [0]           = '/<strategyId>/';
	$replacements [0]       = $data['strategy_id'];
	$patterns [1]           = '/<chartId>/';
	$replacements [1]       = $data['chart_id'];
	$patterns [2]           = '/<mini>/';
	$replacements [2]       = 'Y';	
	$data['chart_menu_api'] = preg_replace ( $patterns, $replacements, $data['chart_menu'] );
	
	$this->stratscan_model->ChartMenu ( $data, $data['ChartId'], $data['menuHTML'], $data['menuChartCount'], $error);
	
	if ($error['code'] !== 200) {
		show_error($error['message'] , $error['code'] );
	}
	
	$data['menu'] = $data['menuHTML'];
	$data['menu_spacer'] = null;
	for ($i = 0; $i <= $data['menuChartCount']; $i++){
		$data['menu_spacer'] = $data['menu_spacer'].'<td id="spacer" style="border:none"></td>';
	}
	
	/* Build initial amChart framework for static charting (criteria page) */	
	$data['chartload_api'] = $this->config->item ( 'chartload_api' ).'?criteria=Y';
		
	$this->stratscan_model->get_chart_code($data, $data['chart_code'], $error);
	
	if ($error['code'] !== 200) {
		show_error($error['message'] , $error['code'] );
	}
		
	$data['other_jscript'] = $data['chart_code'];
	$patterns     [0] = '/<IMAGES_PATH>/';
	$replacements [0] = '../assets/default/images/amchart/';
		
	$data['other_jscript'] = preg_replace ( $patterns, $replacements, $data['other_jscript'] );	
	
	$this->template->write_view('header', 'stratscan/criteria_menu', $data);
	$this->template->write_view('content', 'stratscan/criteria');
	$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);
}
/*
 * ----------------------------------------------------------------------
*  Opens up the Strategy Definition form
*  Identify the strategy name etc. This will probably become absolute
* ----------------------------------------------------------------------
*/
function identify() {

	$error = null;

	$data['error_code']    = $this->input->post ( 'error_code' );

	if ($data['error_code'] == '' or $data['error_code'] == 200){

		if ($this->input->post ( 'submit' )) {
				
			$saved_strategy_options = null;
			$saved_chart_options    = null;

			$data['strategy_id']    = $this->input->post ( 'strategy_id' );
			$data['session_id']     = $this->session->userdata('db_session_id');
			$data['user_id']        = $this->session->userdata('user_id');

			$data['charts_api'] = $this->config->item ( 'charts_api' );
			$data['charts_api'] = $data['charts_api'].$data['strategy_id'];
				
			$this->stratscan_model->return_strategy_chart_options  ( $data, $saved_strategy_options, $saved_chart_options, $error);

			if ($error['code'] !== 200) {
				show_error($error['message'] , $error['code'] );
			}

			$data['strategy_name'] = (isset($saved_strategy_options->name)? $saved_strategy_options->name : null);
			$data['strategy_desc'] = (isset($saved_strategy_options->desc)? $saved_strategy_options->desc : null);

			$data['hidden_strat_id'] =  array('strategy_id'  => $data['strategy_id']);

			$this->template->write_view('header', 'stratscan/identify_menu', $data);
			$this->template->write_view('content', 'stratscan/identify');
			$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);

		} else {
			$this->strategies();
		}
	} else {
		show_error('Error has occurred. Please contact you systems administrator.' , $data['error_code'] );
	}
}
/*
 * ----------------------------------------------------------------------
*  Submit form Strategy Definition
*  Saves the chart name and description is its function
*  Form: Strategy Definition
* ----------------------------------------------------------------------
*/
function identify_validation() {

	$error = array();

	$data['strategy_id']    = $this->input->post ( 'strategy_id' );
	$data['strategy_name']  = $this->input->post ( 'Strat_Name' );
	$data['strategy_desc']  = $this->input->post ( 'Strat_Desc' );

	$data['hidden_strat_id'] =  array('strategy_id'  => $data['strategy_id']);

	if ($this->form_validation->run('strat_definition') == FALSE) {

		$this->template->write_view('header', 'stratscan/identify_menu', $data);
		$this->template->write_view('content', 'stratscan/identify');
		$this->template->render($region = NULL, $buffer = FALSE, $parse = FALSE);

	} else {
			
		$data['session_id']   = $this->session->userdata('db_session_id');
		$data['user_id']      = $this->session->userdata('user_id');
			
		$data['strategy_api'] = $this->config->item ( 'strategy_api' );
			
		$patterns     [0] = '/<strategyId>/';
		$replacements [0] = $data['strategy_id'];
			
		$data['strategy_api'] = preg_replace ( $patterns, $replacements, $data['strategy_api'] );

		$data['strategy_detail'] = Array(
				"name"       => $data['strategy_name'],
				"desc"       => $data['strategy_desc'],
				"position"   => null,
				"exchange"   => null,
				"product"    => null,
				"contract"   => null );
			
		$this->stratscan_model->save_strategy ( $data, $data['strategy_id'], $error);

		if ($error['code'] !== 200) {
			show_error($error['message'] , $error['code'] );
		}

		redirect('/stratscan/strategies', 'return');
	}
}
/*
 * ----------------------------------------------------------------------
 * Logouts of the application
 * Form: All screens
 * ----------------------------------------------------------------------
 */
function logout() {

	$data['delete_session_api']   = $this->config->item ( 'delete_session_api' );
	$data['session_id']           = $this->session->userdata('db_session_id');
	$data['user_id']              = $this->session->userdata('user_id');

	Unirest::delete($data['delete_session_api'], null, null, $data['user_id'], $data['session_id'] );

	$this->session->sess_destroy();
	redirect('/auth/', 'refresh');
}

}
/* End of file stratscan.php */
/* Location: ./application/controllers/stratscan.php */