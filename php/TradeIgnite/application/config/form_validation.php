<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
		'user_login' => array(
				array(
					'field'   => 'auth_username',
					'label'   => 'User Name',
					'rules'   => 'required|xss_clean|callback_authenticate_username'
				),
				array(
					'field'   => 'auth_password',
					'label'   => 'Password',
					'rules'   => 'required|callback_authenticate_credentials'
				)
		),
		'strat_definition' => array(
				array(
						'field'   => 'Strat_Name',
						'label'   => 'Strategy Name',
						'rules'   => 'required|xss_clean'
				),
				array(
						'field'   => 'Strat_Desc',
						'label'   => 'Strategy Description',
						'rules'   => 'xss_clean'
				)
		)
);

/* End of file form_validation.php */
/* Location: ./application/config/form_validation.php */
		
		