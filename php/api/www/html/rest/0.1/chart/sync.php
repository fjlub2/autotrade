<?php
$method = $_SERVER['REQUEST_METHOD'];

if ($method == "PUT") {
  include '../../../../php/dbConnect.php';

  $entityBody = file_get_contents('php://input');
  $user       = pg_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_USER']));
  $pw         = pg_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_PW']));

  $uctId      = pg_escape_string(htmlspecialchars($_GET["chartId"]));

  $query = "SELECT core_src.\"a_User_setContext\"('{$pw}') json_data";
  $result = pg_query($conn, $query);
  if  (!$result) {
    $session = '{"messages":{"items":[{"code":503,"message":"Service Unavailable"}]}}';
  } else {
    $session = pg_fetch_result($result, 'json_data');
  }

  if($session === NULL) {
    $query = "SELECT ss_src.\"a_ChartSync_put\"('{$uctId}','{$entityBody}') json_data";

    $result = pg_query($conn, $query);
    if  (!$result) {
      $json = '{"messages":{"items":[{"code":503,"message":"Service Unavailable"}]}}';
    } else {
      $json = pg_fetch_result($result, 'json_data');

      $query = "SELECT core_src.\"a_User_clearContext\"() json_data";
      $result = pg_query($conn, $query);
      pg_close($conn);
    }
  } else {
    $json = $session;
  }
} else {
  $json = '{"messages":{"items":[{"code":405,"message":"Method not allowed"}]}}';
}

header('Content-Type: application/json;charset=UTF-8');

$supportsGzip = strpos( $_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip' ) !== false;

if ( $supportsGzip ) {
  $json = gzencode( $json , 5);
  header('Content-Encoding: gzip');
}

header('Cache-Control: no-cache, no-store, no-transform');
header('Content-Length: ' . strlen( $json ));
header('Vary: Accept-Encoding');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');

echo $json;

?>