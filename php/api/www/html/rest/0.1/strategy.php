<?php
#$a_time = round(microtime(true),4);

$method = $_SERVER['REQUEST_METHOD'];

if ($method == "PUT") {
  include '../../../php/dbConnect.php';

  $entityBody = file_get_contents('php://input');
  $user       = pg_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_USER']));
  $pw         = pg_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_PW']));

  if(isset($_GET['strategyId'])) {
    $ustId = pg_escape_string(htmlspecialchars($_GET["strategyId"]));
  }

  $query = "SELECT core_src.\"a_User_setContext\"('{$pw}') json_data";
  $result = pg_query($conn, $query);
  if  (!$result) {
    $session = '{"messages":{"items":[{"code":503,"message":"Service Unavailable"}]}}';
  } else {
    $session = pg_fetch_result($result, 'json_data');
  }

  if($session === NULL) {
    if(isset($ustId)) {
      $query = "SELECT ss_src.\"a_Strategy_put\"('{$ustId}','{$entityBody}') json_data";
    } else {
      $query = "SELECT ss_src.\"a_Strategy_put\"('{$entityBody}') json_data";
    }

    $result = pg_query($conn, $query);
    if  (!$result) {
      $json = '{"messages":{"items":[{"code":503,"message":"Service Unavailable"}]}}';
    } else {
      $json = pg_fetch_result($result, 'json_data');

      $query = "SELECT core_src.\"a_User_clearContext\"() json_data";
      $result = pg_query($conn, $query);
      pg_close($conn);
    }
  } else {
    $json = $session;
  }
} elseif ($method == "GET") {
  include '../../../php/dbConnect.php';

  $user   = pg_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_USER']));
  $pw     = pg_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_PW']));

  $ustId  = pg_escape_string(htmlspecialchars($_GET["strategyId"]));

  $query = "SELECT core_src.\"a_User_setContext\"('{$pw}') json_data";
  $result = pg_query($conn, $query);
  if  (!$result) {
    $session = '{"messages":{"items":[{"code":503,"message":"Service Unavailable"}]}}';
  } else {
    $session = pg_fetch_result($result, 'json_data');
  }

  if($session === NULL) {
    $query = "SELECT ss_src.\"a_Strategy_get\"('{$ustId}'::int) json_data";

    $result = pg_query($conn, $query);
    if  (!$result) {
      $json = '{"messages":{"items":[{"code":503,"message":"Service Unavailable"}]}}';
    } else {
      $json = pg_fetch_result($result, 'json_data');

      $query = "SELECT core_src.\"a_User_clearContext\"() json_data";
      $result = pg_query($conn, $query);
      pg_close($conn);
    }
  } else {
    $json = $session;
  }
} elseif ($method == "COPY") {
  include '../../../php/dbConnect.php';

  $user   = pg_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_USER']));
  $pw     = pg_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_PW']));

  $ustId  = pg_escape_string(htmlspecialchars($_GET["strategyId"]));
  $draft  = pg_escape_string(htmlspecialchars($_GET["draft"]));

  $query = "SELECT core_src.\"a_User_setContext\"('{$pw}') json_data";
  $result = pg_query($conn, $query);
  if  (!$result) {
    $session = '{"messages":{"items":[{"code":503,"message":"Service Unavailable"}]}}';
  } else {
    $session = pg_fetch_result($result, 'json_data');
  }

  if($session === NULL) {
    $query = "SELECT ss_src.\"a_Strategy_copy\"('{$ustId}'::int, '{$draft}') json_data";

    $result = pg_query($conn, $query);
    if  (!$result) {
      $json = '{"messages":{"items":[{"code":503,"message":"Service Unavailable"}]}}';
    } else {
      $json = pg_fetch_result($result, 'json_data');

      $query = "SELECT core_src.\"a_User_clearContext\"() json_data";
      $result = pg_query($conn, $query);
      pg_close($conn);
    }
  } else {
    $json = $session;
  }
} elseif ($method == "DELETE") {
  include '../../../php/dbConnect.php';

  $user   = pg_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_USER']));
  $pw     = pg_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_PW']));

  $ustId  = pg_escape_string(htmlspecialchars($_GET["strategyId"]));

  $query = "SELECT core_src.\"a_User_setContext\"('{$pw}') json_data";
  $result = pg_query($conn, $query);
  if  (!$result) {
    $session = '{"messages":{"items":[{"code":503,"message":"Service Unavailable"}]}}';
  } else {
    $session = pg_fetch_result($result, 'json_data');
  }

  if($session === NULL) {
    $query = "SELECT ss_src.\"a_Strategy_delete\"('{$ustId}'::int) json_data";

    $result = pg_query($conn, $query);
    if  (!$result) {
      $json = '{"messages":{"items":[{"code":503,"message":"Service Unavailable"}]}}';
    } else {
      $json = pg_fetch_result($result, 'json_data');

      $query = "SELECT core_src.\"a_User_clearContext\"() json_data";
      $result = pg_query($conn, $query);
      pg_close($conn);
    }
  } else {
    $json = $session;
  }
} else {
  $json = '{"messages":{"items":[{"code":405,"message":"Method not allowed"}]}}';
}

header('Content-Type: application/json;charset=UTF-8');

$supportsGzip = strpos( $_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip' ) !== false;

#$b_time = round(microtime(true),4);

if ( $supportsGzip ) {
  $json = gzencode( $json , 5);
  header('Content-Encoding: gzip');
}

#$c_time = round(microtime(true),4);

header('Cache-Control: no-cache, no-store, no-transform');
header('Content-Length: ' . strlen( $json ));
header('Vary: Accept-Encoding');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');

echo $json;

/*
$d_time = round(microtime(true),4);

echo "\n length = ".strlen( $json );
echo "\n a -> b = ".substr(($b_time - $a_time),0,5). " seconds";
echo "\n b -> c = ".substr(($c_time - $b_time),0,5). " seconds";
echo "\n c -> d = ".substr(($d_time - $c_time),0,5). " seconds";
echo "\n total  = ".substr(($d_time - $a_time),0,5). " seconds";
*/

?>